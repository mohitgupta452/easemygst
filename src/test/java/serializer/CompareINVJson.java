package serializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class CompareINVJson {
	
	public static void main(String args[]) {
		
		//load gstn json
		//load emg json
		
		ObjectMapper mapper = new ObjectMapper();
		int common=0;
		int extra=0;
		int totalGstn=0;
		int totalEmgst=0;
		double totalGstnTaxable=0.0;
		double commonTotTaxAmt=0.0;
		long totalCommonItem=0;
		double commontTaxable=0.0;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			String gstnJson=new String(Files.readAllBytes(Paths.get("/home/tcinv1052/Desktop/gstn_b2b_new.json")));
			
			String emgJson=new String(Files.readAllBytes(Paths.get("/home/tcinv1052/Desktop/b2b_transaction.json")));
			
//			System.out.println("gstn "+gstnJson);
			/*JsonNode jn=mapper.convertValue(gstnJson, JsonNode.class);
			System.out.println(jn.get("b2b"));*/
			ArrayNode gstnArrNode=(ArrayNode)mapper.readTree(gstnJson);
			ArrayNode emgArrNode=(ArrayNode) mapper.readTree(emgJson);

			
			
			ArrayNode extrasGstn=mapper.createArrayNode();
			ArrayNode extrasEmg=mapper.createArrayNode();

			
			
			
			for(JsonNode gstnNode:gstnArrNode) {
				
				
				boolean isExist=false;
				
				String ctin=gstnNode.get("ctin").asText();
				
				ArrayNode invs =(ArrayNode) gstnNode.get("inv");
				
				
				for(JsonNode inv : invs) {
					Iterator<JsonNode>iterator=inv.get("itms").elements();
					while(iterator.hasNext()){
						JsonNode itm=iterator.next();
						JsonNode det=itm.get("itm_det");
						double txval =det.get("txval").asDouble();
						totalGstnTaxable+=txval;
						
					}
					
				totalGstn++;
				
				for(JsonNode emgNode:emgArrNode) {
					
					try {
					/*	Date date=null;
					try {
						 date=sdf.parse(emgNode.get("invoiceDate").asText());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					if(			
							emgNode.get("invoiceNumber").asText().equalsIgnoreCase(inv.get("inum").asText()) &&
				     /* new SimpleDateFormat("dd-mm-yyyy").format(date).
					  equalsIgnoreCase(gstnNode.get("idt").asText()) &&*/
					 emgNode.get("ctin").asText().equalsIgnoreCase(ctin))
					
					{
						common++;
						isExist=true;
						Iterator<JsonNode>iterator1=inv.get("itms").elements();
						while(iterator1.hasNext()){
							JsonNode itm=iterator1.next();
							JsonNode det=itm.get("itm_det");
							double txval =det.get("txval").asDouble();
							
							double taxAmt= det.get("iamt").asDouble()+det.get("camt").asDouble()+(det.has("csamt")?det.get("csamt").asDouble():0)+det.get("samt").asDouble();
							commontTaxable+=txval;
							commonTotTaxAmt+=taxAmt;
							
							
							
						}
						
						break;
					}
					}
					catch(NullPointerException exception) {
						System.out.println("exception occcurered");
						exception.printStackTrace();
						System.out.println();
					}
					isExist=false;
				}
				
				if(!isExist){
					extra++;
					extrasGstn.add(inv);
}
				
				}
				
				
			}
			
			double totalExtraTaxable=0.0;
			for(JsonNode inv : extrasGstn) {
				if(inv.has("inum")){
					System.out.println("invoice "+inv.get("inum"));
				}
				Iterator<JsonNode>iterator=inv.get("itms").elements();
				while(iterator.hasNext()){
					JsonNode itm=iterator.next();
					JsonNode det=itm.get("itm_det");
					double txval =det.get("txval").asDouble();
					totalExtraTaxable+=txval;
					
				}
			
			}
			
			
			//System.out.println("extras "+extrasGstn);
			
System.out.println("common count-----"+common+" extra count -----"+extra+" total count----"+(common+extra));
System.out.println("total gstn "+totalGstn+" total txval----  "+new BigDecimal(totalGstnTaxable).toPlainString());
System.out.println("total extra gstn taxable "+new BigDecimal(totalExtraTaxable).toPlainString()+" total gstn-total extra "+(new BigDecimal(totalGstnTaxable-totalExtraTaxable).toPlainString()));
System.out.println("common invoices taxable  "+new BigDecimal(commontTaxable).toPlainString());
System.out.println("common total tax amt  "+new BigDecimal(commonTotTaxAmt).toPlainString());



			
			
			
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
