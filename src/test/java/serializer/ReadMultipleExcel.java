package serializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.GstExcel;
 
/**
 * A dirty simple program that reads an Excel file.
 * @author www.codejava.net
 *
 */
public class ReadMultipleExcel {
//	private static List<String> temp;
	private static int _INVOICE_TYPE_INDEX=8;
	private static int _INVOICE_DATE_INDEX = 3;
	private static int _TAXABLE_VALUE_INDEX = 11;
	private static int _SUPPLY_VALUE_INDEX =5;
	private static String cellvalue;
	private static int rowNum;
	private static String cellvaluedate;
	private static String cellvaluetaxable;
	private static String cellvaluesupply;
	private static String cellRange;
	
	
    public static void main(String[] args) throws IOException, AppException {
        String excelFilePath = "C:\\Users\\Ahsaas\\Downloads\\201803INDIAGST_V01-V.xls";
        
        SampleTest test = new SampleTest(new File(excelFilePath));
        ValidateExcel excel=new ValidateExcel();
        
        int count=test.getWb().getNumberOfSheets();
        //System.out.println(test.getSheet().getLastRowNum());
        List<String> temp=new ArrayList<String>();
        
        List<B2BTransactionEntity> b2bTransactions = new ArrayList<>();
		B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
        for (int i = 0; i <count; i++) {
        	test.loadSheet(i);
            Sheet sheet=test.getSheet();
            Iterator<Row> iterator = test.getSheet().iterator();
            int columnCount=0;
           
    		while (iterator.hasNext()) {
    			Row row = iterator.next();
    			rowNum = row.getRowNum();
    			System.out.print(rowNum+" ");    			
    			//cellvalue=test.getWb().getSheetAt(0).getRow(i).getCell(8).toString();
    			cellvaluedate=test.getWb().getSheetAt(0).getRow(i).getCell(3).toString();
    			//cellvaluetaxable=test.getWb().getSheetAt(0).getRow(i).getCell(11).toString();
    			//cellvaluesupply=test.getWb().getSheetAt(0).getRow(i).getCell(5).toString();
    			int maxCell=  row.getLastCellNum();
    			int max=1;
    			//System.out.println(cellvalue);
    			
    			if (rowNum == 0 ) {
    				rowNum++;

    				String cellRange = "A" + rowNum + ":" + test.getCharForNumber(row.getPhysicalNumberOfCells())
    						+ rowNum;
    				List<String> values = test.excelReadRow(cellRange);
    				if (!Objects.isNull(values))
    					values.removeIf(item -> item == null || "".equals(item));
    				if (Objects.nonNull(values))
    					columnCount = values.size();
    			} else if (rowNum > 0) {
    				rowNum++;

    				while(max>0 && max<=maxCell)
    				{
	    				cellRange = "A" + rowNum + ":" + test.getCharForNumber(max) + rowNum;
	    				temp=test.excelReadRow(cellRange);
	    				max++;
    				}
    				if(temp!=null && (!temp.isEmpty())) {
    					System.out.println(cellRange+" "+temp);
    				}
    			}   
    			//System.out.print(temp);
    			
    		}
    		if(temp!=null && i>0) {
//    			b2bDetail.setOriginalInvoiceNumber(temp.get());
//				b2bDetail.setAmmendment(true);
	    		//System.out.print(temp.get(_INVOICE_TYPE_INDEX));
//    			System.out.print(temp);
//				b2bDetail.setOriginalInvoiceDate(temp.get(_INVOICE_DATE_INDEX));
//				b2bDetail.setAmmendment(true);
    			//System.out.print(i+" "+cellvalue+" "+" "+cellvaluedate+" "+cellvaluetaxable+" "+cellvaluesupply);
				//System.out.println(temp.get(_SUPPLY_VALUE_INDEX));
//				b2bDetail.setTaxableValue(temp.get(_TAXABLE_VALUE_INDEX));
//				b2bDetail.setAmmendment(true);
//				b2bDetail.setOriginalInvoiceNumber(temp.get(_SUPPLY_VALUE_INDEX));
//				b2bDetail.setAmmendment(true);
    		}else {
    			//System.out.print(i+" "+cellvalue+" "+" "+cellvaluedate+" "+cellvaluetaxable+" "+cellvaluesupply);
    			//System.out.println(temp);
    		}
    		if(i>0) {
    			//System.out.print(i+" ");

				//System.out.print(cellvaluesupply+" ");
				//System.out.print(temp);
//    		String check=excel.ValidateInvoiceType("firstSheet", _INVOICE_TYPE_INDEX, rowNum,cellvalue);
    		//b2bDetail.setOriginalInvoiceNumber(temp.get(_INVOICE_TYPE_INDEX));
			//b2bDetail.setAmmendment(true);
    		//System.out.print(check);
//    		String date=excel.ValidateDate("firstSheet", _INVOICE_DATE_INDEX, rowNum,cellvaluedate);
    		
    		//System.out.print(date);   		
    		//String tax=excel.ValidateTaxableValue("firstSheet", _TAXABLE_VALUE_INDEX, rowNum,cellvaluetaxable);
    		
        	//System.out.print(tax);  		
        	//String placesupply=excel.ValidatePlaceOfSupply("firstSheet", _SUPPLY_VALUE_INDEX, rowNum,cellvaluesupply);
        	
    		//System.out.print(placesupply);
    		}
    		//System.out.print(temp);
    		System.out.println();
        }
        //System.out.print(cellvalue1);
        test.loadSheet("firstSheet");
        //test.loadSheet(0);
        
    }
}
