package serializer;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.Utility;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility.ROUNDINGMODE;

public class MainTest {

	// @Inject
	// private static TaxpayerService taxpayerService;
	//
	// @Inject
	// public ScheduleManager scheduleManager;
	//
	// @Inject
	// public RedisService redisService;

	public static List<String> getMonthYearsAsPerPreference(String monthYear, String gstin) throws AppException {
		List<String> monthYears = new ArrayList<>();
		String preference = "Q";

		if ("Q".equalsIgnoreCase(preference)) {

			if ("082017".equalsIgnoreCase(monthYear) || "092017".equalsIgnoreCase(monthYear)) {
				monthYears.add("082017");
				monthYears.add("092017");
			} else {

				YearMonth yearMonth = Utility.convertStrToYearMonth(monthYear);

				int returnYear = yearMonth.getYear();

				int quarterMonth = Utility.calculateQuarterMonth(yearMonth);

				for (int month = 3; month >= 1; month--)
					monthYears.add(Utility.appendZeroWithMonthYear(quarterMonth--, returnYear));
			}
		} else if ("M".equalsIgnoreCase(preference)) {
			monthYears.add(monthYear);
		}

		return monthYears;
	}

	public static void main(String[] args) {
		
		String ctin="07AACCO5589P1ZL";
		
		final Pattern pattern = Pattern
				.compile("[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Zz1-9A-Ja-j]{1}[0-9a-zA-Z]{1}");
		Pattern pattern2=Pattern.compile("[0-9]{4}[A-Z]{3}[0-9]{5}[UO]{1}[N][A-Z0-9]{1}");
		
		System.out.println(!(pattern.matcher(ctin).matches()||pattern2.matcher(ctin).matches()));
			
		
		
		}

	public int anothermethod(boolean isTrue) {
		
		int returnin=2;
		
		try {
			return ++ returnin;
		}
		catch(Exception e) {
			
		/*	HttpRequestWithBody body = Unirest.post("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/errorreport");
			body.header("accept", "application/json");
			body.field("file",
					new File("/home/tclap-1020/Downloads/19AAGCS8862B1Z3_8190dd59-d767-4bb1-bc20-038edc197bd8.csv"));
			body.field("folderName", "errorreport");
			body.header("content-type","application/json");
           HttpResponse<String> resposne= body.asString();
           
           resposne.getBody();
           
           System.out.println("response "+resposne.getStatusText());*/

//			HttpResponse<String> jsonResponse = Unirest
//					.post("http://localhost:8080/rest/emgst/upload/datafile")
//					.header("accept", "application/json").field("file",
//							new File("/home/tclap-1020/Downloads/19AABCV4646B1ZF-022018-GSTR-ALL-P (2).CSV")).field("folderName", "errorreport").asString();
//
//				 jsonResponse.getBody();
			
			
		//	String enc="eyJlcnJvckNvZGVzIjoiMTA3In0=";
			
			//Abstractli
		//System.out.println(new String(org.apache.commons.net.util.Base64.decodeBase64(enc)));
			
		
		return 0;
		
//		RedisService redisService = new RedisService();
//		//redisService.init();
//		String token = redisService.getToken("tokengetFile");
//		System.out.println("token" + token);
	}
	
	}
	
	
	
	
}
	

