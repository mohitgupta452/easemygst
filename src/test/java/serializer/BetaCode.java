package serializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.data.view.serializer.B2CSDetailSerializer;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.impl.HSNTransaction;
import com.ginni.easemygst.portal.utils.GstExcel;
 
/**
 * A dirty simple program that reads an Excel file.
 * @author www.codejava.net
 *
 */
public class BetaCode {
//	private static List<String> temp;
	private static int _INVOICE_TYPE_INDEX;
	private static int _INVOICE_NUMBER_INDEX;
	private static int _INVOICE_DATE_INDEX;
	private static int _INVOICE_VALUE_INDEX;
	private static int _TAXABLE_VALUE_INDEX;
	private static int _SUPPLY_VALUE_INDEX;
	private static int rowNum;
	private static String cellRange;
	private static int cellNum;
	
	
	
    public static void main(String[] args) throws IOException, AppException {
    	boolean isInvoice_type;
    	boolean isInvoice_number;
    	boolean isDate_index;
    	boolean isTaxable_index;
    	boolean isSupply_index;
    	boolean isInvoice_value;
        String excelFilePath = "C:\\Users\\Ahsaas\\Downloads\\201803INDIAGST_V01-V.xls";
        
        SampleTest test = new SampleTest(new File(excelFilePath));
        ValidateExcel excel=new ValidateExcel();
        
        int count=test.getWb().getNumberOfSheets();
        //System.out.println(test.getSheet().getLastRowNum());
        List<String> temp=new ArrayList<String>();
        
        List<B2BTransactionEntity> b2bTransactions = new ArrayList<>();
		B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		
		List<B2CSTransactionEntity> b2csTransactions = new ArrayList<>();
		B2CSTransactionEntity b2csTransaction = new B2CSTransactionEntity();
		
		List<B2CLTransactionEntity> b2clTransactions = new ArrayList<>();
		B2CLTransactionEntity b2clTransaction =new B2CLTransactionEntity();
		B2CLDetailEntity b2clDetail= new B2CLDetailEntity();
		
		List<CDNURTransactionEntity> cdnurTransactions = new ArrayList<>();
		CDNURTransactionEntity cdnurTransaction = new CDNURTransactionEntity();
		CDNURDetailEntity cdnurDetail =new CDNURDetailEntity();
		
		List<EXPTransactionEntity> expTransactions = new ArrayList<>();
		EXPTransactionEntity expTransaction = new EXPTransactionEntity();
		ExpDetail expDetail=new ExpDetail();
		
		List<ATTransactionEntity> atTransactions = new ArrayList<>();
		ATTransactionEntity atTransaction = new ATTransactionEntity();
		
		ExcelError ee = new ExcelError();
		

        
        for (int i = 0; i <count-1; i++) {
        	isInvoice_type=false;
        	isInvoice_number=false;
        	isDate_index=false;
        	isTaxable_index=false;
        	isSupply_index=false;
        	isInvoice_value=false;
        	test.loadSheet(i);
            Sheet sheet=test.getSheet();
            Iterator<Row> iterator = test.getSheet().iterator();
            int columnCount=0;
           
    		while (iterator.hasNext()) {
    			Row row = iterator.next();
    			rowNum = row.getRowNum();
    			int colNum = row.getLastCellNum();
    			Iterator<Cell> cellIterator = row.cellIterator();
    			while (cellIterator.hasNext()) 
                {
                	Cell cell = cellIterator.next();
                	
                	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
	                		if(cell.getStringCellValue().equals("Invoice date"))
	                		{
//	                			System.out.println(cell.getColumnIndex());
//	                			System.out.println(colNum);
	                			isDate_index=true;
	                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
	                		}
	                		//System.out.println(rowNum+" "+isdate);
	                		if(cell.getStringCellValue().equals("Taxable Value"))
	                		{
//	                			System.out.println(cell.getColumnIndex());
//	                			System.out.println(colNum);
	                			isTaxable_index=true;
	                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
	                		}
	                		if(cell.getStringCellValue().equals("Invoice Type"))
	                		{
//	                			System.out.println(cell.getColumnIndex());
//	                			System.out.println(colNum);
	                			isInvoice_type=true;
	                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
	                		}
	                		if(cell.getStringCellValue().equals("Place Of Supply"))
	                		{
//	                			System.out.println(cell.getColumnIndex());
//	                			System.out.println(colNum);
	                			isSupply_index=true;
	                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
	                		}
	                		if(cell.getStringCellValue().equals("Invoice Number"))
	                		{
//	                			System.out.println(cell.getColumnIndex());
//	                			System.out.println(colNum);
	                			isInvoice_number=true;
	                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
	                		}
	                		if(cell.getStringCellValue().equals("Invoice Value"))
	                		{
//	                			System.out.println(cell.getColumnIndex());
//	                			System.out.println(colNum);
	                			isInvoice_value=true;
	                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
	                		}
                	}
                }
    			int maxCell=  row.getLastCellNum();
    			int max=1;
    			if (rowNum == 0 ) {
    				rowNum++;

    				String cellRange = "A" + rowNum + ":" + test.getCharForNumber(row.getPhysicalNumberOfCells())
    						+ rowNum;
    				List<String> values = test.excelReadRow(cellRange);
    				if (!Objects.isNull(values))
    					values.removeIf(item -> item == null || "".equals(item));
    				if (Objects.nonNull(values))
    					columnCount = values.size();
    			} else if (rowNum > 0) {
    				rowNum++;
    				while(max>0 && max<=maxCell)
    				{
	    				cellRange = "A" + rowNum + ":" + test.getCharForNumber(max) + rowNum;
	    				temp=test.excelReadRow(cellRange);
	    				max++;
    				}
    				if(temp!=null) {
//    					System.out.println(temp);
    					if(isDate_index) {
	    					Date date=excel.ValidateDate(ee,test.getWb().getSheetName(i), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
	    					if(date!=null) {
	    						if(test.getWb().getSheetName(i).equals("b2b")) {
			    					b2bDetail.setOriginalInvoiceDate(date);
			    					b2bDetail.setAmmendment(true);
//			    					System.out.print(date);
	    						}else if(test.getWb().getSheetName(i).equals("b2cl")) {
    								b2clDetail.setOriginalInvoiceDate(date);
//    								System.out.print(date);
    							}else if(test.getWb().getSheetName(i).equals("b2cs")) {
//    								System.out.print(date);
//	    							b2csTransaction.set
	    						}else if(test.getWb().getSheetName(i).equals("cdnur")) {
//	    							System.out.print(date);
//	    							cdnurDetail.set
	    						}else if(test.getWb().getSheetName(i).equals("exp")) {
//	    							System.out.print(date);
	    							expDetail.setOriginalInvoiceDate(date);
	    							expDetail.setAmmendment(true);
	    						}
	    					}else {
	    						System.out.println(ee.getErrorDesc());
	    					}
    					}
    					if(isInvoice_number) {
    						String invoice_number=excel.ValidateInvoiceNumber(ee,test.getWb().getSheetName(i), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
    						if(invoice_number!=null) {
    							if(test.getWb().getSheetName(i).equals("b2b")) {
		    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
		        					b2bDetail.setAmmendment(true);
//		    						System.out.print(invoice_number+" ");
    							}else if(test.getWb().getSheetName(i).equals("b2cl")) {
    								b2clDetail.setInvoiceNumber(invoice_number);
    								b2clDetail.setAmmendment(true);
//    								System.out.print(invoice_number+" ");
    							}else if(test.getWb().getSheetName(i).equals("b2cs")) {
    								b2csTransaction.setInvoiceNumber(invoice_number);
    								b2csTransaction.setAmmendment(true);
//    								System.out.print(invoice_number+" ");
    							}else if(test.getWb().getSheetName(i).equals("cdnur")) {
    								cdnurDetail.setInvoiceNumber(invoice_number);
    								cdnurDetail.setAmmendment(true);
//    								System.out.print(invoice_number+" ");
    							}else if(test.getWb().getSheetName(i).equals("exp")) {
    								expDetail.setInvoiceNumber(invoice_number);
    								expDetail.setAmmendment(true);
//    								System.out.print(invoice_number+" ");
    							}
    						}else {
    							System.out.println(ee.getErrorDesc());
    						}
    						
    					}
    					if(isInvoice_value) {
    						double invoice_value=excel.ValidateInvoiceValue(ee,test.getWb().getSheetName(i), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
    						if(invoice_value!=0L) {
    							if(test.getWb().getSheetName(i).equals("b2b")) {
//		    						b2bDetail.setInvoiceValue(invoice_value);
//		        					b2bDetail.setAmmendment(true);
//		    						System.out.print(invoice_value+" ");
    							}else if(test.getWb().getSheetName(i).equals("b2cl")) {
//    								b2clDetail.setInvoiceValue(invoice_value);
//    								b2clDetail.setAmmendment(true);
//    								System.out.print(invoice_value+" ");
    							}else if(test.getWb().getSheetName(i).equals("b2cs")) {
//    								b2csTransaction.setInvoiceValue(invoice_value);
//    								b2csTransaction.setAmmendment(true);
//    								System.out.print(invoice_value+" ");
    							}else if(test.getWb().getSheetName(i).equals("cdnur")) {
//    								cdnurDetail.setInvoiceValue(invoice_value);
//    								cdnurDetail.setAmmendment(true);
//    								System.out.print(invoice_value+" ");
    							}else if(test.getWb().getSheetName(i).equals("exp")) {
//    								expDetail.setInvoiceValue(invoice_value);
//    								expDetail.setAmmendment(true);
//    								System.out.print(invoice_value+" ");
    							}
    						}else {
//    							System.out.println(ee.getErrorDesc());
    						}
    						
    					}
    					if(isInvoice_type) {
	    					String invoiceType=excel.ValidateInvoiceType(ee,test.getWb().getSheetName(i), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
	    					if(invoiceType!=null) {
	    						if(test.getWb().getSheetName(i).equals("b2b")) {
			    					b2bDetail.setInvoiceType(invoiceType);
			    					b2bDetail.setAmmendment(true);
//			    					System.out.print(invoiceType+" ");
	    						}else if(test.getWb().getSheetName(i).equals("b2cs")) {
//	    							b2csTransaction.set
//	    							System.out.print(invoiceType+" ");
	    						}
	    					}else {
	    						System.out.println(ee.getErrorDesc());
	    					}
    					}
    					
    					if(isTaxable_index) {
	    					double taxable_value=excel.ValidateTaxableValue(ee,test.getWb().getSheetName(i),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
	    					if(taxable_value!=0L) {
	    						if(test.getWb().getSheetName(i).equals("b2b")) {
			    					b2bDetail.setTaxableValue(taxable_value);
			    					b2bDetail.setAmmendment(true);
//			    					System.out.print(taxable_value+" ");
	    						}else if(test.getWb().getSheetName(i).equals("b2cl")) {
    								b2clDetail.setTaxableValue(taxable_value);
    								b2clDetail.setAmmendment(true);
//    								System.out.print(taxable_value+" ");
    							}else if(test.getWb().getSheetName(i).equals("b2cs")) {
	    							b2csTransaction.setTaxableValue(taxable_value);
//	    							System.out.print(taxable_value+" ");
	    							b2csTransaction.setAmmendment(true);
	    						}else if(test.getWb().getSheetName(i).equals("cdnur")) {
	    							cdnurDetail.setTaxableValue(taxable_value);
	    							cdnurDetail.setAmmendment(true);
//	    							System.out.print(taxable_value+" ");
	    						}else if(test.getWb().getSheetName(i).equals("exp")) {
	    							expDetail.setTaxableValue(taxable_value);
	    							expDetail.setAmmendment(true);
//	    							System.out.print(taxable_value+" ");
	    						}
	    					}else {
	    						System.out.println(ee.getErrorDesc());
	    					}
    					}
    					
    					if(isSupply_index) {
	    					String place_of_supply=excel.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
	    					if(place_of_supply!=null) {
	    						if(test.getWb().getSheetName(i).equals("b2b")) {
//	    							System.out.print(place_of_supply);
	    							b2bDetail.setPos(place_of_supply);
	    							b2bDetail.setAmmendment(true);
	    						}else if(test.getWb().getSheetName(i).equals("b2cl")) {
	    							b2clDetail.setPos(place_of_supply);
	    							b2clDetail.setAmmendment(true);
//	    							System.out.print(place_of_supply);
	    						}else if(test.getWb().getSheetName(i).equals("b2cs")) {
//	    							b2csTransaction.setPos(place_of_supply);
//	    							b2csTransaction.setAmmendment(true);
//	    							System.out.print(place_of_supply);
	    						}else if(test.getWb().getSheetName(i).equals("cdnur")) {
//	    							cdnurDetail.set;
	    							b2csTransaction.setAmmendment(true);
	    						}else if(test.getWb().getSheetName(i).equals("at")) {
	    							atTransaction.setPos(place_of_supply);;
	    							atTransaction.setAmmendment(true);
//	    							System.out.print(place_of_supply);
	    						}
	    					}else {
	    						System.out.println(ee.getErrorDesc());
	    					}
	    					
    					}
    					
    					//System.out.println("data saved successfully");
    					//Declare the setter method of place of supply
    					
//	    				System.out.println(temp.get(_INVOICE_TYPE_INDEX));
//	    				System.out.println(temp.get(_TAXABLE_VALUE_INDEX));
//	    				System.out.println(temp.get(_SUPPLY_VALUE_INDEX));
//	    				System.out.println(temp.get(_INVOICE_DATE_INDEX));
//    					System.out.println(temp.get(_INVOICE_NUMBER_INDEX));
//    					System.out.println(b2bDetail.getPos());
    				}
    			}
    		}
    			System.out.println();
        }
    }
}