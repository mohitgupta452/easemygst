package serializer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		XSSFWorkbook workbook=new XSSFWorkbook();
		XSSFSheet sheet=workbook.createSheet("Write Excel");
		Object[][] data= {{"test1","department1",123},{"test2","department2",123},{"test3","department3",123},{"test4","department4",123}};
		int rowcount=0;
		for(Object[] d:data)
		{
			Row row=sheet.createRow(rowcount++);
			int colcount=0;
			for(Object field:d)
			{
				Cell cell=row.createCell(colcount++);
				if(field instanceof String)
				{
					cell.setCellValue((String)field);
				}else {
					cell.setCellValue((Integer)field);
				}
			}
		}
		try {
		FileOutputStream outputStream = new FileOutputStream("C:\\Users\\Ahsaas\\Downloads\\JavaBooks.xlsx");
				workbook.write(outputStream);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}
