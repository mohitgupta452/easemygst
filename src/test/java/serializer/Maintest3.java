package serializer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;
import org.apache.poi.util.SystemOutLogger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class Maintest3 {

	public static void main(String[] args) throws AppException {

		Path path = Paths.get("/home/tclap-1020/Downloads/b2b.json");

		try {
			String content = new String(Files.readAllBytes(Paths.get("/home/tclap-1020/Downloads/b2b.json")));

			ObjectMapper mapper = new ObjectMapper();

			GetGstr2Resp getGstr2Resp = JsonMapper.objectMapper.readValue(content, GetGstr2Resp.class);

			StringBuilder response = new StringBuilder();

			response.append("CTIN,CTIN FILLING STATUS,INVOICE NUMBER,INVOICE DATE,SNUM,SGST,CGST,IGST,RATE").append("\n");

			for (B2BTransactionEntity b2bTransactionEntity : getGstr2Resp.getB2b()) {

				response.append(b2bTransactionEntity.getCtin()).append(",")
						.append(b2bTransactionEntity.getFillingStatus()).append(",");

				for (B2BDetailEntity b2bDetailEntity : b2bTransactionEntity.getB2bDetails()) {

					response.append(b2bDetailEntity.getInvoiceNumber()).append(",")
							.append(b2bDetailEntity.getInvoiceDate()).append(",");

					for (Item item : b2bDetailEntity.getItems()) {

						response.append(item.getSerialNumber()).append(",").append(item.getSgst()).append(",")
								.append(item.getCgst()).append(",").append(item.getIgst()).append(",")
								.append(item.getCess()).append(",").append(item.getTaxRate()).append(",")
								.append(item.getTaxableValue()).append("\n");
					}

				}

			}
			
			System.out.println(response);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
