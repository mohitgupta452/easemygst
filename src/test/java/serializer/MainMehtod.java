package serializer;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.apache.poi.ss.usermodel.Row;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;
import com.ginni.easemygst.portal.utils.GstExcel;

public class MainMehtod {

	public static void main(String[] args) throws Exception {

//		List<B2BTransactionEntity> b2bTransactions = new ArrayList<>();
		B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();
//		B2BDetailEntity b2bDetail = new B2BDetailEntity();
//		

		List<B2CSTransactionEntity> b2csTransactions = new ArrayList<>();
		B2CSTransactionEntity b2csTransaction = new B2CSTransactionEntity();

		TaxpayerGstin taxpayerGstin = new TaxpayerGstin();
		taxpayerGstin.setGstin("gst123");
		String taxpayer = taxpayerGstin.getGstin();
		Set<ReturnTransactionDTO> ReturnTransaction = new HashSet<ReturnTransactionDTO>();

		String excelFilePath = "C:\\Users\\Ahsaas\\Downloads\\201803INDIAGST_V01-V.xls";
		String excelFileGSTR2 = "C:\\Users\\Ahsaas\\Downloads\\GNGSTR2.xls";
		GstExcel gstexcel = new GstExcel(new File(excelFilePath));
		System.out.println("program is running now...");
		Transaction factory1 = new TransactionFactory("B2B");
		gstexcel.loadSheet(0);
//		Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getReturnTransactionsByGstin(gstin, ReturnType.GSTR1.toString());
//		gstexcel.uploadExcel(ReturnType.GSTR1.toString(), b2bTransaction.getMonthYear(), null, "sap",taxpayerGstin, ReturnTransaction, userBean, "no");
//		Transaction factory1=new TransactionFactory("B2CS");
//		gstexcel.loadSheet(4);
//		gstexcel.uploadExcel(ReturnType.GSTR1.toString(), b2csTransaction.getMonthYear(), null, "sap",taxpayerGstin, ReturnTransaction, userBean, "no");
//		gstexcel.ExcelData(gstexcel,ReturnType.GSTR1.toString(), b2bTransaction.getMonthYear(),taxpayerGstin,gstexcel.getWb().getSheetName(0));
		String sheetName1 = gstexcel.getWb().getSheetName(0);
		gstexcel.uploadData(gstexcel, TransactionType.B2B.toString(), ReturnType.GSTR1.toString(),
				b2bTransaction.getMonthYear(), taxpayerGstin, sheetName1, SourceType.SAP);

		Transaction factory2 = new TransactionFactory("B2CS");
		gstexcel.loadSheet(4);
		String sheetName2 = gstexcel.getWb().getSheetName(4);
		gstexcel.uploadData(gstexcel, TransactionType.B2CS.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName2, SourceType.SAP);

		Transaction factory3 = new TransactionFactory("AT");
		gstexcel.loadSheet(12);
		String sheetName3 = gstexcel.getWb().getSheetName(12);
		gstexcel.uploadData(gstexcel, TransactionType.AT.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName3, SourceType.SAP);

		Transaction factory4 = new TransactionFactory("B2CL");
		gstexcel.loadSheet(3);
		String sheetName4 = gstexcel.getWb().getSheetName(3);
		gstexcel.uploadData(gstexcel, TransactionType.B2CL.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName4, SourceType.SAP);

		Transaction factory5 = new TransactionFactory("CDN");
		gstexcel.loadSheet(6);
		String sheetName5 = gstexcel.getWb().getSheetName(6);
		gstexcel.uploadData(gstexcel, TransactionType.CDN.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName5, SourceType.SAP);

		Transaction factory6 = new TransactionFactory("EXP");
		gstexcel.loadSheet(10);
		String sheetName6 = gstexcel.getWb().getSheetName(10);
		gstexcel.uploadData(gstexcel, TransactionType.EXP.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName6, SourceType.SAP);

		Transaction factory7 = new TransactionFactory("CDNUR");
		gstexcel.loadSheet(8);
		String sheetName7 = gstexcel.getWb().getSheetName(8);
		gstexcel.uploadData(gstexcel, TransactionType.CDNUR.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName7, SourceType.SAP);

		Transaction factory8 = new TransactionFactory("HSNSUM");
		gstexcel.loadSheet(17);
		String sheetName8 = gstexcel.getWb().getSheetName(17);
		gstexcel.uploadData(gstexcel, TransactionType.HSNSUM.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName8, SourceType.SAP);

		Transaction factory9 = new TransactionFactory("TXPD");
		gstexcel.loadSheet(14);
		String sheetName9 = gstexcel.getWb().getSheetName(14);
		gstexcel.uploadData(gstexcel, TransactionType.TXPD.toString(), ReturnType.GSTR1.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName9, SourceType.SAP);
		

		GstExcel gstr2 = new GstExcel(new File(excelFileGSTR2));
		
		gstr2.loadSheet(1);
		String sheetNameb2b = gstr2.getWb().getSheetName(1);
		gstexcel.uploadData(gstr2, TransactionType.B2B.toString(), ReturnType.GSTR2.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetNameb2b, SourceType.SAP);
		
		
		Transaction factory10 = new TransactionFactory("B2BUR");
		gstr2.loadSheet(2);
		String sheetName10 = gstr2.getWb().getSheetName(2);
		gstr2.uploadData(gstr2, TransactionType.B2BUR.toString(), ReturnType.GSTR2.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName10, SourceType.SAP);
		
		Transaction factory11 = new TransactionFactory("IMPG");
		gstr2.loadSheet(3);
		String sheetName11 = gstr2.getWb().getSheetName(3);
		gstr2.uploadData(gstr2, TransactionType.IMPG.toString(), ReturnType.GSTR2.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName11, SourceType.SAP);
		
		Transaction factory12 = new TransactionFactory("TXPD");
		gstr2.loadSheet(8);
		String sheetName12 = gstr2.getWb().getSheetName(8);
		gstr2.uploadData(gstr2, TransactionType.TXPD.toString(), ReturnType.GSTR2.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName12, SourceType.SAP);
		
		Transaction factory13 = new TransactionFactory("NIL");
		gstr2.loadSheet(5);
		String sheetName13 = gstr2.getWb().getSheetName(5);
		gstr2.uploadData(gstr2, TransactionType.NIL.toString(), ReturnType.GSTR2.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetName13, SourceType.SAP);
		
		gstr2.loadSheet(11);
		String sheetNamehsn = gstr2.getWb().getSheetName(11);
		gstexcel.uploadData(gstr2, TransactionType.HSNSUM.toString(), ReturnType.GSTR2.toString(),
				b2csTransaction.getMonthYear(), taxpayerGstin, sheetNamehsn, SourceType.SAP);
		

	}
}
