package serializer;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.Constant;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

public class QuarterTest {
	
	public static void main(String[] args) {
		String monthId="072018";
		YearMonth month=YearMonth.parse(monthId, DateTimeFormatter.ofPattern("MMyyyy"));
		System.out.println(CalanderCalculatorUtility.getFiscalYearByMonthYear(month));
		Object gstin="asdasd";
		Object fyear="sdsa";
		String preference="M";
		System.out.println(String.format(ApplicationMetadata.PREFERENCE_SAVED,gstin,Constant.Returnpreference.valueOf(preference),fyear));
		System.out.println(Constant.Returnpreference.valueOf("M"));
		System.out.println(CalanderCalculatorUtility.quarterlist("2017-2018"));
		System.out.println(CalanderCalculatorUtility.quarterlist("2018-2019"));
		
		
	}
	
}
