package serializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.GstExcel;
 
/**
 * A dirty simple program that reads an Excel file.
 * @author www.codejava.net
 *
 */
public class ReadExcelData {
     
    public static void main(String[] args) throws IOException, AppException {
        String excelFilePath = "C:\\Users\\Ahsaas\\Downloads\\201803INDIAGST_V01-V.xls";
        
   /*
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
         
        Workbook workbook = new HSSFWorkbook(inputStream);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();
        
        System.out.println(workbook.getNumberOfSheets());
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
             
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                 
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        System.out.print(cell.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        System.out.print(cell.getBooleanCellValue());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        System.out.print(cell.getNumericCellValue());
                        break;
                }
                System.out.print(" - ");
            }
            System.out.println();
        }*/
        SampleTest test = new SampleTest(new File(excelFilePath));
        
        int count=test.getWb().getNumberOfSheets();
        System.out.println(count);
        test.loadSheet("firstSheet");
        
        test.loadSheet(1);
        
        Sheet sheet=test.getSheet();
        Iterator<Row> iterator = test.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();

			//System.out.println(rowNum);
			int maxCell=  row.getLastCellNum();
			int max=1;
			//System.out.println(rowNum);
			
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + test.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = test.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;

				while(max>0 && max<=maxCell)
				{
				String cellRange = "A" + rowNum + ":" + test.getCharForNumber(max) + rowNum;
				
				List<String> temp = test.excelReadRow(cellRange);
				System.out.print(cellRange);
				System.out.println(temp);
				max++;
				}
			}
			System.out.println();
			
			/*
			Iterator<Cell> cellIterator = row.cellIterator();
			
			
			
			test.excelReadRow(cellRange);
			*/
            
//            while (cellIterator.hasNext()) {
//                Cell cell = cellIterator.next();
//                switch (cell.getCellType()) {
//                case Cell.CELL_TYPE_STRING:
//                    System.out.println(cell.getStringCellValue());
//                    break;
//                case Cell.CELL_TYPE_BOOLEAN:
//                    System.out.print(cell.getBooleanCellValue());
//                    break;
//                case Cell.CELL_TYPE_NUMERIC:
//                    System.out.print(cell.getNumericCellValue());
//                    break;
//                case Cell.CELL_TYPE_FORMULA:
//                    System.out.print(cell.getCellFormula());
//                    break;
//                }
//            }
		  
		}
    }
}
