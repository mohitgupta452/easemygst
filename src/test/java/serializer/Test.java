package serializer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.Period;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalAmount;
import java.util.Calendar;

import com.ginni.easemygst.portal.business.dto.OffsetLiabiltyDTO.TaxDetail;


import lombok.Data;

public @Data class Test {

	private double a=10;
	
	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		YearMonth previous=YearMonth.parse("062017", DateTimeFormatter.ofPattern("MMyyyy")).minus(Period.ofMonths(1));
		   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-LL-yyyy");
		   String firstday = LocalDate.of(previous.getYear(), previous.getMonthValue(), 1).with(TemporalAdjusters.firstDayOfMonth()).format(formatter);
		   String lastDay = LocalDate.of(previous.getYear(), previous.getMonthValue(), 1).with(TemporalAdjusters.lastDayOfMonth()).format(formatter);
		   
		   
		System.out.println(firstday);
		System.out.println(lastDay);

//	    int lastDate = a.get(Calendar.DATE);
	}
	
	public double value(String type) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

		  Method m = this.getClass().getDeclaredMethod("get"+type);
		  double igst= (double) m.invoke(this);
		  System.out.println(igst);
		  return igst;
	}
	
}
