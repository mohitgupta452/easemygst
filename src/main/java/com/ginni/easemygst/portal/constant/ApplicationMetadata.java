package com.ginni.easemygst.portal.constant;

public interface ApplicationMetadata {
	
	String GST_YEARMONTH="072017";
	
	String _GST_START_FINANCIALYEAR="2017-2018";
	
	String SERVER_IP_ADDRESS="server_ip_address";
	
	String GET_IP_URL="http://checkip.amazonaws.com";
	
	long AUTH_TOKEN_VALIDITY_MS=2*60*60*1000;//2 hours
	
	String FINANCIAL_YEAR_START = "fstart";
	
	String FINANCIAL_YEAR_END = "fend";
	
	String GST_START_DATE = "gststart";

	String FAILURE_MSG = "Unable to process your request, Please check error for more information";
	
	String PARTNER_FAILURE_MSG = "Invalid partner code %s, Please check the code and try again.";
	
	String PARTNER_SUBMIT_SUCCESS_MSG = "Partner %s (%s) tagged successfully with your account.";
	
	String INVALID_JSON = "Invalid JSON structure. Please check the request payload.";
	
	String MISSING_REQUEST_PAYLOAD="request payload is missing";
	
	String NOT_OWNER_MSG = "You are not registered as owner of the mentioned PAN Number, To proceed further please contact %s Or if you are real owner of this pan, please contact support@easemygst.com to proceed further.";
	
	String SUCCESS_MSG = "Request completed successfully";
	
	String SUCCESS_MSG_3B = "GSTR 3B data generated successfully. Please click on the download tab above to view the same.";
	
	String PROCESS_DATA_SUCCESS_MSG = "We have successfully startrd the Processing of the file, this will take a moment to finish. Please check logs for more information.";
	
	String PROCESS_DATA_FAILURE_MSG = "Request completed successfully";
	
	String NEW_USER_ACCOUNT = "Account has been successfully created and mail has been sent to %s";
	
	String ERP_SUCCESS_MSG = "ERP has been configured successfully for your account. Please see the logs for further information.";
	
	String ERP_FAILURE_MSG = "Unable to connect to ERP. Please try after some time.";
	
	String ERP_REMOVE_SUCCESS_MSG = "ERP has been successfully disconnected for your account.";
	
	String ERP_REMOVE_FAILURE_MSG = "Unable to disconnect the ERP. Please try after some time.";
	
	String PLAN_CHANGE_SUCCESS = "Your current subscription plan has been changed and same will get reflect after end of this term.";
	
	String PLAN_CHANGE_FAILURE = "Unable to change your current subscription plan at this time. Please contact support@easemygst.com for more information";
	
	String PLAN_UNITS_CHANGE_SUCCESS = "Your current subscription plan GSTIN has been changed to %s";
	
	String PLAN_UNITS_END_CHANGE_SUCCESS = "Your current subscription plan GSTIN has been changed to %s and same will get reflect after end of this term.";
	
	String PLAN_UNITS_CHANGE_FAILURE = "Unable to change your current subscription plan GSTIN at this time. Please contact support@easemygst.com for more information";
	
	String CUSTOMER_BILLING_SUCCESS_MSG = "Your billing details has been successfully updated in our system.";
	
	String CUSTOMER_BILLING_FAILURE_MSG = "Unable to change to your billing details at this time. Please try after some time.";
	
	String EMAIL_INVOICE_SUCCESS = "Invoice number %s has been successfully emailed to you on your registered email address %s";
	
	String EMAIL_INVOICE_FAILURE = "Unable to send invoice at this time. Please try after sometime.";
	
	String INVOICE_ADD_MSG = "Invoice %s created successfully for your GSTIN %s";
	
	String INVOICE_UPDATE_MSG = "Invoice %s updated successfully for your GSTIN %s";
	
	String INVOICE_DELETE_MSG = "Invoice %s deleted successfully for your GSTIN %s";
	
	String INVOICE_EMAIL = "Invoice email sent successfully to %s";
	
	String HSN_SUCCESS_MSG = "Requested HSN added successfully to your list.";
	
	String EXCEL_UPLOAD = "%s items uploaded successfully";
	
	String CHANGE_PASSWORD_MSG = "Password has been changed to new password. Please use new password for sign in next time.";
	
	String CREATE_ORGANISATION = "Organisation %s created successfully for your account.";
	
	String RENAME_ORGANISATION = "Organisation name successfully changed to %s for your account.";
	
	String FORGOT_PASSWORD_MSG = "Verification required, an OTP has been sent to your registered mobile number %s";
	
	String STORE_SUCCESS_MSG = "Welcome %s, please validate your details. OTP has been successfully sent to %s";
	
	String SIGNIN_SUCCESS_MSG = "Welcome %s, you have now successfully signed in.";
	
	String SIGNUP_SUCCESS_MSG = "Thank you, mobile verified successfully. Please add pan or choose skip to proceed.";
	
	String FORGOT_SUCCESS_MSG = "Thank you, mobile verified successfully. Please enter new password to proceed further.";
	
	String CONTACT_SUCCESS_MSG = "Thank you for contacting us. We will get back to you shortly.";
	
	String PARTNER_VERIFY_SUCCESS_MSG = "Thank you for registering with us. We have sent an OTP on your mobile number %s and email address %s";
	
	String PARTNER_SUCCESS_MSG = "Thank you for registering with us. We have sent you a unique partner code on your registered email address";
	
	String USER_SUCCESS_MSG = "Thank you for registering with us.";
	
	String PARTNER_OTP_SUCCESS_MSG = "New OTP has been successfully sent to %s";
	
	String DUPLICATE_PARTNER_MSG = "Unable to process your request, entered emailor mobile is already registered with us.";
	
	String DUPLICATE_HSNDATA_MSG = "Duplicate code, it was there in your list already.";
	
	String RESEND_OTP_SUCCESS = "OTP has been successfully resent to your registered mobile number %s";
	
	String MOBILE_OTP_SUCCESS = "OTP has been successfully sent to your mobile number %s";
	
	String PAN_ADD = "PAN %s has been added to your account successfully.";
	
	String PAN_UPDATE = "PAN %s has been updated successfully.";
	
	String PAN_BILLING_UPDATE = "PAN %s billing address has been updated successfully.";
	
	String PAN_DELETE = "PAN %s has been deleted successfully.";
	
	String GSTIN_ADD = "GSTIN %s has been added successfully";
	
	String GSTIN_UPDATE = "GSTIN %s has been updated successfully.";
	
	String TURNOVER_UPDATE = "Turnover Details has been updated successfully for GSTIN %s";
	
	String GSTIN_DELETE = "GSTIN %s has been deleted successfully.";
	
	String VENDOR_ADD = "Vendor %s has been added successfully for PAN %s";
	
	String CUSTOMER_ADD = "Customer %s has been added successfully for PAN %s";
	
	String VENDOR_GSTIN_ADD = "Vendor %s has been added successfully for GSTIN %s";
	
	String VENDOR_CUSTOMER_GSTIN_ADD = "%s %s has been added successfully for GSTIN %s";
	
	String ITEM_GSTIN_ADD = "Item %s has been added successfully for GSTIN %s";
	
	String VENDOR_UPDATE = "Vendor %s has been updated successfully";
	
	String VENDOR_DELETE = "Vendor %s has been deleted successfully.";
	
	String CUSTOMER_UPDATE = "Customer %s has been updated successfully";
	
	String CUSTOMER_DELETE = "Customer %s has been deleted successfully.";
	
	String INVOICE_ADD_GSTIN_USER = "Access has been grant successfully to user %s for GSTIN %s for managing Vendor/Customer/Invoice";
	
	String GSTIN_ADD_USER = "Access has been grant successfully to user %s for GSTIN %s";
	
	String GSTIN_UPDATE_USER = "Access has been updated successfully for the user %s for GSTIN %s";
	
	String GSTIN_DELETE_USER = "Access has been revoked successfully from GSTIN %s for selected user";
	 
	String GSTIN_REVOKE_USER = "Access has been revoked successfully from GSTIN %s for user %s";
	
	String GSTIN_REVOKE_ALL_USER = "Complete access has been revoked successfully for user %s";
	
	String PAN_ADD_USER = "Access has been grant successfully to user %s for PAN %s";
	
	String PAN_DELETE_USER = "Access has been revoked successfully from PAN %s for selected user";
	
	String GSTIN_OTP = "OTP has been sent successfully sent to your registered mobile number by GSTN";
	
	String GSTIN_AUTH = "GSTIN has been successfully authenticated by GSTN";
	
	String GSTIN_AUTH_REFRESH = "GSTIN has been successfully re-authenticated by GSTN";
	
	String PAN_VALIDATE = "PAN %s had been successfully validated";
	
	String GSTIN_VALIDATE = "GSTIN %s had been successfully validated";
	
	String RESP_CODE_KEY = "resp_code";
	
	String RESP_MSG_KEY = "resp_msg";
	
	String RESP_AUTH_TOKEN = "auth_token";
	
	String RESP_ERROR = "error";
	
	String RESP_DATA = "data";
	
	String RESP_RET_SUMM = "ret_summ";
	
	String RESP_RET_TRANS = "ret_trans";
	
	String RESP_TIMESTAMP = "timestamp";
	
	String RESP_SUMMARY = "summary";
	
	String RESP_IS_EDIT = "isEdit";
	
	String RESP_OUTWARD = "outward";
	
	String RESP_INWARD = "inward";
	
	String RESP_LIABILITY = "liability";
	
	String RESP_KEYWORDS = "keywords";
	
	String RESP_KEYWORDS_VALUE = "Amend,Error,Modify,Delete,Accept,Reject,Pending,Draft";
	
	String RESP_PRIMARY = "primary";
	
	String RESP_SECONDARY = "secondary";
	
	String DIR_TAXPAYER_DISPLAY_LOGO = "taxpayer/logo/";
	
	String DEFAULT_FAILURE_RESPONSE = "{\"resp_code\":\"" + ExceptionCode._FAILURE + 
			"\"," + "\"resp_msg\":\""+ ApplicationMetadata.FAILURE_MSG + "\"}";
	
	String INVALID_JSON_REQUEST = "{\"error_cd\":\"" + ExceptionCode._INVALID_JSON + 
			"\"," + "\"message\":\""+ ApplicationMetadata.INVALID_JSON + "\"}";
	
	String GSTR_RETURN_FILED = "Your %s has been filed successfully, Reference number for the same is %s";
	
	String GSTR_RETURN_SUBMIT = "Your %s has been submitted successfully, Reference number for the same is %s";
	
	String GSTR_GENERATE = "Your %s has been filed successfully, Reference number for the same is %s";
	
	String SUCCESS_DELETE="Records has been deleted successfully.";
	
	String SUCCESS_EMAIL="E-mail has been successfully sent";
	
	String SYNC_WITH_GSTN_TIMINGS="sync_with_gstn_timings";
	
	String SYNC_TIME_NOT_FOUND="Not Available";
	
	String ITC_AMOUNTS="itc_amounts";
	
	String RETURN_SAVE_IS_NOT_ALLOWED="Return save to GSTN in trial mode";
	
	String RETURN_SAVE_IS_NOT_ALLOWED_FOR_GSTIN="Return save to GSTN is not allowed for this GSTIN";
	
	String SAVE_TO_GSTN_IS_IN_PROGRESS="Save to GSTN  is in progress please check the sync status for more information";
	
	String PREFERENCE_SAVED = "GSTIN <b>%s</b> </br> You have successfully selected %s Filing Cycle for the FY %s .";
	
	String HSN_CODE_MISSING_IN_SOME_ITEMS = "Hsn Code Missing From Some Items please rectify and sync again";

	String NOTIFICATION_DEACTIVATED="Notification successfully removed";
	
	String NOTIFICATION_SEEN="Notification Status changed to seen";
	
	String INVALID_DATE_FORMAT="Please provide date format dd-mm-yyyy";

     
	public enum TAX_TYPE{
		IGST,
		SGST,
		CGST,
		CESS
	}
	
	
	public enum AccessType{
		VIEW,
		SAVE,
		SUBMIT,
		MODIFY,
		FILE,
	}
	

	

	

	
	
}
