package com.ginni.easemygst.portal.constant;

public interface ExceptionCode {

	// Codes
	String _SUCCESS = "1000"; //Success
	String _FAILURE = "1001"; //Failure
	
	String _ERROR = "2000"; //error condition
	String _INSUFFICIENT_PARAMETERS = "2001"; //Insufficient parameters
	String _INVALID_PARAMETER = "2002"; //Invalid parameter submitted
	String _INVALID_OTP = "2003"; //Invalid parameter submitted
	String _INVALID_JSON = "2004"; //Invalid parameter submitted
	String _INVALID_CRDENTIALS = "2005"; //invalid credentials
	String _INVALID_GSTIN = "2006"; //error condition
	String _INVALID_ACCESS = "2007";
	String _INVALID_NULL_ACCESS_RIGHT = "2008";
	String _INVALID_GSTIN_UNIT = "2009"; //Invalid parameter submitted
	String _INVALID_INVOICE_DATE = "2010";
	String _INVALID_PASSWORD = "2011"; //error condition
	String INVALID_TURNOVER="2012";
	String _GSTN_AUTH_FAILED="2013";
	String _INVALID_SIGN_DATA="2014";
	String _INVALID_MONTHYEAR="2015";
	
	String _DUPLICATE = "3000"; //error condition
	String _DUPLICATE_USER = "3001"; //duplicate user condition
	String _DUPLICATE_PARTNER = "3002"; //duplicate user condition
	String _DUPLICATE_PAN_ACCESS = "3003"; //duplicate user condition
	String _DUPLICATE_GSTIN_ACCESS = "3004"; //duplicate user condition
	String _SELF_ACCESS = "3005"; //duplicate user condition
	String _DUPLICATE_ITEM = "3006"; //error condition
	String _DUPLICATE_INVOICE = "3007"; //error condition
	String _DUPLICATE_ORGANISATION = "3008"; //error condition
	String _DUPLICATE_GSTIN = "3009"; //error condition
	
	String _NOT_OWNER = "9000"; //not an owner condition
	String _USER_NOT_FOUND = "9001";
	String _PASSWORD_NOT_MATCH = "9002";
	
	// Object related Codes
	String _NULL_OBJECT_FOUND_INVALID_REQUEST = "4000";
	String _NO_PREFERNCE_SELECTED="4010";
	String _GSTIN_NOT_REGISTERED="4011";
	String _INVALID_GSTIN_NUMBER = "4001";
	String _ORGANISATION_NOT_FOUND = "4002";
	String _NOTSYNCED="4003";
	String _ALREADY_SUBMITED="4004";
	String _ALREADY_FILED_CUSTOM_MSG="40031";

    String _NO_HSN_AND_DOC_DATA="4005";
    String _NO_SUBMIT_INVOICES="4006";
    String _INVALID_ITC="4007";
    String _GET_ALREADY_PROCESSED="4008";
    String _PREVIOUS_GET_IN_PROCESS="4009";

	String _UNAUTHORISED_GSTIN = "400001";

	
	String _UNAUTHORISED_GSTIN_ACCESS = "6000";
	String _SUBSCRIPTION_EXPIRED = "6001";
	String _RETURN_ALREADY_SUBMITTED = "6003";
	
	// Object related Codes
	String _ERP_NOT_CONFIGURED = "8000";
	
	String _SUBSCRIPTION_QUANTITY_REQUIRED = "7000";
	String _NOT_ALLOWED_IN_TRIAL = "7001"; //not an owner condition
	
	// Object related Codes
	String _ERP_NOT_SUPPORTED = "8001";
	
	
}
