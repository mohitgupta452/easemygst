package com.ginni.easemygst.portal.constant;

public interface EwayBillConstant {
	
	 String _SEK="ewb-enc-sek-%s";
	
	 String _APP_KEY="ewb-app-key-%s";
	
	 String _AUTHTOKEN="ewb-authtoken-%s";
	 
	 String _STATUS_FAILURE="0";
	 
	 String _STATUS_SUCCESS="1";
	 
	 String _PARTIAL_FAILURE_MEASSAGE="";
	 
	 String _PARTIAL_FAILURE_CODE="";
	 
}
