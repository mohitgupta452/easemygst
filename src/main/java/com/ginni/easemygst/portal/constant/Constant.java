package com.ginni.easemygst.portal.constant;

public interface Constant {

	public enum TaxpayerType {

		PRIMARY("PRIMARY"), SECONDARY("SECONDARY");

		TaxpayerType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	};
	
	public enum GstReturnFrequency {

		MONTHLY("MONTHLY"), QUARTERLY("QUARTERLY"), ANNUALLY("ANNUALLY");

		GstReturnFrequency(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	};
	
	
	public enum Returnpreference {

		M("Monthly"), Q("Quarterly");

		Returnpreference (String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	};
	
	
	
	public enum TaxpayerGstinType {

		VIEW("VIEW"), SAVE("SAVE"), SUBMIT("SUBMIT"), FILE("FILE");

		TaxpayerGstinType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	};
	
	public enum TaxpayerStatus {

		ACTIVE("ACTIVE"), INACTIVE("INACTIVE"), AUTH("AUTH"), UNAUTH("UNAUTH");

		TaxpayerStatus(String status) {
			this.status = status;
		}

		private String status;

		@Override
		public String toString() {
			return this.status;
		};
	};
	
	public enum Status {

		ACTIVE("ACTIVE"), INACTIVE("INACTIVE");

		Status(String status) {
			this.status = status;
		}

		private String status;

		@Override
		public String toString() {
			return this.status;
		};
	};
	
	public enum ViewType1 {

		SUPPLIER("SUPPLIER"), BUYER("BUYER");

		ViewType1(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	};
	
}
