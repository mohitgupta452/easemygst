package com.ginni.easemygst.portal.constant;

public interface GstMetadata {
	
	// Header Key
	
	String HEADER_KEY_CONTENT_TYPE = "Content-Type";
	
	String HEADER_KEY_CLIENT_ID = "clientid";
	
	String HEADER_KEY_AUTH_TOKEN = "auth-token";
	
	String HEADER_KEY_APP_KEY = "app_key";
	
	String HEADER_KEY_USERNAME = "username";
	
	String HEADER_KEY_STATE_CD = "state-cd";
	
	String HEADER_KEY_TXN = "txn";
	
	String HEADER_KEY_REQUEST_ID = "RequestId";
	
	String HEADER_KEY_CLIENT_SECRET = "client-secret";
	
	String HEADER_KEY_IP_USR = "ip-usr";
	
	String HEADER_KEY_GSTIN = "gstin";
	
	String HEADER_KEY_RET_PRD = "ret_period";
	
	
	// Header Values
	
	String HEADER_CONTENT_TYPE = "application/json";
	
	String HEADER_CLIENT_ID = "l7xxab02b257435547aeaa00f23eea753552";//"l7xxdf2b47b7d728426699a05c8d1ec33a60";
	
	String HEADER_CLIENT_SECRET = "4a6a5885b1a64297b9b6d422b1af5e87";//"30a28162eb024f6e859a12bbb9c31725";
	
	
	
	
	// SPICE GSP Header Keys
	
	String HEADER_KEY_ASP_ID = "Asp-Id";//"l7xxdf2b47b7d728426699a05c8d1ec33a60";
	
	String HEADER_KEY_ASP_SECRET = "Asp-Secret";//"30a28162eb024f6e859a12bbb9c31725";
	
	// EXCELLON GSP Header Keys
	
	String HEADER_KEY_GSP_AUTH_TOKEN = "AuthenticationToken";//"l7xxdf2b47b7d728426699a05c8d1ec33a60";
	
	String HEADER_KEY_GSP_REQUEST_ID = "RequestId";//"30a28162eb024f6e859a12bbb9c31725";
	
	String HEADER_KEY_GSP_ID = "ExactSubsciptionId";//"30a28162eb024f6e859a12bbb9c31725";
	
	
	// SPICE GSP Header Values
	
	String HEADER_ASP_ID = "demo";//"l7xxdf2b47b7d728426699a05c8d1ec33a60";
	
	String HEADER_ASP_SECRET = "demo";//"30a28162eb024f6e859a12bbb9c31725";
	
	// EXCELLON GSP Header Values
	
	String HEADER_VALUE_GSP_ID = "FEAF8A0E-497C-4BE8-81E8-ADEC4EA221DE";
	
	
	// API Url
	
	String ENDPOINT_GSTN_DEV = "http://devapi.gstsystem.co.in";
	
	String ENDPOINT_GSP_URL = "https://api.spicegsp.com:8443";
	
	String ENDPOINT_EXCELLON_DEV = "http://demoapi.exactgst.com";
	
	
	// Success & Failure Status Code
	
	String STATUS_CODE_1 = "1";
	
	String STATUS_CODE_0 = "0";
	
	
	public enum ReturnsModule {
		
		MAIN(""), 
		GSTR1("gstr1"), 
		GSTR1A("gstr1a"),
		GSTR2("gstr2"), 
		GSTR3("gstr3"),
		GSTR6("gstr6"),
		GSTR2A("gstr2a"),
		GSTR3B("gstr3b"),
		Ledgers("ledgers")
		;
		
		ReturnsModule(String module) {
			this.module = module;
		}

		private String module;

		@Override
		public String toString() {
			return this.module;
		};
	};
	
	
	// API Function
	String FUNCTION_AUTHENTICATION = "/taxpayerapi/[VERSION]/authenticate";
	
	public enum Authentication {

		OTPREQUEST("OTPREQUEST_POST_v0.2"), 
		AUTHTOKEN("AUTHTOKEN_POST_v0.2"), 
		REFRESHTOKEN("REFRESHTOKEN_POST_v0.2");

		Authentication(String function) {
			this.function = function;
		}

		private String function;

		@Override
		public String toString() {
			return this.function;
		};
	};
	
	String FUNCTION_RETURNS = "/taxpayerapi/[VERSION]/returns/";
	
	
	String FUNCTION_RETURNS_3B = "/taxpayerapi/[VERSION]/returns";

	
	
	public enum Returns {
		
		RETSAVE("RETSAVE_PUT_v1.1"), 
		RETSAVE3B("RETSAVE_PUT_v0.3"),
		B2B("B2B_GET_v0.3"), 
		B2BA("B2BA_GET_v0.3"), 
		B2CL("B2CL_GET_v0.3"), 
		B2CLA("B2CLA_GET_v0.3"), 
		B2CS("B2CSA_GET_v0.3"), 
		CDN("CDN_GET_v0.3"), 
		CDNA("CDNA_GET_v0.3"), 
		NIL("NIL_GET_v0.3"), 
		EXP("EXP_GET_v0.3"), 
		EXPA("EXPA_GET_v0.3"), 
		AT("AT_GET_v0.3"), 
		ATA("ATA_GET_v0.3"), 
		TXP("TXP_GET_v0.3"), 
		HSNSUM("HSNSUM_GET_v0.3"), 
		RETSUM("RETSUM_GET_v0.3"), 
		RETSUBMIT("RETSUBMIT_POST_v0.3"), 
		IMPG("IMPG_GET_v0.3"), 
		IMPGA("IMPGA_GET_v0.3"), 
		IMPS("IMPS_GET_v0.3"),
		IMPSA("IMPSA_GET_v0.3"), 
		ISD("ISD_GET_v0.3"), 
		TDS("TDS_GET_v0.3"), 
		TCS("TCS_GET_v0.3"), 
		ITC("ITC_GET_v0.3"),
		TXLI("TXLI_GET_v0.3"), 
		ATXLI("ATXLI_GET_v0.3"), 
		TXPD("TXPD_GET_v0.3"), 
		ITCRVSL("ITCRVSL_GET_v0.3"),
		RETDET("RETDET_GET_v0.3"), 
		GENERATE("GENERATE_GET_v0.3"), 
		ARNTRACK("ARNTRACK_GET_v0.3"), 
		RETTRACK("RETTRACK_GET_v0.3"), 
		ATTRACK("ATTRACK_GET_v0.3"), 
		PITCTRACK("PITCTRACK_GET_v0.3"),
		TDSTRACK("TDSTRACK_GET_v0.3"), 
		RETSTATUS("RETSTATUS_GET_v0.3"),
		RETFILE("RETFILE_POST_v0.3"),
		GETCHUNKDATA("_GET_v0.3"),
		RETOFFSET("RETOFFSET_POST_v0.3"),
		FILEDET("FILEDET_GET_v0.3"),
		FILE_GET_DOWNLOAD("_GET_v0.3"),
		SEARCH_TP("TP_GET_v0.2");

		Returns(String function) {
			this.function = function;
		}

		private String function;

		@Override
		public String toString() {
			return this.function;
		};
	};
	
	String FUNCTION_LEDGERS = "/taxpayerapi/[VERSION]/ledgers";
	
	public enum Ledgers {
		
		CASHDTL("CASHDTL_GET_v0.3"), 
		ITCDTL("ITCDTL_GET_v0.3"), 
		TAX("TAX_GET_v0.3"), 
		UTLCSH("UTLCSH_POST_v0.3"), 
		UTLITC("UTLITC_POST_v0.3"), 
		CASHSUM("CASHSUM_GET_v0.3"), 
		ITCSUM("ITCSUM_GET_v0.3");

		Ledgers(String function) {
			this.function = function;
		}

		private String function;

		@Override
		public String toString() {
			return this.function;
		};
	};
	
	String FUNCTION__FILES="/api/Files/FileDownload?endpoint=";
	
	String SEARCH_TAXPAYER="/commonapi/[VERSION]/search";
	
}