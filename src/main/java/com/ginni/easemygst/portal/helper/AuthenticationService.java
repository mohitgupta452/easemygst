package com.ginni.easemygst.portal.helper;

import java.io.Serializable;
import java.lang.invoke.MethodHandles;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.security.TokenService;

@Named("authService")
@RequestScoped
public class AuthenticationService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();
	
	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	@Inject
	private UserBean userBean;
	
	@Inject 
	private TokenService tokenService;
	
	public boolean validateTokenKey(String token, String key) {
		if(tokenService.isValidToken(token, key)) { 
			return true;
		}
		return false;
	}
	
	public boolean validateToken(String token, String ipAddress) {
		userBean.setIpAddress(ipAddress);
		if(tokenService.isValidSignature(token, ipAddress)) { 
			getUserFromToken(token);
			return true;
		}
		return false;
	}
	
	private void getUserFromToken(String token) {
		userBean.setUserFromRedis(token);
		
		
	}
	
}
