package com.ginni.easemygst.portal.helper;

import org.jboss.resteasy.spi.LoggableFailure;

public class InsufficentRightException  extends LoggableFailure {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficentRightException()
	   {
	      super(403);
	   }

	   public InsufficentRightException(String s)
	   {
	      super(s, 403);
	   }


	
	
}
