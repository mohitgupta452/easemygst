package com.ginni.easemygst.portal.helper;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

public @Data class EwayBillReportModel {

	private String billNo;

	private String generatedDate;

	private String generatedBy;

	private String validUpto;

	private String distance;

	private String type;

	private String documentDetail;

	private String addressfrom;

	private String addressto;

	private double taxAmount;

	private double cgstAmount;

	private double sgstAmount;

	private double igstAmount;

	private double cessAmount;

	private String trsnsportId;

	private String trsnsportDate;
	
	private String mode;
	
	private String barCode;
	
	private String qrCode;

	@JsonIgnore
	private List<ItemsDetails> itemsDetails = new ArrayList<>();

	@JsonIgnore
	private List<VehicleDetais> vehicleDetais = new ArrayList<>();

	public @Data static class ItemsDetails {

		private String hsnCode;

		private String product;

		private String quantity;

		private double totalAmount;

		private String taxRate;

	}

	public @Data static class VehicleDetais {

		private String vehicleNo;

		private String enterDate;

		private String mode;

		private String cewbNo;

		private String enterBy;

		private String city;

	}

}
