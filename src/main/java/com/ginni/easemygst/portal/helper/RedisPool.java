package com.ginni.easemygst.portal.helper;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisPool {

	public final static JedisPool _JEDISPOOL;
	
	private final static String host = "127.0.0.1";
	private final static int port = 6379;
	
	static {
			JedisPoolConfig poolConfig = new JedisPoolConfig();
			int maxConnections = 100;
			poolConfig.setMaxTotal(maxConnections);
			poolConfig.setMaxIdle(50);
			poolConfig.setMinIdle(20);
			poolConfig.setBlockWhenExhausted(true);
			
		_JEDISPOOL=new JedisPool(poolConfig,host,port);
		
		
	}

	
}
