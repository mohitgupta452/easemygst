package com.ginni.easemygst.portal.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.database.DBUtils;

/**
 * Servlet implementation class AcitvateUserEmail
 */
@WebServlet("/Activate")
public class ActivateUserEmail extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Inject
	private UserService userService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActivateUserEmail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		log.debug("entering activation user doGet");
		
		String reqid = request.getParameter("reqid");
		
		Properties config = new Properties();
		try {
			config.load(DBUtils.class.getClassLoader().getResourceAsStream("config.properties"));
			log.info("config.properties loaded successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String FAILURE_URL = config.getProperty("main.web.url.email.verified.failure");
		String SUCCESS_URL = config.getProperty("main.web.url.email.verified.success");
		
		if(reqid!=null) {
			String part = reqid.substring(reqid.indexOf("Sm")+2, reqid.length());
			int i = Integer.parseInt(part);
			
			UserDTO userDTO = new UserDTO();
			userDTO.setUsername(null);
			userDTO.setId(i);
			
			log.debug("validating user {} for easemygst", i);
			if (userService.isActiveIdentityExists(userDTO.getId())) {
				log.debug("user {} found successfully", i);
				userDTO = userService.activateUserEmail(i);
			}

			// if username is null
			if (userDTO.getUsername()==null) {
				log.error("Unable to process request");
				response.sendRedirect(FAILURE_URL);
				return;
			}

			log.debug("leaving activation user doGet");
			response.sendRedirect(SUCCESS_URL);
			return;
			
		} else {
			log.error("Unable to process request");
			response.sendRedirect(FAILURE_URL);
			return;
		} 
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
