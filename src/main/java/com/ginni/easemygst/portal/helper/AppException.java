package com.ginni.easemygst.portal.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.gst.response.Error;

public class AppException extends Exception {

	private static final long serialVersionUID = 1L;

	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	private static Properties expconfig = null;

	private static String DEFAULT_MESSAGE = "Unable to process at this time. Please try again later";

	String code="1001";

	String message;

	static {
		try {
			log.info("Static block starts of AppException");
			loadExceptionConfigProperties();
			log.info("Static block ends of AppException");
		} catch (IOException e) {
			log.error("Unable to load exception.properties.");
		}
	}

	public AppException() {

	}

	public AppException(String code) {
		super(code);
		this.code = code;
	}

	public AppException(Throwable cause) {
		super(cause);
	}
	
	public AppException(String code,String message)
	{
		this.code=code;
		this.message=message;
	}

	public AppException(String code, Throwable cause) {
		super(code, cause);
	}

	private static void loadExceptionConfigProperties() throws IOException {
		expconfig = new Properties();
		expconfig.load(AppException.class.getClassLoader().getResourceAsStream("exception.properties"));
		log.info("exception.properties loaded successfully");
	}

	public static String getMessage(String code) {
		return code != null ? expconfig.getProperty(code) : DEFAULT_MESSAGE;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static Object getMessageJson(String code) {
		String desc = code != null ? expconfig.getProperty(code) : DEFAULT_MESSAGE;
		code = code != null ? code : "0";

		Error error = new Error();
		error.setCode(code);
		error.setMessage(desc);

		return error;
	}
	
	public Object getMessageJsonFromObject(String code) {
		String desc = this.message != null ? this.message : code != null ? expconfig.getProperty(code) : DEFAULT_MESSAGE;
		code = code != null ? code : "2000";

		Error error = new Error();
		error.setCode(code);
		error.setMessage(desc);

		return error;
	}
	
	public Object getMessageJsonFromObject() {
		String desc = this.message != null ? this.message :DEFAULT_MESSAGE;
		code = code != null ? code : "1001";

		Error error = new Error();
		error.setCode(code);
		error.setMessage(desc);

		return error;
	}
	
}
