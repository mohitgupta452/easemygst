package com.ginni.easemygst.portal.helper;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodHandles;
import java.security.Key;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.TinDTO;
import com.ginni.easemygst.portal.business.dto.TaxSummaryDTO;
import com.ginni.easemygst.portal.business.dto.UniversalReturnDTO;
import com.ginni.easemygst.portal.business.entity.BrtMappingDTO;
import com.ginni.easemygst.portal.business.entity.PartnerDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.GstinSubscriptionDuration;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import redis.clients.jedis.Jedis;

@Named("redisService")
@RequestScoped
public class RedisService implements Serializable {
	
	private static final String _CURRENT_PROFILE=AppConfig.getActiveProfile();

	private static final long serialVersionUID = 1L;

	protected final static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	protected final static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	private final static String CHARACTER_ENCODING = "UTF-8";
	
	@PostConstruct
	public void init() {
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
		Calendar cal = Calendar.getInstance();
		 		cal.set(2017,6, 1);
		this.setBytes(ApplicationMetadata.GST_START_DATE, SerializationUtils.serialize(cal.getTime()), false, 0);
		}
	}
	

	public void setUser(String key, UserDTO userDTO, boolean expire, int seconds) {

		key=_CURRENT_PROFILE+key;
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize(userDTO));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", e.getMessage());
		}
	}

	public UserDTO getUser(String key) {

		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public void setPartner(String key, PartnerDTO partnerDTO, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {

			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize(partnerDTO));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public PartnerDTO getPartner(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public void setTaxpayerGstin(String key, TaxpayerGstin taxpayerGstin, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {

			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize(taxpayerGstin));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}



	public void setReport(String key, Object object, boolean expire, int seconds) {

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {

			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize((Serializable) object));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}
	
	
	public  <T> T getReport(String key,T t) {
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public TaxpayerGstin getTaxpayerGstin(String key) {

		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}
	
	public void setTaxSummary(String key,TaxSummaryDTO summary, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {

			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize(summary));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public TaxSummaryDTO getTaxSummary(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}
	
	
	public void setUserallTin(String key, List<TinDTO> usertinlist, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {

			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize((Serializable)usertinlist));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}
	
	
	
	public List<TinDTO> getUserAlltin(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}
	
	
	
	

	public void setUserOrgTaxpayer(String key, List<TaxpayerDTO> taxpayerDTOs, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {

			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize((Serializable) taxpayerDTOs));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public void removeUserOrgTaxpayer(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.expire(key.getBytes(), 0);
		}
	}

	public List<TaxpayerDTO> getUserOrgTaxpayer(String key) {
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public Map<String, Object> getMapStringObject(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public void setMapStringObject(String key, Map<String, Object> map, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize((Serializable) map));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public SortedSet<UniversalReturnDTO> getSSRT(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public void setSSRT(String key, SortedSet<UniversalReturnDTO> map, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize((Serializable) map));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public void setBrtMapping(String key, List<BrtMappingDTO> brtMappings, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize((Serializable) brtMappings));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public List<BrtMappingDTO> getBrtMapping(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public void setSecKey(String key, Key keygen, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize(keygen));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public Key getSecKey(String key) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				return SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return null;
	}

	public void setValue(String key, String value, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key, value);
			if (expire)
				jedis.expire(key, seconds);
		}
	}

	public String getValue(String key) {
		
		key=_CURRENT_PROFILE+key;

		String value = null;
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key)) {
				value = jedis.get(key);
			}
		}
		return value;

	}

	public void setBytes(String key, byte[] value, boolean expire, int seconds) {
		
		key=_CURRENT_PROFILE+key;


		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), value);
			if (expire)
				jedis.expire(key, seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public byte[] getBytes(String key) {
		
		key=_CURRENT_PROFILE+key;

		byte[] bytes = null;
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			;
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				bytes = jedis.get(key.getBytes());
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return bytes;
	}

	public void setExpiry(String key, int seconds) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key)) {
				jedis.expire(key, seconds);
			} else if (jedis.exists(key.getBytes())) {
				jedis.expire(key.getBytes(CHARACTER_ENCODING), seconds);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public void setEstimatedTimeForGetFile(int minutes, String token, String gstin, String monthYear,
			String returnType) {
		
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set("tokengetFile", "b8198810a2824e848f6cb7eb4be9a370");
			jedis.set("monthYear", monthYear);
			jedis.set("returnType", returnType);
			jedis.set("gstin", gstin);
		}
	}

	public String getToken(String key) {
		
		key=_CURRENT_PROFILE+key;

		String result = null;
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key))
				result = jedis.get(key);
		}
		return result;
	}

	public void setKeyValue(String key, String value) {
		
		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key, value);
		}

	}

	public void setSubscriptionDurationForGstin(String key, GstinSubscriptionDuration subscription, boolean expire,
			int seconds) {

		key=_CURRENT_PROFILE+key;

		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			jedis.set(key.getBytes(CHARACTER_ENCODING), SerializationUtils.serialize(subscription));
			if (expire)
				jedis.expire(key.getBytes(), seconds);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
	}

	public boolean isKeyExist(String key) {
		
		key=_CURRENT_PROFILE+key;

		boolean isKeyExist = false;
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource()) {
			isKeyExist = jedis.exists(key);
		}
		return isKeyExist;
	}

	public GstinSubscriptionDuration getGstinSubscritionDurationByGstinMonthYear(String key) {
		
		key=_CURRENT_PROFILE+key;

		GstinSubscriptionDuration gstinSubscriptionDuration = null;
		try (Jedis jedis = RedisPool._JEDISPOOL.getResource();) {
			if (jedis.exists(key.getBytes(CHARACTER_ENCODING))) {
				byte[] value = jedis.get(key.getBytes());
				gstinSubscriptionDuration = SerializationUtils.deserialize(value);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException in jedis {}", key);
		}
		return gstinSubscriptionDuration;
	}

}
