package com.ginni.easemygst.portal.helper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.transaction.crud.ConsolidatedService;
import com.ginni.easemygst.portal.transaction.crud.ConsolidatedService.DATA_SEPARATOR;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.utils.AzzureFIleUpload;
import com.ginni.easemygst.portal.utils.AzzureFIleUpload.AZZURE_DIRECTORY;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.ExcelTemplate;
import com.ginni.easemygst.portal.utils.ManipulateFile;
import com.ginni.easemygst.portal.utils.Utility;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class ExternalServiceExecutor {

	static Logger LOGGER = LoggerFactory.getLogger(ExternalServiceExecutor.class);

	String updateQueryDataUpload = "UPDATE `data_upload_info` set status=? WHERE id = ?";

	String updateQueryDataUploadUrl = "UPDATE `data_upload_info` set fileName=?,updationTime=CURRENT_TIMESTAMP WHERE id = ?";

	String selectSubmitReturn = "SELECT id, actionDate from `gstin_action` WHERE gstin = ? and particular = ? and returnType = ? and status = 'SUBMIT'";

	// @PostPersist
	public void myPostConstruct(Object obj) throws IOException {

		ExecutorService executorService = Executors.newSingleThreadExecutor();

		executorService.execute(new Runnable() {

			@Override
			public void run() {
				csvProcess(obj);
				LOGGER.debug("end of csv executor thread");
			}
		});

		LOGGER.debug("return from executor thread..");
	}

	@SuppressWarnings("unchecked")
	private void csvProcess(Object obj) {

		try {
			if (obj instanceof List) {
				List<DataUploadInfo> dataUpdaloadInfos = (List<DataUploadInfo>) obj;

				LOGGER.debug("executing data info started...");
				dataUpdaloadInfos.forEach(info -> {

					if ("GSTR".equalsIgnoreCase(info.getReturnType())) {
						this.csvProcessCosolidated(info);
					} else {
						Connection connection = null;
						LOGGER.info("processing data upload info for id {}" + info.getId());
						TransactionFactory transactionFactory = new TransactionFactory(info.getTransaction());
						TaxpayerGstin taxpayerGstin = null;

						String headerIndex = null;
						boolean submitCheck = true;

						if (!Objects.isNull(info.getTaxpayerGstin()) && info.getMatched() == 1) {
							taxpayerGstin = info.getTaxpayerGstin();

							/*
							 * PreparedStatement subStatement = null; try { connection =
							 * DBUtils.getConnection(); subStatement =
							 * connection.prepareStatement(selectSubmitReturn); subStatement.setInt(1,
							 * taxpayerGstin.getId()); subStatement.setString(2, info.getMonthYear());
							 * subStatement.setString(3, info.getReturnType()); ResultSet rs =
							 * subStatement.executeQuery();
							 * 
							 * if (rs.next()) { storeErpLogs(info, true,
							 * "Invalid! Return has been already submitted for the requested month year",
							 * connection, null); } else { submitCheck = true; }
							 * 
							 * } catch (Exception e) { e.printStackTrace(); } finally { try { if
							 * (subStatement != null && !subStatement.isClosed())
							 * DBUtils.close(subStatement); if (connection != null &&
							 * !connection.isClosed()) DBUtils.close(connection); } catch (Exception e) {
							 * e.printStackTrace();
							 * LOGGER.error("SQL exception while updating status of data upload", e); } }
							 */

							if (submitCheck) {

								if (info.getStatus().equalsIgnoreCase("STARTED")) {
									URL url = null;
									try {
										url = new URL(info.getUrl());
									} catch (MalformedURLException e1) {
										e1.printStackTrace();
									}
									Scanner scanner = null;
									StringBuilder sb = new StringBuilder();
									boolean checkHeaders = true;
									try {
										String key = info.getTransaction().toLowerCase();
										Map<String, String> headers = ExcelTemplate
												.getExcelSheetTemplateHeaders(info.getReturnType().toLowerCase(), key);
										sb.append(headers.get(key));
										sb.append("\n");
										scanner = new Scanner(url.openStream());
										while (scanner.hasNext()) {
											String line = scanner.nextLine();
											if (checkHeaders) {
												if (key.equalsIgnoreCase("b2cs")) {
													headerIndex = "0,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17";
												}
												if (key.equalsIgnoreCase("b2cs") && line.replace(",", "-")
														.replace("|", ",").split(",").length == 16) {
													sb = new StringBuilder();
													key = key + "_ginesys";
													Map<String, String> newheaders = ExcelTemplate
															.getExcelSheetTemplateHeaders(
																	info.getReturnType().toLowerCase(), key);
													sb.append(newheaders.get(key));
													sb.append("\n");
												}
												checkHeaders = false;
											}
											sb.append(line.replace(",", "-").replace("|", ","));
											sb.append("\n");
										}
										scanner.close();

										InputStream in = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
										String dataFileUrl = uploadDataFile(in,
												"Data_" + info.getFileName() + "_"
														+ Calendar.getInstance().getTimeInMillis() + ".csv",
												"datareport", info.getGstin());
										PreparedStatement dataStatement = null;
										try {
											connection = DBUtils.getConnection();
											dataStatement = connection.prepareStatement(updateQueryDataUploadUrl);
											dataFileUrl = info.getFileName() + "#" + dataFileUrl;
											dataStatement.setString(1, dataFileUrl);
											dataStatement.setInt(2, info.getId());
											dataStatement.executeUpdate();

										} finally {
											if (dataStatement != null && !dataStatement.isClosed())
												DBUtils.close(dataStatement);
											if (connection != null && !connection.isClosed())
												DBUtils.close(connection);

										}
									} catch (Exception e1) {
										e1.printStackTrace();
									}
								}

								URL url = null;
								try {
									url = new URL(info.getUrl());
								} catch (MalformedURLException e1) {
									e1.printStackTrace();
								}
								Scanner scanner = null;
								try {
									scanner = new Scanner(url.openStream());
								} catch (Exception e1) {
									e1.printStackTrace();
								}

								PreparedStatement dataStatement = null;
								try {
									connection = DBUtils.getConnection();
									dataStatement = connection.prepareStatement(updateQueryDataUpload);

									try {

										String temp = info.getUrl()
												.substring(info.getUrl().indexOf("mycontainer/") + 12);
										temp = temp.substring(0, temp.indexOf("/"));

										SourceType source = SourceType.ERP;

										if (!Objects.isNull(temp)
												&& !Objects.isNull(SourceType.valueOf(temp.toUpperCase()))) {
											source = SourceType.valueOf(temp.toUpperCase());
										}

										String delete = null;
										if (!info.getTransaction().equals("ALL")) {
											delete = taxpayerGstin.isFlushData() ? "yes" : "no";
										}

										int count = transactionFactory.processCsvData(scanner, info.getReturnType(),
												headerIndex, taxpayerGstin,
												info.getGstin() + "-" + info.getReturnType() + "-"
														+ info.getTransaction(),
												Utility.getValidReturnPeriod(info.getMonthYear(), taxpayerGstin
														.getFilePreferenceByFinancialYear(CalanderCalculatorUtility
																.getFiscalYearByMonthYear(Utility
																		.convertStrToYearMonth(info.getMonthYear())))),
												null, delete, source);
										LOGGER.debug("Transaction {} successfull processed with {} records",
												info.getTransaction(), count);

										dataStatement.setString(1, "PROCESSED");
										storeErpLogs(info, false, "File data processed successfully, " + count
												+ " line items updated successfully", null);
									} catch (AppException e) {
										LOGGER.error("app exception while process {} for {}", info.getTransaction(),
												info.getFileName(), e);
										String errors = e.getMessage();
										if (!StringUtils.isEmpty(errors) && errors.contains("|")) {
											errors = errors.replace("|", "\n");
											errors = "FILE,COLUMN,ROW,DESCRIPTION\n" + errors;
											InputStream in = new ByteArrayInputStream(errors.getBytes("UTF-8"));
											String errorFileUrl = uploadErrorReport(in,
													"ERROR_" + info.getFileName() + "_"
															+ Calendar.getInstance().getTimeInMillis() + ".csv",
													"errorreport", info.getGstin());
											storeErpLogs(info, true,
													"Unable to process file data, please check the logs for more information",
													errorFileUrl);
										}
										dataStatement.setString(1, "PROCESSED_WITH_ERROR");
									} catch (Exception e) {
										LOGGER.error("exception while process {} for {}", info.getTransaction(),
												info.getFileName(), e);
										try {
											dataStatement.setString(1, "PROCESSED_WITH_ERROR");
											storeErpLogs(info, true,
													"Unable to process file data, received file is not in proper format",
													null);
										} catch (Exception e1) {
											LOGGER.error("SQL exception while storing failure logs {}",
													e1.getMessage());
										}
									}
									dataStatement.setInt(2, info.getId());
									dataStatement.executeUpdate();
								} catch (Exception e2) {
									LOGGER.error("SQL exception while updating status of data upload {}", e2);
									e2.printStackTrace();
								} finally {
									try {
										if (dataStatement != null && !dataStatement.isClosed())
											DBUtils.close(dataStatement);
										if (connection != null && !connection.isClosed())
											DBUtils.close(connection);
									} catch (Exception e) {
										e.printStackTrace();
										LOGGER.error("SQL exception while updating status of data upload", e);
									}
								}
							}
						}
						LOGGER.info("processing completed for data upload info for id {}" + info.getId());
					}
				});
				LOGGER.debug("executing data info ends...");
			}
		} catch (Exception ex) {

		} finally {

		}
	}

	private void csvProcessCosolidated(DataUploadInfo info) {

		ConsolidatedService consolidatedService = new ConsolidatedService();

		try {

			LOGGER.debug("executing data info started...");

			Connection connection = null;
			LOGGER.info("processing data upload info for id {}" + info.getId());
			TaxpayerGstin taxpayerGstin = null;

			String headerIndex = null;
			boolean submitCheck = true;
			String localCsvUrl = null;

			if (!Objects.isNull(info.getTaxpayerGstin()) && info.getMatched() == 1) {
				taxpayerGstin = info.getTaxpayerGstin();

				if (submitCheck) {

					boolean process = false;

					if ("STARTED".equalsIgnoreCase(info.getStatus()) || process) {
						URL url = null;
						try {
							url = new URL(info.getUrl());
						} catch (MalformedURLException e1) {
							e1.printStackTrace();
						}

						try {

							String pipedFile = IOUtils.toString(url, "UTF-8");

							pipedFile = pipedFile.replaceAll("(?i)\\|others\\|", "|LOCAL|")
									.replaceAll("(?i)\\|no tax\\|", "|LOCAL|").replaceAll("(?i)\\|locals\\|", "|LOCAL|")
									.replaceAll("(?i)\\|CST\\|", "|INTER STATE|");

							InputStream is1Piped = new ByteArrayInputStream(pipedFile.getBytes("UTF-8"));

							String localPipedCsvUrl = ManipulateFile.uploadCsvFile(is1Piped,
									"piped-csv-uploadinfoid-" + UUID.randomUUID().toString() + info.getId());

							consolidatedService.importExcelToDatabase(localPipedCsvUrl, info.getId(),
									DATA_SEPARATOR.PIPE);

							consolidatedService.callSetReturnTypeProcedure(info.getTaxpayerGstin().getGstin(),
									info.getMonthYear(), String.valueOf(info.getId()));

							LOGGER.debug("replacing piped csv");
							StringBuilder sb = new StringBuilder();
							String key = info.getTransaction().toLowerCase();
							Map<String, String> headers = ExcelTemplate
									.getExcelSheetTemplateHeadersConsolidated(info.getReturnType().toLowerCase(), key);
							sb.append(headers.get(key));
							sb.append("\n");

							sb.append(pipedFile.replace(",", "-").replace("|", ",").replace("\"", ""));

							LOGGER.debug("finish replacing pipe csv");

							String dataFileUrl = uploadDataFile(sb,
									"Data_" + info.getFileName() + "_" + UUID.randomUUID().toString() + ".csv",
									"datareport", info.getGstin());
							PreparedStatement dataStatement = null;
							LOGGER.info("data file url {}", dataFileUrl);
							try {
								connection = DBUtils.getConnection();
								dataStatement = connection.prepareStatement(updateQueryDataUploadUrl);
								dataFileUrl = StringUtils.substringBefore(info.getFileName(), "#") + "#" + dataFileUrl;
								dataStatement.setString(1, dataFileUrl);
								dataStatement.setInt(2, info.getId());
								dataStatement.executeUpdate();

							} catch (SQLException e) {
								LOGGER.error("sql exceptuion while update data file url {}", dataFileUrl, e);
							}

							finally {
								if (dataStatement != null && !dataStatement.isClosed())
									DBUtils.close(dataStatement);
								if (connection != null && !connection.isClosed())
									DBUtils.close(connection);
							}
						} catch (Exception e1) {
							LOGGER.error("exception", e1);
							throw new AppException(e1);
						}
					}
					PreparedStatement dataStatement = null;
					try {

						try {

							String temp = info.getUrl().substring(info.getUrl().indexOf("mycontainer/") + 12);
							temp = temp.substring(0, temp.indexOf("/"));

							SourceType source = SourceType.ERP;

							if (!Objects.isNull(temp) && !Objects.isNull(SourceType.valueOf(temp.toUpperCase()))) {
								source = SourceType.valueOf(temp.toUpperCase());
							}

							String delete = "Yes";

							GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
							gstinTransactionDTO.setTransactionType(TransactionType.ALL.toString());
							gstinTransactionDTO.setMonthYear(info.getMonthYear());
							gstinTransactionDTO.setTaxpayerGstin(
									(TaxpayerGstinDTO) EntityHelper.convert(taxpayerGstin, TaxpayerGstinDTO.class));

							int count = consolidatedService.proecessConsolidateCsv(gstinTransactionDTO,
									String.valueOf(info.getId()), source);

							LOGGER.debug("Transaction {} successfull processed with {} records", info.getTransaction(),
									count);

							connection = DBUtils.getConnection();
							dataStatement = connection.prepareStatement(updateQueryDataUpload);
							dataStatement.setString(1, "PROCESSED");
							storeErpLogs(info, false,
									"File data processed successfully, " + count + " line items updated successfully",
									null);
						} catch (AppException e) {
							LOGGER.error("app exception while process {} for {}", info.getTransaction(),
									info.getFileName(), e);
							String errors = e.getMessage();
							if (!StringUtils.isEmpty(errors) && errors.contains("|")) {
								errors = errors.replace("|", "\n");
								errors = "FILE,COLUMN,ROW,DESCRIPTION\n" + errors;
								InputStream in = new ByteArrayInputStream(errors.getBytes("UTF-8"));
								String errorFileUrl = uploadErrorReport(
										in, "ERROR_" + info.getFileName() + "_"
												+ Calendar.getInstance().getTimeInMillis() + ".csv",
										"errorreport", info.getGstin());

								storeErpLogs(info, true,
										"Unable to process file data, please check the logs for more information",
										errorFileUrl);
							}
							connection = DBUtils.getConnection();
							dataStatement = connection.prepareStatement(updateQueryDataUpload);
							dataStatement.setString(1, "PROCESSED_WITH_ERROR");
						} catch (Exception e) {
							LOGGER.error("exception while process {} for {}", info.getTransaction(), info.getFileName(),
									e);
							try {
								connection = DBUtils.getConnection();
								dataStatement = connection.prepareStatement(updateQueryDataUpload);
								dataStatement.setString(1, "PROCESSED_WITH_ERROR");
								storeErpLogs(info, true,
										"Unable to process file data, received file is not in proper format", null);
							} catch (Exception e1) {
								LOGGER.error("SQL exception while storing failure logs {}", e1.getMessage());
							}
						}
						dataStatement.setInt(2, info.getId());
						dataStatement.executeUpdate();
					} catch (Exception e2) {
						LOGGER.error("SQL exception while updating status of data upload ", e2);
					} finally {
						try {
							if (dataStatement != null && !dataStatement.isClosed())
								DBUtils.close(dataStatement);
							if (connection != null && !connection.isClosed())
								DBUtils.close(connection);
						} catch (Exception e) {
							LOGGER.error("SQL exception while updating status of data upload", e);
						}
					}
				}
			}
			LOGGER.info("processing completed for data upload info for id {}" + info.getId());

			LOGGER.debug("executing data info ends...");
		} catch (Exception ex) {

		} finally {

		}
	}

	private void storeErpLogs(DataUploadInfo info, boolean error, String description, String errorFile)
			throws SQLException {

		String inserQueryErpLogs = " INSERT INTO `erp_logs` (`userId`, `filename`, `gstin`, `monthYear`, `returnType`, `description`,"
				+ " `status`, `isError`, `errorFile`, `transactionType`, `creationTime`) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";

		try (Connection insertConn = DBUtils.getConnection();

				PreparedStatement logsStatement = insertConn.prepareStatement(inserQueryErpLogs);) {
			insertConn.setAutoCommit(false);

			logsStatement.setInt(1, Objects.isNull(info.getUser()) ? null : info.getUser().getId());
			if (info.getFileName().contains("#"))
				logsStatement.setString(2, info.getFileName().split("#")[0]);
			else
				logsStatement.setString(2, info.getFileName());
			logsStatement.setString(3, info.getGstin());
			logsStatement.setString(4, info.getMonthYear());
			logsStatement.setString(5, info.getReturnType());
			logsStatement.setString(6, description);

			if (error) {
				if (!StringUtils.isEmpty(errorFile))
					logsStatement.setString(7, "FAILURE" + "#" + errorFile);
				else
					logsStatement.setString(7, "FAILURE");
				logsStatement.setByte(8, (byte) 1);
			} else {
				logsStatement.setString(7, "SUCCESS");
				logsStatement.setByte(8, (byte) 0);
			}

			logsStatement.setString(9, errorFile);
			logsStatement.setString(10, info.getTransaction());

			logsStatement.executeUpdate();
			insertConn.commit();
			DBUtils.closeQuietly(insertConn);
		}
	}

	private String uploadErrorReport(InputStream ins, String fileName, String folderName, String gstin)
			throws UnirestException, IOException {

		Map<String, Object> request = new HashMap<>();
		request.put("file", ins);
		request.put("folderName", "errorreport");

		String requestJson = JsonMapper.objectMapper.writeValueAsString(request);

		// the file we want to upload
		// String respo =
		// RestClient.callApi("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/errorreport"
		// + "", "POST", new JsonNode(requestJson), new HashMap<>());

		File file = new File(ManipulateFile.getParentDirBasePath() + File.separator + "errorreport" + File.separator
				+ gstin + Calendar.getInstance().getTimeInMillis() + ".csv");
		OutputStream outputStream = null;
		// write the inputStream to a FileOutputStream
		outputStream = new FileOutputStream(file);

		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = ins.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		outputStream.flush();
		outputStream.close();

		LOGGER.info("error file {}", file.getAbsolutePath());

		HttpResponse<String> jsonResponse = Unirest
				.post("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/errorreport")
				.header("accept", "application/json").field("file", file).field("folderName", "errorreport").asString();

		LOGGER.info("response of error file upload on azzure {}  {}", jsonResponse.getBody(),
				jsonResponse.getStatusText());

		if (jsonResponse.getStatus() == 200) {
			return JsonMapper.objectMapper.readTree(jsonResponse.getBody()).get("url").asText();
		}

		return null;

	}

	private String uploadDataFile(StringBuilder data, String fileName, String folderName, String subDirectory)
			throws UnirestException, IOException {

		File file = new File(ManipulateFile.getParentDirBasePath() + File.separator + subDirectory + "_"
				+ UUID.randomUUID().toString() + ".csv");
		// write the inputStream to a FileOutputStream

		FileUtils.writeStringToFile(file, data.toString(), StandardCharsets.UTF_8);

		LOGGER.info("data file {}", file.getAbsolutePath());
		HttpResponse<JsonNode> jsonResponse = Unirest
				.post("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/datafile")
				.header("accept", "application/json").field("file", file).field("folderName", "datafile").asJson();

		LOGGER.info("response of data file upload body  {}    body{} ", jsonResponse.getBody(),
				jsonResponse.getStatus());

		if (jsonResponse.getStatus() == 200) {
			return jsonResponse.getBody().getObject().getString("url");

		}

		return null;

	}
	
/*	private String uploadDataFileToBlob(File file, String fileName, String folderName, String subDirectory)
			throws UnirestException, IOException {
		
		InputStream inputStream = new FileInputStream(file);
		  
		 return AzzureFIleUpload.uploadFileOnAzzureBlob(inputStream, fileName, AZZURE_DIRECTORY.DATAFILE);

	}*/
	
	private String uploadDataFile(File file, String fileName, String folderName, String subDirectory) {

		LOGGER.info("data file {}", file.getAbsolutePath());
		HttpResponse<JsonNode> jsonResponse;
		try {
			jsonResponse = Unirest
					.post("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/datafile")
					.header("accept", "application/json").field("file", file).field("folderName", "datafile").asJson();
			LOGGER.info("response of data file upload body  {}    body{} ", jsonResponse.getBody(),
					jsonResponse.getStatus());

			if (jsonResponse.getStatus() == 200) {
				return jsonResponse.getBody().getObject().getString("url");

			}
		} catch (UnirestException e) {
			
			LOGGER.error("exception while uploading data file ",e);
		}

		

		return null;

	}

	private String uploadDataFile(InputStream ins, String fileName, String folderName, String subDirectory)
			throws UnirestException, IOException {

		// the file we want to upload
		// String respo =
		// RestClient.callApi("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/errorreport"
		// + "", "POST", new JsonNode(requestJson), new HashMap<>());

		File file = new File(ManipulateFile.getParentDirBasePath() + File.separator + subDirectory + "_"
				+ UUID.randomUUID().toString() + ".csv");
		OutputStream outputStream = null;
		// write the inputStream to a FileOutputStream
		outputStream = new FileOutputStream(file);

		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = ins.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		outputStream.flush();
		outputStream.close();

		LOGGER.info("data file {}", file.getAbsolutePath());
		HttpResponse<JsonNode> jsonResponse = Unirest
				.post("http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/datafile")
				.header("accept", "application/json").field("file", file).field("folderName", "datafile").asJson();

		LOGGER.info("response of data file upload body  {}    body{} ", jsonResponse.getBody());
		if (jsonResponse.getStatus() == 200) {
			return jsonResponse.getBody().getObject().getString("url");
		}

		return null;

	}

	
	
	private void processConsolidatedExcel(DataUploadInfo info,String filePath)
			throws AppException, SQLException, UnirestException, IOException {
		
		File consolidateExcel= new File(filePath);
		ConsolidatedService consolidatedService = new ConsolidatedService();

		Connection connection=null;
		
		TaxpayerGstin taxpayerGstin =info.getTaxpayerGstin();
		

		boolean process = true;

		if ("STARTED".equalsIgnoreCase(info.getStatus()) || process) {

			try {

				consolidatedService.importExcelToDatabase(consolidateExcel.getAbsolutePath(),info.getId(), DATA_SEPARATOR.COMMA);

				consolidatedService.callSetReturnTypeProcedure(info.getTaxpayerGstin().getGstin(),
						info.getMonthYear(), String.valueOf(info.getId()));

				String dataFileUrl = uploadDataFile(consolidateExcel,
						"Data_" + info.getFileName() + "_" + UUID.randomUUID().toString() + ".csv",
						"datareport", info.getGstin());
				
				
				PreparedStatement dataStatement = null;
				LOGGER.info("data file url {}", dataFileUrl);
				try {
					connection = DBUtils.getConnection();
					dataStatement = connection.prepareStatement(updateQueryDataUploadUrl);
					dataFileUrl = StringUtils.substringBefore(info.getFileName(), "#") + "#" + dataFileUrl;
					dataStatement.setString(1, dataFileUrl);
					dataStatement.setInt(2, info.getId());
					dataStatement.executeUpdate();

				} catch (SQLException e) {
					LOGGER.error("sql exceptuion while update data file url {}", dataFileUrl, e);
				}

				finally {
					if (dataStatement != null && !dataStatement.isClosed())
						DBUtils.close(dataStatement);
					if (connection != null && !connection.isClosed())
						DBUtils.close(connection);
				}
			} catch (Exception e1) {
				LOGGER.error("exception", e1);
				throw new AppException(e1);
			}
		}
		PreparedStatement dataStatement = null;
		try {

			try {

				SourceType source = SourceType.ERP;

				String delete = "Yes";

				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				gstinTransactionDTO.setTransactionType(TransactionType.ALL.toString());
				gstinTransactionDTO.setMonthYear(info.getMonthYear());
				gstinTransactionDTO.setTaxpayerGstin(
						(TaxpayerGstinDTO) EntityHelper.convert(taxpayerGstin, TaxpayerGstinDTO.class));

				int count = consolidatedService.proecessConsolidateCsv(gstinTransactionDTO,
						String.valueOf(info.getId()), source);

				LOGGER.debug("Transaction {} successfull processed with {} records", info.getTransaction(),
						count);

				connection = DBUtils.getConnection();
				dataStatement = connection.prepareStatement(updateQueryDataUpload);
				dataStatement.setString(1, "PROCESSED");
				storeErpLogs(info, false,
						"File data processed successfully, " + count + " line items updated successfully",
						null);
			} catch (AppException e) {
				LOGGER.error("app exception while process {} for {}", info.getTransaction(),
						info.getFileName(), e);
				String errors = e.getMessage();
				if (!StringUtils.isEmpty(errors) && errors.contains("|")) {
					errors = errors.replace("|", "\n");
					errors = "FILE,COLUMN,ROW,DESCRIPTION\n" + errors;
					InputStream in = new ByteArrayInputStream(errors.getBytes("UTF-8"));
					String errorFileUrl = uploadErrorReport(
							in, "ERROR_" + info.getFileName() + "_"
									+ Calendar.getInstance().getTimeInMillis() + ".csv",
							"errorreport", info.getGstin());

					storeErpLogs(info, true,
							"Unable to process file data, please check the logs for more information",
							errorFileUrl);
				}
				connection = DBUtils.getConnection();
				dataStatement = connection.prepareStatement(updateQueryDataUpload);
				dataStatement.setString(1, "PROCESSED_WITH_ERROR");
			} catch (Exception e) {
				LOGGER.error("exception while process {} for {}", info.getTransaction(), info.getFileName(),
						e);
				try {
					connection = DBUtils.getConnection();
					dataStatement = connection.prepareStatement(updateQueryDataUpload);
					dataStatement.setString(1, "PROCESSED_WITH_ERROR");
					storeErpLogs(info, true,
							"Unable to process file data, received file is not in proper format", null);
				} catch (Exception e1) {
					LOGGER.error("SQL exception while storing failure logs {}", e1.getMessage());
				}
			}
			dataStatement.setInt(2, info.getId());
			dataStatement.executeUpdate();
		} catch (Exception e2) {
			LOGGER.error("SQL exception while updating status of data upload ", e2);
		} finally {
			try {
				if (dataStatement != null && !dataStatement.isClosed())
					DBUtils.close(dataStatement);
				if (connection != null && !connection.isClosed())
					DBUtils.close(connection);
			} catch (Exception e) {
				LOGGER.error("SQL exception while updating status of data upload", e);
			}
		}
	


	}
	
	public void processConsolidatedExcelAsync(DataUploadInfo dataUploadInfo,String filePath) throws IOException {

		ExecutorService executorService = Executors.newSingleThreadExecutor();

		executorService.execute(new Runnable() {

			@Override
			public void run() {
				
				try {
					new ExternalServiceExecutor().processConsolidatedExcel(dataUploadInfo,filePath);
				} catch (AppException | SQLException | UnirestException | IOException e) {
					
					LOGGER.error("ecxeption while process consolidated excel {}",e);
				}
				
				LOGGER.debug("end of csv executor thread");
			}
		});

		LOGGER.debug("return from executor thread..");
	}


}