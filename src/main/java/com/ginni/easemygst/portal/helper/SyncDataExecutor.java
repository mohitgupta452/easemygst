package com.ginni.easemygst.portal.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ReturnsService;

@Stateless
public class SyncDataExecutor implements Callable<String> {

	private Logger log=LoggerFactory.getLogger(this.getClass());
	
	private GstinTransactionDTO  gstinTransactionDTO;
	
	@Inject
	ReturnsService returnService;
	
	@Override
	public String call() throws Exception {
		Map<String,Object>response=new HashMap<>();;
		try {
			log.error( "save data to gstn executor for gstn {},monthYear {}, returnType {}",
					gstinTransactionDTO.getTaxpayerGstin().getGstin(), gstinTransactionDTO.getMonthYear(),
					gstinTransactionDTO.getReturnType());
			response.putAll(returnService.syncData(gstinTransactionDTO));
			log.debug("sync result {}",StringUtils.join(response.values(),"|"));
		} catch (Exception e) {
				log.error("error in save data to gstn for gstn {},monthYear {}, returnType {},error {}",
						gstinTransactionDTO.getTaxpayerGstin().getGstin(), gstinTransactionDTO.getMonthYear(),
						gstinTransactionDTO.getReturnType(), e.getMessage());
				log.error("error occured",e);

			}
		log.debug("end saveDataToGstn");
		return "ok";
	}

	public SyncDataExecutor(GstinTransactionDTO gstinTransactionDTO){
		this.gstinTransactionDTO=gstinTransactionDTO ;
		
	}
	public SyncDataExecutor(){
		
		
	}
	
}
