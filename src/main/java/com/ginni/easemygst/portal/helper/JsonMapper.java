package com.ginni.easemygst.portal.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonMapper {

	public enum MixinType {

		SIGNIN("signin"), PROFILE("profile"), TAXPAYER("taxpayer"), GSTRETURN("gstreturn"), STATE("state"), TAXPAYER_GSTIN("taxpayer_gstin"), USER_CONTACT(
				"user_contact"), USER("user"), USER_GSTIN("user_gstin"), USER_TAXPAYER("user_taxpayer"), ALL("all");

		MixinType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	public static ObjectMapper objectMapper;

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}

}
