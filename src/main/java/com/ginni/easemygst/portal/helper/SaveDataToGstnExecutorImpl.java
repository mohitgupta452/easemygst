package com.ginni.easemygst.portal.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ReturnsService;

@Stateless(name="Executor")
public class SaveDataToGstnExecutorImpl implements Executor {
	
	private ReturnsService returnService;
	
	ExecutorService executorService = Executors.newSingleThreadExecutor();

	private Logger log=LoggerFactory.getLogger(this.getClass());
	
	private GstinTransactionDTO gstinTransactionDto;
	public SaveDataToGstnExecutorImpl() {
		super();
		
	}
	
	public SaveDataToGstnExecutorImpl(GstinTransactionDTO gstinTransactionDto) {
		super();
		this.gstinTransactionDto = gstinTransactionDto;
	}
	public GstinTransactionDTO getGstinTransactionDto() {
		return gstinTransactionDto;
	}
	public void setGstinTransactionDto(GstinTransactionDTO gstnTransactionDto) {
		this.gstinTransactionDto = gstnTransactionDto;
	}
	
	
	public void saveToGstn(){
	executorService.execute(new Runnable() {

		@Override
		public void run() {
			
			Map<String,Object>response=new HashMap<>();;
			try {
				log.error( "save data to gstn executor for gstn {},monthYear {}, returnType {}",
						gstinTransactionDto.getTaxpayerGstin().getGstin(), gstinTransactionDto.getMonthYear(),
						gstinTransactionDto.getReturnType());
				response.putAll(returnService.syncData(gstinTransactionDto));
				log.debug("sync result {}",StringUtils.join(response.values(),"|"));
			} catch (Exception e) {
					log.error("error in save data to gstn for gstn {},monthYear {}, returnType {},error {}",
							gstinTransactionDto.getTaxpayerGstin().getGstin(), gstinTransactionDto.getMonthYear(),
							gstinTransactionDto.getReturnType(), e.getMessage());
					log.error("error occured",e);

				}
			log.debug("end saveDataToGstn");
		}
	});
	executorService.shutdown();
	}
	
	@Asynchronous
	@Override
	public void execute(Runnable command) {
		command.run();
		
	}

}
