package com.ginni.easemygst.portal.helper.config;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.dto.UniversalReturnDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.entity.Calendar;
import com.ginni.easemygst.portal.persistence.entity.GstCalendar;
import com.ginni.easemygst.portal.persistence.procedure.TransactionProcedures;

@ApplicationScoped
public class AppConfig {

	private Logger log = LoggerFactory.getLogger(AppConfig.class);

	private static List<StateDTO> stateDTOs;

	private static List<GstCalendar> gstCalenders;
	
	private static List<Calendar> calendars;
	
	private static SortedSet<UniversalReturnDTO> uniReturnDTOs;
	
	public AppConfig() throws SQLException, AppException {
		this.init();
	}
	
	private static void initState() {
		try {
			FinderService finderService = (FinderService) InitialContext
					.doLookup("java:global/easemygst/FinderServiceImpl");
			stateDTOs = finderService.getAllState();
		} catch (NamingException | AppException e) {
			e.printStackTrace();
		}
	}
	
	private void setIpAddress() throws AppException {
		
			RedisService redisService = new RedisService();
			redisService.setKeyValue(ApplicationMetadata.SERVER_IP_ADDRESS,
					StringUtils.trim(RestClient.getCall(ApplicationMetadata.GET_IP_URL)));
	}

	private static void initGstCalendars() {
		try {
			FinderService finderService = (FinderService) InitialContext
					.doLookup("java:global/easemygst/FinderServiceImpl");
			gstCalenders = finderService.getAllGstCalendars();
		} catch (NamingException | AppException e) {
			e.printStackTrace();
		}
	}
	
	private static void initCalendars() {
		try {
			FinderService finderService = (FinderService) InitialContext
					.doLookup("java:global/easemygst/FinderServiceImpl");
			calendars = finderService.getAllCalendars();
		} catch (NamingException | AppException e) {
			e.printStackTrace();
		}
	}

	private void init() throws SQLException, AppException {
		initState();
		initGstCalendars();
		initCalendars();
		setIpAddress();
	/*	try (Connection connection = DBUtils.getConnection(); Statement statement = connection.createStatement();) {
			connection.setAutoCommit(false);
			statement.addBatch(TransactionProcedures.getDropb2bprocedure());
			statement.addBatch(TransactionProcedures.getB2binsertupdateprocedure());

			statement.addBatch(TransactionProcedures.getDropatproc());
			statement.addBatch(TransactionProcedures.getAtproc());

			statement.addBatch(TransactionProcedures.getDroptxpdproc());
			statement.addBatch(TransactionProcedures.getTxpdproc());

			statement.addBatch(TransactionProcedures.getDropb2csproc());
			statement.addBatch(TransactionProcedures.getB2csproc());

			statement.addBatch(TransactionProcedures.getDropcdnproc());
			statement.addBatch(TransactionProcedures.getCdnprocedure());

			statement.addBatch(TransactionProcedures.getDropcdnurproc());
			statement.addBatch(TransactionProcedures.getCdnurprocedure());

			statement.addBatch(TransactionProcedures.getDropexpproc());
			statement.addBatch(TransactionProcedures.getExpproc());

			statement.addBatch(TransactionProcedures.getB2cldropproc());
			statement.addBatch(TransactionProcedures.getB2clproc());

			statement.addBatch(TransactionProcedures.getDropnilproc());
			statement.addBatch(TransactionProcedures.getNilproc());

			statement.addBatch(TransactionProcedures.getDropitemproc());
			statement.addBatch(TransactionProcedures.getItemproc());

			statement.addBatch(TransactionProcedures.getDropImpsProc());
			statement.addBatch(TransactionProcedures.getImpsProc());

			statement.addBatch(TransactionProcedures.getDropimpgproc());
			statement.addBatch(TransactionProcedures.getImpgproc());

			statement.addBatch(TransactionProcedures.getDropb2burproc());
			statement.addBatch(TransactionProcedures.getB2burprocedure());

			statement.executeBatch();

			connection.commit();
			DBUtils.close(statement);
			DBUtils.close(connection);

		} catch (SQLException e) {
			e.printStackTrace();
			log.error("procedure creation failed.... {}", e);
			throw e;
		}
		*/

	}

	public static StateDTO getStateNameByCode(String code) {
		if (stateDTOs == null)
			initState();
		return stateDTOs.stream().filter(state -> Integer.valueOf(code) == (Integer.valueOf(state.getScode())))
				.findFirst().orElse(null);
	}

	public static StateDTO getStateNameByStateCode(String scode) {
		if (stateDTOs == null)
			initState();
		return stateDTOs.stream().filter(state -> scode.endsWith(state.getCode())).findFirst().orElse(null);
	}
	
	public static SortedSet<UniversalReturnDTO> getUniversalReturnDTO() {
		return uniReturnDTOs;
	}
	
	public static void setUniversalReturnDTO(SortedSet<UniversalReturnDTO> temp) {
		uniReturnDTOs = temp;
	}

	public static GstCalendar getGstCalendarByCode(String monthId, String returnType) {
		if (gstCalenders == null)
			initGstCalendars();
		return gstCalenders.stream().filter(cal -> (cal.getMonthId().equalsIgnoreCase(monthId)
				&& cal.getReturnType().equalsIgnoreCase(returnType))).findFirst().orElse(null);
	}
	
	public static Calendar getCalendarByCode(String monthId) {
		if (calendars == null)
			initCalendars();
		return calendars.stream().filter(cal -> (cal.getMonthId().equalsIgnoreCase(monthId))).findFirst().orElse(null);
	}
	
	public static GstCalendar getGstCalendarByCode(String monthId,String returnType,String fyear) {
		if (calendars == null)
			initGstCalendars();
		return gstCalenders.stream()
				.filter(cal -> (cal.getMonthId().equalsIgnoreCase(monthId) && cal.getFyear().equalsIgnoreCase(fyear)
					&& cal.getReturnType().equalsIgnoreCase(returnType)))
				.findFirst().orElse(null);
	}
	
	

	
	public static String getProperty(String key) throws IOException {
		
		Properties appConfig = new Properties();

		appConfig.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/config.properties"));
		
		return appConfig.getProperty(key);
	}
	public static String getActiveProfile() {

		try {
			return getProperty("profile-active");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
