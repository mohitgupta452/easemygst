package com.ginni.easemygst.portal.helper;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO.TaxpayerDTOUserMixIn;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO.TaxpayerGstinDTOUserMixIn;

public class EntityHelper {
	
	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	public static <T> Object convert(T t, Class<?> c) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		/*mapper.addMixIn(TaxpayerDTO.class, TaxpayerDTOUserMixIn.class);
		mapper.addMixIn(TaxpayerGstinDTO.class, TaxpayerGstinDTOUserMixIn.class);*/
		return mapper.convertValue(t, c);
	}

	@SuppressWarnings("finally")
	public static <T> T convert(T t1, T t2) {
		try {
			Method[] methods = t2.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1) {
					if (method.invoke(t2) != null) {
						Method method2 = t2.getClass().getMethod("set"+method.getName().substring
								(3, method.getName().length()), method.getReturnType());
						method2.invoke(t1, method.invoke(t2));
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException | NoSuchMethodException e) {
			log.error("exception while calling convert for primary and secondary object");
			e.printStackTrace();
		} finally {
			return t1;
		}
	}

}
