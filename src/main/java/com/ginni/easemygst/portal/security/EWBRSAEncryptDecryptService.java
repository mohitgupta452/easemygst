package com.ginni.easemygst.portal.security;

import java.io.IOException;
import java.util.Properties;

import javax.ejb.Singleton;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.EWayBillService.EWB_EVENT;

@Singleton
public class EWBRSAEncryptDecryptService extends EncryptDecrypt {

	private static final long serialVersionUID = 1L;
	
	@Override
	public String encrypt(byte[] message) throws IOException {
		return this.encrypt(message, this.getCretificatePath());
	}

	@Override
	public String encrypt(String message) throws IOException {

		return this.encrypt(message, this.getCretificatePath());
	}

	private String getCretificatePath() throws IOException {

		Properties ewbprop = new Properties();

		ewbprop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/ewbconfig.properties"));

		return ewbprop.getProperty("certificate-path");
	}

}
