package com.ginni.easemygst.portal.security;

import java.io.IOException;

import javax.inject.Singleton;

import com.ginni.easemygst.portal.helper.config.AppConfig;

@Singleton
public class GSTRSAEncryptDecryptService extends EncryptDecrypt{

	private static final long serialVersionUID = 1L;

	@Override
	public String encrypt(byte[] message) throws IOException {
		return this.encrypt(message,this.getCretificatePath());
	}

	@Override
	public String encrypt(String message) throws IOException {
		
		return this.encrypt(message,this.getCretificatePath());
	}
	
	private String getCretificatePath() throws IOException {
	 return	 AppConfig.getProperty("main.gstn.certificate.path");
	}

}
