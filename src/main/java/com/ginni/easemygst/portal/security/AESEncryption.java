package com.ginni.easemygst.portal.security;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.helper.AppException;

public class AESEncryption {

	protected Logger log = LoggerFactory.getLogger(AESEncryption.class);

	private final String AES_TRANSFORMATION = "AES/ECB/PKCS7Padding";
	private final String AES_ALGORITHM = "AES";
	private final int ENC_BITS = 256;
	private final String CHARACTER_ENCODING = "UTF-8";

	private Cipher ENCRYPT_CIPHER;
	private Cipher DECRYPT_CIPHER;
	private KeyGenerator KEYGEN;

	public AESEncryption() {
		try {
			ENCRYPT_CIPHER = Cipher.getInstance(AES_TRANSFORMATION);
			DECRYPT_CIPHER = Cipher.getInstance(AES_TRANSFORMATION);
			KEYGEN = KeyGenerator.getInstance(AES_ALGORITHM);
			KEYGEN.init(ENC_BITS);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			log.error("Unable to get aesEncryption object {}", e.getMessage());
			e.printStackTrace();
		}
	}

	public String generateSecureKey() throws Exception {
		SecretKey secretKey = KEYGEN.generateKey();
		return encodeBase64String(secretKey.getEncoded());
	}
	
	public byte[] generateSecureKeyBytes() throws Exception {
		SecretKey secretKey = KEYGEN.generateKey();
		return secretKey.getEncoded();
	}

	public String encodeBase64String(byte[] bytes) {
		return new String(java.util.Base64.getEncoder().encode(bytes));
	}

	public byte[] decodeBase64StringTOByte(String stringData) {
		try {
			return java.util.Base64.getDecoder().decode(stringData.getBytes(CHARACTER_ENCODING));
		} catch (UnsupportedEncodingException e) {
			log.error("Unsupported encoding exception while calling decodeBase64StringTOByte",e);
		}
		return null;
	}

	public String encrypt(String strToEncrypt, byte[] secret) throws AppException {
		try {
			SecretKeySpec sk = new SecretKeySpec(secret, AES_ALGORITHM);
			ENCRYPT_CIPHER.init(Cipher.ENCRYPT_MODE, sk);
			return Base64.encodeBase64String(ENCRYPT_CIPHER.doFinal(strToEncrypt.getBytes(CHARACTER_ENCODING)));
		} catch (Exception e) {
			log.error("Error while encrypting: ", e);
			throw new AppException();
		}
	}

	public String encryptWithBase64(String strToEncrypt, byte[] secret) throws AppException {
		try {
			SecretKeySpec sk = new SecretKeySpec(secret, AES_ALGORITHM);
			ENCRYPT_CIPHER.init(Cipher.ENCRYPT_MODE, sk);
			return Base64.encodeBase64String(ENCRYPT_CIPHER.doFinal(
					org.apache.commons.net.util.Base64.decodeBase64(strToEncrypt)));
		} catch (Exception e) {
			log.error("Error while encrypting: ", e);
			throw new AppException();
		}
	}
	public byte[] decrypt(String strToDecrypt, byte[] secret) throws AppException {
		try {
			SecretKeySpec sk = new SecretKeySpec(secret, AES_ALGORITHM);
			DECRYPT_CIPHER.init(Cipher.DECRYPT_MODE, sk);
			return DECRYPT_CIPHER.doFinal( org.apache.commons.net.util.Base64.decodeBase64(strToDecrypt.getBytes()));
		} catch (Exception e) {
			log.error("Error while encrypting: ", e);
		
			throw new AppException(e);
		}
	}

	public String getEncryptedForRefreshToken(String oldAppKey, String oldSEK,String newApppKey) throws Exception {

		try {
			
			/*
			 * 	appKey=aesEncryption.generateSecureKey();
			byte[] authSEK = aesEncryption.decrypt(taxpayerGstin.getSek(),aesEncryption.decodeBase64StringTOByte(taxpayerGstin.getAppKey()));
			newAppKey = aesEncryption.encryptWithBase64(appKey, authSEK);
			 */

		byte[] authSEK = this.decrypt(oldSEK,this.decodeBase64StringTOByte(oldAppKey));
       return this.encryptWithBase64(newApppKey, authSEK);
		} catch (Exception e) {
			log.error("Error while getAppKeyForRefreshToken: ", e);
			throw e;
		}
	}

}
