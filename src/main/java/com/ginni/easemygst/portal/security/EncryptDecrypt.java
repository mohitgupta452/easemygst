package com.ginni.easemygst.portal.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

import javax.crypto.Cipher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.utils.ManipulateFile;

public abstract class EncryptDecrypt implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static Logger log = LoggerFactory.getLogger(EncryptDecrypt.class);
	
	abstract public String encrypt(byte[] message) throws IOException;
	
	abstract public String encrypt(String message) throws IOException;

//	private static String CERTIFICATE = "certificate/GSTN_public.cer";
	
	//private static String CERTIFICATE_PROD = "certificate/GSTN_G2B_Prod_Public.cer";

	/*
	 * Convert into hex values
	 */
	private String hex(String binStr) {

		String newStr = new String();

		try {
			String hexStr = "0123456789ABCDEF";
			byte[] p = binStr.getBytes();
			for (int k = 0; k < p.length; k++) {
				int j = (p[k] >> 4) & 0xF;
				newStr = newStr + hexStr.charAt(j);
				j = p[k] & 0xF;
				newStr = newStr + hexStr.charAt(j) + " ";
			}
		} catch (Exception e) {
			System.out.println("Failed to convert into hex values: " + e);
		}
		return newStr;
	}

	@SuppressWarnings("finally")
	public File getResourceFile(String template, String fileName) {
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(template);

		File outFile = new File(ManipulateFile.getParentDirBasePath()+"/"+fileName);
		
		if(outFile.exists())
			return outFile;
		
		OutputStream outputStream = null;

		try {
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(outFile);

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			return outFile;
		}
	}

	/*
	 * Encrypt a message using a certificate file cacert.pem (contains public
	 * key). Decrypt the encrypted message using a private key file (cakey.p8c).
	 */
	@SuppressWarnings("finally")
	protected String encrypt(String message,String certificatePath) {

		byte[] messageBytes;
		byte[] tempPub = null;
		String sPub = null;
		byte[] ciphertextBytes = null;

		try {

			// Obtain a RSA Cipher Object
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			
			// Loading certificate file
			InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(certificatePath);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
			inStream.close();

			// Read the public key from certificate file
			RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();
			tempPub = pubkey.getEncoded();
			sPub = new String(tempPub);

			// Set plain message
			messageBytes = message.getBytes("UTF-8");

			// Initialize the cipher for encryption
			cipher.init(Cipher.ENCRYPT_MODE, pubkey);

			// Encrypt the message
			ciphertextBytes = cipher.doFinal(messageBytes);

		} catch (IOException e) {
			System.out.println("IOException:" + e);
		} catch (CertificateException e) {
			System.out.println("CertificateException:" + e);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("NoSuchAlgorithmException:" + e);
		} catch (Exception e) {
			System.out.println("Exception:" + e);
		} finally {
			return Base64.getEncoder().encodeToString(ciphertextBytes);
		}
	}

	/*
	 * Encrypt a message using a certificate file cacert.pem (contains public
	 * key). Decrypt the encrypted message using a private key file (cakey.p8c).
	 */
	@SuppressWarnings("finally")
	protected String encrypt(byte[] message,String cretificatePath) {

		byte[] messageBytes;
		byte[] tempPub = null;
		String sPub = null;
		byte[] ciphertextBytes = null;

		try {

			// Obtain a RSA Cipher Object
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			
			// Loading certificate file
			InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(cretificatePath);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
			inStream.close();

			// Read the public key from certificate file
			RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();
			tempPub = pubkey.getEncoded();
			sPub = new String(tempPub);
			// Set plain message
			messageBytes = message;

			// Initialize the cipher for encryption
			cipher.init(Cipher.ENCRYPT_MODE, pubkey);

			// Encrypt the message
			ciphertextBytes = cipher.doFinal(messageBytes);
		} catch (IOException e) {
			System.out.println("IOException:" + e);
		} catch (CertificateException e) {
			System.out.println("CertificateException:" + e);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("NoSuchAlgorithmException:" + e);
		} catch (Exception e) {
			System.out.println("Exception:" + e);
		} finally {
			return Base64.getEncoder().encodeToString(ciphertextBytes);
		}
	}
	
	public String getDecryptedData(byte[] data, String key) {
		String response=null;
		try {
			AESEncryption aesEncry = new AESEncryption();
			byte[] decryptedData = aesEncry.decrypt(new String(data),
			Base64.getDecoder().decode(new String(key.getBytes()).getBytes("UTF-8")));
			response = new String(decryptedData);
		} catch (Exception e) {
			log.error("exception occured while {} " + e);
		}
	return response;	
 }

}
