package com.ginni.easemygst.portal.security;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.ejb.Singleton;

import com.google.common.io.ByteStreams;


@Singleton
public class ExactEncrypt {

	private static String CERTIFICATE = "certificate/EXACT_PublicKey.der";

	public static byte[] readFileBytes() throws IOException {
		InputStream url =  Thread.currentThread().getContextClassLoader().getResourceAsStream(CERTIFICATE);
		return ByteStreams.toByteArray(url);
	}

	public static PublicKey readPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(readFileBytes());
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(publicSpec);
	}

	@SuppressWarnings("finally")
	public static String encrypt(String plaintext) {

		byte[] ciphertextBytes = null;
		
		try {
			PublicKey key = readPublicKey();
			byte[] message = plaintext.getBytes("UTF8");
			
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			ciphertextBytes = cipher.doFinal(message);
			
		} catch (IOException e) {
			System.out.println("IOException:" + e);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("NoSuchAlgorithmException:" + e);
		} catch (Exception e) {
			System.out.println("Exception:" + e);
		} finally {
			return Base64.getEncoder().encodeToString(ciphertextBytes);
		}
	}

}
