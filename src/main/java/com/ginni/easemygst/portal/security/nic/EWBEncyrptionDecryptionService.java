package com.ginni.easemygst.portal.security.nic;

import java.io.UnsupportedEncodingException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.business.service.EWayBillService.EWB_EVENT;
import com.ginni.easemygst.portal.constant.EwayBillConstant;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBAuthRequest;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBBaseRequest;
import com.ginni.easemygst.portal.security.AESEncryption;
import com.ginni.easemygst.portal.security.EWBRSAEncryptDecryptService;

@RequestScoped
public class EWBEncyrptionDecryptionService {

	private static final Logger _LOGGER = org.slf4j.LoggerFactory.getLogger(EWBEncyrptionDecryptionService.class);

	private static final ObjectMapper mapper = new ObjectMapper();

	@Inject
	private EWBRSAEncryptDecryptService ewbrsaEncryptDecryptService;

	@Inject
	private RedisService redisService;

	public void encryptRequest(EWBBaseRequest baseRequest) throws Exception {

		if (EWB_EVENT.AUTHENTICATE == baseRequest.getEvent())
			this.encryptAuthRequest(baseRequest);
		else
			this.encryptEBWRequest(baseRequest);
	}

	private void encryptAuthRequest(EWBBaseRequest request) throws Exception {

		EWBAuthRequest authRequest = (EWBAuthRequest) request;

		authRequest.setAppKey(ewbrsaEncryptDecryptService.encrypt(authRequest.getAppKeyBytes()));
		authRequest.setPassword(ewbrsaEncryptDecryptService.encrypt(authRequest.getPassword()));

		request.setData(mapper.convertValue(authRequest, JsonNode.class));

	}

	private void encryptEBWRequest(EWBBaseRequest request)
			throws AppException, JsonProcessingException, UnsupportedEncodingException {

		AESEncryption aesEncryption = new AESEncryption();

		ObjectNode encRequest = mapper.createObjectNode();

		request.setData(encRequest.put("data", aesEncryption.encrypt(mapper.writeValueAsString(request),
				aesEncryption.decodeBase64StringTOByte(this.getDecryptSek(request.getGstin())))));
	}

	public String decryptErrorCode(String encErrorCode) {

		byte[] responseBytes = Base64.decodeBase64(encErrorCode.getBytes());

		String response = new String(responseBytes);

		_LOGGER.error("ewb nic error code {}", response);
		return response;
	}

	public String decryptResponse(String encResponse, String gstin) throws UnsupportedEncodingException, AppException {

		return new String(Base64.decodeBase64(this.aesDecryption(encResponse, this.getDecryptSek(gstin))));

	}

	private String getDecryptSek(String gstin) throws UnsupportedEncodingException, AppException {

		String encSek = redisService.getValue(String.format(EwayBillConstant._SEK, gstin));

		String app_key = redisService.getValue(String.format(EwayBillConstant._APP_KEY, gstin));

		return this.aesDecryption(encSek, app_key);
	}

	private String aesDecryption(String encData, String dercrptionKey) throws UnsupportedEncodingException, AppException {

		AESEncryption aesEncryption = new AESEncryption();

		return aesEncryption.encodeBase64String(aesEncryption.decrypt(encData, aesEncryption.decodeBase64StringTOByte(dercrptionKey)));
	}
	
	private byte[] aesDecryption(String encData, byte [] dercrptionKeyBytes) throws UnsupportedEncodingException, AppException {

		AESEncryption aesEncryption = new AESEncryption();

		return aesEncryption.decrypt(encData,dercrptionKeyBytes);
	}

}
