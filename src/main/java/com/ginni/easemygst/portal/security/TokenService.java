package com.ginni.easemygst.portal.security;

import static org.mockito.Matchers.booleanThat;

import java.io.Serializable;
import java.lang.invoke.MethodHandles;
import java.security.Key;
import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.crypto.SecretKey;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.helper.config.AppConfig;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.PrematureJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.MacProvider;

/**
 * 
 * iat - issued at - Current Time
 * iss - issuer - EaseMyGST
 * aud - audience - FirstName + LastName
 * prn - principal - Username
 * jti - jwt id - UserId
 * exp - expiry - now() + 2 hrs
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since Dec 3, 2016
 *
 */
@Stateless
@Transactional
public class TokenService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();
	
	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	private final int SECURE_RANDOM_BYTES = 64;
	
	@Inject
	private RedisService redisService;

	public String generateToken(UserDTO userDTO) {

		if (!Objects.isNull(userDTO)) {
			Map<String, Object> claims = new HashMap<>();
			claims.put("iss", "EaseMyGST");
			claims.put("aud", userDTO.getFirstName()+" "+userDTO.getLastName());
			claims.put("prn", userDTO.getUsername());
			claims.put("jti", userDTO.getId());
			claims.put("ipa", userDTO.getCreationIpAddress());
			
			//generated key
			Key key = generateKey();	
			
			boolean expiry=true;
			int expirySecond=7200;
			
			OffsetDateTime issAt = OffsetDateTime.now(ZoneOffset.UTC);
			OffsetDateTime expirationTime = issAt.plusMinutes(360);
			
			JwtBuilder jwtBuilder = Jwts.builder().setIssuedAt(Date.from(issAt.toInstant())).setClaims(claims)
					.signWith(SignatureAlgorithm.HS512, key);
			jwtBuilder.setExpiration(Date.from(expirationTime.toInstant()));
			
			redisService.setUser(jwtBuilder.compact(), userDTO, expiry, expirySecond);
			redisService.setValue(jwtBuilder.compact()+"_IPA",userDTO.getCreationIpAddress(), expiry, expirySecond);
			redisService.setSecKey(jwtBuilder.compact()+"_KEY", key, expiry, expirySecond);
			
			return jwtBuilder.compact();
		}
		return null;
	}

	public String generateToken(Key key, Map<String, Object> claims, OffsetDateTime expirationTime) {

		OffsetDateTime issAt = OffsetDateTime.now(ZoneOffset.UTC);
		JwtBuilder jwtBuilder = Jwts.builder().setIssuedAt(Date.from(issAt.toInstant())).setClaims(claims)
				.signWith(SignatureAlgorithm.HS512, key);

		if (!(Objects.isNull(expirationTime) || expirationTime.isBefore(issAt))) {
			jwtBuilder.setExpiration(Date.from(expirationTime.toInstant()));
		}
		return jwtBuilder.compact();
	}
	
	public Boolean isValidSignature(String token, String ipAddress) {
		try {
			Key key = redisService.getSecKey(token+"_KEY");
			String ipa = redisService.getValue(token+"_IPA");
			if(key!=null && ipa!=null && ipa.equals(ipAddress)) {
				Jwts.parser().setSigningKey(key).parseClaimsJws(token);
				return Boolean.TRUE;
			} else
				return Boolean.FALSE;
		} catch (SignatureException e) {
			return Boolean.FALSE;
		}
	}
	
	public Boolean isValidSignature(String token) {
		try {
			Key key = redisService.getSecKey(token+"_KEY");
			if(key!=null) {
				Jwts.parser().setSigningKey(key).parseClaimsJws(token);
				return Boolean.TRUE;
			} else
				return Boolean.FALSE;
		} catch (SignatureException e) {
			return Boolean.FALSE;
		}
	}
	
	public Boolean isValidToken(String token, String key) {
		try {
			if(token!=null) {
				Jwts.parser().setSigningKey(key).parseClaimsJws(token);
				return Boolean.TRUE;
			} else
				return Boolean.FALSE;
		} catch (SignatureException e) {
			return Boolean.FALSE;
		}
	}

	public Boolean isValidSignature(String token, Key key) {
		try {
			Jwts.parser().setSigningKey(key).parseClaimsJws(token);
			return Boolean.TRUE;
		} catch (SignatureException e) {
			return Boolean.FALSE;
		}
	}

	public Key generateKey() {
		SecretKey secretKey = MacProvider.generateKey(SignatureAlgorithm.HS512, generateSecureRandom());
		return secretKey;
	}

	private SecureRandom generateSecureRandom() {
		SecureRandom random = new SecureRandom();
		random.nextBytes(new byte[SECURE_RANDOM_BYTES]);
		return random;
	}

	public Map<String, Object> getClaims(String token, Key key)
			throws SignatureException, ExpiredJwtException, PrematureJwtException {
		Jws<Claims> claims = null;

		claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token); 

		Map<String, Object> claimsMap = new HashMap<>();
		claims.getBody().forEach((k, v) -> claimsMap.put(k, v));
		return claimsMap;
	}
	
}

