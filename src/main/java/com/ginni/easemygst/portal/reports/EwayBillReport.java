package com.ginni.easemygst.portal.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EwayBillReportModel;
import com.ginni.easemygst.portal.helper.EwayBillReportModel.ItemsDetails;
import com.ginni.easemygst.portal.helper.EwayBillReportModel.VehicleDetais;
import com.ginni.easemygst.portal.requestresponse.EWBGenerateRequest;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBItem;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBGenerateResponse;
import com.ginni.easemygst.portal.utils.AzzureFIleUpload;
import com.ginni.easemygst.portal.utils.AzzureFIleUpload.AZZURE_DIRECTORY;
import com.ginni.easemygst.portal.utils.ManipulateFile;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class EwayBillReport {

	private final static ObjectMapper _MAPPER = new ObjectMapper();
	private static final Logger _LOGGER = org.slf4j.LoggerFactory.getLogger(EwayBillReport.class);

	public String generateAndUploadEwayBillPdfToAzzure(EWBGenerateRequest ewbGenerateRequest,
			EWBGenerateResponse ewbGenerateResponse) throws AppException {

		try {

			EwayBillReportModel ewayBillReportModel = this.getEwayBillJReportModel(ewbGenerateRequest,
					ewbGenerateResponse);
			JasperPrint jasperPrint = this.generateJReport(ewayBillReportModel);
			File ewayPdf = this.JasperPrintToEwayPdf(
					ewbGenerateResponse.getEwayBillNo() + "-" + ewbGenerateRequest.getReferenceNumber(), jasperPrint);
			return this.uploadEwayBillToAzzure(ewayPdf);
		} catch (Exception e) {
			_LOGGER.error(" error", e);
			throw new AppException(e);
		}

	}

	private JasperPrint generateJReport(EwayBillReportModel billReportModel) throws JRException {

		JasperReport jasperReport = JasperCompileManager
				.compileReport(Thread.currentThread().getContextClassLoader().getResourceAsStream("jasper/Bill.jrxml"));

		JRBeanCollectionDataSource itemsJRBean = new JRBeanCollectionDataSource(billReportModel.getItemsDetails());

		JRBeanCollectionDataSource vehicleJRBean = new JRBeanCollectionDataSource(billReportModel.getVehicleDetais());

		Map<String, Object> paramMap = _MAPPER.convertValue(billReportModel, Map.class);

		paramMap.put("items", itemsJRBean);

		paramMap.put("vehicle", vehicleJRBean);

		return JasperFillManager.fillReport(jasperReport, paramMap, new JREmptyDataSource());
	}

	private File JasperPrintToEwayPdf(String fileName, JasperPrint jasperPrint) throws JRException, IOException {

		String filePath = ManipulateFile.getParentDirBasePath() + File.separator + "ewaypdf" + File.separator + fileName
				+ ".pdf";

		java.io.OutputStream outputStream = new FileOutputStream(new File(filePath));

		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		return new File(filePath);
	}

	private String uploadEwayBillToAzzure(File file) throws AppException {

		return AzzureFIleUpload.uploadFileToAzzureService(AZZURE_DIRECTORY.EWAYBILLPDF, file);

	}

	private EwayBillReportModel getEwayBillJReportModel(EWBGenerateRequest ewbGenerateRequest,
			EWBGenerateResponse ewbGenerateResponse) {

		EwayBillReportModel ewayBillReportModel = new EwayBillReportModel();

		ewayBillReportModel.setBillNo(ewbGenerateResponse.getEwayBillNo());

		String code = ewbGenerateResponse.getEwayBillNo() + "/" + ewbGenerateRequest.getFromGstin() + "/"
				+ ewbGenerateResponse.getValidUpto();

		ewayBillReportModel.setQrCode(code);
		ewayBillReportModel.setBarCode(code);

		ewayBillReportModel.setAddressfrom(ewbGenerateRequest.getGstin() + "<br/>" + ewbGenerateRequest.getFromTrdName()
				+ "<br/>" + ewbGenerateRequest.getFromAddr1() + "<br/>" + ewbGenerateRequest.getFromAddr2() + "<br/>"
				+ ewbGenerateRequest.getFromPlace() + "<br/>" + ewbGenerateRequest.getFromPincode());

		ewayBillReportModel.setAddressto(ewbGenerateRequest.getToGstin() + "<br/>" + ewbGenerateRequest.getToTrdName()
				+ "<br/>" + ewbGenerateRequest.getToAddr1() + "<br/>" + ewbGenerateRequest.getToAddr2() + "<br/>"
				+ ewbGenerateRequest.getToPlace() + "<br/>" + ewbGenerateRequest.getToPincode());

		ewayBillReportModel.setCessAmount(ewbGenerateRequest.getCessValue().doubleValue());
		ewayBillReportModel.setCgstAmount(ewbGenerateRequest.getCgstValue().doubleValue());
		ewayBillReportModel.setDistance(ewbGenerateRequest.getTransDistance());

		ewayBillReportModel.setDocumentDetail(ewbGenerateRequest.getDocType() + " - " + ewbGenerateRequest.getDocNo()
				+ " - " + ewbGenerateRequest.getDocDate());

		ewayBillReportModel.setGeneratedBy(ewbGenerateRequest.getFromGstin());

		ewayBillReportModel.setGeneratedDate(ewbGenerateResponse.getEwayBillDate());

		ewayBillReportModel.setIgstAmount(ewbGenerateRequest.getIgstValue().doubleValue());
		ewayBillReportModel.setSgstAmount(ewbGenerateRequest.getSgstValue().doubleValue());
		ewayBillReportModel.setTaxAmount(ewbGenerateRequest.getTotalValue().doubleValue());

		ewayBillReportModel.setTrsnsportDate(ewbGenerateRequest.getTransDocDate());

		ewayBillReportModel
				.setTrsnsportId(ewbGenerateRequest.getTransporterName());
		ewayBillReportModel.setType(ewbGenerateRequest.getVehicleType());
		ewayBillReportModel.setValidUpto(StringUtils.split(ewbGenerateResponse.getValidUpto(), " ")[0]);
		ewayBillReportModel.setMode(ewbGenerateRequest.getTransMode());

		this.setVehicleDetails(ewbGenerateRequest, ewayBillReportModel);
		this.setItemsDetails(ewbGenerateRequest, ewayBillReportModel);

		return ewayBillReportModel;
	}

	private void setVehicleDetails(EWBGenerateRequest ewbGenerateRequest, EwayBillReportModel ewayBillReportModel) {

		VehicleDetais vehicleDetais = new VehicleDetais();
		ewayBillReportModel.getVehicleDetais().add(vehicleDetais);
		vehicleDetais.setEnterDate("entered date");
		vehicleDetais.setEnterBy("ENTERED BY");
		vehicleDetais.setVehicleNo(ewbGenerateRequest.getVehicleNo());
		vehicleDetais.setMode(ewbGenerateRequest.getTransMode());
		vehicleDetais.setCity(ewbGenerateRequest.getFromPlace());
	}

	private void setItemsDetails(EWBGenerateRequest ewbGenerateRequest, EwayBillReportModel ewayBillReportModel) {

		for (EWBItem ewbItem : ewbGenerateRequest.getEwbItems()) {
			ItemsDetails itemsDetails = new ItemsDetails();

			itemsDetails.setHsnCode(String.valueOf(ewbItem.getHsnCode()));

			itemsDetails.setTotalAmount(ewbItem.getTaxableAmount().doubleValue());

			itemsDetails.setTaxRate(String.valueOf(ewbItem.getIgstRate().intValue()) + "+"
					+ String.valueOf(ewbItem.getCessRate().intValue()) + "+"
					+ String.valueOf(ewbItem.getCgstRate().intValue()) + "+"
					+ (ewbGenerateRequest.getSgstValue().intValue()));
			itemsDetails.setQuantity(ewbGenerateRequest.getEwbItems().get(0).getQuantity().toPlainString());
			itemsDetails.setProduct(ewbGenerateRequest.getEwbItems().get(0).getProductDesc());

			ewayBillReportModel.getItemsDetails().add(itemsDetails);

		}

	}

}
