package com.ginni.easemygst.portal.reports;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.ginni.easemygst.portal.business.dto.ComparisonDTO;
import com.ginni.easemygst.portal.business.dto.GstinComparisonDTO;
import com.ginni.easemygst.portal.utils.ManipulateFile;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
	
public class Gstr3bvs1Report {
	private static final Logger log = Logger.getLogger(Gstr3bvs1Report.class);
	
	@SuppressWarnings({ "finally", "unchecked" })
	public static String generatereport(Map<String, Object> reportMap,String fileType) {
		InputStream srcInputStream1=null;
		InputStream srcInputStream2=null;
		InputStream srcInputStream3=null;
		List<String> sheetNames=new ArrayList<>();
		sheetNames.add("Consolidated Summary");
		sheetNames.add("Pan and month wise");
     		String destFile=null;
	   	    ComparisonDTO overall=(ComparisonDTO)reportMap.get("overall");
	   	    reportMap.put("image","http://dev.easemygst.com/assets/images/Emailer/gst-logo.png");
		    List<GstinComparisonDTO> tindetail=  (  List<GstinComparisonDTO>)reportMap.get("tinDetail");
		   reportMap.put("amt1",overall.getTaxesAsPerGSTR1().doubleValue());
			reportMap.put("amt2",overall.getOutwardTaxesandSupplies().doubleValue());
			reportMap.put("amt3",overall.getDifference().doubleValue());
			reportMap.put("amt4",overall.getPercentageofExcess().doubleValue());
			if(fileType.equalsIgnoreCase("pdf")){
     	   srcInputStream1 = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("jasper/timesnewroman/Outwardpdf1.jrxml");
           srcInputStream2 = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("jasper/timesnewroman/Outwardpdf2.jrxml");
           srcInputStream3 = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("jasper/timesnewroman/Outwardpdf3.jrxml");
			}
			else
			{
				 srcInputStream1 = Thread.currentThread().getContextClassLoader()
							.getResourceAsStream("jasper/timesnewroman/Outwardxls1.jrxml");
			     srcInputStream2 = Thread.currentThread().getContextClassLoader()
							.getResourceAsStream("jasper/timesnewroman/Outwardxls2.jrxml");
			     srcInputStream3 = Thread.currentThread().getContextClassLoader()
							.getResourceAsStream("jasper/timesnewroman/Outwardxls3.jrxml");
			}
		try {
			JasperReport jasperreport1 = JasperCompileManager.compileReport(srcInputStream1); 
			JasperPrint printFileName1 = JasperFillManager.fillReport(jasperreport1, reportMap, new JREmptyDataSource());
			JasperReport jasperreport2 = JasperCompileManager.compileReport(srcInputStream2); 
			JasperPrint printFileName2 = JasperFillManager.fillReport(jasperreport2,Gstr3bvs1Report.getMap(reportMap,"panDetail"), new JREmptyDataSource());
			JasperReport jasperreport3 = JasperCompileManager.compileReport(srcInputStream3);
			List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
			jasperPrintList.add(printFileName1);
			jasperPrintList.add(printFileName2);
			
		    for (GstinComparisonDTO data : tindetail) {
		    	reportMap.put("tinNo", data.getGstin()+"-("+data.getState()+")");
		    	if(!Objects.isNull(data.getComparisonDTOlist())){
		    	   	sheetNames.add(data.getState());
		    	jasperPrintList.add(  	
       	 JasperFillManager.fillReport(jasperreport3,Gstr3bvs1Report.getMap(data.getComparisonDTOlist(),reportMap), new JREmptyDataSource()));
	          }
		    }
		    destFile =Gstr3bvs1Report.filegenration(jasperPrintList,fileType,reportMap,sheetNames);
		} catch (Exception e) {
			if(log.isDebugEnabled())
		     log.debug(e.getMessage());
		} finally {
			return destFile;
		}

	}
	

	private static Map<String, Object> getMap(List<ComparisonDTO> comparisonDTOlist,Map<String, Object> reportMap) {
		Map<String, Object> tinmap=new HashMap<>();
		  int i=1,j=1;
     	for (ComparisonDTO data : comparisonDTOlist) {
			tinmap.put("month" + j, data.getShortname());
			j++;
			tinmap.put("amt" + i, data.getTaxesAsPerGSTR1().doubleValue());
			i++;
			tinmap.put("amt" + i, data.getOutwardTaxesandSupplies().doubleValue());
			i++;
			tinmap.put("amt" + i, data.getDifference().doubleValue());
			i++;
			tinmap.put("amt" + i, data.getPercentageofExcess().doubleValue());
			i++;
		}
     	 tinmap.put("panNo",reportMap.get("panNo"));
     	 tinmap.put("tinNo",reportMap.get("tinNo"));
     	 tinmap.put("company",reportMap.get("company"));
     	 tinmap.put("date",reportMap.get("date"));
      tinmap.put("image","http://dev.easemygst.com/assets/images/Emailer/gst-logo.png");
      tinmap.put("size",comparisonDTOlist.size());
      tinmap.put("username",reportMap.get("username"));
  	  tinmap.put("date",reportMap.get("date"));
	return tinmap;
		
	}


	public  static Map<String, Object>  getMap(Map<String, Object> reportMap,String key){
		
		Map<String, Object> panmap=new HashMap<>();
		  List<ComparisonDTO> pandetails=(List<ComparisonDTO>) reportMap.get(key);
		  int i=1,j=1;
    	for (ComparisonDTO data : pandetails) {
			panmap.put("month" + j, data.getShortname());
			j++;
			panmap.put("amt" + i, data.getTaxesAsPerGSTR1().doubleValue());
			i++;
			panmap.put("amt" + i, data.getOutwardTaxesandSupplies().doubleValue());
			i++;
			panmap.put("amt" + i, data.getDifference().doubleValue());
			i++;
			panmap.put("amt" + i, data.getPercentageofExcess().doubleValue());
			i++;
		  
	}
    panmap.put("image","http://dev.easemygst.com/assets/images/Emailer/gst-logo.png");
    panmap.put("panNo",reportMap.get("panNo"));
	panmap.put("date",reportMap.get("date"));
    panmap.put("company",reportMap.get("company"));
	panmap.put("size",pandetails.size());
	panmap.put("username",reportMap.get("username"));
	panmap.put("date",reportMap.get("date"));
	return panmap;
	}
	
	
	@SuppressWarnings("deprecation")
	public static String filegenration(List<JasperPrint> jasperPrintList,String filetype,Map<String, Object> reportMap,List<String> sheetNames) throws JRException{
		String destFile = null;
		if(filetype.equalsIgnoreCase("xlsx")){
			String fileName = "outward"+UUID.randomUUID()+".xlsx";
			destFile = ManipulateFile.getParentDirBasePath() + File.separator + fileName;
		JRXlsxExporter exporter = new JRXlsxExporter();
		exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
		exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, destFile);
		exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, false);
		exporter.setParameter(JRXlsExporterParameter.CREATE_CUSTOM_PALETTE, true);
		exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, false);
		exporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, false);
		exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, false);
		exporter.setParameter(JRXlsExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, true);
		exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, false);
		exporter.setParameter(JRXlsExporterParameter.SHEET_NAMES,sheetNames.toArray(new String[sheetNames.size()]));
		exporter.exportReport();
		}
		else if(filetype.equalsIgnoreCase("pdf")){
			String fileName = "outward"+UUID.randomUUID()+".pdf";
			destFile = ManipulateFile.getParentDirBasePath() + File.separator + fileName;
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList)); //Set as export input my list with JasperPrint s
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			configuration.setCreatingBatchModeBookmarks(true); //add this so your bookmarks work, you may set other parameters
			exporter.setConfiguration(configuration);
			exporter.exportReport();
		}
		return destFile;
	}
	
}
