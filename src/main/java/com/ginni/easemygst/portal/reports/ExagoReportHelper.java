package com.ginni.easemygst.portal.reports;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.Taxpayer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility.ROUNDINGMODE;
import com.mashape.unirest.http.JsonNode;

public class ExagoReportHelper {

	public static final String BASE_URL = "https://reporting.easemygst.com/ExagoWebApi";// "https://reports.easemygst.com/api";

	public static final String APP_BASE_URL = "https://reporting.easemygst.com/Exago/";// "https://reports.easemygst.com/exago/";

	public static final String CREATE_SESSION_URL = "/rest/Sessions";

	public static final String REST_ROLES_URL = "/rest/roles";

	public static final String SET_PARAMETERS_URL = "/rest/parameters";

	public String initUrl = "https://reporting.easemygst.com/Exago/ExagoHome.aspx";

	public String appUrl = "https://reporting.easemygst.com/Exago/ExagoHome.aspx";

	public Map<String, String> getHeaders() {

		Map<String, String> headers = new HashMap<>();
		headers.put("Authorization", "Basic ZW1nc3RfcmVwb3J0czplbWdzdF9yZXBvcnRz");
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");

		return headers;
	}

	public void generateForm3B(int userId, int orgId, String monthYear) throws SQLException {

		try (Connection connection = DBUtils.getConnection();
				CallableStatement item3b = connection.prepareCall("{call generate_form3b_new(?,?,?)}");

		) {

			item3b.setInt(1, userId);
			item3b.setInt(2, orgId);
			item3b.setString(3, monthYear);

			if (item3b.execute()) {
				System.out.println("form 3b generated successfully for " + userId);
			}

		} catch (Exception e) {
			throw e;
		}

	}

	// @PostPersist
	public void runInBackgroud(int userId, int orgId, String monthYear) throws IOException {

		ExecutorService executorService = Executors.newSingleThreadExecutor();

		executorService.execute(new Runnable() {

			@Override
			public void run() {
				try {
					generateForm3B(userId, orgId, monthYear);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

	}

	public String getReportUrl(UserDTO userDTO, String monthYear) throws AppException {

		// runInBackgroud(userDTO.getId(), userDTO.getOrganisation().getId(),
		// monthYear);
		try {
			generateForm3B(userDTO.getId(), userDTO.getOrganisation().getId(), monthYear);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "OK";
	}

	public String getReportUrl(UserDTO userDTO, boolean isAdmin) throws AppException {

		int apiCount = -1;

		String sId = "";

		com.ginni.easemygst.portal.persistence.entity.Calendar customCalendar = null;

			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			if (month == 0)
				month = 12;
			String mmyyyy = String.format("%02d", month) + String.format("%04d", year);
			customCalendar = AppConfig.getCalendarByCode(mmyyyy);
			//runInBackgroud(userDTO.getId(), userDTO.getOrganisation().getId(), mmyyyy);
			if (Objects.isNull(customCalendar)) {
				mmyyyy = "072017";
				customCalendar = AppConfig.getCalendarByCode(mmyyyy);
			}

		/*
		 * try { generateForm3B(userDTO.getId(),
		 * userDTO.getOrganisation().getId()); } catch (SQLException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 */

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("configFn", "WebReports.xml");

		Map<String, String> headers = getHeaders();

		String output = RestClient.callApi(BASE_URL + CREATE_SESSION_URL, "POST", new JsonNode(jsonObject.toString()),
				headers);
		JSONObject outputJson = new JSONObject(output);
		if (!outputJson.has("status_cd")) {
			if (outputJson.has("AppUrl")) {
				appUrl = outputJson.getString("AppUrl");
				apiCount++;
			}
			if (outputJson.has("Id")) {
				sId = outputJson.getString("Id");
				apiCount++;
			}
		} else {
			// invalid response. please check logs for more information.
		}

		if (!StringUtils.isEmpty(sId)) {

			String role = "Secondary";
			if (isAdmin) {
				role = "SYSTEM";
			} else {
				if (!Objects.isNull(userDTO.getOrganisation())) {
					if (!StringUtils.isEmpty(userDTO.getOrganisation().getCode())
							&& userDTO.getOrganisation().getCode().contains("E")) {
						role = "Primary";
					}
				}

				if (userDTO.getUsername().equals("livetest.emg@gmail.com")
						|| userDTO.getUsername().equals("livetest.emg+debugmode@gmail.com")) {
					role = "SYSTEM";
				}
			}

			// set ROLE as Standard and Enterprise
			JSONObject paramObject1 = new JSONObject();
			paramObject1.put("IsActive", true);

			String outputRole = RestClient.callApi(BASE_URL + REST_ROLES_URL + "/" + role + "?sid=" + sId, "PATCH",
					new JsonNode(paramObject1.toString()), headers);
			JSONObject outputRoleJson = new JSONObject(outputRole);
			if (!outputRoleJson.has("status_cd")) {
				if (outputRoleJson.has("flag")) {
					apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}

			JSONObject paramObject = new JSONObject();
			paramObject.put("IsHidden", "false");
			paramObject.put("Id", "userid");
			paramObject.put("Value", userDTO.getId());

			// set USER ID of the user
			String outputUser = RestClient.callApi(BASE_URL + SET_PARAMETERS_URL + "/userid?sid=" + sId, "PATCH",
					new JsonNode(paramObject.toString()), headers);
			JSONObject outputUserJson = new JSONObject(outputUser);
			if (!outputUserJson.has("status_cd")) {
				if (outputUserJson.has("flag")) {
					apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}

			JSONObject paramAccObject = new JSONObject();
			paramAccObject.put("IsHidden", "false");
			paramAccObject.put("Id", "accountid");
			paramAccObject.put("Value", userDTO.getOrganisation().getId());

			// set USER ID of the user
			String outputAccount = RestClient.callApi(BASE_URL + SET_PARAMETERS_URL + "/accountid?sid=" + sId, "PATCH",
					new JsonNode(paramAccObject.toString()), headers);
			JSONObject outputAccJson = new JSONObject(outputAccount);
			if (!outputAccJson.has("status_cd")) {
				if (outputAccJson.has("flag")) {
					apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}
			
			JSONObject paramGiniObject = new JSONObject();
			paramGiniObject.put("IsHidden", "false");
			paramGiniObject.put("Id", "ginesysid");
			
			try {
				FinderService finderService = (FinderService) InitialContext.doLookup("java:global/easemygst/FinderServiceImpl");
				paramGiniObject.put("Value",finderService.findProvider(userDTO.getOrganisation().getId()));
				}
			 catch (NamingException e) {
				e.printStackTrace();
			}
		

			// set USER ID of the user
			String outputGini = RestClient.callApi(BASE_URL + SET_PARAMETERS_URL + "/ginesysid?sid=" + sId, "PATCH",
					new JsonNode(paramGiniObject.toString()), headers);
			JSONObject outputGiniJson = new JSONObject(outputGini);
			if (!outputGiniJson.has("status_cd")) {
				if (outputGiniJson.has("flag")) {
					//apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}

		}

		return apiCount == 4 ? sId : null;
	}

	public Map<String,Object> getReportUrlWithParam(UserDTO userDTO, boolean isAdmin, String gstin, String mmyyyy)
			throws AppException {

		String sId = getReportUrl(userDTO, isAdmin);
		
		String url=null;
		
		String reportMsg="The report displayed is as on %s at %s. Next Report will be generated on %s.";
		
		Map<String,Object> responseMap = new HashMap<>();

		if (!StringUtils.isEmpty(sId)) {

			TaxpayerGstin taxpayerGstin = null;
			String panName = "";
			String gstinName = "";
			
			Map<String, String> headers = getHeaders();

			if (StringUtils.isEmpty(gstin)) {
				List<UserGstin> gstins = new ArrayList<>();
				try {
					FinderService finderService = (FinderService) InitialContext
							.doLookup("java:global/easemygst/FinderServiceImpl");
					gstins = finderService.getUserGstinByUserOrg(userDTO.getId(), userDTO.getOrganisation().getId());
					if (!gstins.isEmpty()) {
						taxpayerGstin = gstins.get(0).getTaxpayerGstin();
						gstinName = taxpayerGstin.getDisplayName();
						Taxpayer taxpayer = finderService.findTaxpayer(taxpayerGstin.getGstin().substring(2, 12));
						panName = taxpayer.getLegalName();
					}
				} catch (NamingException | AppException e) {
					e.printStackTrace();
				}
			} else {
				FinderService finderService;
				try {
					finderService = (FinderService) InitialContext.doLookup("java:global/easemygst/FinderServiceImpl");
					taxpayerGstin = finderService.findTaxpayerGstin(gstin);
					if (!Objects.isNull(taxpayerGstin)) {
						gstinName = taxpayerGstin.getDisplayName();
						Taxpayer taxpayer = finderService.findTaxpayer(taxpayerGstin.getGstin().substring(2, 12));
						panName = taxpayer.getLegalName();
					}
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}

			JSONObject paramPanObject = new JSONObject();
			paramPanObject.put("IsHidden", "false");
			paramPanObject.put("Id", "panName");
			paramPanObject.put("Value", panName);

			// set USER ID of the user
			String outputPan = RestClient.callApi(BASE_URL + SET_PARAMETERS_URL + "/panName?sid=" + sId, "PATCH",
					new JsonNode(paramPanObject.toString()), headers);
			JSONObject outputPanJson = new JSONObject(outputPan);
			if (!outputPanJson.has("status_cd")) {
				if (outputPanJson.has("flag")) {
					// apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}

			JSONObject paramGstinObject = new JSONObject();
			paramGstinObject.put("IsHidden", "false");
			paramGstinObject.put("Id", "gstinName");
			paramGstinObject.put("Value", gstinName);

			// set USER ID of the user
			String outputGstin = RestClient.callApi(BASE_URL + SET_PARAMETERS_URL + "/gstinName?sid=" + sId, "PATCH",
					new JsonNode(paramGstinObject.toString()), headers);
			JSONObject outputGstinJson = new JSONObject(outputGstin);
			if (!outputGstinJson.has("status_cd")) {
				if (outputGstinJson.has("flag")) {
					// apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}
			
			String my = "";
			if(!StringUtils.isEmpty(mmyyyy)) {
				com.ginni.easemygst.portal.persistence.entity.Calendar customCalendar = AppConfig.getCalendarByCode(mmyyyy);
				if(!Objects.isNull(customCalendar)) {
					my = customCalendar.getMonthName();
				}
			}

			JSONObject paramMyObject = new JSONObject();
			paramMyObject.put("IsHidden", "false");
			paramMyObject.put("Id", "monthYear");
			paramMyObject.put("Value", my);

			// set USER ID of the user
			String outputMy = RestClient.callApi(BASE_URL + SET_PARAMETERS_URL + "/monthYear?sid=" + sId, "PATCH",
					new JsonNode(paramMyObject.toString()), headers);
			JSONObject outputMyJson = new JSONObject(outputMy);
			if (!outputMyJson.has("status_cd")) {
				if (outputMyJson.has("flag")) {
					// apiCount++;
				}
			} else {
				// invalid response. please check logs for more information.
			}

			url= APP_BASE_URL + appUrl;

		} else {

			url= initUrl;
		}
		
		responseMap.put("url",url);
		responseMap.put("msg",
				String.format(reportMsg,
						LocalDate.now().format(java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy"))
						,CalanderCalculatorUtility.getCurrentTimeWithRounding(ROUNDINGMODE.HALF_HOUR_DOWN).format(java.time.format.DateTimeFormatter.ofPattern("HH:mm")),
						CalanderCalculatorUtility.getCurrentTimeWithRounding(ROUNDINGMODE.HALF_HOUR_UP).format(java.time.format.DateTimeFormatter.ofPattern("HH:mm"))));
		
	return responseMap;
	}

}
