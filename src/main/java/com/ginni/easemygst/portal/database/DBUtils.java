package com.ginni.easemygst.portal.database;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DBUtils {

	private static InitialContext ctx = null;
	private static DataSource dataSource = null;
	private static Properties dbconfig = null;

	private static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	static {

		try {
			loadDBConfigProperties();
			log.info("Static block started of DBUtils");
			ctx = new InitialContext();
			dataSource = (DataSource) ctx.lookup(dbconfig.getProperty("dsname"));
			log.info("Static Block Ends.");
		} catch (NamingException ex) {
			log.error(ex.getMessage());
		} catch (IOException e) {
			log.error("Unable to load dbconfig.properties.");
		}
	}

	private static void loadDBConfigProperties() throws IOException {
		dbconfig = new Properties();
		dbconfig.load(DBUtils.class.getClassLoader().getResourceAsStream("dbconfig.properties"));
		log.info("dbconfig.properties loaded successfully");

	}

	/**
	 * Close a connection; avoid closing if null, and report any SQLExceptions
	 * that occur.
	 * 
	 * @param conn
	 *            a java.sql.Connection object.
	 * @throws SQLException
	 *             failed to close the connection
	 */
	public static void close(Connection conn) throws SQLException {
		if (conn == null) {
			return;
		}
		if (!conn.isClosed()) {
			conn.close();
		}
	}

	/**
	 * Close the Connection object. Releases the Connection object's database
	 * and JDBC resources immediately instead of waiting for them to be
	 * automatically released.
	 * 
	 * @param conn
	 *            a Connection object.
	 */

	public static void closeQuietly(Connection conn) {
		if (conn == null) {
			return;
		}
		try {
			close(conn);
		} catch (SQLException e) {
			/* Quiet */}
	}

	/**
	 * Close a Statement, avoid closing if null.
	 *
	 * @param stmt
	 *            Statement to close.
	 * @throws SQLException
	 *             if a database access error occurs
	 */

	public static void close(Statement stmt) throws SQLException {
		if (stmt != null) {
			stmt.close();
		}
	}

	/**
	 * Close the Statement object. Releases the Statement object's database and
	 * JDBC resources immediately instead of waiting for them to be
	 * automatically released.
	 * 
	 * @param stmt
	 *            a Statement object.
	 */
	public static void closeQuietly(Statement stmt) {
		if (stmt == null) {
			return;
		}
		try {
			close(stmt);

		} catch (Exception ignore) {
			/* Quiet */}
	}

	/**
	 * Close the ResultSet object. Releases the ResultSet object's database and
	 * JDBC resources immediately instead of waiting for them to be
	 * automatically released.
	 * 
	 * @param rs
	 *            a ResultSet object.
	 * @throws SQLException
	 */
	public static void close(ResultSet rs) throws SQLException {
		if (rs == null) {
			return;
		}
		rs.close();
	}

	/**
	 * Close the ResultSet object. Releases the ResultSet object's database and
	 * JDBC resources immediately instead of waiting for them to be
	 * automatically released.
	 * 
	 * @param rs
	 *            a ResultSet object.
	 */
	public static void closeQuietly(ResultSet rs) {
		if (rs == null) {
			return;
		}
		try {
			close(rs);
		} catch (SQLException e) {
			/* Quiet */}
	}

	/**
	 * Close a Connection, Statement and ResultSet. Avoid closing if null and
	 * hide any SQLExceptions that occur.
	 *
	 * @param conn
	 *            Connection to close.
	 * @param stmt
	 *            Statement to close.
	 * @param rs
	 *            ResultSet to close.
	 */
	public static void closeQuietly(Connection conn, Statement stmt, ResultSet rs) {

		try {
			closeQuietly(rs);
		} finally {
			try {
				closeQuietly(stmt);
			} finally {
				closeQuietly(conn);
			}
		}
	}

	public static Connection getConnection() throws SQLException {

		log.info("requesting new connection...");
		if (ctx == null) {
			try {
				ctx = new InitialContext();
			} catch (NamingException e) {
				log.error(e.getMessage());
			}
		}
		if (dataSource == null) {
			try {
				dataSource = (DataSource) ctx.lookup(dbconfig.getProperty("dsname"));
			} catch (NamingException e) {
				log.error(e.getMessage());
			}
		}

		return dataSource == null ? null : DBUtils.setIsolation(dataSource.getConnection());
	}
	
	private static Connection setIsolation(Connection connection) {
		try {
			 connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

	public static DataSource getDataSource() {
		return dataSource;
	}

	
}
