package com.ginni.easemygst.portal.database;


import java.util.Date;
import java.util.List;
import java.util.Map;

public abstract class BaseEntity<T> {
	
	protected List<T> dataList;
	
	public abstract String getPrimaryKey();
	
	public abstract String getForeignKey();
	
	public Map<String, String> data;
	
	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}


	public String getCurrentTimestamp() {
		Date today = new Date();
	    return new java.sql.Timestamp(today.getTime()).toString();
	}
}