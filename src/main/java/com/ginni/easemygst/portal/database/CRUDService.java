package com.ginni.easemygst.portal.database;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author Sumeet
 *
 */
@SuppressWarnings("rawtypes")
public class CRUDService<T extends BaseEntity<T>> {

	private static String CLASS_NAME = CRUDService.class.getSimpleName();

	private Logger log = Logger.getLogger(getClass().getName());

	/**
	 * Use this method execute insert query on DB Query = INSERT INTO
	 * <TABLE_NAME> (Parameter1, Parameter2) VALUES (Value1, Value2)
	 *
	 * @param t
	 * @return
	 */

	public T create(T t) {

		log.debug(CLASS_NAME + "create method start...");

		StringBuilder sqlInsert = new StringBuilder("INSERT INTO ");
		List<String> key = new ArrayList<String>();
		List<String> value = new ArrayList<String>();

		String tTableName = t.getClass().getSimpleName().toLowerCase();
		Field[] tField = t.getClass().getDeclaredFields();

		for (Field field : tField) {
			String val = null;
			try {
				val = (String) field.get(t);
			} catch (IllegalAccessException e) {
				log.error(e.getMessage());

			}
			if (val != null && !val.isEmpty()) {
				key.add(field.getName());
				try {
					value.add(val);
				} catch (IllegalArgumentException e) {
					log.error(e.getMessage());
				}
			}
		}

		sqlInsert.append(tTableName + " ");

		StringBuilder keyString = new StringBuilder("(");
		StringBuilder valueString = new StringBuilder("(");

		for (int i = 0; i < key.size(); i++) {
			if (i > 0) {
				keyString.append(", ");
				valueString.append(", ");
			}
			keyString.append(key.get(i));
			valueString.append("'" + value.get(i) + "'");
		}
		keyString.append(")");
		valueString.append(")");

		sqlInsert.append(keyString);
		sqlInsert.append(" VALUES ");
		sqlInsert.append(valueString);

		/*
		 * if (dbConnect.getConnectionStatus()) { connection =
		 * dbConnect.openConnection();
		 *
		 * } else { throw new InstantiationException(); }
		 */
		Connection conn = null;
		Statement stm = null;

		try {
			conn = DBUtils.getConnection();
			log.info("query generated = " + sqlInsert);
			stm = conn.createStatement();
			stm.executeUpdate(sqlInsert.toString());

		} catch (SQLException e) {
			log.error("db exception " + e.toString());
		} finally {
			DBUtils.closeQuietly(stm);
			DBUtils.closeQuietly(conn);
		}

		log.error(CLASS_NAME + "create method ends...");

		return t;

	}

	/**
	 * Use this method execute insert query on DB Query = INSERT INTO
	 * <TABLE_NAME> (Parameter1, Parameter2) VALUES (Value1, Value2)
	 *
	 * @param t
	 * @return
	 */

	public T replace(T t) {

		log.debug(CLASS_NAME + "replace method start...");

		StringBuilder sqlInsert = new StringBuilder("REPLACE INTO ");
		List<String> key = new ArrayList<String>();
		List<String> value = new ArrayList<String>();

		String tTableName = t.getClass().getSimpleName().toLowerCase();
		Field[] tField = t.getClass().getDeclaredFields();

		for (Field field : tField) {
			String val = null;
			try {
				val = (String) field.get(t);
			} catch (IllegalAccessException e) {
				log.error(e.getMessage());

			}
			if (val != null && !val.isEmpty()) {
				key.add(field.getName());
				try {
					value.add(val);
				} catch (IllegalArgumentException e) {
					log.error(e.getMessage());
				}
			}
		}

		sqlInsert.append(tTableName + " ");

		StringBuilder keyString = new StringBuilder("(");
		StringBuilder valueString = new StringBuilder("(");

		for (int i = 0; i < key.size(); i++) {
			if (i > 0) {
				keyString.append(", ");
				valueString.append(", ");
			}
			keyString.append(key.get(i));
			valueString.append("'" + value.get(i) + "'");
		}
		keyString.append(")");
		valueString.append(")");

		sqlInsert.append(keyString);
		sqlInsert.append(" VALUES ");
		sqlInsert.append(valueString);

		/*
		 * if (dbConnect.getConnectionStatus()) { connection =
		 * dbConnect.openConnection();
		 *
		 * } else { throw new InstantiationException(); }
		 */
		Connection conn = null;
		Statement stm = null;

		try {
			conn = DBUtils.getConnection();
			log.info("query generated = " + sqlInsert);
			stm = conn.createStatement();
			stm.executeUpdate(sqlInsert.toString());

		} catch (SQLException e) {
			log.error("db exception " + e.toString());
		} finally {
			DBUtils.closeQuietly(stm);
			DBUtils.closeQuietly(conn);
		}

		log.error(CLASS_NAME + "replace method ends...");

		return t;

	}

	/**
	 * Use this method execute select query on DB Query = SELECT Parameter1,
	 * Parameter2 FROM <TABLE_NAME>
	 *
	 * @param t
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T read(T t) {

		log.debug(CLASS_NAME + "read method starts...");

		Connection conn = null;
		Statement stm = null;
		ResultSet rs = null;

		StringBuilder sqlSelect = new StringBuilder("SELECT *");

		String tTableName = t.getClass().getSimpleName().toLowerCase();
		Field[] tField = t.getClass().getDeclaredFields();

		sqlSelect.append(" FROM ");
		sqlSelect.append(tTableName);

		/*
		 * try { dbConnect = DBConnect.getInstance(); if
		 * (dbConnect.getConnectionStatus()) { connection =
		 * dbConnect.openConnection();
		 *
		 * } else { throw new InstantiationException(); }
		 */

		try {

			conn = DBUtils.getConnection();
			stm = conn.createStatement();
			rs = stm.executeQuery(sqlSelect.toString());

			List<T> resultList = new ArrayList<T>();
			T obj = null;

			while (rs.next()) {
				obj = (T) t.getClass().newInstance();
				for (Field field : tField) {
					field.set(obj, rs.getString(field.getName()));
				}

				resultList.add(obj);
			}

			Class[] args = { List.class };
			t.getClass().getMethod("setDataList", args).invoke(t, resultList);
		} catch (Exception e) {
			log.error("db exception " + e.toString());
		} finally {
			DBUtils.closeQuietly(conn, stm, rs);
		}
		log.debug(CLASS_NAME + "read method ends...");
		return t;
	}

	/**
	 * Use this method execute update query on DB Query = UPDATE
	 * <TABLE_NAME> SET Parameter1=Value1 WHERE <PRIMARY_KEY>=Value
	 *
	 * @param t
	 * @return
	 */
	public T update(T t) {

		log.debug(CLASS_NAME + "update method starts...");

		Connection conn = null;
		Statement stm = null;

		StringBuilder sqlUpdate = new StringBuilder("UPDATE ");
		List<String> key = new ArrayList<String>();
		List<String> value = new ArrayList<String>();

		String tTableName = t.getClass().getSimpleName().toLowerCase();
		Field[] tField = t.getClass().getDeclaredFields();

		for (Field field : tField) {
			key.add(field.getName());
			try {
				value.add((String) field.get(t));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				log.error(e.getMessage());
			}
		}

		sqlUpdate.append(tTableName);
		sqlUpdate.append(" SET ");

		StringBuilder setString = new StringBuilder();

		int count = 0;
		for (int i = 0; i < key.size(); i++) {
			if (value.get(i) != null && !value.get(i).isEmpty() && value.get(i) != "") {
				if (count > 0) {
					setString.append(", ");
				}
				setString.append(key.get(i));
				setString.append("='");
				setString.append(value.get(i));
				setString.append("'");
				count++;
			}
		}

		sqlUpdate.append(setString);
		sqlUpdate.append(" WHERE ");

		String pKeyValue = "";
		try {
			pKeyValue = (String) t.getClass().getField(t.getPrimaryKey()).get(t);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			log.error(e.getMessage());

		}

		sqlUpdate.append(t.getPrimaryKey());
		sqlUpdate.append("=");
		sqlUpdate.append("'" + pKeyValue + "'");

		log.info("query generated = " + sqlUpdate);
		try {
			conn = DBUtils.getConnection();
			/*
			 * dbConnect = DBConnect.getInstance(); if
			 * (dbConnect.getConnectionStatus()) { connection =
			 * dbConnect.openConnection();
			 *
			 * } else { throw new InstantiationException(); }
			 */

			stm = conn.createStatement();
			stm.executeUpdate(sqlUpdate.toString());

		} catch (Exception e) {
			log.error("db exception " + e.toString());
		} finally {
			DBUtils.closeQuietly(stm);
			DBUtils.closeQuietly(conn);
		}

		log.debug(CLASS_NAME + "update method ends...");

		return t;
	}

	/**
	 * Use this method execute delete query on DB
	 *
	 * @param t
	 * @return
	 */
	public T delete(T t) {

		Connection conn = null;
		Statement stm = null;

		log.debug(CLASS_NAME + "delete method starts...");

		StringBuilder sqlUpdate = new StringBuilder("DELETE FROM ");

		String tTableName = t.getClass().getSimpleName().toLowerCase();

		sqlUpdate.append(tTableName);
		sqlUpdate.append(" WHERE ");

		String pKeyValue = "";
		try {
			pKeyValue = (String) t.getClass().getField(t.getPrimaryKey()).get(t);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			log.error(e.getMessage());
		}

		sqlUpdate.append(t.getPrimaryKey());
		sqlUpdate.append("=");
		sqlUpdate.append("'" + pKeyValue + "'");

		log.info("query generated = " + sqlUpdate);

		try {
			conn = DBUtils.getConnection();
			/*
			 * dbConnect = DBConnect.getInstance(); if
			 * (dbConnect.getConnectionStatus()) { connection =
			 * dbConnect.openConnection();
			 *
			 * } else { throw new InstantiationException(); }
			 */
			stm = conn.createStatement();
			stm.executeUpdate(sqlUpdate.toString());

		} catch (Exception e) {
			log.info("error on delete .. cause: "+e.getMessage());
		}finally {
			DBUtils.closeQuietly(stm);
			DBUtils.closeQuietly(conn);
		}
		log.debug(CLASS_NAME + "delete method ends...");

		return t;
	}

	/**
	 * Use this method execute select query by primary key on DB Query = SELECT
	 * Parameter1, Parameter2 FROM <TABLE_NAME> WHERE <PRIMARY_KEY>='VALUE'
	 *
	 * @param t
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T readById(T t) {

		Connection conn = null;
		Statement stm = null;
		ResultSet rs = null;

		log.debug(CLASS_NAME + "readById method starts...");

		StringBuilder sqlSelect = new StringBuilder("SELECT *");

		String tTableName = t.getClass().getSimpleName().toLowerCase();
		Field[] tField = t.getClass().getDeclaredFields();

		sqlSelect.append(" FROM ");
		sqlSelect.append(tTableName);
		sqlSelect.append(" WHERE ");

		String pKeyValue = "";
		try {
			pKeyValue = (String) t.getClass().getField(t.getPrimaryKey()).get(t);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			log.error(e.getMessage());
		}

		sqlSelect.append(t.getPrimaryKey());
		sqlSelect.append("=");
		sqlSelect.append("'" + pKeyValue + "'");

		log.info("query generated = " + sqlSelect);
		try {

			conn = DBUtils.getConnection();
			/*
			 * dbConnect = DBConnect.getInstance(); if
			 * (dbConnect.getConnectionStatus()) { connection =
			 * dbConnect.openConnection();
			 *
			 * } else { throw new InstantiationException(); }
			 */

			stm = conn.createStatement();
			rs = stm.executeQuery(sqlSelect.toString());

			List<T> resultList = new ArrayList<T>();

			T obj = null;
			while (rs.next()) {
				obj = (T) t.getClass().newInstance();

				for (Field field : tField) {
					field.set(obj, rs.getString(field.getName()));
				}

				resultList.add(obj);
			}

			Class[] args = { List.class };
			t.getClass().getMethod("setDataList", args).invoke(t, resultList);

		} catch (Exception e) {
			log.error(e.getMessage());

		} finally {
			DBUtils.closeQuietly(conn, stm, rs);
		}

		log.error("readById method ends...");
		return t;
	}

	/**
	 * Use this method execute select query by any param on DB Query = SELECT
	 * Parameter1, Parameter2 FROM <TABLE_NAME> WHERE <PARAM_NAME>='VALUE'
	 *
	 * @param t
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T readByName(T t, String... param) {

		log.debug(CLASS_NAME + "readByName method starts...");

		Connection conn = null;
		Statement stm = null;
		ResultSet rs = null;

		StringBuilder sqlSelect = new StringBuilder("SELECT *");

		String tTableName = t.getClass().getSimpleName().toLowerCase();
		Field[] tField = t.getClass().getDeclaredFields();

		sqlSelect.append(" FROM ");
		sqlSelect.append(tTableName);
		sqlSelect.append(" WHERE ");

		int count = 0;
		for (int i = 0; i < param.length; i++) {
			String pKeyValue = "";
			try {
				pKeyValue = (String) t.getClass().getField(param[i]).get(t);
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				log.error(e.getMessage());
			}

			if (pKeyValue != null && pKeyValue != "") {
				if (count > 0) {
					sqlSelect.append(" and ");
				}
				sqlSelect.append(param[i]);
				sqlSelect.append("=");
				sqlSelect.append("'" + pKeyValue + "'");
				count++;
			}
		}

		log.info("query generated = " + sqlSelect);
		try {

			conn = DBUtils.getConnection();
			/*
			 * dbConnect = DBConnect.getInstance(); if
			 * (dbConnect.getConnectionStatus()) { connection =
			 * dbConnect.openConnection();
			 *
			 * } else { throw new InstantiationException(); }
			 */

			stm = conn.createStatement();
			rs = stm.executeQuery(sqlSelect.toString());

			List<T> resultList = new ArrayList<T>();
			T obj = null;
			while (rs.next()) {
				obj = (T) t.getClass().newInstance();

				for (Field field : tField) {
					field.set(obj, rs.getString(field.getName()));
				}

				resultList.add(obj);
			}

			Class[] args = { List.class };
			t.getClass().getMethod("setDataList", args).invoke(t, resultList);

		} catch (Exception e) {
			log.error("exception generated at = " + e.getMessage());
		} finally {
			DBUtils.closeQuietly(conn, stm, rs);
		}
		log.debug(CLASS_NAME + "create method ends...");

		return t;
	}

	/**
	 *
	 * Query =
	 *
	 * @param t
	 * @return
	 */
	public long count(T t) {

		return 0;
	}

	/**
	 *
	 * Query = Select Builder
	 *
	 * @param t,
	 *            query
	 * @return List
	 */
	@SuppressWarnings("unchecked")
	public T executeQuery(T t, String query) {

		log.debug(CLASS_NAME + "executeQuery method starts...");

		Connection conn = null;
		Statement stm = null;
		ResultSet rs = null;

		List<T> resultList = new ArrayList<T>();
		try {

			Field[] tField = t.getClass().getDeclaredFields();
			/*
			 * dbConnect = DBConnect.getInstance(); if
			 * (dbConnect.getConnectionStatus()) { connection =
			 * dbConnect.openConnection();
			 *
			 * } else { throw new InstantiationException(); }
			 */

			conn = DBUtils.getConnection();

			stm = conn.createStatement();
			rs = stm.executeQuery(query);

			T obj = null;
			while (rs.next()) {
				obj = (T) t.getClass().newInstance();

				for (Field field : tField) {
					field.set(obj, rs.getString(field.getName()));
				}

				resultList.add(obj);
			}

			Class[] args = { List.class };
			t.getClass().getMethod("setDataList", args).invoke(t, resultList);

		} catch (Exception e) {
			log.error("db exception " + e.toString());
		} finally {
			DBUtils.closeQuietly(conn, stm, rs);
		}
		log.debug(CLASS_NAME + "executeQuery  method ends...");

		return t;
	}

	/**
	 *
	 * Query = Update Query
	 *
	 * @param t,
	 *            query
	 * @return List
	 */
	public void executeUpdate(String query) {

		log.debug(CLASS_NAME + "executeUpdate method starts...");

		Connection conn = null;
		Statement stm = null;

		//List<T> resultList = new ArrayList<T>();
		try {

			//Field[] tField = t.getClass().getDeclaredFields();
			/*
			 * dbConnect = DBConnect.getInstance(); if
			 * (dbConnect.getConnectionStatus()) { connection =
			 * dbConnect.openConnection();
			 *
			 * } else { throw new InstantiationException(); }
			 */

			conn = DBUtils.getConnection();

			stm = conn.createStatement();
		    stm.executeUpdate(query);

		} catch (Exception e) {
			log.error("db exception " + e.toString());
		} finally {
			DBUtils.closeQuietly(stm);
			DBUtils.closeQuietly(conn);
		}
		log.debug(CLASS_NAME + "executeUpdate  method ends...");

		//return t;
	}
}