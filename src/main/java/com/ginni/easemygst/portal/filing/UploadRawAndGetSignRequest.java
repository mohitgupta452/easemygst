package com.ginni.easemygst.portal.filing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Data;

/**
 * Created by sumeet
 */
@JsonInclude(Include.NON_NULL)
public @Data class UploadRawAndGetSignRequest {

	@JsonProperty("TimeStamp")
    private String TimeStamp;
    
	@JsonProperty("Reference_ID")
    private String Reference_ID;
    
	@JsonProperty("JSON_String")
    private String JSON_String;
    
}
