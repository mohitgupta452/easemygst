package com.ginni.easemygst.portal.filing;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Data;

/**
 * 
 * @author Sumeet
 *
 */
@JsonInclude(Include.NON_NULL)
public @Data class DocumentMeta {

	@JsonProperty("status_cd")
	private String status_cd;
	
	private String status;
	
	private String result;
	
	private String Reference_ID;
	
	private String PayLoad_ID;
	
	private String TimeStamp;
	
	private String Error;
	
	private String ErrorCode;
	
	private String Transaction_ID;

}
