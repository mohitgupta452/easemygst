package com.ginni.easemygst.portal.filing;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Created by sumeet
 */
@JsonInclude(Include.NON_NULL)
public @Data class DownloadedData {
    
	@JsonProperty("status_cd")
	private String status_cd;
	
	String content;
    
	String pkcs7;

    String code;
    
    String message;
    
    String details;

}
