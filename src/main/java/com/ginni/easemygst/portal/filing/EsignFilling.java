package com.ginni.easemygst.portal.filing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import javax.enterprise.context.RequestScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.net.ntp.TimeStamp;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.dto.GstrFlowDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.gst.api.GspFactory;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.mashape.unirest.http.JsonNode;

@RequestScoped
public class EsignFilling {

	static Properties config;
	
	private static Logger log=LoggerFactory.getLogger(EsignFilling.class);
	
	private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");

	static {
		try {
			config = new Properties();
			config.load(GspFactory.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Transform Calendar to ISO 8601 string. */
	public static String fromCalendar(final Calendar calendar) {
		Date date = calendar.getTime();
		String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
		return formatted;// formatted.substring(0, 22) + ":" +
							// formatted.substring(22);
	}

	public static String callApi(String requestUrl, String input) {
		try {

			URL url = new URL(requestUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			System.out.println(config.getProperty("main.legal.desk.api.key"));
			System.out.println(config.getProperty("main.legal.desk.api.id"));
			conn.setRequestProperty("X-Parse-REST-API-Key", config.getProperty("main.legal.desk.api.key"));
			conn.setRequestProperty("X-Parse-Application-Id", config.getProperty("main.legal.desk.api.id"));
			System.out.println(input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			/*if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}*/

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				conn.disconnect();
				return output;
			}
			
			conn.disconnect();
			
			return "";

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return "";
	}

	public static String uploadData(String gstin,String monthYear,String returnType,String content) throws Exception {

		UploadRawAndGetSignRequest request = new UploadRawAndGetSignRequest();
		//String content = null;//"{\"gstin\":\"37ABCDE9552F3Z4\",\"ret_pd\":\"112016\",\"checksum\":\"AflJufPlFStqKBZ\",\"ttl_inv\":1000.00,\"ttl_tax\":500.00,\"ttl_igst\":124.99,\"ttl_sgst\":5589.87,\"ttl_cgst\":3423.0,\"sec_sum\":[{\"sec_nm\":\"b2b\",\"checksum\":\"AflJufPlFStqKBZ\",\"ttl_inv\":1000.00,\"ttl_tax\":500.00,\"ttl_igst\":124.99,\"ttl_sgst\":5589.87,\"ttl_cgst\":3423.0,\"cpty_sum\":[{\"ctin\":\"20GRRHF2562D3A3\",\"checksum\":\"AflJufPlFStqKBZ\",\"ttl_inv\":1000.00,\"ttl_tax\":500.00,\"ttl_igst\":124.99,\"ttl_sgst\":5589.87,\"ttl_cgst\":3423.0}]}]}";
		request.setJSON_String(content);

		// request.setReference_ID(Calendar.getInstance().getTimeInMillis()+"");
		// request.setTimeStamp("2017-08-30T21:29:18.000000");

		String requestJson = JsonMapper.objectMapper.writeValueAsString(request);

		JsonNode jsonNode = new JsonNode("");
		jsonNode.getObject().put("PayLoad",Base64.getEncoder().encodeToString(content.getBytes()));
		jsonNode.getObject().put("TimeStamp", "2017-08-31T14:25:18.000000");
		jsonNode.getObject().put("Reference_ID", Calendar.getInstance().getTimeInMillis() + "");

		Properties config = new Properties();
		try {
			config.load(GspFactory.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, String> headers = new HashMap<>();
		headers.put("X-Parse-REST-API-Key", config.getProperty("main.legal.desk.api.key"));
		headers.put("X-Parse-Application-Id", config.getProperty("main.legal.desk.api.id"));
		headers.put("Content-Type", "application/json");

		String url = config.getProperty("main.legal.desk.api.esign.request.url");
		try {
			String respo = callApi(url, jsonNode.getObject().toString());

			JSONObject jsonObject = new JSONObject(respo);

			if (jsonObject.has("PayLoad_ID")) {
				return jsonObject.getString("PayLoad_ID");
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getSignedData(String docId,String gstin,String monthYear,String returnType)throws AppException  {
		
		GstrFlowDTO gstrFlowDto=null;
		JsonNode jsonNode = new JsonNode("");
		jsonNode.getObject().put("PayLoad_ID",docId);
	
		
		jsonNode.getObject().put("TimeStamp", format.format(new Date()));
		
		Properties config = new Properties();
		try {
			config.load(GspFactory.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, String> headers = new HashMap<>();
		headers.put("X-Parse-REST-API-Key", config.getProperty("main.legal.desk.api.key"));
		headers.put("X-Parse-Application-Id", config.getProperty("main.legal.desk.api.id"));
		headers.put("Content-Type", "application/json");

		String url = config.getProperty("main.legal.desk.api.pkcs.request.url");
		try {
			String respo = callApi(url, jsonNode.getObject().toString());

			JSONObject jsonObject = new JSONObject(respo);
            System.out.println(respo);
			if (jsonObject.has("PKCS7")) {
				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				TaxpayerGstinDTO gstinDTO=new TaxpayerGstinDTO();
				gstinDTO.setGstin(gstin);
				gstinTransactionDTO.setTaxpayerGstin(gstinDTO);
				gstinTransactionDTO.setMonthYear(monthYear);
				gstinTransactionDTO.setReturnType(returnType);
				try {
					FunctionalService functionalService = (FunctionalService) InitialContext
							.doLookup("java:global/easemygst/FunctionalServiceImpl");
					gstrFlowDto=functionalService.fileGstr(gstinTransactionDTO,jsonObject.getString("PKCS7"));//need to channge
				} catch (NamingException | AppException e) {
					log.error("exception {}",e);
					throw e;
				}
				
				return Objects.nonNull(gstrFlowDto.getReferenceId())?gstrFlowDto.getReferenceId():null;
			} else {
				return null;
			}
		} 
		catch (AppException e) {
			throw e;
		
			
		}
		catch (Exception e) {
			e.printStackTrace();
		return null;
			
		}
		
	}

}
