package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;

import edu.umd.cs.findbugs.annotations.SuppressWarnings;

/**
 * 
 * @author Himanshu Agarwal
 */
public abstract class Excel {
	private final static Logger _LOGGER = LoggerFactory.getLogger(Excel.class);
	private String workBookName = null;
	protected Workbook wb = null;
	protected Sheet sheet = null;
	protected File file = null;
	protected FileInputStream fileInputStream = null;
	protected InputStream inputStream = null;

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	// will create a new xlsx file in specified location on executing
	// generateExcelFile() method
	@SuppressWarnings("deprecation")
	public Excel(String path, String workBookName) throws AppException {
		if (path == null || path.equals("")) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		if (workBookName == null || workBookName.equals("")) {
			this.workBookName = "default[" + new Date().getDate() + "].xlsx";
		} else
			this.workBookName = workBookName;

		this.file = new File(path + File.separator + this.workBookName);
		if (!(this.file.exists()))
			(new File(file.getParent())).mkdirs();

		this.wb = new SXSSFWorkbook();
		this.sheet = this.wb.createSheet();

		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
	}

	public Workbook getWb() {
		return wb;
	}

	public void setWb(SXSSFWorkbook wb) {
		this.wb = wb;
	}

	public void removeSheetAt(int i) {
		this.wb.removeSheetAt(i);
	}

	public Sheet getSheet() {
		return this.sheet;
	}

	public void setSheet(SXSSFSheet sheet) {
		this.sheet = sheet;
	}

	// will read the specified Excel file
	public Excel(File file) throws AppException {

		initWorkbook(file);
	}

	private void initWorkbook(File file) {
		try {

			wb  = WorkbookFactory.create(file);

			this.file = file;
		} catch (Exception ex) {
			try {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			} catch (AppException e) {
				e.printStackTrace();
			}
		}

	}

	// will read the specified Excel file
	/*
	 * public Excel(FileInputStream fileInputStream) throws AppException {
	 * 
	 * try { wb = new SXSSFWorkbook(fileInputStream); sheet = wb.getSheetAt(0);
	 * this.fileInputStream = fileInputStream; } catch (Exception ex) { throw
	 * new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } }
	 */

	// will read the specified Excel file
	/*
	 * public Excel(InputStream inputStream) throws AppException {
	 * 
	 * try { wb = new SXSSFWorkbook(inputStream); sheet = wb.getSheetAt(0);
	 * this.inputStream = inputStream; } catch (Exception ex) { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } }
	 */

	// will read the specified Excel sheet
	public void loadSheet(int i) throws AppException {

		try {
			if (wb == null) {
				initWorkbook(this.file);
			}
			this.sheet = wb.getSheetAt(i);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	// will read the specified Excel sheet
	public void loadSheet(String sheetName) throws AppException {

		try {
			if (wb == null) {
				initWorkbook(this.file);
			}
			this.sheet = wb.getSheet(sheetName);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	// will read the specified Excel sheet
	public void createSheet(String str) throws AppException {

		try {
			sheet = wb.createSheet(str);
		} catch (Exception ex) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	protected void closeExcelFile() {
		try {
			this.wb.close();
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	protected Cell getCell(String cellName) {
		CellReference ref = new CellReference(cellName);
		Row row = sheet.getRow(ref.getRow());
		if (row == null) {
			row = sheet.createRow(ref.getRow());
		}

		Cell cell = row.getCell(ref.getCol());
		if (cell == null) {
			cell = row.createCell(ref.getCol());
		}

		return cell;
	}

	public void setCellValue(String cellName, String value) {

		CellStyle style = this.wb.createCellStyle();
		style.setLocked(false);
		style.setWrapText(true);

		Cell cell = this.getCell(cellName);
		cell.setCellValue(value);
		cell.setCellStyle(style);

	}
	
	
	public void setCellValue(String cellName, String value,CellStyle style) {
		Cell cell = this.getCell(cellName);
		cell.setCellValue(value);
		cell.setCellStyle(style);

	}
	

	public void setHeaders(List<String> headers, String sheetName) throws AppException {

		try {
			createSheet(sheetName);

			int i = 1;
			for (String header : headers) {
				setCellValue(getCharForNumber(i) + 1, header);
				i++;
			}
		} catch (Exception ex) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public void setValues(List<List<String>> datas, String sheetName) throws AppException {

		try {

			int j = 2;

			CellStyle style = this.wb.createCellStyle();
			style.setLocked(false);
			style.setWrapText(true);
			
			for (List<String> data : datas) {
				int i = 1;
				for (String str : data) {
					setCellValue(getCharForNumber(i) + j, str,style);
					i++;
				}
				j++;
			}
		} catch (Exception ex) {
			throw new AppException(ex);
		}

	}

	@SuppressWarnings("static-access")
	public void setHeaders(Map<String, String> headers) {

		for (Entry<String, String> header : headers.entrySet()) {
			Cell cell = this.getCell(header.getKey());
			cell.setCellValue(header.getValue());
			this.sheet.autoSizeColumn(cell.getColumnIndex());

			CellStyle Cell = wb.createCellStyle();
			// DEclare Font
			Font font = wb.createFont();
			Cell.setLocked(true);

			Cell.setBottomBorderColor(IndexedColors.ROYAL_BLUE.getIndex());
			Cell.setBorderBottom(CellStyle.BORDER_THICK);
			Cell.setTopBorderColor(IndexedColors.ROYAL_BLUE.getIndex());
			Cell.setBorderTop(CellStyle.BORDER_THICK);
			Cell.setLeftBorderColor(IndexedColors.DARK_RED.getIndex());
			Cell.setBorderLeft(CellStyle.BORDER_THIN);
			Cell.setRightBorderColor(IndexedColors.DARK_RED.getIndex());
			Cell.setBorderRight(CellStyle.BORDER_THIN);

			sheet.setColumnWidth(cell.getColumnIndex(), 8000);
			font.setBold(true);
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			Cell.setFont(font);
			// Declare rowExcel Format
			Cell.setDataFormat(wb.createDataFormat().getFormat("#,#0.00"));
			// Declare Alignment
			Cell.setAlignment(Cell.ALIGN_CENTER);

			cell.setCellStyle(Cell);

		}

	}

	/*
	 * public void setDataValidations(Map<String, String[]> validations) {
	 * 
	 * XSSFCell firstCell; XSSFCell lastCell;
	 * 
	 * DataValidationHelper validationHelper = new
	 * XSSFDataValidationHelper(this.sheet);
	 * 
	 * for (Entry<String, String[]> entry : validations.entrySet()) {
	 * 
	 * String[] cellRange = entry.getKey().split(":"); firstCell =
	 * getCell(cellRange[0]); lastCell = getCell(cellRange[1]);
	 * 
	 * CellRangeAddressList range = new CellRangeAddressList();
	 * range.addCellRangeAddress(firstCell.getRowIndex(),
	 * firstCell.getColumnIndex(), lastCell.getRowIndex(),
	 * lastCell.getColumnIndex());
	 * 
	 * DataValidationConstraint constraint =
	 * validationHelper.createExplicitListConstraint(entry.getValue());
	 * 
	 * DataValidation dataValidation =
	 * validationHelper.createValidation(constraint, range);
	 * dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
	 * dataValidation.setSuppressDropDownArrow(true);
	 * sheet.addValidationData(dataValidation); } }
	 */

	// Only need one of these
	DataFormatter fmt = new DataFormatter();

	public String excelReadSingleValue(String cellName) throws IOException {
		FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		if (this.getCell(cellName).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {

			if (HSSFDateUtil.isCellDateFormatted(this.getCell(cellName))) {
				Date date = this.getCell(cellName).getDateCellValue();
				return sdf.format(date);
			} else {
				// Once per cell
				String valueAsSeenInExcel = fmt.formatCellValue(this.getCell(cellName));
				return valueAsSeenInExcel.replace("'", "").replace(",", "");
			}
		}
		if (this.getCell(cellName).getCellType() == HSSFCell.CELL_TYPE_FORMULA) {
			return evaluator.evaluate(this.getCell(cellName)).getNumberValue() + "";
		}

		// Once per cell
		String valueAsSeenInExcel = fmt.formatCellValue(this.getCell(cellName));
		return valueAsSeenInExcel.replace("'", "");
	}

	// for read formula cell
	public String excelReadSingleRawValue(String cellName) throws IOException {
		return new DecimalFormat("#.##").format(new Double(this.getCell(cellName).getStringCellValue()));

	}

	// for read formula cell
	public String excelReadSingleRawValueWithoutFormat(String cellName) {
		// return this.getCell(cellName).getRawValue();
		return this.getCell(cellName).getStringCellValue();

	}

	public List<String> excelReadRow(String cellRange) throws AppException {
		try {
			// String[] rowExcel;
			List<String> values = new ArrayList<>();
			String[] cells = cellRange.split(":");

			CellReference stRow = new CellReference(cells[0]);
			CellReference endRow = new CellReference(cells[1]);
			// rowExcel = new String[(endRow.getCol() - stRow.getCol()) + 1];
			// int j = 0;
			String cellValue = "";
			boolean isEmptyRow = true;
			for (int i = stRow.getCol(); i <= endRow.getCol(); i++) {
				// rowExcel.add(excelReadSingleValue("" +
				// ((char)(stRow.getCol()+65))+""+i ));
				// rowExcel[j++] = excelReadSingleValue("" + ((char) (i + 65)) +
				// (stRow.getRow() + 1));]
				
				cellValue = excelReadSingleValue("" + ((char) (i + 65)) + (stRow.getRow() + 1));
				//_LOGGER.debug("row value  "+i+""+cellValue);
				if (!StringUtils.isBlank(cellValue) && isEmptyRow)
					isEmptyRow = false;
				values.add(StringUtils.isEmpty(cellValue)?cellValue:cellValue.trim());
			
			}
			if (!isEmptyRow)
				return values;
			return null;

		} catch (Exception ex) {
			_LOGGER.error("excel while reading excel ",ex);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	public StringBuilder excelReadRow(String cellRange,StringBuilder rows) throws AppException {
		try {
			// String[] rowExcel;
			String[] cells = cellRange.split(":");

			CellReference stRow = new CellReference(cells[0]);
			CellReference endRow = new CellReference(cells[1]);
			
			String cellValue = "";
			boolean isEmptyRow = true;
			for (int i = stRow.getCol(); i <= endRow.getCol(); i++) {
				cellValue = excelReadSingleValue("" + ((char) (i + 65)) + (stRow.getRow() + 1));
				if (!StringUtils.isBlank(cellValue) && isEmptyRow)
					isEmptyRow = false;
				rows.append(StringUtils.isEmpty(cellValue)?cellValue:cellValue.trim().replace(",","-"));
				if(i!=endRow.getCol())
					rows.append(",");
			}
			if (!isEmptyRow)
				return rows.append("\r\n");
			return null;

		} catch (Exception ex) {
			_LOGGER.error("excel while reading excel ",ex);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public List<String> excelReadMultipleRowValue(String cellRange) throws IOException {
		return null;

	}

	public String getCharForNumber(int i) {
		int rem = i % 26;
		int div = i / 26;
		if (i > 0 && i < 27)
			return String.valueOf((char) (i + 64));
		else {
			return String.valueOf((char) (div + 64)) + String.valueOf((char) (rem + 64));
		}
	}

	public File generateExcelFile() {

		FileOutputStream outputFile = null;
		try {
			if (file.exists()) {
				file.delete();
			}
			outputFile = new FileOutputStream(this.file);
			ZipSecureFile.setMinInflateRatio(0);;
			wb.write(outputFile);
			outputFile.flush();
			System.out.println("Date Successfully Inserted....");
			this.file.setReadOnly();

		} catch (Exception e) {
			e.printStackTrace();
			_LOGGER.error("error in writing excel file ",e);
		} finally {
			try {
				outputFile.close();
				this.closeExcelFile();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		return this.file;
	}

}