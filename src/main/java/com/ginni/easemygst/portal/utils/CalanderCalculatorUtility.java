package com.ginni.easemygst.portal.utils;

import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jfree.data.time.Quarter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;

public class CalanderCalculatorUtility {

	public static final String _EMG_MONTHYEAR_FORMAT = "MMyyyy";

	private static final DateTimeFormatter viewFormatterShortMonth = DateTimeFormatter.ofPattern("MMM");

	private static final DateTimeFormatter viewFormatterComma = DateTimeFormatter.ofPattern("yy");

	private static final DateTimeFormatter viewFormatterMonthWithSpace = DateTimeFormatter.ofPattern("MMMM yyyy");

	private static final String _EMGSTDATEFORMATTER = "dd/mm/yyyy";

	public enum ROUNDINGMODE {
		HALF_HOUR_UP, HALF_HOUR_DOWN
	}

	protected static Logger log = LoggerFactory.getLogger(CalanderCalculatorUtility.class);

	public static String getFiscalStartMonth() {

		return "04";
	}

	public static String getFiscalStartMonth(String fiscalYear) {

		if ("2017-2018".equalsIgnoreCase(fiscalYear))
			return "072017";
		return "04";
	}

	public static String getFiscalEndMonth() {
		return "03";
	}

	public static String getFiscalYearByMonthYear(YearMonth yearMonth) {

		int month = yearMonth.getMonthValue();
		int year = yearMonth.getYear();

		int rangeYear = 0;

		if (4 <= month) {
			rangeYear = year + 1;
		} else
			rangeYear = year - 1;

		return year < rangeYear ? fiscalAppender(year, rangeYear) : fiscalAppender(rangeYear, year);

	}

	private static String fiscalAppender(int startYear, int endYear) {

		return new StringBuilder().append(startYear).append("-").append(endYear).toString();
	}

	public static boolean isCurrentFiscal(String fiscalYear) {

		String[] fiscalYears = fiscalYear.split("-");

		Year currentYear = Year.now();

		for (String year : fiscalYears) {
			if (currentYear.equals(Year.parse(year)))
				return true;
			else {
				return false;
			}
		}
		return false;

	}

	public static String getMonthYearToFullMonthName(String monthYear) throws AppException {
		return getStrMonthYearToMonth(monthYear).getDisplayName(TextStyle.FULL, Locale.ENGLISH);

	}

	public static YearMonth convertStrMonthYearToYearMonth(String monthYear) throws AppException {
		try {
			DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder().appendPattern(_EMG_MONTHYEAR_FORMAT)
					.toFormatter();

			return YearMonth.parse(monthYear, dateTimeFormatter);
		} catch (DateTimeParseException exception) {
			log.error("date format exception while parsing month year {}", monthYear, exception);
			throw new AppException(ExceptionCode._INVALID_PARAMETER, "Invalid month-year format " + monthYear);
		}
	}

	public static Month getStrMonthYearToMonth(String monthYear) throws AppException {

		String month = getStrMonthFromMonthYear(monthYear);

		return Month.of(Integer.parseInt(month));
	}

	public static String getStrMonthFromMonthYear(String monthYear) throws AppException {

		if (StringUtils.isNotEmpty(monthYear)) {
			return StringUtils.substring(monthYear, 0, 2);
		}
		throw new AppException("9999", "Month Year should not to empty");
	}

	public static String getShortCommaStyleMonthYear(String monthYear) throws AppException {

		if (StringUtils.isNotEmpty(monthYear)) {

			YearMonth yearMonth = convertStrMonthYearToYearMonth(monthYear);

			return viewFormatterShortMonth.format(yearMonth) + "'" + viewFormatterComma.format(yearMonth);
		}
		throw new AppException("9999", "Month Year should not to empty");
	}

	public static String getFullMonthYearWithSpace(String monthYear) throws AppException {
		if (StringUtils.isNotEmpty(monthYear)) {
			YearMonth month = convertStrMonthYearToYearMonth(monthYear);

			return viewFormatterMonthWithSpace.format(month);
		}
		throw new AppException("9999", "Month Year should not to empty");
	}

	public static String getCurrentFinancialYear() {

		return getFiscalYearByMonthYear(YearMonth.now());

	}

	public static String getPreviousFinancialYear() {

		return getFiscalYearByMonthYear(YearMonth.now().minus(Period.ofYears(1)));

	}

	private static String getNextFinancialYear(String currentFinancialYear) {

		Year currentFinancialYearEnd = Year.parse(StringUtils.substringAfter(currentFinancialYear, "-"));

		return fiscalAppender(currentFinancialYearEnd.getValue(), currentFinancialYearEnd.plusYears(1).getValue());

	}

	public static java.util.List<String> getFinancialYearsFromGstStart() {

		String gstStartFinYear = ApplicationMetadata._GST_START_FINANCIALYEAR;

		String currentFinancialYear = getCurrentFinancialYear();

		java.util.List<String> financialYears = new ArrayList<>();
		financialYears.add(gstStartFinYear);

		while (!gstStartFinYear.equalsIgnoreCase(currentFinancialYear)) {

			gstStartFinYear = getNextFinancialYear(gstStartFinYear);
			financialYears.add(gstStartFinYear);

		}

		return financialYears;

	}

	public static java.util.List<String> getmonthYears(String fyear) {
		if (fyear.equalsIgnoreCase("2017-2018")) {
			return Arrays.asList("072017", "082017", "092017", "102017", "112017", "122017", "012018", "022018",
					"032018");
		}

		int currentMonth = YearMonth.now().getMonthValue()-1;
		java.util.List<String> monthlist = new ArrayList<>();
		
		LocalDate c = LocalDate.now();
		int day = c.getDayOfMonth();
		if(day>20) {
			currentMonth++;
		}

		String[] years = fyear.split("-");
		if (currentMonth >= 4) {
			for (int i = 4; i <= currentMonth; i++) {
				monthlist.add(String.format("%06d", Integer.parseInt(i + years[0])));
			}
		} else {
			for (int i = 4; i <= 12; i++) {
				monthlist.add(String.format("%06d", Integer.parseInt(i + years[0])));
			}
			for (int i = 1; i <= currentMonth; i++) {
				monthlist.add(String.format("%06d", Integer.parseInt(i + years[1])));
			}
		}
		return monthlist;

	}

	public static java.util.List<String> quarterlist(String fyear) {

		if (isCurrentFiscal(fyear)) {
			String quater = getcurrentquarter();
			switch (quater) {
			case "Q4":
				return Arrays.asList("Q1", "Q2", "Q3", "Q4");
			case "Q3":
				return Arrays.asList("Q1", "Q2", "Q3");
			case "Q2":
				return Arrays.asList("Q1", "Q2");
			case "Q1":
				return Arrays.asList("Q1");
			}
		}
		return Arrays.asList("Q1", "Q2", "Q3", "Q4");

	}

	public static String getcurrentquarter() {
		LocalDate c = LocalDate.now();
		int month = c.getMonthValue();
		return (month >= Calendar.JANUARY && month <= Calendar.MARCH) ? "Q4"
				: (month >= Calendar.APRIL && month <= Calendar.JUNE) ? "Q1"
						: (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) ? "Q2" : "Q3";
	}

	public static String getfilingmonthyear(String fyear, String quater) {

		if (quater.equalsIgnoreCase("Q1") && fyear.equalsIgnoreCase("2017-2018")) {
			return "072017";
		}
		Map<String, String> m = new HashMap<>();
		m.put("Q1", "06");
		m.put("Q2", "09");
		m.put("Q3", "12");
		m.put("Q4", "03");
		String[] years = fyear.split("-");
		switch (quater) {
		case "Q1":
		case "Q2":
		case "Q3":
			return m.get(quater) + years[0];
		case "Q4":
			return m.get(quater) + years[1];
		}
		return "";

	}

	public static LocalTime getCurrentTimeWithRounding(ROUNDINGMODE roundingMode) {

		LocalTime currentTime = LocalTime.now();

		int minute = currentTime.getMinute()>30?currentTime.getMinute()-30:currentTime.getMinute();

		if (roundingMode == ROUNDINGMODE.HALF_HOUR_DOWN)
			return currentTime.plus(-Math.abs(minute), ChronoUnit.MINUTES);
		return currentTime.plus(Math.abs(30 - minute), ChronoUnit.MINUTES);

	}

}