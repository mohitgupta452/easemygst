package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;
import javax.naming.InitialContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.ExternalServiceExecutor;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.transaction.impl.SAPExcelProcessor;
import com.ginni.easemygst.portal.transaction.impl.TransactionUtility;

public class GstExcel extends Excel {
	
	@Inject
	private Logger log;

	public GstExcel(File file) throws AppException {
		super(file);
		this.file = file;
	}
	
	static String selectSubmitReturn = "SELECT id, actionDate from `gstin_action` WHERE gstin = ? and particular = ? and returnType = ? and status = 'SUBMIT'";
	
	public static void checkReturnSubmitCondition(Integer id, String monthYear, String gstReturn) {
		Connection connection = null;
		PreparedStatement subStatement = null;
		ResultSet rs = null;
		try {
			connection = DBUtils.getConnection();
			subStatement = connection.prepareStatement(selectSubmitReturn);
			subStatement.setInt(1, id);
			subStatement.setString(2, monthYear);
			subStatement.setString(3, gstReturn);
			rs = subStatement.executeQuery();

			if (rs.next()) {
				throw new AppException(ExceptionCode._RETURN_ALREADY_SUBMITTED);
			} 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (subStatement != null && !subStatement.isClosed())
					DBUtils.close(subStatement);
				if (rs != null && !rs.isClosed())
					DBUtils.close(rs);
				if (connection != null && !connection.isClosed())
					DBUtils.close(connection);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public int uploadExcel(String gstReturn, String monthYear,ObjectNode mappings, String provider,TaxpayerGstin taxpayerGstin,
			Set<ReturnTransactionDTO> returnTransactionDTOs,UserBean userBean,String delete)
			throws Exception {
		
			try {
				
				SourceType source=SourceType.EXCEL;
				
				//checkReturnSubmitCondition(taxpayerGstin.getId(), monthYear, gstReturn);
				
				int valuesCount = 0;
				ReturnTransactionDTO dto = new ReturnTransactionDTO();
				dto.setCode("INVOICES");
				dto.setName("INVOICES");
				returnTransactionDTOs.add(dto);
				

				for (ReturnTransactionDTO returnTransactionDTO : returnTransactionDTOs) {
					String code = returnTransactionDTO.getCode();
					String key = code.toLowerCase();
					
					if(!StringUtils.isEmpty(provider)) {
						if(provider.equalsIgnoreCase("tally")) {
							source=SourceType.TALLY;
							//load tally json mapping
						} else if(provider.equalsIgnoreCase("sap")) {
							//load sap json mapping
							source=SourceType.SAP;

							if(key.equalsIgnoreCase("cdn")) {
								
							}
						} 
					}

					code = !Objects.isNull(mappings) && !"sap".equalsIgnoreCase(provider)
							? mappings.has(key + "_sheet") ? mappings.get(key + "_sheet").asText() : code : code;
					
					if("sap".equalsIgnoreCase(provider)){
						this.loadSheet(0);
						if(this.getSheet().getSheetName().equalsIgnoreCase("GSTR1_Credit_Debit_Note")) {
							code = "cdn";
							key = code.toLowerCase();
							String temp = "{\"cdn_sheet\":\"GSTR1_Credit_Debit_Note\",\"cdn_column\":\"6,5,4,3,1,0,2,10,11,9,13,12,16,17,19,21,23,25,31,26\"}";
							try {
								mappings = new ObjectMapper().readValue(temp,ObjectNode.class);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}}
					 else
					if (mappings.has(key + "_sheet"))
						this.loadSheet(code);

					if ((!Objects.isNull(this.getSheet()) &&(!Objects.isNull(mappings) &&  mappings.has(key + "_sheet")))||
							"sap".equalsIgnoreCase(provider) ){
						Iterator<Row> iterator = this.getSheet().rowIterator();

						String columns = null;
						columns = !Objects.isNull(mappings)&& !"sap".equalsIgnoreCase(provider)
								? mappings.has(key + "_column") ? mappings.get(key + "_column").asText() : null : null;
						
						if(this.getSheet().getSheetName().equalsIgnoreCase("GSTR1_Credit_Debit_Note")) {
							columns = !Objects.isNull(mappings) ? mappings.has(key + "_column") ? mappings.get(key + "_column").asText() : null : null;
						}

						if(iterator.hasNext() && SourceType.SAP.toString().equalsIgnoreCase(provider) && key.equalsIgnoreCase("invoices")){
							JsonNode output =  new SAPExcelProcessor().processExcelList(this, gstReturn,
														 columns, taxpayerGstin, monthYear,
														 code,userBean, delete);
							if(output.has("totalCount"))
								valuesCount=output.get("totalCount").asInt();
							break;
							
						}else
						if (iterator.hasNext() && !key.equals("invoices")) {
							valuesCount += processData(this, gstReturn, columns, taxpayerGstin, monthYear, key, code,userBean,delete,source);
							if("sap".equalsIgnoreCase(provider) && "GSTR1_Credit_Debit_Note".equalsIgnoreCase(this.getSheet().getSheetName())) {
								break;
							}
						} else 
							if (iterator.hasNext() && key.equals("invoices")){
							TransactionUtility tu = new TransactionUtility();
							JsonNode output =  tu.processExcelList(this, gstReturn,
														 columns, taxpayerGstin, monthYear,
														 code,userBean,delete);
							if(output.has("totalCount"))
								valuesCount=output.get("totalCount").asInt();
						}

					}
					
				}
				return valuesCount;
			} catch (AppException e) {
				e.printStackTrace();
				throw e;
			}
	}
	public int processData(GstExcel gstExcel, String gstReturn, String columns, TaxpayerGstin taxpayerGstin, String monthYear,
			String transaction, String sheetName,UserBean userBean,String delete,SourceType sourceType) throws Exception {

		
		TransactionFactory transactionFactory = new TransactionFactory(transaction);
		//transactionFactory.deleteTransaction(taxpayerGstin.getGstin(), monthYear,TransactionType.valueOf(transaction.toUpperCase()), ReturnType.valueOf(gstReturn.toUpperCase()),delete,DataSource.EMGST);
	     return  transactionFactory.processExcelList(gstExcel, gstReturn, columns, taxpayerGstin, sheetName,monthYear,userBean,sourceType,delete);


//		if (!Objects.isNull(object)) {
//			transactions = object.getString("data");
//			rowCount = object.getInt("size");
//		}
		// process and store transactions
	//processAndStoreTransaction(monthYear, gstReturn, transaction, gstin, transactions);

	//	return rowCount;
	}
	public int processExcelData(GstExcel gstExcel,String transaction, String gstReturn, TaxpayerGstin taxpayerGstin, String sheetName,
			String monthYear, SourceType source) throws Exception {

		TransactionFactory transactionFactory = new TransactionFactory(transaction);
	     return  transactionFactory.processList(gstExcel,transaction,gstReturn,taxpayerGstin, sheetName,monthYear,source);

	}
	public int uploadData(GstExcel gstExcel,String transaction,String gstReturn, String monthYear,TaxpayerGstin taxpayerGstin,String sheetName,SourceType source)
			throws Exception {
			
		processExcelData(this,transaction,gstReturn,taxpayerGstin,monthYear,sheetName,source);
		return 200;
	}

}
