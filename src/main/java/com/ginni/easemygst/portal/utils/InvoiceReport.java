package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.dto.InvoiceLineItemDTO;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class InvoiceReport {

	@Inject
	private Logger log;

	@SuppressWarnings({ "finally", "unchecked" })
	public static String generateInvoice(Map<String, Object> reportMap) {

		String destFile = null;

		InputStream srcInputStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("jasper/invoice.jrxml");

		try {
			JasperReport jasperreport = JasperCompileManager.compileReport(srcInputStream);

			List<InvoiceLineItemDTO> itemDTOs = (List<InvoiceLineItemDTO>) reportMap.get("ds1");

			JRBeanCollectionDataSource beanList = new JRBeanCollectionDataSource(itemDTOs);

			reportMap.put("ds1", beanList);

			JasperPrint printFileName = JasperFillManager.fillReport(jasperreport, reportMap, new JREmptyDataSource());

			if (printFileName != null) {
				/**
				 * 1- export to PDF
				 */
				String fileName = "Invoice-" + reportMap.get("invoiceNumber").toString().replace("/", "-") + ".pdf";
				destFile = ManipulateFile.getParentDirBasePath() + File.separator + fileName;
				JasperExportManager.exportReportToPdfFile(printFileName, destFile);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return destFile;
		}

	}

}
