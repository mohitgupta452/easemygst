package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.helper.config.AppConfig;

public class ManipulateFile {

	private final static Logger _Logger = org.slf4j.LoggerFactory.getLogger(ManipulateFile.class);

	public static File getPlatformBasedParentDir() {

		File parentDir = null;

		if (SystemUtils.IS_OS_WINDOWS) {
			parentDir = new File("c:" + File.separator + "easemygsttemp" + File.separator + AppConfig.getActiveProfile()
					+ File.separator);
		} else if (SystemUtils.IS_OS_LINUX) {
			parentDir = new File(SystemUtils.USER_HOME + File.separator + "easemygsttemp" + File.separator
					+ AppConfig.getActiveProfile() + File.separator);
		} else if (SystemUtils.IS_OS_MAC) {
			parentDir = new File(SystemUtils.USER_HOME + File.separator + "easemygsttemp" + File.separator
					+ AppConfig.getActiveProfile() + File.separator);
		}

		_Logger.debug("parent dircetory : {} ", parentDir);

		if (!parentDir.exists()) {
			parentDir.mkdirs();
		}
		parentDir.setWritable(true);

		return parentDir;
	}

	public static String getParentDirBasePath() {

		File parentDir = getPlatformBasedParentDir();

		return parentDir.getAbsolutePath();
	}

	public static String getParentDirBasePath(String folderName) {

		File parentDir = getPlatformBasedParentDir();

		File dir = new File(parentDir.getAbsolutePath() + File.separator + folderName);
		if (!dir.exists())
			dir.mkdirs();

		return dir.getAbsolutePath();
	}

	public static String uploadMultipartFile(MultipartFormDataInput input) {

		String fileName = null;
		Map<String, List<InputPart>> formParts = input.getFormDataMap();

		List<InputPart> inPart = formParts.get("file");

		for (InputPart inputPart : inPart) {

			try {

				// Retrieve headers, read the Content-Disposition header to
				// obtain the original name of the file
				MultivaluedMap<String, String> headers = inputPart.getHeaders();
				fileName = getFileName(headers);
				int pos = fileName.lastIndexOf(".");
				String temp = fileName.substring(0, pos);
				String temp1 = fileName.substring(pos, fileName.length());
				fileName = temp + "-" + UUID.randomUUID() + temp1;

				// Handle the body of that part with an InputStream
				InputStream istream = inputPart.getBody(InputStream.class, null);

				fileName = getParentDirBasePath() + File.separator + fileName;
				byte[] bytes = IOUtils.toByteArray(istream);

				writeFile(bytes, fileName);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return fileName;
	}
	
	
	public static String getUploadedFileName(MultipartFormDataInput input) {
		
		Map<String, List<InputPart>> formParts = input.getFormDataMap();

		List<InputPart> inPart = formParts.get("file");

		for (InputPart inputPart : inPart) {
				// Retrieve headers, read the Content-Disposition header to
				// obtain the original name of the file
				MultivaluedMap<String, String> headers = inputPart.getHeaders();
				
		return getFileName(headers);
		}
		return "unknown";
		
	}
	

	// get uploaded filename, is there a easy way in RESTEasy?
	private static String getFileName(MultivaluedMap<String, String> headers) {

		String[] contentDisposition = headers.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}

		return "unknown";
	}

	// save to somewhere
	private static void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();
	}
	
	//upload csv
	public static String uploadCsvFile(MultipartFormDataInput input) {
		String location=ManipulateFile.getParentDirBasePath()+File.separator+"csvs"+File.separator;
		String fileName = null;
		Map<String, List<InputPart>> formParts = input.getFormDataMap();

		List<InputPart> inPart = formParts.get("file");

		for (InputPart inputPart : inPart) {

			try {

				// Retrieve headers, read the Content-Disposition header to
				// obtain the original name of the file
				MultivaluedMap<String, String> headers = inputPart.getHeaders();
				
				fileName = "csv" + "-" + UUID.randomUUID(); 

				// Handle the body of that part with an InputStream
				InputStream istream = inputPart.getBody(InputStream.class, null);

				fileName = location+ fileName;
				byte[] bytes = IOUtils.toByteArray(istream);

				writeFile(bytes, fileName);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return fileName;
	}
	
	
	public static String uploadCsvFile(InputStream istream,String fname ) {
		String location=ManipulateFile.getParentDirBasePath()+File.separator+"csvs"+File.separator;
		String fileName = null;

			try {
				fileName = "csv" + "-" +fname; 
				fileName = location+ fileName;
				byte[] bytes = IOUtils.toByteArray(istream);

				_Logger.info("writing csv file on : {}",fileName);
				writeFile(bytes, fileName);

			} catch (IOException e) {
				_Logger.error("io exception while creating csv  {}",fileName,e);;
			}

		

		return fileName;
	}

}
