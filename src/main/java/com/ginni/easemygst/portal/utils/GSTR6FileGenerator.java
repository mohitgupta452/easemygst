package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;

import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.utils.AzzureFIleUpload.AZZURE_DIRECTORY;

public class GSTR6FileGenerator {

	public static String generateCSV(GetGstr2Resp getGstr2Resp, TransactionType transactionType,String gstin,String monthYear) throws IOException, AppException {
		
		String path=ManipulateFile.getParentDirBasePath()+"GSTR6CSV"+File.separator+gstin+"-"+monthYear+"-"+transactionType+".csv";
		
		StringBuilder csvBuilder = new StringBuilder();

		csvBuilder.append(
				"CTIN,CTIN FILLING STATUS,INVOICE NUMBER,INVOICE DATE,SNUM,SGST,CGST,IGST,CESS,RATE,TAXABLE VALUE")
				.append("\n");

		if (TransactionType.B2B == transactionType)
			csvBuilder = createB2b(getGstr2Resp.getB2b(), csvBuilder);
		else if (TransactionType.CDN == transactionType)
			csvBuilder = createCDN(getGstr2Resp.getCdn(), csvBuilder);
		
		Files.write(Paths.get(path), csvBuilder.toString().getBytes());
		
		return AzzureFIleUpload.uploadFileToAzzureService(AZZURE_DIRECTORY.GSTR6, new File(path));
		
	}

	private static StringBuilder createB2b(List<B2BTransactionEntity> b2bTransactionEntities,
			StringBuilder csvBuilder) {
		
		SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");

		for (B2BTransactionEntity b2bTransactionEntity : b2bTransactionEntities) {
			
			csvBuilder.append(b2bTransactionEntity.getCtin()).append(",")
			.append(b2bTransactionEntity.getFillingStatus()).append(",");
			
			boolean isFirst=true;

			for (B2BDetailEntity b2bDetailEntity : b2bTransactionEntity.getB2bDetails()) {
				
				if(!isFirst)
					csvBuilder.append(",,");
				isFirst=false;
				csvBuilder.append(b2bDetailEntity.getInvoiceNumber()).append(",")
						.append(dateFormat.format(b2bDetailEntity.getInvoiceDate())).append(",");

				csvBuilder = createItem(b2bDetailEntity.getItems(), csvBuilder);

			}

		}
		return csvBuilder;

	}

	private static StringBuilder createCDN(List<CDNTransactionEntity> cdnTransactionEntities,
			StringBuilder csvBuilder) {

		SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");

		for (CDNTransactionEntity cdnTransactionEntity : cdnTransactionEntities) {

			csvBuilder.append(cdnTransactionEntity.getOriginalCtin()).append(",")
					.append(cdnTransactionEntity.getFillingStatus()).append(",");
			
			boolean isFirst=true;

			for (CDNDetailEntity cdnDetailEntity : cdnTransactionEntity.getCdnDetails()) {
				
				if(!isFirst)
					csvBuilder.append(",,");

				csvBuilder.append(cdnDetailEntity.getInvoiceNumber()).append(",")
						.append(dateFormat.format(cdnDetailEntity.getInvoiceDate())).append(",");
				csvBuilder = createItem(cdnDetailEntity.getItems(), csvBuilder);
			}

		}
		return csvBuilder;

	}

	private static StringBuilder createItem(List<Item> items, StringBuilder csvBuilder) {

		boolean isFirst=true;

		for (Item item : items) {
			
			if(!isFirst)
				csvBuilder.append(",,,,");

			csvBuilder.append(item.getSerialNumber()).append(",").append(item.getSgst()).append(",")
					.append(item.getCgst()).append(",").append(item.getIgst()).append(",").append(item.getCess())
					.append(",").append(item.getTaxRate()).append(",").append(item.getTaxableValue()).append("\n");
		}

		return csvBuilder;
	}

}
