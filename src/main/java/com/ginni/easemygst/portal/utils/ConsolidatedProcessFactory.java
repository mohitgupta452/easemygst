package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Row;

import com.ginni.easemygst.portal.helper.AppException;

public class ConsolidatedProcessFactory {
	
	
	public static String processConsolidatedExcel(String filePath,String datauploadInfoID) throws AppException, IOException {
		
		String fileExtension=FilenameUtils.getExtension(filePath);
		
		      if("xls".equalsIgnoreCase(fileExtension) || "xlsx".equalsIgnoreCase(fileExtension))
		    	  filePath=	ConsolidatedProcessFactory.convertExcelToCSV(filePath,fileExtension);
		      return filePath;
		
	}
	
	
	private static String convertExcelToCSV(String filePath,String fileExtension) throws AppException, IOException {
		
		File excelFile = new File(filePath);
		
		GstExcel consolidatedExcel=new GstExcel(excelFile);
		
		consolidatedExcel.loadSheet(0);
		
		Iterator<Row> iterator = consolidatedExcel.getSheet().iterator();
		
		StringBuilder rows = new StringBuilder();
		
		String csvPathStr=ManipulateFile.getParentDirBasePath()+File.separator+"consolidatedcsv"
				+File.separator+excelFile.getName().replace(fileExtension,"")+"csv";
		
		Path csvFilePath= Paths.get(csvPathStr);
		
		int rowNum=0;
		int columnCount=0;
		while (iterator.hasNext()) {

			Row row = iterator.next();
			rowNum = row.getRowNum();
				rowNum++;
				String cellRange = "A" + rowNum + ":" + consolidatedExcel.getCharForNumber(columnCount) + rowNum;
				 
			     if(Objects.isNull(consolidatedExcel.excelReadRow(cellRange,rows)))
			    		 break;
			
			if(rowNum % 5000 == 0) {
				Files.write(csvFilePath,rows.toString().getBytes(),rowNum > 5000 ? StandardOpenOption.APPEND:StandardOpenOption.CREATE);
			rows = new StringBuilder();
			}
	}
		Files.write(csvFilePath,rows.toString().getBytes(),rowNum > 5000 ? StandardOpenOption.APPEND:StandardOpenOption.CREATE);
		return csvPathStr;
	}
	
}
