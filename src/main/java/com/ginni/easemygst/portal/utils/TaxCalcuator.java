package com.ginni.easemygst.portal.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaxCalcuator {
	
	private static final boolean _CALCULATE_TAX=false;
	
	private static  BigDecimal calculateTaxableAmt(BigDecimal taxAmt,BigDecimal taxRate) {
		
		return taxAmt.multiply(BigDecimal.valueOf(100)).divide(taxRate,2,RoundingMode.FLOOR);
		
	}
	
	private static boolean isvalidTaxAmount(BigDecimal taxAmt,BigDecimal taxRate,BigDecimal taxabaleValue) {
		
		// tax amt = taxablevalue * tax rate/ 100
		
	     if(taxabaleValue.compareTo(calculateTaxableAmt(taxAmt, taxRate)) == 0)
	    		 return true;
	     return false;

	}
	
	
	public static  BigDecimal getValidGstTaxableValue(BigDecimal cgst,BigDecimal igst,BigDecimal taxRate ,BigDecimal taxabaleValue) {
		
		if(_CALCULATE_TAX) {
		BigDecimal taxAmt=BigDecimal.ZERO;
		
		if(BigDecimal.ZERO.compareTo(taxRate) != 0)
		{
			if(BigDecimal.ZERO.compareTo(cgst) != 0) {
				taxAmt=cgst.multiply(BigDecimal.valueOf(2));
			}
			else if(BigDecimal.ZERO.compareTo(igst) != 0)
			taxAmt=igst;
			
			if(!isvalidTaxAmount(taxAmt, taxRate, taxabaleValue))
			return calculateTaxableAmt(taxAmt, taxRate);
		}
		}
		return taxabaleValue;
	
	}
	

}
