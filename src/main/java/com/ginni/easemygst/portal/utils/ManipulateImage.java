package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManipulateImage {
	
	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	static String BASE_PATH = "C:/easemygst/";

	// Decode String into an Image
    public static String convertStringtoImage(String encodedImageStr, String dirName, String fileName) {
 
        try {
        	
        	String filePath = BASE_PATH + dirName;
        	File dir = new File(filePath);
        	dir.mkdirs();
        	
            // Decode String using Base64 Class
            byte[] imageByteArray = Base64.decodeBase64(encodedImageStr); 
 
            // Write Image into File system - Make sure you update the path
            FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName);
            imageOutFile.write(imageByteArray);
 
            imageOutFile.close();
 
            log.info("Image Successfully Stored");
            return filePath;
        } catch (FileNotFoundException fnfe) {
        	log.info("Image Path not found {}" + fnfe.getMessage());
        } catch (IOException ioe) {
        	log.info("Exception while converting the Image {}" + ioe.getMessage());
        }
        
		return null;
    }
    
}
