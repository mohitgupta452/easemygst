package com.ginni.easemygst.portal.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.time.MonthDay;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.dto.WrapperDashBoardSummaryDto.MonthYearCodeName;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.security.EncryptDecrypt;
import com.ginni.easemygst.portal.security.GSTRSAEncryptDecryptService;

public class Utility {

	protected static Logger log = LoggerFactory.getLogger(Utility.class);

	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	static final String ABNO = "012345678901234567890123456789012345678901234567890123456789";

	static final String[] months = { "January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December" };

	static final String[] monthShort = { "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov",
			"Dec" };

	static SecureRandom rnd = new SecureRandom();

	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyy");

	protected static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd MMM yyy HH:MM a");

	protected static DecimalFormat decimalFormat = new DecimalFormat("#0.00");

	public static String getMonth(int i) {

		return months[i];
	}

	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public static String getShortMonth(int i) {
		return monthShort[i];
	}

	public static boolean validateEmail(String email) {
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		System.out.println(email + " : " + matcher.matches());
		return matcher.matches();
	}

	public static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	public static String randomOtp(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(ABNO.charAt(rnd.nextInt(ABNO.length())));
		return sb.toString();
	}

	public static String formatDate(Long time) {
		Date date = Calendar.getInstance().getTime();
		date.setTime(time);
		return dateFormat.format(date);
	}

	public static String formatDateTime(Long time) {
		Date date = Calendar.getInstance().getTime();
		date.setTime(time);
		return dateTimeFormat.format(date);
	}

	public static String formatDecimalNumber(double number) {
		return decimalFormat.format(number);
	}

	public static String checkValue(String value) {

		try {
			if (value != null) {
				if (value.contains("-")) {
					value = "-" + value.replaceAll("-", "").replaceAll(",", "");
				}
				if (value.contains("'")) {
					value = value.replaceAll("'", "");
				}
				if (value.contains(",")) {
					value = value.replaceAll(",", "");
				}
				if (value.contains("&")) {
					value = value.replaceAll("&", "");
				}
				if (value.trim().length() < 1) {
					value = "";
				}
			} else {
				value = "";
			}
		} catch (Exception e) {
			return "";
		}
		return value;
	}

	public static String checkValidIntegerValue(String value) {

		try {
			if (value.trim().length() == 0)
				return "0";
		} catch (Exception e) {
			return "0";
		}

		return value;
	}

	public static String checkValidDoubleValue(String value) {

		try {
			if (value.trim().length() == 0)
				return "0.0";
		} catch (Exception e) {
			return "0.0";
		}

		return value;
	}

	public static String getFirstLetter(String word) {
		String resp = "";
		for (String s : word.trim().split("\\s+")) {
			if (s.length() > 0)
				resp += s.charAt(0);
		}
		return resp.toUpperCase();
	}

	public static String validateIntegerValue(String value) {
		try {
			Integer.parseInt(value);
			return Integer.toString(Integer.parseInt(value));
		} catch (Exception e) {
			return "0";
		}
	}

	public static String validateStringValue(String value) {
		try {
			if (value != null)
				if (value.trim().length() == 0)
					return "NA";
				else
					return value.trim();
			else
				return "NA";
		} catch (Exception e) {
			return "NA";
		}
	}

	public static String validateDoubleValue(String value) {
		try {
			Double.parseDouble(value);
			DecimalFormat decimalFormat = new DecimalFormat("#0.00");
			if (Double.valueOf(Double.parseDouble(value)).isNaN()
					|| Double.valueOf(Double.parseDouble(value)).isInfinite())
				return "0.00";
			return decimalFormat.format(Double.parseDouble(value));
		} catch (Exception e) {
			return "0.00";
		}
	}

	public static String validateFloatValue(String value) {
		try {
			Double.parseDouble(value);
			DecimalFormat decimalFormat = new DecimalFormat("#0.0");
			if (Double.valueOf(Double.parseDouble(value)).isNaN()
					|| Double.valueOf(Double.parseDouble(value)).isInfinite())
				return "0.0";
			return decimalFormat.format(Double.parseDouble(value));
		} catch (Exception e) {
			return "0.0";
		}
	}

	public static YearMonth convertStrToYearMonth(String monthYear) throws AppException {
		try {
			DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder().appendPattern("MMyyyy").toFormatter();

			return YearMonth.parse(monthYear, dateTimeFormatter);
		} catch (DateTimeParseException exception) {
			log.error("date format exception while parsing month year {}", monthYear, exception);
			throw new AppException(ExceptionCode._INVALID_PARAMETER, "Invalid month-year format " + monthYear);
		}
	}

	public static String convertTimeToDaysHours(long timeInMilliseconds) {

		long seconds = timeInMilliseconds / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		long days = hours / 24;
		String time = days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;

		String[] temp = time.split(":");
		String output = "";

		if (Integer.parseInt(temp[0]) == 0) {
			if (Integer.parseInt(temp[1]) != 0) {
				if (Integer.parseInt(temp[1]) == 1)
					output += Integer.parseInt(temp[1]) + " hour ";
				else
					output += Integer.parseInt(temp[1]) + " hours ";
			}
			if (Integer.parseInt(temp[2]) != 0) {
				if (Integer.parseInt(temp[2]) == 1)
					output += Integer.parseInt(temp[2]) + " minute ";
				else
					output += Integer.parseInt(temp[2]) + " minutes ";
			}
			if (Integer.parseInt(temp[3]) != 0) {
				if (Integer.parseInt(temp[3]) == 1)
					output += Integer.parseInt(temp[3]) + " second ";
				else
					output += Integer.parseInt(temp[3]) + " seconds ";
			}
		} else {
			if (Integer.parseInt(temp[0]) == 1)
				output = Integer.parseInt(temp[0]) + " day";
			else
				output = Integer.parseInt(temp[0]) + " days";
		}

		return output;
	}

	public static BigDecimal getBigDecimal(double d) {
		return BigDecimal.valueOf(d);
	}

	public static BigDecimal getBigDecimalScale(Double d) {
		return BigDecimal.valueOf(d).setScale(2, RoundingMode.FLOOR);
	}

	public static String unzipData(InputStream is, String key) {
		String json = null;
		try {
			TarArchiveInputStream tarInput = new TarArchiveInputStream(new GzipCompressorInputStream(is));
			TarArchiveEntry currentEntry = tarInput.getNextTarEntry();
			BufferedReader br = null;
			StringBuilder sb = new StringBuilder();
			EncryptDecrypt encDec = new GSTRSAEncryptDecryptService();
			while (currentEntry != null) {
				br = new BufferedReader(new InputStreamReader(tarInput)); // Read directly from tarInput
				System.out.println("For File = " + currentEntry.getName());
				String line;
				while ((line = br.readLine()) != null) {
					json = encDec.getDecryptedData(line.getBytes(), key);

				}
				currentEntry = tarInput.getNextTarEntry(); // You forgot to iterate to the next file
			}

		} catch (Exception e) {
			log.error("exception while calling unzip {}", e);
		}

		return json;
	}

	public static String decodedString(String str) {
		return new String(Base64.getDecoder().decode(str));
	}

	public static String getValidReturnPeriod(String currentReturnPeriod, String userFilePreference)
			throws AppException {

		if (StringUtils.isNotEmpty(userFilePreference) && "Q".equalsIgnoreCase(userFilePreference)) {

			if ("072017".equalsIgnoreCase(currentReturnPeriod))
				return currentReturnPeriod;

			currentReturnPeriod = calculateQuarterMonthYearStr(currentReturnPeriod);
		}
		return currentReturnPeriod;

	}

	public static String appendZeroWithMonthYear(int returnMonth, int returnYear) {

		return (returnMonth < 10 ? "0" + returnMonth : String.valueOf(returnMonth)) + returnYear;
	}

	public static String calculateQuarterMonthYearStr(String monthYear) throws AppException {

		YearMonth returnYearMonth = Utility.convertStrToYearMonth(monthYear);
		int returnMonth = returnYearMonth.getMonthValue();

		returnMonth = calculateQuarterMonth(returnYearMonth);

		return appendZeroWithMonthYear(returnMonth, returnYearMonth.getYear());

	}

	public static int calculateQuarterMonth(YearMonth yearMonth) throws AppException {

		int returnMonth = yearMonth.getMonthValue();

		returnMonth += (returnMonth % 3) == 0 ? 0 : 3 - (returnMonth % 3);

		return returnMonth;
	}

	public static boolean isSubmitReturn(Integer taxpayerGstinID, String monthYear, String returnType) {

		String selectSubmitReturn = "SELECT status from `gstin_action` WHERE gstin = ? and particular = ? and returnType = ?";

		try (Connection connection = DBUtils.getConnection();
				PreparedStatement subStatement = connection.prepareStatement(selectSubmitReturn);) {

			subStatement.setInt(1, taxpayerGstinID);
			subStatement.setString(2, monthYear);
			subStatement.setString(3, returnType);
			ResultSet rs = subStatement.executeQuery();

			if (rs.next()) {

				String status = rs.getString(1);
				if (StringUtils.isNotEmpty(status)
						&& ("FILED".equalsIgnoreCase(status) || "SUBMIT".equalsIgnoreCase(status)))
					return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
