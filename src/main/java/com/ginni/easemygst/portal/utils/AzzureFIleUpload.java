package com.ginni.easemygst.portal.utils;

import java.io.File;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.helper.AppException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


public class AzzureFIleUpload {
	
	public enum AZZURE_DIRECTORY {
		DATAFILE,ERRORREPORT,EWAYBILLPDF,GSTR6
	}
	
	private final static String connectionString="DefaultEndpointsProtocol=https;AccountName=blobaddcheck9a5a;AccountKey=hvHVxMKtaniq+oNr0Z745Hmsuf1sMEHDFysKq3gP0A3SpfaPWx2lkQvt4xxzNwotjJAXuh4AbjjmnqDSdCubsg==;EndpointSuffix=core.windows.net";

	
	private static final Logger _LOGGER =org.slf4j.LoggerFactory.getLogger(AzzureFIleUpload.class);
	
   private static final String _URL="http://easemygstuploadwebapp-170628123724.azurewebsites.net/rest/emgst/upload/"; 
	
	public static String uploadFileToAzzureService(AZZURE_DIRECTORY directory,File file) throws AppException {
		
		_LOGGER.info("azzure upload service directory {} , file {}",directory.toString(),file.getName());

		
		HttpResponse<JsonNode> jsonResponse;
		try {
			jsonResponse = Unirest
					.post(_URL+directory)
					.header("accept", "application/json").field("file", file).field("folderName",directory).asJson();
		} catch (UnirestException e) {
			
			_LOGGER.error("exception while upload file {} to {} azzure sesrvice ",file.getName());
			
			throw new AppException("azzure-4000","Unable to process your file");
		}
		
		_LOGGER.info("response of data file upload body  {}    body{} ",jsonResponse.getBody(),jsonResponse.getStatus());
		
		if (jsonResponse.getStatus() == 200) {
			return jsonResponse.getBody().getObject().getString("url");
		}
		throw new AppException("azzure-4000","Unable to process your file");
		
	}

/*	
	public static String uploadFileOnAzzureBlob(InputStream ins, String fileName, AZZURE_DIRECTORY folderName) {
=======
	
/*	public static String uploadFileOnAzzureBlob(InputStream ins, String fileName, AZZURE_DIRECTORY folderName) {
>>>>>>> development
		
		int status=0;
		String uploadBlobUrl=null;
		
		String subDirectory=fileName;
		_LOGGER.info(" uploadFileInputStream Method Starts Here. ");
		try{
		    CloudStorageAccount storageAccount = CloudStorageAccount.parse(connectionString);
		    _LOGGER.info(" Successfully created Storage account Object. ");

		    CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
		    _LOGGER.info("Successfully created blob client. ");

		    CloudBlobContainer container = blobClient.getContainerReference("mycontainer");
		    _LOGGER.info("Successfully get reference of container named mycontainer. ");
		    
		    BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
		   
		    containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);

		    container.uploadPermissions(containerPermissions);
		    _LOGGER.info("Set Permission successfully on container public permission is applid. ");

		    CloudBlockBlob blob = container.getBlockBlobReference(folderName+"/"+subDirectory+"/"+fileName);
		    _LOGGER.info("Successfully get blob reference with foldername "+folderName);
		    
		    BlobOutputStream blobOutputStream = blob.openOutputStream();

		    ByteArrayInputStream inputStream = new ByteArrayInputStream(IOUtils.toByteArray(ins)); // assuming buffer is a byte[] with your data

		    int next = inputStream.read();
		    while (next != -1) {
		          blobOutputStream.write(next);
		          next = inputStream.read();
		    }
		    blobOutputStream.close();
		    _LOGGER.info("Blob is uploaded with following URI "+blob.getUri());
		    URI blobUrl=blob.getUri();
		    InputStream checkUrlIns= blobUrl.toURL().openStream();
		    status=checkUrlIns.available();
		   
		    if(status>0){
		    	 _LOGGER.info("If Blob url is present returning blob url.");
		    	uploadBlobUrl=blobUrl.toString();
		    }
		}
		catch (Exception e){
		    e.printStackTrace();
		}
		return uploadBlobUrl;
	}*/
}
