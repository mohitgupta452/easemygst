package com.ginni.easemygst.portal.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.impl.FunctionalServiceImpl;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.security.EncryptDecrypt;

public class ExcelTemplate extends Excel {

	private static Properties excelConfig = null;

	private static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	static {

		try {
			loadDBConfigProperties();
			log.info("Static Block Ends.");
		} catch (IOException e) {
			log.error("Unable to load dbconfig.properties.");
		}
	}

	public ExcelTemplate(File file) throws Exception {
		super(file);
	}

	public ExcelTemplate(String path, String workBookName) throws Exception {
		super(path, workBookName);
	}

	private static void loadDBConfigProperties() throws IOException {
		excelConfig = new Properties();
		excelConfig.load(DBUtils.class.getClassLoader().getResourceAsStream("excel.properties"));
		log.info("excel.properties loaded successfully");

	}

	public static File getExcelTemplate(String returnType, String transaction) throws Exception {

		Excel ex = null;
		String workbookName = "template";
		List<String> sheets = new ArrayList<>();
		if (transaction.equalsIgnoreCase("all")) {
			String value = excelConfig.getProperty(String.format("template.return.%s.all", returnType));
			String[] temp = value.split(",");
			sheets.addAll(Arrays.asList(temp));
			workbookName = returnType + "_template.xlsx";
		} else {
			sheets.add(transaction);
			workbookName = transaction + "_template.xlsx";
		}

		try {
			ex = new ExcelTemplate(ManipulateFile.getParentDirBasePath(), workbookName);
		} catch (Exception e) {
			log.error("Unable to create excel template for {}", workbookName);
			e.printStackTrace();
		}

		for (String string : sheets) {
			log.debug("sheet--{}",string);
			String value = excelConfig
					.getProperty(String.format("template.return.%s.transaction.%s", returnType, string));
			String[] temp = value.split(",");
			ex.setHeaders(Arrays.asList(temp), string);
		}

		ex.removeSheetAt(0);
		return ex.generateExcelFile();
	}

	public static File getExcelTemplateData(String returnType, String transaction, List<List<String>> data)
			throws Exception {

		Excel ex = null;
		String workbookName = "template";
		List<String> sheets = new ArrayList<>();
		if (transaction.equalsIgnoreCase("all")) {
			String value = excelConfig.getProperty(String.format("template.return.%s.all", returnType));
			String[] temp = value.split(",");
			sheets.addAll(Arrays.asList(temp));
			workbookName = Utility.randomString(10) + ":" + returnType + "_data.xlsx";
		} else {
			sheets.add(transaction);
			workbookName = Utility.randomString(10) + ":" + transaction + "_data.xlsx";
		}

		try {
			ex = new ExcelTemplate(ManipulateFile.getParentDirBasePath(), workbookName);
		} catch (Exception e) {
			log.error("Unable to create excel template for {}", workbookName);
			e.printStackTrace();
		}

		for (String string : sheets) {
			String value = excelConfig
					.getProperty(String.format("template.return.%s.transaction.%s", returnType, string));
			String[] temp = value.split(",");
			ex.setHeaders(Arrays.asList(temp), string);
			ex.setValues(data, string);
		}

		ex.removeSheetAt(0);
		return ex.generateExcelFile();
	}

	public static File getExcelTemplateErrorData(String returnType, String transaction, List<List<String>> data)
			throws Exception {

		Excel ex = null;
		String workbookName = "template";
		List<String> sheets = new ArrayList<>();
		if (transaction.equalsIgnoreCase("all")) {
			String value = excelConfig.getProperty(String.format("template.return.%s.all", returnType));
			String[] temp = value.split(",");
			sheets.addAll(Arrays.asList(temp));
			workbookName = Utility.randomString(10) + ":" + returnType + "_data.xlsx";
		} else {
			sheets.add(transaction);
			workbookName = Utility.randomString(10) + ":" + transaction + "_data.xlsx";
		}

		try {
			ex = new ExcelTemplate(ManipulateFile.getParentDirBasePath(), workbookName);
		} catch (Exception e) {
			log.error("Unable to create excel template for {}", workbookName);
			e.printStackTrace();
		}

		for (String string : sheets) {
			String value = excelConfig
					.getProperty(String.format("template.return.%s.transaction.%s", returnType, string));
			String[] temp = value.split(",");
			List<String>headers=new ArrayList<>(Arrays.asList(temp));
			headers.add("Error Message");
			ex.setHeaders(headers, string);
			ex.setValues(data, string);
		}

		ex.removeSheetAt(0);
		return ex.generateExcelFile();
	}
	
	public static File getExcelTemplateData(String returnType, String transaction, Map<String, List<List<String>>> map)
			throws Exception {

		Excel ex = null;
		String workbookName = "template";
		List<String> sheets = new ArrayList<>();
		if (transaction.equalsIgnoreCase("all")) {
			String value = excelConfig.getProperty(String.format("template.return.%s.all", returnType));
			String[] temp = value.split(",");
			sheets.addAll(Arrays.asList(temp));
			workbookName = Utility.randomString(10) + ":" + returnType + "_data.xlsx";
		} else {
			sheets.add(transaction);
			workbookName = Utility.randomString(10) + ":" + transaction + "_data.xlsx";
		}

		try {
			ex = new ExcelTemplate(ManipulateFile.getParentDirBasePath(), workbookName);
		} catch (Exception e) {
			log.error("Unable to create excel template for {}", workbookName);
			e.printStackTrace();
		}

		for (String string : sheets) {
			String value = excelConfig
					.getProperty(String.format("template.return.%s.transaction.%s", returnType, string));
			String[] temp = value.split(",");
			Iterator it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				ex.setHeaders(Arrays.asList(temp), pair.getKey().toString());
				if(!Objects.isNull(pair.getValue())) 
					ex.setValues((List<List<String>>) pair.getValue(), string);
			}
		}

		ex.removeSheetAt(0);
		return ex.generateExcelFile();
	}

	public static File getExcelFromListData(List<List<String>> data)
			throws Exception {

		Excel ex = null;
		String workbookName = "template";
		List<String> sheets = new ArrayList<>();

		sheets.add("data");
		workbookName = Utility.randomString(10) + ":" + "requested_data.xlsx";

		try {
			ex = new ExcelTemplate(ManipulateFile.getParentDirBasePath(), workbookName);
		} catch (Exception e) {
			log.error("Unable to create excel template for {}", workbookName);
			e.printStackTrace();
		}

		for (String string : sheets) {
			ex.setHeaders(data.get(0), string);
			data.remove(0);
			ex.setValues(data, string);
		}

		ex.removeSheetAt(0);
		return ex.generateExcelFile();
	}

	public static String getExcelTemplateHeaders(String returnType, String transaction) throws Exception {

		String value = excelConfig
				.getProperty(String.format("template.return.%s.transaction.%s", returnType, transaction));

		return value;
	}

	public static Map<String, String> getExcelSheetTemplateHeaders(String returnType, String transaction)
			throws Exception {

		Map<String, String> headers = new HashMap<String, String>();

		if (!transaction.equalsIgnoreCase("all")) {
			String value = excelConfig
					.getProperty(String.format("template.return.%s.transaction.%s", returnType, transaction));
			headers.put(transaction, value);
		} else {
			String value = excelConfig.getProperty(String.format("template.return.%s.%s",returnType,transaction));
			String[] transactions = value.split(",");
			for (String string : transactions) {
				String heads = excelConfig
						.getProperty(String.format("template.return.%s.transaction.%s", returnType, string));
				headers.put(string, heads);
			}
		}

		return headers;

	}

	
	public static  String getSheetName(String returnType, String transactionType)throws AppException {
		Properties props = new Properties();
		InputStream inputStream = null;
		String fileName = "/transactionSampleExcel.properties";
		String field=null;
		try {
			inputStream = DBUtils.class.getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IOException();
			props.load(inputStream);
			 field = props.getProperty(StringUtils.lowerCase(returnType + "." + transactionType));
			 if(!StringUtils.isEmpty(field))
				 field.trim();
			if ("all".equalsIgnoreCase(field)) {
				AppException ae= new AppException();
				ae.setMessage("Sample file is ");
			} 
		}

		catch (IOException e) {
			log.error("file not found with this name--{}", fileName);
		}

		return field;

	}
	
	
	public static File getSampleData(GstinTransactionDTO gstinTransactionDto) throws AppException{
		String fileName=ExcelTemplate.getSheetName(gstinTransactionDto.getReturnType(), gstinTransactionDto.getTransactionType());
		log.debug("getsample data start");
		File file=null;
		try{
		//file=new File(Thread.currentThread().getContextClassLoader().getResource("sampleexcels"+File.separator+fileName).getFile());
			//file=new File(FunctionalServiceImpl.class.getClassLoader().getResource(File.separator+"sampleexcels"+File.separator+fileName).getFile());
			file=ExcelTemplate.getResourceFile(File.separator+"sampleexcels"+File.separator+fileName, fileName);
			/*InputStream is=Thread.currentThread().getContextClassLoader().getResourceAsStream(File.separator+"sampleexcels"+File.separator+fileName);
			OutputStream os=new FileOutputStream("temp.xlsx");
			IOUtils.copy(is, os);*/
		log.debug("file info {} ",file.getPath() );

		}
		catch(Exception e){
			AppException ae= new AppException();
			ae.setMessage("Sample file not available ");
			log.debug("sample file nnot available");

			throw ae;
		}
		log.debug("getsample data end");

		return file;
		
	}
	
	
	@SuppressWarnings("finally")
	private static File getResourceFile(String template, String fileName) {
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(template);

		File outFile = new File(ManipulateFile.getParentDirBasePath()+"/"+fileName);
		
		if(outFile.exists())
			return outFile;
		
		OutputStream outputStream = null;

		try {
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(outFile);

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			return outFile;
		}
	}
	
	
	public static File getPublicNotificationFile(GstinTransactionDTO gstinTransactionDto,String fileName) throws AppException{
		
		File tempFile=null;
		try{
			Path path=Paths.get(ManipulateFile.getParentDirBasePath()+"/notificationpdfs"+File.separator+fileName);
			
		/*URL url=new URL("/home/tcinv1052/Desktop/"+fileName);
		 tempFile=new File(("notificationFile"));
		 FileUtils.copyURLToFile(url, tempFile);*/
			tempFile=path.toFile();
		
		} catch (Exception e) {
			log.error("exception in get public notification {}",e);
		}
		
		return tempFile;
		
	}
	
	
	
	public static Map<String, String> getExcelSheetTemplateHeadersConsolidated(String returnType, String transaction)
			throws Exception {

		Map<String, String> headers = new HashMap<String, String>();

		if (transaction.equalsIgnoreCase("all")) {
			String value = excelConfig
					.getProperty(String.format("template.return.%s.transaction.%s", returnType, transaction));
			headers.put(transaction, value);
		}

		return headers;

	}
}
