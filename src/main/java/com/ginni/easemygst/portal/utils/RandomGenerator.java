package com.ginni.easemygst.portal.utils;

import java.security.SecureRandom;

public class RandomGenerator {

	private static final String ALPHA_NUMERIC_STRING = "AB@CDEF*GHIJKLMNOPQRSTUVWXY?Z@0!1#234*5#6@7!8*9";

	public static final String generateOTP() {
		return String.format("%06d", (new SecureRandom()).nextInt(999999) + 1);
	}

	public static final String randomAlphaNumeric(int count) {

		StringBuilder builder = new StringBuilder();

		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

}