package com.ginni.easemygst.portal.business.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.InterestAndLateFeePaybleDto.IntrLateFeeDetailDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.InwardSupplyDto.InwardSupplyDetailDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.ItcEligibilityDto.ItcDetailDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.SupplyInterDto.DetailInterDto;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.Detail;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.Gstr3BTransactionType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.InwardSupplyType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.ItcDetailType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.OutwardSupplyDetail;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.SupplyDetail;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.ZeroDetail;
import com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew;

import lombok.Data;

public @Data class Gstr3bDto {
	@JsonProperty("sup_details")
	private SupplyDetailDto supplyDetail=new SupplyDetailDto();
	
	@JsonProperty("itc_elg")
	private ItcEligibilityDto itcDetail=new ItcEligibilityDto();
	
	@JsonProperty("inter_sup")
	private SupplyInterDto supplyInter;
	
	@JsonProperty("inward_sup")
	private InwardSupplyDto inwardSupply;
	
	@JsonProperty("intr_ltfee")
	private InterestAndLateFeePaybleDto interLateFeePayble;
	
	
	public Gstr3bDto(){
		
	}
	
public static class SupplyDetailDto{
		
		@JsonProperty("osup_det")
		 private OutwardSupplyDetail outTaxableSup;
		 
		@JsonProperty("osup_zero")
		 private OutwardSupplyDetail outSupZeroRated;

		@JsonProperty("osup_nil_exmp")
		 private OutwardSupplyDetail outSupNilRatedExempted;

		@JsonProperty("isup_rev")
		 private OutwardSupplyDetail inwardSupRevCharge;

		@JsonProperty("osup_nongst")
		 private OutwardSupplyDetail outSupNonGst;
		
		public SupplyDetailDto(){
			this.outTaxableSup=new OutwardSupplyDetail();
			this.outSupZeroRated=new OutwardSupplyDetail();
			this.outSupNilRatedExempted=new OutwardSupplyDetail();
			this.inwardSupRevCharge=new OutwardSupplyDetail();
			this.outSupNonGst=new OutwardSupplyDetail();

			
		}

		public OutwardSupplyDetail getOutTaxableSup() {
			return outTaxableSup;
		}

		public void setOutTaxableSup(OutwardSupplyDetail outTaxableSup) {
			this.outTaxableSup = outTaxableSup;
		}

		public OutwardSupplyDetail getOutSupZeroRated() {
			return outSupZeroRated;
		}

		public void setOutSupZeroRated(OutwardSupplyDetail outSupZeroRated) {
			this.outSupZeroRated = outSupZeroRated;
		}

		public OutwardSupplyDetail getOutSupNilRatedExempted() {
			return outSupNilRatedExempted;
		}

		public void setOutSupNilRatedExempted(OutwardSupplyDetail outSupNilRatedExempted) {
			this.outSupNilRatedExempted = outSupNilRatedExempted;
		}

		public OutwardSupplyDetail getInwardSupRevCharge() {
			return inwardSupRevCharge;
		}

		public void setInwardSupRevCharge(OutwardSupplyDetail inwardSupRevCharge) {
			this.inwardSupRevCharge = inwardSupRevCharge;
		}

		public OutwardSupplyDetail getOutSupNonGst() {
			return outSupNonGst;
		}

		public void setOutSupNonGst(OutwardSupplyDetail outSupNonGst) {
			this.outSupNonGst = outSupNonGst;
		}
	}
	
	
	public Gstr3bDto(List<TblOutwardCombinedNew> part1AndPart5s, List<TblOutwardCombinedNew> part4s,
			List<TblOutwardCombinedNew> part3s/*,List<TblOutwardCombinedNew> part2s*/) {
		this.supplyDetail=new SupplyDetailDto();
		this.supplyInter=new SupplyInterDto();
		this.itcDetail=new ItcEligibilityDto();
		this.inwardSupply=new InwardSupplyDto();
		this.interLateFeePayble=new InterestAndLateFeePaybleDto();
		
		List<TblOutwardCombinedNew>datas=new ArrayList<>();
		datas.addAll(part1AndPart5s);
		datas.addAll(part4s);
		
		for(TblOutwardCombinedNew part:part1AndPart5s){
			Gstr3BTransactionType type=Gstr3BTransactionType.valueOf(part.getTransaction_Type());
			switch(type){
			case PART1A:
				this.getSupplyDetail().setOutTaxableSup(new OutwardSupplyDetail(part));
				break;
				
			case PART1B:
				this.getSupplyDetail().setOutSupZeroRated(new OutwardSupplyDetail(part));
				break;
			case PART1C:
				this.getSupplyDetail().setOutSupNilRatedExempted(new OutwardSupplyDetail(part));
				break;
				
			case PART1D:
				this.getSupplyDetail().setInwardSupRevCharge(new OutwardSupplyDetail(part));
				break;
			case PART1E:
				this.getSupplyDetail().setOutSupNonGst(new OutwardSupplyDetail(part));
				break;
				
			case PART5A:
				this.getInterLateFeePayble().setInterestLateFeeDetails(new IntrLateFeeDetailDto(part));;
				break;
				
			case PART5B:
				this.getInterLateFeePayble().setLateFeeDetails(new IntrLateFeeDetailDto(part));;
				break;
				
				
				
			 default:
				 System.out.println("invalid part type");
			 
				
			}
			
		}
		ArrayList<TblOutwardCombinedNew> combinedParts = new ArrayList<>();
		ItcEligibilityDto itc = new ItcEligibilityDto();
		this.setItcDetail(itc);
		SupplyInterDto inter = new SupplyInterDto();
		this.setSupplyInter(inter);
		combinedParts.addAll(part3s);
		//combinedParts.addAll(part2s);
		double igstAvailable=0;
		double cgstAvailable=0;
		double sgstAvailable=0;
		double cessAvailable=0;
		
		double igstReversed=0;
		double cgstReversed=0;;
		double sgstReversed=0;;
		double cessReversed=0;;
		ItcDetailDto netItc=new ItcDetailDto();


		
		
		
		for (TblOutwardCombinedNew part : combinedParts) {
			BaseAmountDto bto = new BaseAmountDto(part);
			Gstr3BTransactionType type = Gstr3BTransactionType.valueOf(part.getTransaction_Type());
			

			switch (type) {
			case PART3A:
				itc.setImpg(bto);
				igstAvailable=igstAvailable+bto.getIgstAmount();
				 cgstAvailable=cgstAvailable+bto.getCgstAmount();
				 sgstAvailable=sgstAvailable+bto.getSgstAmount();
				 cessAvailable=cessAvailable+bto.getCessAmount();
				break;

			case PART3B:
				itc.setImps(bto);
				igstAvailable=igstAvailable+bto.getIgstAmount();
				 cgstAvailable=cgstAvailable+bto.getCgstAmount();
				 sgstAvailable=sgstAvailable+bto.getSgstAmount();
				 cessAvailable=cessAvailable+bto.getCessAmount();
				break;

			case PART3C:
				itc.setIsrc(bto);
				igstAvailable=igstAvailable+bto.getIgstAmount();
				 cgstAvailable=cgstAvailable+bto.getCgstAmount();
				 sgstAvailable=sgstAvailable+bto.getSgstAmount();
				 cessAvailable=cessAvailable+bto.getCessAmount();

				break;
			case PART3D:
				itc.setIsd(bto);
				igstAvailable=igstAvailable+bto.getIgstAmount();
				 cgstAvailable=cgstAvailable+bto.getCgstAmount();
				 sgstAvailable=sgstAvailable+bto.getSgstAmount();
				 cessAvailable=cessAvailable+bto.getCessAmount();

				break;
			case PART3E:
				// itc.setRules(bto);
				igstAvailable=igstAvailable+bto.getIgstAmount();
				 cgstAvailable=cgstAvailable+bto.getCgstAmount();
				 sgstAvailable=sgstAvailable+bto.getSgstAmount();
				 cessAvailable=cessAvailable+bto.getCessAmount();

				break;

			case PART3F:
				itc.setOther(bto);
				igstAvailable=igstAvailable+bto.getIgstAmount();
				 cgstAvailable=cgstAvailable+bto.getCgstAmount();
				 sgstAvailable=sgstAvailable+bto.getSgstAmount();
				 cessAvailable=cessAvailable+bto.getCessAmount();
				break;

			case PART3G:{
				itc.getItcReversed().setRules(bto);
				 igstReversed=igstReversed+bto.getIgstAmount();
				 cgstReversed=cgstReversed+bto.getCgstAmount();
				 sgstReversed=sgstReversed+bto.getSgstAmount();
				 cessReversed=cessReversed+bto.getCessAmount();
				
			}

				break;

			case PART3H:
			{
				itc.getItcReversed().setOther(bto);;
				 igstReversed=igstReversed+bto.getIgstAmount();
				 cgstReversed=cgstReversed+bto.getCgstAmount();
				 sgstReversed=sgstReversed+bto.getSgstAmount();
				 cessReversed=cessReversed+bto.getCessAmount();

			}
				break;
				
			case PART3J:{
				itc.getIneligibleItc().setRules(bto);;
			}

				break;

			case PART3K:
			{
				itc.getIneligibleItc().setOther(bto);;

			}
				break;
				
				
			/*case PART2A:
				DetailInterDto unergDetails = new DetailInterDto(part);
				inter.getUnregPersonDetails().add(unergDetails);
				break;
				
			case PART2B:
				DetailInterDto compTaxablePersonDetails = new DetailInterDto(part);
				inter.getCompTaxablePersonDetails().add(compTaxablePersonDetails);
				break;
				
			case PART2C:
				DetailInterDto uinDetails = new DetailInterDto(part);
				inter.getUinDetails().add(uinDetails);
				break;*/

			default:
				System.out.println("invalid part type");

			}
			
			
		}
		netItc.setIgstAmount(igstAvailable-igstReversed);
		netItc.setCgstAmount(cgstAvailable-cgstReversed);
		netItc.setSgstAmount(sgstAvailable-sgstReversed);
		netItc.setCessAmount(cessAvailable-cessReversed);


		
		
		this.getItcDetail().setNetItcAvailable(netItc);
		Map<String, List<TblOutwardCombinedNew>> groupByPartMap = part4s.stream()
				.collect(Collectors.groupingBy(TblOutwardCombinedNew::getTransaction_Type));
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART2A.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART2A.toString())){
			this.getSupplyInter().getUnregPersonDetails().add(new DetailInterDto(part));
			}
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART2B.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART2B.toString())){
			this.getSupplyInter().getCompTaxablePersonDetails().add(new DetailInterDto(part));
			}
		}else{
			this.getSupplyInter().getCompTaxablePersonDetails().add(new DetailInterDto());
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART2C.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART2C.toString())){
			this.getSupplyInter().getUinDetails().add(new DetailInterDto(part));
			}
		}
		/*if(groupByPartMap.containsKey(Gstr3BTransactionType.PART3A.toString())){
			BaseAmountDto consolidated=new BaseAmountDto();
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART3A.toString())){
				consolidated.setIgstAmount(consolidated.getIgstAmount()+part.getIgst());
				consolidated.setCgstAmount(consolidated.getCgstAmount()+part.getCgst());
				consolidated.setSgstAmount(consolidated.getIgstAmount()+part.getSgst());
				consolidated.setTaxableValue(consolidated.getTaxableValue()+part.getTaxable_Value());
			}
			this.getItcDetail().setImpg(consolidated);

		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART3B.toString())){
			BaseAmountDto consolidated=new BaseAmountDto();
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART3B.toString())){
				consolidated.setIgstAmount(consolidated.getIgstAmount()+part.getIgst());
			}
			this.getItcDetail().setImpg(consolidated);
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART3C.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART3C.toString())){
			//this.getItcDetail().setNetItcAvailable(new ItcDetail(part));
				this.getItcDetail().getAvailableItc().add(new ItcDetail(part));
			}
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART3D.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART3D.toString())){
			//this.getItcDetail().getIneligibleItc().add(new ItcDetail(part));
				this.getItcDetail().getAvailableItc().add(new ItcDetail(part));
			}
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART3E.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART3E.toString())){
			//this.getItcDetail().setNetItcAvailable(new ItcDetail(part));
				this.getItcDetail().getAvailableItc().add(new ItcDetail(part));
			}
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART3F.toString())){
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART3F.toString())){
			//this.getItcDetail().getIneligibleItc().add(new ItcDetail(part));
				this.getItcDetail().getAvailableItc().add(new ItcDetail(part));
			}
		}*/
		
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART4A.toString())){
			InwardSupplyDetailDto consolidated=new InwardSupplyDetailDto();
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART4A.toString())){
			InwardSupplyDetailDto temp=new InwardSupplyDetailDto(part);
			consolidated.setInter(consolidated.getInter()+temp.getInter());
			consolidated.setIntra(consolidated.getIntra()+temp.getIntra());
			consolidated.setInwardSupType(temp.getInwardSupType());
/*			this.getInwardSupply().getInwardSupDetail().add(new InwardSupplyDetailDto(part));
*/			}
			this.getInwardSupply().getInwardSupDetail().add(consolidated);

		}else{
			TblOutwardCombinedNew tempPart=new TblOutwardCombinedNew();
			tempPart.setSupply_Type(SupplyType.INTER.toString());
			tempPart.setTransaction_Type(Gstr3BTransactionType.PART4A.toString());
			this.getInwardSupply().getInwardSupDetail().add(new InwardSupplyDetailDto(tempPart));
		}
		if(groupByPartMap.containsKey(Gstr3BTransactionType.PART4B.toString())){
			InwardSupplyDetailDto consolidated=new InwardSupplyDetailDto();
			for(TblOutwardCombinedNew part:groupByPartMap.get(Gstr3BTransactionType.PART4B.toString())){
				InwardSupplyDetailDto temp=new InwardSupplyDetailDto(part);
				consolidated.setInter(consolidated.getInter()+temp.getInter());
				consolidated.setIntra(consolidated.getIntra()+temp.getIntra());
				consolidated.setInwardSupType(temp.getInwardSupType());
			//this.getInwardSupply().getInwardSupDetail().add(new InwardSupplyDetailDto(part));
			}
			this.getInwardSupply().getInwardSupDetail().add(consolidated);

		}else{
			TblOutwardCombinedNew tempPart=new TblOutwardCombinedNew();
			tempPart.setTransaction_Type(Gstr3BTransactionType.PART4B.toString());
			tempPart.setSupply_Type(SupplyType.INTER.toString());
			this.getInwardSupply().getInwardSupDetail().add(new InwardSupplyDetailDto(tempPart));
		}
		
		
		
		
		
	}
	
	
	public static class ItcEligibilityDto{
		
		@JsonProperty("impg")
		private BaseAmountDto impg=new BaseAmountDto(); 
		
		@JsonProperty("imps")
		private BaseAmountDto imps=new BaseAmountDto(); 
		
		@JsonProperty("isrc")
		private BaseAmountDto isrc=new BaseAmountDto(); 
		
		@JsonProperty("isd")
		private BaseAmountDto isd=new BaseAmountDto(); 
		
		@JsonProperty("rul")
		private BaseAmountDto rules=new BaseAmountDto();
		
		@JsonProperty("other")
		private BaseAmountDto other=new BaseAmountDto(); 
		
		
		@JsonProperty("itc_rev")
		private ItcRevInelg itcReversed=new ItcRevInelg();

		@JsonProperty("itc_net")
		private ItcDetailDto netItcAvailable=new ItcDetailDto();
		
		@JsonProperty("itc_inelg")
		private ItcRevInelg ineligibleItc=new ItcRevInelg();
		
		
		
		
		public BaseAmountDto getImpg() {
			return impg;
		}


		public void setImpg(BaseAmountDto impg) {
			this.impg = impg;
		}


		public BaseAmountDto getImps() {
			return imps;
		}


		public void setImps(BaseAmountDto imps) {
			this.imps = imps;
		}


		public BaseAmountDto getIsrc() {
			return isrc;
		}


		public void setIsrc(BaseAmountDto isrc) {
			this.isrc = isrc;
		}


		public BaseAmountDto getIsd() {
			return isd;
		}


		public void setIsd(BaseAmountDto isd) {
			this.isd = isd;
		}


		public BaseAmountDto getRules() {
			return rules;
		}


		public void setRules(BaseAmountDto rules) {
			this.rules = rules;
		}


		public BaseAmountDto getOther() {
			return other;
		}


		public void setOther(BaseAmountDto other) {
			this.other = other;
		}


		public ItcRevInelg getItcReversed() {
			return itcReversed;
		}


		public void setItcReversed(ItcRevInelg itcReversed) {
			this.itcReversed = itcReversed;
		}


		public ItcDetailDto getNetItcAvailable() {
			return netItcAvailable;
		}


		public void setNetItcAvailable(ItcDetailDto netItcAvailable) {
			this.netItcAvailable = netItcAvailable;
		}


		public ItcRevInelg getIneligibleItc() {
			return ineligibleItc;
		}


		public void setIneligibleItc(ItcRevInelg ineligibleItc) {
			this.ineligibleItc = ineligibleItc;
		}


		public@Data static class ItcRevInelg{
			@JsonProperty("rul")
			private BaseAmountDto rules=new BaseAmountDto();
			
			@JsonProperty("other")
			private BaseAmountDto other=new BaseAmountDto(); 
			
			public ItcRevInelg(){
				super();
			}
			}
		
		
	//	@JsonInclude(Include.NON_NULL)
		public@Data static class ItcDetailDto{
			@JsonProperty("ty")
			private  ItcDetailType itcDetailType;
			
			@JsonProperty("iamt")
			private  double igstAmount;

			@JsonProperty("camt")
			private  double cgstAmount;

			@JsonProperty("samt")
			private  double sgstAmount;
			
			@JsonProperty("csamt")
			private  double cessAmount;
			
			
			public ItcDetailDto(){
				super();
				
			}
			
			public ItcDetailDto(TblOutwardCombinedNew part){
				super();
				Gstr3BTransactionType type = Gstr3BTransactionType.valueOf(part.getTransaction_Type());
				ItcDetailType itcType=null;

				switch (type) {
				case PART3A:
					itcType=ItcDetailType.IMPG;
					break;

				case PART3B:
					itcType=ItcDetailType.IMPS;
					break;

				case PART3C:
					itcType=ItcDetailType.ISRC;
					break;
				case PART3D:
					itcType=ItcDetailType.ISD;
					break;
				case PART3E:
					itcType=ItcDetailType.RUL;
					break;

				case PART3F:
					itcType=ItcDetailType.OTH;
					break;
					
				case PART3G:
					itcType=ItcDetailType.RUL;
					break;

				case PART3H:
					itcType=ItcDetailType.OTH;
					break;

				default:
					System.out.println("invalid part type");

				}
				this.itcDetailType=itcType;
				this.igstAmount=part.getIgst();
				this.cgstAmount=part.getCgst();
				this.sgstAmount=part.getSgst();
				this.cessAmount=part.getCess();
			}
		}
		
	}
	
	
	public static class SupplyInterDto{
		
		@JsonProperty("unreg_details")
		private List<DetailInterDto>unregPersonDetails=new ArrayList<>();
		
		@JsonProperty("comp_details")
		private List<DetailInterDto>compTaxablePersonDetails=new ArrayList<>();

		@JsonProperty("uin_details")
		private List<DetailInterDto>uinDetails=new ArrayList<>();
		
		
		public @Data  static class DetailInterDto{
			
			@JsonProperty("nature_of_supply")
			private String natureOfSupply;
			
			@JsonProperty("pos")
			private String pos;
			
			@JsonProperty("txval")
			private  double taxableValue;
			
			@JsonProperty("iamt")
			private  double igstAmount;
			
			public DetailInterDto(){
				
				
			}
			
			public DetailInterDto(TblOutwardCombinedNew part){
				this.igstAmount=part.getIgst();
				this.taxableValue=part.getTaxable_Value();
				this.pos=part.getTo_State();
				if(!StringUtils.isEmpty(pos)){
					this.pos=pos.length()==1?"0"+this.pos:this.pos;
				}
			}

			
			
			
		}


		public List<DetailInterDto> getUnregPersonDetails() {
			return unregPersonDetails;
		}


		public void setUnregPersonDetails(List<DetailInterDto> unregPersonDetails) {
			this.unregPersonDetails = unregPersonDetails;
		}


		public List<DetailInterDto> getCompTaxablePersonDetails() {
			return compTaxablePersonDetails;
		}


		public void setCompTaxablePersonDetails(List<DetailInterDto> compTaxablePersonDetails) {
			this.compTaxablePersonDetails = compTaxablePersonDetails;
		}


		public List<DetailInterDto> getUinDetails() {
			return uinDetails;
		}


		public void setUinDetails(List<DetailInterDto> uinDetails) {
			this.uinDetails = uinDetails;
		}


		
		
	}


public  static class InwardSupplyDto{

	@JsonProperty("isup_details")
	List<InwardSupplyDetailDto>inwardSupDetail=new ArrayList<>();
	
	public@Data static class InwardSupplyDetailDto{
		@JsonProperty("ty")
		private  InwardSupplyType inwardSupType;
		
		@JsonProperty("inter")
		private  double inter;

		@JsonProperty("intra")
		private  double intra;
		
		/*public InwardSupplyDetail(InwardSupplyType supType,double inter,double intra){
			this.inwardSupType=supType;
			this.setInter(inter);
			this.setIntra(intra);
		}*/
		
		public InwardSupplyDetailDto(){
			
		}
		
		public InwardSupplyDetailDto(TblOutwardCombinedNew part){
			if(Gstr3BTransactionType.PART4A.toString().equalsIgnoreCase(part.getTransaction_Type())){
				this.inwardSupType=InwardSupplyType.GST;
			}else if(Gstr3BTransactionType.PART4B.toString().equalsIgnoreCase(part.getTransaction_Type())){
				this.inwardSupType=InwardSupplyType.NONGST;
			}
			
			if(SupplyType.INTER.toString().equalsIgnoreCase(part.getSupply_Type())){
				this.setInter(part.getTaxable_Value());
			}else if(SupplyType.INTRA.toString().equalsIgnoreCase(part.getSupply_Type())){
				this.setIntra(part.getTaxable_Value());
			}
			
		}
	}

	public List<InwardSupplyDetailDto> getInwardSupDetail() {
		return inwardSupDetail;
	}

	public void setInwardSupDetail(List<InwardSupplyDetailDto> inwardSupDetail) {
		this.inwardSupDetail = inwardSupDetail;
	}

	
	
	
}


public  static class InterestAndLateFeePaybleDto{
	@JsonProperty("intr_details")
	public IntrLateFeeDetailDto interestLateFeeDetails=new IntrLateFeeDetailDto();
	
	@JsonProperty("ltFee_details")
	public IntrLateFeeDetailDto lateFeeDetails=new IntrLateFeeDetailDto();
	
	public@Data static class IntrLateFeeDetailDto{
		
		@JsonProperty("iamt")
		private  double igstAmount;

		@JsonProperty("camt")
		private  double cgstAmount;

		@JsonProperty("samt")
		private  double sgstAmount;
		
		@JsonProperty("csamt")
		private  double cessAmount;

		@JsonProperty("txval")
		private  double taxableValue;
		
		public IntrLateFeeDetailDto(){
			
		}
		public IntrLateFeeDetailDto(TblOutwardCombinedNew part){
			super();
			this.igstAmount=part.getIgst();
			this.cgstAmount=part.getCgst();
			this.sgstAmount=part.getSgst();
			this.cessAmount=part.getCess();
			this.taxableValue=part.getTaxable_Value();
		}
		
	}

	public IntrLateFeeDetailDto getInterestLateFeeDetails() {
		return interestLateFeeDetails;
	}

	public void setInterestLateFeeDetails(IntrLateFeeDetailDto interestLateFeeDetails) {
		this.interestLateFeeDetails = interestLateFeeDetails;
	}

	public IntrLateFeeDetailDto getLateFeeDetails() {
		return lateFeeDetails;
	}

	public void setLateFeeDetails(IntrLateFeeDetailDto lateFeeDetails) {
		this.lateFeeDetails = lateFeeDetails;
	}

	
	
}

	public@Data static class BaseAmountDto{
		@JsonProperty("txval")
		private  double taxableValue;
		
		@JsonProperty("iamt")
		private  double igstAmount;

		@JsonProperty("camt")
		private  double cgstAmount;

		@JsonProperty("samt")
		private  double sgstAmount;
		
		@JsonProperty("csamt")
		private  double cessAmount;
		
		public BaseAmountDto(double igst,double cgst,double sgst,double cess){
			this.igstAmount=igst;
			this.cgstAmount=cgst;
			this.sgstAmount=sgst;
			this.cessAmount=cess;
		}
		
		
		public BaseAmountDto(TblOutwardCombinedNew part){
			this.igstAmount=part.getIgst();
			this.cgstAmount=part.getCgst();
			this.sgstAmount=part.getSgst();
			this.cessAmount=part.getCess();
			this.taxableValue=part.getTaxable_Value();
		}
		public BaseAmountDto(){
			
		}

	}
}
