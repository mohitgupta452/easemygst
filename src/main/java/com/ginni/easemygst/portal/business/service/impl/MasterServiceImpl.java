package com.ginni.easemygst.portal.business.service.impl;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.BusinessTypeDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.HelpContentDTO;
import com.ginni.easemygst.portal.business.entity.HsnSacDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService;
import com.ginni.easemygst.portal.constant.Constant;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.BusinessType;
import com.ginni.easemygst.portal.persistence.entity.GstReturn;
import com.ginni.easemygst.portal.persistence.entity.HelpContent;
import com.ginni.easemygst.portal.persistence.entity.HsnChapter;
import com.ginni.easemygst.portal.persistence.entity.HsnSac;
import com.ginni.easemygst.portal.persistence.entity.HsnSubChapter;
import com.ginni.easemygst.portal.persistence.entity.ReturnTransaction;
import com.ginni.easemygst.portal.persistence.service.CrudService;

@Stateless
public class MasterServiceImpl implements MasterService {

	@Inject
	private Logger log;

	@Inject
	private FinderService finderService;

	@Inject
	private CrudService crudService;

	@Inject
	private UserBean userBean;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public GstReturnDTO createGstReturn(GstReturnDTO gstReturnDTO) throws AppException {

		if (!(Objects.isNull(gstReturnDTO) && StringUtils.isEmpty(gstReturnDTO.getName())
				&& StringUtils.isEmpty(gstReturnDTO.getCode()) && StringUtils.isEmpty(gstReturnDTO.getFrequency())
				&& Objects.isNull(gstReturnDTO.getDueDate()))) {

			GstReturn gstReturn = (GstReturn) EntityHelper.convert(gstReturnDTO, GstReturn.class);
			gstReturn.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			gstReturn.setCreationIpAddress(userBean.getIpAddress());
			gstReturn.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			gstReturn.setActive((byte) 1);
			gstReturn.setStatus(Constant.Status.ACTIVE.toString());

			gstReturn = crudService.create(gstReturn);
			gstReturnDTO = (GstReturnDTO) EntityHelper.convert(gstReturn, GstReturnDTO.class);

			return gstReturnDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public GstReturnDTO updateGstReturn(GstReturnDTO gstReturnDTO) throws AppException {

		if (!(Objects.isNull(gstReturnDTO) && Objects.isNull(gstReturnDTO.getId()))) {

			GstReturn gstReturnEm = em.getReference(GstReturn.class, gstReturnDTO.getId());

			GstReturn gstReturn = (GstReturn) EntityHelper.convert(gstReturnDTO, GstReturn.class);
			gstReturn.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			gstReturn.setUpdationIpAddress(userBean.getIpAddress());
			gstReturn.setActive((byte) 1);
			gstReturn.setStatus(Constant.Status.ACTIVE.toString());
			gstReturn.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

			gstReturnEm = EntityHelper.convert(gstReturnEm, gstReturn);
			gstReturn = crudService.update(gstReturnEm);
			gstReturnDTO = (GstReturnDTO) EntityHelper.convert(gstReturn, GstReturnDTO.class);

			return gstReturnDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public GstReturnDTO deleteGstReturn(int id) throws AppException {

		if (id > 0) {

			GstReturnDTO gstReturnDTO = null;
			GstReturn gstReturn = em.getReference(GstReturn.class, id);
			gstReturn.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			gstReturn.setUpdationIpAddress(userBean.getIpAddress());
			gstReturn.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			gstReturn.setActive((byte) 0);
			gstReturn.setStatus(Constant.Status.ACTIVE.toString());

			gstReturn = crudService.update(gstReturn);
			gstReturnDTO = (GstReturnDTO) EntityHelper.convert(gstReturn, GstReturnDTO.class);

			return gstReturnDTO;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public BusinessTypeDTO createBusinessType(BusinessTypeDTO businessTypeDTO) throws AppException {

		if (!(Objects.isNull(businessTypeDTO) && StringUtils.isEmpty(businessTypeDTO.getName())
				&& StringUtils.isEmpty(businessTypeDTO.getCode()))) {

			BusinessType businessType = (BusinessType) EntityHelper.convert(businessTypeDTO, BusinessType.class);
			businessType.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			businessType.setCreationIpAddress(userBean.getIpAddress());
			businessType.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			businessType.setActive((byte) 1);

			businessType = crudService.create(businessType);
			businessTypeDTO = (BusinessTypeDTO) EntityHelper.convert(businessType, BusinessTypeDTO.class);

			return businessTypeDTO;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public BusinessTypeDTO updateBusinessType(BusinessTypeDTO businessTypeDTO) throws AppException {

		if (!(Objects.isNull(businessTypeDTO) && Objects.isNull(businessTypeDTO.getId()))) {

			BusinessType businessTypeEm = em.getReference(BusinessType.class, businessTypeDTO.getId());

			BusinessType businessType = (BusinessType) EntityHelper.convert(businessTypeDTO, BusinessType.class);
			businessType.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			businessType.setUpdationIpAddress(userBean.getIpAddress());
			businessType.setActive((byte) 1);

			businessTypeEm = EntityHelper.convert(businessTypeEm, businessType);
			businessTypeDTO = (BusinessTypeDTO) EntityHelper.convert(businessType, BusinessTypeDTO.class);

			return businessTypeDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public BusinessTypeDTO deleteBusinessType(int id) throws AppException {

		if (id > 0) {

			BusinessTypeDTO businessTypeDTO = null;
			BusinessType businessType = em.getReference(BusinessType.class, id);
			businessType.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			businessType.setUpdationIpAddress(userBean.getIpAddress());
			businessType.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			businessType.setActive((byte) 0);

			businessType = crudService.update(businessType);
			businessTypeDTO = (BusinessTypeDTO) EntityHelper.convert(businessType, BusinessTypeDTO.class);

			return businessTypeDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public ReturnTransactionDTO createReturnTransaction(ReturnTransactionDTO returnTransactionDTO) throws AppException {

		if (!(Objects.isNull(returnTransactionDTO) && StringUtils.isEmpty(returnTransactionDTO.getName())
				&& StringUtils.isEmpty(returnTransactionDTO.getCode()))) {

			ReturnTransaction returnTransaction = (ReturnTransaction) EntityHelper.convert(returnTransactionDTO,
					ReturnTransaction.class);
			returnTransaction
					.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			returnTransaction.setCreationIpAddress(userBean.getIpAddress());
			returnTransaction.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			returnTransaction.setActive((byte) 1);

			returnTransaction = crudService.create(returnTransaction);
			returnTransactionDTO = (ReturnTransactionDTO) EntityHelper.convert(returnTransaction,
					ReturnTransactionDTO.class);

			return returnTransactionDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public ReturnTransactionDTO updateReturnTransaction(ReturnTransactionDTO returnTransactionDTO) throws AppException {

		if (!(Objects.isNull(returnTransactionDTO) && Objects.isNull(returnTransactionDTO.getId()))) {

			ReturnTransaction returnTransactionEm = em.getReference(ReturnTransaction.class,
					returnTransactionDTO.getId());

			ReturnTransaction returnTransaction = (ReturnTransaction) EntityHelper.convert(returnTransactionDTO,
					ReturnTransaction.class);
			returnTransaction
					.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			returnTransaction.setUpdationIpAddress(userBean.getIpAddress());
			returnTransaction.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			returnTransaction.setActive((byte) 1);

			returnTransactionEm = EntityHelper.convert(returnTransactionEm, returnTransaction);
			returnTransaction = crudService.update(returnTransactionEm);

			returnTransactionDTO = (ReturnTransactionDTO) EntityHelper.convert(returnTransaction,
					ReturnTransaction.class);

			return returnTransactionDTO;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public ReturnTransactionDTO deleteReturnTransaction(int id) throws AppException {

		if (id > 0) {

			ReturnTransactionDTO returnTransactionDTO = null;
			ReturnTransaction returnTransaction = em.getReference(ReturnTransaction.class, id);
			returnTransaction
					.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			returnTransaction.setUpdationIpAddress(userBean.getIpAddress());
			returnTransaction.setActive((byte) 0);
			returnTransaction.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			returnTransaction = crudService.update(returnTransaction);
			returnTransactionDTO = (ReturnTransactionDTO) EntityHelper.convert(returnTransaction,
					ReturnTransactionDTO.class);

			return returnTransactionDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public HelpContentDTO getHelpContent(String id) throws AppException {

		if (!StringUtils.isEmpty(id)) {

			HelpContentDTO contentDTO = new HelpContentDTO();
			HelpContent content = finderService.findHelpContent(id);

			if (!Objects.isNull(content))
				contentDTO = (HelpContentDTO) EntityHelper.convert(content, HelpContentDTO.class);

			return contentDTO;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean saveHelpContent(HelpContentDTO helpContentDTO) throws AppException {

		if (!Objects.isNull(helpContentDTO) && !StringUtils.isEmpty(helpContentDTO.getLink())) {

			HelpContent content = new HelpContent();
			HelpContent contentEm = finderService.findHelpContent(helpContentDTO.getLink());

			content = (HelpContent) EntityHelper.convert(helpContentDTO, HelpContent.class);

			if (!Objects.isNull(contentEm)) {
				contentEm = EntityHelper.convert(contentEm, content);
				contentEm.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
				crudService.update(contentEm);
			} else {
				content.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
				crudService.create(content);
			}

			return Boolean.TRUE;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public List<HsnSacDTO> getHsnSacContent(String type, String code) throws AppException {

		if (!StringUtils.isEmpty(type) && !StringUtils.isEmpty(code)) {

			List<HsnSacDTO> hsnSacDTOs = new ArrayList<>();
			List<HsnSac> hsnSacs = finderService.findHsnSacContent(type, code);

			if (Objects.isNull(hsnSacs))
				return null;

			for (HsnSac hsnSac : hsnSacs) {
				hsnSacDTOs.add((HsnSacDTO) EntityHelper.convert(hsnSac, HsnSacDTO.class));
			}

			return hsnSacDTOs;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public String searchHsnCode(String searchKey) throws AppException {

		if (!StringUtils.isEmpty(searchKey)) {

			String searchString = "%" + searchKey + "%";
			List<HsnSubChapter> hsnProducts = finderService.searchHsnProductByKey(searchString);
			Set<HsnChapter> hsnChapters = new HashSet<>();
			for (HsnSubChapter hp : hsnProducts) {
				hsnChapters.add(hp.getHsnChapter());
			}

			try {
				return JsonMapper.objectMapper.writeValueAsString(hsnChapters);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				return "[]";
			}

		} else {
			return "[]";
		}
	}

}
