package com.ginni.easemygst.portal.business.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataStatusDTO {

	private BigDecimal totalInvoices;
	
	private BigDecimal pendingInvoices;
	
	private BigDecimal completedInvoices;
	
	private BigDecimal inprocessInvoices;
	
	private String transaction;
	
	private String url;
	
	private String dateUpdate;
	
	private String timeUpdate;
	
	private String status;
	
	public DataStatusDTO(BigDecimal completedInvoices,BigDecimal pendingInvoices,BigDecimal totalInvoices,BigDecimal inprocessInvoices,String transaction,Timestamp updationTime,String status){
		Date time=new Date(updationTime.getTime());
		
		SimpleDateFormat timeFmt=new SimpleDateFormat("hh:mm a");
		this.completedInvoices=completedInvoices;
		this.pendingInvoices=pendingInvoices;
		this.totalInvoices=totalInvoices;
		this.transaction=transaction;
		this.inprocessInvoices=inprocessInvoices;
		this.url="www.easemygst.com";
		this.status=status;
		this.dateUpdate=new SimpleDateFormat("dd MMM yyyy").format(time);
		this.timeUpdate=timeFmt.format(time);
	}


	public BigDecimal getTotalInvoices() {
		return totalInvoices;
	}

	public void setTotalInvoices(BigDecimal totalInvoices) {
		this.totalInvoices = totalInvoices;
	}

	public BigDecimal getPendingInvoices() {
		return pendingInvoices;
	}

	public void setPendingInvoices(BigDecimal pendingInvoices) {
		this.pendingInvoices = pendingInvoices;
	}

	public BigDecimal getCompletedInvoices() {
		return completedInvoices;
	}

	public void setCompletedInvoices(BigDecimal completedInvoices) {
		this.completedInvoices = completedInvoices;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public String getDateUpdate() {
		return dateUpdate;
	}


	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}


	public String getTimeUpdate() {
		return timeUpdate;
	}


	public void setTimeUpdate(String timeUpdate) {
		this.timeUpdate = timeUpdate;
	}


	public BigDecimal getInprocessInvoices() {
		return inprocessInvoices;
	}


	public void setInprocessInvoices(BigDecimal inprocessInvoices) {
		this.inprocessInvoices = inprocessInvoices;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
