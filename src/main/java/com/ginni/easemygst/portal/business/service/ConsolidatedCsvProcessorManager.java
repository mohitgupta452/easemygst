package com.ginni.easemygst.portal.business.service;

import java.util.List;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;

@Local
public interface ConsolidatedCsvProcessorManager {

	public List getDataByReturnTypeTransaction(GstinTransactionDTO gstinTransactionDTO,String dataUploadId);


	int saveData(GstinTransactionDTO gstinTransactionDto, List<UploadedCsv> lineItems);


	boolean importExcelToDatabase(String fileName,int id);


	int saveUploadedInfo(GstinTransactionDTO gstinTransactionDto, String file);
	
}
