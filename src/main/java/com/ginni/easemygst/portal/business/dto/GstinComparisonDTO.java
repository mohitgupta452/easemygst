package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

public @Data class GstinComparisonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String gstin;
	
	private String state;
	
	private List<ComparisonDTO> comparisonDTOlist;
	
	private boolean isAuthReq=false;
	
	
}
