package com.ginni.easemygst.portal.business.dto;

import java.util.Date;
import java.util.List;

import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

import lombok.Data;

public @Data class CommonTransactionDto {
	private String id;
	
	private String invoiceNumber;

	private String flags;
	
	private double taxableValue;
	
	private double taxAmount;
	
	
//	private B2BTransactionEntity b2bTransaction;
	
	private List<Item> items;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;
	
	private boolean toBeSync;

	private boolean isTransit;

	private boolean isValid;

	private boolean reverseCharge;

	private String source;

	private String sourceId;

	private String synchId;

	private String transitId;

	private String type;
	
	private String financialYear="2017-2018";
	
	private String ctin;
	
	private String ctinName;
	
	private String invoiceType;
	
	private String etin;
	
	private Date invoiceDate;
	
	private String pos;
}
