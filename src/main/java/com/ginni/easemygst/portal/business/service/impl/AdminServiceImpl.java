package com.ginni.easemygst.portal.business.service.impl;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.service.AdminService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.service.CrudService;
@Stateless
@Transactional
public class AdminServiceImpl  implements AdminService{

	@Inject
	private CrudService crudService;
	
	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Inject
	private Logger log;
	
	@Inject
	private FinderService finderService;
	
	@Override
	public void unSync(String type,String returntype,String gstin,String monthYear) throws AppException{
	    this.validateGstin(gstin);
	    this.validateMonthYear(monthYear);
		if(!Objects.isNull(finderService.findTaxpayerGstin(gstin))){
		Map<String, Object> queryParameters = new HashMap<>();
		Map<String, String> queries = new HashMap<>();
		String queryName = "";
 
		queryParameters.put("returnType",returntype);
		queryParameters.put("gstin",gstin);
		queryParameters.put("monthYear",monthYear);
		
		if (TransactionType.B2B.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE b2b_details set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE b2bTransactionId in (SELECT id from b2b_transaction WHERE taxpayerGstinId=(SELECT id from taxpayer_gstin where gstin=:gstin) AND monthYear=:monthYear AND returnType=:returnType)";
			queries.put(TransactionType.B2B.toString(), queryName);
		}
		
		if (TransactionType.CDN.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE cdn_details set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE cdnTransactionId in (SELECT id from cdn_transaction WHERE gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) AND monthYear=:monthYear AND returnType=:returnType)";
			queries.put(TransactionType.CDN.toString(), queryName);
		}
		
		if (TransactionType.CDNUR.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE cdnur_details set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE cdnUrTransactionId IN(SELECT id from cdnur_transaction where gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear and returnType=:returnType)";
			queries.put(TransactionType.CDNUR.toString(), queryName);
		}
		
		if (TransactionType.B2CS.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE b2cs_transaction SET toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) AND monthYear=:monthYear AND returnType=:returnType";
			queries.put(TransactionType.B2CS.toString(), queryName);
		}
		
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "update b2bur_transaction set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' where gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear AND returnType=:returnType";
			queries.put(TransactionType.B2BUR.toString(), queryName);
		}
		if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "update hsn set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' where taxpayerGstinId=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear and returnType=:returnType";
			queries.put(TransactionType.HSNSUM.toString(), queryName);
		}
		
		if (TransactionType.AT.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "update at_transaction set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' where gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) and returnType=:returnType AND monthYear=:monthYear";
			queries.put(TransactionType.AT.toString(), queryName);
		}
		
		if (TransactionType.NIL.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE nil_transaction set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear and returnType=:returnType";
			queries.put(TransactionType.NIL.toString(), queryName);
		}
		if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE doc_issue set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear and returnType=:returnType";
			queries.put(TransactionType.DOC_ISSUE.toString(), queryName);
		}
		if (TransactionType.B2CL.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "update b2cl_details SET toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' where b2clTransactionId IN(SELECT id from b2cl_transaction where taxpayerGstinId=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear and returnType=:returnType)";
			queries.put(TransactionType.B2CL.toString(), queryName);
		}
		if (TransactionType.TXPD.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
			queryName = "UPDATE txpd_transaction set toBeSync=true,isTransit=false,isError=false,errMsg='',flags='' WHERE gstin=(SELECT id from taxpayer_gstin where gstin=:gstin) and monthYear=:monthYear and returnType=:returnType";
			queries.put(TransactionType.TXPD.toString(), queryName);
		}
		
		queries.forEach((k,v)->{
			javax.persistence.Query query=em.createNativeQuery(v);
			queryParameters.forEach((k1,v1)->{
				query.setParameter(k1,v1);
			});
		int count=query.executeUpdate();
		log.debug("unsync count in" + k +"{}" +count);
		});
		
		
		}
		else{
			throw new  AppException(ExceptionCode._GSTIN_NOT_REGISTERED,"GSTIN not registered with us");	
		
		}
	}
	
	
	
     public void validateGstin(String gstin) throws AppException{
    		if (!StringUtils.isEmpty(gstin)) {
    			boolean isGstin = false;
    			gstin = gstin.trim();
    			if (Character.isDigit(gstin.charAt(0)))
    				isGstin = true;
    			if (isGstin) {
    				try {
    					if (!StringUtils.isEmpty(gstin) && gstin.length() == 15) {
    						String ctin = gstin;
    						String st = ctin.substring(0, 2);
    						int scode = Integer.parseInt(st);
    						if (!(scode > 0 && scode <= 37))
    							throw new NumberFormatException();
    					} else {
    						throw new  AppException(ExceptionCode._INVALID_GSTIN," Invalid GSTIN value it should be exactly 15 character and should contain valid state code");

    					}
    				} catch (NumberFormatException e) {
    					throw new  AppException(ExceptionCode._INVALID_GSTIN," Invalid GSTIN value it should be exactly 15 character and should contain valid state code");
    				}
    			} 

    		} 
    		else{
				throw new  AppException(ExceptionCode._INVALID_GSTIN," Invalid GSTIN value it should be exactly 15 character and should contain valid state code");

    		}
		
	}
     
     public void validateMonthYear(String monthYear) throws AppException {

 		try {
 				YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
 		} catch (Exception e) {
 			
 			throw new  AppException(ExceptionCode._INVALID_MONTHYEAR," Invalid month year value  use format MMyyyy");

 		}

 	
 	}
     
	
}
