package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.TaxPaidA;
import com.ginni.easemygst.portal.data.part.TaxPaidB;

public @lombok.Data class TaxPaidDto {

	@JsonProperty("tx_pb_a")
	private TaxPaidA taxPaidA;
	
	@JsonProperty("tx_pb_b")
	private TaxPaidB taxPaidB;
	
	/*@JsonProperty("pdcash")
	private List<TaxPaidBByCash>paidByCash;//this attribute is placed under tx_pd_b in excel but in json it is under taxpd
	*/
	
}
