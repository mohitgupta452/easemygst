package com.ginni.easemygst.portal.business.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

public @Data class TransactionalDetailDTO {


	@JsonFormat(pattern = "#0.00")
	private Double taxableValue=0.0 ;
	
	@JsonFormat(pattern = "#0.00")
	private Double taxAmount =0.0;
	
	@JsonFormat(pattern = "#0.00")
	private Double igstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double cgstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double sgstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double cessAmount ;
	

	
}
