package com.ginni.easemygst.portal.business.entity;


import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.business.dto.GstCalendarDto;

import lombok.Data;
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public @Data class FilePreferenceDTO {

	private String gstin;
	
	private String preference;
	
    private Map<String,List<GstCalendarDto>>dueDateCalendar;
	
	private Map<String, Map<String, List<GstCalendarDto>>> dueDateCalendarQuarter;
	
	
}
