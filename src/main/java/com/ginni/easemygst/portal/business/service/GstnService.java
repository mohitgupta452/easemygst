package com.ginni.easemygst.portal.business.service;

import java.io.IOException;
import java.io.InputStream;

import javax.ejb.Local;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.constant.GstMetadata.ReturnsModule;
import com.ginni.easemygst.portal.gst.request.FileGstr1AReq;
import com.ginni.easemygst.portal.gst.request.FileGstr1Req;
import com.ginni.easemygst.portal.gst.request.FileGstr2Req;
import com.ginni.easemygst.portal.gst.request.FileGstr3Req;
import com.ginni.easemygst.portal.gst.request.GSTR3;
import com.ginni.easemygst.portal.gst.request.GenGstr3Req;
import com.ginni.easemygst.portal.gst.request.GetGstr1AReq;
import com.ginni.easemygst.portal.gst.request.GetGstr1Req;
import com.ginni.easemygst.portal.gst.request.GetReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.GetGstr2Req;
import com.ginni.easemygst.portal.gst.request.GetGstr2ReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.GetGstr3DetailReq;
import com.ginni.easemygst.portal.gst.request.GetGstr3ReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.GetITCLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.request.GetLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.request.Gstr1ASummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr1SummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr2SummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest;
import com.ginni.easemygst.portal.gst.request.LiabilityLedger;
import com.ginni.easemygst.portal.gst.request.SaveGstr1AReq;
import com.ginni.easemygst.portal.gst.request.SaveGstr1Req;
import com.ginni.easemygst.portal.gst.request.SaveGstr2Req;
import com.ginni.easemygst.portal.gst.request.SaveGstr3Req;
import com.ginni.easemygst.portal.gst.request.SaveGstr3bRequest;
import com.ginni.easemygst.portal.gst.request.SetOffLiabilityDataReq;
import com.ginni.easemygst.portal.gst.request.SetOffLiabilityReq;
import com.ginni.easemygst.portal.gst.request.SubmitGstr1AReq;
import com.ginni.easemygst.portal.gst.request.SubmitGstr1Req;
import com.ginni.easemygst.portal.gst.request.SubmitGstr2Req;
import com.ginni.easemygst.portal.gst.request.SubmitRefundData;
import com.ginni.easemygst.portal.gst.request.SubmitRefundReq;
import com.ginni.easemygst.portal.gst.request.ViewTrackReturnsReq;
import com.ginni.easemygst.portal.gst.response.CashITCBalance;
import com.ginni.easemygst.portal.gst.response.FileGstr1AResp;
import com.ginni.easemygst.portal.gst.response.FileGstr1Resp;
import com.ginni.easemygst.portal.gst.response.FileGstr2Resp;
import com.ginni.easemygst.portal.gst.response.FileGstr3Resp;
import com.ginni.easemygst.portal.gst.response.GenGstr3Resp;
import com.ginni.easemygst.portal.gst.response.GetCashLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusResp;
import com.ginni.easemygst.portal.gst.response.GetCreditLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetGstr1AResp;
import com.ginni.easemygst.portal.gst.response.GetGstr1Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr2ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr3ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetITCLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetReturnResp;
import com.ginni.easemygst.portal.gst.response.Gstr1ASummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr1SummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr2SummaryResp;
import com.ginni.easemygst.portal.gst.response.LiabilityLedgerDetail;
import com.ginni.easemygst.portal.gst.response.SaveGstr1AResp;
import com.ginni.easemygst.portal.gst.response.SaveGstr1Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr2Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr3Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr3bResponse;
import com.ginni.easemygst.portal.gst.response.SubmitGstr1AResp;
import com.ginni.easemygst.portal.gst.response.SubmitGstr1Resp;
import com.ginni.easemygst.portal.gst.response.SubmitGstr2Resp;
import com.ginni.easemygst.portal.gst.response.ViewTrackReturnsResp;
import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface GstnService {
  
	ObjectMapper _MAPPER= new ObjectMapper();
	
	public GetGstr1Resp getGstr1Data(GetGstr1Req getGstr1Req) throws AppException;
	
	public SaveGstr1Resp saveGstr1Data(SaveGstr1Req saveGstr1Req) throws AppException;
	
	public GetGstr1ReturnStatusResp getGstr1ReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException; 
	
	public SubmitGstr1Resp submitGstr1Data(SubmitGstr1Req submitGstr1Req) throws AppException;
	
	public String getGstr1Summary(Gstr1SummaryReq gstr1SummaryReq) throws AppException;
	
	public FileGstr1Resp fileGstr1Return(FileGstr1Req gstr1SummaryReq) throws AppException;
	
	
	public GetGstr1AResp getGstr1AData(GetGstr1AReq getGstr1AReq) throws AppException;
	
	public SaveGstr1AResp saveGstr1AData(SaveGstr1AReq saveGstr1AReq) throws AppException;
	
	public SubmitGstr1AResp submitGstr1AData(SubmitGstr1AReq submitGstr1AReq) throws AppException;
	
	public Gstr1ASummaryResp getGstr1ASummary(Gstr1ASummaryReq gstr1ASummaryReq) throws AppException;
	
	public FileGstr1AResp fileGstr1AReturn(FileGstr1AReq fileGstr1AReq) throws AppException;
	
	
	public String getGstr2Data(GetGstr2Req getGstr2Req) throws AppException;
	
	public SaveGstr2Resp saveGstr2Data(SaveGstr2Req saveGstr2Req) throws AppException;
	
	public GetGstr2ReturnStatusReq getGstr2ReturnStatus(GetGstr2ReturnStatusResp getGstr2ReturnStatusResp) throws AppException;
	
	public SubmitGstr2Resp submitGstr2Data(SubmitGstr2Req submitGstr2Req) throws AppException;
	
	public Gstr2SummaryResp getGstr2Summary(Gstr2SummaryReq gstr2SummaryReq) throws AppException;
	
	public FileGstr2Resp fileGstr2Return(FileGstr2Req fileGstr2Req) throws AppException;
	
	
	public GenGstr3Resp genGstr3Data(GenGstr3Req genGstr3Req) throws AppException;
	
	public SaveGstr3Resp saveGstr3Data(SaveGstr3Req saveGstr3Req) throws AppException;
	
	public GetGstr3ReturnStatusReq getGstr3ReturnStatus(GetGstr3ReturnStatusResp getGstr3ReturnStatusResp) throws AppException;
	
	public GetGstr3Resp getGstr3Data(GSTR3 getGstr3Req) throws AppException;
	
	public FileGstr3Resp fileGstr3Return(FileGstr3Req fileGstr3Req) throws AppException;
	
	
	
	public GetITCLedgerDetailsResp getITCLedgerDetailsReq (GetITCLedgerDetailsReq getITCLedgerDetailsReq) throws AppException;
	
	public LiabilityLedgerDetail getLiabilityLedgerDetailsReq (LiabilityLedger getLiabilityLedgerDetailsReq) throws AppException;
	
	public String getGstr2ReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException;
	
	public String getGst1ReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException, IOException;
	
	String  getGstr1AData(GetGstr1Req getGstr1Req) throws AppException;	
	
	public String  getGstr2DataByToken(GetGstr2Req getGstr2Req) throws AppException;
	
	public String generateGstr3(GenGstr3Req genGstr3Req) throws AppException;
	
	public String getGstr3Details(GetGstr3DetailReq getGstr3DetailReq) throws AppException;

	public String setoffLiabilities(SetOffLiabilityDataReq setOffReq)throws AppException ;

	public String submitRefund(SubmitRefundData submitrefund) throws AppException;
	
	public InputStream  getGstr2AUrlData(GetGstr2Req getGstr2Req) throws AppException;
	
	public String getSearchTaxpayerResult(GetGstr1Req getGstr1Req) throws AppException;

	public SaveGstr3bResponse saveGstr3bData(Gstr3BRequest saveGstr3bReq) throws AppException;

	public String getGstr3bReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException;

	public String getGstr3bData(GetGstr1Req getGstr1Req) throws AppException;

	public String getGstr1DataNew(GetGstr1Req getGstr1Req) throws AppException;

	public String getGstr1DataActionRequired(GetGstr2Req getGstr2Req) throws AppException;
	
	public ViewTrackReturnsResp viewTrackReturns(ViewTrackReturnsReq trackReturnsReq) throws AppException;

	String getGstrSummary(Gstr1SummaryReq gstr1SummaryReq, ReturnsModule returnsModule) throws AppException;

	String getGstr1datafromgstn(GetGstr1Req getGstr1Req) throws AppException;

	CashITCBalance getCashLedgerDetailsReq(GetLedgerDetailsReq getCashLedgerDetailsReq) throws AppException;

	GetCreditLedgerDetailsResp getCreditLedgerDetailsReq(GetLedgerDetailsReq getCashLedgerDetailsReq)
			throws AppException;

}
