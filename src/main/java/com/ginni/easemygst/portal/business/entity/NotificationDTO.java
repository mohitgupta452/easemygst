package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class NotificationDTO implements Serializable {

	private int id;

	private String attachments;

	private String event;

	private Timestamp generationTime;

	private String image;

	private byte isRead;

	private String message;

	private Timestamp modifiedTime;

	private Timestamp processedTime;

	private String remarks;

	private int status;

	private String subject;

	private String type;
	
	public interface NotificationDTOMixIn extends CommonMixin {
		
	}

}
