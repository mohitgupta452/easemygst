package com.ginni.easemygst.portal.business.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.entity.CommonMixin;

import lombok.Data;

public @Data class LiabilityLedgerDto {
	
	
	@JsonProperty("sno")
	Integer sno;
	
	@JsonFormat(pattern = "DD-MM-YYYY")
	@JsonProperty("dt")
	private Date date;
	
	@JsonProperty("tran_code")
	private String transactionCode;
	
	@JsonProperty("desc")
	private String desc;
	
	@JsonProperty("type")
	private String type;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("igst")
	private Double igst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("sgst")
	private Double sgst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cgst")
	private Double cgst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cess")
	private Double cess;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("balance")
	private Double balance;
	
	@JsonProperty("refNo")
	private String refNo;
	
public interface ItcLedgerDtoMixIn extends CommonMixin {
		
	}
	
	

}
