package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class FlagsCounterDTO {

	private boolean isSynced;
	
	private boolean toBeSynced;
	
	private boolean isError;
	
	private boolean isTransit;
	
	private long count;
	
	 public FlagsCounterDTO (boolean isSynced,boolean toBeSynced,boolean isError,boolean isTransit,long count){
		
		this.isSynced	= isSynced;
		
		this.toBeSynced= toBeSynced;
		
		this.isError= isError;
		
		this.isTransit=isTransit;
		
		this.count= count;
	}
}
