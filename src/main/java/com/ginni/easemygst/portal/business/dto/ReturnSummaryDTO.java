package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class ReturnSummaryDTO {

	int id;
	
	String description;
	
	String name;
	
	String returnName;
	
	String status;
	
	int diff;
	
	String dueDate;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReturnSummaryDTO other = (ReturnSummaryDTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	
	
}
