package com.ginni.easemygst.portal.business.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public @lombok.Data class TodDto {
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("gr_to")
	private Double grossTurnOver = new Double(0.00);
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("exp_to")
	private Double exportTurnOver = new Double(0.00);
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("nil_to")
	private Double nilAndExemptedTurnOver = new Double(0.00);
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("non_gst_to")
	private Double nonGstTurnOver = new Double(0.00);
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("net_taxable_to")
	private Double netTaxableTurnOver = new Double(0.00);
	
}
