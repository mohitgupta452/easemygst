package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;

import com.ginni.easemygst.portal.gst.request.GetLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.response.CashITCBalance;
import com.ginni.easemygst.portal.gst.response.GetCashLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetCreditLedgerDetailsResp;
import com.ginni.easemygst.portal.helper.AppException;
@Local
public interface LedgerService {

	
	
	public GetCreditLedgerDetailsResp getCreditLedgerDetails(GetLedgerDetailsReq cashLedgerDetailsReq) throws AppException;

	CashITCBalance getCashLedgerDetails(GetLedgerDetailsReq cashLedgerDetailsReq) throws AppException;
	
	
}
