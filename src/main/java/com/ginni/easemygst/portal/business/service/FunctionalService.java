package com.ginni.easemygst.portal.business.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.dto.GstinComparison2aVS3bDTO;
import com.ginni.easemygst.portal.business.dto.GstinComparisonDTO;
import com.ginni.easemygst.portal.business.dto.GstrFlowDTO;
import com.ginni.easemygst.portal.business.dto.LedgerDto;
import com.ginni.easemygst.portal.business.dto.TrackReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.ReturnStatusDTO;
import com.ginni.easemygst.portal.data.part.Gstr3Summary;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.TrackReturnsInfo;

@Local
public interface FunctionalService {
	

	String syncGstr1TransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	String syncGstr1ActionRequiredTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	String syncGstr1SummaryData(GstinTransactionDTO gstinTransactionDTO) throws AppException, JsonProcessingException;
	
	public void saveGstr1DataToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public GetCheckStatusResp checkReturnStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException, Exception;
	
	
	boolean syncGstr1ATransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	boolean syncGstr1ASummaryData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public void saveGstr1ADataToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	
	public GetGstr2Resp syncGstr2ATransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException, JsonParseException, JsonMappingException, IOException ;
	
	String syncGstr2TransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	String syncGstr2ActionRequiredTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	boolean syncGstr2SummaryData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public void saveGstr2DataToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	
	
	public GstrFlowDTO submitGstr(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;
	
	public GstrFlowDTO submitGstrToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;

	
	public GstrFlowDTO generateGstr(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;
	
	public GstrFlowDTO fileGstr(GstinTransactionDTO gstinTransactionDTO,String summary) throws Exception;
	
	public String utilizeCredit(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;
	
	public String utilizeCash(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;
	
	public String getCashLedgerDetails(LedgerDto cashLedgerDto)throws AppException;
	
	public String getItcLedgerDetails(LedgerDto cashLedgerDto) throws AppException ;
	
	public String getLiabilityLedgerDetails(LedgerDto cashLedgerDto) throws AppException ;

	List<ReturnStatusDTO> checkReturnStatusByTransaction(GstinTransactionDTO gstinTransactionDTO,String transactionType) throws AppException;
	
	public GetCheckStatusGstr2Resp checkGstr2ReturnStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;
	
	List updateErrorReports(GstinTransactionDTO gstinTransactionDTO,GetCheckStatusResp checkstatusResp,String transitId) throws AppException, Exception;
	
	List updateErrorReportsGstr2(GstinTransactionDTO gstinTransactionDTO,GetCheckStatusGstr2Resp statusResp,String transitId) throws AppException, Exception;

	public JsonNode compareReturnSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException;
	
	public GetGstr2Resp syncGst1ATransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException, JsonParseException, JsonMappingException, IOException;
	
	public List<ReturnStatus> getIPTransGstin(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public GstrFlowDTO submitGstr2ToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception;
	
	public GetCheckStatusResp processReturnStatusGstr1(ReturnStatus returnStatus) throws Exception;

	public void checkACtionStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException;

	public GetCheckStatusGstr2Resp processReturnStatusGstr2(ReturnStatus returnStats) throws Exception;

	GetGstr3Resp getGstr3Details(GstinTransactionDTO gstnTransactionDTO) throws AppException;

	GetGstr3Resp setOffGstr3Liability(GstinTransactionDTO gstnTransactionDTO) throws AppException;

	GetGstr3Resp submitRefund(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	int saveDataToTokenResponse(GstinTransactionDTO gstinTransactionDTO,String type) throws AppException;

	public String syncGstr3BTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	public GetGstr2Resp syncGstr1ActionRequiredTransactionDataNew(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonParseException, JsonMappingException, IOException;

	public void processUrls(GstinTransactionDTO gstinTransactionDto);

	public void getReturnsTrackFromGSTN(List<TrackReturnsInfo> trackReturnsInfos) throws AppException;

	String syncGstr6TransactionData(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonParseException, JsonMappingException, IOException;

}
