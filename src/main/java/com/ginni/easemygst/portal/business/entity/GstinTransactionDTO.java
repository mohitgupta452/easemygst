package com.ginni.easemygst.portal.business.entity;

import java.sql.Timestamp;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.data.TaxpayerAction;

import lombok.Data;

public @Data class GstinTransactionDTO {
	
	private int id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String monthYear;

	private String remark;

	private String returnType;

	private String status;
	
	private byte isChanged;

	private String transactionObject;

	private String transactionType;
	
	private TaxpayerAction taxpayerAction; 

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	private TaxpayerGstinDTO taxpayerGstin;
	
	private UserBean userBean;
	
	private int countTotalReturnStatus;
	
	private String transitId;
	
	private int countPStatus;
	
	private int countPEStatus;
	
	private int countIPStatus;
	
	private int countERStatus;
	
	private int totalSync;
	
	private int totalNotSync;
	
	private String ctin;
	
	public interface GstinTransactionDTOMixIn extends CommonMixin {
		
	}
	
}
