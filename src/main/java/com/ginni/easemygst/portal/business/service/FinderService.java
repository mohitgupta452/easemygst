package com.ginni.easemygst.portal.business.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.entity.BrtMappingDTO;
import com.ginni.easemygst.portal.business.entity.BusinessTypeDTO;
import com.ginni.easemygst.portal.business.entity.CityDTO;
import com.ginni.easemygst.portal.business.entity.ErpTransactionDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinActionDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.GstnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.NonGovStateDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.Calendar;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.Debugmode;
import com.ginni.easemygst.portal.persistence.entity.ErpConfig;
import com.ginni.easemygst.portal.persistence.entity.ErpLog;
import com.ginni.easemygst.portal.persistence.entity.ErpTransaction;
import com.ginni.easemygst.portal.persistence.entity.FilePrefernce;
import com.ginni.easemygst.portal.persistence.entity.GstCalendar;
import com.ginni.easemygst.portal.persistence.entity.GstReturn;
import com.ginni.easemygst.portal.persistence.entity.GstinAction;
import com.ginni.easemygst.portal.persistence.entity.GstinTransaction;
import com.ginni.easemygst.portal.persistence.entity.GstnTransaction;
import com.ginni.easemygst.portal.persistence.entity.HelpContent;
import com.ginni.easemygst.portal.persistence.entity.HsnChapter;
import com.ginni.easemygst.portal.persistence.entity.HsnProduct;
import com.ginni.easemygst.portal.persistence.entity.HsnSac;
import com.ginni.easemygst.portal.persistence.entity.HsnSubChapter;
import com.ginni.easemygst.portal.persistence.entity.InvoiceVendorDetails;
import com.ginni.easemygst.portal.persistence.entity.ItemCode;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.Partner;
import com.ginni.easemygst.portal.persistence.entity.Payment;
import com.ginni.easemygst.portal.persistence.entity.Plan;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.Taxpayer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TpMapping;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.persistence.entity.UserContact;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.persistence.entity.UserLastlog;
import com.ginni.easemygst.portal.persistence.entity.UserTaxpayer;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;

@Local
public interface FinderService {
	
	Debugmode findDebugmodeByUsernameAndPassword(String username,String password);
	
	UserGstin findUserGstinById(Integer id);
	
	TaxpayerGstin findTaxpayerGstinAll(String gstin);
	
	TaxpayerGstin findTaxpayerGstin(String gstin);
	
	GstnTransaction getGstnTransaction(GstnTransactionDTO gstnTransactionDTO) throws AppException;
	
	GSTR1 getMultipleGstinTransaction(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	List<ReturnStatus> getReturnStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	GstinAction findGstinAction(GstinActionDTO gstinActionDTO, String gstin);
	
	List<GstReturnDTO> getGstReturn() throws AppException;
	
	List<BusinessTypeDTO> getBusinessType() throws AppException;
	
	List<ReturnTransactionDTO> getReturnTransaction() throws AppException;
	
	List<StateDTO> getAllState() throws AppException;
	
	List<BusinessTypeDTO> getAllBusinessType() throws AppException;
	
	List<NonGovStateDTO> getAllNonGovState() throws AppException;
	
	List<CityDTO> getAllCities(int stateId) throws AppException;
	
	HelpContent findHelpContent(String link);
	
	List<HsnSac> findHsnSacContent(String type, String code);
	
	List<HsnSubChapter> searchHsnProductByKey(String searchKey);
	
	Partner findPartner(String email, String contactNo);
	
	Partner findPartnerByCode(String code);
	
	GstinTransaction getGstinTransaction(GstinTransactionDTO gstinTransactionDTO, Boolean reconciled) throws AppException;
	
	ErpTransaction getErpTransaction(ErpTransactionDTO erpTransactionDTO) throws AppException;
	
	String getCtinTransaction(GstinTransactionDTO gstinTransactionDTO, String returnType) throws AppException;
	
	List<Taxpayer> findTaxpayerId(int userId, String type);
	
	ItemCode findItemCodeByTaxpayerId(int userId, String desc);
	
	List<Vendor> findVendorEmailFlag();
	
	List<Vendor> findVendorFollowEmailFlag();
	
	UserContact getUserContact(int userId);
	
	List<DataUploadInfo> getDataUploadInfoByStatus(String status);
	
	ErpConfig findErpConfigUsingUniqueKey(String key, String sno);
	
	ErpConfig findErpConfigUsingUniqueKey(String key);
	
	List<UserLastlog> getUserLastLogs(int userId);
	
	User getExistingUser(int userId, int active);
	
	UserDTO getExistingUser(String username, int active);
	
	UserDTO getExistingUser(String username);
	
	UserDTO getUserTaxpayer(int userId) throws AppException;
	
	Taxpayer findTaxpayer(String pan);
	
	Taxpayer findTaxpayerByUser(String pan, int userId, String type);
	
	List<UserGstin> getUserGstin(int userId) throws AppException;
	
	List<UserGstin> getUserGstinByUserOrg(int userId, int organisationId) throws AppException;
	
	List<Organisation> getDistinctUserGstinByUser(int userId) throws AppException;
	
	UserDTO findUserByOrganisation(int orgId) throws AppException;
	
	TaxpayerGstin findTaxpayerGstinByGstin(String gstin);
	
	GstReturn findGstReturn(int id);
	
	List<UserGstin> findUserGstinByUserAndPan(String pan, int userId);
	
	List<UserTaxpayer> findUserTaxpayerByTaxpayer(int taxpayerId);
	
	List<UserGstin> findUserGstinByTaxpayer(int taxpayerId);
	
	List<DataUploadInfo> findDataUploadInfoKey(String[] key, String[] sno);
	
	List<UserGstin> findUserGstinByUserTaxpayer(String username, String gstin);
	
	List<UserTaxpayer> findPrimaryTaxpayer(int userId);
	
	List<UserTaxpayer> findSecondaryTaxpayer(int userId);
	
	boolean findUserTaxpayer(int userId, int taxpayerId);
	
	boolean findUserGstin(int userId, int taxpayerGstinId, int returnId);
	
	List<BrtMappingDTO> getBrtMapping(String[] temp);
	
	List<BrtMappingDTO> getAllBrtMapping();
	
	HsnChapter findHsnChapter(String code);
	
	HsnSac findHsnSac(Integer panId, String code);
	
	HsnSubChapter findHsnSubChapter(String code);
	
	HsnProduct findHsnProduct(String code);
	
	UserTaxpayer findUserTaxpayerByUser(String username, String pan);
	
	UserTaxpayer findUserTaxpayerByPan(String pan);
	
	ErpConfig findErpConfigByUser(Integer userId);
	
	List<ErpLog> getErpLogs(Integer userId);
	
	ErpLog getErpLog(Integer userId, String description);
	
	ErpConfig findErpConfigByUserProvider(Integer userId, String provider);
	
	List<GstinAction> findGstinActionByTaxpayerGstinId(Integer taxpayerGstinId);
	
	List<GstinTransaction> findGstinTransactionByIdMonth(Integer taxpayerGstinId, String monthYear);
	
	Vendor findVendorByRequest(String requestId);
	
	boolean checkTpMapping(Integer primaryTaxpayerId, Integer secondaryTaxpayerId);
	
	List<TpMapping> findSecondaryTpMapping(Integer secondaryTaxpayerId);
	
	Vendor findVendorByTpGstin(Integer taxpayerId, String gstin);
	
	List<Vendor> findVendorByTpPan(Integer taxpayerId, String pan);
	
	List<Vendor> getVendorByTaxpayerId(Integer taxpayerId);
	
	List<Vendor> getVendorByTaxpayerId(Integer taxpayerId, String type);
	
	List<HsnSac> getHsnSacByTaxpayerId(Integer taxpayerId);
	
	List<ItemCode> getItemCodeByTaxpayerId(Integer taxpayerId);
	
	List<InvoiceVendorDetails> getInvoiceVendorDetails(Integer taxpayerGstinId);
	
	InvoiceVendorDetails getInvoiceVendorDetailsById(Integer taxpayerGstinId, Integer id);
	
	InvoiceVendorDetails findInvoiceByNumber(String invoiceNo, Integer integer);
	
	Payment getPaymentFromId(String paymentId);

	Map<String,Object> getGstinTransactionByGstinMonthYearReturnType(GstinTransactionDTO gstinTransactionDTO,FilterDto filter) throws AppException;

	public <T> Map<String,Object> getFilteredData(GstinTransactionDTO gstinTransactionDTO,FilterDto filter,Class<T> type);
	
	Plan findPlanByCode(String code);
	
	List<Plan> findAllPlan();
	
	List<Plan> findPlanByType(String code);

	Organisation getOrganisationBySubscription(String subscription_id);
	
	Organisation getOrganisationByCustomer(String customer_id);

	List<UserGstin> getUserGstinByOrganisation(Integer id);

	public Class getEntityTypeByTransaction(String type);

	public <T>List<T> getInvoiceIdsByTypeMonthYearGstin(GstinTransactionDTO gstinTransactionDTO,FilterDto filterDto);

	<T>List<String> getInvoicesByTypeMonthYearGstin(GstinTransactionDTO gstinTransactionDTO);
	
	List getGstinTransactionByGstinMonthYear(GstinTransactionDTO gstinTransactionDTO,FilterDto filter) throws AppException;
	
	GSTR2 getMultipleGstinTransactionGstr2(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	List<ReturnStatus> getReturnStatusByTransaction(GstinTransactionDTO gstinTransactionDTO, String transactionType) throws AppException;
	
	public List<ReturnStatus> getReturnStatusByFlag(GstinTransactionDTO gstinTransactionDTO,String flag) throws AppException;
	
	public boolean isSubmit(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	public boolean isSubmit(String gstin,String monthYear,String returnType) throws AppException;
	
	public boolean getSubmitDetails(String gstin,String monthYear,String returnType) throws AppException;
	
	public JsonNode getSheetMapping(SourceType sourceType,ReturnType returnType);

	public List<GstCalendar> getAllGstCalendars() throws AppException;
	
	public List<Calendar> getAllCalendars() throws AppException;

	public List<Item> getBlankHsnInvoiceIdsByTypeMonthYearGstin(GstinTransactionDTO gstinTransactionDTO, FilterDto filterDto);
	
	public boolean getDataStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public TokenResponse getGstnDataStatus(GstinTransactionDTO gstinTransactionDTO);

	public List<String> getMonthYearsAsPerPreference(String monthYear, String gstin) throws AppException;
	
	public GstinAction findGstinActionByMonthYearReturn(String monthYear,String returnType,TaxpayerGstin taxpayerGstin);

	FilePrefernce findpreference(String gstin, String fyear);

	User findUserIdByUsername(String username);

	String findProvider(int OrganisationId);

	Organisation findOrganisationByUsername(String username) throws AppException;
	
	String findLegalName(String panCard);

}

