package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class LiabilityRegisterDTO {

	private long taxLiability = 0;
	
	private long creditLedger = 0;
	
	private long cashLedger = 0;
	
	private long netLiability = 0;
	
}
