package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
public @Data class ErpConfigDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;

	private byte active;

	private String connection;
	
	private byte isEdit;
	
	private String uniqueKey;
	
	private String serialNo;

	private String provider;

	private String transactionMappings;
	
}
