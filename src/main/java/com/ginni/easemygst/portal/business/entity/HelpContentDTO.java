package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

public @Data class HelpContentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String html;

	private String link;

	private String pageName;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	public interface HelpContentDTOMixIn extends CommonMixin {
		
	}

}
