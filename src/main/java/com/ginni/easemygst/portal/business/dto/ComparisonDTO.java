package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

import lombok.Data;

public @Data class ComparisonDTO implements Serializable {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

private String monthYear;

private String shortname;

private BigDecimal taxesAsPerGSTR1;

private BigDecimal outwardTaxesandSupplies;

private BigDecimal difference;

private BigDecimal percentageofExcess=BigDecimal.ZERO;

	public ComparisonDTO() {

	}

  public ComparisonDTO(String monthYear,Double taxesAsPerGSTR1,Double outwardTaxesandSupplies){
	  this.monthYear=monthYear;
	  this.taxesAsPerGSTR1=BigDecimal.valueOf(taxesAsPerGSTR1);
	  this.outwardTaxesandSupplies=BigDecimal.valueOf(outwardTaxesandSupplies);
	  this.difference=this.taxesAsPerGSTR1.subtract(this.outwardTaxesandSupplies);
	  if(this.taxesAsPerGSTR1.compareTo(BigDecimal.ZERO)>0){
	  this.percentageofExcess=this.difference.divide(BigDecimal.valueOf(taxesAsPerGSTR1),2,RoundingMode.HALF_UP
			  ).multiply(new BigDecimal(100));
	  }
			 if(!monthYear.equalsIgnoreCase("all"))
				  this.setShortName(monthYear);
  }


public void setShortName(String monthYear){
	
	try {
		this.shortname=CalanderCalculatorUtility.getShortCommaStyleMonthYear(monthYear);
	} catch (AppException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	
}
