package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ChangeStatusDto {
	
	public enum OperationType{
		ALL("ALL"),CTIN_FILTERED("CTIN_FILTERED");
		String type;
		 OperationType(String type){
			this.type=type;
		}
		 
		 @Override
			public String toString() {
				return this.type;
			};
	}

	private String invoiceId;
	
	@JsonProperty("supplierInvoiceId")
	private String supplierInvoiceId;
	
	private String status;
	
	private String type;//reconcile type
	
	@JsonProperty("flag")
	private String taxPayerAction;
	
	@JsonProperty("subAction")
	private String subAction;
	
	@JsonProperty("ctin")
	private String ctin;
	
	@JsonProperty("ctinName")
	private String ctinName;
	
	
	@JsonProperty("operationType")
	private String operationType;
	
	
	
	
	
}
