package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class MismatchCountDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("mismatch_roundoff")
	private long mismatchRoundoff;
	
	@JsonProperty("mismatch_invoice_no")
	private long mismatchInvoiceNo;
	
	@JsonProperty("mismatch_invoice_date")
	private long mismatchInvoiceDate;
	
	@JsonProperty("mismatch_major")
	private long mismatchMajor;

	@JsonProperty("missing_outward")
	private long missing_outward;
	
	@JsonProperty("missing_inward")
	private long missing_inward;
	
	@JsonProperty("no_similar_count")
	private long no_similar_count;
	
	@JsonProperty("similar_count")
	private long similar_count;
	
	public long getTotalCount(){
		return (this.mismatchInvoiceDate+this.mismatchInvoiceNo+this.getMismatchMajor()+this.mismatchRoundoff+this.missing_inward);
	}
}
