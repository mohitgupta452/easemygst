package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

public @Data class TaxpayerAccountDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String credit;

	private String debit;

	private String description;

	private int referenceId;

	private String tag;

	private String transactionId;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	public interface TaxpayerAccountDTOMixIn extends CommonMixin {
		
	}
	
}
