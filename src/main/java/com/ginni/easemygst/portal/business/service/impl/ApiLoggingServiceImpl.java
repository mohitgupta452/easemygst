package com.ginni.easemygst.portal.business.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.NotificationService;
import com.ginni.easemygst.portal.persistence.entity.ActionLog;
import com.ginni.easemygst.portal.persistence.entity.Publicnotification;
import com.ginni.easemygst.portal.persistence.entity.transaction.EmgstApiLog;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.subscription.SubscriptionService;
import com.mashape.unirest.http.JsonNode;

@Stateless
public class ApiLoggingServiceImpl implements ApiLoggingService {

	protected final Logger _Logger = LoggerFactory.getLogger(this.getClass());	
	
	private static final List<String> ignorableUrls =Arrays.asList(new String[]{"/get/notification/","/get/states"});
	
	public enum UserAction{
		ADD_TIN("ADD_TIN"),
		ADD_USER("ADD_USER"),
		EDIT_USER_ACCESS("EDIT_USER_ACCESS"),
		ACCESS_GRANTED_FOR_PARTICULAR_TIN("ACCESS_GRANTED_FOR_PARTICULAR_TIN"),
		ACCESS_REVOKED_FOR_PARTICULAR_TIN("ACCESS_REVOKED_FOR_PARTICULAR_TIN"),
		ACCESS_GRANTED_FOR_ALL_TIN("ACCESS_GRANTED_FOR_ALL_TIN"),
		ACCESS_REVOKED_FOR_ALL_TIN("ACCESS_REVOKED_FOR_ALL_TIN"),

		CHANGE_PASSWORD("CHANGE_PASSWORD"),
		ADD_PARTNER("ADD_PARTNER"),
		UPLOAD_INVOICES("UPLOAD_INVOICES"),
		SAVE_TO_GSTN("SAVE_TO_GSTN"),
		SYNCED_WITH_ERROR_TO_GSTN("SYNCED_WITH_ERROR_TO_GSTN"),
		SUBMIT_RETURN("SUBMIT_RETURN"),
		ADD_TURNOVER_DETAILS("ADD_TURNOVER_DETAILS"),
		SYNC_WITH_ERP("SYNC_WITH_ERP"),
		GET_2A("GET_2A"),
		CLAIM_ITC("CLAIM_ITC"),
		DELETE_INVOICE("DELETE_INVOICE"),
		DELETE_ALL_INVOICES("DELETE_ALL_INVOICES"),
		UPLOAD_INVOICES_WITH_DELETE_ALL("UPLOAD_INVOICES_WITH_DELETE_ALL"),
		ADD_IVOICE("ADD_IVOICE"),
		EDIT_IVOICE("EDIT_IVOICE"),
		GENRATE_GSTR3B("GENRATE_GSTR3B"),
		SAVE_GSTR3B("SAVE_GSTR3B"),
		CONNECT_ERP("CONNECT_ERP"),
		ADD_VENDOR("ADD_VENDOR"),
		ADD_CUSTOMER("ADD_CUSTOMER"),
		EDIT_GSTN_USERNAME("EDIT_GSTN_USERNAME"),	
		DISCONNECT_ERP("DISCONNECT_ERP")
;
											
		private String type;
		private UserAction(String type){
			this.type=type;
		}
		
		/*@Override
		public String toString() {
			return this.type;
		};*/
	}
	@Inject
	private HttpServletRequest req;
	
	@Inject
	private UserBean userBean;
	
	@Inject 
	private CrudService crudService;
	
	public boolean logEmgstRequest(ContainerRequestContext  request) {
		String url=request.getUriInfo().getAbsolutePath().toString();
		if(this.isLoggable(request.getUriInfo())){
		this.isLoggable(request.getUriInfo());
		EmgstApiLog emgstLog=new EmgstApiLog();
		emgstLog.setUrl(url);
		emgstLog.setIpAddress(req.getRemoteHost());
		emgstLog.setRequestTime(new Date());
		emgstLog.setRequestType(req.getMethod());
		emgstLog.setRequestPayload(this.readEntityStream(request));
		if(!Objects.isNull(userBean)&&!Objects.isNull(userBean.getUserDto()))
			emgstLog.setRequestedBy(userBean.getUserDto().getUsername());
		crudService.create(emgstLog);
		}
		return false;
	}
	
	

	private String readEntityStream(ContainerRequestContext requestContext)
    {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        final InputStream inputStream = requestContext.getEntityStream();
        final StringBuilder builder = new StringBuilder();
        try
        {
        	IOUtils.copy(inputStream, outStream);
            byte[] requestEntity = outStream.toByteArray();
            if (requestEntity.length == 0) {
                builder.append("");
            } else {
                builder.append(new String(requestEntity));
            }
            requestContext.setEntityStream(new ByteArrayInputStream(requestEntity) );
        } catch (IOException ex) {
            _Logger.debug("----Exception occurred while reading entity stream in auth filter :{}",ex.getMessage());
        }
        return builder.toString();
    }
	
	
	private boolean isLoggable(UriInfo uriInfo)
    {
		for(String url:ignorableUrls){
       if(uriInfo.getAbsolutePath().toString().contains(url)){
    	   return false;
       }
		}
		return true;
    }
	
	public boolean saveAction(GstinTransactionDTO gstinTransactionDto,ActionLog actionLog,MessageDto
			 messageDto){
		/*UserDTO userDto=null;
		//MessageDto messageDto=new MessageDto();
		if(!Objects.isNull(userBean)&&!Objects.isNull(userBean.getUserDto()))
			userDto=userBean.getUserDto();
		actionLog.setOrganizationId(userDto.getOrganisation().getId());
		actionLog.setUserId(userDto.getUsername());
		actionLog.setActionTime(new Date());
		actionLog.setGstin(gstinTransactionDto.getTaxpayerGstin().getGstin());
		String message =this.getNotificationMessageBy(actionLog.getAction());
		actionLog.setMessage(this.formatMessage(message, messageDto));
		
		crudService.create(actionLog);*/
		return true;
	}
	
	@Override
	public boolean saveAction(ActionLog actionLog,MessageDto
			 messageDto){
		/*UserDTO userDto=null;
		//MessageDto messageDto=new MessageDto();
		if(!Objects.isNull(userBean)&&!Objects.isNull(userBean.getUserDto()))
			userDto=userBean.getUserDto();
		actionLog.setOrganizationId(userDto.getOrganisation().getId());
		actionLog.setActionTime(new Date());
		actionLog.setUserId(userDto.getUsername());
		String message =this.getNotificationMessageBy(actionLog.getAction());
		actionLog.setMessage(this.formatMessage(message, messageDto));
		
		crudService.create(actionLog);*/
		return true;
	}
	
	@Override
	public boolean saveAction(UserAction action,MessageDto
			 messageDto){
		try{
		UserDTO userDto=null;
		ActionLog actionLog=new ActionLog();
		//MessageDto messageDto=new MessageDto();
		if(!Objects.isNull(userBean)&&!Objects.isNull(userBean.getUserDto()))
			userDto=userBean.getUserDto();
		actionLog.setOrganizationId(userDto.getOrganisation().getId());
		actionLog.setActionTime(new Date());
		actionLog.setAction(action);
		actionLog.setUserId(userDto.getUsername());
		String message =this.getNotificationMessageBy(action);
		actionLog.setMessage(this.formatMessage(message, messageDto));
		actionLog.setIconCategory(this.getNotificationIcon(action));
		actionLog.setCategory(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString());

		
		crudService.create(actionLog);
		}catch (Exception e) {
			_Logger.error("error in save action", e);
			e.printStackTrace();
		}
		return true;
	}
	
	
	@Override
	public boolean saveAction(UserAction action,MessageDto
			 messageDto,String gstin){
		try{
		UserDTO userDto=null;
		ActionLog actionLog=new ActionLog();
		//MessageDto messageDto=new MessageDto();
		if(!Objects.isNull(userBean)&&!Objects.isNull(userBean.getUserDto()))
			userDto=userBean.getUserDto();
		actionLog.setOrganizationId(userDto.getOrganisation().getId());
		actionLog.setActionTime(new Date());
		actionLog.setAction(action);
		actionLog.setGstin(gstin);
		actionLog.setUserId(userDto.getUsername());
		String message =this.getNotificationMessageBy(action);
		actionLog.setMessage(this.formatMessage(message, messageDto));
		actionLog.setIconCategory(this.getNotificationIcon(action));
		actionLog.setCategory(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString());

		
		crudService.create(actionLog);
		}catch (Exception e) {
			_Logger.error("error in save action", e);
			e.printStackTrace();
		}
		return true;
	}
	
	private String getNotificationMessageBy(UserAction action) {
		String message = null;
		try
		{
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("/useraction.properties");

		
			if (Objects.isNull(is))
				throw new IOException();
			properties.load(is);
			message = properties.getProperty(action.toString());
		} catch (IOException e) {
			_Logger.error("error in reading user action property file file not found", e);
			e.printStackTrace();
		}
		catch (Exception e) {
			_Logger.error("error in save action", e);
			e.printStackTrace();
		}

		return message;

	}
	
	private String formatMessage(String message,MessageDto messageDto){
		try{
		if(!StringUtils.isEmpty(message)&&Objects.nonNull(messageDto)){
			message=message.replace("[GSTIN]", Objects.nonNull(messageDto.getGstin())?messageDto.getGstin():"");
			message=message.replace("[RETURN_TYPE]", Objects.nonNull(messageDto.getReturnType())?messageDto.getReturnType():"");
			message=message.replace("[MONTHYEAR]", Objects.nonNull(messageDto.getMonthYear())?messageDto.getMonthYear():"");
			//message.replace("[USERID]", messageDto.getus);
			message=message.replace("[USERNAME_PRIMARY]", Objects.nonNull(messageDto.getUserNamePrimary())?messageDto.getUserNamePrimary():"");
			message=message.replace("[USERNAME_SECONDARY]", Objects.nonNull(messageDto.getUserNameSecondary())?messageDto.getUserNameSecondary():"");
			message=message.replace("[EMAILID_PRIMARY]", Objects.nonNull(messageDto.getEmailIdPrimary())?messageDto.getEmailIdPrimary():"");
			message=message.replace("[NO_OF_INVOICES]", Objects.nonNull(messageDto.getNoOfInvoices())?messageDto.getNoOfInvoices():"");
			message=message.replace("[EMAILID_SECONDARY]", Objects.nonNull(messageDto.getEmialIdSecondary())?messageDto.getEmialIdSecondary():"");
			message=message.replace("[PARTNER_NAME]", Objects.nonNull(messageDto.getPartnerName())?messageDto.getPartnerName():"");
			message=message.replace("[ITC_AMOUNT]", Objects.nonNull(messageDto.getItcAmount())?messageDto.getItcAmount():"");
			message=message.replace("[INVOICE_NUMBER]", Objects.nonNull(messageDto.getInvoiceNumber())?messageDto.getInvoiceNumber():"");
			message=message.replace("[CUSTOMER_NAME]", Objects.nonNull(messageDto.getCustomerName())?messageDto.getCustomerName():"");
			message=message.replace("[VENDOR_NAME]", Objects.nonNull(messageDto.getVendorName())?messageDto.getVendorName():"");
			message=message.replace("[TRANSACTION_TYPE]", Objects.nonNull(messageDto.getTransactionType())?messageDto.getTransactionType():"");
			message=message.replace("[USERNAME_PREVIOUS]", Objects.nonNull(messageDto.getUserDisplayNamePrevious())?messageDto.getUserDisplayNamePrevious():"");
			message=message.replace("[USERNAME_NEW]", Objects.nonNull(messageDto.getUserDisplayNameNew())?messageDto.getUserDisplayNameNew():"");
			
	}
		}catch(Exception e){
			_Logger.error("errro in format notifiacation message ",e);
		}
		return message;
	}
	
	
	
	private String getNotificationIcon(UserAction action) {
		String message = "INFO";
		try
		{
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("/useractionicon.properties");

		
			if (Objects.isNull(is))
				throw new IOException();
			properties.load(is);
			message = properties.getProperty(action.toString());
		} catch (IOException e) {
			_Logger.error("error in reading user action icon property file file not found", e);
			e.printStackTrace();
		}
		catch (Exception e) {
			_Logger.error("error in reading user action icon", e);
			e.printStackTrace();
		}

		return message;

	}
	
	@Override
	public boolean saveGovtNotification(JsonNode request){
		try{
		Publicnotification notification=new Publicnotification();
			notification.setCategory(request.getObject().has("category")?request.getObject().getString("category"):"");
			notification.setCircularNumber(request.getObject().has("circularNumber")?request.getObject().getString("circularNumber"):"");
			String dateStr=request.getObject().has("circularDate")?request.getObject().getString("circularDate"):null;
			Date date=StringUtils.isEmpty(dateStr)?null:SubscriptionService.formsDateFormat.parse(dateStr);
			notification.setDate(date);
			notification.setDocumentNumber(request.getObject().has("documentNumber")?request.getObject().getString("documentNumber"):"");
			notification.setUrl(request.getObject().has("url")?request.getObject().getString("url"):"");
			notification.setSubject(request.getObject().has("subject")?request.getObject().getString("subject"):"");
			notification.setIconCategory("INFO");
			crudService.create(notification);
		}catch(ParseException e){
			_Logger.error("error in parsing  govt notification date ", e);
			e.printStackTrace();
		}
		
		catch (Exception e) {
			_Logger.error("error in saving public notification", e);
			e.printStackTrace();
		}
		return true;
	}

}
