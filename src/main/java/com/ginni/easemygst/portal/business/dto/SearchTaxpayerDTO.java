package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SearchTaxpayerDTO {
	
	@JsonProperty("ctb")
	private String constitutionBusiness;
	
	@JsonProperty("rgdt")
	private String registrationDate;
	
	@JsonProperty("sts")
	private String status;
	
	@JsonProperty("dty")
	private String taxpayerType;
	
	@JsonProperty("ctj")
	private String centreJudrication;

}
