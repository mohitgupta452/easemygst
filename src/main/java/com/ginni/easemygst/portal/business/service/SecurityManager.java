package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface SecurityManager {
public void checkIfSaveNotAllowedForGstin(GstinTransactionDTO gstinTransactionDto) throws AppException;

public boolean checkSubscriptionDurationByGstin(GstinTransactionDTO gstinTransactionDto) throws AppException;

public void checkTrialPeriodConstraint() throws AppException;

}
