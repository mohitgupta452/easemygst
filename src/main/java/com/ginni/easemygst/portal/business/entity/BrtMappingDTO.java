package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

public @Data class BrtMappingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;

	private byte active;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	private BusinessTypeDTO businessTypeBean;

	private GstReturnDTO gstReturnBean;

	private ReturnTransactionDTO returnTransaction;

	public interface BrtMappingDTOMixIn extends CommonMixin {
		
	}
}
