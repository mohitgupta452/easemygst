package com.ginni.easemygst.portal.business.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface CommonMixin { 
	@JsonIgnore abstract Byte getUpdationIpAddress();
	@JsonIgnore abstract Byte getUpdationTime();
	@JsonIgnore abstract Byte getUpdatedBy();
	@JsonIgnore abstract Byte getCreationIpAddress();
	@JsonIgnore abstract Byte getCreationTime();
	@JsonIgnore abstract Byte getCreatedBy();
	@JsonIgnore abstract Byte getActive();
}
