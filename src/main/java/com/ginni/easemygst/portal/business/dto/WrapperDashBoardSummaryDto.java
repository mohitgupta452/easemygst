package com.ginni.easemygst.portal.business.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.view.serializer.FinancialYearMapSerializer;

import lombok.Data;

public @Data class WrapperDashBoardSummaryDto {
	
	@JsonProperty("gstr1")
	private DashBoardSummaryDto gstr1=new DashBoardSummaryDto();
	
	@JsonProperty("gstr2")
	private DashBoardSummaryDto gstr2=new DashBoardSummaryDto();
	
	@JsonProperty("gstr1a")
	private DashBoardSummaryDto gstr1a=new DashBoardSummaryDto();
	
	@JsonProperty("gstr3")
	private DashBoardSummaryDto gstr3=new DashBoardSummaryDto();
	
	@JsonProperty("monthYear")
	private String monthYear;
	
	boolean isPrevious;
	
	boolean isNext;
	
	String  previousMonthYear;
	
	String  nextMonthYear;
	
	String monthName;
	
	String code;
	
	String breadcrumb;
	
	List<ReturnSummaryDTO> returnSummary;
	
	String  previousMonthYearShort;
	
	String  nextMonthYearShort;
	
	//@JsonSerialize(using=FinancialYearMapSerializer.class)
	@JsonIgnore
	Map<String,String> currentFiscal=new TreeMap<>();
	
	
	List<MonthYearCodeName>currentFiscals=new ArrayList<>();
	

	public@Data static class MonthYearCodeName{
		private String code;
		
		private String name;
	}
	
	public List<MonthYearCodeName> getCurrentFiscals(){
		List<MonthYearCodeName>datas=new ArrayList<>();
		for(Map.Entry<String,String> ent:this.getCurrentFiscal().entrySet()){
			MonthYearCodeName month=new MonthYearCodeName();
			month.setCode(ent.getKey());
			month.setName(ent.getValue());
			datas.add(month);
			}
		return datas;
	}
	
}
