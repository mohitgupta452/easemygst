package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

public @Data class BillingAddressDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String address;

	private String city;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String legalName;

	private String pincode;

	private String state;

	private String tinNumber;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	public interface BillingAddressDTOMixIn extends CommonMixin {
		
	}

}
