package com.ginni.easemygst.portal.business.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;


import lombok.Data;

public @Data class ChallanDTO {
	
    private String particular;	

    private BigDecimal tax;
	
	private BigDecimal interest=BigDecimal.ZERO;
	
	private BigDecimal fee=BigDecimal.ZERO;
	
	private BigDecimal total;
	
 	int numOfDays = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_YEAR);

	public ChallanDTO(String particular, BigDecimal tax,int noofdays,BigDecimal liability) {
		super();
		this.particular = particular;
		this.tax = tax;
		if(this.tax.compareTo(BigDecimal.ZERO) > 0 && noofdays>0){
		this.interest =tax.multiply(new BigDecimal(18*noofdays)).divide(new BigDecimal(100*numOfDays),2,RoundingMode.HALF_UP);
		}
		if(!this.particular.equalsIgnoreCase("integrated") && !this.particular.equalsIgnoreCase("cess") && !(liability.compareTo(BigDecimal.ZERO)==0 )){
		this.fee = new BigDecimal(25).multiply(new BigDecimal(noofdays));
		}
	   if(!this.particular.equalsIgnoreCase("integrated") && !this.particular.equalsIgnoreCase("cess")&& (liability.compareTo(BigDecimal.ZERO)==0 ) ){
			this.fee = new BigDecimal(10).multiply(new BigDecimal(noofdays));
		}
		this.total =tax.add(this.interest.add(this.fee)) ;
	}

	public ChallanDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
}
