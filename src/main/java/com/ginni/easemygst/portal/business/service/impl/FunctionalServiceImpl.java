package com.ginni.easemygst.portal.business.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.AccessTimeout;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.CashLedgerDto;
import com.ginni.easemygst.portal.business.dto.GstrFlowDTO;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ItcLedgerDto;
import com.ginni.easemygst.portal.business.dto.LedgerDto;
import com.ginni.easemygst.portal.business.dto.LiabilityLedgerDto;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.GstnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.ReturnStatusDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.business.service.GstnService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ReturnsService.DeleteType;
import com.ginni.easemygst.portal.business.service.ReturnsService.GstnAction;
import com.ginni.easemygst.portal.business.service.ScheduleManager;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata.Returns;
import com.ginni.easemygst.portal.data.BaseSummaryTotal;
import com.ginni.easemygst.portal.data.part.CashLedgerBalance;
import com.ginni.easemygst.portal.data.part.LedgerCashTransactionDetail;
import com.ginni.easemygst.portal.data.part.LedgerITCDetails;
import com.ginni.easemygst.portal.data.part.LedgerITCTransactionDetail;
import com.ginni.easemygst.portal.data.part.LedgerTax;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.gst.GSTNResponseProcessor;
import com.ginni.easemygst.portal.gst.api.SignDataVerificationUtil;
import com.ginni.easemygst.portal.gst.request.FileGstr1Req;
import com.ginni.easemygst.portal.gst.request.GenGstr3Req;
import com.ginni.easemygst.portal.gst.request.GetGstr1AReq;
import com.ginni.easemygst.portal.gst.request.GetGstr1Req;
import com.ginni.easemygst.portal.gst.request.GetGstr2Req;
import com.ginni.easemygst.portal.gst.request.GetGstr3DetailReq;
import com.ginni.easemygst.portal.gst.request.GetReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.Gstr1ASummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr1SummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr2SummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest;
import com.ginni.easemygst.portal.gst.request.SaveGstr1Req;
import com.ginni.easemygst.portal.gst.request.SaveGstr2Req;
import com.ginni.easemygst.portal.gst.request.SetOffLiabilityDataReq;
import com.ginni.easemygst.portal.gst.request.SetOffLiabilityDataReq.PaidCash;
import com.ginni.easemygst.portal.gst.request.SubmitGstr1Req;
import com.ginni.easemygst.portal.gst.request.SubmitGstr2Req;
import com.ginni.easemygst.portal.gst.request.SubmitRefundData;
import com.ginni.easemygst.portal.gst.request.SubmitRefundData.Claim;
import com.ginni.easemygst.portal.gst.request.ViewTrackReturnsReq;
import com.ginni.easemygst.portal.gst.response.EFileDetails;
import com.ginni.easemygst.portal.gst.response.FileGstr1Resp;
import com.ginni.easemygst.portal.gst.response.GSTNRespStatus;
import com.ginni.easemygst.portal.gst.response.Get2AFileResp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr1AResp;
import com.ginni.easemygst.portal.gst.response.GetGstr1Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.gst.response.Gstr1ASummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr1SummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr2SummaryResp;
import com.ginni.easemygst.portal.gst.response.LiabilityLedgerDetail;
import com.ginni.easemygst.portal.gst.response.SaveGstr1Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr2Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr3bResponse;
import com.ginni.easemygst.portal.gst.response.SectionSummary;
import com.ginni.easemygst.portal.gst.response.SubmitGstr1Resp;
import com.ginni.easemygst.portal.gst.response.SubmitGstr2Resp;
import com.ginni.easemygst.portal.gst.response.TransactionHeads;
import com.ginni.easemygst.portal.gst.response.ViewTrackReturnsResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.notify.LegacyEmail;
import com.ginni.easemygst.portal.persistence.entity.EncodedData;
import com.ginni.easemygst.portal.persistence.entity.FilePrefernce;
import com.ginni.easemygst.portal.persistence.entity.GstinAction;
import com.ginni.easemygst.portal.persistence.entity.GstinTransaction;
import com.ginni.easemygst.portal.persistence.entity.GstnInvsCountMetadata;
import com.ginni.easemygst.portal.persistence.entity.GstnTransaction;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TrackReturnsInfo;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TaxpayerSummary;
import com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.GSTR6FileGenerator;
import com.ginni.easemygst.portal.utils.Utility;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.core.util.AsJson;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

@Stateless
@Transactional
public class FunctionalServiceImpl implements FunctionalService {

	@Inject
	private Logger log;

	@Inject
	private FinderService finderService;

	@Inject
	private UserBean userBean;

	@Inject
	private CrudService crudService;

	@Inject
	private GstnService gstnService;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private ReturnsService returnService;

	@Inject
	private RedisService redisService;

	@Inject
	private ScheduleManager scheduleManager;

	@Inject
	private ApiLoggingService apiLoggingService;

	@Inject
	private GSTNResponseProcessor gstnResponseProcessor;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public String syncGstr1TransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		String tempResponse = "";

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();
			String gstin = gstinTransactionDTO.getTaxpayerGstin().getGstin();
			String gstReturn = gstinTransactionDTO.getReturnType();

			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				transactionTypes.add(gstinTransactionDTO.getTransactionType());
			} else {
				Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getReturnTransactionsByGstin(gstin,
						gstReturn);
				for (ReturnTransactionDTO returnTransactionDTO : returnTransactionDTOs) {
					transactionTypes.add(returnTransactionDTO.getCode());
				}
			}

			for (String type : transactionTypes) {
				if (type.equalsIgnoreCase(gstinTransactionDTO.getTransactionType())) {// temporary
																						// condition
																						// until
																						// gstr1
																						// deserializer
																						// developed
																						// will
																						// retrieve
																						// only
																						// data
																						// of
																						// specified
																						// transaction
					GetGstr1Req getGstr1Req = new GetGstr1Req();
					getGstr1Req.setAction(type);
					getGstr1Req.setActionReq("N");
					getGstr1Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
					getGstr1Req.setMonthYear(gstinTransactionDTO.getMonthYear());
					// getGstr1Req.setStateCode(gstinTransactionDTO.getTaxpayerGstin().getStateBean().getScode());
					// getGstr1Req.setFromTime("01/07/2017");

					gstnService.getGstr1Data(getGstr1Req);
					GetGstr1Resp getGstr1Resp = new GetGstr1Resp();
					getGstr1Resp = gstnService.getGstr1Data(getGstr1Req);
					//
					// new
					// GstnServiceExecutor().insertUpdateGstnData(getGstr1Resp,
					// TransactionType.valueOf(type),
					// finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()),
					// gstinTransactionDTO.getMonthYear());
				}
			}

			return tempResponse;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public String syncGstr3BTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		String tempResponse = "";
		Gstr3BRequest gstr3b = returnService.getGstr3BRequest(gstinTransactionDTO);
		// List<TblOutwardCombinedNew>
		// tbls=returnService.getGstr3BData(gstinTransactionDTO);

		try {
			if (gstr3b != null) {

				gstr3b.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				gstr3b.setReturnPeriod(gstinTransactionDTO.getMonthYear());
				gstr3b.setAction(Returns.RETSAVE3B.toString());

				String transitId = Utility.randomString(12);

				JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

				InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("gstnjson/savegstr3b.json");
				String jsonTxt = IOUtils.toString(is);

				ObjectMapper mapper = new ObjectMapper();
				JsonNode schemaNode = mapper.readTree(jsonTxt);
				JsonNode data1 = mapper.convertValue(gstr3b, JsonNode.class);
				String data = mapper.writeValueAsString(gstr3b);
				ProcessingReport report = VALIDATOR.validateUnchecked(schemaNode, data1);

				final boolean success = report.isSuccess();

				final JsonNode reportAsJson = ((AsJson) report).asJson();

				log.info("rpayload json  {}", data1.asText());
				log.info("json validation result {}  report {}", success, reportAsJson);
				SaveGstr3bResponse saveGstr3Resp = null;
				if (success) {
					saveGstr3Resp = gstnService.saveGstr3bData(gstr3b);

					log.debug("save gstr3bresponse {}" + saveGstr3Resp.toString());
					ReturnStatus returnStatus = new ReturnStatus();

					gstinTransactionDTO.setTransitId(transitId);
					if (saveGstr3Resp != null && !StringUtils.isEmpty(saveGstr3Resp.getReferenceId())) {
						// this.updateFlag(gstr3b, gstinTransactionDTO, transitId,returnStatus);
						returnStatus.setCreatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						returnStatus.setCreationIpAddress(userBean.getIpAddress());
						returnStatus.setCreationTime(Timestamp.from(Instant.now()));
						returnStatus.setTaxpayerGstin(
								finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
						returnStatus.setReturnType(ReturnType.GSTR3B.toString());
						returnStatus.setTransaction(ReturnType.GSTR3B.toString());
						returnStatus.setTransitId(transitId);
						returnStatus.setMonthYear(gstr3b.getReturnPeriod());
						returnStatus.setEvent("SAVE");
						returnStatus.setReferenceId(saveGstr3Resp.getReferenceId());
						returnStatus.setTransactionId("");
						returnStatus.setStatus("PENDING");
						crudService.create(returnStatus);
						// this.checkReturnStatusByTransaction(gstinTransactionDTO,"GSTR3B");
						gstinTransactionDTO.setTransitId(transitId);
						gstinTransactionDTO.setReturnType(ReturnType.GSTR3B.toString());
						GetCheckStatusResp respon = this.checkReturnStatusGstr3b(gstinTransactionDTO);

						tempResponse = (String) this.processGstnReturnStatusResponse(respon)
								.get(ApplicationMetadata.RESP_MSG_KEY);
					} else {
						gstnResponseProcessor.processGstnResponse(saveGstr3Resp,
								gstinTransactionDTO.getTaxpayerGstin().getGstin());
					}
				} else {
					try {
						new LegacyEmail().sendToEMGSupport(
								"Request payload :" + data1.asText() + "\n Error Report " + reportAsJson,
								"INVALID JSON  " + gstinTransactionDTO.getTaxpayerGstin().getGstin());
					} catch (Exception e) {
						log.error("error while sending email ", e);
					}
					throw new AppException(ExceptionCode._INVALID_JSON);
				}

			} else
				throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.NO_DATA_TO_BE_SYNC);
		} catch (AppException e) {
			log.error("app exception ", e);
			throw e;
		} catch (IOException | IllegalArgumentException e) {
			log.error("io exeption", e);
			if (e.getCause().getCause() instanceof AppException) {
				throw (AppException) e.getCause().getCause();
			}
			throw new AppException();
		}

		return tempResponse;
	}

	@Override
	public String syncGstr1ActionRequiredTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		String tempResponse = "";
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();
			String gstin = gstinTransactionDTO.getTaxpayerGstin().getGstin();
			String gstReturn = gstinTransactionDTO.getReturnType();

			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				transactionTypes.add(gstinTransactionDTO.getTransactionType());
			} else {
				transactionTypes.add(TransactionType.B2B.toString());
				transactionTypes.add(TransactionType.CDN.toString());
			}

			for (String type : transactionTypes) {

				GetGstr1Req getGstr1Req = new GetGstr1Req();
				getGstr1Req.setAction(type);
				getGstr1Req.setActionReq("Y");
				getGstr1Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr1Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr1Req.setStateCode(gstinTransactionDTO.getTaxpayerGstin().getStateBean().getScode());

				GetGstr1Resp getGstr1Resp = new GetGstr1Resp();
				getGstr1Resp = gstnService.getGstr1Data(getGstr1Req);

				if (!Objects.isNull(getGstr1Resp)) {
					if (getGstr1Resp.getStatusCd().equalsIgnoreCase("1")) {
						Method[] methods = GetGstr1Resp.class.getMethods();
						for (Method method : methods) {
							if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class") == -1) {
								String methodName = method.getName();
								if (methodName.toLowerCase().contains(type.toLowerCase())) {
									try {
										Object obj = method.invoke(getGstr1Resp);

										GstnTransactionDTO gstnTransactionDTO = new GstnTransactionDTO();
										gstnTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
										gstnTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
										gstnTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
										gstnTransactionDTO.setTransactionType(type);
										gstnTransactionDTO.setActionRequired((byte) 1);

										GstnTransaction gstnTransaction = finderService
												.getGstnTransaction(gstnTransactionDTO);
										if (Objects.isNull(gstnTransaction)) {
											gstnTransaction = new GstnTransaction();
											gstnTransaction.setTaxpayerGstin(finderService.findTaxpayerGstin(
													gstinTransactionDTO.getTaxpayerGstin().getGstin()));
											gstnTransaction.setMonthYear(gstinTransactionDTO.getMonthYear());
											gstnTransaction.setReturnType(gstinTransactionDTO.getReturnType());
											gstnTransaction.setTransactionType(type);
											gstnTransaction.setActionRequired((byte) 1);
										}

										String transactionObject = JsonMapper.objectMapper.writeValueAsString(obj);
										gstnTransaction.setTransactionObject(transactionObject);

										if (Objects.isNull(gstnTransaction.getId())) {
											gstnTransaction.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setCreationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

											crudService.create(gstnTransaction);
										} else {
											gstnTransaction.setUpdatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setUpdationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
											crudService.update(gstnTransaction);
										}

									} catch (IllegalAccessException | IllegalArgumentException
											| InvocationTargetException | JsonProcessingException e) {
										e.printStackTrace();
									}

								}
							}
						}
					}
				}
			}

			return tempResponse;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public JsonNode compareReturnSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException {

		ObjectNode objectNode = new ObjectMapper().createObjectNode();

		String gstnSummary = this.syncGstr1SummaryData(gstinTransactionDTO);

		log.debug("gstn summary  {} ", gstnSummary);

		Gstr1SummaryResp gstr1SummaryResp = new ObjectMapper().readValue(gstnSummary, Gstr1SummaryResp.class);

		gstnResponseProcessor.processGstnResponse(gstr1SummaryResp, gstinTransactionDTO.getTaxpayerGstin().getGstin());

		boolean status = this.compareSummary(gstr1SummaryResp, gstinTransactionDTO);

		objectNode.set("summary", new ObjectMapper().readValue(gstnSummary, ObjectNode.class));
		objectNode.put("isMatch", true);
		objectNode.put("message", true ? "" : "summary mismatched from gstn" + GSTNRespStatus.CONTACTSUPPORT_MSG);
		return objectNode;
	}

	@Override
	public String syncGstr1SummaryData(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonProcessingException {

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			String type = "SUMMARY";
			Gstr1SummaryReq gstr1SummaryReq = new Gstr1SummaryReq();
			gstr1SummaryReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			gstr1SummaryReq.setReturnPeriod(gstinTransactionDTO.getMonthYear());
			String response = gstnService.getGstr1Summary(gstr1SummaryReq);

			TaxpayerSummary summary;

			TaxpayerGstin gstin = finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

			return response;

			/*
			 * for(SectionSummary sectionSummary : gstr1SummaryResp.getSecSummary()){
			 * List<TaxpayerSummary> cptSummaries = new ArrayList<>();
			 * 
			 * if("L".equalsIgnoreCase(gstr1SummaryResp.getSummaryType())) for(CPartySummary
			 * cPartySummary : sectionSummary.getCpsummaries()) { TaxpayerSummary cSummary =
			 * new TaxpayerSummary();
			 * 
			 * cSummary.setTaxpayerGstin(gstin);
			 * cSummary.setChecksum(cPartySummary.getChecksum());
			 * cSummary.setInvoiceCount(cPartySummary.getTotalInvoice());
			 * cSummary.setTransactionType(sectionSummary.getSectionName());
			 * 
			 * this.setBaseSummary(cSummary, cPartySummary); cptSummaries.add(cSummary); }
			 * 
			 * summary = new TaxpayerSummary();
			 * summary.setMonthYear(gstr1SummaryResp.getReturnPeriod());
			 * summary.setSummaryType(gstr1SummaryResp.getSummaryType());
			 * summary.setCptSummaries(cptSummaries); this.setBaseSummary(summary,
			 * sectionSummary); summary.setTaxpayerGstin(gstin);
			 * summary.setChecksum(sectionSummary.getChecksum());
			 * summary.setInvoiceCount(sectionSummary.getTotalInvoice());
			 * 
			 * summary.setTransactionType(sectionSummary.getSectionName());
			 * crudService.create(summary);
			 * 
			 * }
			 */
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private boolean compareSummary(Gstr1SummaryResp gstr1SummaryResp, GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonProcessingException {
		gstinTransactionDTO.setTransactionType("All");

		Map<String, Object> emgReturnSummaryMap = returnService.getTransactionSummary(gstinTransactionDTO);
		log.debug("emg summary  {} ", emgReturnSummaryMap);

		for (SectionSummary secSumm : gstr1SummaryResp.getSecSummary()) {
			Object sumObject = emgReturnSummaryMap.get(secSumm.getSectionName().toLowerCase());
			if (!Objects.isNull(sumObject)) {
				TransactionDataDTO dataDTO = ((TransactionDataDTO) sumObject);
				if (secSumm.getSectionName().equalsIgnoreCase("NIL")) {
					if ((secSumm.getTotalNgsupAmt() != dataDTO.getNonGstAmount())
							|| (secSumm.getTotalNilSupAmt() != dataDTO.getNilAmount())
							|| (secSumm.getTotalExptAmt() != dataDTO.getExemptedAmount()))
						return false;
				} else if (secSumm.getSectionName().equalsIgnoreCase("DOC_ISSUE")) {
					if ((secSumm.getNetDocIssued() != dataDTO.getNetDocIssued())
							|| (secSumm.getTotalDocCanceled() != dataDTO.getTotalCanceled())
							|| (secSumm.getTotalDocIssue() != dataDTO.getTotalDocIssued()))
						return false;
				} else if (!BigDecimal.valueOf(secSumm.getTotalValue()).equals(BigDecimal
						.valueOf(dataDTO.getTaxableValue()).add(BigDecimal.valueOf(dataDTO.getTaxAmount())))) {
					return false;
				}
			}
		}
		return true;
	}

	private void setBaseSummary(TaxpayerSummary summary, BaseSummaryTotal sectionSummary) {
		summary.setCess(sectionSummary.getTotalCess());
		// summary.setIgst(sectionSummary.getTotalIgst());
		summary.setCgst(sectionSummary.getTotalCgst());
		summary.setSgst(sectionSummary.getTotalSgst());
		summary.setTaxAmount(sectionSummary.getTotalTax());
		summary.setValue(sectionSummary.getTotalValue());
		InfoService.setInfo(summary, userBean);
		summary.setSource(DataSource.GSTN.toString());
	}

	private boolean isEmptyRequest(Object obj) {

		Method[] methods = obj.getClass().getMethods();

		for (Method method : methods) {
			if (method.getName().startsWith("get")
					&& method.getReturnType().getSimpleName().equalsIgnoreCase(List.class.getSimpleName())) {
				List objects = null;
				try {
					objects = (List) method.invoke(obj);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (objects != null && !objects.isEmpty()) {

					return true;
				}
			}
		}
		return false;
	}

	@Override
	@AccessTimeout(value = 15, unit = TimeUnit.MINUTES)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveGstr1DataToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		try {

			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

			GSTR1 gstr1 = finderService.getMultipleGstinTransaction(gstinTransactionDTO);

			if (gstr1 != null && this.isEmptyRequest(gstr1)) {
				YearMonth yearmonth = YearMonth.parse(gstinTransactionDTO.getMonthYear(),
						DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				FilePrefernce fileprefernce = finderService
						.findpreference(gstinTransactionDTO.getTaxpayerGstin().getGstin(), fyear);

				if (Objects.isNull(fileprefernce.getCurrentGt())
						|| fileprefernce.getCurrentGt().compareTo(BigDecimal.ZERO) < 0
								&& Objects.isNull(fileprefernce.getPreviousGt())
								&& fileprefernce.getPreviousGt().compareTo(BigDecimal.ZERO) < 0) {
					throw new AppException(ExceptionCode.INVALID_TURNOVER);
				}

				String returnPeriod = Utility.getValidReturnPeriod(gstinTransactionDTO.getMonthYear(),
						fileprefernce.getPreference());
				gstr1.setCurgrossTurnover(fileprefernce.getCurrentGt().setScale(2, RoundingMode.FLOOR));
				gstr1.setGrossTurnover(fileprefernce.getPreviousGt().setScale(2, RoundingMode.FLOOR));

				gstr1.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				gstr1.setMonthYear(returnPeriod);

				String transitId = Utility.randomString(12);

				SaveGstr1Req saveGstr1Req = new SaveGstr1Req();
				saveGstr1Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				saveGstr1Req.setAction(Returns.RETSAVE.toString());
				saveGstr1Req.setMonthYear(returnPeriod);

				String data = JsonMapper.objectMapper.writeValueAsString(gstr1);
				JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

				// Done this because of UIN number not matching for one of the client
				// naval@teamcomputers.com
				String fileName = "gstnjson/gstr1.json";

				InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
				String jsonTxt = IOUtils.toString(is);

				ObjectMapper mapper = new ObjectMapper();
				JsonNode schemaNode = mapper.readTree(jsonTxt);
				JsonNode data1 = mapper.readTree(data);

				ProcessingReport report = VALIDATOR.validateUnchecked(schemaNode, data1);

				final boolean success = report.isSuccess();

				final JsonNode reportAsJson = ((AsJson) report).asJson();

				SaveGstr1Resp saveGstr1Resp = null;
				log.info("rpayload json  {}", data);
				log.info("json validation result {}  report {}", success, reportAsJson);
				if (success) {
					saveGstr1Req.setData(data);
					saveGstr1Resp = gstnService.saveGstr1Data(saveGstr1Req);

					log.debug("save gstr1response {}" + saveGstr1Resp.toString());

					gstinTransactionDTO.setTransitId(transitId);

					if (saveGstr1Resp != null && !StringUtils.isEmpty(saveGstr1Resp.getReferenceId())) {
						ReturnStatus returnStatus = new ReturnStatus();
						this.updateFlag(gstr1, gstinTransactionDTO, transitId, returnStatus);

						returnStatus.setCreatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						returnStatus.setCreationIpAddress(userBean.getIpAddress());
						returnStatus.setCreationTime(Timestamp.from(Instant.now()));
						returnStatus.setTaxpayerGstin(
								finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
						returnStatus.setReturnType(ReturnType.GSTR1.toString());
						returnStatus.setTransaction(gstinTransactionDTO.getTransactionType());
						returnStatus.setTransitId(transitId);
						returnStatus.setMonthYear(gstinTransactionDTO.getMonthYear());
						returnStatus.setEvent("SAVE");
						returnStatus.setReferenceId(saveGstr1Resp.getReferenceId());
						returnStatus.setTransactionId(saveGstr1Resp.getTransactionId());
						returnStatus.setStatus("PENDING");
						returnStatus.setGsp(saveGstr1Resp.getGsp());
						crudService.create(returnStatus);
					} else {
						gstnResponseProcessor.processGstnResponse(saveGstr1Resp,
								gstinTransactionDTO.getTaxpayerGstin().getGstin());
					}
				} else {

					try {
						new LegacyEmail().sendToEMGSupport(
								"User : " + userBean.getUserDto().getUsername() + "\n Request payload :" + data
										+ "\n Error Report " + reportAsJson,
								"INVALID JSON  " + gstinTransactionDTO.getTaxpayerGstin().getGstin());
					} catch (Exception e) {
						log.error("error while sending email ", e);
					}
					throw new AppException(ExceptionCode._INVALID_JSON);
				}

			} else
				throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.NO_DATA_TO_BE_SYNC);
		} catch (AppException e) {
			log.error("app exception ", e);
			throw e;
		} catch (IOException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			log.error("io exeption", e);
			throw new AppException();
		}
	}

	@SuppressWarnings({ "unchecked" })
	private void updateFlag(Object returnObj, GstinTransactionDTO gstinTransactionDTO, String transitId,
			ReturnStatus returnStatus)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, AppException {
		String tempTranType = gstinTransactionDTO.getTransactionType();
		int totalCount = 0;
		Method[] methods = returnObj.getClass().getMethods();

		for (Method method : methods) {
			if (method.getName().startsWith("get")
					&& method.getReturnType().getSimpleName().equalsIgnoreCase(List.class.getSimpleName())) {
				List objects = (List) method.invoke(returnObj);

				if (!Objects.isNull(objects) && !objects.isEmpty()) {
					GstnInvsCountMetadata gstnInvsCountMetadata = new GstnInvsCountMetadata();
					gstnInvsCountMetadata.setReturnStatus(returnStatus);
					String transType = StringUtils.upperCase(StringUtils.substringAfter(method.getName(), "get"));
					gstnInvsCountMetadata.setTransactionType(transType);
					gstinTransactionDTO.setTransactionType(transType);
					totalCount += this.updateFlagByQuery(returnObj, gstinTransactionDTO, transitId);
					;

					gstnInvsCountMetadata.setCount(objects.size());
					returnStatus.getGstnInvsCountMetadatas().add(gstnInvsCountMetadata);
				}

			}
		}
		gstinTransactionDTO.setCountTotalReturnStatus(totalCount);
		gstinTransactionDTO.setTransactionType(tempTranType);

	}

	@SuppressWarnings({ "unchecked" })
	private int updateFlagByQuery(Object returnObj, GstinTransactionDTO gstinTransactionDTO, String transitId)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, AppException {
		TaxpayerGstin gstin = (TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
				TaxpayerGstin.class);

		Class type = finderService.getEntityTypeByTransaction(gstinTransactionDTO.getTransactionType());
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate cu = cb.createCriteriaUpdate(type);
		Root root = cu.from(type);
		cu.set("isTransit", true);
		cu.set("transitId", transitId);

		try {
			cu.where(cb.and(this.getCommonPredicate(gstinTransactionDTO, cb, root).toArray(new Predicate[] {})));
		} catch (Exception e) {
			log.error("error in update flag by query ", e);
			throw new AppException();
		}
		int updatedCount = em.createQuery(cu).executeUpdate();
		gstinTransactionDTO.setCountTotalReturnStatus(updatedCount);
		log.debug("in transit marked invoice  count {}", updatedCount);
		return updatedCount;
	}

	private List getTransactionList(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		TaxpayerGstin gstin = (TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
				TaxpayerGstin.class);
		List<String> monthYears = finderService.getMonthYearsAsPerPreference(gstinTransactionDTO.getMonthYear(),
				gstinTransactionDTO.getTaxpayerGstin().getGstin());
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("taxPayerGstin", gstin);
		parameters.put("returnType", gstinTransactionDTO.getReturnType());
		parameters.put("monthYear", monthYears);
		Query q = em.createQuery(this.getParentTableQuery(gstinTransactionDTO));
		for (Map.Entry<String, Object> ent : parameters.entrySet()) {
			q.setParameter(ent.getKey(), ent.getValue());
		}
		List result = q.getResultList();
		return result;
	}

	private String getParentTableQuery(GstinTransactionDTO gstinTransactionDTO) {
		TransactionType type = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		String query = "";
		String query1 = "";

		switch (type) {
		case B2B:
			query = "B2bTransaction.getByGstinMonthyearReturntype";
			query1 = "select b from B2BTransactionEntity b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear in :monthYear";
			break;
		case CDN:
			query = "CdnTransaction.getByGstinMonthyearReturntype";
			query1 = "select b from CDNTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear in :monthYear";
			break;
		case B2CL:
			query = "B2CLTransaction.getByGstinMonthyearReturntype";
			query1 = "select b from B2CLTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear in :monthYear";
			break;
		case EXP:
			query = "CdnTransaction.getByGstinMonthyearReturntype";
			query1 = "select b from EXPTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear in :monthYear";
			break;
		case CDNUR:
			query1 = "select b from CDNURTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear in :monthYear";
			break;

		}

		return query1;
	}

	private List<Predicate> getCommonPredicate(GstinTransactionDTO gstinTransactionDTO, CriteriaBuilder cb, Root b)
			throws AppException {
		TaxpayerGstin gstin = (TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
				TaxpayerGstin.class);

		List<String> monthYears = finderService.getMonthYearsAsPerPreference(gstinTransactionDTO.getMonthYear(),
				gstinTransactionDTO.getTaxpayerGstin().getGstin());
		Predicate pGstin = null;
		Predicate pRType = null;
		Predicate pMonthYear = null;
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(b.get("isTransit"), false));
		predicates.add(cb.equal(b.get("isError"), false));

		String type = gstinTransactionDTO.getTransactionType();
		if (TransactionType.B2B.toString().equalsIgnoreCase(type)|| TransactionType.B2BA.toString().equalsIgnoreCase(type)) {
			/*
			 * pGstin = cb.equal(b.get("b2bTransaction").get("taxpayerGstin"), gstin);
			 * pRType = cb.equal(b.get("b2bTransaction").get("returnType"),
			 * gstinTransactionDTO.getReturnType()); pMonthYear =
			 * cb.in(b.get("b2bTransaction").get("monthYear").in(monthYears));
			 */
			predicates.add(b.get("b2bTransaction").in(this.getTransactionList(gstinTransactionDTO)));
			predicates.add(cb.or(cb.equal(b.get("dataSource"), DataSource.EMGST), cb.equal(b.get("flags"), "rejected"),
					cb.equal(b.get("flags"), "pending")));

		} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)) {
			/*
			 * pGstin = cb.equal(b.get("cdnTransaction").get("gstin"), gstin); pRType =
			 * cb.equal(b.get("cdnTransaction").get("returnType"),
			 * gstinTransactionDTO.getReturnType()); pMonthYear =
			 * cb.in(b.get("cdnTransaction").get("monthYear").in(monthYears));
			 */
			predicates.add(b.get("cdnTransaction").in(this.getTransactionList(gstinTransactionDTO)));

			predicates.add(cb.or(cb.equal(b.get("dataSource"), DataSource.EMGST), cb.equal(b.get("flags"), "rejected"),
					cb.equal(b.get("flags"), "pending")));

		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)) {
			/*
			 * pGstin = cb.equal(b.get("cdnUrTransaction").get("gstin"), gstin); pRType =
			 * cb.equal(b.get("cdnUrTransaction").get("returnType"),
			 * gstinTransactionDTO.getReturnType()); pMonthYear =
			 * cb.in(b.get("cdnUrTransaction").get("monthYear").in(monthYears));
			 */
			predicates.add(b.get("cdnUrTransaction").in(this.getTransactionList(gstinTransactionDTO)));

		} else if (TransactionType.B2CL.toString().equalsIgnoreCase(type)) {
			/*
			 * pGstin = cb.equal(b.get("b2clTransaction").get("gstin"), gstin); pRType =
			 * cb.equal(b.get("b2clTransaction").get("returnType"),
			 * gstinTransactionDTO.getReturnType()); pMonthYear =
			 * cb.in(b.get("b2clTransaction").get("monthYear").in(monthYears));
			 */
			predicates.add(b.get("b2clTransaction").in(this.getTransactionList(gstinTransactionDTO)));

		} else if (TransactionType.EXP.toString().equalsIgnoreCase(type)|| TransactionType.EXPA.toString().equalsIgnoreCase(type)) {
			/*
			 * pGstin = cb.equal(b.get("expTransaction").get("gstin"), gstin); pRType =
			 * cb.equal(b.get("expTransaction").get("returnType"),
			 * gstinTransactionDTO.getReturnType()); pMonthYear =
			 * cb.in(b.get("expTransaction").get("monthYear").in(monthYears));
			 */
			predicates.add(b.get("expTransaction").in(this.getTransactionList(gstinTransactionDTO)));

		} else if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("taxpayerGstin"), gstin);
			pRType = cb.equal(b.get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = b.get("monthYear").in(monthYears);

		} else {
			pGstin = cb.equal(!Objects.isNull(b.get("gstin")) ? b.get("gstin") : b.get("taxpayerGstin"), gstin);
			pRType = cb.equal(b.get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = b.get("monthYear").in(monthYears);

		}

		if (!Objects.isNull(pGstin)) {
			predicates.add(pGstin);
			predicates.add(pRType);
			predicates.add(pMonthYear);
		}
		return predicates;
	}

	public void checkIPReturnStaus(String gstin) {

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public GetCheckStatusResp checkReturnStatus(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		String response = "";
		GetCheckStatusResp checkStatusResp = null;
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<ReturnStatus> returnStatus = finderService.getReturnStatus(gstinTransactionDTO);

			List<GstinTransaction> gstinTransactions = null;// finderService.getMultipleGstinTransaction(gstinTransactionDTO);
			GstinTransaction gstinTransaction = null;
			if (!Objects.isNull(gstinTransactions) && !gstinTransactions.isEmpty()) {
				gstinTransaction = gstinTransactions.get(0);
			}

			ReturnStatusDTO returnStatusDTO = new ReturnStatusDTO();
			List<ReturnStatusDTO> returnStatusDTOs = new ArrayList<>();
			try {
				if (!Objects.isNull(returnStatus)) {

					for (ReturnStatus returnStatus2 : returnStatus) {

						GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
						getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
						getReturnStatusReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
						getReturnStatusReq.setMonthYear(gstinTransactionDTO.getMonthYear());
						getReturnStatusReq.setRef_id(returnStatus2.getReferenceId());

						// getReturnStatusReq.setTransId(returnStatus2.getTransitId());

						// GetGstr1ReturnStatusResp getGstr1ReturnStatusResp = gstnService
						// .getGstr1ReturnStatus(getReturnStatusReq);
						String returnPeriod = Utility.getValidReturnPeriod(gstinTransactionDTO.getMonthYear(),
								gstinTransactionDTO.getTaxpayerGstin().getReturnPreference());

						getReturnStatusReq.setMonthYear(returnPeriod);

						String checkStatusResponse = gstnService.getGst1ReturnStatus(getReturnStatusReq);
						if (!StringUtils.isEmpty(checkStatusResponse))
							checkStatusResp = JsonMapper.objectMapper.readValue(checkStatusResponse,
									GetCheckStatusResp.class);

						if (checkStatusResp != null) {
							this.updateErrorReports(gstinTransactionDTO, checkStatusResp, returnStatus2.getTransitId());

							returnStatus2.setFlag(checkStatusResp.getStatus());
							// if(!checkStatusResp.getStatus().equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_IN_PROCESS))
							returnStatus2.setStatus("PROCESSED");
							returnStatus2.setResponse(checkStatusResponse);

							returnStatus2.setUpdatedBy(
									userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
							returnStatus2.setUpdationIpAddress(userBean.getIpAddress());
							returnStatus2.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
							returnStatus2.setTotalSync(gstinTransactionDTO.getCountPStatus());
							if (!"IP".equalsIgnoreCase(checkStatusResp.getStatus()))
								returnStatus2.setTotalNotSync(
										(gstinTransactionDTO.getCountERStatus() + gstinTransactionDTO.getCountPEStatus()
												+ gstinTransactionDTO.getCountIPStatus()));
							returnStatus2.setTotalCount(gstinTransactionDTO.getCountTotalReturnStatus());
							crudService.update(returnStatus2);

						} else {
							throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.ERR_MSG);
						}
					}

				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				log.error("json excpetion", e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
			return checkStatusResp;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public GetCheckStatusResp checkReturnStatusGstr3b(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, IOException {

		GetCheckStatusResp checkStatusResp = null;
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<ReturnStatus> returnStatus = finderService.getReturnStatus(gstinTransactionDTO);

			try {
				if (!Objects.isNull(returnStatus)) {

					for (ReturnStatus returnStatus2 : returnStatus) {

						GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
						getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
						getReturnStatusReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
						getReturnStatusReq.setMonthYear(gstinTransactionDTO.getMonthYear());
						getReturnStatusReq.setRef_id(returnStatus2.getReferenceId());

						String checkStatusResponse = gstnService.getGstr3bReturnStatus(getReturnStatusReq);
						if (!StringUtils.isEmpty(checkStatusResponse))
							checkStatusResp = JsonMapper.objectMapper.readValue(checkStatusResponse,
									GetCheckStatusResp.class);

						if (checkStatusResp != null) {

							returnStatus2.setFlag(checkStatusResp.getStatus());
							// if(!checkStatusResp.getStatus().equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_IN_PROCESS))
							returnStatus2.setResponse(checkStatusResponse);

							if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(checkStatusResp.getStatus())) {
								returnStatus2.setStatus("PROCESSED");
							}
							returnStatus2.setUpdatedBy(
									userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
							returnStatus2.setUpdationIpAddress(userBean.getIpAddress());
							returnStatus2.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
							// returnStatus2.setStatus("PROCESSED");

							crudService.update(returnStatus2);

						} else {
							throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.ERR_MSG);
						}
					}

				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				log.error("json excpetion", e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
			return checkStatusResp;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private Map<String, Object> processGstnReturnStatusResponse(GetCheckStatusResp response) throws AppException {
		// GetCheckStatusResp returnStatus = null;
		String respMsg = "";
		String status = "PENDING";
		Map<String, Object> resp = new HashMap<>();

		if (response != null) {
			if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(response.getStatus())) {
				respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG;
				resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR.equalsIgnoreCase(response.getStatus())) {
				respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG_AFTER_SOME_TIME;
				resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			} else if (GSTNRespStatus.CHECKSTATUS_ERROR.equalsIgnoreCase(response.getStatus())) {
				if("R3B".equalsIgnoreCase(response.getFormType()) &&
						("RT-3BSS101".equalsIgnoreCase(response.getErrorReport().getError_cd()) || 
								"RT-3BSS103".equalsIgnoreCase(response.getErrorReport().getError_cd()))) {
					respMsg=response.getErrorReport().getError_msg();
				}else
				if (response.getError() != null)
					respMsg = response.getError().getMessage();
				else
					respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
			} else if (GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR.equalsIgnoreCase(response.getStatus())) {
				respMsg = GSTNRespStatus.CHECKSTATUS_PE_MSG;
			} else if (GSTNRespStatus.CHECKSTATUS_IN_PROCESS.equalsIgnoreCase(response.getStatus())) {
				respMsg = GSTNRespStatus.CHECKSTATUS_IP_MSG;
			}
			if(StringUtils.isEmpty(respMsg))
				respMsg=GSTNRespStatus.CHECKSTATUS_ER_MSG;
			resp.put(ApplicationMetadata.RESP_MSG_KEY, respMsg);
		} else
			throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.ERR_MSG);
		return resp;
	}

	private String checkActionStatus(GstinAction action, GstinTransactionDTO gstinTransactionDTO)
			throws AppException, IOException {

		GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
		getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
		getReturnStatusReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
		getReturnStatusReq.setMonthYear(gstinTransactionDTO.getMonthYear());
		getReturnStatusReq.setRef_id(action.getReferenceId());

		String checkStatusResponse = gstnService.getGst1ReturnStatus(getReturnStatusReq);
		action.setCheckStatusResponse(checkStatusResponse);

		// log.debug("return status for action {} gstin {} response
		// {}",action.getStatus(),
		// gstinTransactionDTO.getTaxpayerGstin().getGstin(),checkStatusResponse);

		if (!StringUtils.isEmpty(checkStatusResponse)) {
			// crudService.update(action);
			// GetCheckStatusResp
			// checkStatusResp=JsonMapper.objectMapper.readValue(checkStatusResponse,GetCheckStatusResp.class
			// );

			// gstnResponseProcessor.processGstnResponse(checkStatusResp,
			// gstinTransactionDTO.getTaxpayerGstin().getGstin());

		}

		return null;
	}

	public void checkACtionStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException {

		GstinAction gstinAction = em.getReference(GstinAction.class, 9);
		this.checkActionStatus(gstinAction, gstinTransactionDTO);

	}

	@Override
	public boolean syncGstr1ATransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();
			String gstin = gstinTransactionDTO.getTaxpayerGstin().getGstin();
			String gstReturn = gstinTransactionDTO.getReturnType();

			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				transactionTypes.add(gstinTransactionDTO.getTransactionType());
			} else {
				/*
				 * transactionTypes.add(TransactionType.B2B.toString());
				 * transactionTypes.add(TransactionType.CDN.toString());
				 */
				Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getReturnTransactionsByGstin(gstin,
						gstReturn);
				for (ReturnTransactionDTO returnTransactionDTO : returnTransactionDTOs) {
					transactionTypes.add(returnTransactionDTO.getCode());
				}

			}

			for (String type : transactionTypes) {

				GetGstr1AReq getGstr1AReq = new GetGstr1AReq();
				getGstr1AReq.setAction(type);
				getGstr1AReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr1AReq.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr1AReq.setStateCode(gstinTransactionDTO.getTaxpayerGstin().getStateBean().getScode());

				GetGstr1AResp getGstr1AResp = gstnService.getGstr1AData(getGstr1AReq);

				if (!Objects.isNull(getGstr1AResp)) {
					if (getGstr1AResp.getStatusCd().equalsIgnoreCase("1")) {
						Method[] methods = GetGstr1Resp.class.getMethods();
						for (Method method : methods) {
							if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class") == -1) {
								String methodName = method.getName();
								if (methodName.toLowerCase().contains(type.toLowerCase())) {
									try {
										Object obj = method.invoke(getGstr1AResp);

										GstnTransactionDTO gstnTransactionDTO = new GstnTransactionDTO();
										gstnTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
										gstnTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
										gstnTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
										gstnTransactionDTO.setTransactionType(type);
										gstnTransactionDTO.setActionRequired((byte) 1);

										GstnTransaction gstnTransaction = finderService
												.getGstnTransaction(gstnTransactionDTO);
										if (Objects.isNull(gstnTransaction)) {
											gstnTransaction = new GstnTransaction();
											gstnTransaction.setTaxpayerGstin(finderService.findTaxpayerGstin(
													gstinTransactionDTO.getTaxpayerGstin().getGstin()));
											gstnTransaction.setMonthYear(gstinTransactionDTO.getMonthYear());
											gstnTransaction.setReturnType(gstinTransactionDTO.getReturnType());
											gstnTransaction.setTransactionType(type);
											gstnTransaction.setActionRequired((byte) 1);
										}

										String transactionObject = JsonMapper.objectMapper.writeValueAsString(obj);
										gstnTransaction.setTransactionObject(transactionObject);

										if (Objects.isNull(gstnTransaction.getId())) {
											gstnTransaction.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setCreationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

											crudService.create(gstnTransaction);
										} else {
											gstnTransaction.setUpdatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setUpdationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
											crudService.update(gstnTransaction);
										}

									} catch (IllegalAccessException | IllegalArgumentException
											| InvocationTargetException | JsonProcessingException e) {
										e.printStackTrace();
									}

								}
							}
						}
					}
				}
			}

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public boolean syncGstr1ASummaryData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			String type = "SUMMARY";
			Gstr1ASummaryReq gstr1ASummaryReq = new Gstr1ASummaryReq();
			gstr1ASummaryReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			gstr1ASummaryReq.setReturnPeriod(gstinTransactionDTO.getMonthYear());

			Gstr1ASummaryResp gstr1ASummaryResp = gstnService.getGstr1ASummary(gstr1ASummaryReq);
			try {
				GstnTransactionDTO gstnTransactionDTO = new GstnTransactionDTO();
				gstnTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
				gstnTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
				gstnTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
				gstnTransactionDTO.setTransactionType(type);
				gstnTransactionDTO.setActionRequired((byte) 0);

				GstnTransaction gstnTransaction = finderService.getGstnTransaction(gstnTransactionDTO);
				if (Objects.isNull(gstnTransaction)) {
					gstnTransaction = new GstnTransaction();
					gstnTransaction.setTaxpayerGstin(
							finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
					gstnTransaction.setMonthYear(gstinTransactionDTO.getMonthYear());
					gstnTransaction.setReturnType(gstinTransactionDTO.getReturnType());
					gstnTransaction.setTransactionType(type);
					gstnTransaction.setActionRequired((byte) 0);
				}

				String transactionObject = JsonMapper.objectMapper.writeValueAsString(gstr1ASummaryResp);
				gstnTransaction.setTransactionObject(transactionObject);

				if (Objects.isNull(gstnTransaction.getId())) {
					gstnTransaction.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					gstnTransaction.setCreationIpAddress(userBean.getIpAddress());
					gstnTransaction.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.create(gstnTransaction);
				} else {
					gstnTransaction.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					gstnTransaction.setUpdationIpAddress(userBean.getIpAddress());
					gstnTransaction.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(gstnTransaction);
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public void saveGstr1ADataToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException {

	}

	@Override
	public String syncGstr2TransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		String tempResponse = "";
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();
			String gstin = gstinTransactionDTO.getTaxpayerGstin().getGstin();
			String gstReturn = gstinTransactionDTO.getReturnType();

			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				transactionTypes.add(gstinTransactionDTO.getTransactionType());
			} else {
				/*
				 * transactionTypes.add(TransactionType.B2B.toString());
				 * transactionTypes.add(TransactionType.CDN.toString());
				 * transactionTypes.add(TransactionType.IMPG.toString());
				 * transactionTypes.add(TransactionType.IMPS.toString());
				 * transactionTypes.add(TransactionType.NIL.toString());
				 * transactionTypes.add(TransactionType.ISD.toString());
				 * transactionTypes.add(TransactionType.TDS.toString());
				 * transactionTypes.add(TransactionType.TCS.toString());
				 */
				Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getReturnTransactionsByGstin(gstin,
						gstReturn);
				for (ReturnTransactionDTO returnTransactionDTO : returnTransactionDTOs) {
					transactionTypes.add(returnTransactionDTO.getCode());
				}
			}

			for (String type : transactionTypes) {

				GetGstr2Req getGstr2Req = new GetGstr2Req();
				getGstr2Req.setAction(type);
				getGstr2Req.setActionReq("N");
				getGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr2Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr2Req.setStateCode(gstinTransactionDTO.getTaxpayerGstin().getStateBean().getScode());
				getGstr2Req.setReturnType(ReturnType.valueOf(gstReturn.toUpperCase()));

				// GetGstr2Resp getGstr2Resp =
				// gstnService.getGstr2Data(getGstr2Req);
				GetGstr1Resp getGstr2Resp = new GetGstr1Resp();
				tempResponse = gstnService.getGstr2Data(getGstr2Req);

				if (!Objects.isNull(getGstr2Resp)) {
					if (getGstr2Resp.getStatusCd().equalsIgnoreCase("1")) {
						Method[] methods = GetGstr1Resp.class.getMethods();
						for (Method method : methods) {
							if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class") == -1) {
								String methodName = method.getName();
								if (methodName.toLowerCase().contains(type.toLowerCase())) {
									try {
										Object obj = method.invoke(getGstr2Resp);

										GstnTransactionDTO gstnTransactionDTO = new GstnTransactionDTO();
										gstnTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
										gstnTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
										gstnTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
										gstnTransactionDTO.setTransactionType(type);
										gstnTransactionDTO.setActionRequired((byte) 0);

										GstnTransaction gstnTransaction = finderService
												.getGstnTransaction(gstnTransactionDTO);
										if (Objects.isNull(gstnTransaction)) {
											gstnTransaction = new GstnTransaction();
											gstnTransaction.setTaxpayerGstin(finderService.findTaxpayerGstin(
													gstinTransactionDTO.getTaxpayerGstin().getGstin()));
											gstnTransaction.setMonthYear(gstinTransactionDTO.getMonthYear());
											gstnTransaction.setReturnType(gstinTransactionDTO.getReturnType());
											gstnTransaction.setTransactionType(type);
											gstnTransaction.setActionRequired((byte) 0);
										}

										String transactionObject = JsonMapper.objectMapper.writeValueAsString(obj);
										gstnTransaction.setTransactionObject(transactionObject);

										if (Objects.isNull(gstnTransaction.getId())) {
											gstnTransaction.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setCreationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

											crudService.create(gstnTransaction);
										} else {
											gstnTransaction.setUpdatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setUpdationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
											crudService.update(gstnTransaction);
										}

									} catch (IllegalAccessException | IllegalArgumentException
											| InvocationTargetException | JsonProcessingException e) {
										e.printStackTrace();
									}

								}
							}
						}
					}
				}
			}

			return tempResponse;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public String syncGstr2ActionRequiredTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		String tempResponse = "";
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();

			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				transactionTypes.add(gstinTransactionDTO.getTransactionType());
			} else {
				transactionTypes.add(TransactionType.B2B.toString());
				transactionTypes.add(TransactionType.CDN.toString());

			}

			for (String type : transactionTypes) {

				GetGstr2Req getGstr2Req = new GetGstr2Req();
				getGstr2Req.setAction(type);
				getGstr2Req.setActionReq("Y");
				getGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr2Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr2Req.setStateCode(gstinTransactionDTO.getTaxpayerGstin().getStateBean().getScode());
				getGstr2Req.setReturnType(ReturnType.GSTR2);

				// GetGstr2Resp getGstr2Resp =
				// gstnService.getGstr2Data(getGstr2Req);
				GetGstr2Resp getGstr2Resp = new GetGstr2Resp();
				tempResponse = gstnService.getGstr2Data(getGstr2Req);

				if (!Objects.isNull(getGstr2Resp)) {
					if (getGstr2Resp.getStatusCd().equalsIgnoreCase("1")) {
						Method[] methods = GetGstr1Resp.class.getMethods();
						for (Method method : methods) {
							if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class") == -1) {
								String methodName = method.getName();
								if (methodName.toLowerCase().contains(type.toLowerCase())) {
									try {
										Object obj = method.invoke(getGstr2Resp);

										GstnTransactionDTO gstnTransactionDTO = new GstnTransactionDTO();
										gstnTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
										gstnTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
										gstnTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
										gstnTransactionDTO.setTransactionType(type);
										gstnTransactionDTO.setActionRequired((byte) 1);

										GstnTransaction gstnTransaction = finderService
												.getGstnTransaction(gstnTransactionDTO);
										if (Objects.isNull(gstnTransaction)) {
											gstnTransaction = new GstnTransaction();
											gstnTransaction.setTaxpayerGstin(finderService.findTaxpayerGstin(
													gstinTransactionDTO.getTaxpayerGstin().getGstin()));
											gstnTransaction.setMonthYear(gstinTransactionDTO.getMonthYear());
											gstnTransaction.setReturnType(gstinTransactionDTO.getReturnType());
											gstnTransaction.setTransactionType(type);
											gstnTransaction.setActionRequired((byte) 1);
										}

										String transactionObject = JsonMapper.objectMapper.writeValueAsString(obj);
										gstnTransaction.setTransactionObject(transactionObject);

										if (Objects.isNull(gstnTransaction.getId())) {
											gstnTransaction.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setCreationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

											crudService.create(gstnTransaction);
										} else {
											gstnTransaction.setUpdatedBy(userBean.getUserDto().getFirstName() + "_"
													+ userBean.getUserDto().getLastName());
											gstnTransaction.setUpdationIpAddress(userBean.getIpAddress());
											gstnTransaction
													.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
											crudService.update(gstnTransaction);
										}

									} catch (IllegalAccessException | IllegalArgumentException
											| InvocationTargetException | JsonProcessingException e) {
										e.printStackTrace();
									}

								}
							}
						}
					}
				}
			}

			return tempResponse;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public GetGstr2Resp syncGstr2ATransactionData(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonParseException, JsonMappingException, IOException {
		String tempResponse = "";
		String tokenResp = "";
		// this.processUrls(gstinTransactionDTO);
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();

			if (!gstinTransactionDTO.getTransactionType().equalsIgnoreCase("%")) {
				if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
					transactionTypes.add(gstinTransactionDTO.getTransactionType());
				} else {
					transactionTypes.add(TransactionType.B2B.toString());
					transactionTypes.add(TransactionType.CDN.toString());
				}
			}

			else {
				transactionTypes.add(TransactionType.B2B.toString());
				transactionTypes.add(TransactionType.CDN.toString());
			}
			GetGstr2Resp getGstr2Resp = null;
			for (String type : transactionTypes) {

				// check if 2A is already processed
				// finderService.getDataStatus(gstinTransactionDTO);

				GetGstr2Req getGstr2Req = new GetGstr2Req();
				getGstr2Req.setAction(Returns.valueOf(type).toString());
				getGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr2Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr2Req.setReturnType(ReturnType.GSTR2A);

				if (ReturnType.GSTR1.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType())) {
					getGstr2Req.setReturnType(ReturnType.GSTR1);
					getGstr2Req.setActionReq("Y");
					getGstr2Req.setFromTime("01-11-2017");

				}
				// new String(Files.readAllBytes(Paths.get("")));
				tempResponse = gstnService.getGstr2Data(getGstr2Req);
				log.info("get data from gstn response {}", tempResponse);

				getGstr2Resp = JsonMapper.objectMapper.readValue(tempResponse, GetGstr2Resp.class);

				if (!Objects.isNull(getGstr2Resp))
					gstnResponseProcessor.processGstnResponse(getGstr2Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());

				if (getGstr2Resp.getToken() != null) {
					if (!StringUtils.isEmpty(getGstr2Resp.getToken())) {
						Map<String, Object> queryParam = new HashMap<>();
						queryParam.put("gstn",
								finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
						queryParam.put("returnType", gstinTransactionDTO.getReturnType());
						queryParam.put("monthYear", gstinTransactionDTO.getMonthYear());
						queryParam.put("trans", type);
						List<TokenResponse> listResponse = crudService.findWithNamedQuery("TokenResponse.findByGstin",
								queryParam);
						if (!listResponse.isEmpty()) {

							for (TokenResponse tokenResponse : listResponse) {
								tokenResponse.setToken(getGstr2Resp.getToken());
								tokenResponse.setUpdationTime(Timestamp.from(Instant.now()));
								tokenResponse.setStatus("PENDING");
								tokenResponse.setUrlcount(0);
								crudService.update(tokenResponse);
							}
						} else {
							TokenResponse tokenResponse = new TokenResponse();
							tokenResponse.setMonthYear(gstinTransactionDTO.getMonthYear());
							tokenResponse.setReturnType(gstinTransactionDTO.getReturnType());
							tokenResponse.setTaxpayerGstin(
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
							tokenResponse.setStatus("PENDING");
							tokenResponse.setValidity(getGstr2Resp.getEst());
							tokenResponse.setCreationTime(Timestamp.from(Instant.now()));
							tokenResponse.setTransaction(type);
							tokenResponse.setToken(getGstr2Resp.getToken());
							tokenResponse.setUpdationTime(Timestamp.from(Instant.now()));
							tokenResponse.setUrlcount(0);
							crudService.create(tokenResponse);
						}
						throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.GET_CALL_IN_PROGRESS);
					}
				}

			}
			return getGstr2Resp;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}
	

	@Override
	public GetGstr2Resp syncGstr1ActionRequiredTransactionDataNew(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonParseException, JsonMappingException, IOException {
		String tempResponse = "";
		String tokenResp = "";
		// this.processUrls(gstinTransactionDTO);
		// return new GetGstr2Resp();
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();

			if (!gstinTransactionDTO.getTransactionType().equalsIgnoreCase("%")) {
				if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
					transactionTypes.add(gstinTransactionDTO.getTransactionType());
				} else {
					transactionTypes.add(TransactionType.B2B.toString());
					transactionTypes.add(TransactionType.CDN.toString());
				}
			}

			else {
				transactionTypes.add(TransactionType.B2B.toString());
				transactionTypes.add(TransactionType.CDN.toString());
			}
			GetGstr2Resp getGstr2Resp = null;
			for (String type : transactionTypes) {

				// check if request already processed
				finderService.getDataStatus(gstinTransactionDTO);

				GetGstr2Req getGstr2Req = new GetGstr2Req();
				getGstr2Req.setAction(Returns.valueOf(type).toString());
				getGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr2Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr2Req.setReturnType(ReturnType.GSTR1);
				getGstr2Req.setActionReq("Y");
				getGstr2Req.setFromTime("01-07-2017");

				// new String(Files.readAllBytes(Paths.get("")));
				tempResponse = gstnService.getGstr2Data(getGstr2Req);
				log.info("get data from gstn response {}", tempResponse);

				getGstr2Resp = JsonMapper.objectMapper.readValue(tempResponse, GetGstr2Resp.class);

				if (!Objects.isNull(getGstr2Resp))
					gstnResponseProcessor.processGstnResponse(getGstr2Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());

				if (getGstr2Resp.getToken() != null) {
					if (!StringUtils.isEmpty(getGstr2Resp.getToken())) {
						Map<String, Object> queryParam = new HashMap<>();
						queryParam.put("gstn",
								finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
						queryParam.put("returnType", gstinTransactionDTO.getReturnType());
						queryParam.put("monthYear", gstinTransactionDTO.getMonthYear());
						queryParam.put("trans", type);
						List<TokenResponse> listResponse = crudService.findWithNamedQuery("TokenResponse.findByGstin",
								queryParam);
						if (!listResponse.isEmpty()) {

							for (TokenResponse tokenResponse : listResponse) {
								tokenResponse.setToken(getGstr2Resp.getToken());
								tokenResponse.setUpdationTime(Timestamp.from(Instant.now()));
								tokenResponse.setStatus("PENDING");
								tokenResponse.setUrlcount(0);
								crudService.update(tokenResponse);
							}
						} else {
							TokenResponse tokenResponse = new TokenResponse();
							tokenResponse.setMonthYear(gstinTransactionDTO.getMonthYear());
							tokenResponse.setReturnType(gstinTransactionDTO.getReturnType());
							tokenResponse.setTaxpayerGstin(
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
							tokenResponse.setStatus("PENDING");
							tokenResponse.setValidity(getGstr2Resp.getEst());
							tokenResponse.setCreationTime(Timestamp.from(Instant.now()));
							tokenResponse.setTransaction(type);
							tokenResponse.setToken(getGstr2Resp.getToken());
							tokenResponse.setUpdationTime(Timestamp.from(Instant.now()));
							tokenResponse.setUrlcount(0);
							crudService.create(tokenResponse);
						}
						throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.GET_CALL_IN_PROGRESS);
					}
				}

			}
			return getGstr2Resp;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void getGstr1DataFromGstn(GstinTransactionDTO gstinTransactionDto, String token) throws AppException {
		GetGstr1Req request = new GetGstr1Req();
		request.setAction("GET_GET_v0.3");
		request.setGstin(gstinTransactionDto.getTaxpayerGstin().getGstin());
		request.setMonthYear(gstinTransactionDto.getMonthYear());
		request.setReturnType(ReturnType.GSTR1);
		request.setToken(token);
		String response = gstnService.getGstr1DataNew(request);
		log.info("response of no of files api {} ", response);

	}

	public void callScheduleMehtod(GstinTransactionDTO gstinTransactionDTO) {
		this.processUrls(gstinTransactionDTO);
		log.info("urls successfully processed");
	}

	@Override
	public void processUrls(GstinTransactionDTO gstinTransactionDto) {
		log.info("entering in process urls schedular{}");
		// query to find whose creation time and updation time both 30 minutes

		Query q = em.createNativeQuery(
				"SELECT r.id,r.gstin,r.returnType,r.monthYear,r.transaction,r.token,r.creationTime,r.updationTime,r.urlcount,r.validity,r.status from token_response r where r.status='PENDING' and r.creationTime < DATE_SUB(NOW(),INTERVAL 30 MINUTE) and r.updationTime < DATE_SUB(NOW(),INTERVAL 30 MINUTE) and r.urlcount=0",
				TokenResponse.class);
		List<TokenResponse> listResponse = q.getResultList();

		try {
			if (!listResponse.isEmpty()) {
				log.info("getting results from tokenresponse {}" + listResponse.size());
				for (TokenResponse tokenResp : listResponse) {
					//
					// this.getGstr1DataFromGstn(gstinTransactionDto, tokenResp.getToken());
					//

					// data set into DTOs
					YearMonth yearmonth = YearMonth.parse(tokenResp.getMonthYear(),
							DateTimeFormatter.ofPattern("MMyyyy"));
					String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
					GstinTransactionDTO gstnDto = new GstinTransactionDTO();
					gstnDto.setTaxpayerGstin(
							taxpayerService.getTaxpayerGstin(tokenResp.getTaxpayerGstin().getGstin(), fyear));
					gstnDto.setReturnType(tokenResp.getReturnType());
					gstnDto.setMonthYear(tokenResp.getMonthYear());
					gstnDto.setTransactionType(tokenResp.getTransaction());
					// this.getUrlResponse(gstnDto);
					// creating request to get Urls
					GetGstr2Req getGstr2Req = new GetGstr2Req();
					getGstr2Req.setAction(Returns.FILEDET.toString());
					getGstr2Req.setToken(tokenResp.getToken());
					getGstr2Req.setGstin(gstnDto.getTaxpayerGstin().getGstin());
					getGstr2Req.setReturnType(ReturnType.valueOf(tokenResp.getReturnType()));
					getGstr2Req.setMonthYear(tokenResp.getMonthYear());

					String response = gstnService.getGstr1DataActionRequired(getGstr2Req);
					log.info("url responsse received {} " + response);
					GetGstr2Req getGstrReq = new GetGstr2Req();
					Get2AFileResp urlResp = JsonMapper.objectMapper.readValue(response, Get2AFileResp.class);
					InputStream endpointResp = null;

					getGstrReq.setAction(Returns.FILE_GET_DOWNLOAD.toString());
					getGstrReq.setMonthYear(tokenResp.getMonthYear());

					// deleting existing data

					/*
					 * Map<String, Object> queryParam = new HashMap<>();
					 * queryParam.put("referenceId", tokenResp.getId()); List<EncodedData>
					 * encodedDataList =
					 * crudService.findWithNamedQuery("EncodedData.findByReferenceId", queryParam);
					 * if (!encodedDataList.isEmpty()) {
					 * log.info("deleting existing encoded data {}" + encodedDataList.size()); for
					 * (EncodedData encode : encodedDataList) {
					 * crudService.delete(EncodedData.class, encode.getId()); } }
					 */
					/*
					 * try { if (urlResp.getUrls() != null) { if (!urlResp.getUrls().isEmpty()) {
					 * tokenResp.setUrlcount(urlResp.getUrls().size());
					 * crudService.update(tokenResp); for (FileUrlResp url : urlResp.getUrls()) {
					 * try { getGstrReq.setGstin(gstnDto.getTaxpayerGstin().getGstin());
					 * getGstrReq.setEndpoint(URLDecoder.decode(url.getUl(), "UTF-8")); endpointResp
					 * = gstnService.getGstr2AUrlData(getGstrReq); if (endpointResp != null) {
					 * String encodedString = Utility.unzipData(endpointResp, urlResp.getEk());
					 * EncodedData encodedData = new EncodedData();
					 * encodedData.setEncodedData(encodedString); TokenResponse tokenResponse = new
					 * TokenResponse(); tokenResponse.setId(tokenResp.getId());
					 * encodedData.setReferenceId(tokenResponse);
					 * encodedData.setInvoiceCount(Integer.parseInt(url.getIc()));
					 * encodedData.setStatus("PENDING"); crudService.create(encodedData); } else {
					 * log.error("exception occured inputstream is null {}"); } } catch (Exception
					 * e) { log.error("exception while inserting into encoded_data",e); } } } else
					 * log.info("No urls found to process {}"); } else
					 * log.info("urls not recevied from gstn {}"); } catch (Exception e) {
					 * log.error("exception occured after getting url response{} " + e); }
					 */
				}
			} else
				log.info("No Request found to be processed");
		} catch (Exception e) {
			log.error("exception occured {}" + e);
		}
	}

	public GetGstr2Resp syncGst1ATransactionData(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonParseException, JsonMappingException, IOException {

		String tempResponse = "";
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();

			if (!gstinTransactionDTO.getTransactionType().equalsIgnoreCase("%")) {
				if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
					transactionTypes.add(gstinTransactionDTO.getTransactionType());
				} else {
					transactionTypes.add(TransactionType.B2B.toString());
					transactionTypes.add(TransactionType.CDN.toString());
				}
			}

			else {
				transactionTypes.add(TransactionType.B2B.toString());
				transactionTypes.add(TransactionType.CDN.toString());
			}
			GetGstr2Resp getGstr2Resp = null;
			for (String type : transactionTypes) {

				GetGstr1Req getGstr1AReq = new GetGstr1Req();
				getGstr1AReq.setAction(Returns.valueOf(type).toString());
				getGstr1AReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr1AReq.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr1AReq.setReturnType(ReturnType.GSTR1A);

				tempResponse = gstnService.getGstr1AData(getGstr1AReq);

				getGstr2Resp = JsonMapper.objectMapper.readValue(tempResponse, GetGstr2Resp.class);

				if (getGstr2Resp.getToken() != null) {
					if (!StringUtils.isEmpty(getGstr2Resp.getToken())) {
						Map<String, Object> queryParam = new HashMap<>();
						queryParam.put("gstn",
								finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
						queryParam.put("returnType", gstinTransactionDTO.getReturnType());
						queryParam.put("monthYear", gstinTransactionDTO.getMonthYear());
						queryParam.put("trans", type);
						List<TokenResponse> listResponse = crudService.findWithNamedQuery("TokenResponse.findByGstin",
								queryParam);
						if (!listResponse.isEmpty()) {
							for (TokenResponse tokenResponse : listResponse) {
								tokenResponse.setToken(getGstr2Resp.getToken());
								tokenResponse.setUpdationTime(Timestamp.from(Instant.now()));
								tokenResponse.setStatus("PENDING");
								tokenResponse.setUrlcount(0);
								crudService.update(tokenResponse);
							}
						} else {
							TokenResponse tokenResponse = new TokenResponse();
							tokenResponse.setMonthYear(gstinTransactionDTO.getMonthYear());
							tokenResponse.setReturnType(gstinTransactionDTO.getReturnType());
							tokenResponse.setTaxpayerGstin(
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
							tokenResponse.setStatus("PENDING");
							tokenResponse.setValidity(getGstr2Resp.getEst());
							tokenResponse.setCreationTime(Timestamp.from(Instant.now()));
							tokenResponse.setTransaction(type);
							tokenResponse.setToken(getGstr2Resp.getToken());
							tokenResponse.setUpdationTime(Timestamp.from(Instant.now()));
							tokenResponse.setUrlcount(0);
							crudService.create(tokenResponse);
						}
						throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.GET_CALL_IN_PROGRESS);
					}
				}
				if (getGstr2Resp == null)
					gstnResponseProcessor.processGstnResponse(getGstr2Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());
			}
			return getGstr2Resp;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public boolean syncGstr2SummaryData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			String type = "SUMMARY";
			Gstr2SummaryReq gstr2SummaryReq = new Gstr2SummaryReq();
			gstr2SummaryReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			gstr2SummaryReq.setReturnPeriod(gstinTransactionDTO.getMonthYear());

			Gstr2SummaryResp gstr2SummaryResp = gstnService.getGstr2Summary(gstr2SummaryReq);
			try {
				GstnTransactionDTO gstnTransactionDTO = new GstnTransactionDTO();
				gstnTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
				gstnTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
				gstnTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
				gstnTransactionDTO.setTransactionType(type);
				gstnTransactionDTO.setActionRequired((byte) 0);

				GstnTransaction gstnTransaction = finderService.getGstnTransaction(gstnTransactionDTO);
				if (Objects.isNull(gstnTransaction)) {
					gstnTransaction = new GstnTransaction();
					gstnTransaction.setTaxpayerGstin(
							finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
					gstnTransaction.setMonthYear(gstinTransactionDTO.getMonthYear());
					gstnTransaction.setReturnType(gstinTransactionDTO.getReturnType());
					gstnTransaction.setTransactionType(type);
					gstnTransaction.setActionRequired((byte) 0);
				}

				String transactionObject = JsonMapper.objectMapper.writeValueAsString(gstr2SummaryResp);
				gstnTransaction.setTransactionObject(transactionObject);

				if (Objects.isNull(gstnTransaction.getId())) {
					gstnTransaction.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					gstnTransaction.setCreationIpAddress(userBean.getIpAddress());
					gstnTransaction.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.create(gstnTransaction);
				} else {
					gstnTransaction.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					gstnTransaction.setUpdationIpAddress(userBean.getIpAddress());
					gstnTransaction.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(gstnTransaction);
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public void saveGstr2DataToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		try {
			GSTR2 gstr2 = finderService.getMultipleGstinTransactionGstr2(gstinTransactionDTO);
			if (gstr2 != null && this.isEmptyRequest(gstr2)) {

				gstr2.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				gstr2.setMonthYear(gstinTransactionDTO.getMonthYear());

				String transitId = Utility.randomString(12);

				SaveGstr2Req saveGstr2Req = new SaveGstr2Req();
				saveGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				saveGstr2Req.setAction(Returns.RETSAVE.toString());
				saveGstr2Req.setMonthYear(gstr2.getMonthYear());

				String data = JsonMapper.objectMapper.writeValueAsString(gstr2);

				JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

				InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("gstnjson/gstr2.json");
				String jsonTxt = IOUtils.toString(is);

				ObjectMapper mapper = new ObjectMapper();
				JsonNode schemaNode = mapper.readTree(jsonTxt);
				JsonNode data1 = mapper.readTree(data);

				ProcessingReport report = VALIDATOR.validateUnchecked(schemaNode, data1);

				final boolean success = report.isSuccess();

				final JsonNode reportAsJson = ((AsJson) report).asJson();

				SaveGstr2Resp saveGstr2Resp = null;
				log.info("rpayload json  {}", data);
				log.info("json validation result {}  report {}", success, reportAsJson);

				if (success) {
					saveGstr2Req.setData(data);
					saveGstr2Resp = gstnService.saveGstr2Data(saveGstr2Req);

					log.debug("save gstr2response {}" + saveGstr2Resp.toString());
					ReturnStatus returnStatus = new ReturnStatus();

					gstinTransactionDTO.setTransitId(transitId);
					if (saveGstr2Resp != null && !StringUtils.isEmpty(saveGstr2Resp.getReferenceId())) {
						this.updateFlag(gstr2, gstinTransactionDTO, transitId, returnStatus);
						returnStatus.setCreatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						returnStatus.setCreationIpAddress(userBean.getIpAddress());
						returnStatus.setCreationTime(Timestamp.from(Instant.now()));
						returnStatus.setTaxpayerGstin(
								finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
						returnStatus.setReturnType(ReturnType.GSTR2.toString());
						returnStatus.setTransaction(gstinTransactionDTO.getTransactionType());
						returnStatus.setTransitId(transitId);
						returnStatus.setMonthYear(gstr2.getMonthYear());
						returnStatus.setEvent("SAVE");
						returnStatus.setReferenceId(saveGstr2Resp.getReferenceId());
						returnStatus.setTransactionId(saveGstr2Resp.getTransactionId());
						returnStatus.setStatus("PENDING");
						crudService.create(returnStatus);
					} else {
						gstnResponseProcessor.processGstnResponse(saveGstr2Resp,
								gstinTransactionDTO.getTaxpayerGstin().getGstin());
					}
				} else {
					try {
						new LegacyEmail().sendToEMGSupport(
								"Request payload :" + data + "\n Error Report " + reportAsJson,
								"INVALID JSON  " + gstinTransactionDTO.getTaxpayerGstin().getGstin());
					} catch (Exception e) {
						log.error("error while sending email ", e);
					}
					throw new AppException(ExceptionCode._INVALID_JSON);
				}

			} else
				throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.NO_DATA_TO_BE_SYNC);
		} catch (AppException e) {
			log.error("app exception ", e);
			throw e;
		} catch (IOException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			log.error("io exeption", e);
			if (e.getCause().getCause() instanceof AppException) {
				throw (AppException) e.getCause().getCause();
			}
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private String checkGstrFlowEligibility(GstinTransactionDTO gstinTransactionDTO, String keyToCheck)
			throws Exception {

		Properties fsmConfig = new Properties();
		try {
			fsmConfig.load(MethodHandles.lookup().lookupClass().getClassLoader().getResourceAsStream("fsm.properties"));
			log.info("fsm.properties loaded successfully");

			Date gstStartDate = SerializationUtils
					.deserialize(redisService.getBytes(ApplicationMetadata.GST_START_DATE));

			String flowMasterKey = "gstr_flow";
			if (!StringUtils.isEmpty(gstinTransactionDTO.getReturnType())
					&& gstinTransactionDTO.getReturnType().toLowerCase().contains("gstr1a"))
				flowMasterKey = "gstr_flow_gstr1a";

			String flow = fsmConfig.getProperty(flowMasterKey);

			String[] tempFlow = flow.split(",");

			List<String> flowList = Arrays.asList(tempFlow);

			int index = flowList.indexOf(keyToCheck.toUpperCase());

			String stateToCheck = "";
			boolean prevMonthYear = false;

			if (index == -1) {
				return null;
			}

			if (index != 0) {
				stateToCheck = flowList.get(index - 1);
			} else {
				prevMonthYear = true;
				stateToCheck = flowList.get(flowList.size() - 1);
			}

			if (!StringUtils.isEmpty(stateToCheck)) {

				String[] temp = stateToCheck.split("_");
				String rType = temp[0];
				String state = temp[1];

				String particular = gstinTransactionDTO.getMonthYear();

				if (prevMonthYear) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MMYYYY");
					int month = Integer.parseInt(particular.substring(0, 2));
					int year = Integer.parseInt(particular.substring(2, 6));

					Calendar newCal = Calendar.getInstance();
					newCal.set(Calendar.DATE, 1);
					newCal.set(Calendar.YEAR, year);
					newCal.set(Calendar.MONTH, month - 1);
					newCal.add(Calendar.MONTH, -1);

					Date tempDate = newCal.getTime();
					if (tempDate.before(gstStartDate)) // as this is the first
						return "TRUE";

					// check for previous month year
					particular = dateFormat.format(newCal.getTime());
				}

				// GstinActionDTO gstinActionDTO = new GstinActionDTO();
				// gstinActionDTO.setReturnType(rType);
				// gstinActionDTO.setStatus(state);
				// gstinActionDTO.setParticular(particular);
				//
				// GstinAction action = finderService.findGstinAction(gstinActionDTO,
				// gstinTransactionDTO.getTaxpayerGstin().getGstin());
				//
				// if (!Objects.isNull(action) && !StringUtils.isEmpty(action.getReferenceId()))
				// throw new AppException(ExceptionCode._ALREADY_SUBMITED);
				try {
					if (!finderService.isSubmit(gstinTransactionDTO))
						;
				} catch (AppException e) {

				}
				return "TRUE";

				// return "You have not " + state + " " + rType + " for the month year " +
				// particular;

			} else {
				return null;
			}

		} catch (Exception e) {
			log.error("Excpetion while loading fsm.properties {}", e);
			throw e;
		}

	}

	@Override
	public GstrFlowDTO submitGstr(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		if (!Objects.isNull(gstinTransactionDTO)) {

			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			Calendar cal = Calendar.getInstance();

			if (!Objects.isNull(taxpayerGstin)) {

				String keyToCheck = gstinTransactionDTO.getReturnType() + "_" + GstnAction.SUBMIT.toString();
				String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

				GstrFlowDTO flowDTO = new GstrFlowDTO();

				if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {
					GstinAction gstinAction = new GstinAction();
					gstinAction.setTaxpayerGstin(taxpayerGstin);
					gstinAction.setReturnType(gstinTransactionDTO.getReturnType());
					gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
					gstinAction.setStatus(GstnAction.SUBMIT.toString());
					gstinAction.setActionDate(cal.getTime());

					crudService.create(gstinAction);

					flowDTO.setTransactionId("123456789");
					flowDTO.setReferenceId("ABCDEFGH");
					flowDTO.setError(false);

				} else {

					flowDTO.setError(true);
					if (StringUtils.isEmpty(result)) {
						flowDTO.setErrorMsg("Unable to process your request");
					} else {
						flowDTO.setErrorMsg(result);
					}

				}

				return flowDTO;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public GstrFlowDTO submitGstrToGstn(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		if (!returnService.isAllSynced(gstinTransactionDTO.getMonthYear(),
				gstinTransactionDTO.getTaxpayerGstin().getGstin(),
				ReturnType.valueOf(gstinTransactionDTO.getReturnType().toUpperCase())))
			throw new AppException(ExceptionCode._NOTSYNCED);

		if (!Objects.isNull(gstinTransactionDTO)) {

			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			Calendar cal = Calendar.getInstance();

			if (!Objects.isNull(taxpayerGstin)) {

				String keyToCheck = gstinTransactionDTO.getReturnType() + "_" + GstnAction.SUBMIT.toString();
				String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

				GstrFlowDTO flowDTO = new GstrFlowDTO();

				SubmitGstr1Req submitGstr1Req = new SubmitGstr1Req();
				submitGstr1Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				submitGstr1Req.setRetPeriod(gstinTransactionDTO.getMonthYear());
				submitGstr1Req.setAction(Returns.RETSUBMIT.toString());

				if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {
					SubmitGstr1Resp submitGstr1Resp = gstnService.submitGstr1Data(submitGstr1Req);
					gstnResponseProcessor.processGstnResponse(submitGstr1Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());
					GstinAction gstinAction = new GstinAction();
					gstinAction.setTaxpayerGstin(taxpayerGstin);
					gstinAction.setReturnType(gstinTransactionDTO.getReturnType());
					gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
					gstinAction.setStatus(GstnAction.SUBMIT.toString());
					gstinAction.setActionDate(cal.getTime());

					if (StringUtils.isNotEmpty(submitGstr1Resp.getReferenceId()))
						;
					gstinAction.setReferenceId(submitGstr1Resp.getReferenceId());

					gstinAction = crudService.create(gstinAction);

					this.checkActionStatus(gstinAction, gstinTransactionDTO);

					taxpayerService.updateMonthYearForGstin(taxpayerGstin);

					flowDTO.setTransactionId(submitGstr1Resp.getTransactionId());
					flowDTO.setReferenceId(submitGstr1Resp.getReferenceId());
					flowDTO.setError(false);

				} else {

					flowDTO.setError(true);
					if (StringUtils.isEmpty(result)) {
						flowDTO.setErrorMsg("Unable to process your request");
					} else {
						flowDTO.setErrorMsg(result);
					}

				}
				return flowDTO;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public GstrFlowDTO generateGstr(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		if (!Objects.isNull(gstinTransactionDTO)) {

			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			Calendar cal = Calendar.getInstance();

			if (!Objects.isNull(taxpayerGstin)) {

				String keyToCheck = gstinTransactionDTO.getReturnType() + "_" + GstnAction.GENERATE.toString();
				String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

				GstrFlowDTO flowDTO = new GstrFlowDTO();
				GenGstr3Req genGstr3Req = new GenGstr3Req();
				genGstr3Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				genGstr3Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				genGstr3Req.setAction(Returns.GENERATE.toString());

				if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {

					String resp = gstnService.generateGstr3(genGstr3Req);
					GstinAction gstinAction = new GstinAction();
					gstinAction.setTaxpayerGstin(taxpayerGstin);
					gstinAction.setReturnType(gstinTransactionDTO.getReturnType());
					gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
					gstinAction.setStatus(GstnAction.GENERATE.toString());
					gstinAction.setActionDate(cal.getTime());

					crudService.create(gstinAction);

					flowDTO.setError(false);

				} else {

					flowDTO.setError(true);
					if (StringUtils.isEmpty(result)) {
						flowDTO.setErrorMsg("Unable to process your request");
					} else {
						flowDTO.setErrorMsg(result);
					}

				}

				return flowDTO;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public GstrFlowDTO fileGstr(GstinTransactionDTO gstinTransactionDTO, String b64SingSummary) throws Exception {

		if (!Objects.isNull(gstinTransactionDTO)) {

			String gstnSummary = redisService.getValue(gstinTransactionDTO.getTaxpayerGstin().getGstin()
					+ gstinTransactionDTO.getReturnType() + gstinTransactionDTO.getMonthYear());

			String b64Summary = Base64.getEncoder().encodeToString(gstnSummary.getBytes());

			if (!SignDataVerificationUtil.verifySign_nsdl(b64Summary, b64SingSummary))
				throw new AppException(ExceptionCode._INVALID_SIGN_DATA);
			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			Calendar cal = Calendar.getInstance();

			if (!Objects.isNull(taxpayerGstin)) {

				String keyToCheck = gstinTransactionDTO.getReturnType() + "_" + GstnAction.FILED.toString();
				String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

				GstrFlowDTO flowDTO = new GstrFlowDTO();

				if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {

					FileGstr1Req fileGstr1Req = new FileGstr1Req();
					fileGstr1Req.setAction(Returns.RETFILE.toString());
					fileGstr1Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
					fileGstr1Req.setRetPeriod(gstinTransactionDTO.getMonthYear());
					fileGstr1Req.setB64SignData(b64SingSummary);
					fileGstr1Req.setB64Summary(gstnSummary);
					fileGstr1Req.setSignType("DSC");
					fileGstr1Req.setSignId("");
					FileGstr1Resp fileGstr1Resp = gstnService.fileGstr1Return(fileGstr1Req);

					gstnResponseProcessor.processGstnResponse(fileGstr1Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());

					if (!StringUtils.isEmpty(fileGstr1Resp.getAcknowledgementNo())) {
						GstinAction gstinAction = new GstinAction();
						gstinAction.setTaxpayerGstin(taxpayerGstin);
						gstinAction.setReturnType(gstinTransactionDTO.getReturnType());
						gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
						gstinAction.setStatus(GstnAction.FILED.toString());
						gstinAction.setActionDate(cal.getTime());
						gstinAction.setReferenceId(fileGstr1Resp.getAcknowledgementNo());
						crudService.create(gstinAction);

						taxpayerGstin.setMonthYear(null);

						taxpayerGstin.setUpdatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
						taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
						crudService.update(taxpayerGstin);
						taxpayerService.updateMonthYearForGstin(taxpayerGstin);
						flowDTO.setTransactionId("123456789");
						flowDTO.setReferenceId(fileGstr1Resp.getAcknowledgementNo());
						flowDTO.setError(false);
					} else {
						flowDTO.setError(true);
						flowDTO.setErrorMsg((Objects.nonNull(fileGstr1Resp.getError())
								&& Objects.nonNull(fileGstr1Resp.getError().getMessage()))
										? fileGstr1Resp.getError().getMessage()
										: "Unable to process your request");

					}

				} else {

					flowDTO.setError(true);
					if (StringUtils.isEmpty(result)) {
						flowDTO.setErrorMsg("Unable to process your request");
					} else {
						flowDTO.setErrorMsg(result);
					}

				}

				return flowDTO;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String utilizeCredit(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		if (!Objects.isNull(gstinTransactionDTO)) {

			JSONObject output = new JSONObject();
			output.put("error", "true");

			String keyToCheck = "GSTR3_UTILIZECREDIT";
			String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

			if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {

				gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString());
				GstrSummaryDTO outSummaryDTO = new GstrSummaryDTO();// returnService.getTransactionSummary(gstinTransactionDTO);

				gstinTransactionDTO.setReturnType(ReturnType.GSTR2.toString());
				GstrSummaryDTO inSummaryDTO = new GstrSummaryDTO();// returnService.getTransactionSummary(gstinTransactionDTO);

				double liabilityIgst = outSummaryDTO.getTotalIgst();
				double liabilityCgst = outSummaryDTO.getTotalCgst();
				double liabilitySgst = outSummaryDTO.getTotalSgst();
				double liabilityCess = outSummaryDTO.getTotalCess();

				double creditIgst = inSummaryDTO.getTotalIgst();
				double creditCgst = inSummaryDTO.getTotalCgst();
				double creditSgst = inSummaryDTO.getTotalSgst();
				double creditCess = inSummaryDTO.getTotalCess();

				double igst_igst = 0.0;
				double igst_cgst = 0.0;
				double igst_sgst = 0.0;

				double cgst_cgst = 0.0;
				double cgst_igst = 0.0;

				double sgst_sgst = 0.0;
				double sgst_igst = 0.0;

				double cess_cess = 0.0;

				// adjusting IGST liability
				double temp = liabilityIgst - creditIgst >= 0.0 ? creditIgst : liabilityIgst;
				liabilityIgst = liabilityIgst - temp;
				creditIgst = creditIgst - temp;
				igst_igst += temp;

				temp = liabilityIgst - creditCgst >= 0.0 ? creditCgst : liabilityIgst;
				liabilityIgst = liabilityIgst - temp;
				creditCgst = creditCgst - temp;
				igst_cgst += temp;

				temp = liabilityIgst - creditSgst >= 0.0 ? creditSgst : liabilityIgst;
				liabilityIgst = liabilityIgst - temp;
				creditSgst = creditSgst - temp;
				igst_sgst += temp;

				// adjusting CGST liability
				temp = liabilityCgst - creditCgst >= 0.0 ? creditCgst : liabilityCgst;
				liabilityCgst = liabilityCgst - temp;
				creditCgst = creditCgst - temp;
				cgst_cgst += temp;

				temp = liabilityCgst - creditIgst >= 0.0 ? creditIgst : liabilityCgst;
				liabilityCgst = liabilityCgst - temp;
				creditIgst = creditIgst - temp;
				cgst_igst += temp;

				// adjusting SGST liability
				temp = liabilitySgst - creditSgst >= 0.0 ? creditSgst : liabilitySgst;
				liabilitySgst = liabilitySgst - temp;
				creditSgst = creditSgst - temp;
				sgst_sgst += temp;

				temp = liabilitySgst - creditIgst >= 0.0 ? creditIgst : liabilitySgst;
				liabilitySgst = liabilitySgst - temp;
				creditIgst = creditIgst - temp;
				sgst_igst += temp;

				// adjusting CESS liability
				temp = liabilityCess - creditCess >= 0.0 ? creditCess : liabilityCess;
				liabilityCess = liabilityCess - temp;
				creditCess = creditCess - temp;
				cess_cess += temp;

				JSONObject outwardObject = new JSONObject();
				outwardObject.put("totalIgst", liabilityIgst);
				outwardObject.put("totalCgst", liabilityCgst);
				outwardObject.put("totalSgst", liabilitySgst);
				outwardObject.put("totalCess", liabilityCess);
				outwardObject.put("totalTax", liabilityIgst + liabilityCgst + liabilitySgst + liabilityCess);

				JSONObject inwardObject = new JSONObject();
				inwardObject.put("totalIgst", creditIgst);
				inwardObject.put("totalCgst", creditCgst);
				inwardObject.put("totalSgst", creditSgst);
				inwardObject.put("totalCess", creditCess);
				inwardObject.put("totalTax", creditIgst + creditCgst + creditSgst + creditCess);

				JSONObject diffObject = new JSONObject();
				diffObject.put("igst_igst", igst_igst);
				diffObject.put("igst_cgst", igst_cgst);
				diffObject.put("igst_sgst", igst_sgst);
				diffObject.put("cgst_cgst", cgst_cgst);
				diffObject.put("cgst_igst", cgst_igst);
				diffObject.put("sgst_sgst", sgst_sgst);
				diffObject.put("sgst_igst", sgst_igst);
				diffObject.put("cess_cess", cess_cess);

				JSONObject balLiabObject = new JSONObject();
				balLiabObject.put("totalIgst", liabilityIgst - igst_igst - igst_cgst - igst_sgst);
				balLiabObject.put("totalCgst", liabilityCgst - cgst_igst - cgst_cgst);
				balLiabObject.put("totalSgst", liabilitySgst - sgst_igst - sgst_sgst);
				balLiabObject.put("totalCess", liabilityCess - cess_cess);
				// balLiabObject.put("totalTax", creditIgst + creditCgst +
				// creditSgst + creditCess);

				JSONObject balCreditObject = new JSONObject();
				balCreditObject.put("totalIgst", creditIgst - igst_igst - igst_cgst - igst_sgst);
				balCreditObject.put("totalCgst", creditCgst - cgst_igst - cgst_cgst);
				balCreditObject.put("totalSgst", creditSgst - sgst_igst - sgst_sgst);
				balCreditObject.put("totalCess", creditCess - cess_cess);
				// balCreditObject.put("totalTax", creditIgst + creditCgst +
				// creditSgst + creditCess);

				Calendar cal = Calendar.getInstance();
				TaxpayerGstin taxpayerGstin = finderService
						.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

				GstinAction gstinAction = new GstinAction();
				gstinAction.setTaxpayerGstin(taxpayerGstin);
				gstinAction.setReturnType(ReturnType.GSTR3.toString());
				gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
				gstinAction.setStatus(GstnAction.UTILIZECREDIT.toString());
				gstinAction.setActionDate(cal.getTime());

				crudService.create(gstinAction);

				output.put("error", "false");
				output.put("outward", outwardObject);
				output.put("inward", inwardObject);
				output.put("diff", diffObject);
				output.put("balLiab", balLiabObject);
				output.put("balCredit", balCreditObject);

			} else {

				output.put("errorMsg", "Unable to process your request");

			}

			return output.toString();

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String utilizeCash(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		if (!Objects.isNull(gstinTransactionDTO)) {

			JSONObject output = new JSONObject();
			output.put("error", "true");

			String keyToCheck = "GSTR3_UTILIZECASH";
			String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

			if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {

				gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString());
				GstrSummaryDTO outSummaryDTO = new GstrSummaryDTO();// returnService.getTransactionSummary(gstinTransactionDTO);

				gstinTransactionDTO.setReturnType(ReturnType.GSTR2.toString());
				GstrSummaryDTO inSummaryDTO = new GstrSummaryDTO();// returnService.getTransactionSummary(gstinTransactionDTO);

				double liabilityIgst = outSummaryDTO.getTotalIgst();
				double liabilityCgst = outSummaryDTO.getTotalCgst();
				double liabilitySgst = outSummaryDTO.getTotalSgst();
				double liabilityCess = outSummaryDTO.getTotalCess();

				double creditIgst = inSummaryDTO.getTotalIgst();
				double creditCgst = inSummaryDTO.getTotalCgst();
				double creditSgst = inSummaryDTO.getTotalSgst();
				double creditCess = inSummaryDTO.getTotalCess();

				double diffIgst = 0.0;
				double diffCgst = 0.0;
				double diffSgst = 0.0;
				double diffCess = 0.0;

				// adjusting IGST liability
				double temp = liabilityIgst - creditIgst >= 0.0 ? creditIgst : liabilityIgst;
				liabilityIgst = liabilityIgst - temp;
				creditIgst = creditIgst - temp;
				diffIgst += temp;

				// adjusting CGST liability
				temp = liabilityCgst - creditCgst >= 0.0 ? creditCgst : liabilityCgst;
				liabilityCgst = liabilityCgst - temp;
				creditCgst = creditCgst - temp;
				diffCgst += temp;

				// adjusting SGST liability
				temp = liabilitySgst - creditSgst >= 0.0 ? creditSgst : liabilitySgst;
				liabilitySgst = liabilitySgst - temp;
				creditSgst = creditSgst - temp;
				diffSgst += temp;

				// adjusting CESS liability
				temp = liabilityCess - creditCess >= 0.0 ? creditCess : liabilityCess;
				liabilityCess = liabilityCess - temp;
				creditCess = creditCess - temp;
				diffCess += temp;

				JSONObject outwardObject = new JSONObject();
				outwardObject.put("totalIgst", liabilityIgst);
				outwardObject.put("totalCgst", liabilityCgst);
				outwardObject.put("totalSgst", liabilitySgst);
				outwardObject.put("totalCess", liabilityCess);
				outwardObject.put("totalTax", liabilityIgst + liabilityCgst + liabilitySgst + liabilityCess);

				JSONObject inwardObject = new JSONObject();
				inwardObject.put("totalIgst", creditIgst);
				inwardObject.put("totalCgst", creditCgst);
				inwardObject.put("totalSgst", creditSgst);
				inwardObject.put("totalCess", creditCess);
				inwardObject.put("totalTax", creditIgst + creditCgst + creditSgst + creditCess);

				JSONObject diffObject = new JSONObject();
				diffObject.put("igst_igst", diffIgst);
				diffObject.put("cgst_cgst", diffCgst);
				diffObject.put("sgst_sgst", diffSgst);
				diffObject.put("cess_cess", diffCess);

				JSONObject balanceObject = new JSONObject();
				balanceObject.put("igst", creditIgst - diffIgst);
				balanceObject.put("cgst", creditCgst - diffCgst);
				balanceObject.put("sgst", creditSgst - diffSgst);
				balanceObject.put("cess", creditCess - diffCess);

				Calendar cal = Calendar.getInstance();
				TaxpayerGstin taxpayerGstin = finderService
						.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

				GstinAction gstinAction = new GstinAction();
				gstinAction.setTaxpayerGstin(taxpayerGstin);
				gstinAction.setReturnType(ReturnType.GSTR3.toString());
				gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
				gstinAction.setStatus(GstnAction.UTILIZECASH.toString());
				gstinAction.setActionDate(cal.getTime());

				gstinAction
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				gstinAction.setCreationIpAddress(userBean.getIpAddress());
				gstinAction.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

				crudService.create(gstinAction);

				output.put("error", "false");
				output.put("outward", outwardObject);
				output.put("inward", inwardObject);
				output.put("diff", diffObject);
				output.put("balance", balanceObject);

			} else {

				output.put("errorMsg", "Unable to process your request");

			}

			return output.toString();

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public String getCashLedgerDetails(LedgerDto cashLedgerDto) throws AppException {
		if (!Objects.isNull(cashLedgerDto)) {

			LedgerITCDetails lts = new LedgerITCDetails();
			LedgerCashTransactionDetail transaction = new LedgerCashTransactionDetail();
			List<LedgerCashTransactionDetail> transactions = new ArrayList<>();
			transactions.add(transaction);
			CashLedgerBalance opBal = new CashLedgerBalance();
			CashLedgerBalance clBal = new CashLedgerBalance();
			lts.setFromDate(cashLedgerDto.getFromDate());
			lts.setToDate(cashLedgerDto.getFromDate());

			lts.setOpeningBalance(opBal);
			lts.setClosingBalance(clBal);
			lts.setTransactionDetails(transactions);
			lts.setGstin(cashLedgerDto.getGstin());
			// setting opening balance
			opBal.setOpeningTotal(1200000.00);
			opBal.setDescription("twelve lac only");
			LedgerTax lt = new LedgerTax();
			lt.setFee(5000.0);
			lt.setInterest(12.0);
			lt.setPenalty(1200.0);
			lt.setTax(500000.00);
			lt.setTotal(90000.00);
			opBal.setCgst(lt);
			opBal.setSgst(lt);
			opBal.setIgst(lt);

			// setting closing balancee

			clBal.setClosingTotal(1500000.00);
			clBal.setDescription("fifteen lac only");
			LedgerTax lt1 = new LedgerTax();
			lt1.setFee(6000.0);
			lt1.setInterest(17.0);
			lt1.setPenalty(1800.0);
			lt1.setTax(510000.00);
			lt1.setTotal(390000.00);
			clBal.setCgst(lt);
			clBal.setSgst(lt);
			clBal.setIgst(lt);

			// setting transaction
			transaction.setBalance(150000000.0);
			transaction.setCess(lt);
			transaction.setCgst(lt1);
			transaction.setDate(new Date());
			// transaction.setDebitNo("1231414");
			transaction.setDescription("description ....");
			transaction.setTransactionType("B2b");

			// will delete above code

			CashLedgerBalance openingBalance = lts.getOpeningBalance();
			CashLedgerBalance closingBalance = lts.getClosingBalance();
			List<LedgerCashTransactionDetail> cashtransactions = lts.getTransactionDetails();
			List<CashLedgerDto> clds = new ArrayList<>();
			CashLedgerDto opnCld = new CashLedgerDto();
			clds.add(opnCld);
			// setting opening balance
			int sn = 1;
			if (!Objects.isNull(openingBalance)) {
				opnCld.setSno(sn);
				opnCld.setDate(lts.getFromDate());
				opnCld.setRefNo("-");
				opnCld.setDesc(openingBalance.getDescription());
				opnCld.setType("-");
				opnCld.setIgst(!Objects.isNull(openingBalance.getIgst()) ? openingBalance.getIgst().getTotal() : null);
				opnCld.setSgst(!Objects.isNull(openingBalance.getSgst()) ? openingBalance.getSgst().getTotal() : null);
				opnCld.setCgst(!Objects.isNull(openingBalance.getCgst()) ? openingBalance.getCgst().getTotal() : null);
				opnCld.setCess(!Objects.isNull(openingBalance.getCess()) ? openingBalance.getCess().getTotal() : null);
				opnCld.setBalance(openingBalance.getOpeningTotal());
			}
			// setting tranasaction

			for (LedgerCashTransactionDetail tran : cashtransactions) {
				sn++;
				CashLedgerDto tCld = new CashLedgerDto();
				tCld.setSno(sn++);
				tCld.setDate(tran.getDate());
				tCld.setRefNo(tran.getReferenceNo());
				tCld.setDesc(tran.getDescription());
				tCld.setType(tran.getTransactionType());
				tCld.setIgst(!Objects.isNull(tran.getIgst()) ? tran.getIgst().getTotal() : null);
				tCld.setSgst(!Objects.isNull(tran.getSgst()) ? tran.getSgst().getTotal() : null);
				tCld.setCgst(!Objects.isNull(tran.getCgst()) ? tran.getCgst().getTotal() : null);
				tCld.setCess(!Objects.isNull(tran.getCess()) ? tran.getCess().getTotal() : null);
				tCld.setBalance(tran.getBalance());
				clds.add(tCld);

			}

			// setting closing balance
			CashLedgerDto clsingCld = new CashLedgerDto();
			clds.add(clsingCld);
			sn++;
			if (!Objects.isNull(closingBalance)) {

				clsingCld.setSno(sn);
				clsingCld.setDate(lts.getToDate());
				clsingCld.setRefNo("-");
				clsingCld.setDesc(closingBalance.getDescription());
				clsingCld.setType("-");
				clsingCld.setIgst(
						!Objects.isNull(closingBalance.getIgst()) ? closingBalance.getIgst().getTotal() : null);
				clsingCld.setSgst(
						!Objects.isNull(closingBalance.getSgst()) ? closingBalance.getSgst().getTotal() : null);
				clsingCld.setCgst(
						!Objects.isNull(closingBalance.getCgst()) ? closingBalance.getCgst().getTotal() : null);
				clsingCld.setCess(
						!Objects.isNull(closingBalance.getCess()) ? closingBalance.getCess().getTotal() : null);
				clsingCld.setBalance(closingBalance.getClosingTotal());
			}

			try {
				return JsonMapper.objectMapper.writeValueAsString(clds);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return null;

	}

	public String getItcLedgerDetails(LedgerDto cashLedgerDto) throws AppException {
		if (!Objects.isNull(cashLedgerDto)) {

			LedgerITCDetails lts = new LedgerITCDetails();
			LedgerITCTransactionDetail transaction = new LedgerITCTransactionDetail();
			List<LedgerITCTransactionDetail> transactions = new ArrayList<>();
			transactions.add(transaction);

			lts.setFromDate(cashLedgerDto.getFromDate());
			lts.setToDate(cashLedgerDto.getFromDate());

			lts.setItcTransactionDetails(transactions);
			lts.setGstin(cashLedgerDto.getGstin());
			// setting opening balance

			LedgerTax lt = new LedgerTax();
			lt.setFee(5000.0);
			lt.setInterest(12.0);
			lt.setPenalty(1200.0);
			lt.setTax(500000.00);
			lt.setTotal(90000.00);

			// setting closing balancee

			LedgerTax lt1 = new LedgerTax();
			lt1.setFee(6000.0);
			lt1.setInterest(17.0);
			lt1.setPenalty(1800.0);
			lt1.setTax(510000.00);
			lt1.setTotal(390000.00);
			// setting transaction
			transaction.setBalance(150000000.0);
			transaction.setCess(lt);
			transaction.setCgst(lt1);
			transaction.setDate(new Date());
			transaction.setDebitNo("1231414");
			transaction.setTransactionType("B2b");
			transaction.setTransactionDescription("description of the transaction");

			// will delete above code
			List<LedgerITCTransactionDetail> itcTransactions = lts.getItcTransactionDetails();
			List<ItcLedgerDto> ilds = new ArrayList<>();

			// setting tranasaction
			int sn = 0;
			for (LedgerITCTransactionDetail tran : itcTransactions) {
				sn++;
				ItcLedgerDto ild = new ItcLedgerDto();
				ild.setSno(sn++);
				ild.setDate(tran.getDate());
				ild.setDebitNo(tran.getDebitNo());
				ild.setDesc(tran.getTransactionDescription());
				ild.setType(tran.getTransactionType());
				ild.setIgst(!Objects.isNull(tran.getIgst()) ? tran.getIgst().getTotal() : null);
				ild.setSgst(!Objects.isNull(tran.getSgst()) ? tran.getSgst().getTotal() : null);
				ild.setCgst(!Objects.isNull(tran.getCgst()) ? tran.getCgst().getTotal() : null);
				ild.setCess(!Objects.isNull(tran.getCess()) ? tran.getCess().getTotal() : null);
				ild.setBalance(tran.getBalance());
				ilds.add(ild);

			}

			try {
				return JsonMapper.objectMapper.writeValueAsString(ilds);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return null;

	}

	public String getLiabilityLedgerDetails(LedgerDto cashLedgerDto) throws AppException {

		if (!Objects.isNull(cashLedgerDto)) {

			LiabilityLedgerDetail lld = new LiabilityLedgerDetail();
			TransactionHeads transaction = new TransactionHeads();
			List<TransactionHeads> transactions = new ArrayList<>();
			transactions.add(transaction);

			TransactionHeads openingBalance = new TransactionHeads();
			lld.setAmountOverdue(openingBalance);
			openingBalance.setBal(999999.0);
			openingBalance.setDesc("opening balance desc......");
			lld.setGstin(cashLedgerDto.getGstin());
			lld.setMonthYear(cashLedgerDto.getMonthYear());
			;
			lld.setTr(transactions);
			// setting cgst sgst
			LedgerTax lt = new LedgerTax();
			lt.setFee(5000.0);
			lt.setInterest(12.0);
			lt.setPenalty(1200.0);
			lt.setTax(500000.00);
			lt.setTotal(90000.00);
			// setting closing balancee

			LedgerTax lt1 = new LedgerTax();
			lt1.setFee(6000.0);
			lt1.setInterest(17.0);
			lt1.setPenalty(1800.0);
			lt1.setTax(510000.00);
			lt1.setTotal(390000.00);
			// setting overdue amount
			openingBalance.setBal(50000.00);
			openingBalance.setDesc("closing balance");
			;
			openingBalance.setSgst(lt);
			openingBalance.setCgst(lt1);
			openingBalance.setIgst(lt);
			openingBalance.setBal(50000.00);

			// setting transaction
			transaction.setDesc("description ....");
			transaction.setTransactionCode("tra2222");
			transaction.setDate(new Date());
			transaction.setCgst(lt);
			transaction.setIgst(lt1);
			transaction.setSgst(lt);
			transaction.setCess(lt1);

			// will delete above code

			TransactionHeads amtOverdue = lld.getAmountOverdue();
			List<TransactionHeads> liabilitytransactions = lld.getTr();
			List<LiabilityLedgerDto> llds = new ArrayList<>();

			// setting tranasaction
			int sn = 0;
			for (TransactionHeads tran : liabilitytransactions) {
				sn++;
				LiabilityLedgerDto llDto = new LiabilityLedgerDto();
				llDto.setSno(sn++);
				llDto.setDate(tran.getDate());
				llDto.setTransactionCode(tran.getTransactionCode());
				llDto.setRefNo(tran.getRefNo());
				llDto.setDesc(tran.getDesc());
				llDto.setType(tran.getTransactionType());
				llDto.setIgst(!Objects.isNull(tran.getIgst()) ? tran.getIgst().getTotal() : null);
				llDto.setSgst(!Objects.isNull(tran.getSgst()) ? tran.getSgst().getTotal() : null);
				llDto.setCgst(!Objects.isNull(tran.getCgst()) ? tran.getCgst().getTotal() : null);
				llDto.setCess(!Objects.isNull(tran.getCess()) ? tran.getCess().getTotal() : null);
				llDto.setBalance(tran.getBal());
				llds.add(llDto);

			}

			// setting amount overdue
			LiabilityLedgerDto clsingCld = new LiabilityLedgerDto();
			llds.add(clsingCld);
			sn++;
			if (!Objects.isNull(clsingCld)) {

				clsingCld.setSno(sn);
				clsingCld.setTransactionCode("-");
				clsingCld.setDate(new Date());
				clsingCld.setDesc(amtOverdue.getDesc());
				clsingCld.setType("-");
				clsingCld.setIgst(!Objects.isNull(amtOverdue.getIgst()) ? amtOverdue.getIgst().getTotal() : null);
				clsingCld.setSgst(!Objects.isNull(amtOverdue.getSgst()) ? amtOverdue.getSgst().getTotal() : null);
				clsingCld.setCgst(!Objects.isNull(amtOverdue.getCgst()) ? amtOverdue.getCgst().getTotal() : null);
				clsingCld.setCess(!Objects.isNull(amtOverdue.getCess()) ? amtOverdue.getCess().getTotal() : null);
				clsingCld.setBalance(amtOverdue.getBal());
			}

			try {
				return JsonMapper.objectMapper.writeValueAsString(llds);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return null;
	}

	@Override
	public List<ReturnStatusDTO> checkReturnStatusByTransaction(GstinTransactionDTO gstinTransactionDTO,
			String transactionType) throws AppException {

		String response = "";

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<ReturnStatus> returnStatus = finderService.getReturnStatusByTransaction(gstinTransactionDTO,
					transactionType);

			List<GstinTransaction> gstinTransactions = null;// finderService.getMultipleGstinTransaction(gstinTransactionDTO);
			GstinTransaction gstinTransaction = null;
			if (!Objects.isNull(gstinTransactions) && !gstinTransactions.isEmpty()) {
				gstinTransaction = gstinTransactions.get(0);
			}

			ReturnStatusDTO returnStatusDTO = new ReturnStatusDTO();
			List<ReturnStatusDTO> returnStatusDTOs = new ArrayList<>();
			try {
				if (!Objects.isNull(returnStatus)) {

					for (ReturnStatus returnStatus2 : returnStatus) {

						TransactionFactory transactionFactory = new TransactionFactory(returnStatus2.getTransaction());

						GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
						getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
						getReturnStatusReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
						getReturnStatusReq.setMonthYear(gstinTransactionDTO.getMonthYear());
						getReturnStatusReq.setRef_id(returnStatus2.getReferenceId());

						GetGstr1ReturnStatusResp getGstr1ReturnStatusResp = gstnService
								.getGstr1ReturnStatus(getReturnStatusReq);
						response = JsonMapper.objectMapper.writeValueAsString(getGstr1ReturnStatusResp);
						returnStatus2.setStatus("PROCESSED");
						returnStatus2.setResponse(response);
						returnStatus2.setUpdatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						returnStatus2.setUpdationIpAddress(userBean.getIpAddress());
						returnStatus2.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
						crudService.update(returnStatus2);

						String transitId = returnStatus2.getTransitId();
						if (!StringUtils.isEmpty(transitId) && !Objects.isNull(gstinTransaction)) {
							String processedTrasitObject = transactionFactory.processTransitData(
									gstinTransaction.getTransactionObject(), transitId, getGstr1ReturnStatusResp);
							GstinTransactionDTO transactionDTO = (GstinTransactionDTO) EntityHelper
									.convert(gstinTransaction, GstinTransactionDTO.class);
							transactionDTO.setTransactionObject(processedTrasitObject);
							returnService.storeTransactionDataGstn(gstinTransactionDTO);
						}

						returnStatusDTO = (ReturnStatusDTO) EntityHelper.convert(returnStatus2, ReturnStatusDTO.class);
						returnStatusDTOs.add(returnStatusDTO);
					}

				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return returnStatusDTOs;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public GetCheckStatusGstr2Resp checkGstr2ReturnStatus(GstinTransactionDTO gstinTransactionDTO) throws Exception {

		String response = "";
		GetCheckStatusGstr2Resp checkStatusResp = null;
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<ReturnStatus> returnStatus = finderService.getReturnStatus(gstinTransactionDTO);

			List<GstinTransaction> gstinTransactions = null;// finderService.getMultipleGstinTransaction(gstinTransactionDTO);
			GstinTransaction gstinTransaction = null;
			if (!Objects.isNull(gstinTransactions) && !gstinTransactions.isEmpty()) {
				gstinTransaction = gstinTransactions.get(0);
			}

			ReturnStatusDTO returnStatusDTO = new ReturnStatusDTO();
			List<ReturnStatusDTO> returnStatusDTOs = new ArrayList<>();
			try {
				if (!Objects.isNull(returnStatus)) {

					for (ReturnStatus returnStatus2 : returnStatus) {

						GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
						getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
						getReturnStatusReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
						getReturnStatusReq.setMonthYear(gstinTransactionDTO.getMonthYear());
						getReturnStatusReq.setRef_id(returnStatus2.getReferenceId());
						// getReturnStatusReq.setTransId(returnStatus2.getTransitId());

						// GetGstr1ReturnStatusResp getGstr1ReturnStatusResp = gstnService
						// .getGstr1ReturnStatus(getReturnStatusReq);
						String checkStatusResponse = gstnService.getGstr2ReturnStatus(getReturnStatusReq);
						if (!StringUtils.isEmpty(checkStatusResponse))
							checkStatusResp = JsonMapper.objectMapper.readValue(checkStatusResponse,
									GetCheckStatusGstr2Resp.class);

						if (checkStatusResp != null) {
							this.updateErrorReportsGstr2(gstinTransactionDTO, checkStatusResp,
									returnStatus2.getTransitId());

							returnStatus2.setFlag(checkStatusResp.getStatus());
							// if(!checkStatusResp.getStatus().equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_IN_PROCESS))
							returnStatus2.setStatus("PROCESSED");
							returnStatus2.setResponse(checkStatusResponse);
							returnStatus2.setUpdatedBy(
									userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
							returnStatus2.setUpdationIpAddress(userBean.getIpAddress());
							returnStatus2.setTotalSync(gstinTransactionDTO.getCountPStatus());
							if (!"IP".equalsIgnoreCase(checkStatusResp.getStatus()))
								returnStatus2.setTotalNotSync(
										(gstinTransactionDTO.getCountERStatus() + gstinTransactionDTO.getCountPEStatus()
												+ gstinTransactionDTO.getCountIPStatus()));
							returnStatus2.setTotalCount(gstinTransactionDTO.getCountTotalReturnStatus());
							returnStatus2.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
							crudService.update(returnStatus2);

						} else {
							throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.ERR_MSG);
						}

					}
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error("json excpetion", e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
			return checkStatusResp;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void removeQueryParam(Map<String, Object> queryParamMap) {
		if (queryParamMap != null) {
			if (queryParamMap.containsKey("invno"))
				queryParamMap.remove("invno");
			if (queryParamMap.containsKey("ctin"))
				queryParamMap.remove("ctin");
			if (queryParamMap.containsKey("pos"))
				queryParamMap.remove("pos");
			if (queryParamMap.containsKey("suptyp"))
				queryParamMap.remove("suptyp");
			if (queryParamMap.containsKey("transitId"))
				queryParamMap.remove("transitId");
			if (queryParamMap.containsKey("revisedInvNo"))
				queryParamMap.remove("revisedInvNo");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List updateErrorReports(GstinTransactionDTO gstinTransactionDTO, GetCheckStatusResp statusResp,
			String transitId) throws Exception {

		log.info("entering in updateErrorReports method transitId{} " + transitId);
		Map<String, Object> queryParameters = new HashMap<>();
		String returnStatusStr = statusResp.getStatus();
		int count = 0;
		List<B2BDetailEntity> responseLista = new ArrayList<>();
		try {
			if (statusResp != null) {
				if (statusResp.getStatus() != null) {
					queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
					queryParameters.put("gstn", (TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
					queryParameters.put("monthYear", finderService.getMonthYearsAsPerPreference(
							gstinTransactionDTO.getMonthYear(), gstinTransactionDTO.getTaxpayerGstin().getGstin()));
					if (statusResp.getStatus().equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
						if (statusResp.getErrorReport().getB2b() != null) {
							if (!statusResp.getErrorReport().getB2b().isEmpty()) {
								List<B2BDetailEntity> listb2bs = statusResp.getErrorReport().getB2b();
								// log.info("error found in b2b invoices {}"+listb2bs.toString());
								for (B2BDetailEntity b2bdetail : listb2bs) {
									queryParameters.put("invno", b2bdetail.getInvoiceNumber());
									queryParameters.put("ctin", b2bdetail.getCtin());
									// log.info("getting b2b invoices with queryparameters "+queryParameters);
									List<B2BDetailEntity> responseList = crudService
											.findWithNamedQuery("B2bDetailEntity.findErrorInvoices", queryParameters);
									count = count + responseList.size();
									if (!responseList.isEmpty()) {
										B2BDetailEntity b2bdetails = responseList.get(0);
										b2bdetails.setIsError(true);
										b2bdetails.setErrMsg(b2bdetail.getError_msg());
										b2bdetails.setIsTransit(false);
										b2bdetails.setFlags("ERROR");
										crudService.update(b2bdetails);
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getCdnr() != null) {
							if (!statusResp.getErrorReport().getCdnr().isEmpty()) {
								List<CDNDetailEntity> listcdns = statusResp.getErrorReport().getCdnr();
								// log.info("error found in b2b invoices {}"+listcdns.toString());
								for (CDNDetailEntity cdndetail : listcdns) {
									queryParameters.put("invno", cdndetail.getRevisedInvNo());
									queryParameters.put("ctin", cdndetail.getOriginalCtin());
									List<CDNDetailEntity> responseList = crudService
											.findWithNamedQuery("CDNDetailEntity.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											CDNDetailEntity cdndetailEnt = responseList.get(0);
											cdndetailEnt.setIsError(true);
											cdndetailEnt.setErrMsg(cdndetail.getError_msg());
											cdndetailEnt.setIsTransit(false);
											cdndetailEnt.setFlags("ERROR");
											crudService.update(cdndetailEnt);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getCdnur() != null) {
							if (!statusResp.getErrorReport().getCdnur().isEmpty()) {
								List<CDNURDetailEntity> listcdns = statusResp.getErrorReport().getCdnur();
								// log.info("error found in cdnur invoices {}"+listcdns.toString());
								for (CDNURDetailEntity cdndetail : listcdns) {
									queryParameters.put("revisedInvNo", cdndetail.getRevisedInvNo());
									// log.info("getting cdnur invoices with queryparameters "+queryParameters);
									List<CDNURDetailEntity> responseList = crudService
											.findWithNamedQuery("CdnUrDetailEntity.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											CDNURDetailEntity cdnurentity = responseList.get(0);
											cdnurentity.setIsError(true);
											cdnurentity.setErrMsg(cdndetail.getError_msg());
											cdnurentity.setIsTransit(false);
											cdnurentity.setFlags("ERROR");
											crudService.update(cdnurentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getB2cl() != null) {
							if (!statusResp.getErrorReport().getB2cl().isEmpty()) {
								List<B2CLDetailEntity> listb2cl = statusResp.getErrorReport().getB2cl();
								// log.info("error found in b2cl invoices {}"+listb2cl.toString());
								for (B2CLDetailEntity b2cldetail : listb2cl) {
									queryParameters.put("invno", b2cldetail.getInvoiceNumber());
									// log.info("getting b2cl invoices with queryparameters "+queryParameters);
									// queryParameters.put("ctin",
									// b2cldetail.getOriginalCtin());
									List<B2CLDetailEntity> responseList = crudService
											.findWithNamedQuery("B2clDetailEntity.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											B2CLDetailEntity b2clentity = responseList.get(0);
											b2clentity.setIsError(true);
											b2clentity.setErrMsg(b2cldetail.getError_msg());
											b2clentity.setIsTransit(false);
											b2clentity.setFlags("ERROR");
											crudService.update(b2clentity);
										}
									}

								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getB2cs() != null) {
							if (!statusResp.getErrorReport().getB2cs().isEmpty()) {
								List<B2CSTransactionEntity> listb2cs = statusResp.getErrorReport().getB2cs();
								// log.info("error found in b2cs invoices {}"+listb2cs.toString());
								for (B2CSTransactionEntity b2csdetail : listb2cs) {
									queryParameters.put("pos", b2csdetail.getPos());
									// log.info("getting b2cs invoices with queryparameters "+queryParameters);
									List<B2CSTransactionEntity> responseList = crudService.findWithNamedQuery(
											"B2CSTransactionEntity.findErrorInvoice", queryParameters);

									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											for (B2CSTransactionEntity b2csentuty : responseList) {

												if (b2csdetail.getTaxRate() == b2csentuty.getTaxRate()) {
													b2csentuty.setIsError(true);
													b2csentuty.setErrMsg(b2csdetail.getError_msg());
													b2csentuty.setIsTransit(false);
													b2csentuty.setFlags("ERROR");
													crudService.update(b2csentuty);
													++count;
												}
											}
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getAt() != null) {
							if (!statusResp.getErrorReport().getAt().isEmpty()) {
								List<ATTransactionEntity> listat = statusResp.getErrorReport().getAt();
								// log.info("error found in AT invoices {}"+listat.toString());
								for (ATTransactionEntity at : listat) {
									queryParameters.put("pos", at.getPos());
									// log.info("getting at with queryparameters "+queryParameters);
									List<ATTransactionEntity> responseList = crudService.findWithNamedQuery(
											"ATTransactionEntity.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											ATTransactionEntity atentity = responseList.get(0);
											atentity.setIsError(true);
											atentity.setErrMsg(at.getErrorMsg());
											atentity.setIsTransit(false);
											atentity.setFlags("ERROR");
											crudService.update(atentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getExp() != null) {
							if (!statusResp.getErrorReport().getExp().isEmpty()) {
								List<ExpDetail> listexp = statusResp.getErrorReport().getExp();
								// log.info("error found in exp invoices {}"+listexp.toString());
								for (ExpDetail exp : listexp) {
									queryParameters.put("invno", exp.getInvoiceNumber());
									// log.info("getting exp invoices with queryparameters "+queryParameters);
									List<ExpDetail> responseList = crudService
											.findWithNamedQuery("ExpDetail.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											ExpDetail expentity = responseList.get(0);
											expentity.setIsError(true);
											expentity.setErrMsg(exp.getError_msg());
											expentity.setIsTransit(false);
											expentity.setFlags("ERROR");
											crudService.update(expentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getTxpd() != null) {
							if (!statusResp.getErrorReport().getTxpd().isEmpty()) {
								List<TxpdTransaction> listtxp = statusResp.getErrorReport().getTxpd();
								// log.info("error found in txp invoices {}"+listtxp.toString());
								for (TxpdTransaction txp : listtxp) {
									queryParameters.put("pos", txp.getPos());
									// log.info("getting txpd invoices with queryparameters "+queryParameters);
									List<TxpdTransaction> responseList = crudService
											.findWithNamedQuery("TxpdTransaction.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											TxpdTransaction txpentity = responseList.get(0);
											txpentity.setIsError(true);
											txpentity.setErrMsg(txp.getError_msg());
											txpentity.setIsTransit(false);
											txpentity.setFlags("ERROR");
											crudService.update(txpentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getNil() != null) {
							if (!statusResp.getErrorReport().getNil().isEmpty()) {
								List<NILTransactionEntity> listnil = statusResp.getErrorReport().getNil();
								// log.info("error found in nil invoices {}"+listnil.toString());
								for (NILTransactionEntity nil : listnil) {
									queryParameters.put("suptyp", nil.getSupplyType());
									// log.info("getting nil invoices with queryparameters "+queryParameters);
									List<NILTransactionEntity> responseList = crudService.findWithNamedQuery(
											"NILTransactionEntity.findErrorInvoice", queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											NILTransactionEntity nilentity = responseList.get(0);
											nilentity.setIsError(true);
											nilentity.setErrMsg(nil.getErrorMsg());
											nilentity.setIsTransit(false);
											nilentity.setFlags("ERROR");
											crudService.update(nilentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getHsn() != null) {
							if (!statusResp.getErrorReport().getHsn().isEmpty()) {
								List<Hsn> listhsn = statusResp.getErrorReport().getHsn();
								for (Hsn hsn : listhsn) {
									List<Hsn> responseList = crudService.findWithNamedQuery("Hsn.findErrorInvoice",
											queryParameters);
									count = count + responseList.size();
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											Hsn hsnentity = responseList.get(0);
											hsnentity.setIsError(true);
											hsnentity.setErrMsg(hsn.getError_msg());
											hsnentity.setIsTransit(false);
											hsnentity.setFlags("ERROR");
											crudService.update(hsnentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						// if(CollectionUtils.isEmpty(statusResp.getErrorReport().))
						if (!CollectionUtils.isEmpty(statusResp.getErrorReport().getDocissue())) {
							List<DocIssue> listdocs = statusResp.getErrorReport().getDocissue();
							for (DocIssue doc : listdocs) {
								List<DocIssue> responseList = crudService
										.findWithNamedQuery("DocIssue.findErrorInvoice", queryParameters);
								count = count + responseList.size();
								if (statusResp.getStatus()
										.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
									if (!responseList.isEmpty()) {
										DocIssue docentity = responseList.get(0);
										docentity.setIsError(true);
										docentity.setErrMsg(doc.getError_msg());
										docentity.setIsTransit(false);
										docentity.setFlags("ERROR");
										crudService.update(docentity);
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						gstinTransactionDTO.setCountPEStatus(count);
						returnStatusStr = GSTNRespStatus.CHECKSTATUS_PROCESSED; // to mark remaining invocie processed
					}

					queryParameters.put("transitId", transitId);

					List objectsToMark = new ArrayList<>();
					objectsToMark.addAll(crudService.findWithNamedQuery("B2bDetailEntity.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService.findWithNamedQuery("CDNDetailEntity.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("CdnUrDetailEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("B2CSTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService.findWithNamedQuery("TxpdTransaction.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("ATTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(
							crudService.findWithNamedQuery("ExpDetail.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService.findWithNamedQuery("B2clDetailEntity.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("NILTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(
							crudService.findWithNamedQuery("Hsn.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(
							crudService.findWithNamedQuery("DocIssue.findErrorInvoicesByTransitId", queryParameters));

					this.updateReturnStatusFlag(objectsToMark, returnStatusStr, gstinTransactionDTO);
				}
			}
		} catch (Exception e) {
			log.error("error while check status {}", e);
			throw new AppException(ExceptionCode._ERROR);
		}
		return responseLista;
	}

	@SuppressWarnings("unchecked")
	public List updateErrorReportsGstr2(GstinTransactionDTO gstinTransactionDTO, GetCheckStatusGstr2Resp statusResp,
			String transitId) throws Exception {

		Map<String, Object> queryParameters = new HashMap<>();
		String returnStatusStr = statusResp.getStatus();
		List<B2BDetailEntity> responseLista = new ArrayList<>();
		int count = 0;
		boolean isHsnError = false;
		try {
			if (statusResp != null) {
				if (statusResp.getStatus() != null) {
					queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
					queryParameters.put("gstn", (TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
					queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());
					if (statusResp.getStatus().equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {

						if (!CollectionUtils.isEmpty(statusResp.getErrorReport().getB2b())) {
							List<B2BDetailEntity> listb2bs = statusResp.getErrorReport().getB2b();
							for (B2BDetailEntity b2bdetail : listb2bs) {
								count++;
								queryParameters.put("invno", b2bdetail.getInvoiceNumber());
								queryParameters.put("transitId", transitId);
								queryParameters.put("ctin", b2bdetail.getCtin());
								List<B2BDetailEntity> responseList = crudService
										.findWithNamedQuery("B2bDetailEntity.findErrorInvoicesGstr2", queryParameters);
								if (!responseList.isEmpty()) {
									B2BDetailEntity b2bdetails = responseList.get(0);
									b2bdetails.setIsError(true);
									b2bdetails.setErrMsg(b2bdetail.getError_msg());
									b2bdetails.setIsTransit(false);
									b2bdetails.setFlags("ERROR");
									crudService.update(b2bdetails);
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getCdn() != null) {
							if (!statusResp.getErrorReport().getCdn().isEmpty()) {
								List<CDNDetailEntity> listcdns = statusResp.getErrorReport().getCdn();
								for (CDNDetailEntity cdndetail : listcdns) {
									count++;
									queryParameters.put("invno", cdndetail.getInvoiceNumber());
									queryParameters.put("transitId", transitId);
									queryParameters.put("ctin", cdndetail.getOriginalCtin());
									List<CDNDetailEntity> responseList = crudService.findWithNamedQuery(
											"CDNDetailEntity.findErrorInvoiceGstr2", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											CDNDetailEntity cdndetailEnt = responseList.get(0);
											cdndetailEnt.setIsError(true);
											cdndetailEnt.setErrMsg(cdndetail.getError_msg());
											cdndetailEnt.setIsTransit(false);
											cdndetailEnt.setFlags("ERROR");
											crudService.update(cdndetailEnt);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getB2bur() != null) {
							if (!statusResp.getErrorReport().getB2bur().isEmpty()) {
								List<B2bUrTransactionEntity> listb2bs = statusResp.getErrorReport().getB2bur();
								for (B2bUrTransactionEntity b2bdetail : listb2bs) {
									count++;
									queryParameters.put("invno", b2bdetail.getInvoiceNumber());
									List<B2bUrTransactionEntity> responseList = crudService.findWithNamedQuery(
											"B2bUrTransactionEntity.findErrorInvoice", queryParameters);

									if (!responseList.isEmpty()) {
										B2bUrTransactionEntity b2bdetails = responseList.get(0);
										b2bdetails.setIsError(true);
										b2bdetails.setErrMsg(b2bdetail.getErrorMsg());
										b2bdetails.setIsTransit(false);
										b2bdetails.setFlags("ERROR");
										crudService.update(b2bdetails);
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getCdnur() != null) {
							if (!statusResp.getErrorReport().getCdnur().isEmpty()) {
								List<CDNURDetailEntity> listcdns = statusResp.getErrorReport().getCdnur();
								for (CDNURDetailEntity cdndetail : listcdns) {
									queryParameters.put("invno", cdndetail.getInvoiceNumber());
									count++;
									List<CDNURDetailEntity> responseList = crudService
											.findWithNamedQuery("CdnUrDetailEntity.findErrorInvoice", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											CDNURDetailEntity cdnurentity = responseList.get(0);
											cdnurentity.setIsError(true);
											cdnurentity.setErrMsg(cdndetail.getError_msg());
											cdnurentity.setIsTransit(false);
											cdnurentity.setFlags("ERROR");
											crudService.update(cdnurentity);
										}
									}
								}
							}
						}

						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getAt() != null) {
							if (!statusResp.getErrorReport().getAt().isEmpty()) {
								List<ATTransactionEntity> listat = statusResp.getErrorReport().getAt();
								for (ATTransactionEntity at : listat) {
									queryParameters.put("pos", at.getPos());
									count++;
									List<ATTransactionEntity> responseList = crudService.findWithNamedQuery(
											"ATTransactionEntity.findErrorInvoice", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											ATTransactionEntity atentity = responseList.get(0);
											atentity.setIsError(true);
											atentity.setErrMsg(at.getErrorMsg());
											atentity.setIsTransit(false);
											atentity.setFlags("ERROR");
											crudService.update(atentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getTxpd() != null) {
							if (!statusResp.getErrorReport().getTxpd().isEmpty()) {
								List<TxpdTransaction> listtxp = statusResp.getErrorReport().getTxpd();
								for (TxpdTransaction txp : listtxp) {
									queryParameters.put("pos", txp.getPos());
									count++;
									List<TxpdTransaction> responseList = crudService
											.findWithNamedQuery("TxpdTransaction.findErrorInvoice", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											TxpdTransaction txpentity = responseList.get(0);
											txpentity.setIsError(true);
											txpentity.setErrMsg(txp.getError_msg());
											txpentity.setIsTransit(false);
											txpentity.setFlags("ERROR");
											crudService.update(txpentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getImpg() != null) {
							if (!statusResp.getErrorReport().getImpg().isEmpty()) {
								List<IMPGTransactionEntity> listimpg = statusResp.getErrorReport().getImpg();
								for (IMPGTransactionEntity impg : listimpg) {
									queryParameters.put("invno", impg.getBillOfEntryNumber());
									count++;
									List<IMPGTransactionEntity> responseList = crudService.findWithNamedQuery(
											"IMPGTransactionEntity.findErrorInvoice", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											IMPGTransactionEntity impgentity = responseList.get(0);
											impgentity.setIsError(true);
											impgentity.setErrMsg(impg.getErrorMsg());
											impgentity.setIsTransit(false);
											impgentity.setFlags("ERROR");
											crudService.update(impgentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getImps() != null) {
							if (!statusResp.getErrorReport().getImps().isEmpty()) {
								List<IMPSTransactionEntity> listimps = statusResp.getErrorReport().getImps();
								for (IMPSTransactionEntity imps : listimps) {
									queryParameters.put("invno", imps.getInvoiceNumber());
									count++;
									List<IMPSTransactionEntity> responseList = crudService.findWithNamedQuery(
											"IMPSTransactionEntity.findErrorInvoice", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											IMPSTransactionEntity impsentity = responseList.get(0);
											impsentity.setIsError(true);
											impsentity.setErrMsg(imps.getErrorMsg());
											impsentity.setIsTransit(false);
											impsentity.setFlags("ERROR");
											crudService.update(impsentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getNil() != null) {
							if (!statusResp.getErrorReport().getNil().isEmpty()) {
								List<NILTransactionEntity> listnils = statusResp.getErrorReport().getNil();
								for (NILTransactionEntity nil : listnils) {
									queryParameters.put("suptyp", nil.getSupplyType());
									count++;
									List<NILTransactionEntity> responseList = crudService.findWithNamedQuery(
											"NILTransactionEntity.findErrorInvoice", queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										if (!responseList.isEmpty()) {
											NILTransactionEntity nilentity = responseList.get(0);
											nilentity.setIsError(true);
											nilentity.setErrMsg(nil.getErrorMsg());
											nilentity.setIsTransit(false);
											nilentity.setFlags("ERROR");
											crudService.update(nilentity);
										}
									}
								}
							}
						}
						removeQueryParam(queryParameters);
						if (statusResp.getErrorReport().getHsn() != null) {
							if (!statusResp.getErrorReport().getHsn().isEmpty()) {
								List<Hsn> listhsn = statusResp.getErrorReport().getHsn();
								for (Hsn hsn : listhsn) {
									count++;
									List<Hsn> responseList = crudService.findWithNamedQuery("Hsn.findErrorInvoice",
											queryParameters);
									if (statusResp.getStatus()
											.equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR)) {
										responseList.forEach(hsnentity -> {
											if (hsn.getCode().equalsIgnoreCase(hsnentity.getCode())) {
												hsnentity.setIsError(true);
												hsnentity.setErrMsg(hsn.getError_msg());
												hsnentity.setIsTransit(false);
												hsnentity.setFlags("ERROR");
												crudService.update(hsnentity);
											}
										});
									}
									isHsnError = true;

								}
							}
						}
						removeQueryParam(queryParameters);
						gstinTransactionDTO.setCountPEStatus(count);
						returnStatusStr = GSTNRespStatus.CHECKSTATUS_PROCESSED; // to mark remaining invocie processed
					}

					queryParameters.put("transitId", transitId);
					List objectsToMark = new ArrayList<>();
					objectsToMark.addAll(crudService.findWithNamedQuery("B2bDetailEntity.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("ATTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService.findWithNamedQuery("TxpdTransaction.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService.findWithNamedQuery("CDNDetailEntity.findErrorInvoicesByTransitId",
							queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("IMPSTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("IMPGTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("CdnUrDetailEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService.findWithNamedQuery(
							"B2bUrTransactionEntity.findErrorInvoicesByTransitId", queryParameters));
					objectsToMark.addAll(crudService
							.findWithNamedQuery("NILTransactionEntity.findErrorInvoicesByTransitId", queryParameters));

					if (!isHsnError)
						objectsToMark.addAll(
								crudService.findWithNamedQuery("Hsn.findErrorInvoicesByTransitId", queryParameters));

					this.updateReturnStatusFlag(objectsToMark, returnStatusStr, gstinTransactionDTO);
				}
			}
		} catch (Exception e) {
			log.error("error while upaterror reports  {}", e);
			throw new AppException(ExceptionCode._ERROR);
		}
		return responseLista;

	}

	@SuppressWarnings("unchecked")
	private void updateReturnStatusFlag(List objsTOMark, String response, GstinTransactionDTO gstinTransactionDTO)
			throws Exception {

		if (!Objects.isNull(objsTOMark) && !objsTOMark.isEmpty() && !Objects.isNull(response)) {

			int countPStatus = 0;
			int countIPStatus = 0;
			int countERStatus = 0;
			for (Object returnObj : objsTOMark) {
				try {
					if (response.equalsIgnoreCase("P")) {
						BeanUtils.setProperty(returnObj, "isTransit", false);

						if ("DELETE".equalsIgnoreCase(BeanUtils.getProperty(returnObj, "flags"))) {
							BeanUtils.setProperty(returnObj, "isSynced", false);
							returnService.deleteTransaction(gstinTransactionDTO.getTaxpayerGstin().getGstin(),
									gstinTransactionDTO.getMonthYear(),
									TransactionType.valueOf(BeanUtils.getProperty(returnObj, "transactionType")),
									ReturnType.valueOf(gstinTransactionDTO.getReturnType()),
									Arrays.asList(new String[] { BeanUtils.getProperty(returnObj, "id") }),
									DeleteType.SELECTED, DataSource.EMGST);
							continue;
						} else
							BeanUtils.setProperty(returnObj, "isSynced", true);

						BeanUtils.setProperty(returnObj, "toBeSync", false);
						countPStatus++;
					}
					if (response.equalsIgnoreCase("ER")) {
						BeanUtils.setProperty(returnObj, "isTransit", false);
						countERStatus++;
					}
					if (response.equalsIgnoreCase("IP")) {
						BeanUtils.setProperty(returnObj, "isTransit", true);
						countIPStatus++;
					} else
						// if(response.equalsIgnoreCase("ER"))
						BeanUtils.setProperty(returnObj, "isTransit", false);

					crudService.update(returnObj);
				} catch (Exception e) {
					log.error("exception while updating flag {} ", e);
					throw new AppException(ExceptionCode._ERROR);
				}
			}
			gstinTransactionDTO.setCountPStatus(countPStatus);
			gstinTransactionDTO.setCountIPStatus(countIPStatus);
			gstinTransactionDTO.setCountERStatus(countERStatus);
		}
	}

	public List<ReturnStatus> getIPTransGstin(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		Map<String, Object> queryParam = new HashMap<>();
		queryParam.put("monthYear", gstinTransactionDTO.getMonthYear());
		queryParam.put("returnType", gstinTransactionDTO.getReturnType());
		queryParam.put("gstn",
				(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));

		List<ReturnStatus> returnStatusIPData = crudService.findWithNamedQuery("ReturnStatus.findIPByGstn", queryParam);
		if (returnStatusIPData != null)
			return returnStatusIPData;
		else
			throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.NO_DATA_TO_BE_SYNC);
	}
	// @SuppressWarnings({ "unchecked" })
	// private void updateReturnStatusFlag(Object returnObj,String response)
	// throws IllegalAccessException, IllegalArgumentException,
	// InvocationTargetException {
	// try {
	// if(response.equalsIgnoreCase("P")){
	// BeanUtils.setProperty(returnObj, "isTransit", false);
	// BeanUtils.setProperty(returnObj, "isSynced", true);
	// BeanUtils.setProperty(returnObj, "toBeSync", false);
	// }
	// if(response.equalsIgnoreCase("ER"))
	// BeanUtils.setProperty(returnObj, "isTransit", false);
	// if(response.equalsIgnoreCase("IP"))
	// BeanUtils.setProperty(returnObj, "isTransit", true);
	//
	// crudService.update(returnObj);
	// } catch (Exception e) {
	// e.printStackTrace();
	// log.error("exception while updating flag {} ", e);
	// }
	// }

	@Override
	public GstrFlowDTO submitGstr2ToGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException, Exception {
		if (!returnService.isAllSynced(gstinTransactionDTO.getMonthYear(),
				gstinTransactionDTO.getTaxpayerGstin().getGstin(),
				ReturnType.valueOf(gstinTransactionDTO.getReturnType().toUpperCase())))
			throw new AppException(ExceptionCode._NOTSYNCED);

		if (!Objects.isNull(gstinTransactionDTO)) {

			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			Calendar cal = Calendar.getInstance();

			if (!Objects.isNull(taxpayerGstin)) {

				String keyToCheck = gstinTransactionDTO.getReturnType() + "_" + GstnAction.SUBMIT.toString();
				String result = checkGstrFlowEligibility(gstinTransactionDTO, keyToCheck);

				GstrFlowDTO flowDTO = new GstrFlowDTO();

				SubmitGstr2Req submitGstr2Req = new SubmitGstr2Req();
				submitGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				submitGstr2Req.setRetPeriod(gstinTransactionDTO.getMonthYear());
				submitGstr2Req.setAction(Returns.RETSUBMIT.toString());

				if (!StringUtils.isEmpty(result) && result.equals("TRUE")) {
					SubmitGstr2Resp submitGstr2Resp = gstnService.submitGstr2Data(submitGstr2Req);
					gstnResponseProcessor.processGstnResponse(submitGstr2Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());
					GstinAction gstinAction = new GstinAction();
					gstinAction.setTaxpayerGstin(taxpayerGstin);
					gstinAction.setReturnType(gstinTransactionDTO.getReturnType());
					gstinAction.setParticular(gstinTransactionDTO.getMonthYear());
					gstinAction.setStatus(GstnAction.SUBMIT.toString());
					gstinAction.setActionDate(cal.getTime());

					if (StringUtils.isNotEmpty(submitGstr2Resp.getReferenceId()))
						;
					gstinAction.setReferenceId(submitGstr2Resp.getReferenceId());

					crudService.create(gstinAction);

					taxpayerService.updateMonthYearForGstin(taxpayerGstin);

					flowDTO.setTransactionId(submitGstr2Resp.getTransactionId());
					flowDTO.setReferenceId(submitGstr2Resp.getReferenceId());
					flowDTO.setError(false);

				} else {

					flowDTO.setError(true);
					if (StringUtils.isEmpty(result)) {
						flowDTO.setErrorMsg("Unable to process your request");
					} else {
						flowDTO.setErrorMsg(result);
					}

				}
				return flowDTO;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	@Transactional
	public GetCheckStatusResp processReturnStatusGstr1(ReturnStatus returnStatus) throws Exception {

		String response = "";
		GetCheckStatusResp checkStatusResp = null;
		try {
			if (!Objects.isNull(returnStatus)) {
				YearMonth yearmonth = YearMonth.parse(returnStatus.getMonthYear(),
						DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
				getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
				getReturnStatusReq.setGstin(returnStatus.getTaxpayerGstin().getGstin());
				getReturnStatusReq.setMonthYear(returnStatus.getMonthYear());
				getReturnStatusReq.setRef_id(returnStatus.getReferenceId());
				String checkStatusResponse = gstnService.getGst1ReturnStatus(getReturnStatusReq);
				if (!StringUtils.isEmpty(checkStatusResponse))
					checkStatusResp = JsonMapper.objectMapper.readValue(checkStatusResponse, GetCheckStatusResp.class);
				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				gstinTransactionDTO.setReturnType(returnStatus.getReturnType());
				gstinTransactionDTO.setMonthYear(returnStatus.getMonthYear());
				gstinTransactionDTO.setTaxpayerGstin(
						taxpayerService.getTaxpayerGstin(returnStatus.getTaxpayerGstin().getGstin(), fyear));

				if (checkStatusResp != null) {
					this.updateErrorReports(gstinTransactionDTO, checkStatusResp, returnStatus.getTransitId());

					returnStatus.setFlag(checkStatusResp.getStatus());
					returnStatus.setStatus("PROCESSED");
					returnStatus.setResponse(checkStatusResponse);
					returnStatus.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					returnStatus.setUpdationIpAddress(userBean.getIpAddress());
					returnStatus.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					returnStatus.setTotalSync(gstinTransactionDTO.getCountPStatus());

					if (!"IP".equalsIgnoreCase(checkStatusResp.getStatus()))
						returnStatus.setTotalNotSync((gstinTransactionDTO.getCountERStatus()
								+ gstinTransactionDTO.getCountPEStatus() + gstinTransactionDTO.getCountIPStatus()));
					crudService.update(returnStatus);
				} else {
					throw new AppException(ExceptionCode._ERROR);
				}
			}
		} catch (JsonProcessingException e) {
			log.error("json excpetion", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return checkStatusResp;

	}

	@Override
	public GetCheckStatusGstr2Resp processReturnStatusGstr2(ReturnStatus returnStats) throws Exception {

		String response = "";
		GetCheckStatusGstr2Resp checkStatusResp = null;
		try {
			if (!Objects.isNull(returnStats)) {

				GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
				getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
				getReturnStatusReq.setGstin(returnStats.getTaxpayerGstin().getGstin());
				getReturnStatusReq.setMonthYear(returnStats.getMonthYear());
				getReturnStatusReq.setRef_id(returnStats.getReferenceId());
				YearMonth yearmonth = YearMonth.parse(returnStats.getMonthYear(),
						DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				String checkStatusResponse = gstnService.getGstr2ReturnStatus(getReturnStatusReq);
				if (!StringUtils.isEmpty(checkStatusResponse))
					checkStatusResp = JsonMapper.objectMapper.readValue(checkStatusResponse,
							GetCheckStatusGstr2Resp.class);

				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				gstinTransactionDTO.setReturnType(returnStats.getReturnType());
				gstinTransactionDTO.setMonthYear(returnStats.getMonthYear());

				gstinTransactionDTO.setTaxpayerGstin(
						taxpayerService.getTaxpayerGstin(returnStats.getTaxpayerGstin().getGstin(), fyear));

				if (checkStatusResp != null) {
					this.updateErrorReportsGstr2(gstinTransactionDTO, checkStatusResp, returnStats.getTransitId());

					returnStats.setFlag(checkStatusResp.getStatus());
					// if(!checkStatusResp.getStatus().equalsIgnoreCase(GSTNRespStatus.CHECKSTATUS_IN_PROCESS))
					returnStats.setStatus("PROCESSED");
					returnStats.setResponse(checkStatusResponse);
					returnStats.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					returnStats.setUpdationIpAddress(userBean.getIpAddress());
					returnStats.setTotalSync(gstinTransactionDTO.getCountPStatus());
					if (!"IP".equalsIgnoreCase(checkStatusResp.getStatus()))
						returnStats.setTotalNotSync((gstinTransactionDTO.getCountERStatus()
								+ gstinTransactionDTO.getCountPEStatus() + gstinTransactionDTO.getCountIPStatus()));
					returnStats.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(returnStats);

				} else {
					throw new AppException(ExceptionCode._FAILURE);
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("json excpetion", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return checkStatusResp;
	}

	public Map<Integer, String> unZipFile(String outputFolder, InputStream is, String iv, String encKey) {
		log.info("entering in unzip file method.");
		byte[] buffer = new byte[1024];
		outputFolder = "c://easemygst//unzip";
		Map<Integer, String> responseMap = null;
		try {
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}
			ZipInputStream zis = new ZipInputStream(is);
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();
			Integer i = 1;
			responseMap = new HashMap<>();

			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);
				responseMap.put(i, newFile.getName());
				log.info("file unzip " + newFile.getAbsoluteFile());

				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
				i++;
			}

			zis.closeEntry();
			zis.close();
		} catch (IOException ex) {
			log.error("exception while unzipping {}" + ex);
		}
		log.info("completed unzipping of files with reponse " + responseMap);
		return responseMap;
	}

	@Override
	public GetGstr3Resp getGstr3Details(GstinTransactionDTO gstnTransactionDTO) throws AppException {
		GetGstr3Resp gstr3resp = null;
		try {
			GetGstr3DetailReq gstr3detailReq = new GetGstr3DetailReq();
			gstr3detailReq.setAction(Returns.RETSUM.toString());
			gstr3detailReq.setGstin(gstnTransactionDTO.getTaxpayerGstin().getGstin());
			gstr3detailReq.setMonthYear(gstnTransactionDTO.getMonthYear());
			gstr3detailReq.setFinalvalue("Y");

			String response = gstnService.getGstr3Details(gstr3detailReq);
			if (!StringUtils.isEmpty(response)) {
				gstr3resp = JsonMapper.objectMapper.readValue(response, GetGstr3Resp.class);
			} else
				throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.ERR_MSG);
		} catch (Exception e) {
			log.error("exception jsonmapper" + e);
		}
		return gstr3resp;
	}

	public GetGstr3Resp setOffGstr3Liability(GstinTransactionDTO gstnTransactionDTO) throws AppException {
		GetGstr3Resp gstr3resp = null;
		try {
			SetOffLiabilityDataReq setoffliabilityReq = this.getSetoffLiabilityData();// call method to get req object

			String data = JsonMapper.objectMapper.writeValueAsString(setoffliabilityReq);

			JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("gstnjson/SetoffLiability.json");
			String jsonTxt = IOUtils.toString(is);

			ObjectMapper mapper = new ObjectMapper();
			JsonNode schemaNode = mapper.readTree(jsonTxt);
			JsonNode data1 = mapper.readTree(data);

			ProcessingReport report = VALIDATOR.validateUnchecked(schemaNode, data1);

			final boolean success = report.isSuccess();

			final JsonNode reportAsJson = ((AsJson) report).asJson();
			log.info("rpayload json  {}", data);
			log.debug("json validation result {}  report {}", success, reportAsJson);

			if (success) {
				setoffliabilityReq.setAction(Returns.RETOFFSET.toString());
				setoffliabilityReq.setGstin(gstnTransactionDTO.getTaxpayerGstin().getGstin());
				setoffliabilityReq.setMonthYear(gstnTransactionDTO.getMonthYear());
				String response = gstnService.setoffLiabilities(setoffliabilityReq);
				if (!StringUtils.isEmpty(response)) {
					gstr3resp = JsonMapper.objectMapper.readValue(response, GetGstr3Resp.class);
				} else
					throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.ERR_MSG);
			} else
				throw new AppException(ExceptionCode._INVALID_JSON);
		} catch (Exception e) {
			log.error("exception jsonmapper {} " + e);
		}
		return gstr3resp;

	}

	private SetOffLiabilityDataReq getSetoffLiabilityData() {

		SetOffLiabilityDataReq setoffreq = new SetOffLiabilityDataReq();

		SetOffLiabilityDataReq.TaxOffset taxoffset = setoffreq.new TaxOffset();

		PaidCash paidcash = new PaidCash();
		paidcash.setIgstAmt(BigDecimal.valueOf(15.66));
		paidcash.setCgstAmt(BigDecimal.valueOf(16.66));
		paidcash.setSgstAmt(BigDecimal.valueOf(18.66));
		paidcash.setCessAmt(BigDecimal.valueOf(19.66));

		taxoffset.setPaidCash(paidcash);

		SetOffLiabilityDataReq.IntrOffset introffset = setoffreq.new IntrOffset();

		introffset.setCesspaidInterest(BigDecimal.valueOf(15.66));
		introffset.setCgstpaidInterest(BigDecimal.valueOf(17.66));

		setoffreq.setIntrOffset(introffset);
		setoffreq.setTaxOffset(taxoffset);

		return setoffreq;
	}

	@Override
	public GetGstr3Resp submitRefund(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		GetGstr3Resp gstr3resp = null;
		try {
			SubmitRefundData submitrefund = this.getSubmitRefundData();// call method to get req object

			String data = JsonMapper.objectMapper.writeValueAsString(submitrefund);

			JsonValidator VALIDATOR = JsonSchemaFactory.byDefault().getValidator();

			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("gstnjson/submitrefund.json");
			String jsonTxt = IOUtils.toString(is);

			ObjectMapper mapper = new ObjectMapper();
			JsonNode schemaNode = mapper.readTree(jsonTxt);
			JsonNode data1 = mapper.readTree(data);

			ProcessingReport report = VALIDATOR.validateUnchecked(schemaNode, data1);

			final boolean success = report.isSuccess();

			final JsonNode reportAsJson = ((AsJson) report).asJson();
			log.info("rpayload json  {}", data);
			log.debug("json validation result {}  report {}", success, reportAsJson);

			if (success) {

				submitrefund.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				submitrefund.setMonthYear(gstinTransactionDTO.getMonthYear());
				submitrefund.setAction(Returns.RETSUBMIT.toString());

				String response = gstnService.submitRefund(submitrefund);
				if (!StringUtils.isEmpty(response)) {
					gstr3resp = JsonMapper.objectMapper.readValue(response, GetGstr3Resp.class);
				} else
					throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.ERR_MSG);
			} else
				throw new AppException(ExceptionCode._INVALID_JSON);
		} catch (Exception e) {
			log.error("exception jsonmapper {} " + e);
		}
		return gstr3resp;
	}

	private SubmitRefundData getSubmitRefundData() {
		SubmitRefundData submitrefund = new SubmitRefundData();
		SubmitRefundData.RefundClaim obj = submitrefund.new RefundClaim();
		obj.setAccNo(12345);
		Claim claim = new Claim();
		claim.setFee(BigDecimal.valueOf(45.66));
		claim.setIntr(BigDecimal.valueOf(45.66));
		claim.setPen(BigDecimal.valueOf(45.66));
		obj.setCgrClaim(claim);
		submitrefund.setRefundClaim(obj);
		return submitrefund;
	}

	@Override
	public int saveDataToTokenResponse(GstinTransactionDTO gstinTransactionDTO, String type) throws AppException {
		try {
			TokenResponse tp = new TokenResponse();
			tp.setMonthYear(gstinTransactionDTO.getMonthYear());
			tp.setReturnType(gstinTransactionDTO.getReturnType());
			tp.setMonthYear(gstinTransactionDTO.getMonthYear());
			tp.setReturnType(gstinTransactionDTO.getReturnType());
			tp.setValidity("0");
			tp.setCreationTime(Timestamp.from(Instant.now()));
			tp.setUpdationTime(Timestamp.from(Instant.now()));
			tp.setStatus("COMPLETED");
			tp.setToken("NOTOKEN");
			tp.setTransaction(type);
			tp.setUrlcount(0);
			tp.setTaxpayerGstin(finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()));
			TokenResponse tokenResponse = crudService.create(tp);
			EncodedData encodeData = new EncodedData();
			encodeData.setEncodedData("NOENCODEDDATA");
			encodeData.setReferenceId(tokenResponse);
			encodeData.setStatus("COMPLETED");
			encodeData.setInvoiceCount(gstinTransactionDTO.getTotalSync());
			crudService.create(encodeData);

			// logging user action
			MessageDto messageDto = new MessageDto();
			messageDto.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			messageDto.setNoOfInvoices(String.valueOf(gstinTransactionDTO.getTotalSync()));
			messageDto.setTransactionType(type);
			apiLoggingService.saveAction(UserAction.GET_2A, messageDto,
					gstinTransactionDTO.getTaxpayerGstin().getGstin());
			// end logging user action

		} catch (Exception e) {
			log.error("exception while entering data {}" + e);
		}
		return 1;
	}

	public void getReturnsTrackFromGSTN(List<TrackReturnsInfo> trackReturnsInfos) throws AppException {

		for (TrackReturnsInfo trackReturnsInfo : trackReturnsInfos) {

			log.info("view track api under process for ", trackReturnsInfo.toString());

			ViewTrackReturnsReq trackReturnsReq = new ViewTrackReturnsReq();

			trackReturnsReq.setAction(Returns.RETTRACK.toString());
			trackReturnsReq.setGstin(trackReturnsInfo.getTaxpayerGstin().getGstin());
			trackReturnsReq.setReturnPeriod(trackReturnsInfo.getMonthYear());
			trackReturnsReq.setReturnType(StringUtils.upperCase(trackReturnsInfo.getReturnType()));

			try {
				ViewTrackReturnsResp trackReturnsResp = gstnService.viewTrackReturns(trackReturnsReq);

				if (!Objects.isNull(trackReturnsResp)) {
					try {
						gstnResponseProcessor.processGstnResponse(trackReturnsResp,
								trackReturnsInfo.getTaxpayerGstin().getGstin());
					} catch (AppException ae) {
						
						log.error("appexception ",ae);
						if (ExceptionCode._UNAUTHORISED_GSTIN.equalsIgnoreCase(ae.getCode()))
							throw ae;
					}

					if (!Objects.isNull(trackReturnsResp.getEfiledlist())) {
						for (EFileDetails eFileDetails : trackReturnsResp.getEfiledlist()) {

							try {
							GstinAction gstinAction = new GstinAction();
							gstinAction.setTaxpayerGstin(
									finderService.findTaxpayerGstin(trackReturnsInfo.getTaxpayerGstin().getGstin()));
							gstinAction.setReturnType(eFileDetails.getReturnType());
							gstinAction.setParticular(eFileDetails.getReturnPeriod());
							gstinAction.setModeOfFilling(eFileDetails.getModeOfFilling());
							gstinAction.setIsvalid(eFileDetails.getIsValidReturn());
							gstinAction.setStatus(StringUtils.upperCase(eFileDetails.getStatus()));
							gstinAction.setFinanacialYear(CalanderCalculatorUtility.getFiscalYearByMonthYear(
									Utility.convertStrToYearMonth(eFileDetails.getReturnPeriod())));
							try {
								gstinAction.setActionDate(
										new SimpleDateFormat("dd-MM-yyyy").parse(eFileDetails.getDateOfFilling()));
							} catch (ParseException e) {
								log.error("exception while parsing date ", e);
							}
							gstinAction.setReferenceId(eFileDetails.getArnNumber());
							gstinAction = crudService.create(gstinAction);

							log.info("gstin action updated for gstin {} action {} monthYear {}",
									gstinAction.getTaxpayerGstin().getGstin(), gstinAction.getStatus(),
									gstinAction.getParticular());
							}catch(Exception e) {
								log.error("exception ",e);
							}
						}
					}
				}
				trackReturnsInfo.setProcessed(true);
				crudService.update(trackReturnsInfo);

			} catch (AppException e) {
				log.error("Appexception while processing returns track for {} exception message {}",
						trackReturnsInfo.toString(), e.getMessage());
				throw e;
			}

		}
	}

	
	@Override
	public String syncGstr6TransactionData(GstinTransactionDTO gstinTransactionDTO)
			throws AppException, JsonParseException, JsonMappingException, IOException {
		String tempResponse = "";
		String tokenResp = "";
		// this.processUrls(gstinTransactionDTO);
		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			List<String> transactionTypes = new ArrayList<>();

			if (!gstinTransactionDTO.getTransactionType().equalsIgnoreCase("%")) {
				if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
					transactionTypes.add(gstinTransactionDTO.getTransactionType());
				} else {
					transactionTypes.add(TransactionType.B2B.toString());
					transactionTypes.add(TransactionType.CDN.toString());
				}
			}

			else {
				transactionTypes.add(TransactionType.B2B.toString());
				transactionTypes.add(TransactionType.CDN.toString());
			}
			GetGstr2Resp getGstr2Resp = null;
			for (String type : transactionTypes) {

				// check if 2A is already processed
				// finderService.getDataStatus(gstinTransactionDTO);

				GetGstr2Req getGstr2Req = new GetGstr2Req();
				getGstr2Req.setAction(Returns.valueOf(type).toString());
				getGstr2Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
				getGstr2Req.setMonthYear(gstinTransactionDTO.getMonthYear());
				getGstr2Req.setReturnType(ReturnType.GSTR6);

				// new String(Files.readAllBytes(Paths.get("")));
				tempResponse = gstnService.getGstr2Data(getGstr2Req);
				log.info("get data from gstn response {}", tempResponse);

				getGstr2Resp = JsonMapper.objectMapper.readValue(tempResponse, GetGstr2Resp.class);

				if (!Objects.isNull(getGstr2Resp))
					gstnResponseProcessor.processGstnResponse(getGstr2Resp,
							gstinTransactionDTO.getTaxpayerGstin().getGstin());
			}
			return GSTR6FileGenerator.generateCSV(getGstr2Resp, TransactionType.valueOf(gstinTransactionDTO.getTransactionType()),
					gstinTransactionDTO.getTaxpayerGstin().getGstin(),gstinTransactionDTO.getMonthYear());
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}
}
