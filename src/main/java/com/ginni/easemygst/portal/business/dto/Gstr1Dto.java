package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.ATDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CLDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CSDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNDeserializer;
import com.ginni.easemygst.portal.data.deserialize.EXPDeserializer;
import com.ginni.easemygst.portal.data.deserialize.HSNSUMDeserializer;
import com.ginni.easemygst.portal.data.deserialize.NILDeserializer;
import com.ginni.easemygst.portal.data.part.ECOM;
import com.ginni.easemygst.portal.data.serialize.ATSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BURSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CLSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CSSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNURSerializer;
import com.ginni.easemygst.portal.data.serialize.EXPSerializer;
import com.ginni.easemygst.portal.data.serialize.HSNSUMSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPGSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPSBSerializer;
import com.ginni.easemygst.portal.data.serialize.NILSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.view.deserializer.ATDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.B2CLDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.B2bDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.B2bUrDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.B2csDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.CDNDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.CDNURDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.DocIssueDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.EXPDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.HsnSumDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.IMPSDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.ImpgDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.NilDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.TXPDetailDeserializer;
import com.ginni.easemygst.portal.data.view.serializer.TXPDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNTransaction;
import com.ginni.easemygst.portal.transaction.impl.IMPSTransaction;

import lombok.Data;


@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public @Data class Gstr1Dto implements Serializable {

	
	@JsonDeserialize(using=B2bDetailDeserializer.class)
	@JsonProperty("b2bs")
	private List<B2BTransactionEntity> b2bs;
	
	@JsonDeserialize(using=B2csDetailDeserializer.class)
	@JsonProperty("b2css")
	private List<B2CSTransactionEntity> b2css;
	
	
	@JsonDeserialize(using=HsnSumDetailDeserializer.class)
	@JsonProperty("hsnsums")
	private List<Hsn> hsnsums;
	
	@JsonDeserialize(using=EXPDetailDeserializer.class)
	@JsonProperty("exps")
	private List<EXPTransactionEntity> exps;
	
	
	@JsonDeserialize(using=NilDetailDeserializer.class)
	@JsonProperty("nils")
	private List<NILTransactionEntity> nils;
	
	@JsonDeserialize(using=DocIssueDetailDeserializer.class)
	@JsonProperty("docIssues")
	private List<DocIssue> docIssues;
	
	@JsonDeserialize(using=CDNDetailDeserializer.class)
	@JsonProperty("cdns")
	private List<CDNTransactionEntity> cdns;
	
	@JsonDeserialize(using=B2bUrDetailDeserializer.class)
	@JsonProperty("b2burs")
	private List<B2bUrTransactionEntity> b2burs;
	
	@JsonDeserialize(using=CDNURDetailDeserializer.class)
	@JsonProperty("cdnurs")
	private List<CDNURTransactionEntity> cdnurs;
	
	@JsonDeserialize(using=ATDetailDeserializer.class)
	@JsonProperty("ats")
	private List<ATTransactionEntity> ats;
	
	@JsonDeserialize(using=ImpgDetailDeserializer.class)
	@JsonProperty("impgs")
	private List<IMPGTransactionEntity> impgs;
	
	@JsonDeserialize(using=TXPDetailDeserializer.class)
	@JsonProperty("txpds")
	private List<TxpdTransaction> txpds;
	
	@JsonDeserialize(using=IMPSDetailDeserializer.class)
	@JsonProperty("impss")
	private List<IMPSTransactionEntity> impss;
	
	@JsonDeserialize(using=B2CLDetailDeserializer.class)
	@JsonProperty("b2cls")
	private List<B2CLTransactionEntity> b2cls;
	

	/*@JsonDeserialize(using=B2BDeserializer.class)
	@JsonProperty("b2ba")
	private List<B2B> b2ba;

	@JsonDeserialize(using=B2CLDeserializer.class)
	@JsonSerialize(using = B2CLSerializer.class)
	@JsonProperty("b2cl")
	private List<B2CLDetailEntity> b2cl;
	
	@JsonDeserialize(using=B2CLDeserializer.class)
	@JsonSerialize(using = B2CLSerializer.class)
	@JsonProperty("b2cla")
	private List<B2CL> b2cla;
	
	
	
	@JsonDeserialize(using=B2CSDeserializer.class)
	@JsonSerialize(using = B2CSSerializer.class)
	@JsonProperty("b2csa")
	private List<B2CS> b2csa;
	
	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNSerializer.class)
	@JsonProperty("cdnr")
	private List<CDNDetailEntity> cdn;

	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNSerializer.class)
	@JsonProperty("cdnra")
	private List<CDN> cdna;
	
	
	
	
	@JsonDeserialize(using=EXPDeserializer.class)
	@JsonProperty("expa")
	private List<EXP> expa;
	
	@JsonSerialize(using=ATSerializer.class)
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonProperty("at")
	private List<ATTransactionEntity> at;
	
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonProperty("ata")
	private List<AT> ata;

	//@JsonDeserialize(using=TXPDeserializer.class)
	@JsonSerialize(using=TXPSerializer.class)
	@JsonProperty("txpd")
	private List<TxpdTransaction> txpd;
	
	@JsonProperty("ecom")
	private List<ECOM> ecom;
	
	
	
	//@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNURSerializer.class)
	@JsonProperty("cdnur")
	private List<CDNURTransactionEntity> cdnur;
	
	@JsonSerialize(using = IMPGSerializer.class)
	@JsonProperty("imp_g")
	private List<IMPGTransactionEntity> impg;
	
	@JsonSerialize(using = IMPSBSerializer.class)
	@JsonProperty("imp_s")
	private List<IMPSTransactionEntity> imps;
	
	@JsonSerialize(using = B2BURSerializer.class)
	@JsonProperty("b2bur")
	private List<B2bUrTransactionEntity> b2bur;*/
	
}
