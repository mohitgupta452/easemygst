package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class UserGstinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String type;

	private TaxpayerGstinDTO taxpayerGstin;
	
	private GstReturnDTO gstReturnBean;
	
	public interface UserGstinDTOMixIn extends CommonMixin {
		
	}
	
}
