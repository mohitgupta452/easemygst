package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import lombok.Data;

public @Data class UserOrganisationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String name;
	
	private String email;
	
	private Boolean yours;
	
	private Boolean active;
	
}
