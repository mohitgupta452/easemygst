package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.Data;

public @Data class SkuDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Byte active;

	private Byte isDefault;

	private String code;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String description;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	private List<SkuDetailDTO> skuDetails;
	
	public interface SkuDTOMixIn extends CommonMixin {
		
	}

}
