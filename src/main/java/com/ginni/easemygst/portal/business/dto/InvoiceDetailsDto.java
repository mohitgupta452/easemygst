package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;

public @Data class InvoiceDetailsDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;

	private String etin;
	
	private String poNumber;

	private Date invoiceDate;
	
	private Date dueDate;

	private String invoiceNumber;

	private double invoiceValue;

	private JsonNode itemDetails;

	private String pos;
	
	private String type;

	private String natureOfTransaction;

	private double taxAmount;

	private double taxableValue;

	private String remarks;
	
}
