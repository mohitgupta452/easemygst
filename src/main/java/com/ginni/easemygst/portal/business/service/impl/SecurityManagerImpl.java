package com.ginni.easemygst.portal.business.service.impl;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.SecurityManager;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.entity.GstinSubscriptionDuration;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;

public class SecurityManagerImpl implements SecurityManager {
	
	@Inject
	private RedisService redisService;
	
	@Inject
	private CrudService crudService;
	
	@Inject
	private UserBean userBean;
	
	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;
	
	
	

	@Override
	public void checkIfSaveNotAllowedForGstin(GstinTransactionDTO gstinTransactionDto) throws AppException {
		if (("24AAFCM0070J1ZQ".equalsIgnoreCase(gstinTransactionDto.getTaxpayerGstin().getGstin())
				)&& ("072017".equalsIgnoreCase(gstinTransactionDto.getMonthYear())
						|| "082017".equalsIgnoreCase(gstinTransactionDto.getMonthYear())
						|| "092017".equalsIgnoreCase(gstinTransactionDto.getMonthYear()))) {
			AppException ae = new AppException();
			ae.setMessage(ApplicationMetadata.RETURN_SAVE_IS_NOT_ALLOWED_FOR_GSTIN);
			throw ae;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkSubscriptionDurationByGstin(GstinTransactionDTO gstinTransactionDto) throws AppException{
		String tinSubscriptionKey = "sub" + gstinTransactionDto.getTaxpayerGstin().getGstin()
				+ gstinTransactionDto.getMonthYear();
		TaxpayerGstin gstin = new TaxpayerGstin();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMyyyy");
		Calendar cal = Calendar.getInstance();
		YearMonth monthYear = YearMonth.parse(gstinTransactionDto.getMonthYear(), formatter);

		cal.clear();
		cal.set(Calendar.MONTH, monthYear.getMonth().getValue());
		cal.set(Calendar.YEAR, monthYear.getYear());
		Date month = cal.getTime();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());
		GstinSubscriptionDuration subcDuration = redisService
				.getGstinSubscritionDurationByGstinMonthYear(tinSubscriptionKey);
		if (Objects.isNull(subcDuration)) {
			List<GstinSubscriptionDuration> subs = new ArrayList<>();

			subs = crudService.findWithNamedQuery("GstinSubscriptionDuration.findByGstinMonthYearOrganization",
					QueryParameter.with("gstin", gstinTransactionDto.getTaxpayerGstin().getId()).and("monthYear", month)
							.parameters());

			if (!subs.isEmpty())
				subcDuration = subs.get(0);
			redisService.setSubscriptionDurationForGstin(tinSubscriptionKey, subcDuration, false, 60 * 60 * 6);
		}

		if (Objects.isNull(subcDuration)) {
			AppException ae = new AppException();
			ae.setMessage(ApplicationMetadata.RETURN_SAVE_IS_NOT_ALLOWED_FOR_GSTIN);
			throw ae;
		}
		return true;

	}

	
	@Override
public void checkTrialPeriodConstraint()throws AppException {
		if (!Objects.isNull(userBean)) {
			int orgId = userBean.getUserDto().getOrganisation().getId();
			Organisation org = em.find(Organisation.class, orgId);
			if (!Objects.isNull(org)
					&& (StringUtils.isEmpty(org.getStatus()) || org.getStatus().equalsIgnoreCase("trial"))) {
				throw new AppException(ExceptionCode._NOT_ALLOWED_IN_TRIAL);
			}

		}

	}

}
