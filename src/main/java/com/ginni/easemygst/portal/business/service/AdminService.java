package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;

import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface AdminService  {


	void unSync(String type, String returntype, String gstin, String monthYear) throws AppException;
  
	
	
}
