package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

public @Data class UserContactDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public UserContactDTO() {
		
	}
			
	public UserContactDTO(Integer id, String address, String city, String emailAddress, String mobileNumber) {
		this.id = id;
		this.address = address;
		this.city = city;
		this.emailAddress = emailAddress;
		this.mobileNumber = mobileNumber;
	}

	private Integer id;

	private String address;

	private String city;

		private String emailAddress;

	private Byte emailAuth;

	private String image;

	private Byte mobileAuth;

	private String mobileNumber;

	private StateDTO stateBean; 
	
	public interface UserContactDTOMixIn extends CommonMixin {
		@JsonIgnore abstract Byte getEmailAuth();
		@JsonIgnore abstract Byte getMobileAuth();
	}

}
