package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class TrackReturnDTO {
	
	private String gstin;
	
	private String monthYear;
	
	private String returnType;
	
	TrackReturnDTO(String gstin,String monthYear,String returnType){
		
		this.gstin=gstin;
		this.monthYear=monthYear;
		this.returnType=returnType;
	}

}
