package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class UserTaxpayerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String createdBy;

	private Timestamp creationTime;

	private String type;

	private String updatedBy;

	private Timestamp updationTime;
	
	private String creationIpAddress;
	
	private String updationIpAddress;
	
	private TaxpayerDTO taxpayer;
	
	public interface UserTaxpayerDTOMixIn extends CommonMixin {
		
	}
		
}
