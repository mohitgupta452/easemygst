package com.ginni.easemygst.portal.business.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Comparison2avs3bDTO;
import com.ginni.easemygst.portal.business.dto.ComparisonDTO;
import com.ginni.easemygst.portal.business.dto.GstinComparison2aVS3bDTO;
import com.ginni.easemygst.portal.business.dto.GstinComparisonDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.GstnService;
import com.ginni.easemygst.portal.business.service.ReportService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata.ReturnsModule;
import com.ginni.easemygst.portal.gst.GSTNResponseProcessor;
import com.ginni.easemygst.portal.gst.request.GetGstr1Req;
import com.ginni.easemygst.portal.gst.request.Gstr1SummaryReq;
import com.ginni.easemygst.portal.gst.response.GetReturnResp;
import com.ginni.easemygst.portal.gst.response.Gstr1SummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr3bSummaryResp;
import com.ginni.easemygst.portal.gst.response.ItcElg;
import com.ginni.easemygst.portal.gst.response.OsupDet;
import com.ginni.easemygst.portal.gst.response.OsupNilExmp;
import com.ginni.easemygst.portal.gst.response.OsupNongst;
import com.ginni.easemygst.portal.gst.response.OsupZero;
import com.ginni.easemygst.portal.gst.response.SectionSummary;
import com.ginni.easemygst.portal.gst.response.SupDetails;
import com.ginni.easemygst.portal.gst.response.TypeDTO;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.ReportData;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.reports.Gstr2avs3bReport;
import com.ginni.easemygst.portal.reports.Gstr3bvs1Report;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ReportServiceImpl implements ReportService {
	@Inject
	private UserService userservice;
	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private GstnService gstnService;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private GSTNResponseProcessor gstnResponseProcessor;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Inject
	private FinderService finderService;
	
	@Inject
	private RedisService redisService;
	
	@Inject
	private UserBean userbean;

	/**
	 * Calculate gstr1 ang gstr 3b data
	 * @param gstinTransactionDTO
	 * @param refresh
	 * @return
	 * @throws AppException
	 * @throws IOException
	 */
	
	
	public Map<String,Double> getreturnSummary(GstinTransactionDTO gstinTransactionDTO,boolean refresh) throws AppException, IOException {
		Map<String,Double> gstrtaxDeatilMap =new HashMap<>();
		double taxamount=0.0d;
		try{
		String gstnSummary = this.getGstrSummaryData(gstinTransactionDTO,refresh);
		switch(gstinTransactionDTO.getReturnType().toUpperCase())
		{
		case "GSTR1":
			Gstr1SummaryResp gstr1SummaryResp = new ObjectMapper().readValue(gstnSummary, Gstr1SummaryResp.class);
	
			gstnResponseProcessor.processGstnResponse(gstr1SummaryResp,gstinTransactionDTO.getTaxpayerGstin().getGstin());
			if(gstr1SummaryResp.getSecSummary()!=null)
			{
			List<SectionSummary> sectionSummary=gstr1SummaryResp.getSecSummary();
			for (SectionSummary sectionSummary2 : sectionSummary) {
                     if(!sectionSummary2.getSectionName().equalsIgnoreCase("HSN") && !sectionSummary2.getSectionName().equalsIgnoreCase("NIL")
                    		 && !sectionSummary2.getSectionName().equalsIgnoreCase("CDNR") && !sectionSummary2.getSectionName().equalsIgnoreCase("CDNUR")
                    		 ){
					if(sectionSummary2.getTotalCess()!=null){
						taxamount+=sectionSummary2.getTotalCess();
					}
					if(sectionSummary2.getTotalCgst()!=null){
						taxamount+=sectionSummary2.getTotalCgst();
					}
					if(sectionSummary2.getTotalIGST()!=null){
						taxamount+=sectionSummary2.getTotalIGST();
					}
					if(sectionSummary2.getTotalSgst()!=null){
						taxamount+=sectionSummary2.getTotalSgst();
					}

						}
						if (sectionSummary2.getSectionName().equalsIgnoreCase("CDNR")) {
						double	cdnrtaxamount = this.getcreditbebitdetail(gstinTransactionDTO, "CDNR",refresh);
						taxamount+=taxamount+cdnrtaxamount;
						}
						if (sectionSummary2.getSectionName().equalsIgnoreCase("CDNUR")) {
							double	cdnurtaxamount=this.getcreditbebitdetail(gstinTransactionDTO, "CDNUR",refresh);
							taxamount+=taxamount+cdnurtaxamount;
						}  
			}
			}
			break;
		case "GSTR3B":
			Gstr3bSummaryResp gstrSummaryResp = new ObjectMapper().readValue(gstnSummary, Gstr3bSummaryResp.class);
			gstnResponseProcessor.processGstnResponse(gstrSummaryResp,gstinTransactionDTO.getTaxpayerGstin().getGstin());
			if(gstrSummaryResp.getSupDetails()!=null){
				SupDetails supDetails=	gstrSummaryResp.getSupDetails();
				OsupDet osupDet=supDetails.getOsupDet();
				// calculate outward taxable supply 
				 Double  osupDetamt=	osupDet.getCamt()+osupDet.getCsamt()+osupDet.getIamt()+osupDet.getSamt();
				 // calculate outward taxable supply zero related
				 OsupZero osupZero= supDetails.getOsupZero();
				 Double  osupZeroamt=osupZero.getCamt()+osupZero.getCsamt()+osupZero.getIamt()+osupZero.getSamt();
				 //calculate other outward supply (Nil related)
				 OsupNilExmp osupNilExmp= supDetails.getOsupNilExmp();
				 Double  osupNilExmpamt=osupNilExmp.getCamt()+osupNilExmp.getCsamt()+osupNilExmp.getIamt()+osupNilExmp.getSamt();
				 //calculate other outward supply non -gst
				 OsupNongst osupNongst= supDetails.getOsupNongst();
				 Double  osupNongstamt=osupNongst.getCamt()+osupNongst.getCsamt()+osupNongst.getIamt()+osupNongst.getSamt();
				 taxamount =osupDetamt+osupZeroamt+osupNilExmpamt+osupNongstamt;
			}
			break;
		default:
		}
		 gstrtaxDeatilMap.put("taxamount",taxamount);
		return  gstrtaxDeatilMap;
		}
		catch( AppException ap){
			
			if((ap.getCause() instanceof javax.crypto.BadPaddingException) || ap.getCode().equalsIgnoreCase(ExceptionCode._UNAUTHORISED_GSTIN) || ap.getCode().equalsIgnoreCase(ExceptionCode._GSTN_AUTH_FAILED)){
				return gstrtaxDeatilMap;
			}
			else{
				 gstrtaxDeatilMap.put("taxamount",0.00d);
				 return  gstrtaxDeatilMap;
			}
			
		}
		
	} 
	
	public double getcreditbebitdetail(GstinTransactionDTO gstinTransactionDTO,String type, boolean refresh) throws AppException{
		double taxamount=0.00d;
		GetGstr1Req getGstr1Req = new GetGstr1Req();
		getGstr1Req.setAction(type);
		if(type.equalsIgnoreCase("CDNR"))
		getGstr1Req.setActionReq("N");
		getGstr1Req.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
		getGstr1Req.setMonthYear(gstinTransactionDTO.getMonthYear());
		GetReturnResp getGstr1Resp = new GetReturnResp();
		boolean flag=true;
		String response="";
		 String savedresp=this.getsummarydatafromtable(gstinTransactionDTO,type,refresh);
		 if(!StringUtils.isEmpty(savedresp)){
			 response=savedresp;
			 flag=false;
		 }
		 else{
	     response = gstnService.getGstr1datafromgstn(getGstr1Req);
		 }
		try {
			getGstr1Resp = JsonMapper.objectMapper.readValue(response, GetReturnResp.class);
			String status="SUBMIT";
			try{
		 status=em.createNamedQuery("GstinAction.findStatus",String.class).setParameter("particular",gstinTransactionDTO.getMonthYear()).setParameter("gstin",gstinTransactionDTO.getTaxpayerGstin().getGstin()).setParameter("returnType",gstinTransactionDTO.getReturnType()).getSingleResult();	
			}
            catch(NoResultException n){
			}
			if(!StringUtils.isEmpty(status) && flag ){
				  ReportData report=new ReportData();
					report.setReturnType(gstinTransactionDTO.getReturnType());
					report.setGstnresponsedata(response);
					report.setType(type);
					report.setMonthYear(gstinTransactionDTO.getMonthYear());
					report.setGstinId(gstinTransactionDTO.getTaxpayerGstin().getId());
					report.setStatus(status);
					crudService.create(report);	
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(type.equalsIgnoreCase("CDNR") && !Objects.isNull(getGstr1Resp.getCdnr())){
		List<CDNDetailEntity> cdnrlist=getGstr1Resp.getCdnr();
		double creditamt= cdnrlist.stream().filter(a->a.getNoteType().equalsIgnoreCase("C")).mapToDouble(a -> a.getItems().stream().mapToDouble(b->b.getCess()+b.getCgst()+b.getIgst()+b.getSgst()).sum()).sum();
		double debitamtamt= cdnrlist.stream().filter(a->a.getNoteType().equalsIgnoreCase("D")).mapToDouble(a -> a.getItems().stream().mapToDouble(b->b.getCess()+b.getCgst()+b.getIgst()+b.getSgst()).sum()).sum();
		return debitamtamt-creditamt;
		}
		if(type.equalsIgnoreCase("CDNUR") && !Objects.isNull(getGstr1Resp.getCdnur())){
			List<CDNURDetailEntity> cdnrlist=getGstr1Resp.getCdnur();
			double creditamt= cdnrlist.stream().filter(a->a.getNoteType().equalsIgnoreCase("C")).mapToDouble(a -> a.getItems().stream().mapToDouble(b->b.getCess()+b.getCgst()+b.getIgst()+b.getSgst()).sum()).sum();
			double debitamtamt= cdnrlist.stream().filter(a->a.getNoteType().equalsIgnoreCase("D")).mapToDouble(a -> a.getItems().stream().mapToDouble(b->b.getCess()+b.getCgst()+b.getIgst()+b.getSgst()).sum()).sum();
			return debitamtamt-creditamt;
			}
		return taxamount;
	}


	public GstinComparisonDTO genrateSumarry(String gstin,String fyear,boolean refresh) throws AppException, IOException {
		   Map<String, Object> response = new HashMap<>();
	       List<String> monthYears=CalanderCalculatorUtility.getmonthYears(fyear);
/*	   	List<String> monthYears = Arrays.asList("112017");*/
	       TaxpayerGstinDTO taxpayerGstinDTO = (TaxpayerGstinDTO) EntityHelper.convert(finderService.findTaxpayerGstin(gstin),
					TaxpayerGstinDTO.class);
	        response.put("monthYears",monthYears);
	    	GstinTransactionDTO gstinTransactionDTOGSTR1 = new GstinTransactionDTO();
			gstinTransactionDTOGSTR1.setTaxpayerGstin(taxpayerGstinDTO);
			gstinTransactionDTOGSTR1.setReturnType("GSTR1");
			GstinTransactionDTO gstinTransactionDTOGSTR3B = new GstinTransactionDTO();
			gstinTransactionDTOGSTR3B.setTaxpayerGstin(taxpayerGstinDTO);
			gstinTransactionDTOGSTR3B.setReturnType("GSTR3B");
			List<ComparisonDTO> comparisonDTOlist=new ArrayList<>();
		    GstinComparisonDTO gstinComparisonDTO=new GstinComparisonDTO();
		   	gstinComparisonDTO.setGstin(gstin);
			gstinComparisonDTO.setState(AppConfig.getStateNameByCode(gstin.substring(0,2)).getName());
			for (String month : monthYears) {
				gstinTransactionDTOGSTR1.setMonthYear(month);
				gstinTransactionDTOGSTR3B.setMonthYear(month);
				
				Map<String, Double>	 gstrtaxDeatilMap1=this.getreturnSummary(gstinTransactionDTOGSTR1,refresh);
				Map<String, Double>	 gstrtaxDeatilMap3B= this.getreturnSummary(gstinTransactionDTOGSTR3B,refresh);
				if(gstrtaxDeatilMap1.containsKey("taxamount") && gstrtaxDeatilMap3B.containsKey("taxamount") )
				{
		     	double  taxamout1= gstrtaxDeatilMap1.get("taxamount");
		   	    double 	taxamout3B= gstrtaxDeatilMap3B.get("taxamount");
		      	comparisonDTOlist.add(new ComparisonDTO(month,taxamout1,taxamout3B));
				}
				else{
					 gstinComparisonDTO.setAuthReq(true);
					return gstinComparisonDTO;
				}
				 }
		   	gstinComparisonDTO.setComparisonDTOlist(comparisonDTOlist);
		return gstinComparisonDTO;
	}
	
	@Override
	public Map<String, Object> get1VS3breport(String panCard,String fyear,boolean refresh,String refGstin) throws AppException, IOException{
		Map<String, Object> gstinDetail = new HashMap<>();
		List<String> monthYears=CalanderCalculatorUtility.getmonthYears(fyear);
	/*	List<String> monthYears = Arrays.asList("112017");*/
	     List<String> gstinlist=  userservice.getGstinbyPanCard(panCard); 
	     List<GstinComparisonDTO> gstinComparisonDTOlist=new ArrayList<>();
	     List<ComparisonDTO> comparisonDTOlsit=new ArrayList<>();
	     List<ComparisonDTO> pancomparisonDTOlsit=new ArrayList<>();
	     gstinDetail.put("gstinlist",gstinlist);
	     //
	     boolean flag=refresh;
	     for (String gstin : gstinlist) {
	    	 if(refresh  && ! gstin.equalsIgnoreCase(refGstin))
	    		 refresh=false;
	    	 GstinComparisonDTO gstinComparisonDTO=this.genrateSumarry(gstin,fyear,refresh);
	    	 gstinComparisonDTO.setState(AppConfig.getStateNameByCode(gstin.substring(0,2)).getName());
	    	 if(!gstinComparisonDTO.isAuthReq())
	    		 comparisonDTOlsit.addAll(gstinComparisonDTO.getComparisonDTOlist());
	    	 gstinComparisonDTOlist.add(gstinComparisonDTO);
	    	 refresh=flag;
		}
		for (String monthYear : monthYears) {
			BigDecimal taxesAsPerGSTR1 = BigDecimal.ZERO, outwardTaxesandSupplies =BigDecimal.ZERO;
			List<ComparisonDTO> list = comparisonDTOlsit.stream()
					.filter(a -> a.getMonthYear().equalsIgnoreCase(monthYear)).collect(Collectors.toList());
			for (ComparisonDTO data : list) {
				taxesAsPerGSTR1=taxesAsPerGSTR1.add(data.getTaxesAsPerGSTR1());
				outwardTaxesandSupplies=outwardTaxesandSupplies.add(data.getOutwardTaxesandSupplies());
			}
			pancomparisonDTOlsit.add(new ComparisonDTO(monthYear, taxesAsPerGSTR1.doubleValue(), outwardTaxesandSupplies.doubleValue()));
		}
	    ComparisonDTO overall=   new ComparisonDTO("all",	     
	    pancomparisonDTOlsit.parallelStream().mapToDouble(a->a.getTaxesAsPerGSTR1().doubleValue()).sum(),
	    pancomparisonDTOlsit.parallelStream().mapToDouble(a->a.getOutwardTaxesandSupplies().doubleValue()).sum());			    
		gstinDetail.put("tinDetail", gstinComparisonDTOlist);
		gstinDetail.put("panDetail", pancomparisonDTOlsit);
		gstinDetail.put("overall", overall);
		String redisKey=panCard+"-"+fyear;
		redisService.setReport(redisKey,gstinDetail, true, 7200);
		return  gstinDetail;
	}
	
	public String getsummarydatafromtable(GstinTransactionDTO gstinTransactionDTO, String type,boolean refresh) {
		String response = "";
		try {
			if(refresh){
			response = em.createNamedQuery("ReportData.findgstnresponsedata", String.class)
					.setParameter("monthYear", gstinTransactionDTO.getMonthYear())
					.setParameter("returnType", gstinTransactionDTO.getReturnType())
					.setParameter("gstin", gstinTransactionDTO.getTaxpayerGstin().getId()).setParameter("type", type)
					.getSingleResult();
			}
			else{
				response = em.createNamedQuery("ReportData.findallgstnresponsedata", String.class)
						.setParameter("monthYear", gstinTransactionDTO.getMonthYear())
						.setParameter("returnType", gstinTransactionDTO.getReturnType())
						.setParameter("gstin", gstinTransactionDTO.getTaxpayerGstin().getId()).
						setParameter("type", type).	setMaxResults(1).getSingleResult();
				
			}
		} catch (NoResultException n) {
		}
		return response;
	}
	/**
	 * 
	 * Get data from gstn
	 * @param gstinTransactionDTO
	 * @param refresh
	 * @return
	 * @throws AppException
	 * @throws IOException
	 */
	
	@SuppressWarnings("unused")
	public String getGstrSummaryData(GstinTransactionDTO gstinTransactionDTO,boolean refresh)
			throws AppException, IOException {

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {
			Gstr1SummaryReq gstr1SummaryReq = new Gstr1SummaryReq();
			gstr1SummaryReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			gstr1SummaryReq.setReturnPeriod(gstinTransactionDTO.getMonthYear());
	        String savedresp=this.getsummarydatafromtable(gstinTransactionDTO,"all",refresh);
	        if(!StringUtils.isEmpty(savedresp))
	        	return savedresp;
	        String status="SUBMIT";
			try{
			 status=em.createNamedQuery("GstinAction.findStatus",String.class).setParameter("particular",gstinTransactionDTO.getMonthYear()).setParameter("gstin",gstinTransactionDTO.getTaxpayerGstin().getGstin()).setParameter("returnType",gstinTransactionDTO.getReturnType()).getSingleResult();
			}
			catch(NoResultException n){
				status="SUBMIT";
			}
			String response = gstnService.getGstrSummary(gstr1SummaryReq,ReturnsModule.valueOf(gstinTransactionDTO.getReturnType()));
	    	ReportData report=new ReportData();
			report.setReturnType(gstinTransactionDTO.getReturnType());
			report.setGstnresponsedata(response);
			report.setType("all");
			report.setMonthYear(gstinTransactionDTO.getMonthYear());
			report.setGstinId(gstinTransactionDTO.getTaxpayerGstin().getId());
			report.setStatus(status);
			crudService.create(report);
			return response;
		/*	}*/
			
	
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	

	public Map<String,Double> get3bdetailformGSTN(GstinTransactionDTO gstinTransactionDTO, boolean refresh) throws AppException, IOException {
		Map<String,Double> gstr3bDeatilMap=new HashMap<>();
		try{
		String gstnSummary = this.getGstrSummaryData(gstinTransactionDTO,refresh);
		double allotherITC=0.0d;
		double ITCRevers=0.0d;
		double ITCInelg=0.0d;
			Gstr3bSummaryResp gstrSummaryResp = new ObjectMapper().readValue(gstnSummary, Gstr3bSummaryResp.class);
			gstnResponseProcessor.processGstnResponse(gstrSummaryResp,gstinTransactionDTO.getTaxpayerGstin().getGstin());

			if(gstrSummaryResp.getSupDetails()!=null && gstrSummaryResp.getItcElg()!=null){
				log.debug(gstinTransactionDTO.getTaxpayerGstin().getGstin() +" "+gstinTransactionDTO.getMonthYear());
				ItcElg ItcElg=	gstrSummaryResp.getItcElg();
				List<TypeDTO> itcavldetail  =	ItcElg.getItcAvl();
				List<TypeDTO> itcRevdetail  =	ItcElg.getItcRev();
				List<TypeDTO> itcInelgdetail  =   ItcElg.getItcInelg();
				 for (TypeDTO data : itcInelgdetail) {
				
					 ITCInelg=data.getCamt()+data.getCsamt()+data.getIamt()+data.getSamt();	
					 
				}
				 if(!Objects.isNull(itcavldetail)){
				for (TypeDTO data : itcavldetail) {
					if(data.getTy().equalsIgnoreCase("OTH"))
					allotherITC=data.getCamt()+data.getCsamt()+data.getIamt()+data.getSamt();
				}
				 }
				
                 for (TypeDTO data : itcRevdetail) {
                	 ITCRevers+=data.getCamt()+data.getCsamt()+data.getIamt()+data.getSamt();
                	 
				   }

			}
			gstr3bDeatilMap.put("allotherITC",allotherITC);
			gstr3bDeatilMap.put("ITCRevers",ITCRevers);
			gstr3bDeatilMap.put("ITCInelg",ITCInelg);
			

		return gstr3bDeatilMap;
		}
           catch( AppException ap){
			
        	   if((ap.getCause() instanceof javax.crypto.BadPaddingException) || ap.getCode().equalsIgnoreCase(ExceptionCode._UNAUTHORISED_GSTIN) || ap.getCode().equalsIgnoreCase(ExceptionCode._GSTN_AUTH_FAILED)){
   				return gstr3bDeatilMap;
   			}
			else{
				gstr3bDeatilMap.put("allotherITC",0.00d);
				gstr3bDeatilMap.put("ITCRevers",0.00d);
				gstr3bDeatilMap.put("ITCInelg",0.00d);
				 return  gstr3bDeatilMap;
			}
			
		}   
	} 

	public Double get2Ataxamount(String monthYear, String gstin, String returnType) {

		Double taxamount = 0.0D;

		Object[] objectArray = em.createNamedQuery("B2bDetailEntity.findCtinSummary", Object[].class)
				.setParameter("monthYear", monthYear).setParameter("gstin", gstin)
				.setParameter("returnType", returnType).getSingleResult();
		taxamount += Double.valueOf(String.valueOf(objectArray[1]) != "null" ? String.valueOf(objectArray[1]) : "0.00");

		Object[] creditobjectArray = em.createNamedQuery("CdnDetailEntity.findSummaryByctin", Object[].class)
				.setParameter("monthYear", monthYear).setParameter("gstin", gstin)
				.setParameter("returnType", returnType).setParameter("notetype", "C").getSingleResult();
		taxamount -= Double.valueOf(
				String.valueOf(creditobjectArray[1]) != "null" ? String.valueOf(creditobjectArray[1]) : "0.00");

		Object[] debitobjectArray = em.createNamedQuery("CdnDetailEntity.findSummaryByctin", Object[].class)
				.setParameter("monthYear", monthYear).setParameter("gstin", gstin)
				.setParameter("returnType", returnType).setParameter("notetype", "D").getSingleResult();
		taxamount += Double
				.valueOf(String.valueOf(debitobjectArray[1]) != "null" ? String.valueOf(debitobjectArray[1]) : "0.00");
		return taxamount;
	}
	
	
    	public GstinComparison2aVS3bDTO getgstindeatil(String gstin, List<String> monthYears,String fyear, boolean refresh)
			throws AppException, IOException {
		 List<Comparison2avs3bDTO> Comparison2avs3bDTOlist=new ArrayList<>();
	     GstinComparison2aVS3bDTO gstinComparison2aVS3bDTO=new GstinComparison2aVS3bDTO();
	     gstinComparison2aVS3bDTO.setGstin(gstin);
	     GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
	     TaxpayerGstinDTO taxpayerGstinDTO = (TaxpayerGstinDTO) EntityHelper.convert(finderService.findTaxpayerGstin(gstin),
					TaxpayerGstinDTO.class);
	     gstinTransactionDTO.setTaxpayerGstin(taxpayerGstinDTO);
	     gstinTransactionDTO.setReturnType("GSTR3B");
	     for (String month : monthYears) {
	    	 gstinTransactionDTO.setMonthYear(month);
	          Double inwardtaxamount=this.get2Ataxamount(month,gstin,"gstr2");
	    	  Map<String, Double> gst3bDetailMap=this.get3bdetailformGSTN(gstinTransactionDTO,refresh);
	    	 if(gst3bDetailMap.containsKey("allotherITC") && gst3bDetailMap.containsKey("ITCRevers") && gst3bDetailMap.containsKey("ITCInelg")){
	          Comparison2avs3bDTO cdto=new Comparison2avs3bDTO(month,gst3bDetailMap.get("allotherITC"),gst3bDetailMap.get("ITCRevers"),gst3bDetailMap.get("ITCInelg"),inwardtaxamount);
	          Comparison2avs3bDTOlist.add(cdto);
	    	 }
	    	 else{
	    		 gstinComparison2aVS3bDTO.setAuthReq(true);
	    		 return  gstinComparison2aVS3bDTO; 
	    	 }
		}
	     gstinComparison2aVS3bDTO.setComparison2avs3bDTOlist(Comparison2avs3bDTOlist);
		return  gstinComparison2aVS3bDTO;
	}
	
	@Override
	public Map<String, Object> get2aVS3breport(String panCard,String fyear,boolean refresh) throws AppException, IOException{

		Map<String, Object> gstinDetail = new HashMap<>();
		List<String> monthYears = CalanderCalculatorUtility.getmonthYears(fyear);
	/*	List<String> monthYears = Arrays.asList("082017");*/
		List<String> gstinlist = userservice.getGstinbyPanCard(panCard);
		List<GstinComparison2aVS3bDTO> gstinComparison2aVS3bDTOlist = new ArrayList<>();
		List<Comparison2avs3bDTO> comparison2avs3bDTOlsit = new ArrayList<>();
		List<Comparison2avs3bDTO> pancomparison2avs3bDTOlsit = new ArrayList<>();
		gstinDetail.put("gstinlist", gstinlist);
		for (String gstin : gstinlist) {
			GstinComparison2aVS3bDTO gstinComparison2aVS3bDTO = this.getgstindeatil(gstin, monthYears,fyear,refresh);
			gstinComparison2aVS3bDTO.setState(AppConfig.getStateNameByCode(gstin.substring(0, 2)).getName());
			if (!gstinComparison2aVS3bDTO.isAuthReq())
				comparison2avs3bDTOlsit.addAll(gstinComparison2aVS3bDTO.getComparison2avs3bDTOlist());
			gstinComparison2aVS3bDTOlist.add(gstinComparison2aVS3bDTO);
			
			}
		for (String monthYear : monthYears) {
			double allotherITC = 0.0, ITCRevers = 0.0, ITCInelg = 0.0, inwardtaxamount = 0.0d;
			List<Comparison2avs3bDTO> list = comparison2avs3bDTOlsit.stream()
					.filter(a -> a.getMonthYear().equalsIgnoreCase(monthYear)).collect(Collectors.toList());
			for (Comparison2avs3bDTO data : list) {
				allotherITC += data.getAllotherITC().doubleValue();
				ITCRevers += data.getITCReverseinGSTR3B().doubleValue();
				ITCInelg += data.getIneligibleITCasperGSTR3B().doubleValue();
				inwardtaxamount += data.getInwarddetailsasperGSTR2A().doubleValue();
			}
			pancomparison2avs3bDTOlsit
					.add(new Comparison2avs3bDTO(monthYear, allotherITC, ITCRevers, ITCInelg, inwardtaxamount));
		}
		Comparison2avs3bDTO overall = new Comparison2avs3bDTO("all",
				pancomparison2avs3bDTOlsit.parallelStream().mapToDouble(a -> a.getAllotherITC().doubleValue()).sum(),
				pancomparison2avs3bDTOlsit.parallelStream().mapToDouble(a -> a.getITCReverseinGSTR3B().doubleValue()).sum(),
				pancomparison2avs3bDTOlsit.parallelStream().mapToDouble(a -> a.getIneligibleITCasperGSTR3B().doubleValue()).sum(),
				pancomparison2avs3bDTOlsit.parallelStream().mapToDouble(a -> a.getInwarddetailsasperGSTR2A().doubleValue())
						.sum());
		gstinDetail.put("tinDetail", gstinComparison2aVS3bDTOlist);
		gstinDetail.put("panDetail", pancomparison2avs3bDTOlsit);
		gstinDetail.put("overall", overall);
		String redisKey=panCard+"-"+fyear+"-2avs3b";
		redisService.setReport(redisKey,gstinDetail, true, 7200);
		
		return gstinDetail;
	}

	@Override
	public String generate1vs3breportPdf(String panCard,String fyear,String fileType) {
		String redisKey=panCard+"-"+fyear;
		Map<String, Object> reportdata=new HashMap<>();
		reportdata=	redisService.getReport(redisKey, reportdata);
	    String legalName=finderService.findLegalName(panCard);
		reportdata.put("company",legalName);
		reportdata.put("panNo",panCard);
	    String fullName=userbean.getUserDto().getFirstName()+" "+userbean.getUserDto().getLastName(); 
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		String formattedDate = LocalDateTime.now().format(formatter); 
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm");
		String formattedtime = LocalDateTime.now().format(formatter2); 
		reportdata.put("date",formattedDate +" , "+ formattedtime);
		reportdata.put("username",String.format("Report viewed by %s as on %s at %s",fullName,formattedDate,formattedtime));
		return Gstr3bvs1Report.generatereport(reportdata,fileType) ;
	}
	

	@Override
	public String generate2avs3breportPdf(String panCard,String fyear,String fileType) {
		String redisKey=panCard+"-"+fyear+"-2avs3b";
		Map<String, Object> reportdata=new HashMap<>();
		reportdata=	redisService.getReport(redisKey, reportdata);
	    String legalName=finderService.findLegalName(panCard);
		reportdata.put("company",legalName);
		reportdata.put("panNo",panCard);
	    String fullName=userbean.getUserDto().getFirstName()+" "+userbean.getUserDto().getLastName(); 
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		String formattedDate = LocalDateTime.now().format(formatter); 
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm");
		String formattedtime = LocalDateTime.now().format(formatter2); 
		reportdata.put("date",formattedDate +" , "+ formattedtime);
		reportdata.put("username",String.format("Report viewed by %s as on %s at %s",fullName,formattedDate,formattedtime));
		return Gstr2avs3bReport.generatereport(reportdata,fileType) ;
	}
	
	
}