package com.ginni.easemygst.portal.business.entity;

import java.sql.Timestamp;

import lombok.Data;

public @Data class ErpTransactionDTO {

	private int id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String monthYear;

	private String remark;

	private String returnType;

	private String status;
	
	private byte isChanged;

	private String transactionObject;

	private String transactionType;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	private TaxpayerGstinDTO taxpayerGstin;
	
	public interface ErpTransactionDTOMixIn extends CommonMixin {
		
	}
	
}
