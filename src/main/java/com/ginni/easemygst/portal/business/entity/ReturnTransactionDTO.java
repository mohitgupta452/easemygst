package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class ReturnTransactionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;

	private byte active;

	private String code;


	private String description;

	private String name;
	
	private String createdBy;


	
}