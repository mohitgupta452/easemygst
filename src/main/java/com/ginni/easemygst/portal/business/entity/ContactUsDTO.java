package com.ginni.easemygst.portal.business.entity;

import java.sql.Timestamp;

import lombok.Data;

public @Data class ContactUsDTO {
	
	private Integer id;

	private String emailAddress;

	private String firstName;

	private String fullName;

	private String lastName;

	private String message;

	private String mobileNumber;

	private String remark;

	private String title;
	
	private String state;

	private String city;

	private String legalName;

	private String annualTurnover;
	
	private Timestamp creationTime;
	
	private String source;

	
}
