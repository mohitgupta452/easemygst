
package com.ginni.easemygst.portal.business.service;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.BusinessTypeDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.HelpContentDTO;
import com.ginni.easemygst.portal.business.entity.HsnSacDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface MasterService {

	public GstReturnDTO createGstReturn(GstReturnDTO gstReturnDTO) throws AppException;

	public GstReturnDTO updateGstReturn(GstReturnDTO gstReturnDTO) throws AppException;

	public GstReturnDTO deleteGstReturn(int id) throws AppException;
	
	public BusinessTypeDTO createBusinessType(BusinessTypeDTO businessTypeDTO) throws AppException;

	public BusinessTypeDTO updateBusinessType(BusinessTypeDTO businessTypeDTO) throws AppException;

	public BusinessTypeDTO deleteBusinessType(int id) throws AppException;

	public ReturnTransactionDTO createReturnTransaction(ReturnTransactionDTO returnTransactionDTO) throws AppException;

	public ReturnTransactionDTO updateReturnTransaction(ReturnTransactionDTO returnTransactionDTO) throws AppException;

	public ReturnTransactionDTO deleteReturnTransaction(int id) throws AppException;
	
	public HelpContentDTO getHelpContent(String id) throws AppException;
	
	public boolean saveHelpContent(HelpContentDTO helpContentDTO) throws AppException;
	
	public List<HsnSacDTO> getHsnSacContent(String type, String code) throws AppException;
	
	public String searchHsnCode(String searchKey) throws AppException;
	
	public enum ReturnType {

		GSTR1("GSTR1"), GSTR2("GSTR2"), GSTR1A("GSTR1A"), GSTR3("GSTR3"), GSTR4("GSTR4"),
		GSTR5("GSTR5"), GSTR6("GSTR6"), GSTR7("GSTR7"), GSTR8("GSTR8"), GSTR9("GSTR9"),
		GSTR10("GSTR10"), GSTR11("GSTR11"),GSTR2A("GSTR2A"), GSTR3B("GSTR3B"),GSTR("GSTR"),;

		ReturnType(String name) {
			this.name = name;
		}

		private String name;

		@Override
		public String toString() {
			return this.name;
		};
	}
	
	public enum TransactionType {

		B2B("B2B"),B2BUR("B2BUR"), B2CL("B2CL"), B2CS("B2CS"), CDN("CDN"), AT("AT"),
		NIL("NIL"), EXP("EXP"), HSNSUM("HSNSUM"), INVSUM("INVSUM"),CDNUR("CDNUR"),
		REVCH("REVCH"),IMPG("IMPG"),IMPS("IMPS"),ITCR("ITCR"),ISD("ISD"),TDSTCS("TDSTCS"),
		TLD("TLD"),TXPD("TXPD"),DOC_ISSUE("DOC_ISSUE"),ALL("ALL"),B2BA("B2BA"),B2CLA("B2CLA"),
		B2CSA("B2CSA"),CDNA("CDNA"),EXPA("EXPA"),CDNURA("CDNURA"),ATA("ATA"),TXPDA("TXPDA");

		TransactionType(String type) {
			this.type = type;
		}
		
		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}
	
	public List<TransactionType> GSTR1_TRANSACTIONS=Arrays.asList(TransactionType.B2B,TransactionType.B2BUR,TransactionType.B2CL,TransactionType.B2CS,
			TransactionType.CDN,TransactionType.AT,TransactionType.NIL,TransactionType.EXP,TransactionType.HSNSUM,TransactionType.CDNUR,
			TransactionType.TXPD,TransactionType.DOC_ISSUE,TransactionType.B2BA,TransactionType.B2CLA,TransactionType.B2CSA,
			TransactionType.CDNA,TransactionType.EXPA,TransactionType.CDNURA,TransactionType.ATA,TransactionType.TXPDA);
	
	

	
}
