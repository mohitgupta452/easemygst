package com.ginni.easemygst.portal.business.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.entity.CommonMixin;
import com.ginni.easemygst.portal.data.part.CreditDetails;
import com.ginni.easemygst.portal.data.part.RFCLM;

import lombok.Data;

public @Data class Gstr3Dto {
	
	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String ret_period;
	
	@JsonProperty("tod")
	private List<TodDto> turnover;
	
	@JsonProperty("out_sup")
	private List<OutsInsSupplyDto> outward_supply;
	
	@JsonProperty("in_sup")
	private List<OutsInsSupplyDto> inward_supply;

	@JsonProperty("total_txl")
	private List<TotalTaxLiabilityDto> total_tax_liabilty;

	@JsonProperty("tcs_cr")
	private List<CreditDetails> tcs_credit;
	
	@JsonProperty("tds_cr")
	private List<CreditDetails> tds_credit;
	
	@JsonProperty("itc_cr")
	private List<CreditDetails> itc_credit;
	
	@JsonProperty("tpm")
	private TaxPaidDto tax_paid;
	
	@JsonProperty("rf_clm")
	private RFCLM refund_claim;
	
public interface Gstr3DtoMixIn extends CommonMixin {
		
	}

}
