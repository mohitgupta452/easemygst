package com.ginni.easemygst.portal.business.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.Interval;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.ActionLogDto;
import com.ginni.easemygst.portal.business.dto.NotificationCount;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.NotificationService;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.DataFilter;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.ActionLog;
import com.ginni.easemygst.portal.persistence.entity.Publicnotification;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;

@Stateless
@Local
public class NotificationServiceImpl implements NotificationService{
	
	@Inject 
	private Logger log;
	
	@Inject
	private CrudService crudService;
	
	@Inject
	private UserBean userBean;
	
	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;
	
	@Override
	public int updateNotificationAction(String action, String notificationId) throws AppException {
		int id = -1;

		try {
			int notifiId = Integer.parseInt(notificationId);
			ActionLog actionLog = crudService.find(ActionLog.class, notifiId);
			if (Objects.isNull(actionLog)) {
				AppException ae = new AppException();
				ae.setMessage("Invalid notification Id");
				throw ae;
			} else {
				if ("DEACTIVE".equalsIgnoreCase(action)) {
					actionLog.setActive(false);
					actionLog.setSeen(true);
					id = actionLog.getId();
				}
				if ("SEEN".equalsIgnoreCase(action)) {
					actionLog.setSeen(true);
					id = actionLog.getId();
				}
				crudService.update(actionLog);
			}
		} catch (Exception e) {
			AppException ae = new AppException();
			ae.setMessage("invalid notification id provided");
			throw ae;
		}
		return id;
	}

	@SuppressWarnings("unchecked")
	@Override

	public Map<String,Object> getNotificationCount(List<String> categories ,List<Boolean>isSeens)throws AppException{
		Map<String,Object>countMap=new HashMap<>();

		try{
		UserDTO userDto=userBean.getUserDto();
		NotificationCount counts=new NotificationCount();
		//counts.setCount(emgCount);
		counts.setCategory(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString());
		countMap.put(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString(), counts);
		categories.add(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString());
		long count = 0;
		isSeens.add(Boolean.FALSE);
		count = crudService.findResultCounts("ActionLog.findUnseenCountByOrganizationIdAndCategory",
				QueryParameter.with("orgId", userDto.getOrganisation().getId()).and("categories", categories)
						.and("isSeens", isSeens).parameters());
		counts.setCount(count);
		}
		catch(Exception e){
			log.error("error occured in getNotificationCount method {}",e);
			countMap.put(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString(), 0);
		}

		return countMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getNotifications(UserDTO userDto, List<Boolean> isSeens, List<String> categories)
			throws AppException {

		isSeens.add(Boolean.FALSE);
		isSeens.add(Boolean.TRUE);
		Map<String, Object> respMap = new HashMap<>();
		respMap.put("notifications", "[]");
		respMap.put("count", 0);
		// categories.add(NotificationCategory.EMGST_NOTIFICATION.toString());

		if (categories.contains(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString())) {
			List<ActionLog> actionNotifications = crudService.findPaginatedResults(
					"ActionLog.findByOrganizationIdAndCategory",
					QueryParameter.with("orgId", userDto.getOrganisation().getId()).and("categories", categories)
							.and("isSeens", isSeens).parameters(),
					1, 5);

			List<ActionLogDto> processedNotifications = this.processNotificationForView(actionNotifications);
			if (!Objects.isNull(actionNotifications)) {
				for (ActionLog action : actionNotifications) {
					if (action.isSeen() == false) {
						action.setSeen(true);
						crudService.update(action);
					}
				}
			}
			respMap.put("notifications", processedNotifications);
			respMap.put("count", 0);
			// return processedNotifications;
		} else {
			   Map<String,Object> parameters =new HashMap<>();
				parameters.put("category",categories.get(0));
				List<Publicnotification> publicNotifications = crudService
						.findPaginatedResults("Publicnotification.findBycategory",parameters, 1, 2);
				List<ActionLogDto> processedNotifications = this.processPublicNotificationForView(publicNotifications);
				respMap.put("notifications", processedNotifications);
				respMap.put("count", 0);
			// return processedNotifications;
		}

		return respMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getNotificationsAll(UserDTO userDto, List<Boolean> isSeens, List<String> categories,
			FilterDto filter) throws AppException {
		Map<String, Object> respMap = new HashMap<>();

		if (categories.contains(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString())) {

			List<ActionLog> actionNotifications = crudService.findPaginatedResults(
					"ActionLog.findByOrganizationIdAndCategoryAll",
					QueryParameter.with("orgId", userDto.getOrganisation().getId()).and("categories", categories)
							.parameters(),
					filter.getPageNo(), filter.getSize());

			long count = crudService.findResultCounts("ActionLog.findByOrganizationIdAndCategoryAllCount",
					QueryParameter.with("orgId", userDto.getOrganisation().getId()).and("categories", categories)
							.parameters());

			List<ActionLogDto> processedNotifications = this.processNotificationForView(actionNotifications);

			List<Integer> actionId=new ArrayList<>();
			
			if (!Objects.isNull(actionNotifications)) {
				for (ActionLog action : actionNotifications) {
					if (action.isSeen() == false) {
						action.setSeen(true);
						crudService.update(action);
						
						
					}
				}
			}
			
			 /*CriteriaBuilder cb = this.em.getCriteriaBuilder();

		        // create update
		        CriteriaUpdate<ActionLog> update = cb.
		        createCriteriaUpdate(ActionLog.class);

		        // set the root class
		        Root e = update.from(ActionLog.class);
		        update.set("isSeen",true);
		        update.subquery();*/
			respMap.put("notifications", processedNotifications);
			respMap.put("count", count);

		} else {
			 Map<String,Object> parameters =new HashMap<>();
				parameters.put("category",categories.get(0));
			List<Publicnotification> publicNotifications = crudService
					.findPaginatedResults("Publicnotification.findBycategory",parameters, filter.getPageNo(), filter.getSize());
		/*	long count = crudService.findResultCounts("Publicnotification.countAll", null);*/

			List<ActionLogDto> processedNotifications = this.processPublicNotificationForView(publicNotifications);
			respMap.put("notifications", processedNotifications);
			respMap.put("count", publicNotifications.size());

		}
		if (respMap.containsKey("notifications") && Objects.isNull(respMap.get("notifications")))
			respMap.put("notifications", "[]");
		if (respMap.containsKey("count") && Objects.isNull(respMap.get("count")))
			respMap.put("count", 0);

		return respMap;
	}

	@Override
	public Map<String, Object> getFilteredNotificationsAll(UserDTO userDto, List<Boolean> isSeens,
			List<String> categories, FilterDto filter) throws AppException {
		Map<String, Object> respMap = new HashMap<>();

		if (categories.contains(NotificationService.NotificationCategory.EMGST_NOTIFICATION.toString())) {

			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<ActionLog> cq = cb.createQuery(ActionLog.class);
			Root<ActionLog> root = cq.from(ActionLog.class);
			Predicate[] predicates = this.getPredicatesForNotification(cb, root, userDto, isSeens, categories, filter);
			cq.select(root).where(predicates).orderBy(cb.desc(root.get("actionTime")));
			List<ActionLog> actionNotifications = em.createQuery(cq)
					.setFirstResult((filter.getPageNo() - 1) * filter.getSize()).setMaxResults(filter.getSize())
					.getResultList();
			long count = this.getFilteredNotificationCount(cb, ActionLog.class, Arrays.asList(predicates));

			List<ActionLogDto> processedNotifications = this.processNotificationForView(actionNotifications);

			if (!Objects.isNull(actionNotifications)) {
				for (ActionLog action : actionNotifications) {
					if (action.isSeen() == false) {
						action.setSeen(true);
						crudService.update(action);
					}
				}
			}

			respMap.put("notifications", processedNotifications);
			respMap.put("count", count);

		} else {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Publicnotification> cq = cb.createQuery(Publicnotification.class);
			Root<Publicnotification> root = cq.from(Publicnotification.class);
			Predicate[] predicates = this.getPredicatesForPublicNotification(cb, root, userDto, isSeens, categories,
					filter);
			cq.select(root).where(predicates).orderBy(cb.desc(root.get("id")));
			List<Publicnotification> publicNotifications = em.createQuery(cq)
					.setFirstResult((filter.getPageNo() - 1) * filter.getSize()).setMaxResults(filter.getSize())
					.getResultList();
			long count = this.getFilteredNotificationCount(cb, Publicnotification.class, Arrays.asList(predicates));

			List<ActionLogDto> processedNotifications = this.processPublicNotificationForView(publicNotifications);
			respMap.put("notifications", processedNotifications);
			respMap.put("count", count);

		}
		if (respMap.containsKey("notifications") && Objects.isNull(respMap.get("notifications")))
			respMap.put("notifications", "[]");
		if (respMap.containsKey("count") && Objects.isNull(respMap.get("count")))
			respMap.put("count", 0);

		return respMap;
	}

	private <T> long getFilteredNotificationCount(CriteriaBuilder cb, Class<T> type, List<Predicate> predicates) {
		CriteriaQuery<Long> countq = cb.createQuery(Long.class);
		countq.select(cb.count(countq.from(type)));
		em.createQuery(countq);
		countq.where(predicates.toArray(new Predicate[] {}));
		Long filterCount = em.createQuery(countq).getSingleResult();
		if (Objects.nonNull(filterCount))
			return filterCount;
		else
			return 0;
	}

	private Predicate[] getPredicatesForNotification(CriteriaBuilder cb, Root root, UserDTO userDto,
			List<Boolean> isSeens, List<String> categories, FilterDto filter) {
		List<Predicate> predicates = new ArrayList<>();
		Predicate pOrgId = cb.equal(root.get("organizationId"), userDto.getOrganisation().getId());
		Predicate pCategories = root.get("category").in(categories);
		predicates.add(pOrgId);
		predicates.add(pCategories);
		Map<String, DataFilter> filters = filter.getFilterDetail();

		if (!Objects.isNull(filters) && filter.isActive()) {
			for (Map.Entry<String, DataFilter> entry : filters.entrySet()) {
				DataFilter value = entry.getValue();
				Predicate p = null;
				if ("notificationTime".equalsIgnoreCase(entry.getKey())) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Date from = null;
					Date to = null;

					if ("date".equalsIgnoreCase(value.getBy())) {
						try {
							from = sdf.parse(value.getFromDate());
							to = sdf.parse(value.getToDate());

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if ("days".equalsIgnoreCase(value.getBy())) {
						LocalDate currentDate = LocalDate.now();
						int days = Integer.parseInt(value.getNoOfDays());
						from = Date.from(currentDate.minusDays(days).atStartOfDay(ZoneId.systemDefault()).toInstant());
						to = new Date();
					}
					predicates.add(cb.and(cb.greaterThan(root.get("actionTime"), from),
							cb.lessThan(root.get("actionTime"), to)));

				} else if ("gstin".equalsIgnoreCase(entry.getKey())) {
					predicates.add(cb.equal(root.get("gstin"), value.getGstin()));
				} else if ("user".equalsIgnoreCase(entry.getKey())) {
					predicates.add(cb.like(root.get("userId"), "%" + value.getUserName() + "%"));
				}
			}
		}

		return predicates.toArray(new Predicate[] {});

		// return predicates;

	}

	private Predicate[] getPredicatesForPublicNotification(CriteriaBuilder cb, Root root, UserDTO userDto,
			List<Boolean> isSeens, List<String> categories, FilterDto filter) {
		List<Predicate> predicates = new ArrayList<>();

		Predicate pCategories = root.get("category").in(categories);
		predicates.add(pCategories);
		Map<String, DataFilter> filters = filter.getFilterDetail();

		if (!Objects.isNull(filters) && filter.isActive()) {
			for (Map.Entry<String, DataFilter> entry : filters.entrySet()) {
				DataFilter value = entry.getValue();
				Predicate p = null;
				if ("notificationTime".equalsIgnoreCase(entry.getKey())) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Date from = null;
					Date to = null;

					if ("date".equalsIgnoreCase(value.getBy())) {
						try {
							from = sdf.parse(value.getFromDate());
							to = sdf.parse(value.getToDate());

						} catch (ParseException e) {
							e.printStackTrace();
						}

					} else if ("days".equalsIgnoreCase(value.getBy())) {
						LocalDate currentDate = LocalDate.now();
						int days = Integer.parseInt(value.getNoOfDays());
						from = Date.from(currentDate.minusDays(days).atStartOfDay(ZoneId.systemDefault()).toInstant());
						to = new Date();
					}
					predicates.add(cb.and(cb.greaterThan(root.get("date"), from), cb.lessThan(root.get("date"), to)));

				}
			}
		}

		return predicates.toArray(new Predicate[] {});

		// return predicates;

	}

	private List<ActionLogDto> processNotificationForView(List<ActionLog> actionNotifications) {
		List<ActionLogDto> actionLogs = new ArrayList<>();
		for (ActionLog notification : actionNotifications) {
			ActionLogDto notifiDto = new ActionLogDto();
			notifiDto.setId(notification.getId());
			notifiDto.setMessage(notification.getMessage());
			notifiDto.setIconCategory(notification.getIconCategory());
			notifiDto.setNotificationTime(this.getDurationForNotification(notification.getActionTime()));
			notifiDto.setActive(notification.isActive());
			notifiDto.setSeen(notification.isSeen());
			actionLogs.add(notifiDto);
		}

		return actionLogs;
	}

	private List<ActionLogDto> processPublicNotificationForView(List<Publicnotification> publicNotifications) {
		List<ActionLogDto> actionLogs = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		for (Publicnotification notification : publicNotifications) {
			ActionLogDto notifiDto = new ActionLogDto();
			notifiDto.setId(notification.getId());
			notifiDto.setCircularNumber(notification.getCircularNumber());
			notifiDto.setDate(Objects.isNull(notification.getDate()) ? "" : sdf.format(notification.getDate()));
			notifiDto.setDocumentNumber(notification.getDocumentNumber());
			notifiDto.setSubject(notification.getSubject());
			notifiDto.setUrl(notification.getUrl());
			notifiDto.setContent(notification.getContent());
			notifiDto.setNotificationTime(this.getDurationForNotification(notification.getCreationDateTime()));
			notifiDto.setIconCategory(notification.getIconCategory());
			actionLogs.add(notifiDto);
		}

		return actionLogs;
	}

	private String getDurationForNotification(Date creationTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
		Date currentTime = new Date();
		String timeString = "";
		Interval interval = new Interval(creationTime.getTime(), currentTime.getTime());
		org.joda.time.Period period = interval.toPeriod();
		if (period.getYears() > 0 || period.getMonths() > 0 || period.getHours() > 0) {
			timeString = sdf.format(creationTime);
		} else {
			timeString = period.getMinutes() + " Min " + period.getSeconds() + " Sec Ago";
		}

		return timeString;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Publicnotification> getPublicNotification(int userId) {
		  
		String qry="select u.lastLogin from user_lastlogs u where u.userId=:userId order by u.lastLogin desc limit 2";
		
	  List<Timestamp> userLogs= em.createNativeQuery(qry)
			  .setParameter("userId",userId).getResultList();
	  
	       if(!Objects.isNull(userLogs) && userLogs.size() >= 2) {
		javax.persistence.Query query=em.createNamedQuery(Publicnotification._FINDUnseenPublicNotification)
				.setParameter("userLastLog",userLogs.get(0).toLocalDateTime())
				.setParameter("userSecondLastLog",userLogs.get(1).toLocalDateTime());
		return query.getResultList();
	       }
	       return new ArrayList<>();
	}
	
	
	public List<String> getSSEBroadcastMessaage() {
		  
		javax.persistence.Query query=em.createNamedQuery(Publicnotification._FINDActiveMessage)
				.setParameter("userLastLog",LocalDateTime.now());
		return query.getResultList();
	}


}
