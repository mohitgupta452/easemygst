package com.ginni.easemygst.portal.business.dto;


import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

import java.sql.Timestamp;


@JsonInclude(Include.NON_NULL)
public@Data class NotificationCount implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private String category;
	
	private long count;

	

}