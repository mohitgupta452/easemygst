package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class EmailDTO implements Serializable {

	private int id;

	private String attachments;

	private String emailevent;

	private String emailtype;

	private Timestamp generationTime;

	private String mailbcc;

	private String mailbody;

	private String mailcc;

	private String mailsubject;

	private String mailto;

	private Date modifiedtime;

	private Timestamp processedTime;

	private String remarks;

	private int status;
	
	public interface EmailDTOMixIn extends CommonMixin {
		
	}
	
}
