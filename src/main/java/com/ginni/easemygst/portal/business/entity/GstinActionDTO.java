package com.ginni.easemygst.portal.business.entity;

import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;

public @Data class GstinActionDTO {

	private int id;

	private Date actionDate;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String particular;

	private String returnType;

	private String status;

	private String transaction;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	private String preference;

	public interface GstinActionDTOMixIn extends CommonMixin {
		
	}

}
