package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class ReturnHistoryDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7471823355755360943L;

	private String monthYear;

	private String returnType;

	private String status;
	
	private Date date;
	
	private String fullMonthName;
	
	private String shortCommaStyleMonthYear;
	
	private String fullMonthYear;

	public ReturnHistoryDTO() {

	}

	public ReturnHistoryDTO(String monthYear,String returnType) {

		this.monthYear=monthYear;
		this.returnType=returnType;
	}
	public ReturnHistoryDTO(String monthYear,String returnType,String status,Date date) throws AppException {
		this.monthYear=monthYear;
		this.returnType=returnType;
		this.status=status;
		this.setFullMonthName(monthYear);
		this.setShortCommaStyleMonthYear(monthYear);
		this.setFullMonthYear(monthYear);
		if("filed".equalsIgnoreCase(status))
		   this.date=date;
	}
	
	public ReturnHistoryDTO(String monthYear,String returnType,String status,Date startDate,Date dueDate) throws AppException {
		this.monthYear=monthYear;
		this.returnType=returnType;
		this.status=status;
		this.setFullMonthName(monthYear);
		this.setShortCommaStyleMonthYear(monthYear);
		this.setFullMonthYear(monthYear);
		
		if("open".equalsIgnoreCase(status) || "overdue".equalsIgnoreCase(status))
			this.date=dueDate;
		else if("pending".equalsIgnoreCase(status))
			this.date=startDate;
	}
	
	public void setFullMonthName(String monthYear) throws AppException {
		
		this.fullMonthName=CalanderCalculatorUtility.getMonthYearToFullMonthName(monthYear);
		
	}
	
	public void setShortCommaStyleMonthYear(String monthYear) throws AppException {
		
		this.shortCommaStyleMonthYear=CalanderCalculatorUtility.getShortCommaStyleMonthYear(monthYear);
		
	}
	
	public void setFullMonthYear(String monthYear) throws AppException {
		
		this.fullMonthYear=CalanderCalculatorUtility.getFullMonthYearWithSpace(monthYear);
		
	}
	
	
}
