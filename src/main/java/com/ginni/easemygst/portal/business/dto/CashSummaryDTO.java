package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import lombok.Data;

public @Data class CashSummaryDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static DecimalFormat df2 = new DecimalFormat(".##");

	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalIgst")
	public Double getTotalIgst() {
		
		double totalIgst = 42000.0;
		
		try {
			
		} finally {
			return Double.valueOf(df2.format(totalIgst));
		}
	}
	
	@JsonSetter("totalIgst")
	public void setTotalIgst(double igst) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalSgst")
	public Double getTotalSgst() {

		double totalSgst = 18000.0;
		
		try {
			
		} finally {
			return Double.valueOf(df2.format(totalSgst));
		}
	}
	
	@JsonSetter("totalSgst")
	public void setTotalSgst(double sgst) {
		
	}

	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalCgst")
	public Double getTotalCgst() {
		
		double totalCgst = 18000.0;
		
		try {
		
		} finally {
			return Double.valueOf(df2.format(totalCgst));
		}
	}
	
	@JsonSetter("totalCgst")
	public void setTotalCgst(double cgst) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalCess")
	public Double getTotalCess() {
		
		double totalCess = 4000.0;
		
		try {
			
		} finally {
			return Double.valueOf(df2.format(totalCess));
		}
	}
	
	@JsonSetter("totalCess")
	public void setTotalCess(double cess) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalTax")
	public Double getTotalTax() {
		
		double totalTax = 0.0;
		
		try {
			totalTax += this.getTotalCess();
			totalTax += this.getTotalIgst();
			totalTax += this.getTotalCgst();
			totalTax += this.getTotalSgst();
		} finally {
			return Double.valueOf(df2.format(totalTax));
		}
	}
	
	@JsonSetter("totalTax")
	public void setTotalTax(double tax) {
		
	}
	
}
