package com.ginni.easemygst.portal.business.service.impl;

import javax.inject.Inject;

import com.ginni.easemygst.portal.business.service.GstnService;
import com.ginni.easemygst.portal.business.service.LedgerService;
import com.ginni.easemygst.portal.gst.request.GetLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.response.CashITCBalance;
import com.ginni.easemygst.portal.gst.response.GetCreditLedgerDetailsResp;
import com.ginni.easemygst.portal.helper.AppException;

public class LedgerServiceImpl implements LedgerService {

	@Inject
	GstnService gstn;

	@Override
	public CashITCBalance getCashLedgerDetails(GetLedgerDetailsReq cashLedgerDetailsReq)
			throws AppException {
		cashLedgerDetailsReq.setAction("BAL");
		CashITCBalance getCashLedgerDetailsResp = gstn.getCashLedgerDetailsReq(cashLedgerDetailsReq);

		return getCashLedgerDetailsResp;
	}

	@Override
	public GetCreditLedgerDetailsResp getCreditLedgerDetails(GetLedgerDetailsReq cashLedgerDetailsReq)
			throws AppException {
		// TODO Auto-generated method stub
		cashLedgerDetailsReq.setAction("BAL");
		return gstn.getCreditLedgerDetailsReq(cashLedgerDetailsReq);
	}

}
