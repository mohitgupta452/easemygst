package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class AssignedGstinList {

	private String gstin;
	
	private String gstinDisplayName;
	
	private String gstinState;
	
	private boolean isInvoiceAccess;
	
	private String accessType;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssignedGstinList other = (AssignedGstinList) obj;
		if (gstin == null) {
			if (other.gstin != null)
				return false;
		} else if (!gstin.equals(other.gstin))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode());
		return result;
	}
	
}
