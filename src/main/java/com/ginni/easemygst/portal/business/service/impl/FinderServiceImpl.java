package com.ginni.easemygst.portal.business.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.mapping.Array;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.entity.BrtMappingDTO;
import com.ginni.easemygst.portal.business.entity.BusinessTypeDTO;
import com.ginni.easemygst.portal.business.entity.CityDTO;
import com.ginni.easemygst.portal.business.entity.ErpTransactionDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinActionDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.GstnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.NonGovStateDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService.GstnAction;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.gst.response.GSTNRespStatus;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.DataFilter;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.BrtMapping;
import com.ginni.easemygst.portal.persistence.entity.BusinessType;
import com.ginni.easemygst.portal.persistence.entity.City;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.Debugmode;
import com.ginni.easemygst.portal.persistence.entity.ErpConfig;
import com.ginni.easemygst.portal.persistence.entity.ErpLog;
import com.ginni.easemygst.portal.persistence.entity.ErpTransaction;
import com.ginni.easemygst.portal.persistence.entity.FilePrefernce;
import com.ginni.easemygst.portal.persistence.entity.GstCalendar;
import com.ginni.easemygst.portal.persistence.entity.GstReturn;
import com.ginni.easemygst.portal.persistence.entity.GstinAction;
import com.ginni.easemygst.portal.persistence.entity.GstinTransaction;
import com.ginni.easemygst.portal.persistence.entity.GstnTransaction;
import com.ginni.easemygst.portal.persistence.entity.HelpContent;
import com.ginni.easemygst.portal.persistence.entity.HsnChapter;
import com.ginni.easemygst.portal.persistence.entity.HsnProduct;
import com.ginni.easemygst.portal.persistence.entity.HsnSac;
import com.ginni.easemygst.portal.persistence.entity.HsnSubChapter;
import com.ginni.easemygst.portal.persistence.entity.InvoiceVendorDetails;
import com.ginni.easemygst.portal.persistence.entity.ItemCode;
import com.ginni.easemygst.portal.persistence.entity.NonGovState;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.Partner;
import com.ginni.easemygst.portal.persistence.entity.Payment;
import com.ginni.easemygst.portal.persistence.entity.Plan;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.ReturnTransaction;
import com.ginni.easemygst.portal.persistence.entity.State;
import com.ginni.easemygst.portal.persistence.entity.Taxpayer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TpMapping;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.persistence.entity.UserContact;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.persistence.entity.UserLastlog;
import com.ginni.easemygst.portal.persistence.entity.UserTaxpayer;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CommonAttributesEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.SheetMapping;
import com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.FilterType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.Utility;

import net.sf.jasperreports.crosstabs.fill.JRPercentageCalculatorFactory.LongPercentageCalculator;

/**
 * @author tcinv1052
 *
 */
@Transactional
@Stateless
@EJB(name = "finder", beanInterface = MasterService.class)
public class FinderServiceImpl implements FinderService {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private RedisService redisService;

	@Inject
	private TaxpayerService taxpayerService;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public TaxpayerGstin findTaxpayerGstinAll(String gstin) {
		@SuppressWarnings("unchecked")
		List<TaxpayerGstin> taxpayerGstins = crudService.findWithNamedQuery("TaxpayerGstin.findByGstinAll",
				QueryParameter.with("gstin", gstin).parameters(), 1);

		return taxpayerGstins != null && !taxpayerGstins.isEmpty() ? taxpayerGstins.get(0) : null;
	}

	@Override
	public TaxpayerGstin findTaxpayerGstin(String gstin) {
		@SuppressWarnings("unchecked")

		List<TaxpayerGstin> taxpayerGstins = crudService.findWithNamedQuery("TaxpayerGstin.findByGstin",
				QueryParameter.with("gstin", gstin).parameters(), 1);

		return taxpayerGstins != null && !taxpayerGstins.isEmpty() ? taxpayerGstins.get(0) : null;
	}

	@Override
	public FilePrefernce findpreference(String gstin, String fyear) {
		@SuppressWarnings("unchecked")

		List<FilePrefernce> filePrefernce = crudService.findWithNamedQuery("FilePrefernce.findPreference",
				QueryParameter.with("gstin", gstin).and("financialYear", fyear).parameters(), 1);

		return filePrefernce != null && !filePrefernce.isEmpty() ? filePrefernce.get(0) : null;
	}

	@Override
	public GstnTransaction getGstnTransaction(GstnTransactionDTO gstnTransactionDTO) throws AppException {
		if (!Objects.isNull(gstnTransactionDTO)) {

			@SuppressWarnings("unchecked")
			List<GstnTransaction> gstnTransactions = crudService.findWithNamedQuery("GstnTransaction.findTransaction",
					QueryParameter.with("gstin", gstnTransactionDTO.getTaxpayerGstin().getGstin())
							.and("returnType", gstnTransactionDTO.getReturnType())
							.and("monthYear", gstnTransactionDTO.getMonthYear())
							.and("transactionType", gstnTransactionDTO.getTransactionType())
							.and("actionRequired", gstnTransactionDTO.getActionRequired()).parameters(),
					1);

			GstnTransaction gstnTransaction = Objects.isNull(gstnTransactions) ? null
					: gstnTransactions.isEmpty() ? null : gstnTransactions.get(0);

			return gstnTransaction;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public GSTR1 getMultipleGstinTransaction(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {

			String type = null;

			if (StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				type = "ALL";
			} else {
				type = gstinTransactionDTO.getTransactionType();
			}

			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				if (!gstinTransactionDTO.getTransactionType().equalsIgnoreCase("ALL")) {
					type = gstinTransactionDTO.getTransactionType();
				}
			}

			GSTR1 gstr1 = new GSTR1();

			gstinTransactionDTO.setTransactionType(type);
			if (!StringUtils.isEmpty(type)) {

				if (type.equalsIgnoreCase("ALL")) {
					for (TransactionType transType : MasterService.GSTR1_TRANSACTIONS) {
						gstinTransactionDTO.setTransactionType(transType.toString());
						this.createGstr1GstnObj(gstinTransactionDTO, gstr1);
					}
					gstinTransactionDTO.setTransactionType("ALL");

				} else {
					this.createGstr1GstnObj(gstinTransactionDTO, gstr1);
					try {
						TransactionType.valueOf(type + "A");
						gstinTransactionDTO.setTransactionType(type + "A");
						this.createGstr1GstnObj(gstinTransactionDTO, gstr1);
					} catch (Exception e) {
						log.error("invalid transaction {}", type + "A", e);
					}
				}
				gstinTransactionDTO.setTransactionType(type);
			}
			return gstr1;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void createGstr1GstnObj(GstinTransactionDTO gstinTransactionDTO, GSTR1 gstr1) throws AppException {

		String transactionType = gstinTransactionDTO.getTransactionType();
		List responseList = null;
		if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(transactionType))
			transactionType = "docissue";
		responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
		if (!Objects.isNull(responseList) && !responseList.isEmpty())
			try {
				BeanUtils.setProperty(gstr1, StringUtils.lowerCase(transactionType), responseList);
			} catch (IllegalAccessException | InvocationTargetException e) {
				log.error("illegal propertyName with gstr1 ", e);
			}
	}

	public JsonNode getSheetMapping(SourceType sourceType, ReturnType returnType) {

		return (JsonNode) crudService
				.findWithNamedQuery("SheetMapping.findBySource",
						QueryParameter.with("returnType", returnType).and("sourceType", sourceType).parameters())
				.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public GSTR2 getMultipleGstinTransactionGstr2(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {

			@SuppressWarnings("rawtypes")
			Map parameters = new HashMap<>();
			parameters.put("gstin", gstinTransactionDTO.getTaxpayerGstin().getGstin());
			parameters.put("returnType", gstinTransactionDTO.getReturnType());
			parameters.put("monthYear", gstinTransactionDTO.getMonthYear());

			if (StringUtils.isEmpty(gstinTransactionDTO.getTransactionType())) {
				parameters.put("type", "all");
			} else {
				parameters.put("type", gstinTransactionDTO.getTransactionType());
			}

			// if
			// (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionType()))
			// {
			// if
			// (!gstinTransactionDTO.getTransactionType().equalsIgnoreCase("all"))
			// {
			// parameters.put("type", gstinTransactionDTO.getTransactionType());
			// }
			// }
			//
			GSTR2 gstr2 = new GSTR2();
			String type = null;
			if (parameters.containsKey("type"))
				type = parameters.get("type").toString();

			// if(type.equalsIgnoreCase(anotherString))
			if (!StringUtils.isEmpty(type)) {

				if (type.equalsIgnoreCase("all")) {

					for (TransactionType transType : TransactionType.values()) {
						List responseList = new ArrayList<>();
						if (transType.toString().equalsIgnoreCase("B2BUR")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setB2bur(responseList);
						}
						if (transType.toString().equalsIgnoreCase("IMPG")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setImpgtrans(responseList);
						}
						if (transType.toString().equalsIgnoreCase("TXPD")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setTxpd(responseList);
						}
						if (transType.toString().equalsIgnoreCase("HSNSUM")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setHsnsums(responseList);
						}
						if (transType.toString().equalsIgnoreCase("IMPS")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setImps(responseList);
						}
						if (transType.toString().equalsIgnoreCase("AT")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setAt(responseList);
						}
						if (transType.toString().equalsIgnoreCase("NIL")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setNil(responseList);
						}
						if (transType.toString().equalsIgnoreCase("CDN")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setCdn(responseList);
						}
						if (transType.toString().equalsIgnoreCase("CDNUR")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setCdnur(responseList);
						}
						if (transType.toString().equalsIgnoreCase("B2B")) {
							gstinTransactionDTO.setTransactionType(transType.toString());
							responseList = this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null);
							if (!responseList.isEmpty())
								gstr2.setB2b(responseList);
						}
					}
					gstinTransactionDTO.setTransactionType("ALL");

				} else {
					if (type.equalsIgnoreCase("B2BUR"))
						gstr2.setB2bur(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("IMPG"))
						gstr2.setImpgtrans(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("TXPD"))
						gstr2.setTxpd(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("HSNSUM"))
						gstr2.setHsnsums(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("IMPS"))
						gstr2.setImps(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("AT"))
						gstr2.setAt(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("NIL"))
						gstr2.setNil(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("CDN"))
						gstr2.setCdn(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("CDNUR"))
						gstr2.setCdnur(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
					if (type.equalsIgnoreCase("B2B"))
						gstr2.setB2b(this.getGstinTransactionByGstinMonthYear(gstinTransactionDTO, null));
				}
			}

			return gstr2;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReturnStatus> getReturnStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("gstin", gstinTransactionDTO.getTaxpayerGstin().getGstin());
			parameters.put("returnType", gstinTransactionDTO.getReturnType());
			parameters.put("monthYear", gstinTransactionDTO.getMonthYear());
			parameters.put("transitId", gstinTransactionDTO.getTransitId());
			parameters.put("status", "PENDING");

			List<ReturnStatus> returnStatus = crudService.findWithNamedQuery("ReturnStatus.findByTransitId",
					parameters);

			return Objects.isNull(returnStatus) ? null : returnStatus;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ReturnStatus> getReturnStatusByFlag(GstinTransactionDTO gstinTransactionDTO, String flag)
			throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("gstin", gstinTransactionDTO.getTaxpayerGstin().getGstin());
			parameters.put("returnType", gstinTransactionDTO.getReturnType());
			parameters.put("monthYear", gstinTransactionDTO.getMonthYear());
			parameters.put("flag", flag);

			List<ReturnStatus> returnStatus = crudService.findWithNamedQuery("ReturnStatus.findByFlag", parameters);
			return Objects.isNull(returnStatus) ? null : returnStatus;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public GstinAction findGstinAction(GstinActionDTO gstinActionDTO, String gstin) {

		@SuppressWarnings("unchecked")
		List<GstinAction> gstinActions = crudService.findWithNamedQuery("GstinAction.findByStatus",
				QueryParameter.with("gstin", gstin).and("particular", gstinActionDTO.getParticular())
						.and("returnType", gstinActionDTO.getReturnType()).and("status", gstinActionDTO.getStatus())
						.parameters(),
				1);

		if (Objects.isNull(gstinActions) || gstinActions.isEmpty()) {
			return null;
		}

		return gstinActions.get(0);
	}

	@Override
	public List<GstinTransaction> findGstinTransactionByIdMonth(Integer taxpayerGstinId, String monthYear) {

		@SuppressWarnings("unchecked")
		List<GstinTransaction> gstinTransactions = crudService.findWithNamedQuery("GstinTransaction.findByIdMonth",
				QueryParameter.with("id", taxpayerGstinId).and("monthYear", monthYear).parameters());

		if (Objects.isNull(gstinTransactions) || gstinTransactions.isEmpty()) {
			return new ArrayList<>();
		}

		return gstinTransactions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GstReturnDTO> getGstReturn() throws AppException {

		List<GstReturnDTO> gstReturnDTOs = new ArrayList<>();
		List<GstReturn> gstReturns = crudService.findWithNamedQuery("GstReturn.findAll");

		for (GstReturn gstReturn : gstReturns) {
			GstReturnDTO gstReturnDTO = (GstReturnDTO) EntityHelper.convert(gstReturn, GstReturnDTO.class);
			gstReturnDTOs.add(gstReturnDTO);
		}

		return gstReturnDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessTypeDTO> getBusinessType() throws AppException {

		List<BusinessTypeDTO> businessTypeDTOs = new ArrayList<>();
		List<BusinessType> businessTypes = crudService.findWithNamedQuery("BusinessType.findAll");

		for (BusinessType businessType : businessTypes) {
			BusinessTypeDTO businessTypeDTO = (BusinessTypeDTO) EntityHelper.convert(businessType,
					BusinessTypeDTO.class);
			businessTypeDTOs.add(businessTypeDTO);
		}

		return businessTypeDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReturnTransactionDTO> getReturnTransaction() throws AppException {

		List<ReturnTransactionDTO> returnTransactionDTOs = new ArrayList<>();
		List<ReturnTransaction> returnTransactions = crudService.findWithNamedQuery("ReturnTransaction.findAll");

		for (ReturnTransaction returnTransaction : returnTransactions) {
			ReturnTransactionDTO ReturnTransactionDTO = (ReturnTransactionDTO) EntityHelper.convert(returnTransaction,
					ReturnTransactionDTO.class);
			returnTransactionDTOs.add(ReturnTransactionDTO);
		}

		return returnTransactionDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StateDTO> getAllState() throws AppException {

		List<StateDTO> stateDTOs = new ArrayList<>();
		List<State> states = crudService.findWithNamedQuery("State.findAll");

		for (State state : states) {
			StateDTO stateDTO = (StateDTO) EntityHelper.convert(state, StateDTO.class);
			stateDTOs.add(stateDTO);
		}

		return stateDTOs;
	}

	@Override
	public List<GstCalendar> getAllGstCalendars() throws AppException {

		List<GstCalendar> gstCalendars = new ArrayList<>();
		gstCalendars = crudService.findWithNamedQuery("GstCalendar.findAll");

		return gstCalendars;
	}

	@Override
	public List<com.ginni.easemygst.portal.persistence.entity.Calendar> getAllCalendars() throws AppException {

		List<com.ginni.easemygst.portal.persistence.entity.Calendar> calendars = new ArrayList<>();
		calendars = crudService.findWithNamedQuery("Calendar.findAll");

		return calendars;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessTypeDTO> getAllBusinessType() throws AppException {

		List<BusinessTypeDTO> businessTypeDTOs = new ArrayList<>();
		List<BusinessType> businessTypes = crudService.findWithNamedQuery("BusinessType.findAll");

		for (BusinessType businessType : businessTypes) {
			BusinessTypeDTO businessTypeDTO = (BusinessTypeDTO) EntityHelper.convert(businessType,
					BusinessTypeDTO.class);
			businessTypeDTOs.add(businessTypeDTO);
		}

		return businessTypeDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NonGovStateDTO> getAllNonGovState() throws AppException {

		List<NonGovStateDTO> stateDTOs = new ArrayList<>();
		List<NonGovState> states = crudService.findWithNamedQuery("NonGovState.findAll");

		for (NonGovState state : states) {
			NonGovStateDTO stateDTO = (NonGovStateDTO) EntityHelper.convert(state, NonGovStateDTO.class);
			stateDTOs.add(stateDTO);
		}

		return stateDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CityDTO> getAllCities(int stateId) throws AppException {

		List<CityDTO> cityDTOs = new ArrayList<>();
		NonGovState state = new NonGovState();
		state.setId(stateId);
		List<City> cities = crudService.findWithNamedQuery("City.findByState",
				QueryParameter.with("state", state).parameters());

		for (City city : cities) {
			CityDTO cityDTO = (CityDTO) EntityHelper.convert(city, CityDTO.class);
			cityDTOs.add(cityDTO);
		}

		return cityDTOs;
	}

	@Override
	public HelpContent findHelpContent(String link) {

		@SuppressWarnings("unchecked")
		List<HelpContent> helpContents = crudService.findWithNamedQuery("HelpContent.findByLink",
				QueryParameter.with("link", link).parameters(), 1);

		if (Objects.isNull(helpContents) || helpContents.isEmpty()) {
			return null;
		}

		return helpContents.get(0);
	}

	@Override
	public HsnSac findHsnSac(Integer panId, String code) {

		@SuppressWarnings("unchecked")
		List<HsnSac> hsnSacs = crudService.findWithNamedQuery("HsnSac.findByIdCode",
				QueryParameter.with("id", panId).and("code", code).parameters(), 1);

		if (Objects.isNull(hsnSacs) || hsnSacs.isEmpty()) {
			return null;
		}

		return hsnSacs.get(0);
	}

	@Override
	public List<HsnSac> findHsnSacContent(String type, String code) {

		@SuppressWarnings("unchecked")
		List<HsnSac> hsnSacs = crudService.findWithNamedQuery("HsnSac.findByCode",
				QueryParameter.with("type", type).and("code", code + "%").parameters());

		if (Objects.isNull(hsnSacs) || hsnSacs.isEmpty()) {
			return new ArrayList<>();
		}

		return hsnSacs;
	}

	@Override
	public List<HsnSubChapter> searchHsnProductByKey(String searchKey) {

		@SuppressWarnings("unchecked")
		List<HsnSubChapter> hsnProducts = crudService.findWithNamedQuery("HsnSubChapter.findByKey",
				QueryParameter.with("key", searchKey).parameters());

		if (Objects.isNull(hsnProducts)) {
			return new ArrayList<>();
		}

		return hsnProducts;
	}

	@Override
	public Partner findPartner(String email, String contactNo) {

		@SuppressWarnings("unchecked")
		List<Partner> partners = crudService.findWithNamedQuery("Partner.findByData",
				QueryParameter.with("email", email).and("contactNo", contactNo).parameters(), 1);

		if (Objects.isNull(partners) || partners.isEmpty()) {
			return null;
		}

		return partners.get(0);
	}

	@Override
	public Partner findPartnerByCode(String code) {

		@SuppressWarnings("unchecked")
		List<Partner> partners = crudService.findWithNamedQuery("Partner.findByCode",
				QueryParameter.with("code", code).parameters(), 1);

		if (Objects.isNull(partners) || partners.isEmpty()) {
			return null;
		}

		return partners.get(0);
	}

	@Override
	public GstinTransaction getGstinTransaction(GstinTransactionDTO gstinTransactionDTO, Boolean reconciled)
			throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			String type = gstinTransactionDTO.getTransactionType();
			if (reconciled) {
				type += "-MISMATCH";
			}

			@SuppressWarnings("unchecked")
			List<GstinTransaction> gstinTransactions = crudService.findWithNamedQuery(
					"GstinTransaction.findTransaction",
					QueryParameter.with("gstin", gstinTransactionDTO.getTaxpayerGstin().getGstin())
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).and("transactionType", type)
							.parameters(),
					1);

			GstinTransaction gstinTransaction = Objects.isNull(gstinTransactions) ? null
					: gstinTransactions.isEmpty() ? null : gstinTransactions.get(0);

			return gstinTransaction;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ginni.easemygst.portal.business.service.FinderService#
	 * getGstinTransactionByGstinMonthYearReturnType(com.ginni.easemygst.portal.
	 * business.entity.GstinTransactionDTO,
	 * com.ginni.easemygst.portal.persistence.FilterDto)
	 * 
	 * get transaction data by gstin,return type and month year with filter and
	 * pagination
	 */
	@Override
	public Map<String, Object> getGstinTransactionByGstinMonthYearReturnType(GstinTransactionDTO gstinTransactionDTO,
			FilterDto filter) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {
			String type = gstinTransactionDTO.getTransactionType();
			String queryName = "";
			Class classType = B2BDetailEntity.class;
			Map<String, Object> queryParameters = new HashMap<>();
			queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
			queryParameters.put("taxPayerGstin",
					(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
			queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());
			classType = this.getEntityTypeByTransaction(type);

			if (TransactionType.B2B.toString().equalsIgnoreCase(type)) {
				queryName = "B2bTransaction.getByGstinMonthyearReturntype";
			} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)) {
				queryName = "CdnTransaction.getByGstinMonthyearReturntype";
			} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)) {
				queryName = "CdnTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.B2CL.toString().equalsIgnoreCase(type)) {
				queryName = "B2clTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.B2CS.toString().equalsIgnoreCase(type)) {
				queryName = "B2csTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.EXP.toString().equalsIgnoreCase(type)) {
				queryName = "ExpTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.AT.toString().equalsIgnoreCase(type)) {
				queryName = "AtTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.NIL.toString().equalsIgnoreCase(type)) {
				queryName = "NilTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.TXPD.toString().equalsIgnoreCase(type)) {
				queryName = "TxpTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {
				queryName = "HsnTransaction.getByGstinMonthyearReturntype";
			}
			// gstr2
			if (TransactionType.IMPG.toString().equalsIgnoreCase(type)) {
				queryName = "ImpgTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.IMPS.toString().equalsIgnoreCase(type)) {
				queryName = "ImpsTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.B2BUR.toString().equalsIgnoreCase(type)) {
				queryName = "B2bUrTransaction.getByGstinMonthyearReturntype";
			}
			if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type)) {
				queryName = "DocIssue.getByGstinMonthyearReturntype";
			}
			List gstinTransactions;
			Map<String, Object> temp = new HashMap<>();

			// @SuppressWarnings("unchecked")
			if (!Objects.isNull(filter)) {// if filter object is available than
				temp = this.getFilteredData(gstinTransactionDTO, filter, classType);

			} else {
				gstinTransactions = crudService.findWithNamedQuery(queryName, queryParameters);
				temp.put("transactionData", gstinTransactions);
				temp.put("total_count", null);
			}

			return temp;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Class getEntityTypeByTransaction(String type) {

		Class classType = null;

		if (TransactionType.B2B.toString().equalsIgnoreCase(type)
				|| TransactionType.B2BA.toString().equalsIgnoreCase(type)) {

			classType = B2BDetailEntity.class;

		} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)
				|| TransactionType.CDNA.toString().equalsIgnoreCase(type)) {

			classType = CDNDetailEntity.class;

		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)
				|| TransactionType.CDNURA.toString().equalsIgnoreCase(type)) {

			classType = CDNURDetailEntity.class;

		}
		if (TransactionType.B2CL.toString().equalsIgnoreCase(type)
				|| TransactionType.B2CLA.toString().equalsIgnoreCase(type)) {

			classType = B2CLDetailEntity.class;

		}
		if (TransactionType.B2CS.toString().equalsIgnoreCase(type)
				|| TransactionType.B2CSA.toString().equalsIgnoreCase(type)) {

			classType = B2CSTransactionEntity.class;

		}
		if (TransactionType.EXP.toString().equalsIgnoreCase(type)
				|| TransactionType.EXPA.toString().equalsIgnoreCase(type)) {

			classType = ExpDetail.class;

		}
		if (TransactionType.AT.toString().equalsIgnoreCase(type)
				|| TransactionType.ATA.toString().equalsIgnoreCase(type)) {

			classType = ATTransactionEntity.class;

		}
		if (TransactionType.NIL.toString().equalsIgnoreCase(type)) {

			classType = NILTransactionEntity.class;

		}
		if (TransactionType.TXPD.toString().equalsIgnoreCase(type)
				|| TransactionType.TXPDA.toString().equalsIgnoreCase(type)) {

			classType = TxpdTransaction.class;

		}
		if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {

			classType = Hsn.class;

		}
		if (TransactionType.IMPG.toString().equalsIgnoreCase(type)) {

			classType = IMPGTransactionEntity.class;

		}
		if (TransactionType.IMPS.toString().equalsIgnoreCase(type)) {

			classType = IMPSTransactionEntity.class;

		}

		if (TransactionType.B2BUR.toString().equalsIgnoreCase(type)) {

			classType = B2bUrTransactionEntity.class;

		}
		if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type) || "docissue".equalsIgnoreCase(type)) {
			classType = DocIssue.class;
		}
		return classType;
	}

	@Override
	public ErpTransaction getErpTransaction(ErpTransactionDTO erpTransactionDTO) throws AppException {

		if (!Objects.isNull(erpTransactionDTO)) {

			String type = erpTransactionDTO.getTransactionType();

			@SuppressWarnings("unchecked")
			List<ErpTransaction> erpTransactions = crudService.findWithNamedQuery("ErpTransaction.findTransaction",
					QueryParameter.with("gstin", erpTransactionDTO.getTaxpayerGstin().getGstin())
							.and("returnType", erpTransactionDTO.getReturnType())
							.and("monthYear", erpTransactionDTO.getMonthYear()).and("transactionType", type)
							.parameters(),
					1);

			ErpTransaction erpTransaction = Objects.isNull(erpTransactions) ? null
					: erpTransactions.isEmpty() ? null : erpTransactions.get(0);

			return erpTransaction;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getCtinTransaction(GstinTransactionDTO gstinTransactionDTO, String returnType) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			@SuppressWarnings("unchecked")
			List<GstinTransaction> gstinTransactions = crudService.findWithNamedQuery(
					"GstinTransaction.findTransactionByCtin",
					QueryParameter.with("obj", "%" + gstinTransactionDTO.getTaxpayerGstin().getGstin() + "%")
							.and("returnType", returnType).and("monthYear", gstinTransactionDTO.getMonthYear())
							.and("transactionType", gstinTransactionDTO.getTransactionType()).parameters());

			String finalObj = "";

			if (!Objects.isNull(gstinTransactions)) {

				TransactionFactory transactionFactory = new TransactionFactory(
						gstinTransactionDTO.getTransactionType());

				for (GstinTransaction gstinTransaction2 : gstinTransactions) {
					if (!Objects.isNull(gstinTransaction2.getTransactionObject())) {

						String transactionObject = gstinTransaction2.getTransactionObject();

						boolean isFiled = false;
						List<GstinAction> gstinActions = findGstinActionByTaxpayerGstinId(
								gstinTransaction2.getTaxpayerGstin().getId());

						GstinAction tempAction = new GstinAction();
						tempAction.setReturnType(returnType);
						tempAction.setParticular(gstinTransaction2.getMonthYear());
						tempAction.setStatus(GstnAction.FILED.toString());

						if (gstinActions.contains(tempAction))
							isFiled = true;

						if (returnType.equals(ReturnType.GSTR1.toString())
								|| (returnType.equals(ReturnType.GSTR2.toString()) && isFiled)) {

							transactionObject = transactionFactory.fetchTransactionData(transactionObject,
									gstinTransactionDTO.getTaxpayerGstin().getGstin());
							finalObj = transactionFactory.processFetchedData(finalObj, transactionObject,
									gstinTransaction2.getTaxpayerGstin().getGstin(), isFiled);
						}
					}
				}

				return !StringUtils.isEmpty(finalObj) ? finalObj : "[]";
			}

			return "[]";
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public ErpConfig findErpConfigByUser(Integer userId) {

		@SuppressWarnings("unchecked")
		List<ErpConfig> erpConfigs = crudService.findWithNamedQuery("ErpConfig.findByUserId",
				QueryParameter.with("userId", userId).parameters());

		if (Objects.isNull(erpConfigs) || erpConfigs.isEmpty()) {
			return null;
		}

		return erpConfigs.get(0);
	}

	@Override
	public List<ErpLog> getErpLogs(Integer userId) {

		@SuppressWarnings("unchecked")
		List<ErpLog> erpLogs = crudService.findWithNamedQuery("ErpLog.findByUserId",
				QueryParameter.with("userId", userId).parameters(), 550);

		if (Objects.isNull(erpLogs) || erpLogs.isEmpty()) {
			return new ArrayList<>();
		}

		return erpLogs;
	}
	
	@Override
	public String findProvider(int OrganisationId) {
		if (OrganisationId > 0) {
			Integer userId = em.createNamedQuery("User.findIdByorganisationId", Integer.class)
					.setParameter("organisationId", OrganisationId).getSingleResult();
			ErpConfig erpconfig = this.findErpConfigByUser(userId);
			if(Objects.isNull(erpconfig))
			   return "0";
			String provider = erpconfig.getProvider();
			if (provider.equalsIgnoreCase("ginesys")) {
				return "1";
			} else {
				return "0";
			}

		}
		return "0";
	}
	

	@Override
	public ErpLog getErpLog(Integer userId, String description) {

		@SuppressWarnings("unchecked")
		List<ErpLog> erpLogs = crudService.findWithNamedQuery("ErpLog.findByUserAndDesc",
				QueryParameter.with("userId", userId).and("description", description).parameters(), 1);

		if (Objects.isNull(erpLogs) || erpLogs.isEmpty()) {
			return null;
		}

		return erpLogs.get(0);
	}

	@Override
	public ErpConfig findErpConfigByUserProvider(Integer userId, String provider) {

		@SuppressWarnings("unchecked")
		List<ErpConfig> erpConfigs = crudService.findWithNamedQuery("ErpConfig.findByUserIdProvider",
				QueryParameter.with("userId", userId).and("provider", provider).parameters());

		if (Objects.isNull(erpConfigs) || erpConfigs.isEmpty()) {
			return null;
		}

		return erpConfigs.get(0);
	}

	@Override
	public List<GstinAction> findGstinActionByTaxpayerGstinId(Integer taxpayerGstinId) {

		@SuppressWarnings("unchecked")
		List<GstinAction> gstinActions = crudService.findWithNamedQuery("GstinAction.findByTaxpayerGstinId",
				QueryParameter.with("id", taxpayerGstinId).parameters());

		if (Objects.isNull(gstinActions) || gstinActions.isEmpty()) {
			return new ArrayList<>();
		}

		return gstinActions;
	}

	@Override
	public List<Taxpayer> findTaxpayerId(int userId, String type) {

		@SuppressWarnings("unchecked")
		List<Taxpayer> userTaxpayers = crudService.findWithNamedQuery("Taxpayer.findByIdentityType",
				QueryParameter.with("id", userId).and("type", type).parameters(), 1);

		if (Objects.isNull(userTaxpayers) || userTaxpayers.isEmpty()) {
			return null;
		}

		return userTaxpayers;
	}

	@Override
	public List<Vendor> findVendorEmailFlag() {

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.checkEmailFlag");

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return null;
		}

		return vendors;
	}

	@Override
	public List<Vendor> findVendorFollowEmailFlag() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -3);

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.checkFollowEmailFlag",
				QueryParameter.with("timing", calendar.getTime()).parameters());

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return null;
		}

		return vendors;
	}

	@Override
	public User getExistingUser(int userId, int active) {

		@SuppressWarnings("unchecked")
		List<User> users = crudService.findWithNamedQuery("User.findIdentityById",
				QueryParameter.with("id", userId).and("active", (byte) active).parameters(), 1);

		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public List<UserLastlog> getUserLastLogs(int userId) {

		@SuppressWarnings("unchecked")
		List<UserLastlog> users = crudService.findWithNamedQuery("UserLastlog.findById",
				QueryParameter.with("id", userId).parameters(), 1);

		return users;

	}

	@Override
	public UserContact getUserContact(int userId) {

		@SuppressWarnings("unchecked")
		List<UserContact> users = crudService.findWithNamedQuery("UserContact.findById",
				QueryParameter.with("id", userId).parameters(), 1);

		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public UserDTO getExistingUser(String username, int active) {

		@SuppressWarnings("unchecked")
		List<UserDTO> users = crudService.findWithNamedQuery("User.findIdentityByUsername",
				QueryParameter.with("username", username).and("active", (byte) active).parameters(), 1);

		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public UserDTO getExistingUser(String username) {

		@SuppressWarnings("unchecked")
		List<UserDTO> users = crudService.findWithNamedQuery("User.findUserIdentityByUsername",
				QueryParameter.with("username", username).parameters(), 1);

		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public UserDTO getUserTaxpayer(int userId) throws AppException {

		if (userId > 0) {

			@SuppressWarnings("unchecked")
			List<User> users = crudService.findWithNamedQuery("User.findIdentityById",
					QueryParameter.with("id", userId).and("active", (byte) 1).parameters(), 1);

			return users != null ? (UserDTO) EntityHelper.convert(users.get(0), UserDTO.class) : null;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Taxpayer findTaxpayer(String pan) {
		@SuppressWarnings("unchecked")
		List<Taxpayer> taxpayers = crudService.findWithNamedQuery("Taxpayer.findByPan",
				QueryParameter.with("pan", pan).parameters(), 1);

		if (Objects.isNull(taxpayers) || taxpayers.isEmpty()) {
			return null;
		}

		return taxpayers.get(0);
	}

	@Override
	public Taxpayer findTaxpayerByUser(String pan, int userId, String type) {

		@SuppressWarnings("unchecked")
		List<Taxpayer> taxpayers = crudService.findWithNamedQuery("Taxpayer.findByIdentity",
				QueryParameter.with("pan", pan).and("id", userId).and("type", type).parameters(), 1);

		if (Objects.isNull(taxpayers) || taxpayers.isEmpty()) {
			return null;
		}

		return taxpayers.get(0);
	}

	@Override
	public UserTaxpayer findUserTaxpayerByUser(String username, String pan) {

		@SuppressWarnings("unchecked")
		List<UserTaxpayer> userTaxpayers = crudService.findWithNamedQuery("UserTaxpayer.findIdentityByUsername",
				QueryParameter.with("username", username).and("pan", pan).parameters(), 1);

		if (Objects.isNull(userTaxpayers) || userTaxpayers.isEmpty()) {
			return null;
		}

		return userTaxpayers.get(0);
	}

	@Override
	public UserTaxpayer findUserTaxpayerByPan(String pan) {

		@SuppressWarnings("unchecked")
		List<UserTaxpayer> userTaxpayers = crudService.findWithNamedQuery("UserTaxpayer.findIdentityByPan",
				QueryParameter.with("pan", pan).parameters(), 1);

		if (Objects.isNull(userTaxpayers) || userTaxpayers.isEmpty()) {
			return null;
		}

		return userTaxpayers.get(0);
	}

	@Override
	public List<UserGstin> getUserGstin(int userId) throws AppException {

		if (userId > 0) {

			@SuppressWarnings("unchecked")
			List<UserGstin> gstins = crudService.findWithNamedQuery("UserGstin.findIdentityById",
					QueryParameter.with("id", userId).parameters());

			return gstins != null ? gstins : new ArrayList<>();

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public List<UserGstin> getUserGstinByUserOrg(int userId, int organisationId) throws AppException {

		if (userId > 0) {

			@SuppressWarnings("unchecked")
			List<UserGstin> gstins = crudService.findWithNamedQuery("UserGstin.findByUserOrg",
					QueryParameter.with("userId", userId).and("organisationId", organisationId).parameters());

			return gstins != null ? gstins : new ArrayList<>();

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public List<UserGstin> getUserGstinByOrganisation(Integer id) {

		if (id > 0) {

			@SuppressWarnings("unchecked")
			List<UserGstin> gstins = crudService.findWithNamedQuery("UserGstin.findByOrg",
					QueryParameter.with("organisationId", id).parameters());

			return gstins != null ? gstins : new ArrayList<>();

		}
		return new ArrayList<>();
	}

	@Override
	public List<Organisation> getDistinctUserGstinByUser(int userId) throws AppException {

		if (userId > 0) {

			@SuppressWarnings("unchecked")
			List<Organisation> organisations = crudService.findWithNamedQuery("UserGstin.findDistinctByUser",
					QueryParameter.with("userId", userId).parameters());

			return organisations != null ? organisations : new ArrayList<>();

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public UserDTO findUserByOrganisation(int orgId) throws AppException {

		if (orgId > 0) {

			@SuppressWarnings("unchecked")
			List<UserDTO> users = crudService.findWithNamedQuery("User.findUserIdentityByOrg",
					QueryParameter.with("orgId", orgId).parameters(), 1);

			return users != null && users.size() > 0 ? users.get(0) : null;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public TaxpayerGstin findTaxpayerGstinByGstin(String gstin) {

		@SuppressWarnings("unchecked")
		List<TaxpayerGstin> taxpayerGstins = crudService.findWithNamedQuery("TaxpayerGstin.findByGstinNoActive",
				QueryParameter.with("gstin", gstin).parameters(), 1);

		return taxpayerGstins != null && !taxpayerGstins.isEmpty() ? taxpayerGstins.get(0) : null;
	}

	@Override
	public GstReturn findGstReturn(int id) {

		@SuppressWarnings("unchecked")
		List<GstReturn> gstReturns = crudService.findWithNamedQuery("GstReturn.findById",
				QueryParameter.with("id", id).parameters(), 1);

		return gstReturns != null && !gstReturns.isEmpty() ? gstReturns.get(0) : null;
	}

	@Override
	public List<UserGstin> findUserGstinByUserAndPan(String pan, int userId) {

		@SuppressWarnings("unchecked")
		List<UserGstin> userGstins = crudService.findWithNamedQuery("UserGstin.findIdentityByUserPan",
				QueryParameter.with("pan", pan).and("id", userId).parameters());

		return userGstins != null && !userGstins.isEmpty() ? userGstins : null;
	}

	@Override
	public List<UserTaxpayer> findPrimaryTaxpayer(int userId) {

		@SuppressWarnings("unchecked")
		List<UserTaxpayer> userTaxpayers = crudService.findWithNamedQuery("UserTaxpayer.findPrimaryTaxpayer",
				QueryParameter.with("id", userId).parameters());

		return userTaxpayers != null && !userTaxpayers.isEmpty() ? userTaxpayers : null;
	}

	@Override
	public List<UserTaxpayer> findSecondaryTaxpayer(int userId) {

		@SuppressWarnings("unchecked")
		List<UserTaxpayer> userTaxpayers = crudService.findWithNamedQuery("UserTaxpayer.findSecondaryTaxpayer",
				QueryParameter.with("id", userId).parameters());

		return userTaxpayers != null && !userTaxpayers.isEmpty() ? userTaxpayers : null;
	}

	@Override
	public boolean findUserTaxpayer(int userId, int taxpayerId) {

		@SuppressWarnings("unchecked")
		List<UserTaxpayer> userTaxpayers = crudService.findWithNamedQuery("UserTaxpayer.findUserTaxpayer",
				QueryParameter.with("id", userId).and("tid", taxpayerId).parameters());

		return userTaxpayers != null && !userTaxpayers.isEmpty() ? true : false;
	}

	@Override
	public boolean findUserGstin(int userId, int gstinId, int returnId) {

		@SuppressWarnings("unchecked")
		List<UserGstin> userGstins = crudService.findWithNamedQuery("UserGstin.findUserGstin",
				QueryParameter.with("id", userId).and("gid", gstinId).and("rid", returnId).parameters());

		return userGstins != null && !userGstins.isEmpty() ? true : false;
	}

	@Override
	public List<BrtMappingDTO> getBrtMapping(String[] temp) {

		List<BrtMappingDTO> brtMappingDTOs = new ArrayList<>();

		final String _BRT_MAPPING_KEY = "brtmapping20122017";

		if (redisService.getBrtMapping(_BRT_MAPPING_KEY) == null
				|| redisService.getBrtMapping(_BRT_MAPPING_KEY).isEmpty()) {
			@SuppressWarnings("unchecked")
			List<BrtMapping> brtMappings = crudService.findWithNamedQuery("BrtMapping.findByBusinessTypes");

			for (BrtMapping brtMapping : brtMappings) {
				BrtMappingDTO brtMappingDTO = (BrtMappingDTO) EntityHelper.convert(brtMapping, BrtMappingDTO.class);
				brtMappingDTOs.add(brtMappingDTO);
			}
			redisService.setBrtMapping(_BRT_MAPPING_KEY, brtMappingDTOs, false, 0);
		} else {
			brtMappingDTOs = redisService.getBrtMapping(_BRT_MAPPING_KEY);
		}

		return brtMappingDTOs;
	}

	/*
	 * @Override public ErpConfig findErpConfig(String pan) {
	 * 
	 * @SuppressWarnings("unchecked") List<ErpConfig> erpConfigs =
	 * crudService.findWithNamedQuery("ErpConfig.findByPan",
	 * QueryParameter.with("pan", pan).parameters(), 1);
	 * 
	 * if (Objects.isNull(erpConfigs) || erpConfigs.isEmpty()) { return null; }
	 * 
	 * return erpConfigs.get(0); }
	 */

	@Override
	public List<BrtMappingDTO> getAllBrtMapping() {

		List<BrtMappingDTO> brtMappingDTOs = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<BrtMapping> brtMappings = crudService.findWithNamedQuery("BrtMapping.findByAllBusinessTypes");

		for (BrtMapping brtMapping : brtMappings) {
			BrtMappingDTO brtMappingDTO = (BrtMappingDTO) EntityHelper.convert(brtMapping, BrtMappingDTO.class);
			brtMappingDTOs.add(brtMappingDTO);
		}

		return brtMappingDTOs;
	}

	@Override
	public HsnChapter findHsnChapter(String code) {

		@SuppressWarnings("unchecked")
		List<HsnChapter> hsnChapters = crudService.findWithNamedQuery("HsnChapter.findByCode",
				QueryParameter.with("code", code).parameters(), 1);

		if (Objects.isNull(hsnChapters) || hsnChapters.isEmpty()) {
			return null;
		}

		return hsnChapters.get(0);
	}

	@Override
	public HsnSubChapter findHsnSubChapter(String code) {

		@SuppressWarnings("unchecked")
		List<HsnSubChapter> hsnSubChapters = crudService.findWithNamedQuery("HsnSubChapter.findByCode",
				QueryParameter.with("code", code).parameters(), 1);

		if (Objects.isNull(hsnSubChapters) || hsnSubChapters.isEmpty()) {
			return null;
		}

		return hsnSubChapters.get(0);
	}

	@Override
	public HsnProduct findHsnProduct(String code) {

		@SuppressWarnings("unchecked")
		List<HsnProduct> hsnProducts = crudService.findWithNamedQuery("HsnProduct.findByCode",
				QueryParameter.with("code", code).parameters(), 1);

		if (Objects.isNull(hsnProducts) || hsnProducts.isEmpty()) {
			return null;
		}

		return hsnProducts.get(0);
	}

	@Override
	public UserGstin findUserGstinById(Integer id) {

		@SuppressWarnings("unchecked")
		List<UserGstin> userGstins = crudService.findWithNamedQuery("UserGstin.findById",
				QueryParameter.with("id", id).parameters(), 1);

		if (Objects.isNull(userGstins) || userGstins.isEmpty()) {
			return null;
		}

		return userGstins.get(0);
	}

	@Override
	public Debugmode findDebugmodeByUsernameAndPassword(String debuguser, String debugpass) {

		@SuppressWarnings("unchecked")
		List<Debugmode> debugmodes = crudService.findWithNamedQuery("Debugmode.findByIdentity",
				QueryParameter.with("username", debuguser).and("password", debugpass).parameters(), 1);

		if (Objects.isNull(debugmodes) || debugmodes.isEmpty()) {
			return null;
		}

		return debugmodes.get(0);
	}

	@Override
	public Vendor findVendorByRequest(String requestId) {

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.findByRequestId",
				QueryParameter.with("requestId", requestId).parameters(), 1);

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return null;
		}

		return vendors.get(0);
	}

	@Override
	public List<TpMapping> findSecondaryTpMapping(Integer secondaryTaxpayerId) {

		@SuppressWarnings("unchecked")
		List<TpMapping> tpMappings = crudService.findWithNamedQuery("TpMapping.findBySecondaryId",
				QueryParameter.with("secondaryId", secondaryTaxpayerId).parameters(), 1);

		if (Objects.isNull(tpMappings) || tpMappings.isEmpty()) {
			return null;
		}

		return tpMappings;

	}

	@Override
	public Vendor findVendorByTpGstin(Integer taxpayerId, String gstin) {

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.findByGstinId",
				QueryParameter.with("id", taxpayerId).and("gstin", gstin).parameters(), 1);

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return null;
		}

		return vendors.get(0);

	}

	@Override
	public List<Vendor> findVendorByTpPan(Integer taxpayerId, String pan) {

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.findByPanId",
				QueryParameter.with("id", taxpayerId).and("pan", pan).parameters());

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return null;
		}

		return vendors;

	}

	@Override
	public boolean checkTpMapping(Integer primaryTaxpayerId, Integer secondaryTaxpayerId) {

		@SuppressWarnings("unchecked")
		List<TpMapping> tpMappings = crudService.findWithNamedQuery("TpMapping.findById", QueryParameter
				.with("secondaryId", secondaryTaxpayerId).and("primaryId", primaryTaxpayerId).parameters(), 1);

		if (Objects.isNull(tpMappings) || tpMappings.isEmpty()) {
			return false;
		}

		return true;
	}

	@Override
	public List<Vendor> getVendorByTaxpayerId(Integer taxpayerId) {

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.findByTaxpayerId",
				QueryParameter.with("id", taxpayerId).parameters());

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return new ArrayList<>();
		}

		return vendors;
	}

	@Override
	public List<Vendor> getVendorByTaxpayerId(Integer taxpayerId, String type) {

		@SuppressWarnings("unchecked")
		List<Vendor> vendors = crudService.findWithNamedQuery("Vendor.findByTaxpayerIdType",
				QueryParameter.with("id", taxpayerId).and("type", type).parameters());

		if (Objects.isNull(vendors) || vendors.isEmpty()) {
			return new ArrayList<>();
		}

		return vendors;
	}

	@Override
	public List<HsnSac> getHsnSacByTaxpayerId(Integer taxpayerId) {

		@SuppressWarnings("unchecked")
		List<HsnSac> hsnSacs = crudService.findWithNamedQuery("HsnSac.findByTaxpayerId",
				QueryParameter.with("id", taxpayerId).parameters());

		if (Objects.isNull(hsnSacs) || hsnSacs.isEmpty()) {
			return new ArrayList<>();
		}

		return hsnSacs;
	}

	@Override
	public List<ItemCode> getItemCodeByTaxpayerId(Integer taxpayerId) {

		@SuppressWarnings("unchecked")
		List<ItemCode> itemCodes = crudService.findWithNamedQuery("ItemCode.findByTaxpayerId",
				QueryParameter.with("id", taxpayerId).parameters());

		if (Objects.isNull(itemCodes) || itemCodes.isEmpty()) {
			return null;
		}

		return itemCodes;
	}

	@Override
	public List<InvoiceVendorDetails> getInvoiceVendorDetails(Integer taxpayerGstinId) {

		@SuppressWarnings("unchecked")
		List<InvoiceVendorDetails> invoiceVendorDetails = crudService.findWithNamedQuery(
				"InvoiceVendorDetails.findByTaxpayerId", QueryParameter.with("id", taxpayerGstinId).parameters());

		if (Objects.isNull(invoiceVendorDetails) || invoiceVendorDetails.isEmpty()) {
			return null;
		}

		return invoiceVendorDetails;
	}

	@Override
	public InvoiceVendorDetails getInvoiceVendorDetailsById(Integer taxpayerGstinId, Integer id) {

		@SuppressWarnings("unchecked")
		List<InvoiceVendorDetails> invoiceVendorDetails = crudService.findWithNamedQuery(
				"InvoiceVendorDetails.findByTaxpayerAndId",
				QueryParameter.with("taxpayerGstinId", taxpayerGstinId).and("id", id).parameters(), 1);

		if (Objects.isNull(invoiceVendorDetails) || invoiceVendorDetails.isEmpty()) {
			return null;
		}

		return invoiceVendorDetails.get(0);
	}

	@Override
	public List<UserTaxpayer> findUserTaxpayerByTaxpayer(int taxpayerId) {

		@SuppressWarnings("unchecked")
		List<UserTaxpayer> userTaxpayers = crudService.findWithNamedQuery("UserTaxpayer.findByTaxpayerId",
				QueryParameter.with("id", taxpayerId).parameters());

		if (Objects.isNull(userTaxpayers) || userTaxpayers.isEmpty()) {
			return null;
		}

		return userTaxpayers;
	}

	@Override
	public List<UserGstin> findUserGstinByUserTaxpayer(String username, String gstin) {

		@SuppressWarnings("unchecked")
		List<UserGstin> userGstins = crudService.findWithNamedQuery("UserGstin.findByUserTaxpayerId",
				QueryParameter.with("username", username).and("gstin", gstin).parameters());

		if (Objects.isNull(userGstins) || userGstins.isEmpty()) {
			return null;
		}

		return userGstins;
	}

	@Override
	public List<UserGstin> findUserGstinByTaxpayer(int taxpayerId) {

		@SuppressWarnings("unchecked")
		List<UserGstin> userGstins = crudService.findWithNamedQuery("UserGstin.findByTaxpayerId",
				QueryParameter.with("id", taxpayerId).parameters());

		if (Objects.isNull(userGstins) || userGstins.isEmpty()) {
			return null;
		}

		return userGstins;
	}

	@Override
	public ItemCode findItemCodeByTaxpayerId(int taxpayerId, String desc) {

		@SuppressWarnings("unchecked")
		List<ItemCode> itemCodes = crudService.findWithNamedQuery("ItemCode.findByDescTaxpayerId",
				QueryParameter.with("id", taxpayerId).and("desc", desc).parameters(), 1);

		if (Objects.isNull(itemCodes) || itemCodes.isEmpty()) {
			return null;
		}

		return itemCodes.get(0);
	}

	@Override
	public InvoiceVendorDetails findInvoiceByNumber(String invoiceNo, Integer gstin) {

		@SuppressWarnings("unchecked")
		List<InvoiceVendorDetails> invoiceVendorDetails = crudService.findWithNamedQuery(
				"InvoiceVendorDetails.findInvoiceByNumber",
				QueryParameter.with("invoiceNo", invoiceNo).and("gstin", gstin).parameters(), 1);

		if (Objects.isNull(invoiceVendorDetails) || invoiceVendorDetails.isEmpty()) {
			return null;
		}

		return invoiceVendorDetails.get(0);
	}

	@Override
	public Payment getPaymentFromId(String paymentId) {

		@SuppressWarnings("unchecked")
		List<Payment> payments = crudService.findWithNamedQuery("Payment.findByPaymentId",
				QueryParameter.with("paymentId", paymentId).parameters(), 1);

		if (Objects.isNull(payments) || payments.isEmpty()) {
			return null;
		}

		return payments.get(0);
	}

	// filter and pagination code starts

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ginni.easemygst.portal.business.service.FinderService#getFilteredData
	 * (com.ginni.easemygst.portal.business.entity.GstinTransactionDTO,
	 * com.ginni.easemygst.portal.persistence.FilterDto, java.lang.Class)
	 */
	@Override
	public <T> Map<String, Object> getFilteredData(GstinTransactionDTO gstinTransactionDTO, FilterDto filter,
			Class<T> type) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		int pageNo = filter.getPageNo();
		int size = filter.getSize();
		Map<String, DataFilter> filters = filter.getFilterDetail();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cb.createQuery(type);
		Root<T> b = cQuery.from(type);
		List<Predicate> predicates = new ArrayList<>();
		if (!Objects.isNull(filters) && filter.isActive()) {
			for (Map.Entry<String, DataFilter> entry : filters.entrySet()) {// identitfy
																			// filters
																			// and
																			// convert
																			// it
																			// into
																			// predicates

				DataFilter dataFilter = entry.getValue();
				if (dataFilter.isActive()) {
					Map<String, Object> colMapping = this.getMapping(gstinTransactionDTO.getTransactionType());
					String fieldName = (String) colMapping.get(entry.getKey());
					String operator = dataFilter.getOperator();
					Predicate p1 = this.getPredicate(dataFilter.getFilter1Name(), dataFilter.getFilter1Value(),
							fieldName, cb, b, gstinTransactionDTO);
					if (!StringUtils.isEmpty(dataFilter.getFilter2Name())) {
						Predicate p2 = this.getPredicate(dataFilter.getFilter2Name(), dataFilter.getFilter2Value(),
								fieldName, cb, b, gstinTransactionDTO);
						if ("AND".equalsIgnoreCase(operator)) {
							predicates.add(cb.and(p1, p2));
						} else if ("OR".equalsIgnoreCase(operator)) {
							predicates.add(cb.or(p1, p2));

						}
					} else {
						predicates.add(p1);
					}
					cb.and();
				}

			}
		}

		predicates.add(this.getCommonPredicate(gstinTransactionDTO, cb, b));
		// cQuery.select(b).where(predicates.toArray(new Predicate[] {}));

		Selection[] selectedFields = this.getSelectedFieldsToQuery(b, gstinTransactionDTO.getReturnType(),
				gstinTransactionDTO.getTransactionType()).toArray(new Selection[] {});// fields
																						// to
																						// be
																						// displayed
		String orderBy = this.getOrderByField(gstinTransactionDTO.getReturnType(), transactionType);

		if (TransactionType.NIL == TransactionType.valueOf(StringUtils.upperCase(transactionType))
				|| TransactionType.DOC_ISSUE == TransactionType.valueOf(StringUtils.upperCase(transactionType))) {// select
																													// all
			cQuery.select(b).where(predicates.toArray(new Predicate[] {})).orderBy(cb.asc(b.get(orderBy)));

		} else {
			if (TransactionType.AT == TransactionType.valueOf(StringUtils.upperCase(transactionType))
					|| TransactionType.TXPD == TransactionType.valueOf(StringUtils.upperCase(transactionType))
					|| TransactionType.B2CS == TransactionType.valueOf(StringUtils.upperCase(transactionType))
					|| TransactionType.HSNSUM == TransactionType.valueOf(StringUtils.upperCase(transactionType))) {// select
																													// without
																													// orderiing
				cQuery.select(cb.construct(type, selectedFields)).where(predicates.toArray(new Predicate[] {}));

			} else
				cQuery.select(cb.construct(type, selectedFields)).where(predicates.toArray(new Predicate[] {}))
						.orderBy(cb.asc(b.get(orderBy)));
		}

		List<T> resultset = new ArrayList<>();
		if (pageNo <= 0) {
			resultset = em.createQuery(cQuery).getResultList();
		} else
			resultset = em.createQuery(cQuery).setFirstResult((pageNo - 1) * size).setMaxResults(size).getResultList();

		Map<String, Object> temp = new HashMap<>();
		temp.put("transactionData", resultset);
		temp.put("total_count", this.getFilteredCount(cb, type, predicates));

		return temp;
	}

	/**
	 * @param cb
	 * @param type
	 * @param predicates
	 * @return method used to get record counts of a trnasaction
	 */
	private <T> long getFilteredCount(CriteriaBuilder cb, Class<T> type, List<Predicate> predicates) {
		CriteriaQuery<Long> countq = cb.createQuery(Long.class);
		countq.select(cb.count(countq.from(type)));
		em.createQuery(countq);
		countq.where(predicates.toArray(new Predicate[] {}));
		Long filterCount = em.createQuery(countq).getSingleResult();
		if (Objects.nonNull(filterCount))
			return filterCount;
		else
			return 0;
	}

	/*
	 * private List<Path>getSpecificFieldByTransaction(String type, Root b){
	 * 
	 * cQuery.select(cb.construct(type,b.get("id"),b.get("invoiceNumber"))).
	 * where(predicates.toArray(new Predicate[] {}));
	 * 
	 * 
	 * return null; }
	 */

	/**
	 * @param returnType
	 * @param transactionType
	 * @return field on which transaction records to be arranged
	 */
	private String getOrderByField(String returnType, String transactionType) {
		String orderByField = "creationTime";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.B2CL.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.B2CS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "creationTime";
		} else if (TransactionType.EXP.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.AT.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "creationTime";
		} else if (TransactionType.TXPD.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "updationTime";
		} else if (TransactionType.HSNSUM.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "creationTime";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "billOfEntryNumber";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "invoiceNumber";
		} else if (TransactionType.NIL.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "creationTime";
		} else if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "creationTime";
		}

		return orderByField;
	}

	/**
	 * @param b
	 * @param returnType
	 * @param transactionType
	 * @return get fileld list which are to be shown in grid
	 */
	private List<Selection> getSelectedFieldsToQuery(Root b, String returnType, String transactionType) {
		Properties props = new Properties();
		InputStream inputStream = null;
		String fileName = "/transactionfields.properties";
		List<Selection> selectedFields = new ArrayList<>();
		try {
			inputStream = DBUtils.class.getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IOException();
			props.load(inputStream);
			String fields = props.getProperty(StringUtils.lowerCase(returnType + "." + transactionType));
			if ("all".equalsIgnoreCase(fields)) {
				selectedFields.add(b);
			} else {
				StringTokenizer st = new StringTokenizer(fields, ",");

				while (st.hasMoreTokens()) {
					Selection s = b.get(StringUtils.trim(st.nextToken()));
					selectedFields.add(s);
				}
			}
		}

		catch (IOException e) {
			log.error("file not found with this name--{}", fileName);
		}

		return selectedFields;

	}

	/**
	 * @param filterName
	 * @param fieldValue
	 * @param fieldName
	 * @param cb
	 * @param b
	 * @param gstinTransactionDto
	 * @return get predicate as per filter name and value
	 */
	<T> Predicate getPredicate(String filterName, String fieldValue, String fieldName, CriteriaBuilder cb, Root<T> b,
			GstinTransactionDTO gstinTransactionDto) {
		Predicate predicate = null;
		Path<?> path = null;
		if (TransactionType.CDN.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
				&& ("originalCtin".equalsIgnoreCase(fieldName) || "originalCtinName".equalsIgnoreCase(fieldName)
						|| "fillingStatus".equalsIgnoreCase(fieldName)))
			path = b.get("cdnTransaction").get(fieldName);
		else if (TransactionType.B2B.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
				&& ("ctin".equalsIgnoreCase(fieldName) || "ctinName".equalsIgnoreCase(fieldName)
						|| "fillingStatus".equalsIgnoreCase(fieldName)))
			path = b.get("b2bTransaction").get(fieldName);

		if ("type".equalsIgnoreCase(fieldName)) {
			fieldValue = this.getEnumValueRecon(fieldValue);
		}
		if (Objects.isNull(path))
			path = b.get(fieldName);

		if (!path.getJavaType().equals(String.class)) {

			log.debug("other than string value");
			if (FilterType.CONTAINS.toString().equalsIgnoreCase(filterName)) {
				filterName = FilterType.EQUAL.toString();
			}
			if (FilterType.DOES_NOT_CONTAINS.toString().equalsIgnoreCase(filterName)) {
				filterName = FilterType.NOT_EQUAL.toString();

			}

		}
		if (FilterType.CONTAINS.toString().equalsIgnoreCase(filterName)) {
			String likeExp = "%" + fieldValue.toString().trim() + "%";
			if (TransactionType.CDN.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
					&& ("originalCtin".equalsIgnoreCase(fieldName) || "originalCtinName".equalsIgnoreCase(fieldName)
							|| "fillingStatus".equalsIgnoreCase(fieldName)))
				predicate = cb.like(b.get("cdnTransaction").get(fieldName), likeExp);
			else if (TransactionType.B2B.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
					&& ("ctin".equalsIgnoreCase(fieldName) || "ctinName".equalsIgnoreCase(fieldName)
							|| "fillingStatus".equalsIgnoreCase(fieldName)))
				predicate = cb.like(b.get("b2bTransaction").get(fieldName), likeExp);
			else if ("flags".equalsIgnoreCase(fieldName) && "DRAFT".equalsIgnoreCase(fieldValue)
					&& ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDto.getReturnType())
					&& TransactionType.B2B.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())) {

				predicate = cb.and(cb.like(b.get("b2bTransaction").get("fillingStatus"), "%" + "N" + "%"),
						cb.or(cb.like(b.get("type"), "%" + "MISMATCH" + "%"),
								cb.like(b.get("type"), "%" + ReconsileType.MISSING_INWARD.toString() + "%")));

			} else if ("flags".equalsIgnoreCase(fieldName) && "DRAFT".equalsIgnoreCase(fieldValue)
					&& ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDto.getReturnType())
					&& TransactionType.CDN.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())) {
				predicate = cb.and(cb.like(b.get("cdnTransaction").get("fillingStatus"), "%" + "N" + "%"),
						cb.or(cb.like(b.get("type"), "%" + "MISMATCH" + "%"),
								cb.like(b.get("type"), "%" + ReconsileType.MISSING_INWARD.toString() + "%")));
			} else if ("flags".equalsIgnoreCase(fieldName) && "SYNCED".equalsIgnoreCase(fieldValue)
					&& ReturnType.GSTR1.toString().equalsIgnoreCase(gstinTransactionDto.getReturnType())) {
				predicate = cb.equal(b.get("isSynced"), true);
			} else if ("flags".equalsIgnoreCase(fieldName) && "NEW".equalsIgnoreCase(fieldValue)
					&& ReturnType.GSTR1.toString().equalsIgnoreCase(gstinTransactionDto.getReturnType())) {
				predicate = cb.and(cb.equal(b.get("flags"), ""), cb.notEqual(b.get("isSynced"), true));
			}

			else
				predicate = cb.like(b.get(fieldName), likeExp);
		}
		if (FilterType.DOES_NOT_CONTAINS.toString().equalsIgnoreCase(filterName)) {
			String likeExp = "%" + fieldValue.toString().trim() + "%";
			predicate = cb.notLike(b.get(fieldName), likeExp);
		}
		if (FilterType.EMPTY.toString().equalsIgnoreCase(filterName)) {

			predicate = cb.like(b.get(fieldName), "");
		}
		if (FilterType.NON_EMPTY.toString().equalsIgnoreCase(filterName)) {

			predicate = cb.notLike(b.get(fieldName), "");
		}
		if (FilterType.NULL.toString().equalsIgnoreCase(filterName)) {

			predicate = cb.isNull(b.get(fieldName));
		}
		if (FilterType.NOT_NULL.toString().equalsIgnoreCase(filterName)) {
			predicate = cb.isNotNull(b.get(fieldName));

		}
		if (FilterType.EQUAL.toString().equalsIgnoreCase(filterName)) {
			if (path.getJavaType().equals(java.util.Date.class)) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date date = new java.util.Date();
				try {
					date = sdf.parse(fieldValue);
				} catch (ParseException e) {
					log.debug("Invalid Date");
				}
				predicate = cb.equal(b.get(fieldName), date);

			} else if (path.getJavaType().equals(boolean.class) && "reverseCharge".equalsIgnoreCase(fieldName)) {
				boolean isReverse = "Y".equalsIgnoreCase(fieldValue) ? true : false;

				predicate = cb.equal(b.get(fieldName), isReverse);

				// } else if
				// (TransactionType.CDN.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
			} else if (path.getJavaType().equals(SupplyType.class) && "supplyType".equalsIgnoreCase(fieldName)) {
				SupplyType supplyType = SupplyType.INTERSTATE_CONSUMER;
				if ("INTER".contains(StringUtils.upperCase(fieldValue)))
					supplyType = SupplyType.INTER;
				else if ("INTRA".contains(StringUtils.upperCase(fieldValue)))
					supplyType = SupplyType.INTRA;

				predicate = cb.equal(b.get(fieldName), supplyType);

			} else if (TransactionType.CDN.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
					&& ("originalCtin".equalsIgnoreCase(fieldName) || "originalCtinName".equalsIgnoreCase(fieldName)
							|| "fillingStatus".equalsIgnoreCase(fieldName)))
				predicate = cb.equal(b.get("cdnTransaction").get(fieldName), fieldValue);
			else if (TransactionType.B2B.toString().equalsIgnoreCase(gstinTransactionDto.getTransactionType())
					&& ("ctin".equalsIgnoreCase(fieldName) || "ctinName".equalsIgnoreCase(fieldName)
							|| "fillingStatus".equalsIgnoreCase(fieldName)))
				predicate = cb.equal(b.get("b2bTransaction").get(fieldName), fieldValue);

			else
				predicate = cb.equal(b.get(fieldName), fieldValue);
			// predicate = cb.equal(b.get(fieldName), cb.parameter(Double.class,
			// fieldValue));

		}
		if (FilterType.NOT_EQUAL.toString().equalsIgnoreCase(filterName)) {
			predicate = cb.notEqual(b.get(fieldName), fieldValue);

		}

		if (FilterType.GREATER_THAN.toString().equalsIgnoreCase(filterName)) {
			if (path.getJavaType().equals(java.util.Date.class)) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date date = new java.util.Date();
				try {
					date = sdf.parse(fieldValue);

				} catch (ParseException e) {
					log.debug("Invalid Date");
				}
				predicate = cb.greaterThan(b.get(fieldName), date);

			} else
				predicate = cb.greaterThan(b.get(fieldName), fieldValue);
			// predicate = cb.equal(b.get(fieldName), cb.parameter(Double.class,
			// fieldValue));

		}
		if (FilterType.LESS_THAN.toString().equalsIgnoreCase(filterName)) {
			if (path.getJavaType().equals(java.util.Date.class)) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date date = new java.util.Date();
				try {
					date = sdf.parse(fieldValue);

				} catch (ParseException e) {
					log.debug("Invalid Date format in get predicate method ");
				}
				predicate = cb.lessThan(b.get(fieldName), date);

			} else
				predicate = cb.lessThan(b.get(fieldName), fieldValue);
			// predicate = cb.equal(b.get(fieldName), cb.parameter(Double.class,
			// fieldValue));

		}

		return predicate;

	}

	/**
	 * @param key
	 * @return
	 * 
	 * 		mapping reconcile type without space with mapping type with space for
	 *         filter purpose
	 */
	private String getEnumValueRecon(String key) {
		String enumValue = "";
		if (Objects.isNull(key))
			return "";
		if ("MISSING INWARD".equalsIgnoreCase(StringUtils.upperCase(key.trim()))) {
			enumValue = ReconsileType.MISSING_INWARD.toString();
		} else if ("MISSING OUTWARD".equalsIgnoreCase(StringUtils.upperCase(key.trim()))) {
			enumValue = ReconsileType.MISSING_OUTWARD.toString();

		} else if ("MISMATCH DATE".equalsIgnoreCase(StringUtils.upperCase(key.trim()))) {
			enumValue = ReconsileType.MISMATCH_DATE.toString();
		} else if ("MISMATCH MAJOR".equalsIgnoreCase(StringUtils.upperCase(key.trim()))
				|| "MISMATCH TAXAMOUNT".equalsIgnoreCase(StringUtils.upperCase(key.trim()))) {
			enumValue = ReconsileType.MISMATCH_MAJOR.toString();

		} else if ("MISMATCH ROUNDOFF TAXAMOUNT".equalsIgnoreCase(StringUtils.upperCase(key.trim()))) {
			enumValue = ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT.toString();

		} else
			return key;
		return enumValue;
	}

	/**
	 * @param gstinTransactionDTO
	 * @param cb
	 * @param b
	 * @return predicates for common conditions for all transactions like gstin
	 *         monthyear and returntype
	 */
	<T> Predicate getCommonPredicate(GstinTransactionDTO gstinTransactionDTO, CriteriaBuilder cb, Root<T> b) {
		TaxpayerGstin gstin = (TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
				TaxpayerGstin.class);
		Predicate pGstin = null;
		Predicate pRType = null;
		Predicate pMonthYear = null;
		Predicate pDataSource = null;
		Predicate pDelete = null;

		String type = gstinTransactionDTO.getTransactionType();
		if (TransactionType.B2B.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("b2bTransaction").get("taxpayerGstin"), gstin);
			pRType = cb.equal(b.get("b2bTransaction").get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("b2bTransaction").get("monthYear"), gstinTransactionDTO.getMonthYear());
			// pDataSource= cb.equal(b.get("dataSource"),DataSource.EMGST);
			Predicate dsAndRec = cb.and(cb.equal(b.get("dataSource"), DataSource.GSTN),
					cb.notEqual(b.get("type"), ReconsileType.NEW.toString()));

			pDataSource = cb.or(cb.equal(b.get("dataSource"), DataSource.EMGST), dsAndRec);

		} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("cdnTransaction").get("gstin"), gstin);
			pRType = cb.equal(b.get("cdnTransaction").get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("cdnTransaction").get("monthYear"), gstinTransactionDTO.getMonthYear());
			// cb.and(cb.equal(b.get("dataSource"), DataSource.EMGST));
			// pDataSource= cb.equal(b.get("dataSource"),DataSource.EMGST);
			Predicate dsAndRec = cb.and(cb.equal(b.get("dataSource"), DataSource.GSTN),
					cb.notEqual(b.get("type"), ReconsileType.NEW.toString()));
			pDataSource = cb.or(cb.equal(b.get("dataSource"), DataSource.EMGST), dsAndRec);

		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("cdnUrTransaction").get("gstin"), gstin);
			pRType = cb.equal(b.get("cdnUrTransaction").get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("cdnUrTransaction").get("monthYear"), gstinTransactionDTO.getMonthYear());

		} else if (TransactionType.B2CL.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("b2clTransaction").get("gstin"), gstin);
			pRType = cb.equal(b.get("b2clTransaction").get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("b2clTransaction").get("monthYear"), gstinTransactionDTO.getMonthYear());

		} else if (TransactionType.EXP.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("expTransaction").get("gstin"), gstin);
			pRType = cb.equal(b.get("expTransaction").get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("expTransaction").get("monthYear"), gstinTransactionDTO.getMonthYear());

		} else if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {
			pGstin = cb.equal(b.get("taxpayerGstin"), gstin);
			pRType = cb.equal(b.get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("monthYear"), gstinTransactionDTO.getMonthYear());

		} else {
			pGstin = cb.equal(!Objects.isNull(b.get("gstin")) ? b.get("gstin") : b.get("taxpayerGstin"), gstin);
			pRType = cb.equal(b.get("returnType"), gstinTransactionDTO.getReturnType());
			pMonthYear = cb.equal(b.get("monthYear"), gstinTransactionDTO.getMonthYear());

		}
		String flags = "DELETE";
		pDelete = cb.or(cb.notEqual(b.get("flags"), flags), cb.isNull(b.get("flags")));
		;

		if (pDataSource != null)
			return cb.and(pGstin, pRType, pMonthYear, pDataSource, pDelete);
		else
			return cb.and(pGstin, pRType, pMonthYear, pDelete);
	}

	/**
	 * @param type
	 * @return client attribute name to server attribute name mapping
	 */
	public Map<String, Object> getMapping(String type) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("ctin", "ctin");
		map.put("ctin_name", "ctinName");
		map.put("inv_type", "invoiceType");
		map.put("rchrg", "reverseCharge");
		map.put("tax_amt", "taxAmount");
		map.put("taxable_val", "taxableValue");
		map.put("type", "type");
		map.put("subType", "subType");
		map.put("source", "source");
		map.put("flags", "flags");
		map.put("inum", "invoiceNumber");
		map.put("invoice_no", "invoiceNumber");

		map.put("idt", "invoiceDate");

		map.put("org_ctin", "originalCtin");
		map.put("org_ctin_name", "originalCtinName");
		map.put("ntty", "noteType");
		map.put("nt_dt", "revisedInvDate");
		map.put("nt_num", "revisedInvNo");

		map.put("sply_ty", "supplyType");
		map.put("pos", "pos");
		map.put("state_name", "stateName");
		map.put("etin", "etin");
		map.put("cfs", "fillingStatus");

		// hsn desc
		map.put("hsn_desc", "description");
		map.put("unit", "unit");
		map.put("quantity", "quantity");
		map.put("tot_value", "value");
		map.put("hsn_sc", "code");

		return map;

	}

	/*
	 * public Map<String, Object> getMapping(String type) { Map<String, Object> map
	 * = new HashMap<String, Object>();
	 * 
	 * map.put("ctin", "ctin"); map.put("ctin_name", "ctinName");
	 * map.put("inv_type", "invoiceType"); map.put("rchrg", "reverseCharge");
	 * map.put("tax_amt", "taxAmount"); map.put("taxable_val", "taxableValue");
	 * map.put("type", "type"); map.put("subType", "subType"); map.put("source",
	 * "source"); map.put("flags", "flags"); map.put("inum", "invoiceNumber");
	 * map.put("invoice_no", "invoiceNumber");
	 * 
	 * map.put("idt", "invoiceDate");
	 * 
	 * map.put("org_ctin", "originalCtin"); map.put("org_ctin_name",
	 * "originalCtinName"); map.put("ntty", "noteType"); map.put("nt_dt",
	 * "revisedInvDate"); map.put("nt_num", "revisedInvNo");
	 * 
	 * map.put("sply_ty", "supplyType"); map.put("pos", "pos");
	 * map.put("state_name", "stateName"); map.put("etin", "etin"); map.put("cfs",
	 * "fillingStatus");
	 * 
	 * // hsn desc map.put("hsn_desc", "description"); map.put("unit", "unit");
	 * map.put("quantity", "quantity"); map.put("tot_value", "value");
	 * map.put("hsn_sc", "code");
	 * 
	 * return map;
	 * 
	 * }
	 */
	private <T> Map<String, Object> getB2csFilteredData(GstinTransactionDTO gstinTransactionDTO, FilterDto filter,
			Class<T> type) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		int pageNo = filter.getPageNo();
		int size = filter.getSize();
		Map<String, DataFilter> filters = filter.getFilterDetail();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cb.createQuery(type);
		Root<T> b = cQuery.from(type);
		List<Predicate> predicates = new ArrayList<>();
		if (!Objects.isNull(filters) && filter.isActive()) {
			for (Map.Entry<String, DataFilter> entry : filters.entrySet()) {

				DataFilter dataFilter = entry.getValue();
				if (dataFilter.isActive()) {
					Map<String, Object> colMapping = this.getMapping(gstinTransactionDTO.getTransactionType());
					String fieldName = (String) colMapping.get(entry.getKey());
					String operator = dataFilter.getOperator();
					Predicate p1 = this.getPredicate(dataFilter.getFilter1Name(), dataFilter.getFilter1Value(),
							fieldName, cb, b, gstinTransactionDTO);
					if (!StringUtils.isEmpty(dataFilter.getFilter2Name())) {
						Predicate p2 = this.getPredicate(dataFilter.getFilter2Name(), dataFilter.getFilter2Value(),
								fieldName, cb, b, gstinTransactionDTO);
						if ("AND".equalsIgnoreCase(operator)) {
							predicates.add(cb.and(p1, p2));
						} else if ("OR".equalsIgnoreCase(operator)) {
							predicates.add(cb.or(p1, p2));

						}
					} else {
						predicates.add(p1);
					}
					cb.and();
				}

			}
		}

		predicates.add(this.getCommonPredicate(gstinTransactionDTO, cb, b));
		// cQuery.select(b).where(predicates.toArray(new Predicate[] {}));

		Selection[] selectedFields = this.getSelectedFieldsToQuery(b, gstinTransactionDTO.getReturnType(),
				gstinTransactionDTO.getTransactionType()).toArray(new Selection[] {});
		String orderBy = this.getOrderByField(gstinTransactionDTO.getReturnType(), transactionType);

		if (TransactionType.HSNSUM == TransactionType.valueOf(StringUtils.upperCase(transactionType))
				|| TransactionType.DOC_ISSUE == TransactionType.valueOf(StringUtils.upperCase(transactionType))) {
			cQuery.select(b).where(predicates.toArray(new Predicate[] {})).orderBy(cb.asc(b.get(orderBy)));

		} else {
			if (TransactionType.HSNSUM == TransactionType.valueOf(StringUtils.upperCase(transactionType))
					|| TransactionType.HSNSUM == TransactionType.valueOf(StringUtils.upperCase(transactionType))
					|| TransactionType.HSNSUM == TransactionType.valueOf(StringUtils.upperCase(transactionType))
					|| TransactionType.HSNSUM == TransactionType.valueOf(StringUtils.upperCase(transactionType))) {
				cQuery.select(cb.construct(type, selectedFields)).where(predicates.toArray(new Predicate[] {}));

			} else
				cQuery.select(cb.construct(type, selectedFields)).where(predicates.toArray(new Predicate[] {}))
						.orderBy(cb.asc(b.get(orderBy)));
		}

		List<T> resultset = new ArrayList<>();
		if (pageNo <= 0) {
			resultset = em.createQuery(cQuery).getResultList();
		} else
			resultset = em.createQuery(cQuery).setFirstResult((pageNo - 1) * size).setMaxResults(size).getResultList();

		Map<String, Object> temp = new HashMap<>();
		temp.put("transactionData", resultset);
		temp.put("total_count", this.getFilteredCount(cb, type, predicates));

		return temp;
	}

	/* start temporary to be deleted */
	@Override
	public <T> List<String> getInvoicesByTypeMonthYearGstin(GstinTransactionDTO gstinTransactionDTO) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		Class<T> type = this.getEntityTypeByTransaction(transactionType);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cb.createQuery(type);
		Root<T> b = cQuery.from(type);
		cQuery.select(b.get("id")).where(this.getCommonPredicate(gstinTransactionDTO, cb, b));

		List invoices = em.createQuery(cQuery).getResultList();
		return invoices;

	}
	/* end temporary to be deleted */

	/*
	 * SELECT b.b2bTransaction.ctin, b.invoiceNumber,b.invoiceDate
	 * ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,
	 * b.b2bTransaction.ctinName FROM B2BDetailEntity b JOIN b.items i where
	 * b.b2bTransaction.taxpayerGstin=:taxPayerGstin and
	 * b.b2bTransaction.returnType=:returnType and
	 * b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and
	 * (i.totalEligibleTax is NULL or i.totalEligibleTax='') and
	 * b.dataSource=com.ginni.easemygst.portal.transaction.factory.
	 * Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),
	 */
	private List<Item> getItcBulkByFilter(GstinTransactionDTO gstinTransactionDTO, FilterDto filterDto) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Item> cq = cb.createQuery(Item.class);
		Root<B2BDetailEntity> b = cq.from(B2BDetailEntity.class);
		Join<Item, B2BDetailEntity> i = b.join("invoiceId", JoinType.INNER);
		// cq.

		return (new ArrayList<>());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Item> getInvoiceIdsByTypeMonthYearGstin(GstinTransactionDTO gstinTransactionDTO, FilterDto filterDto) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		TransactionType transactionTypeEnum = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		String query = "B2bDetailEntity.findItcBulk";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bDetailEntity.findItcBulk";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnDetailEntity.findItcBulk";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPGTransactionEntity.findItcBulk";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPSTransactionEntity.findItcBulk";
		}
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bUrTransactionEntity.findItcBulk";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnUrDetailEntity.findItcBulk";
		}

		List<Object[]> items = null;
		if (Objects.isNull(filterDto)) {
			items = crudService.findWithNamedQuery(query,
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
		} else {
			if (filterDto.isActive() && Objects.nonNull(filterDto.getFilterDetail())
					&& filterDto.getFilterDetail().containsKey("ctin")) {
				String ctin = "";
				for (Map.Entry<String, DataFilter> ent : filterDto.getFilterDetail().entrySet()) {
					if ("ctin".equalsIgnoreCase(ent.getKey())) {
						ctin = ent.getValue().getFilter1Value();
						ctin = "%" + ctin + "%";
					}
				}

				items = crudService.findPaginatedResults(this.getItcFilterQuery(transactionType),
						QueryParameter.with("taxPayerGstin", taxPayerGstin)
								.and("returnType", gstinTransactionDTO.getReturnType())
								.and("monthYear", gstinTransactionDTO.getMonthYear()).and("ctin", ctin).parameters(),
						filterDto.getPageNo(), filterDto.getSize());
			} else {
				items = crudService.findPaginatedResults(query,
						QueryParameter.with("taxPayerGstin", taxPayerGstin)
								.and("returnType", gstinTransactionDTO.getReturnType())
								.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters(),
						filterDto.getPageNo(), filterDto.getSize());
			}
		}
		List<Item> list = new ArrayList<>();
		for (Object[] item : items) {
			// List list = em.createQuery(cQuery).getResultList();
			Item itm = new Item();
			if (TransactionType.B2B == transactionTypeEnum || TransactionType.CDN == transactionTypeEnum) {
				itm.setCtin(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName(!Objects.isNull(item[10]) ? (String) item[10] : "");
				itm.setInvoiceNum(!Objects.isNull(item[1]) ? (String) item[1] : "");
				itm.setInvoiceDate(!Objects.isNull(item[2]) ? (Date) item[2] : null);
				itm.setIgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCgst(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setSgst(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setCess(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[7]) ? (Double) item[7] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[8]) ? (Double) item[8] : 0.0);
				itm.setId(!Objects.isNull(item[9]) ? (String) item[9] : "");
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
			} else if (TransactionType.IMPG == transactionTypeEnum) {
				itm.setCtin(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setIgst(!Objects.isNull(item[1]) ? (Double) item[1] : 0.0);
				itm.setCgst(!Objects.isNull(item[2]) ? (Double) item[2] : 0.0);
				itm.setSgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCess(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setId(!Objects.isNull(item[7]) ? (String) item[7] : "");
				itm.setInvoiceNum(!Objects.isNull(item[8]) ? (String) item[8] : "");
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
			} else if (TransactionType.IMPS == transactionTypeEnum || TransactionType.CDNUR == transactionTypeEnum
					|| TransactionType.B2BUR == transactionTypeEnum) {
				itm.setInvoiceNum(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setIgst(!Objects.isNull(item[1]) ? (Double) item[1] : 0.0);
				itm.setCgst(!Objects.isNull(item[2]) ? (Double) item[2] : 0.0);
				itm.setSgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCess(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setId(!Objects.isNull(item[7]) ? (String) item[7] : "");
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());

			}

			list.add(itm);

		}
		/*
		 * finalList.addAll(list); }
		 */
		return list;

	}

	private String getItcFilterQuery(String transactionType) {
		String query = null;
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bDetailEntity.findItcBulkWithFilter";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnDetailEntity.findItcBulk";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPGTransactionEntity.findItcBulkWithFilter";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPSTransactionEntity.findItcBulkWithFilter";
		}
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bUrTransactionEntity.findItcBulkWithFilter";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnUrDetailEntity.findItcBulkWithFilter";
		}
		return query;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Item> getBlankHsnInvoiceIdsByTypeMonthYearGstin(GstinTransactionDTO gstinTransactionDTO,
			FilterDto filterDto) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		TransactionType transactionTypeEnum = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		String query = "B2bDetailEntity.findBlankHsnBulk";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bDetailEntity.findBlankHsnBulk";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnDetailEntity.findBlankHsnBulk";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)
				&& ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType())) {
			query = "IMPGTransactionEntity.findBlankHsnBulk";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)
				&& ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType())) {
			query = "IMPSTransactionEntity.findBlankHsnBulk";
		}
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)
				&& ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType())) {
			query = "B2bUrTransactionEntity.findBlankHsnBulk";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnUrDetailEntity.findBlankHsnBulk";

		} else if (TransactionType.B2CL.toString().equalsIgnoreCase(transactionType)) {
			query = "B2clDetailEntity.findBlankHsnBulk";
		} else if (TransactionType.B2CS.toString().equalsIgnoreCase(transactionType)) {
			query = "B2csTransaction.findBlankHsnBulk";
		} else if (TransactionType.EXP.toString().equalsIgnoreCase(transactionType)) {
			query = "ExpDetail.findBlankHsnBulk";
		}

		List<Object[]> items = null;
		if (Objects.isNull(filterDto)) {
			items = crudService.findWithNamedQuery(query,
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
		} else {
			items = crudService.findPaginatedResults(query,
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters(),
					filterDto.getPageNo(), filterDto.getSize());
		}
		List<Item> list = new ArrayList<>();
		for (Object[] item : items) {
			// List list = em.createQuery(cQuery).getResultList();
			Item itm = new Item();
			if (TransactionType.B2B == transactionTypeEnum || TransactionType.CDN == transactionTypeEnum) {
				itm.setCtin(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName(!Objects.isNull(item[10]) ? (String) item[10] : "");
				itm.setInvoiceNum(!Objects.isNull(item[1]) ? (String) item[1] : "");
				itm.setInvoiceDate(!Objects.isNull(item[2]) ? (Date) item[2] : null);
				itm.setIgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCgst(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setSgst(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setCess(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[7]) ? (Double) item[7] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[8]) ? (Double) item[8] : 0.0);
				itm.setId(!Objects.isNull(item[9]) ? (String) item[9] : "");
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
				itm.setUnit(!Objects.isNull(item[11]) ? (String) item[11] : "");
				itm.setQuantity(!Objects.isNull(item[12]) ? (Double) item[12] : 0.0);
			} else if (TransactionType.IMPG == transactionTypeEnum) {
				itm.setCtin(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setIgst(!Objects.isNull(item[1]) ? (Double) item[1] : 0.0);
				itm.setCgst(!Objects.isNull(item[2]) ? (Double) item[2] : 0.0);
				itm.setSgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCess(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setId(!Objects.isNull(item[7]) ? (String) item[7] : "");
				itm.setInvoiceNum(!Objects.isNull(item[8]) ? (String) item[8] : "");
				itm.setInvoiceDate(!Objects.isNull(item[9]) ? (Date) item[9] : null);
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
				itm.setUnit(!Objects.isNull(item[10]) ? (String) item[10] : "");
				itm.setQuantity(!Objects.isNull(item[11]) ? (Double) item[11] : 0.0);

			} else if (TransactionType.IMPS == transactionTypeEnum || TransactionType.CDNUR == transactionTypeEnum
					|| TransactionType.B2BUR == transactionTypeEnum) {
				itm.setInvoiceNum(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setIgst(!Objects.isNull(item[1]) ? (Double) item[1] : 0.0);
				itm.setCgst(!Objects.isNull(item[2]) ? (Double) item[2] : 0.0);
				itm.setSgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCess(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setId(!Objects.isNull(item[7]) ? (String) item[7] : "");
				itm.setInvoiceDate(!Objects.isNull(item[8]) ? (Date) item[8] : null);
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
				itm.setUnit(String.valueOf(!Objects.isNull(item[9]) ? (String) item[9] : ""));
				itm.setQuantity(!Objects.isNull(item[10]) ? (Double) item[10] : 0.0);
				if (TransactionType.CDNUR == transactionTypeEnum) {
					itm.setInvoiceDate(!Objects.isNull(item[11]) ? (Date) item[11] : null);

				}

			} else if (TransactionType.B2CL == transactionTypeEnum) {
				itm.setInvoiceNum(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setInvoiceDate(!Objects.isNull(item[2]) ? (Date) item[2] : null);
				itm.setIgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCgst(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setSgst(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setCess(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[7]) ? (Double) item[7] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[8]) ? (Double) item[8] : 0.0);
				itm.setId(!Objects.isNull(item[9]) ? (String) item[9] : "");
				itm.setUnit(String.valueOf(!Objects.isNull(item[10]) ? (String) item[10] : ""));
				itm.setQuantity(!Objects.isNull(item[11]) ? (Double) item[11] : 0.0);
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());

			} else if (TransactionType.EXP == transactionTypeEnum) {
				itm.setInvoiceNum(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setIgst(!Objects.isNull(item[1]) ? (Double) item[1] : 0.0);
				itm.setCgst(!Objects.isNull(item[2]) ? (Double) item[2] : 0.0);
				itm.setSgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCess(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setId(!Objects.isNull(item[7]) ? (String) item[7] : "");
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
				itm.setUnit(String.valueOf(!Objects.isNull(item[8]) ? (String) item[8] : ""));
				itm.setQuantity(!Objects.isNull(item[9]) ? (Double) item[9] : 0.0);
				itm.setInvoiceDate(!Objects.isNull(item[10]) ? (Date) item[10] : null);

			} else if (TransactionType.B2CS == transactionTypeEnum) {
				itm.setInvoiceNum(!Objects.isNull(item[0]) ? (String) item[0] : "");
				itm.setCtinName("");
				itm.setIgst(!Objects.isNull(item[1]) ? (Double) item[1] : 0.0);
				itm.setCgst(!Objects.isNull(item[2]) ? (Double) item[2] : 0.0);
				itm.setSgst(!Objects.isNull(item[3]) ? (Double) item[3] : 0.0);
				itm.setCess(!Objects.isNull(item[4]) ? (Double) item[4] : 0.0);
				itm.setTaxRate(!Objects.isNull(item[5]) ? (Double) item[5] : 0.0);
				itm.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0.0);
				itm.setId(!Objects.isNull(item[7]) ? (String) item[7] : "");
				itm.setTaxAmount(itm.getIgst() + itm.getCgst() + itm.getSgst() + itm.getCess());
				itm.setUnit(String.valueOf(!Objects.isNull(item[8]) ? (String) item[8] : ""));
				itm.setQuantity(!Objects.isNull(item[9]) ? (Double) item[9] : 0.0);

			}

			list.add(itm);

		}
		/*
		 * finalList.addAll(list); }
		 */
		return list;

	}

	@Override
	public Plan findPlanByCode(String code) {

		@SuppressWarnings("unchecked")
		List<Plan> plans = crudService.findWithNamedQuery("Plan.findByCode",
				QueryParameter.with("code", code).parameters(), 1);

		if (Objects.isNull(plans) || plans.isEmpty()) {
			return null;
		}

		return plans.get(0);
	}

	@Override
	public List<Plan> findAllPlan() {

		@SuppressWarnings("unchecked")
		List<Plan> plans = crudService.findWithNamedQuery("Plan.findAll");

		if (Objects.isNull(plans) || plans.isEmpty()) {
			return new ArrayList<>();
		}

		return plans;
	}

	@Override
	public List<Plan> findPlanByType(String code) {

		@SuppressWarnings("unchecked")
		List<Plan> plans = crudService.findWithNamedQuery("Plan.findByType",
				QueryParameter.with("type", code + "%").parameters());

		if (Objects.isNull(plans) || plans.isEmpty()) {
			return new ArrayList<>();
		}

		return plans;
	}

	@Override
	public Organisation getOrganisationBySubscription(String subscription_id) {

		@SuppressWarnings("unchecked")
		List<Organisation> organisations = crudService.findWithNamedQuery("Organisation.findBySubscriptionId",
				QueryParameter.with("id", subscription_id).parameters());

		if (Objects.isNull(organisations) || organisations.isEmpty()) {
			return null;
		}

		return organisations.get(0);
	}

	// filter pagination code ends
	@Override
	public Organisation getOrganisationByCustomer(String customer_id) {

		@SuppressWarnings("unchecked")
		List<Organisation> organisations = crudService.findWithNamedQuery("Organisation.findByCustomerId",
				QueryParameter.with("id", customer_id).parameters());

		if (Objects.isNull(organisations) || organisations.isEmpty()) {
			return null;
		}

		return organisations.get(0);
	}

	@Override
	public ErpConfig findErpConfigUsingUniqueKey(String key, String sno) {

		@SuppressWarnings("unchecked")
		List<ErpConfig> erpConfigs = crudService.findWithNamedQuery("ErpConfig.findByUniqueKey",
				QueryParameter.with("key", key).and("serial", sno).parameters());

		if (Objects.isNull(erpConfigs) || erpConfigs.isEmpty()) {
			return null;
		}

		return erpConfigs.get(0);
	}

	@Override
	public ErpConfig findErpConfigUsingUniqueKey(String key) {

		@SuppressWarnings("unchecked")
		List<ErpConfig> erpConfigs = crudService.findWithNamedQuery("ErpConfig.findByUKey",
				QueryParameter.with("key", key).parameters());

		if (Objects.isNull(erpConfigs) || erpConfigs.isEmpty()) {
			return null;
		}

		return erpConfigs.get(0);
	}

	@Override
	public List<DataUploadInfo> getDataUploadInfoByStatus(String status) {

		@SuppressWarnings("unchecked")
		List<DataUploadInfo> dataUploadInfos = crudService.findWithNamedQuery("DataUploadInfo.findByStatus",
				QueryParameter.with("status", status).parameters());

		if (Objects.isNull(dataUploadInfos) || dataUploadInfos.isEmpty()) {
			return new ArrayList<>();
		}

		return dataUploadInfos;
	}

	@Override
	public List<DataUploadInfo> findDataUploadInfoKey(String[] key, String[] sno) {

		@SuppressWarnings("unchecked")
		List<DataUploadInfo> dataUploadInfos = crudService.findWithNamedQuery("DataUploadInfo.findByMachineKey",
				QueryParameter.with("key", Arrays.asList(key)).and("sno", Arrays.asList(sno)).parameters(), 250);

		if (Objects.isNull(dataUploadInfos) || dataUploadInfos.isEmpty()) {
			return new ArrayList<>();
		}

		return dataUploadInfos;
	}

	@Override
	public List<String> getMonthYearsAsPerPreference(String monthYear, String gstin) throws AppException {
		List<String> monthYears = new ArrayList<>();
		YearMonth yearmonth = YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		TaxpayerGstin taxpayerGstin = this.findTaxpayerGstin(gstin);
		String preference = taxpayerGstin.getFilePreferenceByFinancialYear(fyear);

		if ("Q".equalsIgnoreCase(preference)) {

			if ("082017".equalsIgnoreCase(monthYear) || "092017".equalsIgnoreCase(monthYear)) {
				monthYears.add("082017");
				monthYears.add("092017");
			} else {

				YearMonth yearMonth = Utility.convertStrToYearMonth(monthYear);

				int returnYear = yearMonth.getYear();

				int quarterMonth = Utility.calculateQuarterMonth(yearMonth);

				for (int month = 3; month >= 1; month--)
					monthYears.add(Utility.appendZeroWithMonthYear(quarterMonth--, returnYear));
			}
		} else
			monthYears.add(monthYear);

		log.debug("month year for sync {}  gstin {} fyear {}  preference {} ", monthYears, gstin, fyear, preference);

		return monthYears;
	}

	@Override
	public List getGstinTransactionByGstinMonthYear(GstinTransactionDTO gstinTransactionDTO, FilterDto filter)
			throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {
			String type = gstinTransactionDTO.getTransactionType();
			String queryName = "";
			Class classType = null;
			Map<String, Object> queryParameters = new HashMap<>();
			queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
			queryParameters.put("gstn",
					(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
			queryParameters.put("monthYear", /* gstinTransactionDTO.getMonthYear() */ this.getMonthYearsAsPerPreference(
					gstinTransactionDTO.getMonthYear(), gstinTransactionDTO.getTaxpayerGstin().getGstin()));
			classType = this.getEntityTypeByTransaction(type);

			String query = this.getGstnSelectedFieldsByType(TransactionType.valueOf(type),
					ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType())));

			List gstinTransactions = null;

			if (!StringUtils.isEmpty(query) && !Objects.isNull(classType)) {

				if (TransactionType.B2B.toString().equalsIgnoreCase(type)
						|| TransactionType.B2BA.toString().equalsIgnoreCase(type)) {
					// queryName = "B2bDetailEntity.findNotSynced";
					queryParameters.put("dataSource", DataSource.EMGST);
				}
				if (TransactionType.CDN.toString().equalsIgnoreCase(type)
						|| TransactionType.CDNA.toString().equalsIgnoreCase(type)) {
					// queryName = "CdnDetailEntity.findNotSynced";
					queryParameters.put("dataSource", DataSource.EMGST);
				}

				Map<String, Object> resultMap = new HashMap<>();
				// @SuppressWarnings("unchecked")
				if (!Objects.isNull(filter)) {
					resultMap = this.getFilteredData(gstinTransactionDTO, filter, classType);
					gstinTransactions = (List) resultMap.get("transactionData");
					// gstinTransactions =
					// this.getFilteredData(gstinTransactionDTO,
					// filter, classType);

				} else {

					// gstinTransactions=crudService.findWithNamedQuery(queryName,queryParameters);
					if (TransactionType.B2CS.toString().equalsIgnoreCase(type)
							|| TransactionType.B2CSA.toString().equalsIgnoreCase(type)) {

						gstinTransactions = this.getB2csDataForGstn(query, queryParameters);

					} else 
					{
						Query jpaQuery = em.createQuery(query, classType);
						for (Entry<String, Object> entry : queryParameters.entrySet()) {
							jpaQuery.setParameter(entry.getKey(), entry.getValue());
						}

						gstinTransactions = jpaQuery.getResultList();
						gstinTransactions = this.groupByDetailEntity(gstinTransactions, type);
						resultMap.put("transactionData", gstinTransactions);
						resultMap.put("total_count", null);
					}

				}
			}
			return gstinTransactions;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private <T extends CommonAttributesEntity> List groupByDetailEntity(List<T> datas, String type) {
		List<String> allowedTypes = Arrays.asList(new String[] { "B2B", "B2CL", "CDN", "EXP", "TXPD", "AT", "CDNUR" });
		if (allowedTypes.contains(StringUtils.upperCase(type))) {
			List<T> result = new ArrayList<>();

			Map<String, List<T>> b2bDetails = datas.stream().collect(Collectors.groupingBy(T::getId));
			;

			for (Map.Entry<String, List<T>> ent : b2bDetails.entrySet()) {
				List<Item> items = new ArrayList<>();
				T b2bDetailEntity = null;
				for (T b2b : ent.getValue()) {
					items.addAll(b2b.getItems());
					b2bDetailEntity = b2b;
				}

				b2bDetailEntity.setItems(items);
				result.add(b2bDetailEntity);
			}
			return result;
		} else
			return datas;

	}
	
	private List<DocIssue> getDocIssueForGstn(String query, Map<String, Object> queryParameters){
		
		return null;
		
	}

	@SuppressWarnings("unchecked")
	private List<B2CSTransactionEntity> getB2csDataForGstn(String query, Map<String, Object> queryParameters) {

		Query jpaQuery = em.createQuery(query);

		for (Entry<String, Object> entry : queryParameters.entrySet()) {
			jpaQuery.setParameter(entry.getKey(), entry.getValue());
		}

		List<Object[]> items = jpaQuery.getResultList();
		List<B2CSTransactionEntity> b2css = new ArrayList<>();
		for (Object[] item : items) {
			B2CSTransactionEntity b2cs = new B2CSTransactionEntity();
			Item createdItem = new Item();
			List<Item> itemList = new ArrayList<>();
			itemList.add(createdItem);
			b2cs.setItems(itemList);
			b2cs.setSupplyType((SupplyType) item[0]);
			b2cs.setPos(!Objects.isNull(item[1]) ? (String) item[1] : "");
			b2cs.setFlags(!Objects.isNull(item[2]) ? (String) item[2] : "");
			b2cs.setStateName(!Objects.isNull(item[3]) ? (String) item[3] : "");
			createdItem.setTaxRate(!Objects.isNull(item[4]) ? (Double) item[4] : 0);
			b2cs.setB2csType(!Objects.isNull(item[5]) ? (String) item[5] : "");
			b2cs.setTaxableValue(!Objects.isNull(item[6]) ? (Double) item[6] : 0);
			createdItem.setIgst(!Objects.isNull(item[7]) ? (Double) item[7] : 0);
			createdItem.setCgst(!Objects.isNull(item[8]) ? (Double) item[8] : 0);
			createdItem.setSgst(!Objects.isNull(item[9]) ? (Double) item[9] : 0);
			createdItem.setCess(!Objects.isNull(item[10]) ? (Double) item[10] : 0);
			b2cs.setIsAmmendment(!Objects.isNull(item[11]) ? (boolean) item[11] : Boolean.FALSE);
			if (b2cs.getIsAmmendment()) {
				b2cs.setOriginalMonth(!Objects.isNull(item[13]) ? (String) item[13] : "");
				b2cs.setOriginalPos(!Objects.isNull(item[12]) ? (String) item[12] : "");
			}
			createdItem.setTaxableValue(b2cs.getTaxableValue());

			b2css.add(b2cs);
		}

		return b2css;
	}

	public String buildQueryByType(GstinTransactionDTO gstinTransactionDto) {
		String query = "Select "
				+ this.getGstnSelectedFieldsByType(TransactionType.valueOf(gstinTransactionDto.getTransactionType()),
						ReturnType.valueOf(gstinTransactionDto.getReturnType()))
				+ " from b where ";
		return null;

	}

	public String getGstnSelectedFieldsByType(TransactionType transactionType, ReturnType returnType) {

		Properties props = new Properties();
		InputStream inputStream = null;
		String fileName = "/gstnselectedfields.properties";
		List<String> selectedFields = new ArrayList<>();
		String fields = null;
		try {
			inputStream = DBUtils.class.getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IOException();
			props.load(inputStream);
			/* String */ fields = props
					.getProperty(StringUtils.lowerCase(returnType.toString() + "." + transactionType.toString()));
			/*
			 * if ("all".equalsIgnoreCase(fields)) { selectedFields.add("all"); } else {
			 * StringTokenizer st = new StringTokenizer(fields, ","); while
			 * (st.hasMoreTokens()) { selectedFields.add(st.nextToken()); } }
			 */
		}

		catch (IOException e) {
			log.error("file not found with this name--{}", fileName);
		}

		return fields;

	}

	@Override
	public List<ReturnStatus> getReturnStatusByTransaction(GstinTransactionDTO gstinTransactionDTO,
			String transactionType) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("gstin", gstinTransactionDTO.getTaxpayerGstin().getGstin());
			parameters.put("returnType", gstinTransactionDTO.getReturnType());
			parameters.put("monthYear", gstinTransactionDTO.getMonthYear());
			parameters.put("transType", transactionType);
			parameters.put("status", "PENDING");

			List<ReturnStatus> returnStatus = crudService.findWithNamedQuery("ReturnStatus.findByDataTrans",
					parameters);

			return Objects.isNull(returnStatus) ? null : returnStatus;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public boolean isSubmit(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		
		GstinActionDTO gstinActionDTO = new GstinActionDTO();
		gstinActionDTO.setReturnType(gstinTransactionDTO.getReturnType());
		gstinActionDTO.setParticular(gstinTransactionDTO.getMonthYear());
		gstinActionDTO.setStatus("FILED");

		GstinAction gstinAction = this.findGstinAction(gstinActionDTO,
				gstinTransactionDTO.getTaxpayerGstin().getGstin());
		if (!Objects.isNull(gstinAction))
			throw new AppException(ExceptionCode._ALREADY_SUBMITED);
		return false;
	}

	public boolean isSubmit(String gstin, String monthYear, String returnType) throws AppException {
		YearMonth yearmonth = YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		gstinTransactionDTO.setMonthYear(monthYear);
		gstinTransactionDTO.setReturnType(returnType);
		gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin, fyear));
		return this.isSubmit(gstinTransactionDTO);
	}

	public boolean getSubmitDetails(String gstin, String monthYear, String returnType) throws AppException {
		YearMonth yearmonth = YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear = CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		gstinTransactionDTO.setMonthYear(monthYear);
		gstinTransactionDTO.setReturnType(returnType);
		gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin, fyear));
		boolean isSub = false;
		try {
			this.isSubmit(gstinTransactionDTO);
		} catch (AppException appException) {
			isSub = true;
		}
		return isSub;
	}

	public TokenResponse getGstnDataStatus(GstinTransactionDTO gstinTransactionDTO) {

		List<TokenResponse> tokenResponses = crudService.findWithNamedQuery(
				"TokenResponse.findByGstinMonthYearTransaction",
				QueryParameter
						.with("taxpayerGstin",
								this.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()))
						.and("returnType", gstinTransactionDTO.getReturnType())
						.and("monthYear", gstinTransactionDTO.getMonthYear())
						.and("transactionType", gstinTransactionDTO.getTransactionType()).parameters(),
				1);

		if (tokenResponses != null && !tokenResponses.isEmpty())

			return tokenResponses.get(0);
		return null;
	}

	@SuppressWarnings("unchecked")
	public boolean getDataStatus(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		TokenResponse tokenResponse = this.getGstnDataStatus(gstinTransactionDTO);

		if (!Objects.isNull(tokenResponse)) {
			String status = tokenResponse.getStatus();

			Timestamp updationTime = tokenResponse.getUpdationTime();

			LocalDateTime currentTime = LocalDateTime.now();

			LocalDateTime updationTimeLocal = updationTime.toLocalDateTime();

			if ("COMPLETED".equalsIgnoreCase(status)) {

				Duration duration = Duration.between(updationTimeLocal, LocalDateTime.of(2017, 10, 10, 0, 0));

				if (duration.isNegative())
					throw new AppException("P-40007",
							String.format("Unable to process your request,Your %s data has been already processed. "
									+ GSTNRespStatus.CONTACTSUPPORT_MSG, gstinTransactionDTO.getReturnType()));

			} else if ("pending".equalsIgnoreCase(status)) {

				Duration duration = Duration.between(updationTimeLocal, currentTime);

				if (duration.toMinutes() < 15) {
					throw new AppException(ExceptionCode._PREVIOUS_GET_IN_PROCESS);

				}

			}

		}
		return true;
	}

	@Override
	public GstinAction findGstinActionByMonthYearReturn(String monthYear, String returnType,
			TaxpayerGstin taxpayerGstin) {

		return null;
	}
	
	@Override
	public User findUserIdByUsername(String username) {
		
		return  (User) em.createNamedQuery("User.findIdByUsername").setParameter("username",username).getSingleResult();
		
	}
	
	@Override
	public Organisation findOrganisationByUsername(String username) throws AppException {
		
		User user=this.findUserIdByUsername(username);
		
		if(!Objects.isNull(user)) {
			
			return user.getOrganisation();
		}
		
		throw new AppException("U-4004","Provided user details not found.");
		
	}

	@Override
	public String findLegalName(String panCard) {
		// TODO Auto-generated method stub
		List<String> taxpayerGstin=em.createNamedQuery("TaxpayerGstin.findLegalName").setParameter("pan",panCard).getResultList();
		return taxpayerGstin.get(0);
	}
}
