package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
public @Data class ReturnStatusDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String event;

	private String monthYear;

	private String referenceId;

	private String returnType;

	private String status;

	private String transactionId;
	
	private String transaction;
	
	private String transitId;
	
	private String response;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	private TaxpayerGstin taxpayerGstin;
	
}
