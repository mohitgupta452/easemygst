package com.ginni.easemygst.portal.business.service;

import java.util.List;
import java.util.Map;

import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.Publicnotification;

public interface NotificationService {

	public enum NotificationCategory {
		EMGST_NOTIFICATION("EMGST_NOTIFICATION"), OTHER_NOTIFICATION("OTHER_NOTIFICATION"), GOVT_NOTIFICATION(
				"GOVT_NOTIFICATION"), GOVT_CIRCULAR_NOTIFICATION(
						"GOVT_CIRCULAR_NOTIFICATION"), PRESS_NOTIFICATION("PRESS_NOTIFICATION");
	
		private String type;
	
		private NotificationCategory(String type) {
			this.type = type;
		}
	
		/*
		 * @Override public String toString() { return this.type; };
		 */
	}

	public int updateNotificationAction(String action,String notificationId)throws AppException;

	public Map<String,Object> getNotifications(UserDTO userDto, List<Boolean> isSeens, List<String> categories)
	throws AppException;

	public Map<String,Object> getNotificationCount( List<String> categories, List<Boolean> isSeens) throws AppException;

	public Map<String,Object> getNotificationsAll(UserDTO userDto, List<Boolean> isSeens, List<String> categories,
	FilterDto filter) throws AppException;

	public Map<String,Object> getFilteredNotificationsAll(UserDTO userDto,List<Boolean>isSeens,List<String> categories,FilterDto filter )throws AppException;
	

	public List<Publicnotification> getPublicNotification(int userId);
	public List<String> getSSEBroadcastMessaage();
}
