package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class SmsDTO implements Serializable {

	private int id;

	private Timestamp generationTime;

	private String message;

	private Date modifiedtime;

	private Timestamp processedTime;

	private String remarks;

	private String smsevent;

	private String smssubject;

	private String smstype;

	private int status;

	private String toMobileNumber;
	
	public interface SmsDTOMixIn extends CommonMixin {
		
	}

}
