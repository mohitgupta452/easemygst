package com.ginni.easemygst.portal.business.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.GstnService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata.Returns;
import com.ginni.easemygst.portal.constant.GstMetadata.ReturnsModule;
import com.ginni.easemygst.portal.gst.api.GstConsumer;
import com.ginni.easemygst.portal.gst.request.FileGstr1AReq;
import com.ginni.easemygst.portal.gst.request.FileGstr1Req;
import com.ginni.easemygst.portal.gst.request.FileGstr2Req;
import com.ginni.easemygst.portal.gst.request.FileGstr3Req;
import com.ginni.easemygst.portal.gst.request.GSTR3;
import com.ginni.easemygst.portal.gst.request.GenGstr3Req;
import com.ginni.easemygst.portal.gst.request.GetGstr1AReq;
import com.ginni.easemygst.portal.gst.request.GetGstr1Req;
import com.ginni.easemygst.portal.gst.request.GetGstr2Req;
import com.ginni.easemygst.portal.gst.request.GetGstr2ReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.GetGstr3DetailReq;
import com.ginni.easemygst.portal.gst.request.GetGstr3ReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.GetITCLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.request.GetLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.request.GetReturnStatusReq;
import com.ginni.easemygst.portal.gst.request.Gstr1ASummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr1SummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr2SummaryReq;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest;
import com.ginni.easemygst.portal.gst.request.LiabilityLedger;
import com.ginni.easemygst.portal.gst.request.SaveGstr1AReq;
import com.ginni.easemygst.portal.gst.request.SaveGstr1Req;
import com.ginni.easemygst.portal.gst.request.SaveGstr2Req;
import com.ginni.easemygst.portal.gst.request.SaveGstr3Req;
import com.ginni.easemygst.portal.gst.request.SetOffLiabilityDataReq;
import com.ginni.easemygst.portal.gst.request.SubmitGstr1AReq;
import com.ginni.easemygst.portal.gst.request.SubmitGstr1Req;
import com.ginni.easemygst.portal.gst.request.SubmitGstr2Req;
import com.ginni.easemygst.portal.gst.request.SubmitRefundData;
import com.ginni.easemygst.portal.gst.request.ViewTrackReturnsReq;
import com.ginni.easemygst.portal.gst.response.CashITCBalance;
import com.ginni.easemygst.portal.gst.response.FileGstr1AResp;
import com.ginni.easemygst.portal.gst.response.FileGstr1Resp;
import com.ginni.easemygst.portal.gst.response.FileGstr2Resp;
import com.ginni.easemygst.portal.gst.response.FileGstr3Resp;
import com.ginni.easemygst.portal.gst.response.GenGstr3Resp;
import com.ginni.easemygst.portal.gst.response.GetCashLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusResp;
import com.ginni.easemygst.portal.gst.response.GetCreditLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetGstr1AResp;
import com.ginni.easemygst.portal.gst.response.GetGstr1Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr2ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.gst.response.GetGstr3ReturnStatusResp;
import com.ginni.easemygst.portal.gst.response.GetITCLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetReturnResp;
import com.ginni.easemygst.portal.gst.response.Gstr1ASummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr1SummaryResp;
import com.ginni.easemygst.portal.gst.response.Gstr2SummaryResp;
import com.ginni.easemygst.portal.gst.response.LiabilityLedgerDetail;
import com.ginni.easemygst.portal.gst.response.SaveGstr1AResp;
import com.ginni.easemygst.portal.gst.response.SaveGstr1Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr2Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr3Resp;
import com.ginni.easemygst.portal.gst.response.SaveGstr3bResponse;
import com.ginni.easemygst.portal.gst.response.SubmitGstr1AResp;
import com.ginni.easemygst.portal.gst.response.SubmitGstr1Resp;
import com.ginni.easemygst.portal.gst.response.SubmitGstr2Resp;
import com.ginni.easemygst.portal.gst.response.ViewTrackReturnsResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.service.CrudService;

@Stateless
@Transactional
public class GstnServiceImpl implements GstnService {

	@Inject
	private Logger log;

	@Inject
	private FinderService finderService;

	@Inject
	private CrudService crudService;

	@Inject
	private GstConsumer gstConsumer;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public GetGstr1Resp getGstr1Data(GetGstr1Req getGstr1Req) throws AppException {

		GetGstr1Resp getGstr1Resp = new GetGstr1Resp();
		String response = "";

		if (getGstr1Req != null && getGstr1Req.getGstin() != null) {
			try {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1Req.getMonthYear());
			getGstr1Req.setAction(getGstr1Req.getAction() + "_GET_v1.1");

			response = gstConsumer.returns(getGstr1Req, ReturnsModule.GSTR1);
			
				getGstr1Resp = JsonMapper.objectMapper.readValue(response, GetGstr1Resp.class);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return getGstr1Resp;
	}
	
	
	@Override
	public String getGstr1datafromgstn(GetGstr1Req getGstr1Req) throws AppException {

		GetReturnResp getGstr1Resp = new GetReturnResp();
		String response = "";
		if (getGstr1Req != null && getGstr1Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1Req.getMonthYear());
			getGstr1Req.setAction(getGstr1Req.getAction() + "_GET_v1.1");

			response = gstConsumer.returns(getGstr1Req, ReturnsModule.GSTR1);
			

		}

		return response;
	}
	
	
	@Override
	public SaveGstr1Resp saveGstr1Data(SaveGstr1Req saveGstr1Req) throws AppException {

		SaveGstr1Resp saveGstr1Resp = new SaveGstr1Resp();

		try {
			if (saveGstr1Req != null && saveGstr1Req.getGstin() != null) {

				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(saveGstr1Req.getGstin());

				gstConsumer.setTaxpayerGstin(taxpayerGstin);
				gstConsumer.addTaxpayerHeaders(true);
				gstConsumer.setReturnPeriod(saveGstr1Req.getMonthYear());
				
				String response = gstConsumer.returns(saveGstr1Req, ReturnsModule.GSTR1);
				log.debug("gstr1 response {}", response);
				if (!StringUtils.isEmpty(response)) {
					saveGstr1Resp = JsonMapper.objectMapper.readValue(response, SaveGstr1Resp.class);
					saveGstr1Resp.setGsp(gstConsumer.getGsp());
				}
			}
		} catch (IOException ex) {
			log.error("savegstr1 response io exception {}", ex);
			throw new AppException(ExceptionCode._INVALID_JSON);
		}

		return saveGstr1Resp;
	}

	@Override
	public GetGstr1ReturnStatusResp getGstr1ReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq)
			throws AppException {

		GetGstr1ReturnStatusResp getGstr1ReturnStatusResp = new GetGstr1ReturnStatusResp();

		if (getGstr1ReturnStatusReq != null && getGstr1ReturnStatusReq.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1ReturnStatusReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1ReturnStatusReq.getMonthYear());

			String response = gstConsumer.returns(getGstr1ReturnStatusReq, null);
			try {
				getGstr1ReturnStatusResp = JsonMapper.objectMapper.readValue(response, GetGstr1ReturnStatusResp.class);
			} catch (IOException e) {
				log.error("gstr1returnstatus response io exception {}", e);
				throw new AppException(ExceptionCode._INVALID_JSON);
			}
			System.out.println(response);
		}

		return getGstr1ReturnStatusResp;
	}

	@Override
	public String getGstr1Summary(Gstr1SummaryReq gstr1SummaryReq) throws AppException {

		Gstr1SummaryResp gstr1SummaryResp = new Gstr1SummaryResp();

		if (gstr1SummaryResp != null && gstr1SummaryReq.getGstin() != null
				&& gstr1SummaryReq.getReturnPeriod() != null) {

			gstr1SummaryReq.setAction(Returns.RETSUM.toString());
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstr1SummaryReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(gstr1SummaryReq.getReturnPeriod());

			String response = gstConsumer.returns(gstr1SummaryReq, ReturnsModule.GSTR1);
			System.out.println(response);
			return response;
		}
		return null;

	}
	
	@Override
	public String getGstrSummary(Gstr1SummaryReq gstr1SummaryReq,ReturnsModule module) throws AppException {

		Gstr1SummaryResp gstr1SummaryResp = new Gstr1SummaryResp();

		if (gstr1SummaryResp != null && gstr1SummaryReq.getGstin() != null
				&& gstr1SummaryReq.getReturnPeriod() != null) {

			gstr1SummaryReq.setAction(Returns.RETSUM.toString());
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstr1SummaryReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(gstr1SummaryReq.getReturnPeriod());

			String response = gstConsumer.returns(gstr1SummaryReq, module);
			System.out.println(response);
			return response;
		}
		return null;

	}
	
	
	
	
	
	
	
	
	

	@Override
	public SubmitGstr1Resp submitGstr1Data(SubmitGstr1Req submitGstr1Req) throws AppException {
		SubmitGstr1Resp submitGstr1Resp = new SubmitGstr1Resp();

		if (submitGstr1Req != null && submitGstr1Req.getGstin() != null && submitGstr1Req.getRetPeriod() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(submitGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(submitGstr1Req.getRetPeriod());
			String response = gstConsumer.returns(submitGstr1Req, ReturnsModule.GSTR1);
			System.out.println(response);
			try {
				submitGstr1Resp = JsonMapper.objectMapper.readValue(response, SubmitGstr1Resp.class);
			} catch (Exception e) {
				log.error("exception while submit", e);
			}
		}

		return submitGstr1Resp;
	}

	@Override
	public FileGstr1Resp fileGstr1Return(FileGstr1Req fileGstr1Req) throws AppException {

		FileGstr1Resp fileGstr1Resp = new FileGstr1Resp();

		if (fileGstr1Req != null && fileGstr1Req.getGstin() != null && fileGstr1Req.getRetPeriod() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(fileGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(fileGstr1Req.getRetPeriod());
			String response = gstConsumer.returns(fileGstr1Req, ReturnsModule.GSTR1);
			System.out.println(response);
			try {
				fileGstr1Resp = JsonMapper.objectMapper.readValue(response, FileGstr1Resp.class);
			} catch (Exception e) {
				log.error("exception while file", e);
			}
		}
		return fileGstr1Resp;
	}

	@Override
	public GetGstr1AResp getGstr1AData(GetGstr1AReq getGstr1AReq) throws AppException {

		GetGstr1AResp getGstr1AResp = new GetGstr1AResp();

		if (getGstr1AReq != null && getGstr1AReq.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1AReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1AReq.getMonthYear());

			String response = gstConsumer.returns(getGstr1AReq, ReturnsModule.GSTR1A);
			System.out.println(response);
		}

		return getGstr1AResp;
	}

	@Override
	public SaveGstr1AResp saveGstr1AData(SaveGstr1AReq saveGstr1AReq) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Gstr1ASummaryResp getGstr1ASummary(Gstr1ASummaryReq gstr1ASummaryReq) throws AppException {

		Gstr1ASummaryResp gstr1ASummaryResp = new Gstr1ASummaryResp();

		if (gstr1ASummaryResp != null && gstr1ASummaryReq.getGstin() != null
				&& gstr1ASummaryReq.getReturnPeriod() != null) {

			gstr1ASummaryReq.setAction(Returns.RETSUM.toString());
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstr1ASummaryReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);

			String response = gstConsumer.returns(gstr1ASummaryReq, ReturnsModule.GSTR1A);
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.has("gstr1Asummary")) {
				JSONArray jsonArray = jsonObject.getJSONArray("gstr1Asummary");
				if (jsonArray.length() > 0) {
					try {
						System.out.println(jsonArray.get(0).toString());
						gstr1ASummaryResp = JsonMapper.objectMapper.readValue(jsonArray.get(0).toString(),
								Gstr1ASummaryResp.class);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			System.out.println(response);
		}

		return gstr1ASummaryResp;
	}

	@Override
	public FileGstr1AResp fileGstr1AReturn(FileGstr1AReq gstr1aSummaryReq) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SubmitGstr1AResp submitGstr1AData(SubmitGstr1AReq submitGstr1AReq) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGstr2Data(GetGstr2Req getGstr2Req) throws AppException {
		String response = "";
		GetGstr2Resp getGstr2Resp = new GetGstr2Resp();

		if (getGstr2Req != null && getGstr2Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr2Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr2Req.getMonthYear());
			if (getGstr2Req.getToken() != null)
				response = gstConsumer.returns(getGstr2Req, null);
			else if (getGstr2Req.getEndpoint() != null) {
				getGstr2Req.setMonthYear(null);
				getGstr2Req.setGstin(null);
				response = gstConsumer.returns(getGstr2Req, null);
			} else
				response = gstConsumer.returns(getGstr2Req,
						ReturnsModule.valueOf(getGstr2Req.getReturnType().toString()));

			System.out.println(response);
		}

		return response;
	}

	@Override
	public String getGstr1DataActionRequired(GetGstr2Req getGstr2Req) throws AppException {
		String response = "";

		if (getGstr2Req != null && getGstr2Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr2Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr2Req.getMonthYear());
			/*
			 * if(getGstr2Req.getToken()!=null) response =
			 * gstConsumer.returns(getGstr2Req,null); else
			 * if(getGstr2Req.getEndpoint()!=null){ getGstr2Req.setMonthYear(null);
			 * getGstr2Req.setGstin(null); response = gstConsumer.returns(getGstr2Req,null);
			 * } else
			 */
			response = gstConsumer.returns(getGstr2Req, ReturnsModule.valueOf(getGstr2Req.getReturnType().toString()));

			System.out.println(response);
		}

		return response;
	}

	@Override // temp not being used
	public String getGstr1DataNew(GetGstr1Req getGstr1Req) throws AppException {
		String response = "";

		/*
		 * if (getGstr2Req != null && getGstr2Req.getGstin() != null) {
		 * getGstr2Req.setActionReq("Y"); TaxpayerGstin taxpayerGstin =
		 * finderService.findTaxpayerGstin(getGstr2Req.getGstin());
		 * gstConsumer.setTaxpayerGstin(taxpayerGstin);
		 * gstConsumer.addTaxpayerHeaders(true);
		 * gstConsumer.setReturnPeriod(getGstr2Req.getMonthYear());
		 * 
		 * if(getGstr2Req.getToken()!=null) response =
		 * gstConsumer.returns(getGstr2Req,null); else
		 * if(getGstr2Req.getEndpoint()!=null){ getGstr2Req.setMonthYear(null);
		 * getGstr2Req.setGstin(null); response = gstConsumer.returns(getGstr2Req,null);
		 * } else response = gstConsumer.returns(getGstr2Req,
		 * ReturnsModule.valueOf(getGstr2Req.getReturnType().toString()));
		 * 
		 * System.out.println(response); }
		 */
		if (getGstr1Req != null && getGstr1Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1Req.getMonthYear());
			response = gstConsumer.returns(getGstr1Req, ReturnsModule.valueOf(getGstr1Req.getReturnType().toString()));

			System.out.println(response);
		}

		return response;
	}

	@Override
	public InputStream getGstr2AUrlData(GetGstr2Req getGstr2Req) throws AppException {
		InputStream response = null;
		GetGstr2Resp getGstr2Resp = new GetGstr2Resp();

		if (getGstr2Req != null && getGstr2Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr2Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr2Req.getMonthYear());

			getGstr2Req.setMonthYear(null);
			getGstr2Req.setGstin(null);

			response = gstConsumer.returnsUrlData(getGstr2Req, null);

			System.out.println(response);
		}

		return response;
	}

	@Override
	public String getGstr1AData(GetGstr1Req getGstr1Req) throws AppException {
		String response = "";

		if (getGstr1Req != null && getGstr1Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1Req.getMonthYear());
			response = gstConsumer.returns(getGstr1Req, ReturnsModule.valueOf(getGstr1Req.getReturnType().toString()));
			System.out.println(response);
		}

		return response;
	}

	@Override
	public SaveGstr2Resp saveGstr2Data(SaveGstr2Req saveGstr2Req) throws AppException {
		// TODO Auto-generated method stub
		SaveGstr2Resp saveGstr2Resp = new SaveGstr2Resp();

		try {
			if (saveGstr2Req != null && saveGstr2Req.getGstin() != null) {

				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(saveGstr2Req.getGstin());
				gstConsumer.setTaxpayerGstin(taxpayerGstin);
				gstConsumer.addTaxpayerHeaders(true);
				gstConsumer.setReturnPeriod(saveGstr2Req.getMonthYear());

				String response = gstConsumer.returns(saveGstr2Req, ReturnsModule.GSTR2);
				saveGstr2Resp = JsonMapper.objectMapper.readValue(response, SaveGstr2Resp.class);
			}
		} catch (IOException ex) {
			log.debug("exception errror {} " + ex);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return saveGstr2Resp;
	}

	@Override
	public Gstr2SummaryResp getGstr2Summary(Gstr2SummaryReq gstr2SummaryReq) throws AppException {

		Gstr2SummaryResp gstr2SummaryResp = new Gstr2SummaryResp();

		if (gstr2SummaryResp != null && gstr2SummaryReq.getGstin() != null
				&& gstr2SummaryReq.getReturnPeriod() != null) {

			gstr2SummaryReq.setAction(Returns.RETSUM.toString());
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstr2SummaryReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);

			String response = gstConsumer.returns(gstr2SummaryReq, ReturnsModule.GSTR2);
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.has("gstr2summary")) {
				JSONArray jsonArray = jsonObject.getJSONArray("gstr2summary");
				if (jsonArray.length() > 0) {
					try {
						System.out.println(jsonArray.get(0).toString());
						gstr2SummaryResp = JsonMapper.objectMapper.readValue(jsonArray.get(0).toString(),
								Gstr2SummaryResp.class);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			System.out.println(response);
		}

		return gstr2SummaryResp;
	}

	@Override
	public SubmitGstr2Resp submitGstr2Data(SubmitGstr2Req submitGstr2Req) throws AppException {
		SubmitGstr2Resp submitGstr2Resp = new SubmitGstr2Resp();

		if (submitGstr2Req != null && submitGstr2Req.getGstin() != null && submitGstr2Req.getRetPeriod() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(submitGstr2Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(submitGstr2Req.getRetPeriod());
			String response = gstConsumer.returns(submitGstr2Req, ReturnsModule.GSTR2);
			System.out.println(response);
			try {
				submitGstr2Resp = JsonMapper.objectMapper.readValue(response, SubmitGstr2Resp.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return submitGstr2Resp;
	}

	@Override
	public FileGstr2Resp fileGstr2Return(FileGstr2Req gstr2SummaryReq) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenGstr3Resp genGstr3Data(GenGstr3Req genGstr3Req) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetGstr3Resp getGstr3Data(GSTR3 getGstr3Req) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveGstr3Resp saveGstr3Data(SaveGstr3Req saveGstr3Req) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FileGstr3Resp fileGstr3Return(FileGstr3Req fileGstr3Req) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CashITCBalance getCashLedgerDetailsReq(GetLedgerDetailsReq getCashLedgerDetailsReq)
			throws AppException {
		CashITCBalance cashLedgerDetailsresponse=new CashITCBalance();
		if (!Objects.isNull(getCashLedgerDetailsReq)&& !StringUtils.isEmpty(getCashLedgerDetailsReq.getGstin())) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getCashLedgerDetailsReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			getCashLedgerDetailsReq.setAction(getCashLedgerDetailsReq.getAction() + "_GET_v0.3");
	/*		gstConsumer.setReturnPeriod(submitGstr2Req.getRetPeriod());*/
			String response = gstConsumer.returns(getCashLedgerDetailsReq, ReturnsModule.Ledgers);
			System.out.println(response);
			try {
				cashLedgerDetailsresponse = JsonMapper.objectMapper.readValue(response, CashITCBalance.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return cashLedgerDetailsresponse;
	}
	
	
	@Override
	public GetCreditLedgerDetailsResp getCreditLedgerDetailsReq(GetLedgerDetailsReq getCashLedgerDetailsReq)
			throws AppException {
		GetCreditLedgerDetailsResp creditLedgerDetailsresponse=new GetCreditLedgerDetailsResp();
		if (!Objects.isNull(getCashLedgerDetailsReq)&& !StringUtils.isEmpty(getCashLedgerDetailsReq.getGstin())) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getCashLedgerDetailsReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			getCashLedgerDetailsReq.setAction(getCashLedgerDetailsReq.getAction() + "_GET_v0.3");
	/*		gstConsumer.setReturnPeriod(submitGstr2Req.getRetPeriod());*/
			String response = gstConsumer.returns(getCashLedgerDetailsReq, ReturnsModule.Ledgers);
			System.out.println(response);
			try {
				creditLedgerDetailsresponse = JsonMapper.objectMapper.readValue(response, GetCreditLedgerDetailsResp.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return creditLedgerDetailsresponse;
	}
	
	
	@Override
	public GetITCLedgerDetailsResp getITCLedgerDetailsReq(GetITCLedgerDetailsReq getITCLedgerDetailsReq)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LiabilityLedgerDetail getLiabilityLedgerDetailsReq(LiabilityLedger getLiabilityLedgerDetailsReq)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetGstr2ReturnStatusReq getGstr2ReturnStatus(GetGstr2ReturnStatusResp getGstr2ReturnStatusResp)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetGstr3ReturnStatusReq getGstr3ReturnStatus(GetGstr3ReturnStatusResp getGstr3ReturnStatusResp)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGstr2ReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException {
		String response = null;

		GetCheckStatusGstr2Resp getGstr2ReturnStatusResp = new GetCheckStatusGstr2Resp();

		if (getGstr1ReturnStatusReq != null && getGstr1ReturnStatusReq.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1ReturnStatusReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1ReturnStatusReq.getMonthYear());

			response = gstConsumer.returns(getGstr1ReturnStatusReq, null);
			try {
				getGstr2ReturnStatusResp = JsonMapper.objectMapper.readValue(response, GetCheckStatusGstr2Resp.class);
			} catch (IOException e) {
				log.error("io exception ", e);
				throw new AppException();
			}
			System.out.println(response);
		}

		return response;
	}

	public String getGst1ReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException {

		GetCheckStatusResp getGstr2ReturnStatusResp = new GetCheckStatusResp();
		String response = null;
		if (getGstr1ReturnStatusReq != null && getGstr1ReturnStatusReq.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1ReturnStatusReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1ReturnStatusReq.getMonthYear());

			response = gstConsumer.returns(getGstr1ReturnStatusReq, null);
		}
		return response;

	}

	@Override
	public String getGstr3bReturnStatus(GetReturnStatusReq getGstr1ReturnStatusReq) throws AppException {

		String response = null;
		if (getGstr1ReturnStatusReq != null && getGstr1ReturnStatusReq.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1ReturnStatusReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1ReturnStatusReq.getMonthYear());

			response = gstConsumer.returns3bStatus(getGstr1ReturnStatusReq, null);
		}
		return response;

	}

	@Override
	public String getGstr2DataByToken(GetGstr2Req getGstr2Req) throws AppException {
		String response = "";
		GetGstr2Resp getGstr2Resp = new GetGstr2Resp();

		if (getGstr2Req != null && getGstr2Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr2Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr2Req.getMonthYear());

			response = gstConsumer.returnsGet(getGstr2Req,
					ReturnsModule.valueOf(getGstr2Req.getReturnType().toString()));
			System.out.println(response);
		}

		return response;
	}

	public String generateGstr3(GenGstr3Req genGstr3Req) throws AppException {
		String response = "";
		if (genGstr3Req != null && genGstr3Req.getGstin() != null) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(genGstr3Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(genGstr3Req.getMonthYear());
			response = gstConsumer.returns(genGstr3Req, ReturnsModule.GSTR3);
			log.debug(response);
		}
		return response;
	}

	public String getGstr3Details(GetGstr3DetailReq getGstr3DetailReq) throws AppException {
		String response = "";
		if (getGstr3DetailReq != null && getGstr3DetailReq.getGstin() != null) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr3DetailReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr3DetailReq.getMonthYear());
			response = gstConsumer.returns(getGstr3DetailReq, ReturnsModule.GSTR3);
			log.debug(response);
		}
		return response;
	}

	public String setoffLiabilities(SetOffLiabilityDataReq setOffReq) throws AppException {
		String response = "";
		if (setOffReq != null) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(setOffReq.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(setOffReq.getMonthYear());
			response = gstConsumer.returns(setOffReq, ReturnsModule.GSTR3);
			log.debug(response);
		}
		return response;

	}

	@Override
	public String submitRefund(SubmitRefundData submitrefund) throws AppException {
		String response = "";
		if (submitrefund != null) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(submitrefund.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(submitrefund.getMonthYear());
			response = gstConsumer.returns(submitrefund, ReturnsModule.GSTR3);
			log.debug(response);
		}
		return response;
	}

	public String getSearchTaxpayerResult(GetGstr1Req getGstr1Req) throws AppException {
		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1Req.getGstin());
		gstConsumer.setTaxpayerGstin(taxpayerGstin);
		gstConsumer.addTaxpayerHeaders(true);
		// gstConsumer.setReturnPeriod("072017");

		String response = gstConsumer.returnsSearchTPData(getGstr1Req, null);
		return response;
	}

	@Override
	public SaveGstr3bResponse saveGstr3bData(Gstr3BRequest saveGstr3bReq) throws AppException {
		// TODO Auto-generated method stub
		SaveGstr3bResponse saveGstr3bResp = new SaveGstr3bResponse();

		try {
			if (saveGstr3bReq != null && saveGstr3bReq.getGstin() != null) {

				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(saveGstr3bReq.getGstin());
				gstConsumer.setTaxpayerGstin(taxpayerGstin);
				gstConsumer.addTaxpayerHeaders(true);
				gstConsumer.setReturnPeriod(saveGstr3bReq.getReturnPeriod());
				String response = gstConsumer.returns(saveGstr3bReq, ReturnsModule.GSTR3B);
				saveGstr3bResp = JsonMapper.objectMapper.readValue(response, SaveGstr3bResponse.class);
			}
		} catch (IOException ex) {
			log.debug("exception errror {} " + ex);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return saveGstr3bResp;
	}

	@Override
	public String getGstr3bData(GetGstr1Req getGstr1Req) throws AppException {
		String response = "";

		if (getGstr1Req != null && getGstr1Req.getGstin() != null) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(getGstr1Req.getGstin());
			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(true);
			gstConsumer.setReturnPeriod(getGstr1Req.getMonthYear());
			response = gstConsumer.returns(getGstr1Req, ReturnsModule.valueOf(getGstr1Req.getReturnType().toString()));
			System.out.println(response);
		}

		return response;
	}
	
public 	ViewTrackReturnsResp viewTrackReturns(ViewTrackReturnsReq trackReturnsReq) throws AppException {
	
	ViewTrackReturnsResp trackReturnsResp=null;
	if (trackReturnsReq != null && trackReturnsReq.getGstin() != null) {

		this.setGstHeaders(trackReturnsReq.getGstin(), trackReturnsReq.getReturnPeriod());
	   String	response = gstConsumer.returns(trackReturnsReq,null);
	   
	   try {
		log.debug("view track request :  {} response {}",_MAPPER.writeValueAsString(trackReturnsReq),
				   response);
	} catch (JsonProcessingException e) {
	log.error("json excption while parsing view track returns request/response",e);
	}
	   try {
		trackReturnsResp=_MAPPER.readValue(response, ViewTrackReturnsResp.class);
	} catch (IOException e) {
		log.error("json excption while parsing view track returns response {}",e.getMessage());
	}
	}

	return trackReturnsResp;
	}

private void setGstHeaders(String gstin,String monthYear) throws AppException {
	
	TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
	gstConsumer.setTaxpayerGstin(taxpayerGstin);
	gstConsumer.addTaxpayerHeaders(true);
	gstConsumer.setReturnPeriod(monthYear);
}
	public void getRefreshedToken() {
		
	}
	
	public void getAuthTokenForRefersh(String sek) {
		
		
		
	}

}
