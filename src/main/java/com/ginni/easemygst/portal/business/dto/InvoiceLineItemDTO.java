package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class InvoiceLineItemDTO {

	private String serialNo;
	
	private String itemDesc;
	
	private String remark;
	
	private String hsn;
	
	private String unit;
	
	private String qty;
	
	private String rateItem;
	
	private String discount;
	
	private String taxableValue;
	
	private String taxRate;
	
	private String taxAmount;
	
}
