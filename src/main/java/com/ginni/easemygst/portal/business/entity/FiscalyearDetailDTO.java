package com.ginni.easemygst.portal.business.entity;

import lombok.Data;

public @Data class FiscalyearDetailDTO {

	private String gstin;
	
	private String preference;
	
	private String monthyear;
	
	private Double previousGt;

	private Double currentGt;
	
	
	
}
