package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public UserDTO() {
		
	}
	
	public UserDTO(Integer id, String username, String password, String firstName, String lastName, Byte isChangeRequired, String requestId, String preferredGsp) {
		this.id = id;
		this.username = username;
		this.password = password;	
		this.firstName = firstName;
		this.lastName = lastName;
		this.isChangeRequired = isChangeRequired;
		this.requestId = requestId;
		this.preferredGsp = preferredGsp;
	}

	private Integer id;

	private Byte active;
	
	private Byte isChangeRequired;

	private String facebookId;

	private String firstName;

	private String googleId;

	private String lastName;

	private String password;

	private String status;
	
	private String aadharNo;

	private String username;
	
	private String requestId;
	
	private String preferredGsp;
	
	private String creationIpAddress;
	
	private PartnerDTO partner;
	
	private OrganisationDTO organisation;
	
	private List<UserContactDTO> userContacts;
	
	private SkuPartnerDTO skuPartner;
	
	private Byte billToPartner;
	
}
