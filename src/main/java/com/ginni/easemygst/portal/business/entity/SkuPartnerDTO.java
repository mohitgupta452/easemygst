package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

public @Data class SkuPartnerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;

	private Byte active;

	private String code;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String description;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	private SkuDTO sku;

	public interface SkuPartnerDTOMixIn extends CommonMixin {
		
	}

}
