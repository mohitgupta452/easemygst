package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class UserLastlogDTO implements Serializable {

	private int id;

	private Timestamp lastLogin;

	private String lastLoginIpAddress;

	private String lastNotificationIpAddress;

	private Timestamp lastNotificationTime;
	
	public interface UserLastlogDTOMixIn extends CommonMixin {
		
	}

}
