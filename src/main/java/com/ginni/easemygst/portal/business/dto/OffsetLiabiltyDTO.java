package com.ginni.easemygst.portal.business.dto;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;

import lombok.Data;

public @Data class OffsetLiabiltyDTO {

	private OffsetDetails otherThanRC;
	
	private OffsetDetails  revCharge;
	
    public OffsetLiabiltyDTO(){
    	otherThanRC=new OffsetDetails();
    	revCharge=new OffsetDetails(); 
	}
	
	public @Data static class OffsetDetails{
		
	private TaxDetail integratedtax;
	private TaxDetail centraltax;
	private TaxDetail statetax;
	private TaxDetail cesstax;
	
	public OffsetDetails(){
	integratedtax=new TaxDetail();
	 centraltax=new TaxDetail();
	 statetax=new TaxDetail();
	 cesstax=new TaxDetail();
	}
		
	}
	public @Data static class TaxDetail{
		
		private BigDecimal taxliabilty=BigDecimal.ZERO;
		
		private BigDecimal igst=BigDecimal.ZERO;
		
		private BigDecimal cgst=BigDecimal.ZERO;
		
		private BigDecimal sgst=BigDecimal.ZERO;
		
		private BigDecimal cess=BigDecimal.ZERO;
	
		private BigDecimal more=BigDecimal.ZERO;
		
		private BigDecimal less=BigDecimal.ZERO;
		
		private BigDecimal unutilisedCash=BigDecimal.ZERO;
		
		private BigDecimal addtionalCashReq=BigDecimal.ZERO; 
		
		
	public TaxDetail(String type,BigDecimal taxliabilty,BigDecimal igst,BigDecimal cgst,BigDecimal sgst,BigDecimal cess,BigDecimal unutilisedCash){
		this.taxliabilty=taxliabilty;
		this.igst=igst;
		this.cgst=cgst;
		this.sgst=sgst;
		this.cess=cess;
		BigDecimal tempvalue=this.getvalue(type);
		if(this.taxliabilty.compareTo(this.getvalue(type))<0){
		this.more=tempvalue.subtract(taxliabilty);
		}
		else{
		this.less=taxliabilty.subtract(tempvalue);
		}
		this.unutilisedCash=unutilisedCash;
		this.addtionalCashReq=this.taxliabilty.subtract(this.igst.add(this.cgst).add(this.sgst).add(this.cess));
		
		
		}
	public TaxDetail(BigDecimal taxliabilty,BigDecimal igst,BigDecimal cgst,BigDecimal sgst,BigDecimal cess,BigDecimal unutilisedCash){
		this.taxliabilty=taxliabilty;
		this.igst=igst;
		this.cgst=cgst;
		this.sgst=sgst;
		this.cess=cess;
		this.unutilisedCash=unutilisedCash;
		this.addtionalCashReq=this.taxliabilty.subtract(this.igst.add(this.cgst).add(this.sgst).add(this.cess).add(unutilisedCash));
		}
	
	public BigDecimal getvalue(String type){
		BigDecimal amount=BigDecimal.ZERO;
		  Method m;
		try {
			m = this.getClass().getDeclaredMethod("get"+type);
		   amount= (BigDecimal) m.invoke(this);
		    return amount;
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException | NoSuchMethodException e) {
		
			e.printStackTrace();
		}
		return amount;
	}


	public TaxDetail() {
		// TODO Auto-generated constructor stub
	}
		
	}

}
