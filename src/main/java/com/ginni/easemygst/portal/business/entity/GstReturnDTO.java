package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import lombok.Data;

public @Data class GstReturnDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Byte active;
	
	private Byte mandatory;

	private String code;
	
	private String frequency;
	
	private Date dueDate;
	
	private Date startDate;
	
	private String preProcess;

	private String createdBy;

	private Timestamp creationTime;

	private String description;

	private String name;
	
	private int dependent;
	
	private int lockedAfter;
	
	private int prevMonth;
	
	private Byte isParent;

	private String status;

	private String updatedBy;

	private Timestamp updationTime;
	
	private String creationIpAddress;
	
	private String updationIpAddress;
	
	public interface GstReturnDTOMixIn extends CommonMixin {
		
	}
}
