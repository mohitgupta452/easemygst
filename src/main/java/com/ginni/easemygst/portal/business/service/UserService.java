package com.ginni.easemygst.portal.business.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.ContactUsDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface UserService {

	public UserDTO createUser(UserDTO userDTO, RegisterUsing registerUsing) throws AppException;

	public boolean isIdentityExists(String identity);

	public UserDTO getUserInfo(String identity);

	public UserDTO getUserProfileInfo(int id);

	public Map<String, Object> getUserOrganisations(int id) throws AppException;

	public UserDTO activateUserEmail(int id);

	public boolean isActiveIdentityExists(int id);

	public boolean changePassword(String identity, String oldPass, String newPass) throws AppException;

	public boolean resetPassword(String identity, String newPass) throws AppException;

	public boolean resetPassword(String identity, String newPass, String requestedBy) throws AppException;

	public String forgotPassword(String identity, String ipAddress) throws AppException;

	public boolean saveContactUsData(ContactUsDTO contactUsDTO) throws AppException;

	public UserDTO verifyActivateAccount(String username, String firstName, String lastName, String mobileNumber,
			String password) throws AppException;

	public UserDTO checkValidOrganisation(Integer organisationId) throws AppException;

	public UserDTO validateUser(String username, String password, RegisterUsing registerUsing) throws AppException;

	public boolean createOrganisation(String name) throws AppException;

	public boolean renameOrganisation(String name) throws AppException;

	public UserDTO secureValidateUser(String username, String password, RegisterUsing registerUsing)
			throws AppException;

	public enum RegisterUsing {

		EASEMYGST("easemygst"), FACEBOOK("facebook"), GOOGLE("google");

		RegisterUsing(String name) {
			this.name = name;
		}

		private String name;

		@Override
		public String toString() {
			return this.name;
		};
	}

	public enum UserStatus {

		ACTIVE("ACTIVE"), INACTIVE("INACTIVE"), HOLD("HOLD");

		UserStatus(String status) {
			this.status = status;
		}

		private String status;

		@Override
		public String toString() {
			return this.status;
		};
	}

	public boolean checkGstins() throws AppException;

	public boolean checkUsers() throws AppException;

	public boolean createUserAccount(String username, String name, String legalName) throws AppException;

	public UserDTO getUserProfileInfo(String username) throws AppException;
	
	public List<String> getGstinbyPanCard(String pancard) throws AppException;
	

	String findUserType(String GSTIN, int userId);

	String findAccessType(String GSTIN, int userId, String returntype);

}
