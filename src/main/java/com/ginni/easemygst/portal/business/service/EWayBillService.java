package com.ginni.easemygst.portal.business.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.data.view.Response;
import com.ginni.easemygst.portal.helper.AppException;

public interface EWayBillService {
	
	public enum EWB_EVENT{
		AUTHENTICATE,
		GENERATE
		
	}
	
	ObjectMapper _MAPPER = new ObjectMapper();
	
	public String registerForEWB(String gstin,String userName,String password,Boolean isActive);
	
	public Response generateEWayBill(JsonNode requestNode) throws AppException, Exception;

	JsonNode getEWBTinDetails(String gstin) throws AppException;

}
