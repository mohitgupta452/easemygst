package com.ginni.easemygst.portal.business.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.ejb.AccessTimeout;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.AcceptWithItcDto;
import com.ginni.easemygst.portal.business.dto.AcceptWithItcDto.ItemDto;
import com.ginni.easemygst.portal.business.dto.B2bDetailEntityWrapper;
import com.ginni.easemygst.portal.business.dto.BulkItcDto;
import com.ginni.easemygst.portal.business.dto.CdnDetailEntityWrapper;
import com.ginni.easemygst.portal.business.dto.ChallanDTO;
import com.ginni.easemygst.portal.business.dto.ChangeStatusDto;
import com.ginni.easemygst.portal.business.dto.ChangeStatusDto.OperationType;
import com.ginni.easemygst.portal.business.dto.CtinInvoiceDto;
import com.ginni.easemygst.portal.business.dto.DashBoardSummaryDto;
import com.ginni.easemygst.portal.business.dto.DataStatusDTO;
import com.ginni.easemygst.portal.business.dto.FlagsCounterDTO;
import com.ginni.easemygst.portal.business.dto.GstCalendarDto;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.Gstr3Dto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.BaseAmountDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.InterestAndLateFeePaybleDto.IntrLateFeeDetailDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.ItcEligibilityDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.ItcEligibilityDto.ItcDetailDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto.SupplyDetailDto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ItcAmountDto;
import com.ginni.easemygst.portal.business.dto.ItcAmountDto.PendingItcAmt;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.MismatchCountDto;
import com.ginni.easemygst.portal.business.dto.MonthSummaryDTO;
import com.ginni.easemygst.portal.business.dto.OffsetLiabiltyDTO;
import com.ginni.easemygst.portal.business.dto.OffsetLiabiltyDTO.OffsetDetails;
import com.ginni.easemygst.portal.business.dto.OffsetLiabiltyDTO.TaxDetail;
import com.ginni.easemygst.portal.business.dto.OutsInsSupplyDto;
import com.ginni.easemygst.portal.business.dto.ReturnHistoryDTO;
import com.ginni.easemygst.portal.business.dto.ReturnSummaryDTO;
import com.ginni.easemygst.portal.business.dto.SearchTaxpayerDTO;
import com.ginni.easemygst.portal.business.dto.TaxAmount;
import com.ginni.easemygst.portal.business.dto.TaxPaidDto;
import com.ginni.easemygst.portal.business.dto.TaxSummaryDTO;
import com.ginni.easemygst.portal.business.dto.TodDto;
import com.ginni.easemygst.portal.business.dto.TotalTaxLiabilityDto;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.dto.TransactionalDetailDTO;
import com.ginni.easemygst.portal.business.dto.WrapperDashBoardSummaryDto;
import com.ginni.easemygst.portal.business.entity.BrtMappingDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.ErpTransactionDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinActionDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.business.service.GccService;
import com.ginni.easemygst.portal.business.service.LedgerService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.TaxpayerService.ReturnCalendar;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.business.util.GstReturnComparator;
import com.ginni.easemygst.portal.business.util.ReturnHistoryComparator;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.Constant.GstReturnFrequency;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.helper.Gstr3Transactions;
import com.ginni.easemygst.portal.data.part.ClaimData;
import com.ginni.easemygst.portal.data.part.CreditDetails;
import com.ginni.easemygst.portal.data.part.Export;
import com.ginni.easemygst.portal.data.part.ExportDetails;
import com.ginni.easemygst.portal.data.part.Gstr3Summary;
import com.ginni.easemygst.portal.data.part.Gstr3Summary.Gstr3TransactionSummary;
import com.ginni.easemygst.portal.data.part.INS;
import com.ginni.easemygst.portal.data.part.ITCCredit;
import com.ginni.easemygst.portal.data.part.Imp;
import com.ginni.easemygst.portal.data.part.ImportDetails;
import com.ginni.easemygst.portal.data.part.InterStateSupplies;
import com.ginni.easemygst.portal.data.part.InterStateSupplyInward;
import com.ginni.easemygst.portal.data.part.IntrSupCon;
import com.ginni.easemygst.portal.data.part.IntrSupRec;
import com.ginni.easemygst.portal.data.part.IntrSupReg;
import com.ginni.easemygst.portal.data.part.IntraStateSupplies;
import com.ginni.easemygst.portal.data.part.ItraSupCon;
import com.ginni.easemygst.portal.data.part.ItraSupReg;
import com.ginni.easemygst.portal.data.part.OUTS;
import com.ginni.easemygst.portal.data.part.RFCLM;
import com.ginni.easemygst.portal.data.part.RevInvoiceOUTDetails;
import com.ginni.easemygst.portal.data.part.RevisionInvoiceOUTS;
import com.ginni.easemygst.portal.data.part.TCSCredit;
import com.ginni.easemygst.portal.data.part.TDSCredit;
import com.ginni.easemygst.portal.data.part.TOD;
import com.ginni.easemygst.portal.data.part.TTL;
import com.ginni.easemygst.portal.data.part.TaxLiabilityOUTS;
import com.ginni.easemygst.portal.data.part.TaxPaidA;
import com.ginni.easemygst.portal.data.part.TaxPaidA.TaxPaidAByCash;
import com.ginni.easemygst.portal.data.part.TaxPaidA.TaxPaidAByItcCredit;
import com.ginni.easemygst.portal.data.part.TaxPaidA.TxPay;
import com.ginni.easemygst.portal.data.part.TaxPaidB;
import com.ginni.easemygst.portal.data.part.TaxPaidB.TaxPaidBByCash;
import com.ginni.easemygst.portal.data.part.TaxPaidB.TaxPaidBPayble;
import com.ginni.easemygst.portal.data.part.TotalTaxLiabilityDetails;
import com.ginni.easemygst.portal.data.view.deserializer.Gstr3bDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.reco.B2bRecoDetailDeserializer;
import com.ginni.easemygst.portal.data.view.deserializer.reco.CdnRecoDetailDeserializer;
import com.ginni.easemygst.portal.data.view.serializer.reco.B2BRecoDetailSerializer;
import com.ginni.easemygst.portal.data.view.serializer.reco.CDNRecoDetailSerializer;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.gst.request.GetLedgerDetailsReq;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.OutwardSupplyDetail;
import com.ginni.easemygst.portal.gst.response.CashBal;
import com.ginni.easemygst.portal.gst.response.CashITCBalance;
import com.ginni.easemygst.portal.gst.response.ClBal;
import com.ginni.easemygst.portal.gst.response.GSTNRespStatus;
import com.ginni.easemygst.portal.gst.response.GetCashLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusGstr2Resp;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusResp;
import com.ginni.easemygst.portal.gst.response.GetCreditLedgerDetailsResp;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.gst.response.ItcBal;
import com.ginni.easemygst.portal.gst.response.OCBal;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.DataFilter;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.ErpTransaction;
import com.ginni.easemygst.portal.persistence.entity.GstCalendar;
import com.ginni.easemygst.portal.persistence.entity.GstReturn;
import com.ginni.easemygst.portal.persistence.entity.GstinAction;
import com.ginni.easemygst.portal.persistence.entity.GstinTransaction;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.Summary3bdata;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew;
import com.ginni.easemygst.portal.persistence.entity.TrackReturnsInfo;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ExceptionSheet;
import com.ginni.easemygst.portal.transaction.factory.Transaction.FilterType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ItcEligibilityType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.transaction.impl.TransactionUtility;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

import redis.clients.util.RedisOutputStream;

@Stateless
@Transactional
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ReturnsServiceImpl implements ReturnsService {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private UserBean userBean;

	@Inject
	private FinderService finderService;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private FunctionalService functionalService;

	@Inject
	private ApiLoggingService apiLoggingService;

	@Inject
	private RedisService redisService;
	
	@Inject
	private LedgerService ledgerService;

	/*
	 * @Resource ManagedExecutorService executor;
	 */

	private ExecutorService executor = Executors.newFixedThreadPool(2);

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	protected final Logger _Logger = LoggerFactory.getLogger(this.getClass());

	private void processAndStoreTransaction(String monthYear, String gstReturn, String transaction, String gstin,
			String transactions) throws AppException {

		TransactionFactory transactionFactory = new TransactionFactory(transaction);
		YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		gstinTransactionDTO.setMonthYear(monthYear);
		gstinTransactionDTO.setReturnType(gstReturn);
		gstinTransactionDTO.setTransactionType(transaction);
		gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

		// String existingData =
		// getExistingGstinTransaction(gstinTransactionDTO);
		// if (!StringUtils.isEmpty(existingData)) {
		// transactions =
		// transactionFactory.processTransactionData(transactions, existingData,
		// TaxpayerAction.MODIFYBYNO);
		// } else {
		// transactions =
		// transactionFactory.processTransactionData(transactions, "[]", null);
		// }
		//
		// gstinTransactionDTO.setTransactionObject(transactions);
		// storeTransactionData(gstinTransactionDTO);
	}

	private int processCompositeData(GstExcel gstExcel, String gstReturn, String columns, String gstin,
			String monthYear, String transaction, String sheetName) throws AppException {

		int rowCount = 0;

		// call TransactionUtility and iterate over map to get work done
		TransactionUtility tu = new TransactionUtility();
		JSONObject output = null;// tu.processExcelList(gstExcel, gstReturn,
									// columns, gstin, monthYear, transaction,
									// sheetName);

		if (!Objects.isNull(output)) {

			if (output.has("b2b")) {
				processAndStoreTransaction(monthYear, gstReturn, "b2b", gstin, output.getString("b2b"));
			}

			if (output.has("b2cl")) {
				processAndStoreTransaction(monthYear, gstReturn, "b2cl", gstin, output.getString("b2cl"));
			}

			if (output.has("b2cs")) {
				processAndStoreTransaction(monthYear, gstReturn, "b2cs", gstin, output.getString("b2cs"));
			}

			if (output.has("size")) {
				rowCount = output.getInt("size");
			}
		}

		return rowCount;
	}

	public int processCompositeData(String gstReturn, String gstin, String monthYear, String request)
			throws AppException {

		int rowCount = 0;

		// call TransactionUtility and iterate over map to get work done
		TransactionUtility tu = new TransactionUtility();
		JSONObject output = null;/// tu.processJsonData(gstReturn, gstin,
									/// monthYear,request);

		if (!Objects.isNull(output)) {

			if (output.has("b2b")) {
				processAndStoreTransaction(monthYear, gstReturn, "b2b", gstin, output.getString("b2b"));
			}

			if (output.has("b2cl")) {
				processAndStoreTransaction(monthYear, gstReturn, "b2cl", gstin, output.getString("b2cl"));
			}

			if (output.has("b2cs")) {
				processAndStoreTransaction(monthYear, gstReturn, "b2cs", gstin, output.getString("b2cs"));
			}

			if (output.has("size")) {
				rowCount = output.getInt("size");
			}
		}

		return rowCount;

	}

	private <T> T saveOrUpdate(Class calsses, String className, Object updatebleObj, Object referencedObj) {

		if (referencedObj != null) {
			Method[] methods = calsses.getMethods();

			for (Method method : methods) {
				String methodName = method.getName();
				try {
					if (methodName.startsWith("get") && method.invoke(calsses.cast(updatebleObj)) != null) {
						// Class classes= method.getReturnType();

						if (methodName.contains("B2bDetails") || methodName.contains("CdnDetails")) {
							List updatableObjs = (List) method.invoke(calsses.cast(updatebleObj));

							List childReferences = (List) method.invoke(referencedObj);

							for (Object childUpdatable : updatableObjs) {
								if (childReferences.contains(childUpdatable)) {
									this.saveOrUpdate(childUpdatable.getClass(), className, childUpdatable,
											childReferences.get(childReferences.indexOf(childUpdatable)));
								}
								// else
								// {
								// childReferences.add(childUpdatable);
								// }
							}
							continue;

						}

						String setterMehodName = methodName.replace("get", "set");

						Method setterMehod = referencedObj.getClass().getMethod(setterMehodName,
								method.getReturnType());

						setterMehod.invoke(calsses.cast(referencedObj), method.invoke(calsses.cast(updatebleObj)));

					}
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
			log.debug("cast class name....{}-----parent name.....{} ", calsses.cast(referencedObj).getClass().getName(),
					className);
			if (calsses.cast(referencedObj).getClass().getName().equalsIgnoreCase(className)) {

				crudService.update(calsses.cast(referencedObj));
				System.out.println("updated.......");
			}
		} else {
			em.persist(updatebleObj);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private B2BTransactionEntity getB2bTransaction(B2BTransactionEntity b2bTransactionEntity) {

		List<B2BTransactionEntity> b2bTransactionEntities = crudService.findWithNamedQuery(
				"B2bTransaction.getByEmbeded",
				QueryParameter.with("taxPayerGstin", b2bTransactionEntity.getTaxpayerGstin())
						.and("ctin", b2bTransactionEntity.getCtin())
						.and("returnType", b2bTransactionEntity.getReturnType())
						.and("monthYear", b2bTransactionEntity.getMonthYear()).parameters());

		if (!b2bTransactionEntities.isEmpty() && b2bTransactionEntities.size() > 0)
			return b2bTransactionEntities.get(0);
		return null;
	}

	@SuppressWarnings("unchecked")
	private CDNTransactionEntity getCdnTransaction(CDNTransactionEntity cdnTransaction) {
		List<CDNTransactionEntity> cdnTransactionEntities = crudService.findWithNamedQuery(
				"CdnTransaction.getByEmbeded",
				QueryParameter.with("taxPayerGstin", cdnTransaction.getGstin())
						.and("originalCtin", cdnTransaction.getOriginalCtin())
						.and("returnType", cdnTransaction.getReturnType())
						.and("monthYear", cdnTransaction.getMonthYear()).parameters());

		if (!cdnTransactionEntities.isEmpty() && cdnTransactionEntities.size() > 0)
			return cdnTransactionEntities.get(0);
		return null;
	}

	@SuppressWarnings("unchecked")
	private B2CLTransactionEntity getB2clTransaction(B2CLTransactionEntity b2clTransaction) {
		List<B2CLTransactionEntity> b2clTransactionEntities = crudService.findWithNamedQuery(
				"B2clTransaction.getByEmbeded",
				QueryParameter.with("taxPayerGstin", b2clTransaction.getGstin())
						.and("returnType", b2clTransaction.getReturnType())
						.and("monthYear", b2clTransaction.getMonthYear()).parameters());

		if (!b2clTransactionEntities.isEmpty() && b2clTransactionEntities.size() > 0)
			return b2clTransactionEntities.get(0);
		return null;
	}

	private Object findExistin(Object object) {
		if (object instanceof B2BTransactionEntity) {
			return this.getB2bTransaction((B2BTransactionEntity) object);
		} else if (object instanceof CDNTransactionEntity)
			return this.getCdnTransaction((CDNTransactionEntity) object);
		else if (object instanceof B2CLTransactionEntity)
			return this.getB2clTransaction((B2CLTransactionEntity) object);

		return null;
	}

	private B2BTransactionEntity storeTransactionData(List transactionEntity) throws AppException {

		// Class transactionClass= transactionEntity.get(0).getClass();
		// B2bDetail b2bDetail =new B2bDetail();
		em.setFlushMode(FlushModeType.COMMIT);

		// B2BTransactionEntity b2bTransactionEntity=null;
		// boolean count=false;
		int count = 0;

		for (Object transaction : transactionEntity) {

			// B2BTransactionEntity
			// updatebleObj=((B2BTransactionEntity)transaction);

			// Object existingObj=this.findExistin(transaction);

			// this.saveOrUpdate(transaction.getClass(),transaction.getClass().getName(),transaction,existingObj);

			em.persist(transaction);

			if (++count == 1000) {
				em.flush();
				em.clear();
			}

			// B2bTransaction
			// b2bTransaction=crudService.create(transactionEntity);
		}
		em.flush();
		log.debug("b2b transaction created");
		return null;

	}
	/*
	 * 
	 * try { if (!Objects.isNull(gstinTransactionDTO) &&
	 * !Objects.isNull(gstinTransactionDTO.getTransactionObject())) {
	 * 
	 * TaxpayerGstin taxpayerGstin = null; TaxpayerGstinDTO taxpayerGstinDTO =
	 * taxpayerService
	 * .getTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()); if
	 * (!Objects.isNull(taxpayerGstinDTO)) { taxpayerGstin =
	 * em.getReference(TaxpayerGstin.class, taxpayerGstinDTO.getId()); } else {
	 * throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
	 * 
	 * String type = gstinTransactionDTO.getTransactionType(); GstinTransaction
	 * gstinTransaction = finderService.getGstinTransaction(gstinTransactionDTO,
	 * false); gstinTransactionDTO.setTransactionType("SUMMARY"); GstinTransaction
	 * gstinTransactionSummary =
	 * finderService.getGstinTransaction(gstinTransactionDTO, false);
	 * gstinTransactionDTO.setTransactionType(type); GstinTransaction
	 * gstinTransactionMismatch =
	 * finderService.getGstinTransaction(gstinTransactionDTO, true);
	 * 
	 * GstrSummaryDTO gstrSummaryDTO = new GstrSummaryDTO(); if
	 * (!Objects.isNull(gstinTransactionSummary)) { gstrSummaryDTO =
	 * JsonMapper.objectMapper.readValue(gstinTransactionSummary.
	 * getTransactionObject(), GstrSummaryDTO.class); }
	 * 
	 * String transactions = gstinTransactionDTO.getTransactionObject();
	 * 
	 * TransactionFactory factory = new
	 * TransactionFactory(gstinTransactionDTO.getTransactionType());
	 * 
	 * Map<String, Object> tempMap = new HashMap<>(); tempMap.put("data",
	 * transactions); tempMap.put("summary", gstrSummaryDTO); tempMap.put("return",
	 * gstinTransactionDTO.getReturnType());
	 * 
	 * // validate transaction data starts String trans = (String)
	 * factory.validateData(JsonMapper.objectMapper.writeValueAsString(tempMap),
	 * gstinTransactionDTO.getMonthYear()); JSONObject jsonObject = new
	 * JSONObject(trans); transactions = jsonObject.get("data") != null ?
	 * jsonObject.get("data").toString() : "[]";
	 * gstinTransactionDTO.setTransactionObject(transactions); // validate
	 * transaction data ends
	 * 
	 * boolean isReconciled = false; String reconciled = ""; String transData = "";
	 * // compare transactions starts if
	 * (gstinTransactionDTO.getReturnType().equalsIgnoreCase(ReturnType.GSTR2.
	 * toString()) && (gstinTransactionDTO.getTransactionType().equalsIgnoreCase(
	 * TransactionType.B2B.toString()) || gstinTransactionDTO.getTransactionType()
	 * .equalsIgnoreCase(TransactionType.CDN.toString()))) { String supplier =
	 * finderService.getCtinTransaction(gstinTransactionDTO,
	 * ReturnType.GSTR1.toString()); reconciled =
	 * factory.compareTransactions(transactions, supplier, ViewType1.BUYER);
	 * isReconciled = true; } else if
	 * (gstinTransactionDTO.getReturnType().equalsIgnoreCase(ReturnType.GSTR1.
	 * toString()) && (gstinTransactionDTO.getTransactionType().equalsIgnoreCase(
	 * TransactionType.B2B.toString()) || gstinTransactionDTO.getTransactionType()
	 * .equalsIgnoreCase(TransactionType.CDN.toString()))) { String buyer =
	 * finderService.getCtinTransaction(gstinTransactionDTO,
	 * ReturnType.GSTR2.toString()); reconciled = factory.compareTransactions(buyer,
	 * transactions, ViewType1.SUPPLIER); isReconciled = true; } // compare
	 * transactions ends
	 * 
	 * // store compared/reconciled data starts if (isReconciled) {
	 * 
	 * JSONObject recoJson = new JSONObject(reconciled); transData =
	 * recoJson.get("data") != null ? recoJson.get("data").toString() : "[]";
	 * gstinTransactionDTO.setTransactionObject(transData); reconciled =
	 * recoJson.get("reconcile") != null ? recoJson.get("reconcile").toString() :
	 * "[]";
	 * 
	 * int missingCount = recoJson.has("missing") && recoJson.get("missing") != null
	 * ? recoJson.getInt("missing") : 0;
	 * 
	 * if (!Objects.isNull(gstinTransactionMismatch)) {
	 * gstinTransactionMismatch.setTransactionObject(reconciled);
	 * gstinTransactionMismatch.setUpdatedBy(userBean.getUserDto().getFirstName( ) +
	 * "_" + userBean.getUserDto().getLastName());
	 * gstinTransactionMismatch.setUpdationIpAddress(userBean.getIpAddress());
	 * crudService.update(gstinTransactionMismatch); } else {
	 * 
	 * gstinTransactionMismatch = (GstinTransaction)
	 * EntityHelper.convert(gstinTransactionDTO, GstinTransaction.class);
	 * gstinTransactionMismatch.setTransactionObject(reconciled);
	 * gstinTransactionMismatch
	 * .setTransactionType(gstinTransactionDTO.getTransactionType() + "-MISMATCH");
	 * gstinTransactionMismatch.setTaxpayerGstin(taxpayerGstin);
	 * 
	 * gstinTransactionMismatch.setCreatedBy(userBean.getUserDto().getFirstName( ) +
	 * "_" + userBean.getUserDto().getLastName());
	 * gstinTransactionMismatch.setCreationIpAddress(userBean.getIpAddress());
	 * 
	 * crudService.create(gstinTransactionMismatch); }
	 * 
	 * Map<String, Object> tempMap1 = new HashMap<>(); tempMap1.put("data",
	 * transData); tempMap1.put("summary", gstrSummaryDTO); tempMap1.put("missing",
	 * missingCount); tempMap1.put("return", gstinTransactionDTO.getReturnType());
	 * // validate transaction data starts trans = (String)
	 * factory.validateData(JsonMapper.objectMapper.writeValueAsString(tempMap1) ,
	 * gstinTransactionDTO.getMonthYear()); jsonObject = new JSONObject(trans);
	 * transactions = jsonObject.get("data") != null ?
	 * jsonObject.get("data").toString() : "[]";
	 * gstinTransactionDTO.setTransactionObject(transactions); // validate
	 * transaction data ends
	 * 
	 * } else { transData = transactions;
	 * gstinTransactionDTO.setTransactionObject(transData); } // store
	 * compared/reconciled data ends
	 * 
	 * // store summary data starts String summ = jsonObject.get("summary") != null
	 * ? jsonObject.get("summary").toString() : "[]"; if
	 * (!Objects.isNull(gstinTransactionSummary)) {
	 * gstinTransactionSummary.setTransactionObject(summ);
	 * gstinTransactionSummary.setUpdatedBy(userBean.getUserDto().getFirstName() +
	 * "_" + userBean.getUserDto().getLastName());
	 * gstinTransactionSummary.setUpdationIpAddress(userBean.getIpAddress());
	 * crudService.update(gstinTransactionSummary); } else { gstinTransactionSummary
	 * = new GstinTransaction();
	 * gstinTransactionSummary.setTaxpayerGstin(taxpayerGstin);
	 * gstinTransactionSummary.setReturnType(gstinTransactionDTO.getReturnType() );
	 * gstinTransactionSummary.setTransactionType("SUMMARY");
	 * gstinTransactionSummary.setMonthYear(gstinTransactionDTO.getMonthYear());
	 * gstinTransactionSummary.setTransactionObject(summ);
	 * 
	 * gstinTransactionSummary.setCreatedBy(userBean.getUserDto().getFirstName() +
	 * "_" + userBean.getUserDto().getLastName());
	 * gstinTransactionSummary.setCreationIpAddress(userBean.getIpAddress());
	 * 
	 * 
	 * crudService.create(gstinTransactionSummary); } // store summary data ends
	 * 
	 * // storing actual transaction data starts if
	 * (!Objects.isNull(gstinTransaction)) {
	 * gstinTransaction.setTransactionObject(gstinTransactionDTO.
	 * getTransactionObject());
	 * gstinTransaction.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" +
	 * userBean.getUserDto().getLastName());
	 * gstinTransaction.setUpdationIpAddress(userBean.getIpAddress());
	 * 
	 * crudService.update(gstinTransaction); } else {
	 * 
	 * gstinTransaction = (GstinTransaction)
	 * EntityHelper.convert(gstinTransactionDTO, GstinTransaction.class);
	 * gstinTransaction.setTaxpayerGstin(taxpayerGstin);
	 * gstinTransaction.setCreatedBy(userBean.getUserDto().getFirstName() + "_" +
	 * userBean.getUserDto().getLastName());
	 * gstinTransaction.setCreationIpAddress(userBean.getIpAddress());
	 * crudService.create(gstinTransaction); } // storing actual transaction data
	 * ends
	 * 
	 * } else { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } } catch
	 * (IOException e) { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
	 */

	private void storeTransactionErpData(ErpTransactionDTO erpTransactionDTO,String fyear) throws AppException {

		if (!Objects.isNull(erpTransactionDTO) && !Objects.isNull(erpTransactionDTO.getTransactionObject())) {

			TaxpayerGstin taxpayerGstin = null;
			TaxpayerGstinDTO taxpayerGstinDTO = taxpayerService
					.getTaxpayerGstin(erpTransactionDTO.getTaxpayerGstin().getGstin(),fyear);
			if (!Objects.isNull(taxpayerGstinDTO)) {
				taxpayerGstin = em.getReference(TaxpayerGstin.class, taxpayerGstinDTO.getId());
			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

			ErpTransaction erpTransaction = finderService.getErpTransaction(erpTransactionDTO);

			// storing erp transaction data starts
			if (!Objects.isNull(erpTransaction)) {
				erpTransaction.setTransactionObject(erpTransactionDTO.getTransactionObject());
				erpTransaction
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				erpTransaction.setCreationIpAddress(userBean.getIpAddress());
				crudService.update(erpTransaction);
			} else {
				erpTransaction
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				erpTransaction.setCreationIpAddress(userBean.getIpAddress());
				erpTransaction = (ErpTransaction) EntityHelper.convert(erpTransactionDTO, ErpTransaction.class);
				erpTransaction.setTaxpayerGstin(taxpayerGstin);
				crudService.create(erpTransaction);
			}
			// storing erp transaction data ends

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private String getExistingGstinTransaction(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			GstinTransaction gstinTransaction = finderService.getGstinTransaction(gstinTransactionDTO, false);

			if (!Objects.isNull(gstinTransaction))
				return gstinTransaction.getTransactionObject();
			else
				return null;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private String getExistingErpTransaction(ErpTransactionDTO erpTransactionDTO) throws AppException {

		if (!Objects.isNull(erpTransactionDTO)) {

			ErpTransaction erpTransaction = finderService.getErpTransaction(erpTransactionDTO);

			if (!Objects.isNull(erpTransaction))
				return erpTransaction.getTransactionObject();
			else
				return null;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean addTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			String existingData = getExistingGstinTransaction(gstinTransactionDTO);
			if (!StringUtils.isEmpty(existingData)) {
				transactions = transactionFactory.processTransactionData(transactions, existingData,
						TaxpayerAction.MODIFY);
			} else {
				transactions = transactionFactory.processTransactionData(transactions, "[]", null);
			}

			gstinTransactionDTO.setTransactionObject(transactions);
			// storeTransactionData(gstinTransactionDTO);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			String existingData = getExistingGstinTransaction(gstinTransactionDTO);
			if (!StringUtils.isEmpty(existingData)) {
				transactions = transactionFactory.processTransactionData(transactions, existingData,
						TaxpayerAction.MODIFY);
			} else {
				transactions = transactionFactory.processTransactionData(transactions, "[]", null);
			}

			gstinTransactionDTO.setTransactionObject(transactions);
			// sstoreTransactionData(gstinTransactionDTO);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());

			Object ct = transactionFactory.saveTransactionData(gstinTransactionDTO, null);
			/*
			 * ct.setCreatedBy(userBean.getUserDto().getFirstName() + "_" +
			 * userBean.getUserDto().getLastName());
			 * ct.setCreationIpAddress(userBean.getIpAddress()); ct.setCreationTime(new
			 * Timestamp(Instant.now().toEpochMilli()));
			 */
			crudService.create(ct);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean storeTransactionDataGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			/// storeTransactionData(gstinTransactionDTO);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateTransactionDataModifyByNo(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			String existingData = getExistingGstinTransaction(gstinTransactionDTO);
			if (!StringUtils.isEmpty(existingData)) {
				transactions = transactionFactory.processTransactionData(transactions, existingData,
						TaxpayerAction.MODIFYBYNO);
			} else {
				transactions = transactionFactory.processTransactionData(transactions, "[]", null);
			}

			gstinTransactionDTO.setTransactionObject(transactions);
			// storeTransactionData(gstinTransactionDTO);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateTransactionErpData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {
			if (!StringUtils.isEmpty(gstinTransactionDTO.getTransactionObject())) {
				ErpTransactionDTO erpTransactionDTO = new ErpTransactionDTO();
				erpTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
				erpTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
				erpTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
				erpTransactionDTO.setTransactionObject(gstinTransactionDTO.getTransactionObject());
				erpTransactionDTO.setTransactionType(gstinTransactionDTO.getTransactionType());

				// will change this
				erpTransactionDTO.setReturnType(erpTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

				TransactionFactory transactionFactory = new TransactionFactory(erpTransactionDTO.getTransactionType());
				String transactions = erpTransactionDTO.getTransactionObject();

				String existingData = getExistingErpTransaction(erpTransactionDTO);
				if (!StringUtils.isEmpty(existingData)) {
					transactions = transactionFactory.processTransactionData(transactions, existingData,
							TaxpayerAction.MODIFYBYNO);
				} else {
					transactions = transactionFactory.processTransactionData(transactions, "[]", null);
				}

				erpTransactionDTO.setTransactionObject(transactions);
				 YearMonth yearmonth=YearMonth.parse(gstinTransactionDTO.getMonthYear(), DateTimeFormatter.ofPattern("MMyyyy"));
					String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				storeTransactionErpData(erpTransactionDTO,fyear);

				return true;
			} else {
				return false;
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateTransactionItcData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			String existingData = getExistingGstinTransaction(gstinTransactionDTO);
			if (!StringUtils.isEmpty(existingData)) {
				transactions = transactionFactory.processTransactionData(transactions, existingData,
						TaxpayerAction.UPDATE_ITC);
			} else {
				transactions = transactionFactory.processTransactionData(transactions, "[]", null);
			}

			gstinTransactionDTO.setTransactionObject(transactions);
			// storeTransactionData(gstinTransactionDTO);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateRecoTypeTransactionData(GstinTransactionDTO gstinTransactionDTO, String recoType)
			throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			if (!StringUtils.isEmpty(recoType) && TaxpayerAction.valueOf(recoType) != null) {

				TaxpayerAction action = TaxpayerAction.valueOf(recoType);

				String existingData = getExistingGstinTransaction(gstinTransactionDTO);
				if (!StringUtils.isEmpty(existingData)) {
					transactions = transactionFactory.processTransactionData(transactions, existingData, action);
				} else {
					transactions = transactionFactory.processTransactionData(transactions, "[]", null);
				}

				gstinTransactionDTO.setTransactionObject(transactions);
				// storeTransactionData(gstinTransactionDTO);

				return true;
			} else {
				return false;
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateRecoTypeTransactionDataByNo(GstinTransactionDTO gstinTransactionDTO, String recoType)
			throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			if (!StringUtils.isEmpty(recoType) && TaxpayerAction.valueOf(recoType) != null) {

				TaxpayerAction action = TaxpayerAction.valueOf(recoType);

				String existingData = getExistingGstinTransaction(gstinTransactionDTO);
				if (!StringUtils.isEmpty(existingData)) {
					transactions = transactionFactory.processTransactionData(transactions, existingData, action);
				} else {
					transactions = transactionFactory.processTransactionData(transactions, "[]", null);
				}

				gstinTransactionDTO.setTransactionObject(transactions);
				// storeTransactionData(gstinTransactionDTO);

				return true;
			} else {
				return false;
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateReconciledTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {
			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			String transactions = gstinTransactionDTO.getTransactionObject();

			String existingData = getExistingGstinTransaction(gstinTransactionDTO);
			if (!StringUtils.isEmpty(existingData)) {
				transactions = transactionFactory.processTransactionData(transactions, existingData,
						TaxpayerAction.RECONCILE);
			} else {
				transactions = transactionFactory.processTransactionData(transactions, "[]", TaxpayerAction.RECONCILE);
			}

			gstinTransactionDTO.setTransactionObject(transactions);
			// storeTransactionData(gstinTransactionDTO);

			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public int deleteTransaction(String gstin, String monthYear, TransactionType transactionType, ReturnType returnType,
			List<String> invoices, DeleteType deleteType, DataSource dataSource) throws AppException {
		String deleteSelected = null;
		String deleteAllQuery = null;
		String updateQuery = null;
		String deleteTransaction = null;
		String updateById = null;
		Map<String, Object> params = new HashMap<>();
		switch (transactionType) {
		case AT:
			deleteSelected = "ATTransactionEntity.removeById";
			deleteAllQuery = "ATTransactionEntity.deleteByMonthYear";
			updateQuery = "ATTransactionEntity.updateFlagByMonthYear";
			updateById = "ATTransactionEntity.updateFlagById";
			break;
		case B2B:
			deleteSelected = "B2BDetailEntity.removeById";
			deleteAllQuery = "B2BDetail.deleteByMonthYear";
			updateQuery = "B2BDetail.updateFlagByMonthYear";
			deleteTransaction = "B2BTransactionEntity.removeByMonthYear";
			updateById = "B2BDetailEntity.updateFlagById";

			break;
		case B2CL:
			deleteSelected = "B2CLDetailEntity.removeById";
			deleteAllQuery = "B2CLDetailEntity.deleteByMonthYear";
			updateQuery = "B2CLDetailEntity.updateFlagByMonthYear";
			deleteTransaction = "B2CLTransactionEntity.removeByMonthYear";
			updateById = "B2CLDetailEntity.updateFlagById";

			break;
		case B2CS:

			deleteSelected = "B2CSTransactionEntity.removeById";
			deleteAllQuery = "B2CSTransactionEntity.deleteByMonthYear";
			updateQuery = "B2CSTransactionEntity.updateFlagByMonthYear";
			updateById = "B2CSTransactionEntity.updateFlagById";

			break;
		case CDN:
			deleteSelected = "CDNDetailEntity.removeById";
			deleteAllQuery = "CDNDetailEntity.deleteByMonthYear";
			updateQuery = "CDNDetailEntity.updateFlagByMonthYear";
			deleteTransaction = "CDNTransactionEntity.removeByMonthYear";
			updateById = "CDNDetailEntity.updateFlagById";

			break;
		case CDNUR:
			deleteSelected = "CDNURDetailEntity.removeById";
			deleteAllQuery = "CDNURDetailEntity.deleteByMonthYear";
			updateQuery = "CDNURDetailEntity.updateFlagByMonthYear";
			deleteTransaction = "CDNURTransactionEntity.removeByMonthYear";
			updateById = "CDNURDetailEntity.updateFlagById";

			break;
		case EXP:
			deleteSelected = "ExpDetail.removeById";
			deleteAllQuery = "ExpDetail.deleteByMonthYear";
			updateQuery = "ExpDetail.updateFlagByMonthYear";
			deleteTransaction = "EXPTransactionEntity.removeByMonthYear";
			updateById = "ExpDetail.updateFlagById";

			break;
		case NIL:
			deleteSelected = "NILTransactionEntity.removeById";
			deleteAllQuery = "NILTransactionEntity.deleteByMonthYear";
			updateQuery = "NILTransactionEntity.updateFlagByMonthYear";
			updateById = "NILTransactionEntity.updateFlagById";

			break;
		case TXPD:
			deleteSelected = "TxpdTransaction.removeById";
			deleteAllQuery = "TxpdTransaction.deleteByMonthYear";
			updateQuery = "TxpdTransaction.updateFlagByMonthYear";
			updateById = "TxpdTransaction.updateFlagById";

			break;
		case HSNSUM:
			deleteSelected = "HsnTransaction.removeById";
			deleteAllQuery = "HsnTransaction.deleteByMonthYear";
			updateQuery = "HsnTransaction.updateFlagByMonthYear";
			// updateById="HsnTransaction.updateFlagById";

			break;
		case B2BUR:
			deleteSelected = "B2bUrTransactionEntity.removeById";
			deleteAllQuery = "B2bUrTransactionEntity.deleteByMonthYear";
			updateQuery = "B2bUrTransactionEntity.updateFlagByMonthYear";
			updateById = "B2bUrTransactionEntity.updateFlagById";

			break;
		case IMPG:
			deleteSelected = "IMPGTransactionEntity.removeById";
			deleteAllQuery = "IMPGTransactionEntity.deleteByMonthYear";
			updateQuery = "IMPGTransactionEntity.updateFlagByMonthYear";
			updateById = "IMPGTransactionEntity.updateFlagById";
			break;
		case IMPS:
			deleteSelected = "IMPSTransactionEntity.removeById";
			deleteAllQuery = "IMPSTransactionEntity.deleteByMonthYear";
			updateQuery = "IMPSTransactionEntity.updateFlagByMonthYear";
			updateById = "IMPSTransactionEntity.updateFlagById";
			break;

		case DOC_ISSUE:
			deleteSelected = "DocIssue.removeById";
			deleteAllQuery = "DocIssue.deleteByMonthYear1";
			updateQuery = "DocIssue.updateFlagByMonthYear";
			updateById = "DocIssue.updateFlagById";
			break;

		default:
			break;
		}
		int count = 0;
		if (deleteType == DeleteType.SELECTED) {

			if (!StringUtils.isEmpty(deleteSelected)) {

				if (invoices != null && !invoices.isEmpty()) {
					Query query = em.createNamedQuery(deleteSelected).setParameter("ids", invoices);
					if (transactionType == TransactionType.B2B || transactionType == TransactionType.CDN)
						query.setParameter("dataSource", dataSource);
					count = query.executeUpdate();
				}

				if (!StringUtils.isEmpty(updateById)) {
					Query query = em.createNamedQuery(updateById).setParameter("ids", invoices);
					if (transactionType == TransactionType.B2B || transactionType == TransactionType.CDN)
						query.setParameter("dataSource", dataSource);
					count = query.executeUpdate();
				}

			}

		} else if (deleteType == DeleteType.ALL) {

			Query query = em.createNamedQuery(deleteAllQuery);
			if (transactionType == TransactionType.B2B || transactionType == TransactionType.CDN)
				query.setParameter("dataSource", dataSource);
			this.executeQuery(gstin, monthYear, returnType, query);

			query = em.createNamedQuery(updateQuery);
			if (transactionType == TransactionType.B2B || transactionType == TransactionType.CDN)
				query.setParameter("dataSource", dataSource);
			count = this.executeQuery(gstin, monthYear, returnType, query);

		}
		
		if(returnType==ReturnType.GSTR2) {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		 GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		 gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
		 gstinTransactionDTO.setMonthYear(monthYear);
		 gstinTransactionDTO.setTransactionType(transactionType.toString());
		 gstinTransactionDTO.setReturnType(returnType.toString());
		this.compareTransactions(gstinTransactionDTO);
		}

		return count;
	}

	@Override
	public int deleteTransactionData(String gstin, String monthYear, TransactionType transactionType,
			ReturnType returnType, List<String> invoices, DeleteType deleteType, DataSource dataSource)
			throws Exception {

		if (!finderService.isSubmit(gstin, monthYear, returnType.toString())) {

			int count = this.deleteTransaction(gstin, monthYear, transactionType, returnType, invoices, deleteType,
					dataSource);

			// if (!Objects.isNull(userBean) && !Objects.isNull(userBean.getUserDto())) {
			// if (count != 0) {
			// log.debug("not deleted but updated {}", count);
			// GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			// gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin));
			// gstinTransactionDTO.setMonthYear(monthYear);
			// gstinTransactionDTO.setTransactionType(transactionType.toString());
			// gstinTransactionDTO.setReturnType(returnType.toString());
			// gstinTransactionDTO.setTaxpayerAction(TaxpayerAction.DELETE);
			// if (returnType == ReturnType.GSTR1)
			// this.syncData(gstinTransactionDTO);
			// if (returnType == ReturnType.GSTR2)
			// functionalService.saveGstr2DataToGstn(gstinTransactionDTO);
			// }
			// }
			return count;
		}
		return 0;

	}

	private int executeQuery(String gstin, String monthYear, ReturnType returnType, Query query) {
		int count = query.setParameter("gstin", gstin).setParameter("monthYear", monthYear)
				.setParameter("returnType", returnType.toString()).executeUpdate();

		log.debug("updation count on delete operation : {}", count);
		return count;

	}

	private boolean genrateForm3b(String gstin) throws AppException {
		if (!StringUtils.isEmpty(gstin)) {

			Query query = em.createNativeQuery("{call generate_form3b(:gstin)}");
			query.setParameter("gstin", gstin);
			log.debug("update rows count {}", query.executeUpdate());
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return true;
	}

	public boolean genrateHsnSummary(GstinTransactionDTO gstinTransactionDto) throws AppException {
		if (!Objects.isNull(gstinTransactionDto)) {

			/*
			 * Query query = em.createNativeQuery(
			 * "{call generateHSNSUM(:gstn,:monthYear,:returnType,:user)}");
			 * query.setParameter("gstn", gstinTransactionDto.getTaxpayerGstin().getId())
			 * .setParameter("monthYear", gstinTransactionDto.getMonthYear())
			 * .setParameter("returnType",
			 * gstinTransactionDto.getReturnType().toLowerCase()) .setParameter("user",
			 * userBean.getUserDto().getId()); log.info( "update rows count {}",
			 * query.executeUpdate());
			 */

			Query query1 = em.createNativeQuery("{call generateHSNTAXSUM(:gstn,:monthYear,:returnType,:user)}");
			query1.setParameter("gstn", gstinTransactionDto.getTaxpayerGstin().getId())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear())
					.setParameter("returnType", gstinTransactionDto.getReturnType().toLowerCase())
					.setParameter("user", gstinTransactionDto.getUserBean().getUserDto().getId());
			log.info("update rows count {}", query1.executeUpdate());
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ginni.easemygst.portal.business.service.ReturnsService#c
	 * hangeStatusTransactionData(com.ginni.easemygst.portal.business.entity.
	 * GstinTransactionDTO, java.util.List) following service is used to
	 * accept,recject,cancel the invoices in gstr2 as per transaction
	 */
	@Override
	public boolean changeStatusTransactionData(GstinTransactionDTO gstinTransactionDTO, List<ChangeStatusDto> invoices)
			throws AppException {
		String trantype = StringUtils.upperCase(gstinTransactionDTO.getTransactionType());
		Class<?> type = finderService.getEntityTypeByTransaction(trantype);

		if (!Objects.isNull(gstinTransactionDTO)) {

			this.changeStatusCode(type, invoices, gstinTransactionDTO);
			return true;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	/**
	 * @param type
	 * @param invoices
	 * @param gstinTransactionDto
	 * @throws AppException
	 *             following service is used to accept,recject,cancel or revert the
	 *             invoices in gstr2
	 */
	@SuppressWarnings("unchecked")
	private <T> void changeStatusCode(Class<T> type, List<ChangeStatusDto> invoices,
			GstinTransactionDTO gstinTransactionDto) throws AppException {

		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		for (ChangeStatusDto invoice : invoices) {

			log.debug("change status code of {}", invoice.getInvoiceId());
			if (TaxpayerAction.REVERT.toString().equalsIgnoreCase(invoice.getTaxPayerAction())) {
				if (TransactionType.B2B == tranType) {
					this.revertB2bInvoice(gstinTransactionDto, invoice);
				} else if (TransactionType.CDN == tranType) {
					this.revertCdnInvoice(gstinTransactionDto, invoice);
				}
			} else {
				CriteriaBuilder cb = em.getCriteriaBuilder();
				// CriteriaQuery<T> cQuery = cb.createQuery(type);
				CriteriaUpdate<T> cUpdate = cb.createCriteriaUpdate(type);
				Root<T> b = cUpdate.from(type);
				ReconsileType reconsileType = ReconsileType.valueOf(StringUtils.upperCase(invoice.getType()));
				TaxpayerAction action = TaxpayerAction.valueOf(StringUtils.upperCase(invoice.getTaxPayerAction()));
				String status = "";
				String recType = "";

				if (TaxpayerAction.ACCEPT == action) {
					status = "ACCEPTED";
					recType = ReconsileType.MATCH.toString();
				} else if (TaxpayerAction.REJECT == action) {
					status = "REJECTED";

				} else if (TaxpayerAction.PENDING == action) {
					status = "PENDING";

				}
				if (TaxpayerAction.MATCH_AND_OVERWRITE == action) {
					this.saveMatchAndOverwriteAction(type, invoice, gstinTransactionDto);
					;

				} else if ((ReconsileType.MISSING_INWARD == reconsileType
						|| ReconsileType.MISSING_OUTWARD == reconsileType
						|| ((ReconsileType.MISMATCH_INVOICENO == reconsileType
								|| ReconsileType.MISMATCH_DATE == reconsileType
								|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
								|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
								|| ReconsileType.MISMATCH_MAJOR == reconsileType)))
						&& TaxpayerAction.ACCEPT != action) {

					// cheking filing status for pending and rject
					if (TransactionType.B2B == tranType) {
						if (ReconsileType.MISSING_INWARD == reconsileType
								|| ReconsileType.MISSING_OUTWARD == reconsileType) {
							B2BDetailEntity tempB2b = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());
							this.checkB2bFilingStatus(tempB2b);
						} else if (((ReconsileType.MISMATCH_INVOICENO == reconsileType
								|| ReconsileType.MISMATCH_DATE == reconsileType
								|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
								|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
								|| ReconsileType.MISMATCH_MAJOR == reconsileType))) {
							B2BDetailEntity invoiceEmg = crudService.find(B2BDetailEntity.class,
									invoice.getInvoiceId());

							List<B2BDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
									"B2bDetailEntity.findMismatchedInvoice",
									QueryParameter
											.with("taxPayerGstin", invoiceEmg.getB2bTransaction().getTaxpayerGstin())
											.and("returnType", invoiceEmg.getB2bTransaction().getReturnType())
											.and("monthYear", invoiceEmg.getB2bTransaction().getMonthYear())

											.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
											.and("dataSource", /*
																 * invoiceEmg. getDataSource()
																 */DataSource.GSTN)
											.and("ctin", invoiceEmg.getB2bTransaction().getCtin()).parameters(),
									1);

							B2BDetailEntity invoiceGstn = null;
							if (!invoiceGstns.isEmpty()) {
								invoiceGstn = invoiceGstns.get(0);

								this.checkB2bFilingStatus(invoiceGstn);
								cUpdate.set("checksum", invoiceGstn.getChecksum());
							}
						}
					} else if (TransactionType.CDN == tranType) {
						if (ReconsileType.MISSING_INWARD == reconsileType
								|| ReconsileType.MISSING_OUTWARD == reconsileType) {
							CDNDetailEntity tempCdn = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
							this.checkCdnFilingStatus(tempCdn);
						} else if (((ReconsileType.MISMATCH_INVOICENO == reconsileType
								|| ReconsileType.MISMATCH_DATE == reconsileType
								|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
								|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
								|| ReconsileType.MISMATCH_MAJOR == reconsileType))) {
							CDNDetailEntity invoiceEmg = crudService.find(CDNDetailEntity.class,
									invoice.getInvoiceId());

							List<CDNDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
									"CdnDetailEntity.findMismatchedInvoice",
									QueryParameter.with("taxPayerGstin", invoiceEmg.getCdnTransaction().getGstin())
											.and("returnType", invoiceEmg.getCdnTransaction().getReturnType())
											.and("monthYear", invoiceEmg.getCdnTransaction().getMonthYear())
											.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
											.and("ctin", invoiceEmg.getCdnTransaction().getOriginalCtin())
											.and("dataSource", invoiceEmg.getDataSource()).parameters(),
									1);
							CDNDetailEntity invoiceGstn = null;
							if (!invoiceGstns.isEmpty()) {
								invoiceGstn = invoiceGstns.get(0);

								this.checkCdnFilingStatus(invoiceGstn);
								cUpdate.set("checksum", invoiceGstn.getChecksum());
							}
						}
					}

					//

					cUpdate.set("flags", status);
					cUpdate.set("previousFlags", "");
					cUpdate.set("source", SourceType.PORTAL.toString());
					cUpdate.set("previousType", reconsileType.toString());
					/*
					 * if(TaxpayerAction.ACCEPT == action) cUpdate.set("type", recType);
					 */
					// cb.and(cb.equal(b.get("id"),
					// invoice.getInvoiceId()),cb.equal(b.get("b2bTransaction").get("fillingStatus"),
					// invoice.get));

					cUpdate.where(cb.equal(b.get("id"), invoice.getInvoiceId()));
					int count = em.createQuery(cUpdate).executeUpdate();
					log.debug("{} invoice {} status has been chagned to {}", count, invoice.getInvoiceId(), status);
				} else if ((ReconsileType.MISMATCH_INVOICENO == reconsileType
						|| ReconsileType.MISMATCH_DATE == reconsileType
						|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
						|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
						|| ReconsileType.MISMATCH_MAJOR == reconsileType) && TaxpayerAction.ACCEPT == action) {

					if (TransactionType.B2B == tranType) {
						B2BDetailEntity invoiceEmg = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());
						invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());
						invoiceEmg.setPreviousType(invoiceEmg.getType());

						List<B2BDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
								"B2bDetailEntity.findMismatchedInvoice",
								QueryParameter.with("taxPayerGstin", invoiceEmg.getB2bTransaction().getTaxpayerGstin())
										.and("returnType", invoiceEmg.getB2bTransaction().getReturnType())
										.and("monthYear", invoiceEmg.getB2bTransaction().getMonthYear())

										.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
										.and("dataSource", /*
															 * invoiceEmg. getDataSource()
															 */DataSource.GSTN)
										.and("ctin", invoiceEmg.getB2bTransaction().getCtin()).parameters(),
								1);

						B2BDetailEntity invoiceGstn = null;
						if (!invoiceGstns.isEmpty()) {
							invoiceGstn = invoiceGstns.get(0);

							this.checkB2bFilingStatus(invoiceGstn);

							B2bDetailEntityWrapper invoiceEmgWrapper = new B2bDetailEntityWrapper(invoiceEmg);
							ObjectMapper mapper = new ObjectMapper();
							SimpleModule module = new SimpleModule();
							module.addSerializer(B2BDetailEntity.class, new B2BRecoDetailSerializer());
							mapper.registerModule(module);
							invoiceEmg.setPreviousInvoice(
									mapper.convertValue(invoiceEmgWrapper, JsonNode.class).get("invoice"));

							/*
							 * invoiceEmg.setPreviousInvoice(
							 * JsonMapper.objectMapper.convertValue(invoiceEmg, JsonNode.class));
							 */
							// save invoice emg here before replacing it
							invoiceEmg.setPreviousType(invoiceEmg.getType());
							invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());

							invoiceEmg.setChecksum(invoiceGstn.getChecksum());
							invoiceEmg.setInvoiceDate(invoiceGstn.getInvoiceDate());
							invoiceEmg.setEtin(invoiceGstn.getEtin());
							invoiceEmg.setInvoiceType(invoiceGstn.getInvoiceType());
							invoiceEmg.setReverseCharge(invoiceGstn.getReverseCharge());
							invoiceEmg.setPos(invoiceGstn.getPos());
							// invoiceEmg.setItems(newItems);
							invoiceEmg.setTaxableValue(invoiceGstn.getTaxableValue());
							invoiceEmg.setTaxAmount(invoiceGstn.getTaxAmount());
							invoiceEmg.setFlags(status);
							invoiceEmg.setSource(SourceType.PORTAL.toString());
							invoiceEmg.setType(ReconsileType.MATCH.toString());
							crudService.update(invoiceEmg);

							List<Item> items = invoiceEmg.getItems();

							this.deleteItemsByInvoiceId(invoiceEmg.getId());

							// List<Item> newItems = new ArrayList<>();
							for (Item itm : invoiceGstn.getItems()) {
								Item tItem = new Item(itm);
								tItem.setInvoiceId(invoiceEmg.getId());
								Optional<Item> sameRtItm = items.stream()
										.filter(i -> i.getTaxRate() == itm.getTaxRate()).findFirst();
								if (sameRtItm.isPresent()) {
									tItem.setHsnCode(sameRtItm.get().getHsnCode());
									tItem.setHsnDescription(sameRtItm.get().getHsnDescription());
									tItem.setUnit(sameRtItm.get().getUnit());
									tItem.setQuantity(sameRtItm.get().getQuantity());
								}

								// newItems.add(tItem);
								crudService.create(tItem);
							}
						}
					} else if (TransactionType.CDN == tranType) {
						CDNDetailEntity invoiceEmg = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
						invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());
						invoiceEmg.setPreviousType(invoiceEmg.getType());
						List<CDNDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
								"CdnDetailEntity.findMismatchedInvoice",
								QueryParameter.with("taxPayerGstin", invoiceEmg.getCdnTransaction().getGstin())
										.and("returnType", invoiceEmg.getCdnTransaction().getReturnType())
										.and("monthYear", invoiceEmg.getCdnTransaction().getMonthYear())
										.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
										.and("ctin", invoiceEmg.getCdnTransaction().getOriginalCtin())
										.and("dataSource", invoiceEmg.getDataSource()).parameters(),
								1);
						CDNDetailEntity invoiceGstn = null;
						if (!invoiceGstns.isEmpty()) {
							invoiceGstn = invoiceGstns.get(0);

							this.checkCdnFilingStatus(invoiceGstn);

							CdnDetailEntityWrapper invoiceEmgWrapper = new CdnDetailEntityWrapper(invoiceEmg);
							ObjectMapper mapper = new ObjectMapper();
							SimpleModule module = new SimpleModule();
							module.addSerializer(CDNDetailEntity.class, new CDNRecoDetailSerializer());
							mapper.registerModule(module);
							invoiceEmg.setPreviousInvoice(
									mapper.convertValue(invoiceEmgWrapper, JsonNode.class).get("invoice"));

							/*
							 * invoiceEmg.setPreviousInvoice(
							 * JsonMapper.objectMapper.convertValue(invoiceEmg, JsonNode.class));
							 */
							// save invoice emg here before replacing it

							// invoiceEmg.setItems(newItems);
							invoiceEmg.setPreviousType(invoiceEmg.getType());
							invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());
							invoiceEmg.setChecksum(invoiceGstn.getChecksum());
							invoiceEmg.setInvoiceDate(invoiceGstn.getInvoiceDate());
							invoiceEmg.setInvoiceType(invoiceGstn.getInvoiceType());

							invoiceEmg.setTaxableValue(invoiceGstn.getTaxableValue());
							invoiceEmg.setTaxAmount(invoiceGstn.getTaxAmount());
							invoiceEmg.setRevisedInvNo(invoiceGstn.getRevisedInvNo());
							invoiceEmg.setRevisedInvDate(invoiceGstn.getRevisedInvDate());
							invoiceEmg.setNoteType(invoiceGstn.getNoteType());
							invoiceEmg.setPreGstRegime(invoiceGstn.getPreGstRegime());
							invoiceEmg.setFlags(status);
							invoiceEmg.setSource(SourceType.PORTAL.toString());

							invoiceEmg.setType(ReconsileType.MATCH.toString());
							crudService.update(invoiceEmg);
							List<Item> items = invoiceEmg.getItems();
							this.deleteItemsByInvoiceId(invoiceEmg.getId());

							// List<Item> newItems = new ArrayList<>();
							for (Item itm : invoiceGstn.getItems()) {
								Item tItem = new Item(itm);
								tItem.setInvoiceId(invoiceEmg.getId());
								Optional<Item> sameRtItm = items.stream()
										.filter(i -> i.getTaxRate() == itm.getTaxRate()).findFirst();
								if (sameRtItm.isPresent()) {
									tItem.setHsnCode(sameRtItm.get().getHsnCode());
									tItem.setHsnDescription(sameRtItm.get().getHsnDescription());
									tItem.setUnit(sameRtItm.get().getUnit());
									tItem.setQuantity(sameRtItm.get().getQuantity());
								}

								// newItems.add(tItem);
								crudService.create(tItem);
							}
						}
					}

				} else if ((ReconsileType.MISSING_INWARD == reconsileType
						|| ReconsileType.MISSING_OUTWARD == reconsileType) && TaxpayerAction.ACCEPT == action) {
					if (TransactionType.B2B == tranType) {
						B2BDetailEntity invoiceGstn = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());

						this.checkB2bFilingStatus(invoiceGstn);
						String preType = invoiceGstn.getType();
						invoiceGstn.setType(ReconsileType.NEW.toString());
						invoiceGstn.setFlags("");
						crudService.update(invoiceGstn);
						B2BDetailEntity inv = new B2BDetailEntity(invoiceGstn);
						inv.setPreviousFlags(invoiceGstn.getFlags());
						inv.setPreviousType(preType);
						inv.setIsTransit(false);
						inv.setType(ReconsileType.MATCH.toString());
						inv.setSource(SourceType.PORTAL.toString());
						inv.setDataSource(DataSource.EMGST);
						inv.setFlags(status);

						InfoService.setInfo(inv, userBean);

						crudService.create(inv);

					} else if (TransactionType.CDN == tranType) {
						CDNDetailEntity invoiceGstn = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
						String preType = invoiceGstn.getType();
						this.checkCdnFilingStatus(invoiceGstn);

						invoiceGstn.setType(ReconsileType.NEW.toString());
						invoiceGstn.setFlags("");
						crudService.update(invoiceGstn);

						CDNDetailEntity inv = new CDNDetailEntity(invoiceGstn);
						inv.setPreviousFlags(invoiceGstn.getFlags());
						inv.setPreviousType(preType);
						inv.setType(ReconsileType.MATCH.toString());
						inv.setIsTransit(false);
						inv.setSource(SourceType.PORTAL.toString());
						inv.setDataSource(DataSource.EMGST);
						inv.setFlags(status);
						InfoService.setInfo(inv, userBean);
						crudService.create(inv);
					}

				}
			}
		}
	}

	/**
	 * @param gstinTransactionDTO
	 * @param invoice
	 * @return
	 * @throws AppException
	 *             following service is used to revert the action taken by user on
	 *             invoices of supplier in gstr2
	 */
	private boolean revertB2bInvoice(GstinTransactionDTO gstinTransactionDTO, ChangeStatusDto invoice)
			throws AppException {

		B2BDetailEntity b2b = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());

		if ("PENDING".equalsIgnoreCase(b2b.getFlags()) || "REJECTED".equalsIgnoreCase(b2b.getFlags())
				|| "ACCEPTED".equalsIgnoreCase(b2b.getFlags())) {

			if ("PENDING".equalsIgnoreCase(b2b.getFlags()) || "REJECTED".equalsIgnoreCase(b2b.getFlags())) {
				b2b.setFlags("");
				crudService.update(b2b);
			} else if ("ACCEPTED".equalsIgnoreCase(b2b.getFlags())
					&& (Objects.nonNull(b2b.getPreviousInvoice()) && Objects.nonNull(b2b.getPreviousType())
							&& b2b.getPreviousType().contains("MISMATCH") /* previous status =='MISMATCH' */)) {
				B2BDetailEntity previousInvoice = new B2BDetailEntity();
				;

				try {

					ObjectMapper mapper = new ObjectMapper();
					SimpleModule module = new SimpleModule();
					module.addDeserializer(B2BDetailEntity.class, new B2bRecoDetailDeserializer());
					mapper.registerModule(module);
					previousInvoice = mapper.convertValue(b2b.getPreviousInvoice(), B2BDetailEntity.class);
				} catch (Exception e) {
					_Logger.error("exception in revertB2bInvoice during reverting previous invoice", e);
				}

				this.deleteItemsByInvoiceId(b2b.getId());
				for (Item item : previousInvoice.getItems()) {
					item.setInvoiceId(previousInvoice.getId());
				}
				previousInvoice.setB2bTransaction(b2b.getB2bTransaction());
				crudService.update(previousInvoice);
				// crudService.delete(B2BDetailEntity.class, b2b.getId());

			} else if ("ACCEPTED".equalsIgnoreCase(b2b.getFlags()) && Objects.nonNull(b2b.getPreviousType())
					&& b2b.getPreviousType().contains("MISSING") /* previous status =='MISSING' */) {

				this.deleteItemsByInvoiceId(b2b.getId());
				this.deleteTransaction(b2b.getB2bTransaction().getTaxpayerGstin().getGstin(),
						gstinTransactionDTO.getMonthYear(), TransactionType.B2B,
						ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType())),
						java.util.Arrays.asList(b2b.getId()), DeleteType.SELECTED, DataSource.EMGST);
				// crudService.delete(B2BDetailEntity.class, b2b.getId());
				this.compareTransactions(gstinTransactionDTO);

			}
		}

		else {
			AppException ae = new AppException();
			ae.setMessage("You can not performed revert action on this invoice");
			throw ae;

		}
		return true;
	}

	/**
	 * @param gstinTransactionDTO
	 * @param invoice
	 * @return
	 * @throws AppException
	 *             following service is used to revert the action taken by user on
	 *             invoices of supplier in gstr2
	 */
	private boolean revertCdnInvoice(GstinTransactionDTO gstinTransactionDTO, ChangeStatusDto invoice)
			throws AppException {

		CDNDetailEntity cdn = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
		if ("PENDING".equalsIgnoreCase(cdn.getFlags()) || "REJECTED".equalsIgnoreCase(cdn.getFlags())
				|| "ACCEPTED".equalsIgnoreCase(cdn.getFlags())) {

			if ("PENDING".equalsIgnoreCase(cdn.getFlags()) || "REJECTED".equalsIgnoreCase(cdn.getFlags())) {
				cdn.setFlags("");
				crudService.update(cdn);
			} else if ("ACCEPTED".equalsIgnoreCase(cdn.getFlags())
					&& (Objects.nonNull(cdn.getPreviousInvoice()) && Objects.nonNull(cdn.getPreviousType())
							&& cdn.getPreviousType().contains("MISMATCH") /* previous status =='MISMATCH' */)) {
				CDNDetailEntity previousInvoice = new CDNDetailEntity();
				try {
					ObjectMapper mapper = new ObjectMapper();
					SimpleModule module = new SimpleModule();
					module.addDeserializer(CDNDetailEntity.class, new CdnRecoDetailDeserializer());
					mapper.registerModule(module);
					previousInvoice = mapper.convertValue(cdn.getPreviousInvoice(), CDNDetailEntity.class);
				} catch (Exception e) {
					_Logger.error("exception in revertCdnInvoice during reverting previous invoice", e);
				}

				this.deleteItemsByInvoiceId(cdn.getId());
				for (Item item : previousInvoice.getItems()) {
					item.setInvoiceId(previousInvoice.getId());
				}
				previousInvoice.setCdnTransaction(cdn.getCdnTransaction());
				crudService.update(previousInvoice);
				// crudService.delete(B2BDetailEntity.class, cdn.getId());

			} else if ("ACCEPTED".equalsIgnoreCase(cdn.getFlags()) && Objects.nonNull(cdn.getPreviousType())
					&& cdn.getPreviousType().contains("MISSING") /* previous status =='MISSING' */) {
				this.deleteItemsByInvoiceId(cdn.getId());
				this.deleteTransaction(cdn.getCdnTransaction().getGstin().getGstin(),
						gstinTransactionDTO.getMonthYear(), TransactionType.CDN,
						ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType())),
						java.util.Arrays.asList(cdn.getId()), DeleteType.SELECTED, DataSource.EMGST);
				// crudService.delete(B2BDetailEntity.class, b2b.getId());
				this.compareTransactions(gstinTransactionDTO);

			}
		} else {
			AppException ae = new AppException();
			ae.setMessage("You can not performed revert action on this invoice");
			throw ae;
		}
		return true;
	}

	private Set<String> appendMessage(String ctin, Set<String> messages) {
		String message = "GSTR 1 return  is not filed by counter party yet for ctin " + ctin;
		messages.add(message);
		return messages;

	}

	private boolean checkB2bFilingStatus(B2BDetailEntity invoiceGstn) throws AppException {

		if ("N".equalsIgnoreCase(invoiceGstn.getB2bTransaction().getFillingStatus())) {
			AppException ae = new AppException();
			// ae.setMessage("invoice with invoice no:"+invoiceGstn.getInvoiceNumber()+" is
			// not filed by counter party yet");
			ae.setMessage("GSTR 1 return  is not filed by counter party yet for ctin "
					+ invoiceGstn.getB2bTransaction().getCtin());

			throw ae;
		}
		return true;

	}

	private boolean checkCdnFilingStatus(CDNDetailEntity invoiceGstn) throws AppException {

		if ("N".equalsIgnoreCase(invoiceGstn.getCdnTransaction().getFillingStatus())) {
			AppException ae = new AppException();
			// ae.setMessage("invoice with invoice no:"+invoiceGstn.getInvoiceNumber()+" is
			// not filed by counter party yet");
			ae.setMessage("GSTR 1 return  is not filed by counter party yet for ctin "
					+ invoiceGstn.getCdnTransaction().getOriginalCtin());

			throw ae;
		}
		return true;

	}

	public <T> void saveMatchAndOverwriteAction(Class<T> type, ChangeStatusDto invoice,
			GstinTransactionDTO gstinTransactionDto) throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));

		if (TransactionType.B2B == tranType) {

			B2BDetailEntity invoiceBuyer = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());
			B2BDetailEntity invoiceSupplier = crudService.find(B2BDetailEntity.class, invoice.getSupplierInvoiceId());

			if (TaxpayerAction.MATCH_AND_OVERWRITE.toString().equalsIgnoreCase(invoice.getTaxPayerAction())) {
				invoiceBuyer.setPreviousFlags(invoiceBuyer.getFlags());
				invoiceBuyer.setPreviousType(invoiceBuyer.getType());

				this.checkB2bFilingStatus(invoiceSupplier);

				B2bDetailEntityWrapper invoiceEmgWrapper = new B2bDetailEntityWrapper(invoiceBuyer);
				ObjectMapper mapper = new ObjectMapper();
				SimpleModule module = new SimpleModule();
				module.addSerializer(B2BDetailEntity.class, new B2BRecoDetailSerializer());
				mapper.registerModule(module);
				invoiceBuyer.setPreviousInvoice(mapper.convertValue(invoiceEmgWrapper, JsonNode.class).get("invoice"));
				// save invoice emg here before replacing it
				invoiceBuyer.setInvoiceNumber(invoiceSupplier.getInvoiceNumber());
				invoiceBuyer.setInvoiceDate(invoiceSupplier.getInvoiceDate());
				invoiceBuyer.setEtin(invoiceSupplier.getEtin());
				invoiceBuyer.setInvoiceType(invoiceSupplier.getInvoiceType());
				invoiceBuyer.setReverseCharge(invoiceSupplier.getReverseCharge());
				invoiceBuyer.setPos(invoiceSupplier.getPos());
				// invoiceEmg.setItems(newItems);
				invoiceBuyer.setTaxableValue(invoiceSupplier.getTaxableValue());
				invoiceBuyer.setTaxAmount(invoiceSupplier.getTaxAmount());
				invoiceBuyer.setFlags("ACCEPTED");
				invoiceBuyer.setSource(SourceType.PORTAL.toString());
				invoiceBuyer.setType(ReconsileType.MATCH.toString());
				crudService.update(invoiceBuyer);
				invoiceSupplier.setType(ReconsileType.NEW.toString());
				crudService.update(invoiceSupplier);
				this.deleteItemsByInvoiceId(invoiceBuyer.getId());

				// List<Item> newItems = new ArrayList<>();
				for (Item itm : invoiceSupplier.getItems()) {
					Item tItem = new Item(itm);
					tItem.setInvoiceId(invoiceBuyer.getId());
					// newItems.add(tItem);
					crudService.create(tItem);
				}
			}
		} else if (TransactionType.CDN == tranType) {

			CDNDetailEntity invoiceBuyer = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
			CDNDetailEntity invoiceSupplier = crudService.find(CDNDetailEntity.class, invoice.getSupplierInvoiceId());

			if (TaxpayerAction.MATCH_AND_OVERWRITE.toString().equalsIgnoreCase(invoice.getTaxPayerAction())) {
				invoiceBuyer.setPreviousFlags(invoiceBuyer.getFlags());
				invoiceBuyer.setPreviousType(invoiceBuyer.getType());

				this.checkCdnFilingStatus(invoiceSupplier);

				CdnDetailEntityWrapper invoiceEmgWrapper = new CdnDetailEntityWrapper(invoiceBuyer);
				ObjectMapper mapper = new ObjectMapper();
				SimpleModule module = new SimpleModule();
				module.addSerializer(CDNDetailEntity.class, new CDNRecoDetailSerializer());
				mapper.registerModule(module);
				invoiceBuyer.setPreviousInvoice(mapper.convertValue(invoiceEmgWrapper, JsonNode.class).get("invoice"));
				// save invoice emg here before replacing it
				invoiceBuyer.setInvoiceNumber(invoiceSupplier.getInvoiceNumber());
				invoiceBuyer.setInvoiceDate(invoiceSupplier.getInvoiceDate());
				invoiceBuyer.setInvoiceType(invoiceSupplier.getInvoiceType());

				invoiceBuyer.setTaxableValue(invoiceSupplier.getTaxableValue());
				invoiceBuyer.setTaxAmount(invoiceSupplier.getTaxAmount());
				invoiceBuyer.setRevisedInvNo(invoiceSupplier.getRevisedInvNo());
				invoiceBuyer.setRevisedInvDate(invoiceSupplier.getRevisedInvDate());
				invoiceBuyer.setNoteType(invoiceSupplier.getNoteType());
				invoiceBuyer.setPreGstRegime(invoiceSupplier.getPreGstRegime());
				invoiceBuyer.setFlags("ACCEPTED");
				invoiceBuyer.setSource(SourceType.PORTAL.toString());

				invoiceBuyer.setType(ReconsileType.MATCH.toString());
				crudService.update(invoiceBuyer);
				invoiceSupplier.setType(ReconsileType.NEW.toString());
				crudService.update(invoiceSupplier);
				this.deleteItemsByInvoiceId(invoiceBuyer.getId());

				// List<Item> newItems = new ArrayList<>();
				for (Item itm : invoiceSupplier.getItems()) {
					Item tItem = new Item(itm);
					tItem.setInvoiceId(invoiceBuyer.getId());
					// newItems.add(tItem);

					crudService.create(tItem);
				}
			}
		}

	}

	private long getTotalCount(GstinTransactionDTO gstinTransactionDTO, boolean isSummary) {
		Map<String, Object> queryParameters = new HashMap<>();

		queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
		queryParameters.put("taxPayerGstin",
				(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
		queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());
		String queryName1 = "";
		String type = gstinTransactionDTO.getTransactionType();
		if (TransactionType.B2B.toString().equalsIgnoreCase(type)) {
			if (isSummary) {
				queryName1 = "B2bDetailEntity.findCountByMonthYearForSummary";

			} else {
				queryName1 = "B2bDetailEntity.findCountByMonthYear";

			}
			queryParameters.put("dataSource", DataSource.EMGST);
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)) {

			if (isSummary) {
				queryName1 = "CdnDetailEntity.findCountByMonthYearForSummary";

			} else {
				queryName1 = "CdnDetailEntity.findCountByMonthYear";

			}
			queryParameters.put("dataSource", DataSource.EMGST);

		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)) {
			queryName1 = "CdnUrDetailEntity.findCountByMonthYear";

		}
		if (TransactionType.B2CL.toString().equalsIgnoreCase(type)) {
			queryName1 = "B2clDetailEntity.findCountByMonthYear";

		}
		if (TransactionType.B2CS.toString().equalsIgnoreCase(type)) {
			queryName1 = "B2csTransaction.findCountByMonthYear";

		}
		if (TransactionType.EXP.toString().equalsIgnoreCase(type)) {
			queryName1 = "ExpDetailEntity.findCountByMonthYear";

		}
		if (TransactionType.AT.toString().equalsIgnoreCase(type)) {
			queryName1 = "AtTransaction.findCountByMonthYear";

		}
		if (TransactionType.NIL.toString().equalsIgnoreCase(type)) {
			queryName1 = "NilTransaction.findCountByMonthYear";

		}
		if (TransactionType.TXPD.toString().equalsIgnoreCase(type)) {
			queryName1 = "TxpdTransaction.findCountByMonthYear";

		}
		if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {
			queryName1 = "HsnTransaction.findCountByMonthYear";

		}
		if (TransactionType.IMPG.toString().equalsIgnoreCase(type)) {
			queryName1 = "IMPGTransactionEntity.findCountByMonthYear";

		}
		if (TransactionType.IMPS.toString().equalsIgnoreCase(type)) {
			queryName1 = "IMPSTransactionEntity.findCountByMonthYear";

		}
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(type)) {
			queryName1 = "B2bUrTransaction.findCountByMonthYear";

		}
		if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type)) {
			queryName1 = "DocIssue.findCountByMonthYear";

		}
		long count = crudService.findResultCounts(queryName1, queryParameters);

		return count;
	}

	private TransactionDataDTO getToBeSyncCounts(GstinTransactionDTO gstinTransactionDTO, String type) {
		Map<String, Object> queryParameters = new HashMap<>();

		queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
		queryParameters.put("taxPayerGstin",
				(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
		queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());
		String queryName1 = "";
		// String type = gstinTransactionDTO.getTransactionType();
		if (TransactionType.B2B.toString().equalsIgnoreCase(type)) {
			queryName1 = "B2bDetailEntity.findToBeSyncCountByMonthYear";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)) {
			queryName1 = "CdnDetailEntity.findToBeSyncCountByMonthYear";

		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)) {
			queryName1 = "CdnUrDetailEntity.findToBeSyncCountByMonthYear";

		}
		if (ReturnType.GSTR1 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {

			if (TransactionType.B2CL.toString().equalsIgnoreCase(type)) {
				queryName1 = "B2clDetailEntity.findToBeSyncCountByMonthYear";

			}
			if (TransactionType.B2CS.toString().equalsIgnoreCase(type)) {
				queryName1 = "B2csTransaction.findToBeSyncCountByMonthYear";

			}
			if (TransactionType.EXP.toString().equalsIgnoreCase(type)) {
				queryName1 = "ExpDetailEntity.findToBeSyncCountByMonthYear";

			}
			if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type)) {
				queryName1 = "DocIssue.findToBeSyncCountByMonthYear";

			}
		}
		if (TransactionType.AT.toString().equalsIgnoreCase(type)) {
			queryName1 = "AtTransaction.findToBeSyncCountByMonthYear";

		}
		if (TransactionType.NIL.toString().equalsIgnoreCase(type)) {
			queryName1 = "NilTransaction.findToBeSyncCountByMonthYear";

		}
		if (TransactionType.TXPD.toString().equalsIgnoreCase(type)) {
			queryName1 = "TxpdTransaction.findToBeSyncCountByMonthYear";

		}
		if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {
			queryName1 = "HsnTransaction.findToBeSyncCountByMonthYear";

		}
		if (ReturnType.GSTR2 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {

			if (TransactionType.IMPG.toString().equalsIgnoreCase(type)) {
				queryName1 = "IMPGTransactionEntity.findToBeSyncCountByMonthYear";

			}
			if (TransactionType.IMPS.toString().equalsIgnoreCase(type)) {
				queryName1 = "IMPSTransactionEntity.findToBeSyncCountByMonthYear";

			}
			if (TransactionType.B2BUR.toString().equalsIgnoreCase(type)) {
				queryName1 = "B2bUrTransaction.findToBeSyncCountByMonthYear";

			}
		}

		List<Object[]> toBeSyncCounts = crudService.findWithNamedQuery(queryName1, queryParameters);
		TransactionDataDTO tDto = new TransactionDataDTO();
		for (Object[] summary : toBeSyncCounts) {
			// log.debug("field {} and value {}", summary[0], summary[1]);
			boolean toBeSync = !Objects.isNull(summary[0]) ? (Boolean) summary[0] : false;
			long toBeSyncCount = !Objects.isNull(summary[1]) ? (Long) summary[1] : 0;

			if (toBeSync) {
				tDto.setToBeSyncCount(toBeSyncCount);
			} else {
				tDto.setNotToBeSyncCount(toBeSyncCount);
			}
			// long count = crudService.findResultCounts(queryName1,
			// queryParameters);
		}
		GstinTransactionDTO gDto = new GstinTransactionDTO();
		gDto.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
		gDto.setMonthYear(gstinTransactionDTO.getMonthYear());
		gDto.setReturnType(gstinTransactionDTO.getReturnType());
		gDto.setTransactionType(type);

		tDto.setInvoices(this.getTotalCount(gDto, true));
		tDto.setErrorCount(this.getErrorCounts(gstinTransactionDTO, type));

		return tDto;
	}

	private long getErrorCounts(GstinTransactionDTO gstinTransactionDTO, String type) {
		Map<String, Object> queryParameters = new HashMap<>();

		queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
		queryParameters.put("taxPayerGstin",
				(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
		queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());
		String queryName1 = "";
		// String type = gstinTransactionDTO.getTransactionType();
		if (TransactionType.B2B.toString().equalsIgnoreCase(type)) {
			queryName1 = "B2bDetailEntity.findErrorCountByMonthYear";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(type)) {
			queryName1 = "CdnDetailEntity.findErrorCountByMonthYear";

		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(type)) {
			queryName1 = "CdnUrDetailEntity.findErrorCountByMonthYear";

		}
		if (ReturnType.GSTR1 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {

			if (TransactionType.B2CL.toString().equalsIgnoreCase(type)) {
				queryName1 = "B2clDetailEntity.findErrorCountByMonthYear";

			}
			if (TransactionType.B2CS.toString().equalsIgnoreCase(type)) {
				queryName1 = "B2csTransaction.findErrorCountByMonthYear";

			}
			if (TransactionType.EXP.toString().equalsIgnoreCase(type)) {
				queryName1 = "ExpDetailEntity.findErrorCountByMonthYear";

			}
			if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type)) {
				queryName1 = "DocIssue.findErrorCountByMonthYear";

			}
		}
		if (TransactionType.AT.toString().equalsIgnoreCase(type)) {
			queryName1 = "AtTransaction.findErrorCountByMonthYear";

		}
		if (TransactionType.NIL.toString().equalsIgnoreCase(type)) {
			queryName1 = "NilTransaction.findErrorCountByMonthYear";

		}
		if (TransactionType.TXPD.toString().equalsIgnoreCase(type)) {
			queryName1 = "TxpdTransaction.findErrorCountByMonthYear";

		}
		if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type)) {
			queryName1 = "HsnTransaction.findErrorCountByMonthYear";

		}
		if (ReturnType.GSTR2 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {

			if (TransactionType.IMPG.toString().equalsIgnoreCase(type)) {
				queryName1 = "IMPGTransactionEntity.findErrorCountByMonthYear";

			}
			if (TransactionType.IMPS.toString().equalsIgnoreCase(type)) {
				queryName1 = "IMPSTransactionEntity.findErrorCountByMonthYear";

			}
			if (TransactionType.B2BUR.toString().equalsIgnoreCase(type)) {
				queryName1 = "B2bUrTransaction.findErrorCountByMonthYear";

			}
		}

		/*
		 * List<Object[]> errorCounts = crudService.findWithNamedQuery(queryName1,
		 * queryParameters); TransactionDataDTO tDto = new TransactionDataDTO();
		 */
		long errorCount = crudService.findResultCounts(queryName1, queryParameters);

		return errorCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ginni.easemygst.portal.business.service.ReturnsService
	 * #getTransactionData(com.ginni.easemygst.portal.business.entity.
	 * GstinTransactionDTO, com.ginni.easemygst.portal.persistence.FilterDto) get
	 * transaction grid data with filter and pagination
	 */
	@Override
	public Map<String, Object> getTransactionData(GstinTransactionDTO gstinTransactionDTO, FilterDto filter)
			throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)
				&& !StringUtils.isEmpty(gstinTransactionDTO.getTaxpayerGstin().getGstin())) {
			Map<String, Object> result = finderService
					.getGstinTransactionByGstinMonthYearReturnType(gstinTransactionDTO, filter);
			List datas = (List) result.get("transactionData");
			long count = 0;
			if (Objects.isNull(result.get("total_count")))
				count = this.getTotalCount(gstinTransactionDTO, false);
			else
				count = (Long) result.get("total_count");// no of records in transactions
			log.debug("total record counts {}", count);
			log.debug("page count {}", datas.size());
			Map<String, Object> temp = new HashMap<>();
			if (!Objects.isNull(datas))
				temp.put("transactionData", datas);
			else
				temp.put("transactionData", "[]");
			temp.put("total_count", count);
			return temp;

		} else {

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public Map<String, Object> getInvoiceData(String type, String invoiceId) throws AppException {

		Class c = finderService.getEntityTypeByTransaction(type);
		Object invoice = crudService.find(c, invoiceId);
		Map<String, Object> temp = new HashMap<>();
		if (!Objects.isNull(invoice))
			temp.put("invoiceData", invoice);
		else
			temp.put("invoiceData", "{}");

		try {
			log.debug("invoice data {}", JsonMapper.objectMapper.writeValueAsString(temp));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getMismatchedInvoices(String type, String invoiceId) throws AppException {

		/* Class c = finderService.getEntityTypeByTransaction(type); */
		TransactionType tType = TransactionType.valueOf(StringUtils.upperCase(type));
		Map<String, Object> response = new HashMap<>();

		if (TransactionType.B2B == tType) {
			B2BDetailEntity invoiceEmg = crudService.find(B2BDetailEntity.class, invoiceId);
			response.put("invoice", invoiceEmg);

			List<B2BDetailEntity> invoiceGstns = crudService.findWithNamedQuery("B2bDetailEntity.findMismatchedInvoice",
					QueryParameter.with("taxPayerGstin", invoiceEmg.getB2bTransaction().getTaxpayerGstin())
							.and("returnType", invoiceEmg.getB2bTransaction().getReturnType())
							.and("monthYear", invoiceEmg.getB2bTransaction().getMonthYear())
							.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
							.and("dataSource", /* invoiceEmg.getDataSource() */DataSource.GSTN)
							.and("ctin", invoiceEmg.getB2bTransaction().getCtin()).parameters(),
					1);
			if (invoiceGstns.isEmpty())
				response.put("cinvoice", new B2BDetailEntity());
			else {
				B2BDetailEntity invoiceGstn = invoiceGstns.get(0);
				invoiceGstn.setOctin(invoiceGstn.getB2bTransaction().getTaxpayerGstin().getGstin());
				invoiceGstn.setOctinName(invoiceGstn.getB2bTransaction().getTaxpayerGstin().getDisplayName());
				response.put("cinvoice", invoiceGstn);

			}
		} else if (TransactionType.CDN == tType) {
			CDNDetailEntity invoiceEmg = crudService.find(CDNDetailEntity.class, invoiceId);
			response.put("invoice", invoiceEmg);

			List<CDNDetailEntity> invoiceGstns = crudService.findWithNamedQuery("CdnDetailEntity.findMismatchedInvoice",
					QueryParameter.with("taxPayerGstin", invoiceEmg.getCdnTransaction().getGstin())
							.and("returnType", invoiceEmg.getCdnTransaction().getReturnType())
							.and("monthYear", invoiceEmg.getCdnTransaction().getMonthYear())
							.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
							.and("ctin", invoiceEmg.getCdnTransaction().getOriginalCtin())
							.and("dataSource", /* invoiceEmg.getDataSource() */DataSource.GSTN).parameters(),
					1);
			if (invoiceGstns.isEmpty())
				response.put("cinvoice", new CDNDetailEntity());
			else {
				CDNDetailEntity invoiceGstn = invoiceGstns.get(0);
				invoiceGstn.setOctin(invoiceGstn.getCdnTransaction().getGstin().getGstin());
				invoiceGstn.setOctinName(invoiceGstn.getCdnTransaction().getGstin().getDisplayName());

				response.put("cinvoice", invoiceGstn);

			}
		}
		return response;

	}
	/*
	 * private List<Object> getGstnInvoice(String type, String invoiceId) throws
	 * AppException {
	 * 
	 * Class c = finderService.getEntityTypeByTransaction(type); TransactionType
	 * tType=TransactionType.valueOf(StringUtils.upperCase(type)); List<Object>
	 * mismatchedInvoices=new LinkedList<>(); if(TransactionType.B2B==tType){
	 * B2BDetailEntity invoice = crudService.find(B2BDetailEntity.class, invoiceId);
	 * mismatchedInvoices = crudService.findWithNamedQuery(
	 * "B2bDetailEntity.findMismatchedInvoice",
	 * QueryParameter.with("b2bTransaction", invoice.getB2bTransaction())
	 * .with("invoieNumber", invoice.getInvoiceNumber()) .with("dataSource",
	 * DataSource.GSTN.toString()).parameters(), 1);
	 * mismatchedInvoices.add(invoice);
	 * 
	 * }else if(TransactionType.CDN==tType){ CDNDetailEntity invoice =
	 * crudService.find(CDNDetailEntity.class, invoiceId); mismatchedInvoices =
	 * crudService.findWithNamedQuery( "CdnDetailEntity.findMismatchedInvoice",
	 * QueryParameter.with("cdnTransaction", invoice.getCdnTransaction())
	 * .with("invoieNumber", invoice.getInvoiceNumber()) .with("dataSource",
	 * DataSource.GSTN.toString()).parameters(), 1);
	 * mismatchedInvoices.add(invoice);
	 * 
	 * } return mismatchedInvoices;
	 * 
	 * 
	 * }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getMismatchedInvNoInvoices(String type, String invoiceId, FilterDto filterDto)
			throws AppException {

		/* Class c = finderService.getEntityTypeByTransaction(type); */
		TransactionType tType = TransactionType.valueOf(StringUtils.upperCase(type));
		Map<String, Object> response = new HashMap<>();

		if (TransactionType.B2B == tType) {
			B2BDetailEntity invoiceEmg = crudService.find(B2BDetailEntity.class, invoiceId);
			if (!ReconsileType.MISSING_INWARD_MISMATCH_INVOICE_NO.toString().equalsIgnoreCase(invoiceEmg.getType())) {
				AppException ae = new AppException();
				ae.setMessage("request can't be completed for this invoice");
				throw ae;
			}
			response.put("missing_inward_invoice", invoiceEmg);

			List<B2BDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
					"B2bDetailEntity.findMismatchedInvNoInvoice",
					QueryParameter.with("taxPayerGstin", invoiceEmg.getB2bTransaction().getTaxpayerGstin())
							.and("returnType", invoiceEmg.getB2bTransaction().getReturnType())
							.and("monthYear", invoiceEmg.getB2bTransaction().getMonthYear())
							.and("type", ReconsileType.MISSING_OUTWARD.toString()).and("dataSource", DataSource.EMGST)
							.and("ctin", invoiceEmg.getB2bTransaction().getCtin()).parameters(),
					1);
			if (invoiceGstns.isEmpty())
				response.put("missing_outward_invoices", new ArrayList<>());
			else {
				response.put("missing_outward_invoices", invoiceGstns);

			}
		} else if (TransactionType.CDN == tType) {
			CDNDetailEntity invoiceEmg = crudService.find(CDNDetailEntity.class, invoiceId);
			if (!ReconsileType.MISSING_INWARD_MISMATCH_INVOICE_NO.toString().equalsIgnoreCase(invoiceEmg.getType())) {
				AppException ae = new AppException();
				ae.setMessage("request can't be completed for this invoice");
				throw ae;
			}
			response.put("missing_inward_invoice", invoiceEmg);

			List<CDNDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
					"CdnDetailEntity.findMismatchedInvNoInvoice",
					QueryParameter.with("taxPayerGstin", invoiceEmg.getCdnTransaction().getGstin())
							.and("returnType", invoiceEmg.getCdnTransaction().getReturnType())
							.and("monthYear", invoiceEmg.getCdnTransaction().getMonthYear())
							.and("ctin", invoiceEmg.getCdnTransaction().getOriginalCtin())
							.and("type", ReconsileType.MISSING_OUTWARD.toString()).and("dataSource", DataSource.EMGST)
							.parameters(),
					1);
			if (invoiceGstns.isEmpty())
				response.put("missing_outward_invoices", new ArrayList<>());
			else {
				response.put("missing_outward_invoices", invoiceGstns);

			}
		}
		return response;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ginni.easemygst.portal.business.service.ReturnsService
	 * #getItcBulkItems(com.ginni.easemygst.portal.business.entity.
	 * GstinTransactionDTO, java.lang.String,
	 * com.ginni.easemygst.portal.persistence.FilterDto) get items from all
	 * transaction whose itc is not claimed
	 */
	@Override
	public <T> Map<String, Object> getItcBulkItems(GstinTransactionDTO gstinTransactionDTO, String elgType,
			FilterDto filterDto) throws AppException {

		elgType = "";// because only eligibility type null and "" required

		List<T> invoices = finderService.getInvoiceIdsByTypeMonthYearGstin(gstinTransactionDTO, filterDto);
		long count = 0;
		// filter code starts
		if (filterDto.isActive() && Objects.nonNull(filterDto.getFilterDetail())
				&& filterDto.getFilterDetail().containsKey("ctin")) {
			String ctin = "";
			for (Map.Entry<String, DataFilter> ent : filterDto.getFilterDetail().entrySet()) {
				if ("ctin".equalsIgnoreCase(ent.getKey())) {
					ctin = ent.getValue().getFilter1Value();
					ctin = "%" + ctin + "%";
				}
			}
			count = this.getPendingItcCountWithFilter(gstinTransactionDTO, ctin);

		} else {
			count = this.getPendingItcCount(gstinTransactionDTO);
		}

		// filter code ends

		Map<String, Object> temp = new HashMap<>();
		if (!Objects.isNull(invoices))
			temp.put("transactionData", invoices);
		else
			temp.put("transactionData", "[]");
		temp.put("total_count", count);
		return temp;

	}

	@Override
	public <T> Map<String, Object> getBlankHsnBulkItems(GstinTransactionDTO gstinTransactionDTO, String elgType,
			FilterDto filterDto) throws AppException {

		elgType = "";// because only eligibility type null and "" required

		List<Item> invoices = finderService.getBlankHsnInvoiceIdsByTypeMonthYearGstin(gstinTransactionDTO, filterDto);
		// long count=this.getPendingItcCount(gstinTransactionDTO);
		Map<String, Object> temp = new HashMap<>();
		if (!Objects.isNull(invoices))
			temp.put("transactionData", invoices);
		else
			temp.put("transactionData", "[]");
		// temp.put("total_count", count);
		return temp;

	}

	private long getPendingItcCount(GstinTransactionDTO gstinTransactionDTO) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		TransactionType transactionTypeEnum = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		String query = "B2bDetailEntity.findItcBulkCount";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bDetailEntity.findItcBulkCount";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnDetailEntity.findItcBulkCount";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPGTransactionEntity.findItcBulkCount";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPSTransactionEntity.findItcBulkCount";
		}
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bUrTransactionEntity.findItcBulkCount";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnUrDetailEntity.findItcBulkCount";
		}

		long count = crudService.findResultCounts(query,
				QueryParameter.with("taxPayerGstin", taxPayerGstin)
						.and("returnType", gstinTransactionDTO.getReturnType())
						.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());

		return count;

	}

	private long getPendingItcCountWithFilter(GstinTransactionDTO gstinTransactionDTO, String filterName) {
		String transactionType = gstinTransactionDTO.getTransactionType();
		TransactionType transactionTypeEnum = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		String query = "B2bDetailEntity.findItcBulkCountWithFilter";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bDetailEntity.findItcBulkCountWithFilter";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnDetailEntity.findItcBulkCountWithFilter";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPGTransactionEntity.findItcBulkCountWithFilter";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			query = "IMPSTransactionEntity.findItcBulkCountWithFilter";
		}
		if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			query = "B2bUrTransactionEntity.findItcBulkCountWithFilter";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			query = "CdnUrDetailEntity.findItcBulkCountWithFilter";
		}

		long count = crudService.findResultCounts(query,
				QueryParameter.with("taxPayerGstin", taxPayerGstin)
						.and("returnType", gstinTransactionDTO.getReturnType())
						.and("monthYear", gstinTransactionDTO.getMonthYear()).and("ctin", filterName)

						.parameters());

		return count;

	}

	// temporary
	@Override
	public <T> boolean setItcElgNull(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		List<String> ids = finderService.getInvoicesByTypeMonthYearGstin(gstinTransactionDTO);
		String queryString = "";
		if (ids.isEmpty())
			return false;
		queryString = "update Item i set i.totalEligibleTax=NULL, i.itcIgst=0.0,i.itcCgst=0.0,i.itcSgst=0.0,i.itcCess=0.0 where i.id in :ids";
		Query query = em.createQuery(queryString);
		query.setParameter("ids", ids);
		int count = query.executeUpdate();

		return true;

	}
	// end temporary

	// temporary
	@Override
	public boolean setReconcileType(GstinTransactionDTO gstinTransactionDTO, ReconsileType recType)
			throws AppException {

		if (recType == ReconsileType.COMPARE_TRANSACTION) {
			this.compareTransactions(gstinTransactionDTO);
			return true;

		}
		if (recType == ReconsileType.COMPARE_TRANSACTION_CTIN) {
			this.compareTransactions(gstinTransactionDTO, gstinTransactionDTO.getCtin());
			return true;

		}
		if (recType == ReconsileType.COPY_DATA) {
			this.copyDataGstr1ToGstr2(gstinTransactionDTO);
			return true;
		}
		List<String> ids = finderService.getInvoicesByTypeMonthYearGstin(gstinTransactionDTO);
		TransactionType type = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());

		for (String id : ids) {
			if (TransactionType.B2B == type) {
				B2BDetailEntity b2b = crudService.find(B2BDetailEntity.class, id);
				b2b.setType(recType.toString());
				crudService.update(b2b);
			} else if (TransactionType.CDN == type) {
				CDNDetailEntity b2b = crudService.find(CDNDetailEntity.class, id);
				b2b.setType(recType.toString());
				crudService.update(b2b);
			}
		}

		return true;

	}
	// end temporary

	// temporary
	// @Override
	public boolean copyDataGstr1ToGstr2(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		TaxpayerGstin tp = new TaxpayerGstin();
		tp.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		TransactionType type = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		if (TransactionType.B2B == type) {
			List<B2BTransactionEntity> b2bs = crudService
					.findWithNamedQuery("B2bTransaction.getByGstinMonthyearReturntype",
							QueryParameter.with("taxPayerGstin", tp)
									.and("monthYear", gstinTransactionDTO.getMonthYear())
									.and("returnType", gstinTransactionDTO.getReturnType()).parameters());

			List<B2BTransactionEntity> newB2bs = new ArrayList<>();
			for (B2BTransactionEntity b2b : b2bs) {
				List<B2BTransactionEntity> b2bSearch = crudService
						.findWithNamedQuery("B2bTransaction.getByGstinMonthyearReturntypeCtin",
								QueryParameter.with("taxPayerGstin", tp)
										.and("monthYear", gstinTransactionDTO.getMonthYear()).and("returnType", "GSTR2")
										.and("ctin", b2b.getCtin()).parameters());

				if (!b2bSearch.isEmpty()) {
					b2bSearch.get(0);
					for (B2BDetailEntity bd : b2b.getB2bDetails()) {
						bd.setDataSource(DataSource.GSTN);
						bd.setB2bTransaction(b2bSearch.get(0));
						;
					}
					crudService.update(b2bSearch.get(0));
				} else {

					b2b.setReturnType("GSTR2");
					for (B2BDetailEntity bd : b2b.getB2bDetails()) {
						bd.setDataSource(DataSource.GSTN);
					}
					crudService.update(b2b);
				}
			}

			// new B2BTransaction().saveTransaction(b2bs);

		} else if (TransactionType.CDN == type) {
			{
				List<CDNTransactionEntity> b2bs = crudService
						.findWithNamedQuery("CdnTransaction.getByGstinMonthyearReturntype",
								QueryParameter.with("taxPayerGstin", tp)
										.and("monthYear", gstinTransactionDTO.getMonthYear())
										.and("returnType", gstinTransactionDTO.getReturnType()).parameters());

				for (CDNTransactionEntity cdn : b2bs) {
					List<CDNTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
							"CdnTransaction.getByGstinMonthyearReturntypeCtin",
							QueryParameter.with("taxPayerGstin", tp)
									.and("monthYear", gstinTransactionDTO.getMonthYear()).and("returnType", "GSTR2")
									.and("ctin", cdn.getOriginalCtin()).parameters());

					if (!b2bSearch.isEmpty()) {
						b2bSearch.get(0);
						for (CDNDetailEntity bd : cdn.getCdnDetails()) {
							bd.setDataSource(DataSource.GSTN);
							bd.setCdnTransaction(b2bSearch.get(0));
							;
						}
						crudService.update(b2bSearch.get(0));
					} else {

						cdn.setReturnType("GSTR2");
						for (CDNDetailEntity bd : cdn.getCdnDetails()) {
							bd.setDataSource(DataSource.GSTN);
						}
						crudService.update(cdn);
					}
					// crudService.update(cdn);
				}
			}
		}

		return true;

	}

	// end temporary
	@Override
	public boolean saveBulkItcData(GstinTransactionDTO gstinTransactionDto, List<BulkItcDto> items)
			throws AppException {
		Map<String, List<BulkItcDto>> groupByElgTypeMap = items.stream()
				.collect(Collectors.groupingBy(BulkItcDto::getElgType));

		for (Map.Entry<String, List<BulkItcDto>> entry : groupByElgTypeMap.entrySet()) {
			String queryString = "";
			List<String> ids = new ArrayList<>();
			for (BulkItcDto item : entry.getValue()) {
				ids.add(item.getItemId());
			}
			ItcEligibilityType elgType = ItcEligibilityType.valueOf(StringUtils.upperCase(entry.getKey()));
			if (!this.isValidItcEligType(gstinTransactionDto,
					TransactionType.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType())),
					elgType)) {
				AppException ae = new AppException(ExceptionCode._ERROR);
				ae.setMessage("Invalid itc eligibilty type -" + elgType.toString() + " for this transaction ");
				throw ae;
			}
			if (ItcEligibilityType.NO == elgType) {
				queryString = "update Item i set i.totalEligibleTax='%s', i.itcIgst=0.0,i.itcCgst=0.0,i.itcSgst=0.0,i.itcCess=0.0 where i.id in :ids";

			} else {
				queryString = "update Item i set i.totalEligibleTax='%s', i.itcIgst=i.igst,i.itcCgst=i.cgst,i.itcSgst=i.sgst,i.itcCess=i.cess where i.id in :ids";
			}
			Query query = em.createQuery(String.format(queryString, elgType.toString()));
			query.setParameter("ids", ids);
			int count = query.executeUpdate();
			_Logger.info("itc of " + count + " items have been succesfully changed");
		}
		return true;
	}

	private boolean isValidItcEligType(GstinTransactionDTO gstinTransactionDto, TransactionType tranType,
			ItcEligibilityType elgType) {
		List<String> valids;
		if (TransactionType.IMPG == tranType) {
			valids = Arrays.asList("no", "ip", "cp");

		} else if (TransactionType.IMPS == tranType) {
			valids = Arrays.asList("no", "is");
		} else {
			valids = Arrays.asList("no", "ip", "cp", "is");
		}
		if (valids.contains(StringUtils.lowerCase(elgType.toString())))
			return true;
		else
			return false;
	}

	@Override
	public String getReconciledTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)
				&& !StringUtils.isEmpty(gstinTransactionDTO.getTaxpayerGstin().getGstin())) {

			// will change this
			gstinTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType().replace("a", "").replace("A", ""));

			String type = gstinTransactionDTO.getTransactionType();
			GstrSummaryDTO gstrSummaryDTO = null;// getTransactionSummary(gstinTransactionDTO);
			gstinTransactionDTO.setTransactionType(type);

			GstinTransaction gstinTransaction = null;
			String reconciled = "[]";
			boolean isReconciled = false;
			TransactionFactory factory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			Object obj = factory.getTransactionSummary(gstrSummaryDTO);

			if (gstinTransactionDTO.getReturnType().equalsIgnoreCase(ReturnType.GSTR2.toString())
					&& (gstinTransactionDTO.getTransactionType().equalsIgnoreCase(TransactionType.B2B.toString())
							|| gstinTransactionDTO.getTransactionType()
									.equalsIgnoreCase(TransactionType.CDN.toString()))) {
				String buyerObj = "";
				GstinTransaction buyer = finderService.getGstinTransaction(gstinTransactionDTO, false);
				buyerObj = Objects.isNull(buyer) ? "" : buyer.getTransactionObject();
				String supplier = finderService.getCtinTransaction(gstinTransactionDTO, ReturnType.GSTR1.toString());
				reconciled = factory.compareTransactions(buyerObj, supplier, ViewType1.BUYER);

				JSONObject recoJson = new JSONObject(reconciled);
				reconciled = recoJson.get("reconcile") != null ? recoJson.get("reconcile").toString() : "[]";
				isReconciled = true;
				/*
				 * gstinTransaction = getGstinTransaction(gstinTransactionDTO, true);
				 */
			} else if ((gstinTransactionDTO.getReturnType().equalsIgnoreCase(ReturnType.GSTR1.toString())
					|| gstinTransactionDTO.getReturnType().equalsIgnoreCase(ReturnType.GSTR1A.toString()))
					&& (gstinTransactionDTO.getTransactionType().equalsIgnoreCase(TransactionType.B2B.toString())
							|| gstinTransactionDTO.getTransactionType()
									.equalsIgnoreCase(TransactionType.CDN.toString()))) {
				String supplierObj = "";
				GstinTransaction supplier = finderService.getGstinTransaction(gstinTransactionDTO, false);
				supplierObj = Objects.isNull(supplier) ? "" : supplier.getTransactionObject();
				String buyer = finderService.getCtinTransaction(gstinTransactionDTO, ReturnType.GSTR2.toString());
				reconciled = factory.compareTransactions(buyer, supplierObj, ViewType1.SUPPLIER);

				JSONObject recoJson = new JSONObject(reconciled);
				reconciled = recoJson.get("reconcile") != null ? recoJson.get("reconcile").toString() : "[]";
				isReconciled = true;
				/*
				 * gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString() );
				 * gstinTransaction = getGstinTransaction(gstinTransactionDTO, true);
				 */
			} else {
				gstinTransaction = finderService.getGstinTransaction(gstinTransactionDTO, false);
				reconciled = gstinTransaction != null ? gstinTransaction.getTransactionObject() : "[]";
			}

			// get processed data from factory class
			String subType = gstinTransactionDTO.getStatus();
			if (!StringUtils.isEmpty(subType)) {
				reconciled = factory.getReconciledTransactionsData(reconciled, subType.toUpperCase());
			} else {
				reconciled = factory.getTransactionsData(reconciled);
			}
			/*
			 * if(Objects.isNull(gstinTransaction) ||
			 * StringUtils.isEmpty(gstinTransaction.getTransactionObject()))
			 * gstinTransaction = getGstinTransaction(gstinTransactionDTO, false);
			 * transaction = Objects.isNull(gstinTransaction) ? null :
			 * gstinTransaction.getTransactionObject();
			 */

			reconciled = StringUtils.isEmpty(reconciled) ? "[]" : reconciled;

			Map<String, Object> temp = new HashMap<>();
			temp.put("data", reconciled);
			temp.put("summary", obj);

			GstinActionDTO gstinActionDTO = new GstinActionDTO();
			gstinActionDTO.setReturnType(gstinTransactionDTO.getReturnType());
			gstinActionDTO.setStatus(GstnAction.SUBMIT.toString());
			gstinActionDTO.setParticular(gstinTransactionDTO.getMonthYear());

			GstinAction action = finderService.findGstinAction(gstinActionDTO,
					gstinTransactionDTO.getTaxpayerGstin().getGstin());

			if (Objects.isNull(action)) {
				gstinActionDTO.setStatus(GstnAction.FILED.toString());
				GstinAction action2 = finderService.findGstinAction(gstinActionDTO,
						gstinTransactionDTO.getTaxpayerGstin().getGstin());
				if (Objects.isNull(action2))
					temp.put("isEdit", "true");
				else
					temp.put("isEdit", "false");
			} else
				temp.put("isEdit", "false");

			try {
				return JsonMapper.objectMapper.writeValueAsString(temp);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			try {
				return JsonMapper.objectMapper.writeValueAsString(temp);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return "";
	}

	/*
	 * @Override public List<List<String>>
	 * getExcelDataByFetchType(GstinTransactionDTO gstinTranDto, DataFetchType
	 * dataFetchType) throws AppException { List<List<String>> xlFormatedData = new
	 * ArrayList<>();
	 * 
	 * if (!Objects.isNull(gstinTranDto) &&
	 * !StringUtils.isEmpty(gstinTranDto.getTaxpayerGstin().getGstin())) { String
	 * transactionData = this.getTransactionData(gstinTranDto); Transaction
	 * transaction = new TransactionFactory(gstinTranDto.getTransactionType()); if
	 * (DataFetchType.INVALID.equals(dataFetchType)) { xlFormatedData =
	 * transaction.getErrorData(transactionData, gstinTranDto.getReturnType());
	 * log.debug("xlformated invalid data"); } else if
	 * (DataFetchType.ALL.equals(dataFetchType)) { xlFormatedData =
	 * transaction.getCompleteData(transactionData, gstinTranDto.getReturnType());
	 * log.debug("xlformated all data"); }
	 * 
	 * } else { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
	 * 
	 * return xlFormatedData; }
	 */

	@Override
	public List<List<String>> getTransactionByType(GstinTransactionDTO gstinTransactionDTO, DataFetchType dataFetchType)
			throws AppException {

		List dataList = this.getTransactionDataForExcel(gstinTransactionDTO);

		if (!Objects.isNull(dataList)) {
			List<List<String>> data = new ArrayList<>();
			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());
			if (dataFetchType.equals(DataFetchType.INVALID))
				data = transactionFactory.getErrorData(dataList, gstinTransactionDTO.getReturnType());
			else if (dataFetchType.equals(DataFetchType.ALL))
				data = transactionFactory.getCompleteData(dataList, gstinTransactionDTO.getReturnType());

			return data;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private List getTransactionDataForExcel(GstinTransactionDTO gstinTransactionDTO) {
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		List data = crudService.findWithNamedQuery(this.getDataQueryByType(gstinTransactionDTO.getTransactionType()),
				QueryParameter.with("taxPayerGstin", gstin).and("returnType", gstinTransactionDTO.getReturnType())
						.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());

		return data;

	}

	private String getDataQueryByType(String transactionType) {
		String orderByField = "";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2bDetailEntity.findErrorDataByMonthYear";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "CdnDetailEntity.findErrorDataByMonthYear";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "CdnUrDetailEntity.findErrorDataByMonthYear";
		} else if (TransactionType.B2CL.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2clDetailEntity.findErrorDataByMonthYear";
		} else if (TransactionType.B2CS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2csTransaction.findErrorDataByMonthYear";
		} else if (TransactionType.EXP.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "ExpDetail.findErrorDataByMonthYear";
		} else if (TransactionType.AT.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "AtTransaction.findErrorDataByMonthYear";
		} else if (TransactionType.TXPD.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "TxpdTransaction.findErrorDataByMonthYear";
		} else if (TransactionType.HSNSUM.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "HsnTransaction.findErrorDataByMonthYear";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "IMPGTransactionEntity.findErrorDataByMonthYear";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "IMPSTransactionEntity.findErrorDataByMonthYear";
		} else if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2bUrTransactionEntity.findErrorDataByMonthYear";
		} else if (TransactionType.NIL.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "NILTransactionEntity.findErrorDataByMonthYear";
		} else if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "DocIssue.findErrorDataByMonthYear";
		}

		return orderByField;
	}

	@Override
	public Map<String, List<List<String>>> getExceptionTransactions(GstinTransactionDTO gstinTransactionDTO)
			throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {
			Map<String, List<List<String>>> data = new HashMap<>();

			List<List<String>> changedList = new ArrayList<>();
			List<List<String>> deletedList = new ArrayList<>();
			List<List<String>> addedList = new ArrayList<>();

			data.put(ExceptionSheet.CHANGES.toString(), changedList);
			data.put(ExceptionSheet.ADDED.toString(), addedList);
			data.put(ExceptionSheet.DELETED.toString(), deletedList);

			TransactionFactory transactionFactory = new TransactionFactory(gstinTransactionDTO.getTransactionType());

			ErpTransactionDTO erpTransactionDTO = new ErpTransactionDTO();
			erpTransactionDTO.setMonthYear(gstinTransactionDTO.getMonthYear());
			erpTransactionDTO.setReturnType(gstinTransactionDTO.getReturnType());
			erpTransactionDTO.setTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin());
			erpTransactionDTO.setTransactionType(gstinTransactionDTO.getTransactionType());

			GstinTransaction changed = finderService.getGstinTransaction(gstinTransactionDTO, false);
			ErpTransaction source = finderService.getErpTransaction(erpTransactionDTO);

			if (!Objects.isNull(source) && !Objects.isNull(changed)) {
				data = transactionFactory.getExceptionData(source.getTransactionObject(),
						changed.getTransactionObject(), gstinTransactionDTO.getReturnType());
			}

			return data;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	/*
	 * @Override public GstrSummaryDTO getTransactionSummary(GstinTransactionDTO
	 * gstinTransactionDTO) throws AppException {
	 * 
	 * try { gstinTransactionDTO.setTransactionType("SUMMARY"); GstinTransaction
	 * gstinTransaction = finderService.getGstinTransaction(gstinTransactionDTO,
	 * false);
	 * 
	 * GstrSummaryDTO gstrSummaryDTO = null; if (!Objects.isNull(gstinTransaction))
	 * { String temp = gstinTransaction.getTransactionObject() != null ?
	 * gstinTransaction.getTransactionObject() : "{}"; gstrSummaryDTO =
	 * JsonMapper.objectMapper.readValue(temp, GstrSummaryDTO.class); } else {
	 * gstrSummaryDTO = new GstrSummaryDTO(); }
	 * 
	 * return gstrSummaryDTO; } catch (IOException e) { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } }
	 */
	public Map<String, Object> getTransactionSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		_Logger.info("getTransactionSummary method strarts ");
		if (!Objects.isNull(gstinTransactionDTO)) {
			boolean isUploaded = false;

			String type = gstinTransactionDTO.getTransactionType();
			String queryName = "";
			Map<String, String> queries = new HashMap<>();

			Map<String, Object> queryParameters = new HashMap<>();

			queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
			queryParameters.put("taxPayerGstin",
					(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
			queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());

			if (TransactionType.B2B.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "B2bDetailEntity.findSummaryByMonthYear";
				queries.put(TransactionType.B2B.toString(), queryName);

			}
			if (TransactionType.CDN.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "CdnDetailEntity.findSummaryByMonthYear";
				queries.put(TransactionType.CDN.toString(), queryName);

			}
			if (TransactionType.CDNUR.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "CdnUrDetailEntity.findSummaryByMonthYear";
				queries.put(TransactionType.CDNUR.toString(), queryName);

			}
			if (ReturnType.GSTR1 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {
				if (TransactionType.B2CL.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "B2clDetailEntity.findSummaryByMonthYear";
					queries.put(TransactionType.B2CL.toString(), queryName);

				}
				if (TransactionType.B2CS.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "B2csTransaction.findSummaryByMonthYear";
					queries.put(TransactionType.B2CS.toString(), queryName);

				}
				if (TransactionType.EXP.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "ExpDetailEntity.findSummaryByMonthYear";
					queries.put(TransactionType.EXP.toString(), queryName);

				}
				if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "DocIssue.findSummaryByMonthYear";
					queries.put(TransactionType.DOC_ISSUE.toString(), queryName);

				}

			}

			if (TransactionType.AT.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "AtTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.AT.toString(), queryName);

			}
			if (TransactionType.TXPD.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "TxpdTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.TXPD.toString(), queryName);

			}
			if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "HsnTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.HSNSUM.toString(), queryName);

			}
			if (TransactionType.NIL.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "NilTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.NIL.toString(), queryName);
			}
			if (ReturnType.GSTR2 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {
				if (TransactionType.IMPG.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "IMPGTransactionEntity.findSummaryByMonthYear";
					queries.put(TransactionType.IMPG.toString(), queryName);
				}
				if (TransactionType.IMPS.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "IMPSTransactionEntity.findSummaryByMonthYear";
					queries.put(TransactionType.IMPS.toString(), queryName);
				}

				if (TransactionType.B2BUR.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "B2bUrTransaction.findSummaryByMonthYear";
					queries.put(TransactionType.B2BUR.toString(), queryName);
				}
			}

			Map<String, Object> response = new HashMap<>();
			for (Map.Entry<String, String> entry : queries.entrySet()) {
				Object[] summary = crudService.runAggregateNamedQuery(entry.getValue(), queryParameters);
				// log.debug("getting summary for transaction {}",
				// entry.getKey());
				boolean isExclamtion = false;
				TransactionDataDTO tdDto = new TransactionDataDTO();
				if (TransactionType.B2B.toString().equalsIgnoreCase(entry.getKey())
						|| TransactionType.CDN.toString().equalsIgnoreCase(entry.getKey())) {
					tdDto.setMismatchCounts(this.getMisMatchCount(gstinTransactionDTO, entry.getKey()));
					isExclamtion = tdDto.getMismatchCounts().getTotalCount() > 0 ? true : false;
				}

				TransactionDataDTO resDto = this.getToBeSyncCounts(gstinTransactionDTO, entry.getKey());
				tdDto.setItcAmounts(this.getItcSummary(gstinTransactionDTO, entry.getKey()));
				tdDto.setToBeSyncCount(resDto.getToBeSyncCount());
				tdDto.setNotToBeSyncCount(resDto.getNotToBeSyncCount());
				tdDto.setInvoices(resDto.getInvoices());
				tdDto.setErrorCount(resDto.getErrorCount());
				if (tdDto.getErrorCount() > 0 || isExclamtion) {
					tdDto.setExclamation(true);
				}

				if (TransactionType.NIL.toString().equalsIgnoreCase(entry.getKey())
						|| TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(entry.getKey())) {
					if (TransactionType.NIL.toString().equalsIgnoreCase(entry.getKey())) {
						tdDto.setExemptedAmount(Objects.isNull(summary[0]) ? 0.0 : (Double) summary[0]);
						tdDto.setNilAmount(Objects.isNull(summary[1]) ? 0.0 : (Double) summary[1]);
						tdDto.setNonGstAmount(Objects.isNull(summary[2]) ? 0.0 : (Double) summary[2]);
						tdDto.setCompositeAmount(Objects.isNull(summary[3]) ? 0.0 : (Double) summary[3]);
						if (tdDto.getNilAmount() > 0)
							isUploaded = true;

					} else {
						tdDto.setTotalDocIssued(Objects.isNull(summary[0]) ? 0 : (Long) summary[0]);
						tdDto.setTotalCanceled(Objects.isNull(summary[1]) ? 0 : (Long) summary[1]);
						tdDto.setNetDocIssued(Objects.isNull(summary[2]) ? 0 : (Long) summary[2]);
					}

				}
				if (!TransactionType.NIL.toString().equalsIgnoreCase(entry.getKey())
						&& !TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(entry.getKey())) {
					tdDto.setTaxableValue(Objects.isNull(summary[0]) ? 0.0 : (Double) summary[0]);
					tdDto.setTaxAmount(Objects.isNull(summary[1]) ? 0.0 : (Double) summary[1]);
					if (TransactionType.CDN.toString().equalsIgnoreCase(entry.getKey())
							|| TransactionType.CDNUR.toString().equalsIgnoreCase(entry.getKey())) {
						String query = TransactionType.CDN.toString().equalsIgnoreCase(entry.getKey())
								? "CdnDetailEntity.findSummaryByMonthYearCredRefund"
								: "CdnUrDetailEntity.findSummaryByMonthYearCredRefund";
						Object[] creditRefundSumm = crudService.runAggregateNamedQuery(query, queryParameters);
						Double creditTaxAmount = Objects.isNull(creditRefundSumm[1]) ? 0.0
								: (Double) creditRefundSumm[1];
						Double creditTaxableValue = Objects.isNull(creditRefundSumm[0]) ? 0.0
								: (Double) creditRefundSumm[0];
						tdDto.setTaxAmount(tdDto.getTaxAmount() - creditTaxAmount);
						tdDto.setTaxableValue(tdDto.getTaxableValue() - creditTaxableValue);

					}
					if (tdDto.getTaxableValue() > 0)
						isUploaded = true;
				}

				response.put(StringUtils.lowerCase(entry.getKey()), tdDto);
			}
			if ("ALL".equalsIgnoreCase(type) && ReturnType.GSTR1 == ReturnType
					.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {// temporary
																							// initializes
				TransactionDataDTO temp = new TransactionDataDTO();
				temp.setTaxableValue(0.0);
				temp.setTaxAmount(0.0);
				// response.put("doc_issue", temp);
				response.put("isUploaded", isUploaded);

			}
			if ("ALL".equalsIgnoreCase(type) && ReturnType.GSTR2 == ReturnType
					.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {// temporary
																							// initializes
				TransactionDataDTO temp = new TransactionDataDTO();
				temp.setTaxableValue(0.0);
				temp.setTaxAmount(0.0);
				response.put("addre", temp);
				response.put("itcr", temp);
				response.put("isUploaded", false);

			}
			_Logger.info("getTransactionSummary method ends ");

			return response;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public Map<String, Object> getGstr2ASumm(GstinTransactionDTO gstinTransactionDTO) {

		Map<String, Object> gstr2ASum = new HashMap<>();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		String transaction = gstinTransactionDTO.getTransactionType();
		gstinTransactionDTO.setTransactionType("B2B");

		TokenResponse tokenResponse = null;
		List<TokenResponse> tokenResponses = crudService
				.findWithNamedQuery("TokenResponse.findLatestGstinMonthYear",
						QueryParameter
								.with("taxpayerGstin",
										finderService
												.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()))
								.and("returnType", gstinTransactionDTO.getReturnType())
								.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters(),
						1);

		if (tokenResponses != null && !tokenResponses.isEmpty())

			tokenResponse = tokenResponses.get(0);

		gstr2ASum.put("initAt", tokenResponse != null ? dateFormat.format(tokenResponse.getUpdationTime()) : "N.A.");
		gstr2ASum.put("status", tokenResponse != null ? tokenResponse.getStatus() : "N.A.");

		gstinTransactionDTO.setTransactionType(transaction);

		return gstr2ASum;

	}

	@Override
	public Map<String, Object> getLastSyncWithGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		_Logger.info("getLastSyncWithGstn called  for gstin {}", gstinTransactionDTO.getTaxpayerGstin().getId());

		ReturnStatus returnStatus = null;
		try {
			returnStatus = em.createNamedQuery("ReturnStatus.findByGstnReturnTypeMonthYear", ReturnStatus.class)
					.setParameter("gstin", gstinTransactionDTO.getTaxpayerGstin().getGstin())
					.setParameter("returnType", gstinTransactionDTO.getReturnType())
					.setParameter("monthYear", gstinTransactionDTO.getMonthYear()).setFirstResult(0).setMaxResults(1)
					.getSingleResult();
		} catch (NoResultException e) {
			_Logger.info("No sync status timing available for gstin {}",
					gstinTransactionDTO.getTaxpayerGstin().getId());
		}
		Map<String, Object> temp = new HashMap<>();
		if (!Objects.isNull(returnStatus) && !Objects.isNull(returnStatus.getCreationTime())) {
			temp.put("last_sync_with_gstn", Transaction.sdfLastSync.format(returnStatus.getCreationTime()));
		} else
			temp.put("last_sync_with_gstn", ApplicationMetadata.SYNC_TIME_NOT_FOUND);

		_Logger.info("getLastSyncWithGstn called successfuly get timings for gstin {}",
				gstinTransactionDTO.getTaxpayerGstin().getId());

		return temp;
	}

	@Override
	public List<ReturnSummaryDTO> getReturnSummaryDto(TaxpayerGstinDTO taxpayerGstin, String month)
			throws AppException {

		List<ReturnSummaryDTO> returnSummaryDTOs = new ArrayList<>();

		List<GstinAction> actions = finderService.findGstinActionByTaxpayerGstinId(taxpayerGstin.getId());
		if (Objects.isNull(actions))
			actions = new ArrayList<>();

		List<BrtMappingDTO> brtMappingDTOs = new ArrayList<>();
		String type = taxpayerGstin.getType();

		String[] tempt = {};
		if (!StringUtils.isEmpty(type)) {
			tempt = type.split(":");
		}
		brtMappingDTOs = finderService.getBrtMapping(tempt);

		TreeSet<GstReturnDTO> monthlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
		TreeSet<GstReturnDTO> quarterlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
		TreeSet<GstReturnDTO> annuallyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());

		for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
			GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
			if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.MONTHLY.toString()))
				monthlyComp.add(gstReturnDTO);
			else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.QUARTERLY.toString()))
				quarterlyComp.add(gstReturnDTO);
			else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.ANNUALLY.toString()))
				annuallyComp.add(gstReturnDTO);
		}

		boolean startFlag = true;

		Date currentDate = Calendar.getInstance().getTime();
		Date startDate = Calendar.getInstance().getTime();

		int inpMonth = 0;
		int inpYear = 0;
		int prevMonth = 0;
		int prevYear = 0;

		inpMonth = Integer.parseInt(month.substring(0, 2));
		inpYear = Integer.parseInt(month.substring(2, 6));

		if (inpMonth == 1) {
			prevMonth = 12;
			prevYear = inpYear - 1;
		} else {
			prevMonth = inpMonth - 1;
			prevYear = inpYear - 1;
		}

		String prevMonthYear = String.format("%02d", prevMonth) + String.format("%04d", prevYear);

		SimpleDateFormat fullDate = new SimpleDateFormat("dd MMM yyyy");
		for (GstReturnDTO gstReturnDTO : monthlyComp) {

			ReturnSummaryDTO returnSummaryDTO = new ReturnSummaryDTO();

			String status = ReturnCalendar.BLANK.toString();
			GstinAction temp = new GstinAction();
			temp.setParticular(month);
			temp.setReturnType(gstReturnDTO.getCode());
			temp.setStatus(ReturnCalendar.FILED.toString());
			Date returnDueDate = gstReturnDTO.getDueDate();
			Date returnStartDate = gstReturnDTO.getStartDate();

			Calendar newCal = Calendar.getInstance();
			newCal.set(Calendar.DATE, returnDueDate.getDate());
			newCal.set(Calendar.YEAR, inpYear);
			newCal.set(Calendar.MONTH, inpMonth - 1);
			newCal.add(Calendar.MONTH, 1);
			if (actions.contains(temp)) {
				temp = actions.get(actions.indexOf(temp));
			} else {
				temp = null;
			}

			Date newTime = newCal.getTime();

			Calendar newCal2 = Calendar.getInstance();
			newCal2.set(Calendar.DATE, returnStartDate.getDate());
			Date newStartTime = newCal2.getTime();

			GstCalendar calendar = AppConfig.getGstCalendarByCode(month, gstReturnDTO.getCode());
			if (!Objects.isNull(calendar)) {
				newTime = calendar.getDueDate();
				newStartTime = calendar.getStartDate();
			}

			double diff = currentDate.getTime() - newTime.getTime();
			double startDiff = currentDate.getTime() - newStartTime.getTime();

			if (!Objects.isNull(temp) && temp.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
				status = ReturnCalendar.FILED.toString();
				returnSummaryDTO.setDiff(0);
			} else {
				diff = diff / (24 * 60 * 60 * 1000);
				if (startDiff >= 0.0 && diff <= 0.0) {
					status = ReturnCalendar.APPROACHING.toString();
				} else if (diff > 0.0) {
					status = ReturnCalendar.LOCKED.toString();
					if (gstReturnDTO.getMandatory() == 1) {
						status = ReturnCalendar.OVERDUE.toString();
					}
					if (currentDate.getDate() >= 11 && currentDate.getDate() <= 15
							&& gstReturnDTO.getCode().equalsIgnoreCase(ReturnType.GSTR1.toString())) {
						status = ReturnCalendar.ONHOLD.toString();
					}
				}

				if (gstReturnDTO.getDependent() > 0) {
					GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getDependent());
					GstinAction temp1 = new GstinAction();
					temp1.setReturnType(gstReturn.getCode());
					temp1.setStatus(GstnAction.FILED.toString());
					if (gstReturn.getPrevMonth() == 1)
						temp1.setParticular(prevMonthYear);
					else
						temp1.setParticular(month);
					if (!actions.contains(temp1)) {
						status = status + "_LOCKED";
					}

					if (gstReturn.getPrevMonth() == 1 && startFlag)
						status = status.replaceAll("_LOCKED", "");
				}

				if (gstReturnDTO.getLockedAfter() > 0) {
					GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getLockedAfter());
					GstinAction temp1 = new GstinAction();
					temp1.setReturnType(gstReturn.getCode());
					temp1.setParticular(month);
					temp1.setStatus(GstnAction.FILED.toString());
					if (actions.contains(temp1) && status.indexOf("_LOCKED") == -1) {
						status = status + "_LOCKED";
					}
				}

				returnSummaryDTO.setDiff(Double.valueOf(diff).intValue());
			}

			// Introduces this because of pre process to incorporate
			// Ledger in workflow
			if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {

				ReturnSummaryDTO returnSummaryDTO1 = new ReturnSummaryDTO();

				String status1 = ReturnCalendar.BLANK.toString();
				GstinAction temp1 = new GstinAction();
				temp1.setParticular(month);
				temp1.setReturnType(gstReturnDTO.getCode());

				if (actions.contains(temp1)) {
					temp1 = actions.get(actions.indexOf(temp1));
				} else {
					temp1 = null;
				}

				if (!Objects.isNull(temp1) && temp1.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
					status1 = ReturnCalendar.FILED.toString();
					returnSummaryDTO1.setDiff(Double.valueOf("0").intValue());
				} else {
					status1 = status;
					returnSummaryDTO1.setDiff(Double.valueOf(diff).intValue());
				}

				String name = "", description = "";
				if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {
					String temp11 = gstReturnDTO.getPreProcess();
					if (temp11.contains("-")) {
						String[] values = temp11.split("-");
						name = values[0];
						description = values[1];
					} else {
						name = gstReturnDTO.getPreProcess();
						description = gstReturnDTO.getPreProcess();
					}
				}

				returnSummaryDTO1.setDueDate(fullDate.format(newTime));
				returnSummaryDTO1.setReturnName(name);
				returnSummaryDTO1.setStatus(status1);
				returnSummaryDTO1.setDescription(description);
				returnSummaryDTO1.setName(name);
				returnSummaryDTOs.add(returnSummaryDTO1);
			}
			// ends here

			returnSummaryDTO.setDueDate(fullDate.format(newTime));
			returnSummaryDTO.setReturnName(gstReturnDTO.getCode());
			returnSummaryDTO.setStatus(status);
			returnSummaryDTO.setDescription(gstReturnDTO.getDescription());
			returnSummaryDTO.setName(gstReturnDTO.getName());
			returnSummaryDTOs.add(returnSummaryDTO);
		}

		return returnSummaryDTOs;
	}

	@Override
	public WrapperDashBoardSummaryDto getDashboardSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		_Logger.info("getDashboardSummary method strarts ");

		List<ReturnSummaryDTO> returnSummaryDTOs = getReturnSummaryDto(gstinTransactionDTO.getTaxpayerGstin(),
				gstinTransactionDTO.getMonthYear());

		Map<String, String> map = new HashMap<>();
		if ("B2B".equalsIgnoreCase(gstinTransactionDTO.getTransactionType())) {
			map.put("GSTR1", "B2B");
			map.put("GSTR2", "B2B");
		} else {
			map.put(gstinTransactionDTO.getReturnType(), gstinTransactionDTO.getTransactionType());

		}
		String transactionType = gstinTransactionDTO.getTransactionType();
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		WrapperDashBoardSummaryDto dbSumm = new WrapperDashBoardSummaryDto();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMyyyy");
		String gstYearMonth = ApplicationMetadata.GST_YEARMONTH;
		// if("local".equalsIgnoreCase(AppConfig.getActiveProfile()))
		// gstYearMonth="052016";
		YearMonth gstStartingYearMonth = YearMonth.parse(gstYearMonth, formatter);
		YearMonth currentYearMonth = YearMonth.now();
		String monthYear = gstinTransactionDTO.getMonthYear();
		YearMonth yearMonth = YearMonth.parse(monthYear, formatter);
		YearMonth previousYearMonth = yearMonth.plusMonths(-1);
		YearMonth nextYearMonth = yearMonth.plusMonths(+1);

		dbSumm.setPreviousMonthYear(previousYearMonth.format(formatter));
		dbSumm.setNextMonthYear(nextYearMonth.format(formatter));

		DateTimeFormatter viewFormater = DateTimeFormatter.ofPattern("MMM yyyy");
		dbSumm.setPreviousMonthYearShort(previousYearMonth.format(viewFormater));
		dbSumm.setNextMonthYearShort(nextYearMonth.format(viewFormater));

		dbSumm.setCurrentFiscal(this.getMonthYearCalendar());
		dbSumm.setMonthYear(yearMonth.format(viewFormater));
		if (yearMonth.compareTo(gstStartingYearMonth) < 0 || yearMonth.compareTo(currentYearMonth) > 0) {
			AppException ae = new AppException();
			ae.setMessage("Invalid month year value");
			throw ae;
		}

		if (yearMonth.compareTo(gstStartingYearMonth) > 0) {
			dbSumm.setPrevious(true);
		} else {
			dbSumm.setPrevious(false);

		}

		if (yearMonth.compareTo(currentYearMonth) < 0) {
			dbSumm.setNext(true);
		} else {
			dbSumm.setNext(false);
		}

		if (!Objects.isNull(gstinTransactionDTO)) {

			for (Map.Entry<String, String> ent : map.entrySet()) {
				DashBoardSummaryDto summDto = new DashBoardSummaryDto();

				gstinTransactionDTO.setTransactionType(ent.getValue());
				gstinTransactionDTO.setReturnType(ent.getKey());
				String queryName = this.getQueryByType(gstinTransactionDTO.getTransactionType());
				if (StringUtils.isEmpty(queryName))
					return new WrapperDashBoardSummaryDto();
				summDto.setInvoices(this.getTotalCount(gstinTransactionDTO, true));
				gstinTransactionDTO.setTransactionType("ALL");
				Map<String, Object> taxValues = this.getTransactionSummary(gstinTransactionDTO);
				Double totalTaxableValue = 0.0;
				Double totalTaxAmount = 0.0;
				for (Map.Entry<String, Object> entry : taxValues.entrySet()) {
					if ("isUploaded".equalsIgnoreCase(entry.getKey())
							|| TransactionType.HSNSUM.toString().equalsIgnoreCase(entry.getKey()))
						continue;

					TransactionDataDTO tDto = (TransactionDataDTO) entry.getValue();
					totalTaxableValue += tDto.getTaxableValue();
					if (TransactionType.TXPD.toString().equalsIgnoreCase(entry.getKey())) {
						totalTaxAmount -= tDto.getTaxAmount();
					} else
						totalTaxAmount += tDto.getTaxAmount();

				}
				summDto.setTransactionSummary(taxValues);
				summDto.setTotalTaxableValue(totalTaxableValue);
				summDto.setTotalTaxAmount(totalTaxAmount);

				if (ReturnType.GSTR2.toString().equalsIgnoreCase(ent.getKey())
						&& (TransactionType.B2B == TransactionType.valueOf(ent.getValue())
								|| TransactionType.CDN == TransactionType.valueOf(ent.getValue())
								|| TransactionType.IMPG == TransactionType.valueOf(ent.getValue())
								|| TransactionType.IMPS == TransactionType.valueOf(ent.getValue())
								|| TransactionType.B2BUR == TransactionType.valueOf(ent.getValue())
								|| TransactionType.CDNUR == TransactionType.valueOf(ent.getValue()))) {

					if (TransactionType.B2B == TransactionType.valueOf(ent.getValue()))
						summDto.setTotalItcClaim(this.getTotalItcAmount(gstinTransactionDTO));

					List<Object[]> itcSummaries = crudService.findWithNamedQuery(
							this.getItcSummaryQueryByType(ent.getValue()),
							QueryParameter.with("taxPayerGstin", taxPayerGstin)
									.and("returnType", gstinTransactionDTO.getReturnType())
									.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
					for (Object[] summary : itcSummaries) {
						String elg = !Objects.isNull(summary[0]) ? (String) summary[0] : null;
						Double valueItc = !Objects.isNull(summary[1]) ? (Double) summary[1] : 0.0;
						Double valueTax = !Objects.isNull(summary[2]) ? (Double) summary[2] : 0.0;

						if ("no".equalsIgnoreCase(elg)) {
							summDto.setNotClaimedItcAmt(valueTax);
						} else if (Objects.isNull(elg)) {
							summDto.setPendingItcAmt(valueTax);
						} else {
							summDto.setClaimedItcAmt(summDto.getClaimedItcAmt() + valueItc);
						}

					}

				}
				List<Object[]> summaries = crudService.findWithNamedQuery(queryName,
						QueryParameter.with("taxPayerGstin", taxPayerGstin)
								.and("returnType", gstinTransactionDTO.getReturnType())
								.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());

				for (Object[] summary : summaries) {
					// log.debug("field {} and value {}", summary[0],
					// summary[1]);
					String type = !Objects.isNull(summary[0]) ? (String) summary[0] : "";
					long value = !Objects.isNull(summary[1]) ? (Long) summary[1] : 0;
					type = StringUtils.upperCase(type);
					if (ReconsileType.MISMATCH_DATE.toString().equalsIgnoreCase(type)) {
						summDto.setMismatchInvoiceDate(value);
					}
					if (ReconsileType.MISMATCH_INVOICENO.toString().equalsIgnoreCase(type)) {
						summDto.setMismatchInvoiceNo(value);
					}
					if (ReconsileType.MISMATCH_MAJOR.toString().equalsIgnoreCase(type)) {
						summDto.setMismatchMajor(value);
					}

					if (ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT.toString().equalsIgnoreCase(type)) {
						summDto.setMismatchRoundoff(value);
					}
					if (ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(type)) {
						summDto.setMissing_inward(value);
						;
					}
					if (ReconsileType.MISSING_OUTWARD.toString().equalsIgnoreCase(type)) {
						summDto.setMissing_outward(value);
						;
					}

					if (ReconsileType.ERROR.toString().equalsIgnoreCase(type)) {
						summDto.setErrorCount(value);
						;
					}

				}

				if ("gstr1".equalsIgnoreCase(gstinTransactionDTO.getReturnType())) {
					dbSumm.setGstr1(summDto);
				} else if ("gstr2".equalsIgnoreCase(gstinTransactionDTO.getReturnType())) {
					dbSumm.setGstr2(summDto);
				}

			}

			int inpMonth = 0;
			int inpYear = 0;

			inpMonth = Integer.parseInt(gstinTransactionDTO.getMonthYear().substring(0, 2));
			inpYear = Integer.parseInt(gstinTransactionDTO.getMonthYear().substring(2, 6));

			String my = Utility.getMonth(inpMonth - 1);
			dbSumm.setCode(gstinTransactionDTO.getMonthYear());
			dbSumm.setMonthName(my + " " + inpYear);
			dbSumm.setBreadcrumb(Utility.getShortMonth(inpMonth - 1) + "' " + inpYear);
			dbSumm.setReturnSummary(returnSummaryDTOs);
			_Logger.info("getDashboardSummary method ends ");

			return dbSumm;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private double getTotalItcAmount(GstinTransactionDTO gstinTransactionDto) {
		List<TransactionType> itcTransactions = new ArrayList<>();
		itcTransactions.add(TransactionType.B2B);
		itcTransactions.add(TransactionType.CDN);
		itcTransactions.add(TransactionType.IMPG);
		itcTransactions.add(TransactionType.IMPS);
		itcTransactions.add(TransactionType.B2BUR);
		itcTransactions.add(TransactionType.CDNUR);
		double totalItc = 0.0;
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		for (TransactionType tran : itcTransactions) {
			List<Object[]> itcSummaries = crudService.findWithNamedQuery(this.getItcSummaryQueryByType(tran.toString()),
					QueryParameter.with("taxPayerGstin", gstin).and("returnType", gstinTransactionDto.getReturnType())
							.and("monthYear", gstinTransactionDto.getMonthYear()).parameters());
			for (Object[] summary : itcSummaries) {
				String elg = !Objects.isNull(summary[0]) ? (String) summary[0] : null;
				Double value = !Objects.isNull(summary[1]) ? (Double) summary[1] : 0.0;
				if ("ip".equalsIgnoreCase(elg) || "cp".equalsIgnoreCase(elg) || "is".equalsIgnoreCase(elg))
					totalItc += value;
			}
		}
		return totalItc;

	}

	private String getQueryByType(String transactionType) {
		String orderByField = "";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2bDetailEntity.findTinDashboardSummary";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "CdnDetailEntity.findTinDashboardSummary";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "CdnUrDetailEntity.findTinDashboardSummary";
		} else if (TransactionType.B2CL.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2clDetailEntity.findTinDashboardSummary";
		} else if (TransactionType.B2CS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2csTransaction.findTinDashboardSummary";
		} else if (TransactionType.EXP.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "ExpDetailEntity.findTinDashboardSummary";
		} else if (TransactionType.AT.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "AtTransaction.findTinDashboardSummary";
		} else if (TransactionType.TXPD.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "TxpdTransaction.findTinDashboardSummary";
		} else if (TransactionType.HSNSUM.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "IMPGTransactionEntity.findTinDashboardSummary";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "IMPSTransactionEntity.findTinDashboardSummary";
		} else if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2bUrTransactionEntity.findTinDashboardSummary";
		} else if (TransactionType.NIL.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "NilTransaction.findTinDashboardSummary";
		} else if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "DocIssue.findTinDashboardSummary";
		}

		return orderByField;
	}

	private String getItcSummaryQueryByType(String transactionType) {
		String orderByField = "";
		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2bDetailEntity.findItcSummary";
		} else if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "CdnDetailEntity.findItcSummary";
		} else if (TransactionType.CDNUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "CdnUrDetailEntity.findItcSummary";
		} else if (TransactionType.IMPG.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "IMPGTransactionEntity.findItcSummary";
		} else if (TransactionType.IMPS.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "IMPSTransactionEntity.findItcSummary";
		} else if (TransactionType.B2BUR.toString().equalsIgnoreCase(transactionType)) {
			orderByField = "B2bUrTransactionEntity.findItcSummary";
		}

		return orderByField;
	}

	@Override
	public ItcAmountDto getItcSummary(GstinTransactionDTO gstinTransactionDTO, String type) {
		/*
		 * TransactionType ttype=TransactionType.valueOf(StringUtils.upperCase(type));
		 */
		ItcAmountDto itc = new ItcAmountDto();
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		if (ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType())
				&& (TransactionType.B2B == TransactionType.valueOf(type)
						|| TransactionType.CDN == TransactionType.valueOf(type)
						|| TransactionType.IMPG == TransactionType.valueOf(type)
						|| TransactionType.IMPS == TransactionType.valueOf(type)
						|| TransactionType.B2BUR == TransactionType.valueOf(type))
				|| TransactionType.CDNUR == TransactionType.valueOf(type)) {

			List<Object[]> itcSummaries = crudService.findWithNamedQuery(this.getItcSummaryQueryByType(type),
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
			for (Object[] summary : itcSummaries) {
				String elg = !Objects.isNull(summary[0]) ? (String) summary[0] : null;
				Double valueItc = !Objects.isNull(summary[1]) ? (Double) summary[1] : 0.0;
				Double valueTax = !Objects.isNull(summary[2]) ? (Double) summary[2] : 0.0;

				if ("no".equalsIgnoreCase(elg)) {
					itc.setNotClaimedItcAmt(valueTax);
				} else if (Objects.isNull(elg)) {
					itc.setPendingItcAmt(valueTax);
				} else {
					itc.setClaimedItcAmt(itc.getClaimedItcAmt() + valueItc);
				}

			}

		}
		return itc;
	}

	@Override
	public ItcAmountDto getItcSummaryForAll(GstinTransactionDTO gstinTransactionDTO) {
		/*
		 * TransactionType ttype=TransactionType.valueOf(StringUtils.upperCase(type));
		 */
		// String type="ALL";
		List<TransactionType> itcTransactions = new ArrayList<>();
		itcTransactions.add(TransactionType.B2B);
		itcTransactions.add(TransactionType.CDN);
		itcTransactions.add(TransactionType.IMPG);
		itcTransactions.add(TransactionType.IMPS);
		itcTransactions.add(TransactionType.B2BUR);
		itcTransactions.add(TransactionType.CDNUR);
		// double totalItc = 0.0;
		double totalPending = 0.0;
		double totalClaimed = 0.0;
		double totalNotClaimed = 0.0;
		List<PendingItcAmt> pendingItcAmounts = new ArrayList<>();
		ItcAmountDto itc = new ItcAmountDto();
		itc.setPendingItcByTransactions(pendingItcAmounts);
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		for (TransactionType tran : itcTransactions) {
			PendingItcAmt pIamt = new PendingItcAmt();
			pIamt.setTransaction(tran.toString());
			pendingItcAmounts.add(pIamt);
			List<Object[]> itcSummaries = crudService.findWithNamedQuery(this.getItcSummaryQueryByType(tran.toString()),
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
			for (Object[] summary : itcSummaries) {
				String elg = !Objects.isNull(summary[0]) ? (String) summary[0] : null;
				Double valueItc = !Objects.isNull(summary[1]) ? (Double) summary[1] : 0.0;
				Double valueTax = !Objects.isNull(summary[2]) ? (Double) summary[2] : 0.0;

				if ("no".equalsIgnoreCase(elg)) {
					totalNotClaimed += valueTax;
				} else if (Objects.isNull(elg)) {
					totalPending += valueTax;
					pIamt.setPendingItcAmt(valueTax);
				} else {
					totalClaimed += valueItc;
				}

			}

		}
		itc.setTotalItcClaim(totalClaimed);
		itc.setNotClaimedItcAmt(totalNotClaimed);
		itc.setPendingItcAmt(totalPending);
		return itc;
	}

	private MismatchCountDto getMisMatchCount(GstinTransactionDTO gstinTransactionDTO, String tranType) {
		MismatchCountDto summDto = new MismatchCountDto();

		String queryName = this.getQueryByType(tranType);
		TaxpayerGstin taxPayerGstin = new TaxpayerGstin();
		taxPayerGstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());

		List<Object[]> summaries = crudService.findWithNamedQuery(queryName,
				QueryParameter.with("taxPayerGstin", taxPayerGstin)
						.and("returnType", gstinTransactionDTO.getReturnType())
						.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());

		for (Object[] summary : summaries) {
			// log.debug("field {} and value {}", summary[0], summary[1]);
			String type = !Objects.isNull(summary[0]) ? (String) summary[0] : "";
			long value = !Objects.isNull(summary[1]) ? (Long) summary[1] : 0;
			type = StringUtils.upperCase(type);
			if (ReconsileType.MISMATCH_DATE.toString().equalsIgnoreCase(type)) {
				summDto.setMismatchInvoiceDate(value);
			}
			if (ReconsileType.MISMATCH_INVOICENO.toString().equalsIgnoreCase(type)) {
				summDto.setMismatchInvoiceNo(value);
			}
			if (ReconsileType.MISMATCH_MAJOR.toString().equalsIgnoreCase(type)) {
				summDto.setMismatchMajor(value);
			}

			if (ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT.toString().equalsIgnoreCase(type)) {
				summDto.setMismatchRoundoff(value);
			}
			if (ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(type)) {
				summDto.setMissing_inward(value);
				/*
				 * summDto.setSimilar_count(value); summDto.setNo_similar_count(value);
				 */
			}
			if (ReconsileType.MISSING_OUTWARD.toString().equalsIgnoreCase(type)) {
				summDto.setMissing_outward(value);

			}

		}

		// B2bDetailEntity.countSimilarData
		List<Object[]> simmNonSimmSummaries = null;
		if (TransactionType.B2B.toString().equalsIgnoreCase(tranType)) {
			simmNonSimmSummaries = crudService.findWithNamedQuery("B2bDetailEntity.countSimmNoSimSummary",
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
		}
		if (TransactionType.CDN.toString().equalsIgnoreCase(tranType)) {
			simmNonSimmSummaries = crudService.findWithNamedQuery("CdnDetailEntity.countSimmNoSimSummary",
					QueryParameter.with("taxPayerGstin", taxPayerGstin)
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
		}

		for (Object[] summary : simmNonSimmSummaries) {
			// log.debug("field {} and value {}", summary[0], summary[1]);
			String type = !Objects.isNull(summary[0]) ? (String) summary[0] : "";
			long value = !Objects.isNull(summary[1]) ? (Long) summary[1] : 0;
			type = StringUtils.upperCase(type);
			if (ReconsileType.MISSING_INWARD_SIMMILAR.toString().equalsIgnoreCase(type)) {
				summDto.setSimilar_count(value);
			}
			if (ReconsileType.MISSING_INWARD_NON_SIMMILAR.toString().equalsIgnoreCase(type)) {
				summDto.setNo_similar_count(value);
			}

		}

		/*
		 * if(TransactionType.B2B.toString().equalsIgnoreCase(tranType)) {
		 * summDto.setNo_similar_count(Long.parseLong(crudService.findWithNamedQuery(
		 * "B2bDetailEntity.countNoSimilarData", QueryParameter.with("taxPayerGstin",
		 * taxPayerGstin) .and("returnType", gstinTransactionDTO.getReturnType())
		 * .and("monthYear",
		 * gstinTransactionDTO.getMonthYear()).parameters()).get(0).toString()));
		 * summDto.setSimilar_count(Long.parseLong(crudService.findWithNamedQuery(
		 * "B2bDetailEntity.countSimilarData", QueryParameter.with("taxPayerGstin",
		 * taxPayerGstin) .and("returnType", gstinTransactionDTO.getReturnType())
		 * .and("monthYear",
		 * gstinTransactionDTO.getMonthYear()).parameters()).get(0).toString()));
		 * //summDto.setSimilar_count(summDto.getMissing_inward()-summDto.
		 * getNo_similar_count()); }
		 * if(TransactionType.CDN.toString().equalsIgnoreCase(tranType)) {
		 * summDto.setNo_similar_count(Long.parseLong(crudService.findWithNamedQuery(
		 * "CdnDetailEntity.countNoSimilarData", QueryParameter.with("taxPayerGstin",
		 * taxPayerGstin) .and("returnType", gstinTransactionDTO.getReturnType())
		 * .and("monthYear",
		 * gstinTransactionDTO.getMonthYear()).parameters()).get(0).toString()));
		 * summDto.setSimilar_count(Long.parseLong(crudService.findWithNamedQuery(
		 * "CdnDetailEntity.countSimilarData", QueryParameter.with("taxPayerGstin",
		 * taxPayerGstin) .and("returnType", gstinTransactionDTO.getReturnType())
		 * .and("monthYear",
		 * gstinTransactionDTO.getMonthYear()).parameters()).get(0).toString())); //
		 * summDto.setSimilar_count(summDto.getMissing_inward()-summDto.
		 * getNo_similar_count()); }
		 */

		return summDto;
	}

	public boolean copyDataService(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {
			List<B2BDetailEntity> b2bs = crudService.findWithNamedQuery("B2bDetailEntity.findTinDashboardSummary",
					QueryParameter.with("taxpayerGstin", gstinTransactionDTO.getTaxpayerGstin())
							.and("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear()).parameters());
			return false;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	/*
	 * @Override public int getDataFromErp(GstinTransactionDTO gstinTransactionDTO)
	 * throws AppException {
	 * 
	 * try { if (!Objects.isNull(gstinTransactionDTO)) {
	 * 
	 * TaxpayerGstin taxpayerGstin =
	 * finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().
	 * getGstin());
	 * 
	 * List<ErpConfig> erpConfigs =
	 * finderService.findErpConfigByTaxpayer(taxpayerGstin.getTaxpayer().getId() );
	 * int size = 0;
	 * 
	 * if (!Objects.isNull(erpConfigs) && !erpConfigs.isEmpty()) {
	 * 
	 * ErpConfig erpConfig = erpConfigs.get(0);
	 * 
	 * TransactionFactory transactionFactory = new TransactionFactory(
	 * gstinTransactionDTO.getTransactionType()); String transactions = "[]";
	 * 
	 * if (!StringUtils.isEmpty(erpConfig.getProvider()) &&
	 * erpConfig.getProvider().equalsIgnoreCase("GINESYS")) {
	 * 
	 * Object object = gccService.getDataFromGcc(gstinTransactionDTO); JsonNode
	 * objects = JsonMapper.objectMapper.readTree(object.toString()); size =
	 * objects.size();
	 * 
	 * transactions = (String)
	 * transactionFactory.readDataFromGcc(object.toString());
	 * 
	 * } else if (!StringUtils.isEmpty(erpConfig.getProvider()) &&
	 * erpConfig.getProvider().equalsIgnoreCase("FTP")) {
	 * 
	 * Object config = erpConfig.getConnection(); JsonNode objects =
	 * JsonMapper.objectMapper.readTree(config.toString()); log.debug(
	 * "ftp server details {}", objects); JsonNode hostnameNode =
	 * objects.get("hostname"); String hostname = hostnameNode.asText(); JsonNode
	 * usernameNode = objects.get("username"); String username =
	 * usernameNode.asText(); JsonNode passwordNode = objects.get("password");
	 * String password = passwordNode.asText();
	 * 
	 * String tempFileName = gstinTransactionDTO.getTaxpayerGstin().getGstin() + "-"
	 * + gstinTransactionDTO.getMonthYear() + "-" +
	 * gstinTransactionDTO.getReturnType() + "-" +
	 * gstinTransactionDTO.getTransactionType() + ".csv";
	 * 
	 * // To be uncomment once we get the FTP location to work FTPService ftpService
	 * = new FTPService(); File ftpFile = new File(ftpService.readFile(hostname,
	 * username, password, tempFileName));
	 * 
	 * if (ftpFile != null && ftpFile.exists()) {
	 * 
	 * size = transactionFactory.processCsvData(ftpFile,
	 * gstinTransactionDTO.getReturnType(), null,
	 * gstinTransactionDTO.getTaxpayerGstin().getGstin(), null);
	 * 
	 * if (!Objects.isNull(object)) { transactions = object.getString("data"); size
	 * = object.getInt("size"); }
	 * 
	 * }
	 * 
	 * } else { throw new AppException(ExceptionCode._ERP_NOT_SUPPORTED); }
	 * 
	 * if (size > 0) { gstinTransactionDTO.setTransactionObject(transactions);
	 * boolean erpData = updateTransactionErpData(gstinTransactionDTO);
	 * 
	 * String existingData = getExistingGstinTransaction(gstinTransactionDTO); if
	 * (!StringUtils.isEmpty(existingData)) { transactions =
	 * transactionFactory.processTransactionData(transactions, existingData,
	 * TaxpayerAction.MODIFYBYNO); } else { transactions =
	 * transactionFactory.processTransactionData(transactions, "[]", null); }
	 * 
	 * gstinTransactionDTO.setTransactionObject(transactions); //
	 * storeTransactionData(gstinTransactionDTO); }
	 * 
	 * return size; } else { throw new
	 * AppException(ExceptionCode._ERP_NOT_CONFIGURED); }
	 * 
	 * } else { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } } catch
	 * (IOException e) { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } finally { }
	 * 
	 * }
	 */

	@Override
	public boolean deleteItemsByInvoiceId(String invoiceId) {
		int deleteCount = em.createNamedQuery("Item.DeleteByInvoiceId").setParameter("invoiceId", invoiceId)
				.executeUpdate();
		log.debug(" previous {} items of invoice {} are deleted", deleteCount, invoiceId);
		return true;
	}

	@Override
	public EmailDTO getEmailContent(GstinTransactionDTO gstinTransactionDTO, String invoiceId) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

			if (!Objects.isNull(taxpayerGstin)) {

				EmailDTO emailDTO = new EmailDTO();
				String recoType = StringUtils.upperCase(gstinTransactionDTO.getRemark());
				ReconsileType reconsileType = ReconsileType.valueOf(recoType);
				if (recoType.contains(ReconsileType.MISMATCH.toString())) {
					reconsileType = ReconsileType.MISMATCH;
				} else if (recoType.contains(ReconsileType.MISSING.toString())) {
					reconsileType = ReconsileType.MISSING;

				}
				String type = gstinTransactionDTO.getTransactionType();
				String object = gstinTransactionDTO.getTransactionObject();

				String key = type + "_" + reconsileType.toString();
				key = key.toUpperCase();

				Properties emailConfig = new Properties();
				try {
					emailConfig.load(MethodHandles.lookup().lookupClass().getClassLoader()
							.getResourceAsStream("email.properties"));
					log.info("email.properties loaded successfully");
				} catch (IOException e) {
					log.error("IOExcpetion while loading email.properties {}", e.getMessage());
				}

				String subject = emailConfig.getProperty(key + "_SUBJECT");
				String body = emailConfig.getProperty(key + "_BODY");

				emailDTO.setMailsubject(subject);
				emailDTO.setMailbody(body);
				// emailDTO.setRemarks(object);
				Map<String, Object> invoices = this.getMismatchedInvoices(type, invoiceId);

				TransactionFactory factory = new TransactionFactory(type);
				if (ReconsileType.MISMATCH.equals(reconsileType) || ReconsileType.MISMATCH_INVOICENO == reconsileType
						|| ReconsileType.MISMATCH_DATE == reconsileType
						|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
						|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
						|| ReconsileType.MISMATCH_MAJOR == reconsileType) {
					emailDTO = factory.getEmailContent(emailDTO, invoices.get("invoice"), invoices.get("cinvoice"));

				} else {
					TransactionType tType = TransactionType
							.valueOf(StringUtils.upperCase(gstinTransactionDTO.getTransactionType()));
					if (TransactionType.B2B == tType) {
						emailDTO = factory.getEmailContent(emailDTO, crudService.find(B2BDetailEntity.class, invoiceId),
								invoices.get("cinvoice"));
					} else if (TransactionType.CDN == tType) {
						emailDTO = factory.getEmailContent(emailDTO, crudService.find(CDNDetailEntity.class, invoiceId),
								invoices.get("cinvoice"));
					}
				}

				return emailDTO;

			} else {

				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	/*
	 * private boolean saveConnection(Integer taxpayerId, ErpConfigDTO erpConfigDTO)
	 * throws AppException {
	 * 
	 * if (!Objects.isNull(erpConfigDTO) &&
	 * !StringUtils.isEmpty(erpConfigDTO.getConnection())) {
	 * 
	 * ErpConfig erpConfig = new ErpConfig(); erpConfig = (ErpConfig)
	 * EntityHelper.convert(erpConfigDTO, ErpConfig.class);
	 * erpConfig.setCreatedBy(userBean.getUserDto().getFirstName() + "_" +
	 * userBean.getUserDto().getLastName());
	 * erpConfig.setCreationIpAddress(userBean.getIpAddress());
	 * 
	 * crudService.create(erpConfig);
	 * 
	 * return true;
	 * 
	 * } else { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } }
	 */

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@AccessTimeout(value = 15, unit = TimeUnit.MINUTES)
	public Map<String, String> syncData(GstinTransactionDTO gstinTransactionDTO) throws Exception {
		log.info("sync data start for {} , return {}", gstinTransactionDTO.getTaxpayerGstin().getGstin(),
				gstinTransactionDTO.getReturnType());

		gstinTransactionDTO.setMonthYear(Utility.getValidReturnPeriod(gstinTransactionDTO.getMonthYear(),
				gstinTransactionDTO.getTaxpayerGstin().getUserPreference()));

		Map<String, String> resp = new HashMap<>();
		String syncTransactionData = "";
		String actionTransactionData = "";
		if (!Objects.isNull(gstinTransactionDTO)) {
			String returnType = gstinTransactionDTO.getReturnType();

			boolean syncSummaryData = false;

			if (returnType.equalsIgnoreCase(ReturnType.GSTR1.toString())) {
				functionalService.saveGstr1DataToGstn(gstinTransactionDTO);
				GetCheckStatusResp returnStatus = null;
				returnStatus = functionalService.checkReturnStatus(gstinTransactionDTO);
				String respMsg = "";

				if (returnStatus != null) {
					if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG;
						resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						// logging user action
						apiLoggingService.saveAction(UserAction.SAVE_TO_GSTN, new MessageDto(),
								gstinTransactionDTO.getTaxpayerGstin().getGstin());
						// end logging user action

					} else if (GSTNRespStatus.CHECKSTATUS_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						if (returnStatus.getError() != null)
							respMsg = returnStatus.getError().getMessage();
						else
							respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
					} else if (GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_PE_MSG;
						// logging user action
						MessageDto messageDto = new MessageDto();
						messageDto.setNoOfInvoices("some");
						apiLoggingService.saveAction(UserAction.SYNCED_WITH_ERROR_TO_GSTN, messageDto,
								gstinTransactionDTO.getTaxpayerGstin().getGstin());
						// end logging user action
					} else if (GSTNRespStatus.CHECKSTATUS_IN_PROCESS.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_IP_MSG;
					} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
					} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_REC_MSG;
					}
					resp.put(ApplicationMetadata.RESP_MSG_KEY, respMsg);
				} else
					throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.ERR_MSG);
			} else if (returnType.equalsIgnoreCase(ReturnType.GSTR1A.toString())) {
				// syncTransactionData =
				// functionalService.syncGstr1ATransactionData(gstinTransactionDTO);
				functionalService.saveGstr1ADataToGstn(gstinTransactionDTO);
				syncSummaryData = functionalService.syncGstr1ASummaryData(gstinTransactionDTO);
			} else if (returnType.equalsIgnoreCase(ReturnType.GSTR3.toString())) {

			} else if (returnType.equalsIgnoreCase(ReturnType.GSTR2.toString())) {
				functionalService.saveGstr2DataToGstn(gstinTransactionDTO);
				GetCheckStatusGstr2Resp returnStatus = null;
				String respMsg = "";
				returnStatus = functionalService.checkGstr2ReturnStatus(gstinTransactionDTO);

				if (returnStatus != null) {
					if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG;
						resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					} else if (GSTNRespStatus.CHECKSTATUS_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						if (returnStatus.getError() != null)
							respMsg = returnStatus.getError().getMessage();
						else
							respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
					} else if (GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_PE_MSG;
					} else if (GSTNRespStatus.CHECKSTATUS_IN_PROCESS.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_IP_MSG;
					} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
						respMsg = GSTNRespStatus.CHECKSTATUS_REC_MSG;
					}
					resp.put(ApplicationMetadata.RESP_MSG_KEY, respMsg);
				} else {
					throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.ERR_MSG);
				}
			}
			resp.put("transactionData", syncTransactionData);
		}
		return resp;
	}

	@Override
	public boolean getMonthSummary(String gstin) throws AppException {

		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
		if (!Objects.isNull(taxpayerGstin)) {
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();

			// need to change this later from true to false
			boolean syncDataFlag = true;

			String monthSummary = taxpayerGstin.getMonthYear();

			String status = null;
			String returnName = null;

			try {
				MonthSummaryDTO monthSummDTO = null;
				monthSummDTO = JsonMapper.objectMapper.readValue(monthSummary.toString(), MonthSummaryDTO.class);
				if (!Objects.isNull(monthSummDTO)) {
					String monthYear = monthSummDTO.getCode();
					List<ReturnSummaryDTO> returnSummary = monthSummDTO.getReturnSummary();
					for (ReturnSummaryDTO summary : returnSummary) {
						status = summary.getStatus();
						returnName = summary.getReturnName();
						if (!status.equalsIgnoreCase(ReturnCalendar.LOCKED.toString())) {
							gstinTransactionDTO.setMonthYear(monthYear);
							gstinTransactionDTO.setReturnType(returnName);
							gstinTransactionDTO.setStatus(status);
							syncDataFlag = true;/*
												 * syncData(gstinTransactionDTO) ;
												 */
						}
					}
				} else {
					return Boolean.FALSE;
				}
			} catch (JsonProcessingException e) {
				log.error(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}

			return syncDataFlag;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	// get gstr3 transactions data temporay method
	public String getGstr3TransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		if (!Objects.isNull(gstinTransactionDTO)) {

			String type = gstinTransactionDTO.getTransactionType();
			String data = null;
			GetGstr3Resp gstr3 = new GetGstr3Resp();
			gstr3.setRet_period("042016");

			// setting turnover
			List<TOD> tods = new ArrayList<>();
			TOD tod = new TOD();
			tods.add(tod);
			tod.setExportTurnOver(1330000.00);
			tod.setGrossTurnOver(5760000.00);
			tod.setNetTaxableTurnOver(7700000.00);
			tod.setNilAndExemptedTurnOver(8800000.0);
			tod.setNonGstTurnOver(76574.0);
			gstr3.setTurnover(tods);

			// outward supply
			OUTS out = new OUTS();
			gstr3.setOutward_supply(out);

			IntrSupReg intr = new IntrSupReg();
			ItraSupReg intra = new ItraSupReg();
			IntrSupCon intrCon = new IntrSupCon();
			ItraSupCon intraCon = new ItraSupCon();

			List<IntraStateSupplies> issList = new ArrayList<>();
			IntraStateSupplies iss = new IntraStateSupplies();
			issList.add(iss);
			// iss.setIamt(9900000.0);
			iss.setCess(99333000.0);
			iss.setTx_r(200000.0);
			iss.setTxval(8760000.0);
			// iss.setState_cd("05");
			iss.setTy("G");

			List<InterStateSupplies> inssList = new ArrayList<>();
			InterStateSupplies inss = new InterStateSupplies();
			inss.setCess(99333000.0);
			inss.setTx_r(200000.0);
			inss.setTxval(8760000.0);
			inss.setState_cd("22");
			inss.setIamt(324242.0);
			// iss.setState_cd("05");
			inss.setTy("G");

			intr.setIn_det(inssList);
			intra.setIntra_det(issList);
			intrCon.setInterc_det(inssList);
			intraCon.setIntrac_det(issList);

			out.setInterStateSuppToRegisteredTaxPayers(intr);
			out.setIntraStateSuppToRegisteredTaxPayers(intra);
			out.setInterStateSuppToConsumers(intrCon);
			out.setIntraStateSuppToConsumers(intraCon);

			Export exp = new Export();
			out.setExport(exp);
			ExportDetails expDetail = new ExportDetails();

			List<ExportDetails> expDetails = new ArrayList<>();
			expDetails.add(expDetail);
			expDetail.setCamt(19900000.0);
			expDetail.setCess(850000.0);
			expDetail.setIamt(980000.0);
			expDetail.setSamt(980000.0);
			expDetail.setTxval(98799.00);
			exp.setExp_det(expDetails);

			RevisionInvoiceOUTS ri = new RevisionInvoiceOUTS();
			ri.setStateCode("07");
			List<RevInvoiceOUTDetails> rids = new ArrayList<>();
			RevInvoiceOUTDetails rid = new RevInvoiceOUTDetails();
			rids.add(rid);
			ri.setDt(rids);
			out.setRevisionOfInvoices(ri);

			TaxLiabilityOUTS tlo = new TaxLiabilityOUTS();
			out.setTotalTaxLiabilityOnOutSupp(tlo);

			TotalTaxLiabilityDetails ttld = new TotalTaxLiabilityDetails();
			List<TotalTaxLiabilityDetails> ttlds = new ArrayList<>();
			ttlds.add(ttld);
			ttld.setCamt(2324234.0);
			ttld.setIamt(9924234.0);
			ttld.setSamt(2424234.0);
			ttld.setTxval(123132.0);
			;
			ttld.setCess(123131.0);
			;
			ttld.setTy("g");
			;

			tlo.setTottx_det(ttlds);

			// end outward supply

			// setting inward supply
			INS ins = new INS();
			gstr3.setInward_supply(ins);
			Imp imp = new Imp();
			ins.setImports(imp);
			ImportDetails id = new ImportDetails();
			List<ImportDetails> ids = new ArrayList<>();
			ids.add(id);
			imp.setImp_det(ids);
			id.setAval(76000.0);
			id.setCsamt(808000.0);
			id.setIamt(230000.0);
			id.setItc_cs(99999.0);
			id.setTy("s");

			IntrSupRec isr = new IntrSupRec();
			List<IntrSupRec> isrs = new ArrayList<>();
			isrs.add(isr);
			ins.setInterStateSuppReceived(isr);
			InterStateSupplyInward issi = new InterStateSupplyInward();
			List<InterStateSupplyInward> issis = new ArrayList<>();
			issis.add(issi);
			issi.setCess(88888.0);
			issi.setIamt(878700.0);
			issi.setTxval(9999.0);
			isr.setInter(issis);

			// end inward

			// set total tax lia

			TTL ttl = new TTL();
			ttl.setTtltx_det(ttlds);
			gstr3.setTotal_tax_liabilty(ttl);

			// set tcs credit

			TCSCredit tcredit = new TCSCredit();
			List<CreditDetails> cds = new ArrayList<>();
			CreditDetails cdetail = new CreditDetails();
			cds.add(cdetail);
			tcredit.setTcs_det(cds);
			cdetail.setCrt(876.0);
			cdetail.setCsrt(9999.0);
			cdetail.setCstx(879787.0);
			cdetail.setCtx(555.0);
			cdetail.setGstin_ec("01THDIDK123");
			cdetail.setIrt(444.0);
			cdetail.setItx(232432.0);
			cdetail.setSrt(444.0);
			cdetail.setStx(232432.0);
			tcredit.setTcs_det(cds);
			gstr3.setTcs_credit(tcredit);

			// set tds credit

			TDSCredit tdc = new TDSCredit();
			tdc.setTds_det(cds);
			gstr3.setTds_credit(tdc);

			// set itc credit

			ITCCredit itc = new ITCCredit();
			itc.setItc_det(cds);

			gstr3.setItc_credit(itc);

			// set tax paid
			TaxPaidDto txpd = new TaxPaidDto();
			TaxPaidA tpa = new TaxPaidA();
			TaxPaidB tpb = new TaxPaidB();
			txpd.setTaxPaidA(tpa);
			txpd.setTaxPaidB(tpb);

			TxPay tp = new TxPay();
			TaxPaidAByCash tpabc = new TaxPaidAByCash();
			List<TaxPaidAByCash> tpabcs = new ArrayList<>();
			tpabcs.add(tpabc);
			TaxPaidAByItcCredit tpabic = new TaxPaidAByItcCredit();
			List<TaxPaidAByItcCredit> tpabics = new ArrayList<>();
			tpabics.add(tpabic);

			tpa.setTaxPayble(tp);
			tpa.setTxPdByCash(tpabcs);
			tpa.setTxPdByCredit(tpabics);

			TaxPaidBPayble tpbp = new TaxPaidBPayble();

			TaxPaidBByCash tpbbc = new TaxPaidBByCash();
			List<TaxPaidBByCash> tpdbbcs = new ArrayList<>();
			tpdbbcs.add(tpbbc);
			tpb.setPaidByCash(tpdbbcs);
			tpb.setPaybleDetail(tpbp);

			gstr3.setTax_paid(txpd);

			// refund claim

			RFCLM rclm = new RFCLM();
			gstr3.setRefund_claim(rclm);
			rclm.setBankacc(9999999777l);
			rclm.setRet_period("042016");
			rclm.setGstin("09MYPAN123NDID0");
			rclm.setDebitno("6666");
			ClaimData cd = new ClaimData();
			cd.setFees(9990.0);
			cd.setInterest(8787.0);
			cd.setOthers(87654.0);
			cd.setPenality(8888.0);
			cd.setTax(87787.0);
			rclm.setCgrfclm(cd);
			rclm.setCsrfclm(cd);
			rclm.setIgrfclm(cd);
			rclm.setSgrfclm(cd);
			// end

			gstr3.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
			Map<String, String> resp = new HashMap<>();
			Gstr3Transactions gstr3Tran = new Gstr3Transactions();
			if (!StringUtils.isEmpty(type))
				type = type.toUpperCase();
			try {

				switch (type) {

				case "TOD":
					List<TodDto> toDetails = gstr3Tran.getTodData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(toDetails);
					break;
				case "OUTS":
					List<OutsInsSupplyDto> osdtos = gstr3Tran.getOutwardSupplyData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(osdtos);
					break;

				case "INS":
					List<OutsInsSupplyDto> osdtos1 = gstr3Tran.getInwardSupplyData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(osdtos1);

					break;
				case "TTL":
					List<TotalTaxLiabilityDto> ttlDtos = gstr3Tran.getTtlData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(ttlDtos);

					break;

				case "ITCCR":
					List<CreditDetails> itcdtls = gstr3Tran.getItcCreditData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(itcdtls);
					break;

				case "TCSCR":
					List<CreditDetails> tCdtls = gstr3Tran.getTcsCreditData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(tCdtls);
					break;

				case "TDSCR":
					List<CreditDetails> tdsdtls = gstr3Tran.getItcCreditData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(tdsdtls);
					break;

				case "TAXPD":
					data = JsonMapper.objectMapper.writeValueAsString(gstr3.getTax_paid());

					break;

				case "RFCLM":
					RFCLM rfclm = gstr3Tran.getRefundClaimData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(rfclm);

					break;

				case "ALL":
					Gstr3Dto gstrDto = gstr3Tran.getAllData(gstr3);
					data = JsonMapper.objectMapper.writeValueAsString(gstrDto);

					break;

				default:
					return null;

				}
				resp.put("data", data);
				resp.put("summary", this.getGstr3Summary(gstinTransactionDTO, gstr3));

				return JsonMapper.objectMapper.writeValueAsString(resp);

			} catch (JsonProcessingException ex) {
				ex.printStackTrace();
			}

			return null;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private String getGstr3Summary(GstinTransactionDTO gstinTransactionDTO, GetGstr3Resp gstr3) throws AppException {
		if (!Objects.isNull(gstinTransactionDTO)) {

			Gstr3Summary summ = new Gstr3Summary();
			Gstr3TransactionSummary tSumm = new Gstr3TransactionSummary();
			TOD todSumm = new TOD();
			todSumm.setExportTurnOver(1500000.00);
			todSumm.setGrossTurnOver(5560000.00);
			todSumm.setNetTaxableTurnOver(9900000.00);
			todSumm.setNilAndExemptedTurnOver(50000000.0);
			summ.setTod(todSumm);

			tSumm.setIgstAmt(1200000.00);
			tSumm.setSgstAmt(1800000.00);
			tSumm.setCgstAmt(1900000.00);
			summ.setInwardSupp(tSumm);
			summ.setTaxPaid(tSumm);
			summ.setRefundClaim(tSumm);
			summ.setTcsCredit(tSumm);
			summ.setOutwardSupp(tSumm);

			try {
				return JsonMapper.objectMapper.writeValueAsString(summ);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
		return null;
	}

	private <T> List<T> findNativeQueryPaginatedResults(String nativeQuery, Map<String, Object> parameters, int pageNo,
			int size, TransactionType type) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		String paginatedQuery = nativeQuery + " Limit " + ((pageNo - 1) * size) + ", " + size;
		Query query = this.em.createNativeQuery(paginatedQuery,
				finderService.getEntityTypeByTransaction(type.toString()));
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		return query.getResultList();
		/*
		 * return query .setFirstResult((pageNo-1)*size) .setMaxResults(size)
		 * .getResultList();
		 */
	}

	@Override
	public Map<String, Object> searchText(String searchStr, GstinTransactionDTO gstinTransactionDTO, FilterDto filter)
			throws AppException {

		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		// String query="SELECT * FROM b2b_transaction bt inner join b2b_details
		// bd on bt.id=bd.b2bTransactionId where
		// bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and
		// bt.monthYear=:monthYear and bd.flags<>'DELETE'";
		String query = this.getNativeQuery(gstinTransactionDTO.getReturnType(),
				gstinTransactionDTO.getTransactionType(), searchStr, QueryType.DATA);

		_Logger.debug("search rsult {}", query);
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("taxPayerGstin", gstin.getId());
		parameters.put("returnType", gstinTransactionDTO.getReturnType());
		parameters.put("monthYear", gstinTransactionDTO.getMonthYear());
		List results = this.findNativeQueryPaginatedResults(query, parameters, filter.getPageNo(), filter.getSize(),
				TransactionType.valueOf(gstinTransactionDTO.getTransactionType()));
		_Logger.debug("search rsult {} {}", query, results);
		long count = 0;
		Map<String, Object> temp = new HashMap<>();
		if (!Objects.isNull(results))
			temp.put("transactionData", results);
		else
			temp.put("transactionData", "[]");
		temp.put("total_count", this.findResultCounts(this.getNativeQuery(gstinTransactionDTO.getReturnType(),
				gstinTransactionDTO.getTransactionType(), searchStr, QueryType.COUNT), parameters));
		return temp;

	}

	private long findResultCounts(String queryName, Map<String, Object> parameters) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNativeQuery(queryName);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		try {
			BigInteger b = (BigInteger) query.getSingleResult();
			if (Objects.isNull(b))
				return 0;
			return b.longValue();
		} catch (NoResultException e) {
			return 0;
		}

	}

	public enum QueryType {
		DATA("DATA"), COUNT("TYPE");

		QueryType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type;
		};

		String type;
	}
	
	public enum SummaryType {
		OutputTaxandITC("OutputTaxandITC"),OFFSETLIABILTY("Offsetliabilty");

		SummaryType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type;
		};

		String type;
	}
	

	private String getNativeQuery(String returnType, String transactionType, String key, QueryType queryType) {
		TransactionType tranType = TransactionType.valueOf(StringUtils.upperCase(transactionType));
		String searchKey = "'%" + key + "%'";
		SimpleDateFormat sdfView = new SimpleDateFormat("dd MMM yyyy");
		SimpleDateFormat sdfDb = new SimpleDateFormat("yyyy-MM-dd");
		;

		try {
			Date date = sdfView.parse(key);
			searchKey = "'%" + sdfDb.format(date) + "%'";
		} catch (ParseException e) {
			if (!StringUtils.isEmpty(key)) {
				searchKey = "'%" + key.replace(" ", "%") + "%'";
				_Logger.info("key to be searched {}", searchKey);
			}
			_Logger.info("search string is not date");
		}

		String query = "";
		String queryCount = "";
		String searchableFields = "";
		List<String> selectedFields = this.getSelectedFieldsToQuery("bd", returnType, transactionType);
		String fields = StringUtils.join(selectedFields, ",");
		switch (tranType) {
		case B2B:
			query = "SELECT *  FROM b2b_details bd inner join b2b_transaction bt  on bt.id=bd.b2bTransactionId  where bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE' and (bd.dataSource='EMGST' or (bd.dataSource='GSTN' and bd.type<>'NEW')) and  ";
			queryCount = "SELECT  count(bd.id)  FROM b2b_transaction bt inner join b2b_details bd on bt.id=bd.b2bTransactionId  where bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE'  and (bd.dataSource='EMGST' or (bd.dataSource='GSTN' and bd.type<>'NEW')) and  ";
			String reverseChargeKey = Objects.nonNull(key) && "Y".equalsIgnoreCase(key) ? "1"
					: Objects.nonNull(key) && "N".equalsIgnoreCase(key) ? "0" : searchKey;
			String revChargOpr = ("Y".equalsIgnoreCase(key) || "N".equalsIgnoreCase(key)) ? " = " : " like ";
			searchableFields = "(bt.ctin like " + searchKey + " or bt.ctinName like " + searchKey
					+ " or bd.invoiceNumber like " + searchKey + " or  bd.reverseCharge  " + revChargOpr
					+ reverseChargeKey + " or bd.invoiceDate like " + searchKey + " or bd.taxableValue like "
					+ searchKey + " or bd.taxAmount like " + searchKey + " or bd.flags like " + searchKey
					+ " or bd.type like " + searchKey + " or bd.source like " + searchKey + " or  bd.etin like "
					+ searchKey + ")";
			break;
		case CDN:
			query = "SELECT * FROM cdn_details cd inner join cdn_transaction ct on ct.id=cd.cdnTransactionId  where ct.gstin=:taxPayerGstin and ct.returnType=:returnType and ct.monthYear=:monthYear and cd.flags<>'DELETE' and (cd.dataSource='EMGST' or (cd.dataSource='GSTN' and cd.type<>'NEW')) and   ";
			queryCount = "SELECT count(cd.id) FROM cdn_transaction ct inner join cdn_details cd on ct.id=cd.cdnTransactionId  where ct.gstin=:taxPayerGstin and ct.returnType=:returnType and ct.monthYear=:monthYear and cd.flags<>'DELETE' and (cd.dataSource='EMGST' or (cd.dataSource='GSTN' and cd.type<>'NEW')) and   ";

			searchableFields = " (ct.originalCtin like " + searchKey + " or ct.originalCtinName like " + searchKey
					+ " or cd.invoiceNumber like " + searchKey + " or cd.invoiceDate like " + searchKey
					+ " or cd.taxableValue like " + searchKey + " or cd.taxAmount like " + searchKey
					+ " or cd.flags like " + searchKey + " or cd.type like " + searchKey + " or cd.noteType like "
					+ searchKey + " or cd.revisedInvNo like " + searchKey + " or cd.revisedInvDate like " + searchKey
					+ " or cd.source like " + searchKey + ")";

			break;
		case CDNUR:
			query = "SELECT * FROM cdnur_transaction ct inner join cdnur_details cd on ct.id=cd.cdnUrTransactionId  where ct.gstin=:taxPayerGstin and ct.returnType=:returnType and ct.monthYear=:monthYear and cd.flags<>'DELETE' and ";
			queryCount = "SELECT count(cd.id) FROM cdnur_transaction ct inner join cdnur_details cd on ct.id=cd.cdnUrTransactionId  where ct.gstin=:taxPayerGstin and ct.returnType=:returnType and ct.monthYear=:monthYear and cd.flags<>'DELETE' and ";

			searchableFields = " (ct.originalCtin like " + searchKey + " or ct.originalCtinName like " + searchKey
					+ " or cd.invoiceNumber like " + searchKey + " or cd.invoiceDate like " + searchKey
					+ " or cd.taxableValue like " + searchKey + " or cd.taxAmount like " + searchKey
					+ " or cd.flags like " + searchKey + " or cd.type like " + searchKey + " or cd.noteType like "
					+ searchKey + " or cd.revisedInvNo like " + searchKey + " or cd.revisedInvDate like " + searchKey
					+ " or cd.source like " + searchKey + ")";

			break;
		case B2CL:

			query = "SELECT bt.returnType,bd.*  FROM b2cl_transaction bt inner join b2cl_details bd on bt.id=bd.b2clTransactionId  where bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE' and ";
			queryCount = "SELECT count(bd.id)  FROM b2cl_transaction bt inner join b2cl_details bd on bt.id=bd.b2clTransactionId  where bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE' and ";

			searchableFields = "  (bd.invoiceNumber like " + searchKey + " or bd.invoiceDate like " + searchKey
					+ " or bd.taxableValue like " + searchKey + " or bd.taxAmount like " + searchKey
					+ " or bd.flags like " + searchKey + " or bd.type like " + searchKey + " or bd.source like "
					+ searchKey + " or  bd.etin like " + searchKey + " or  bd.pos like " + searchKey
					+ " or  bd.stateName like " + searchKey + " )";

			break;
		case EXP:
			query = "SELECT bt.returnType,bd.* FROM exp_transaction bt inner join exp_details bd on bt.id=bd.expTransactionId  where bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE' and ";
			queryCount = "SELECT count(bt.id) FROM exp_transaction bt inner join exp_details bd on bt.id=bd.expTransactionId  where bt.taxpayerGstinId=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE' and ";

			searchableFields = "  (bd.invoiceNumber like " + searchKey + " or bd.invoiceDate like " + searchKey
					+ " or bd.taxableValue like " + searchKey + " or bd.taxAmount like " + searchKey
					+ " or bd.flags like " + searchKey + " or bd.type like " + searchKey + " or bd.source like "
					+ searchKey + " or  bd.exportType like " + searchKey + " or  bd.shippingBillPortCode like "
					+ searchKey + " or  bd.shippingBillNo like " + searchKey + " or  bd.shippingBillDate like "
					+ searchKey + " )";
			break;
		case B2BUR:
			query = "SELECT *  FROM b2bur_transaction bt  where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and  ";
			queryCount = "SELECT count(bt.id)  FROM b2bur_transaction bt  where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and  ";

			searchableFields = " ( bt.invoiceNumber like " + searchKey + " or bt.invoiceDate like " + searchKey
					+ " or bt.taxableValue like " + searchKey + " or bt.taxAmount like " + searchKey
					+ " or bt.flags like " + searchKey + " or bt.type like " + searchKey + " or bt.source like "
					+ searchKey + " or  bt.supplyType like " + searchKey + " or  bt.pos like " + searchKey
					+ " or  bt.stateName like " + searchKey + ")";

			break;

		case B2CS:
			query = "SELECT  * FROM b2cs_transaction bt  where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";
			queryCount = "SELECT  count(bt.id) FROM b2cs_transaction bt  where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";

			searchableFields = " ( bt.invoiceNumber like " + searchKey + " or bt.taxableValue like " + searchKey
					+ " or bt.taxAmount like " + searchKey + " or bt.flags like " + searchKey + " or bt.type like "
					+ searchKey + " or bt.source like " + searchKey + " or  bt.etin like " + searchKey
					+ " or  bt.pos like " + searchKey + " or  bt.stateName like " + searchKey
					+ " or  bt.supplyType like " + searchKey + " or  bt.b2csType like " + searchKey + " )";

			break;

		case AT:
			query = "SELECT *  FROM  at_transaction  bt  where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";
			queryCount = "SELECT  count(bt.id)  FROM  at_transaction  bt  where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";

			searchableFields = " (bt.advanceReceived like " + searchKey + " or bt.taxAmount like " + searchKey
					+ " or bt.flags like " + searchKey + " or bt.type like " + searchKey + " or bt.source like "
					+ searchKey + " or  bt.pos like " + searchKey + " or  bt.stateName like " + searchKey
					+ " or  bt.supplyType like " + searchKey + ")"

			;

			break;

		case TXPD:
			query = "SELECT *  FROM txpd_transaction bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";
			queryCount = "SELECT count(bt.id) FROM txpd_transaction bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";

			searchableFields = " (bt.advanceAdjusted like " + searchKey + " or bt.taxAmount like " + searchKey
					+ " or bt.flags like " + searchKey + " or bt.type like " + searchKey + " or bt.source like "
					+ searchKey + " or  bt.pos like " + searchKey + " or  bt.stateName like " + searchKey
					+ " or  bt.supplyType like " + searchKey + ")"

			;

			break;

		case IMPG:
			query = "SELECT * FROM impg_transaction bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";
			queryCount = "SELECT count(bt.id) FROM impg_transaction bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";
			searchableFields = " (bt.taxableValue like " + searchKey + " or bt.taxAmount like " + searchKey
					+ " or bt.flags like " + searchKey + " or bt.type like " + searchKey + " or bt.source like "
					+ searchKey + " or  bt.ctin like " + searchKey + " or  bt.billOfEntryNumber like " + searchKey
					+ " or  bt.billOfEntryDate like " + searchKey + " or  bt.portCode like " + searchKey + ")";

			break;
		case IMPS:
			query = "SELECT * FROM imps_transaction bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";
			queryCount = "SELECT count( bt.id) FROM imps_transaction bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bt.flags<>'DELETE' and ";

			searchableFields = " (bt.taxableValue like " + searchKey + " or bt.taxAmount like " + searchKey
					+ " or bt.flags like " + searchKey + " or bt.type like " + searchKey + " or bt.source like "
					+ searchKey + " or  bt.invoiceNumber like " + searchKey + " or bt.invoiceDate like " + searchKey
					+ " or  bt.pos like " + searchKey + " or  bt.stateName like " + searchKey + ")";

			break;
		case DOC_ISSUE:
			query = "SELECT * FROM doc_issue bt   where bt.gstin=:taxPayerGstin and bt.returnType=:returnType and bt.monthYear=:monthYear and bd.flags<>'DELETE' and ";

			searchableFields = " (bt.category like " + searchKey + " or bt.snoFrom like " + searchKey
					+ " or bt.flags like " + searchKey + " or bt.type like " + searchKey + " or bt.source like "
					+ searchKey + " or  bt.invoiceNumber like " + searchKey + " or bt.invoiceDate like " + searchKey
					+ " or  bt.pos like " + searchKey + " or  bt.stateName like " + searchKey + ")";
			break;

		default:
			log.debug("invalid transaction type");
		}
		if (QueryType.DATA == queryType)
			return query + searchableFields;
		else
			return queryCount + searchableFields;
	}

	private List<String> getSelectedFieldsToQuery(String alias, String returnType, String transactionType) {
		Properties props = new Properties();
		InputStream inputStream = null;
		String fileName = "/transactionfields.properties";
		List<String> selectedFields = new ArrayList<>();
		try {
			inputStream = DBUtils.class.getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IOException();
			props.load(inputStream);
			String fields = props.getProperty(StringUtils.lowerCase(returnType + "." + transactionType));
			if ("all".equalsIgnoreCase(fields)) {
				selectedFields.add("*");
			} else {
				StringTokenizer st = new StringTokenizer(fields, ",");

				while (st.hasMoreTokens()) {
					String s = alias + "." + StringUtils.trim(st.nextToken());
					selectedFields.add(s);
				}
			}
		}

		catch (IOException e) {
			log.error("file not found with this name--{}", fileName);
		}

		if (TransactionType.B2B.toString().equalsIgnoreCase(transactionType))
			selectedFields.remove(alias + "." + "b2bTransaction");
		if (TransactionType.CDN.toString().equalsIgnoreCase(transactionType))
			selectedFields.remove(alias + "." + "cdnTransaction");
		if (TransactionType.EXP.toString().equalsIgnoreCase(transactionType))
			selectedFields.remove(alias + "." + "expTransaction");

		return selectedFields;

	}

	private List<String> getFieldsToDisplay(String alias, String returnType, String transactionType) {
		Properties props = new Properties();
		InputStream inputStream = null;
		String fileName = "/transactionfields.properties";
		List<String> selectedFields = new ArrayList<>();
		try {
			inputStream = DBUtils.class.getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IOException();
			props.load(inputStream);
			String fields = props.getProperty(StringUtils.lowerCase(returnType + "." + transactionType));
			if ("all".equalsIgnoreCase(fields)) {
				selectedFields.add("*");
			} else {
				StringTokenizer st = new StringTokenizer(fields, ",");

				while (st.hasMoreTokens()) {
					String s = alias + "." + StringUtils.trim(st.nextToken());
					selectedFields.add(s);
				}
			}
		}

		catch (IOException e) {
			log.error("file not found with this name--{}", fileName);
		}

		return selectedFields;

	}

	public List b2bTextSearch(String searchStr) {
		/*
		 * FullTextEntityManager fullTextEntityManager=
		 * Search.getFullTextEntityManager(em); //
		 * fullTextEntityManager.createIndexer().startAndWait();
		 * org.hibernate.search.query.dsl.QueryBuilder
		 * queryBuilder=fullTextEntityManager.getSearchFactory(). buildQueryBuilder()
		 * .forEntity(B2BDetailEntity.class).get();
		 * 
		 * org.apache.lucene.search.Query query=
		 * queryBuilder.keyword().onFields("invoiceNumber").
		 * matching(searchStr).createQuery();
		 * 
		 * Query query2 =
		 * fullTextEntityManager.createFullTextQuery(query,B2BDetailEntity.class );
		 * 
		 * return query2.getResultList();
		 */
		return null;
	}

	@Override
	public boolean compareTransactions(GstinTransactionDTO gstinTransactionDto) {
		TransactionType tran = TransactionType.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		
		
		if (TransactionType.B2B == tran && "gstr2".equalsIgnoreCase(gstinTransactionDto.getReturnType())) {
			this.compareB2B(gstinTransactionDto);
		} else if (TransactionType.CDN == tran && "gstr2".equalsIgnoreCase(gstinTransactionDto.getReturnType())) {
			this.compareCDN(gstinTransactionDto);

		}
		return true;
	}

	private void compareB2B(GstinTransactionDTO gstinTransactionDto) {
		try {
			_Logger.debug("starrt compareB2b method for gstin {}, returnType {}, monthYear {}",
					gstinTransactionDto.getTaxpayerGstin().getGstin(), gstinTransactionDto.getReturnType(),
					gstinTransactionDto.getMonthYear());

			TaxpayerGstin gstin = new TaxpayerGstin();
			gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());
			_Logger.debug("start updating gstn invoice status to new ");
			/*
			 * int count = em .createQuery(
			 * "update B2BDetailEntity b set b.type='NEW' where  b.b2bTransaction in (select b2b from B2BTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.taxpayerGstin =:taxPayerGstin and b2b.returnType =:returnType)  and b.dataSource='GSTN'"
			 * ) .setParameter("taxPayerGstin", gstin).setParameter("returnType",
			 * gstinTransactionDto.getReturnType()) .setParameter("monthYear",
			 * gstinTransactionDto.getMonthYear()).executeUpdate();
			 */
			int count = em.createNativeQuery("{call resetGstnTypeB2b(:gstnId,:returnType,:monthYear)}")
					.setParameter("gstnId", gstin.getId())
					.setParameter("returnType", gstinTransactionDto.getReturnType())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
			// em.getTransaction().commit();
			_Logger.debug("end updating gstn invoice status to new {} ", count);

			_Logger.debug("compare b2b mismatch procedure started ");
			count =em.createNativeQuery("{call compareB2bMismatch(:gstnId,:returnType,:monthYear)}")
					.setParameter("gstnId", gstin.getId())
					.setParameter("returnType", gstinTransactionDto.getReturnType())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
			// em.getTransaction().commit();
			_Logger.debug("compare b2b mismatch procedure end  coun {}",count);
			// missing outward
			_Logger.debug("missing outward comparison via procedure started ");
			count=em.createNativeQuery("{call compareB2B(:gstnId,:returnType,:monthYear)}")
					.setParameter("gstnId", gstin.getId())
					.setParameter("returnType", gstinTransactionDto.getReturnType())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
			// em.getTransaction().commit();
			_Logger.debug("missing outward comparison via procedure end  count {}",count);
			// missing inward ward
			_Logger.debug("missing inward comparison started ");

			int missingInwardCount = em
					.createNativeQuery("{call compareB2bMissingInward(:gstnId,:returnType,:monthYear)}")
					.setParameter("gstnId", gstin.getId())
					.setParameter("returnType", gstinTransactionDto.getReturnType())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
			// em.getTransaction().commit();

			_Logger.debug("end missing inward comparison end updated invoices count {}", missingInwardCount);

			this.compareSimmilarNonSimilar(gstinTransactionDto);
			_Logger.debug("compare b2b successfully executed");

			// end missing inward
		} catch (Exception e) {
			_Logger.error("exception in comparing transaction ", e);
			throw e;
		}

	}

	@Override
	public int compareSimmilarNonSimilar(GstinTransactionDTO gstinTransactionDto) {
		TransactionType tran = TransactionType.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());
		int simmNoSimmCount = 0;
		if (TransactionType.B2B == tran || "all".equalsIgnoreCase(gstinTransactionDto.getTransactionType())) {
			_Logger.debug("simmilar/no simmilar comparison via procedure started ");
			simmNoSimmCount = em
					.createNativeQuery("{call compareB2bSimmilarNonSimmilar(:gstnId,:returnType,:monthYear)}")
					.setParameter("gstnId", gstin.getId())
					.setParameter("returnType", gstinTransactionDto.getReturnType())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
			_Logger.debug("simmilar/no simmilar comparison via procedure end {}", simmNoSimmCount);
		}
		if (TransactionType.CDN == tran || "all".equalsIgnoreCase(gstinTransactionDto.getTransactionType())) {
			_Logger.debug("simmilar/no simmilar comparison via procedure started ");
			simmNoSimmCount = em
					.createNativeQuery("{call compareCdnSimmilarNonSimmilar(:gstnId,:returnType,:monthYear)}")
					.setParameter("gstnId", gstin.getId())
					.setParameter("returnType", gstinTransactionDto.getReturnType())
					.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
			_Logger.debug("simmilar/no simmilar comparison via procedure end {}", simmNoSimmCount);

		}

		return simmNoSimmCount;

	}

	private void compareCDN(GstinTransactionDTO gstinTransactionDto) {
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		_Logger.debug("start updating gstn invoice status to new ");
		/*
		 * int count = em .createQuery(
		 * "update CDNDetailEntity b set b.type='NEW' where  b.cdnTransaction in (select b2b from CDNTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.gstin =:taxPayerGstin and b2b.returnType =:returnType)  and b.dataSource='GSTN'"
		 * ) .setParameter("taxPayerGstin", gstin).setParameter("returnType",
		 * gstinTransactionDto.getReturnType()) .setParameter("monthYear",
		 * gstinTransactionDto.getMonthYear()).executeUpdate();
		 */
		int count = em.createNativeQuery("{call resetGstnTypeCdn(:gstnId,:returnType,:monthYear)}")
				.setParameter("gstnId", gstin.getId()).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
		_Logger.debug("end updating gstn invoice status to new {} ", count);

		_Logger.debug("compare cdn mismatch procedure started ");
		em.createNativeQuery("{call compareCdnMismatch(:gstnId,:returnType,:monthYear)}")
				.setParameter("gstnId", gstin.getId()).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
		_Logger.debug("compare cdn mismatch procedure end ");

		_Logger.debug("missing outward comparison via procedure started ");
		int missingOutwardCount = em.createNativeQuery("{call compareCDN(:gstnId,:returnType,:monthYear)}")
				.setParameter("gstnId", gstin.getId()).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
		_Logger.debug("missing outward comparison via procedure end {}", missingOutwardCount);

		_Logger.debug("missing inward comparison started ");

		int missingInwardCount = em.createNativeQuery("{call compareCdnMissingInward(:gstnId,:returnType,:monthYear)}")
				.setParameter("gstnId", gstin.getId()).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).executeUpdate();
		_Logger.debug("end missing inward comparison end updated invoices count {} ", missingInwardCount);

		_Logger.debug("compare cdn successfully executed");

		this.compareSimmilarNonSimilar(gstinTransactionDto);

		_Logger.debug("missing inward comparison started ");

	}

	private boolean compareTransactions(GstinTransactionDTO gstinTransactionDto, String ctin) {
		TransactionType tran = TransactionType.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		if (TransactionType.B2B == tran) {
			this.compareB2B(gstinTransactionDto, ctin);
		} else if (TransactionType.CDN == tran) {
			this.compareCDN(gstinTransactionDto, ctin);

		}
		return true;
	}

	private void compareB2B(GstinTransactionDTO gstinTransactionDto, String ctin) {
		_Logger.debug("starrt compareB2b method for gstin {}, returnType {}, monthYear {}",
				gstinTransactionDto.getTaxpayerGstin().getGstin(), gstinTransactionDto.getReturnType(),
				gstinTransactionDto.getMonthYear());

		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());
		_Logger.info("start updating gstn invoice status to new for ctin {}", ctin);
		int count = em.createQuery(
				"update B2BDetailEntity b set b.type='NEW' where  b.b2bTransaction in (select b2b from B2BTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.taxpayerGstin =:taxPayerGstin and b2b.returnType =:returnType and b2b.ctin=:ctin)  and b.dataSource='GSTN'")
				.setParameter("taxPayerGstin", gstin).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).setParameter("ctin", ctin)
				.executeUpdate();
		_Logger.debug("end updating gstn  invoice status to new {} for ctin {} ", count, ctin);

		_Logger.debug("compare b2b  procedure started ");
		int updationCount = em.createNativeQuery("{call compareB2bSingleCtin(:gstnId,:returnType,:monthYear,:ctin)}")
				.setParameter("gstnId", gstin.getId()).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).setParameter("ctin", ctin)
				.executeUpdate();
		_Logger.debug("end compare b end updated invoices count {}", updationCount);

		this.compareSimmilarNonSimilar(gstinTransactionDto);
		_Logger.debug("compare b2b successfully executed");

		// end missing inward

	}

	private void compareCDN(GstinTransactionDTO gstinTransactionDto, String ctin) {
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		_Logger.debug("start updating gstn invoice status to new ");
		int count = em.createQuery(
				"update CDNDetailEntity b set b.type='NEW' where  b.cdnTransaction in (select b2b from CDNTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.gstin =:taxPayerGstin and b2b.returnType =:returnType and b2b.originalCtin=:ctin)  and b.dataSource='GSTN'")
				.setParameter("taxPayerGstin", gstin).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).setParameter("ctin", ctin)
				.executeUpdate();
		_Logger.debug("end updating gstn invoice status to new {} ", count);

		_Logger.debug("compare cdn mismatch procedure started ");
		em.createNativeQuery("{call compareCdnSingleCtin(:gstnId,:returnType,:monthYear,:ctin)}")
				.setParameter("gstnId", gstin.getId()).setParameter("returnType", gstinTransactionDto.getReturnType())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).setParameter("ctin", ctin)

				.executeUpdate();
		_Logger.debug("compare cdn mismatch procedure end ");

		this.compareSimmilarNonSimilar(gstinTransactionDto);

		_Logger.debug("missing inward comparison started ");

	}

	@SuppressWarnings("unchecked")
	public Object generateHSNSUM() {

		List<InvoiceItem> b2clDetailEntities = null;

		@SuppressWarnings("rawtypes")
		List<B2CLDetailEntity> b2cl = em
				.createNamedQuery("B2CLDetailEntity.findInvoiceDataByMonthYear", B2CLDetailEntity.class)
				.getResultList();

		// b2clDetailEntities.forEach(b2cl->{
		System.out.println(b2cl);

		return b2clDetailEntities;

	}

	public List getGstnSyncData(String gstin, String moonthYear, TransactionType transactionType) {

		return null;
	}

	public boolean updateB2csTransactionData(B2CSTransactionEntity b2cs) throws AppException {

		try {
			if (Objects.isNull(b2cs)) {
				return false;
			} else {
				/*
				 * B2CSTransactionEntity b2csTran =
				 * crudService.find(B2CSTransactionEntity.class, b2cs.getId());
				 */
				this.deleteItemsByInvoiceId(b2cs.getId());
				crudService.update(b2cs);
			}
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return true;
	}

	@Override
	public boolean saveDocIssueTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		gstinTransactionDTO
				.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "docIssues"));

		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return false;
			} else {

				Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
						Gstr1Dto.class);
				List<DocIssue> docIssues = gstr.getDocIssues();

				Map<String, Object> result = finderService
						.getGstinTransactionByGstinMonthYearReturnType(gstinTransactionDTO, null);

				List<DocIssue> existingDocs = (List) result.get("transactionData");
				for (DocIssue d : existingDocs) {
					if (d.getIsTransit()) {
						AppException ae = new AppException();
						ae.setMessage("doc issue items  are in transit state so can't be updated for the moment");
						throw ae;
					}
				}
				/*
				 * int count=em.createNamedQuery("DocIssue.deleteByMonthYear")
				 * .setParameter("gstin", gstin.getGstin()) .setParameter("monthYear",
				 * gstinTransactionDTO.getMonthYear()) .setParameter("returnType",
				 * gstinTransactionDTO.getReturnType()).executeUpdate();
				 */
				int count = this.deleteTransactionData(gstinTransactionDTO.getTaxpayerGstin().getGstin(),
						gstinTransactionDTO.getMonthYear(), TransactionType.DOC_ISSUE, ReturnType.GSTR1, null,
						DeleteType.ALL, DataSource.EMGST);
				log.debug("{} previous doc issue items deleted ", count);

				for (DocIssue docIssue : docIssues) {
					docIssue.setGstin(gstin);
					docIssue.setReturnType(gstinTransactionDTO.getReturnType());
					docIssue.setMonthYear(gstinTransactionDTO.getMonthYear());
					docIssue.setSource(SourceType.PORTAL.toString());
					InfoService.setInfo(docIssue, userBean);
					docIssue.setNetIssued(docIssue.getTotalNumber() - docIssue.getCanceled());
					crudService.create(docIssue);
				}

			}
		} catch (AppException ae) {
			throw ae;
		} catch (Exception e) {
			log.error("exception in docissue method {}", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return true;
	}

	public boolean isAllSynced(String monthYear, String gstin, ReturnType returnType) throws AppException {

		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
		Map<String, Object> params = new HashMap<>();
		params.put("monthYear", monthYear);
		params.put("gstin", taxpayerGstin);
		params.put("returnType", returnType.toString());

		List<FlagsCounterDTO> counterDTOs = new ArrayList<>();

		counterDTOs.addAll(crudService.findWithNamedQuery("ATTransactionEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("B2bUrTransactionEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("B2CLDetailEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("B2CSTransactionEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("CDNDetailEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("CDNURDetailEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("ExpDetail.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("IMPGTransactionEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("IMPSTransactionEntity.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("TxpdTransaction.getFlagsCount", params));
		counterDTOs.addAll(crudService.findWithNamedQuery("B2BDetailEntity.getFlagsCount", params));

		boolean isNilOnly = true;
		if (this.checkIfNORecord(counterDTOs, null))
			isNilOnly = false;

		List<FlagsCounterDTO> hsns = crudService.findWithNamedQuery("Hsn.getFlagsCount", params);
		if (!isNilOnly)
			this.checkIfNORecord(hsns, TransactionType.HSNSUM);
		counterDTOs.addAll(hsns);
		if (returnType.toString().equalsIgnoreCase("GSTR1")) {
			List<FlagsCounterDTO> docIssue = crudService.findWithNamedQuery("DocIssue.getFlagsCount", params);
			// if(!isNilOnly)
			// this.checkIfNORecord(docIssue,TransactionType.DOC_ISSUE);
			counterDTOs.addAll(docIssue);
		}

		counterDTOs.addAll(crudService.findWithNamedQuery("NILTransactionEntity.getFlagsCount", params));

		if (!this.checkIfNORecord(counterDTOs, null))
			throw new AppException(ExceptionCode._NO_SUBMIT_INVOICES);

		for (FlagsCounterDTO counterDTO : counterDTOs) {
			log.debug("isSynced {} toBeSynced {} isTransit {} isError {} count {} ", counterDTO.isSynced(),
					counterDTO.isToBeSynced(), counterDTO.isTransit(), counterDTO.isError(), counterDTO.getCount());

			if ((counterDTO.isError() || counterDTO.isToBeSynced() || counterDTO.isTransit())
					&& counterDTO.getCount() > 0)
				return false;
		}
		return true;
	}

	private boolean checkIfNORecord(List<FlagsCounterDTO> counterDTOs, TransactionType transactionType)
			throws AppException {

		for (FlagsCounterDTO counterDTO : counterDTOs) {
			if (counterDTO.getCount() > 0)
				return true;
		}
		if (transactionType != null)
			throw new AppException(ExceptionCode._NO_HSN_AND_DOC_DATA,
					transactionType.toString() + " is mandatory,to submit data");
		return false;
	}

	@Override
	public Map<String, String> processReturnStatus(String id) throws AppException {
		Map<String, String> resp = new HashMap<>();
		log.debug("entering in processReturnStatus {} " + id);
		if (!StringUtils.isEmpty(id)) {
			ReturnStatus returnStats = crudService.find(ReturnStatus.class, Integer.parseInt(id));
			if (!Objects.isNull(returnStats)) {
				if (returnStats.getReturnType().equalsIgnoreCase("GSTR1")) {
					try {
						GetCheckStatusResp returnStatus = null;
						returnStatus = functionalService.processReturnStatusGstr1(returnStats);
						String respMsg = "";

						if (returnStatus != null) {
							if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG;
								resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
							} else if (GSTNRespStatus.CHECKSTATUS_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
								if (returnStatus.getError() != null)
									respMsg = returnStatus.getError().getMessage();
								else
									respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_PE_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_IN_PROCESS
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_IP_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
							}
							resp.put(ApplicationMetadata.RESP_MSG_KEY, respMsg);
						} else
							throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.ERR_MSG);
					} catch (Exception e1) {
						log.error("exception {} " + e1.getMessage());
					}
				} else if (returnStats.getReturnType().equalsIgnoreCase("GSTR2")) {
					try {
						GetCheckStatusGstr2Resp returnStatus = null;
						returnStatus = functionalService.processReturnStatusGstr2(returnStats);
						String respMsg = "";

						if (returnStatus != null) {
							if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG;
								resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
							} else if (GSTNRespStatus.CHECKSTATUS_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
								if (returnStatus.getError() != null)
									respMsg = returnStatus.getError().getMessage();
								else
									respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_PE_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_IN_PROCESS
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_IP_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
							}
							resp.put(ApplicationMetadata.RESP_MSG_KEY, respMsg);
						} else
							throw new AppException(ExceptionCode._FAILURE, GSTNRespStatus.ERR_MSG);
					} catch (Exception e1) {
						log.error("exception {} " + e1.getMessage());
					}
				} else if (returnStats.getReturnType().equalsIgnoreCase("GSTR2")) {
					try {
						GetCheckStatusGstr2Resp returnStatus = null;
						returnStatus = functionalService.processReturnStatusGstr2(returnStats);
						String respMsg = "";

						if (returnStatus != null) {
							if (GSTNRespStatus.CHECKSTATUS_PROCESSED.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_PROCESS_MSG;
								resp.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
							} else if (GSTNRespStatus.CHECKSTATUS_ERROR.equalsIgnoreCase(returnStatus.getStatus())) {
								if (returnStatus.getError() != null)
									respMsg = returnStatus.getError().getMessage();
								else
									respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_PROCESSED_ERROR
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_PE_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_IN_PROCESS
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_IP_MSG;
							} else if (GSTNRespStatus.CHECKSTATUS_REC_ERROR
									.equalsIgnoreCase(returnStatus.getStatus())) {
								respMsg = GSTNRespStatus.CHECKSTATUS_ER_MSG;
							}
							resp.put(ApplicationMetadata.RESP_MSG_KEY, respMsg);
						} else
							throw new AppException(ExceptionCode._ERROR);
					} catch (Exception e1) {
						log.error("exception {} " + e1.getMessage());
					}

				}
			}
		} else {
			throw new AppException(ExceptionCode._ERROR);
		}

		return resp;
	}

	@Override
	public List<ReturnStatus> getReturnStatusAllByGstn(GstinTransactionDTO gstnTransactionDTO) throws AppException {
		Map<String, Object> queryParam = new HashMap<>();
		queryParam.put("gstn",
				(TaxpayerGstin) EntityHelper.convert(gstnTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
		queryParam.put("monthYear", gstnTransactionDTO.getMonthYear());
		queryParam.put("returnType", gstnTransactionDTO.getReturnType());

		List<ReturnStatus> returnStatusIPData = crudService.findWithNamedQuery("ReturnStatus.findIPDataByGstn",
				queryParam, 30);
		if (returnStatusIPData != null)
			return returnStatusIPData;
		else
			throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.NO_DATA_TO_BE_SYNC);
	}

	@Override
	public Map<String, Object> getFilteredReturnStatusAllByGstn(GstinTransactionDTO gstnTransactionDTO,
			FilterDto filter) throws AppException {

		TaxpayerGstin gstin = (TaxpayerGstin) EntityHelper.convert(gstnTransactionDTO.getTaxpayerGstin(),
				TaxpayerGstin.class);
		List<String> selectedFields = Arrays
				.asList(new String[] { "id", "event", "monthYear", "referenceId", "returnType", "status", "transitId",
						"response", "transaction", "totalSync", "totalNotSync", "totalCount", "flag", "creationTime" });
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ReturnStatus> cq = cb.createQuery(ReturnStatus.class);
		Root<ReturnStatus> root = cq.from(ReturnStatus.class);
		List<Selection<ReturnStatus>> selectionList = new ArrayList<>();
		for (String field : selectedFields) {
			Selection<ReturnStatus> sel = root.get(field);
			selectionList.add(sel);
		}

		Map<String, DataFilter> filters = filter.getFilterDetail();

		List<Predicate> predicates = new ArrayList<>();
		if (!Objects.isNull(filters) && filter.isActive()) {
			for (Map.Entry<String, DataFilter> entry : filters.entrySet()) {
				DataFilter value = entry.getValue();
				Predicate p = null;
				if ("syncDate".equalsIgnoreCase(entry.getKey())) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					java.util.Date date = new java.util.Date();
					try {
						date = sdf.parse(value.getFilter1Value());
					} catch (ParseException e) {
						log.debug("Invalid Date");
					}
					p = cb.equal(cb.function("date", Date.class, root.get("creationTime")), date);

				} else if ("transaction".equalsIgnoreCase(entry.getKey())) {
					p = cb.equal(root.get(entry.getKey()), value.getFilter1Value());
				}
				predicates.add(p);
			}
		}
		Predicate pGstin = cb.equal(root.get("taxpayerGstin"), gstin);
		Predicate pRType = cb.equal(root.get("returnType"), gstnTransactionDTO.getReturnType());
		Predicate pMonthYear = cb.equal(root.get("monthYear"), gstnTransactionDTO.getMonthYear());
		predicates.add(pGstin);
		predicates.add(pRType);
		predicates.add(pMonthYear);

		cq.select(cb.construct(ReturnStatus.class, selectionList.toArray(new Selection[] {})))
				.where(cb.and(predicates.toArray(new Predicate[] {}))).orderBy(cb.desc(root.get("updationTime")));
		List<ReturnStatus> results = em.createQuery(cq).setFirstResult((filter.getPageNo() - 1) * filter.getSize())
				.setMaxResults(filter.getSize()).getResultList();

		Map<String, Object> temp = new HashMap<>();
		temp.put("syncStatus", results);
		temp.put("total_count", this.getFilteredReturnStatusCount(cb, ReturnStatus.class, predicates));
		return temp;

	}

	private <T> long getFilteredReturnStatusCount(CriteriaBuilder cb, Class<T> type, List<Predicate> predicates) {
		CriteriaQuery<Long> countq = cb.createQuery(Long.class);
		countq.select(cb.count(countq.from(type)));
		em.createQuery(countq);
		countq.where(predicates.toArray(new Predicate[] {}));
		Long filterCount = em.createQuery(countq).getSingleResult();
		if (Objects.nonNull(filterCount))
			return filterCount;
		else
			return 0;
	}

	@Override
	public int saveFileDetails(String rtype, String gstin, String monthYear, String request) throws AppException {
		int count = 0;
		if (!StringUtils.isEmpty(request)) {
			try {
				YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				JsonNode jsonNode = JsonMapper.objectMapper.readValue(request, JsonNode.class);
				GstinAction gstinAction = new GstinAction();
				if (jsonNode.has("action")) {
					Map<String, Object> queryParam = new HashMap<>();
					queryParam.put("status", jsonNode.get("action").asText());
					queryParam.put("particular", monthYear);
					queryParam.put("returnType", rtype);
					queryParam.put("gstin", taxpayerService.getTaxpayerGstin(gstin,fyear).getGstin());
					List<GstinAction> response = crudService.findWithNamedQuery("GstinAction.findByStatus", queryParam);
					if (CollectionUtils.isEmpty(response))
						gstinAction.setStatus(jsonNode.get("action").asText());
					else
						throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.DATA_PRESENT_MSG);
				}
				if (jsonNode.has("remarks"))
					gstinAction.setCheckStatusResponse(jsonNode.get("remarks").asText());
				if (jsonNode.has("arn"))
					gstinAction.setReferenceId(jsonNode.get("arn").asText());
				if (jsonNode.has("date"))
					gstinAction.setActionDate(new SimpleDateFormat("dd MMM yyyy").parse(jsonNode.get("date").asText()));

				gstinAction.setTaxpayerGstin(finderService.findTaxpayerGstin(gstin));
				gstinAction.setParticular(monthYear);
				gstinAction.setReturnType(rtype);

				GstinAction gstinActionResponse = crudService.create(gstinAction);
				if (gstinActionResponse != null) {
					taxpayerService.updateMonthYear(finderService.findTaxpayerGstin(gstin));
					count = 1;
				} else
					log.debug("unable to save data{}");

			} catch (IOException e) {
				log.error("while mapper error{}" + e);
				e.printStackTrace();
			} catch (ParseException e) {
				log.error("exception while converting into date {} " + e);
				e.printStackTrace();
			}
		}
		return count;
	}

	@Override
	public List<DataStatusDTO> getDataStatusGstn(GstinTransactionDTO gstnTransactionDTO) throws AppException {
		Map<String, Object> queryParam = new HashMap<>();
		List<DataStatusDTO> dataStatusDtoList = null;
		queryParam.put("gstn", finderService.findTaxpayerGstin(gstnTransactionDTO.getTaxpayerGstin().getGstin()));
		queryParam.put("monthYear", gstnTransactionDTO.getMonthYear());
		List<TokenResponse> listToken = crudService.findWithNamedQuery("TokenResponse.findByGstinMonthYear",
				queryParam);
		List<Object[]> listobjects = new ArrayList<>();
		if (!listToken.isEmpty()) {
			for (TokenResponse tokenresponse : listToken) {
				Query q = em.createNativeQuery(
						"SELECT IFNULL(SUM(IF(ed.status = 'completed', ed.invoiceCount, 0)),0) AS completedInvoices,IFNULL(SUM(IF(ed.status = 'pending', ed.invoiceCount, 0)),0) AS pendingInvoices ,IFNULL(SUM(ed.invoiceCount),0) as totalInvoices,IFNULL(SUM(IF(ed.status = 'INPROCESS', ed.invoiceCount, 0)),0) AS inprocessInvoices,t.transaction as transaction ,t.updationTime as updationTime, t.status as status FROM encoded_data ed,token_response t where t.id=ed.referenceId and ed.referenceId=? order by t.updationTime desc");
				q.setParameter(1, tokenresponse.getId());
				Object[] objects = (Object[]) q.getSingleResult();
				listobjects.add(objects);
			}
			if (listobjects != null) {
				if (!listobjects.isEmpty())
					dataStatusDtoList = map(DataStatusDTO.class, listobjects);
				else
					throw new AppException(ExceptionCode._ERROR, GSTNRespStatus.NO_REQUEST_FOUND);
			}

		}
		return dataStatusDtoList;
	}

	// public static <T> mapa(Class<T> type,){
	//
	// }

	public static <T> List<T> map(Class<T> type, List<Object[]> records) {
		List<T> result = new LinkedList<>();
		for (Object[] record : records) {
			result.add(maparray(type, record));
		}
		return result;
	}

	public static <T> T maparray(Class<T> type, Object[] tuple) {
		List<Class<?>> tupleTypes = new ArrayList<>();
		for (Object field : tuple) {
			tupleTypes.add(field.getClass());
		}
		try {
			Constructor<T> ctor = type.getConstructor(tupleTypes.toArray(new Class<?>[tuple.length]));
			return ctor.newInstance(tuple);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Item rectifyBlankHSN(String itemId, GstinTransactionDTO gstinTransactionDTO, String hsnCode,
			String hsnDescription, String unit, Double quantity) {

		Item item = crudService.find(Item.class, itemId);

		if (StringUtils.isNotEmpty(hsnCode))
			item.setHsnCode(hsnCode);

		if (StringUtils.isNotEmpty(hsnDescription))
			item.setHsnDescription(hsnDescription);

		if (!Objects.isNull(quantity))
			item.setQuantity(quantity);

		if (StringUtils.isNotEmpty(unit))
			item.setUnit(unit);

		return crudService.update(item);
	}

	@Override
	public Map<String, String> getBlankHsnCount(String monthYear, String gstin, String returnType, String type) {

		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

		type = type.toUpperCase();

		Map<String, String> resultMap = new HashMap<>();

		Map queryParamMap = QueryParameter.with("monthYear", monthYear).and("taxPayerGstin", taxpayerGstin)
				.and("returnType", returnType).parameters();

		if (!StringUtils.isEmpty(type) && (TransactionType.B2B.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
			resultMap.put(TransactionType.B2B.toString(), crudService
					.findWithNamedQuery("B2bDetailEntity.countBlankHsnBulk", queryParamMap).get(0).toString());

		if (!StringUtils.isEmpty(type) && (TransactionType.CDN.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))

			resultMap.put(TransactionType.CDN.toString(), crudService
					.findWithNamedQuery("CdnDetailEntity.countBlankHsnBulk", queryParamMap).get(0).toString());

		if ((!StringUtils.isEmpty(type) && (TransactionType.IMPG.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
				&& ReturnType.GSTR2.toString().equalsIgnoreCase(returnType))

			resultMap.put(TransactionType.IMPG.toString(), crudService
					.findWithNamedQuery("IMPGTransactionEntity.countBlankHsnBulk", queryParamMap).get(0).toString());

		if ((!StringUtils.isEmpty(type) && (TransactionType.IMPS.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
				&& ReturnType.GSTR2.toString().equalsIgnoreCase(returnType))

			resultMap.put(TransactionType.IMPS.toString(), crudService
					.findWithNamedQuery("IMPSTransactionEntity.countBlankHsnBulk", queryParamMap).get(0).toString());

		if ((!StringUtils.isEmpty(type) && (TransactionType.B2BUR.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
				&& ReturnType.GSTR2.toString().equalsIgnoreCase(returnType))

			resultMap.put(TransactionType.B2BUR.toString(), crudService
					.findWithNamedQuery("B2bUrTransactionEntity.countBlankHsnBulk", queryParamMap).get(0).toString());

		if (!StringUtils.isEmpty(type) && (TransactionType.CDNUR.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
			resultMap.put(TransactionType.CDNUR.toString(), crudService
					.findWithNamedQuery("CdnUrDetailEntity.countBlankHsnBulk", queryParamMap).get(0).toString());
		if (!StringUtils.isEmpty(type) && (TransactionType.B2CL.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
			resultMap.put(TransactionType.B2CL.toString(), crudService
					.findWithNamedQuery("B2clDetailEntity.countBlankHsnBulk", queryParamMap).get(0).toString());
		if (!StringUtils.isEmpty(type) && (TransactionType.EXP.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
			resultMap.put(TransactionType.EXP.toString(),
					crudService.findWithNamedQuery("ExpDetail.countBlankHsnBulk", queryParamMap).get(0).toString());
		if (!StringUtils.isEmpty(type) && (TransactionType.B2CS.toString().equalsIgnoreCase(type)
				|| TransactionType.ALL.toString().equalsIgnoreCase(type)))
			resultMap.put(TransactionType.B2CS.toString(), crudService
					.findWithNamedQuery("B2csTransaction.countBlankHsnBulk", queryParamMap).get(0).toString());

		return resultMap;
	}

	@Override
	public Map<String, Object> getMissingInvoices(GstinTransactionDTO gstinTransactionDTO, String missingType,
			FilterDto filter) throws AppException {

		Map<String, Object> resp = new HashMap<>();
		Map<String, DataFilter> filterMap = new HashMap<>();
		if (!Objects.isNull(filter.getFilterDetail()) && filter.isActive()) {
			filterMap.putAll(filter.getFilterDetail());
		}
		DataFilter typeFilter = new DataFilter();
		typeFilter.setActive(true);
		typeFilter.setFilter1Name(FilterType.EQUAL.toString());
		typeFilter.setFilter1Value(ReconsileType.MISSING_INWARD.toString());
		filterMap.put("type", typeFilter);
		DataFilter cfsFilter = new DataFilter();
		cfsFilter.setActive(true);
		cfsFilter.setFilter1Name(FilterType.EQUAL.toString());
		cfsFilter.setFilter1Value("Y");
		filterMap.put("cfs", cfsFilter);
		//
		DataFilter flagFilter = new DataFilter();
		flagFilter.setActive(true);
		flagFilter.setFilter1Name(FilterType.NOT_EQUAL.toString());
		flagFilter.setFilter1Value("REJECTED");
		flagFilter.setFilter2Name(FilterType.NOT_EQUAL.toString());
		flagFilter.setFilter2Value("PENDING");
		flagFilter.setOperator("and");

		filterMap.put("flags", flagFilter);
		//

		DataFilter subTypeFilter = new DataFilter();
		subTypeFilter.setActive(true);
		subTypeFilter.setFilter1Name(FilterType.EQUAL.toString());
		if ("simmilar".equalsIgnoreCase(missingType) || "similar".equalsIgnoreCase(missingType)) {
			subTypeFilter.setFilter1Value(ReconsileType.MISSING_INWARD_SIMMILAR.toString());
		} else {
			subTypeFilter.setFilter1Value(ReconsileType.MISSING_INWARD_NON_SIMMILAR.toString());
		}
		filterMap.put("subType", subTypeFilter);
		filter.setFilterDetail(filterMap);
		filter.setActive(true);

		resp = this.getTransactionData(gstinTransactionDTO, filter);
		return resp;

	}

	@Override
	public Map<String, Object> getMismatchTransaction(GstinTransactionDTO gstinTransactionDTO, String mismatchType,
			FilterDto filter) throws AppException {
		Map<String, Object> resp = new HashMap<>();
		Map<String, DataFilter> filterMap = new HashMap<>();
		filterMap.putAll(filter.getFilterDetail());
		DataFilter cfsFilter = new DataFilter();
		cfsFilter.setActive(true);
		cfsFilter.setFilter1Name(FilterType.EQUAL.toString());
		cfsFilter.setFilter1Value("Y");
		filterMap.put("cfs", cfsFilter);
		//
		DataFilter flagFilter = new DataFilter();
		flagFilter.setActive(true);
		flagFilter.setFilter1Name(FilterType.NOT_EQUAL.toString());
		flagFilter.setFilter1Value("REJECTED");
		flagFilter.setFilter2Name(FilterType.NOT_EQUAL.toString());
		flagFilter.setFilter2Value("PENDING");
		flagFilter.setOperator("and");

		filterMap.put("flags", flagFilter);
		//

		filter.setFilterDetail(filterMap);
		filter.setActive(true);
		resp = this.getTransactionData(gstinTransactionDTO, filter);
		return resp;

	}

	@Override
	public Map<String, Object> getSimilarCtinMissingOutward(GstinTransactionDTO gstinTransactionDTO,
			CtinInvoiceDto detail) throws AppException {
		if (TransactionType.CDN.toString().equalsIgnoreCase(gstinTransactionDTO.getTransactionType())) {
			return this.getSimilarCtinMissingOutwardCdn(gstinTransactionDTO, detail);
		}
		Map<String, Object> resp = new HashMap<>();
		List datas = null;
		long count = 0;
		B2BDetailEntity invoice = crudService.find(B2BDetailEntity.class, detail.getInvoiceId());
		if (Objects.nonNull(invoice)) {
			double diffAmount = (detail.getDiffPercent() / 100) * invoice.getTaxAmount();
			FilterDto filter = detail.getFilter();

			datas = crudService.findPaginatedResults("B2bDetailEntity.findInvoiceByCtin",
					QueryParameter.with("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear())
							.and("taxPayerGstin",
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()))
							.and("ctin", invoice.getB2bTransaction().getCtin()).and("taxAmount", invoice.getTaxAmount())
							.and("diff", diffAmount).parameters(),
					filter.getPageNo(), filter.getSize());

			count = crudService.findResultCounts("B2bDetailEntity.findInvoiceByCtinCount",
					QueryParameter.with("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear())
							.and("taxPayerGstin",
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()))
							.and("ctin", invoice.getB2bTransaction().getCtin()).and("taxAmount", invoice.getTaxAmount())
							.and("diff", diffAmount)

							.parameters());

			if (!Objects.isNull(datas))
				resp.put("transactionData", datas);
			else
				resp.put("transactionData", "[]");
			resp.put("total_count", count);
			return resp;
		} else {
			AppException ae = new AppException();
			ae.setMessage("invalid invoice id");
			throw ae;
		}

	}

	private Map<String, Object> getSimilarCtinMissingOutwardCdn(GstinTransactionDTO gstinTransactionDTO,
			CtinInvoiceDto detail) throws AppException {
		B2BDetailEntity invoice = crudService.find(B2BDetailEntity.class, detail.getInvoiceId());
		Map<String, Object> resp = new HashMap<>();
		List datas = null;
		long count = 0;
		if (Objects.nonNull(invoice)) {
			double diffAmount = (detail.getDiffPercent() / 100) * invoice.getcTaxAmount();
			FilterDto filter = detail.getFilter();
			datas = crudService.findPaginatedResults("CdnDetailEntity.findInvoiceByCtin",
					QueryParameter.with("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear())
							.and("taxPayerGstin",
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()))
							.and("ctin", invoice.getB2bTransaction().getCtin()).and("taxAmount", invoice.getTaxAmount())
							.and("diff", diffAmount)

							.parameters(),
					filter.getPageNo(), filter.getSize());
			count = crudService.findResultCounts("CdnDetailEntity.findInvoiceByCtin",
					QueryParameter.with("returnType", gstinTransactionDTO.getReturnType())
							.and("monthYear", gstinTransactionDTO.getMonthYear())
							.and("taxPayerGstin",
									finderService.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin()))
							.and("ctin", invoice.getB2bTransaction().getCtin()).and("taxAmount", invoice.getTaxAmount())
							.and("diff", diffAmount)

							.parameters());
			if (!Objects.isNull(datas))
				resp.put("transactionData", datas);
			else
				resp.put("transactionData", "[]");
			resp.put("total_count", count);
			return resp;
		} else {
			AppException ae = new AppException();
			ae.setMessage("invalid invoice id");
			throw ae;
		}
	}

	@Override
	public SearchTaxpayerDTO searchTaxPayer(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	public void copyAcceptedInvItems(String gstin, String monthYear, String returnType, String transactionType)
			throws AppException {
		log.debug("copy invoice item method start ");

		List<String> invoiceIds = null;
		String qry = null;

		if ("b2b".equalsIgnoreCase(transactionType)) {
			invoiceIds = crudService.findWithNamedQuery("B2bDetailEntity.findAutoMatch",
					QueryParameter.with("monthYear", monthYear).and("returnType", returnType)
							.and("taxpayerGstin", finderService.findTaxpayerGstin(gstin)).parameters());
			qry = "update b2b_details set isError=0,toBeSync=1,errMsg='',flags='ACCEPTED' where id in :invoices and toBeSync=1";

		} else if ("cdn".equalsIgnoreCase(transactionType)) {
			invoiceIds = crudService.findWithNamedQuery("CDNDetailEntity.findAutoMatch",
					QueryParameter.with("monthYear", monthYear).and("returnType", returnType)
							.and("taxpayerGstin", finderService.findTaxpayerGstin(gstin)).parameters());
			qry = "update cdn_details set isError=0,toBeSync=1,errMsg='',flags='ACCEPTED' where id in :invoices and toBeSync=1";
		}
		YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		
		GstinTransactionDTO gstinTransactionDto = new GstinTransactionDTO();

		gstinTransactionDto.setReturnType(returnType);
		gstinTransactionDto.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
		gstinTransactionDto.setMonthYear(monthYear);
		gstinTransactionDto.setTransactionType(transactionType);
		List<ChangeStatusDto> changeStatusDtos = new ArrayList<>();

		for (String invoiceId : invoiceIds) {

			ChangeStatusDto changeStatusDto = new ChangeStatusDto();
			changeStatusDto.setInvoiceId(invoiceId);
			changeStatusDto.setTaxPayerAction("ACCEPT");
			changeStatusDto.setType(ReconsileType.MISMATCH_MAJOR.toString());

			changeStatusDtos.add(changeStatusDto);
		}
		this.changeStatusCode(B2BDetailEntity.class, changeStatusDtos, gstinTransactionDto);

		int updatedInvs = 0;
		if (invoiceIds != null && !invoiceIds.isEmpty())
			updatedInvs = em.createNativeQuery(qry).setParameter("invoices", invoiceIds).executeUpdate();

		log.debug("copy invoice item method end ,{} invoices updated", updatedInvs);

	}
	//

	@Override
	public boolean changeCtinByInvoiceId(GstinTransactionDTO gstinTransactionDTO, ChangeStatusDto invoice)
			throws AppException {

		TransactionType trantype = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDTO.getTransactionType()));
		// Class<?> type = finderService.getEntityTypeByTransaction(trantype);
		if (TransactionType.B2B == trantype) {
			B2BDetailEntity b2bDetail = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());
			this.checkIsEditibleB2b(b2bDetail);
			B2BTransactionEntity b2bTran = b2bDetail.getB2bTransaction();
			if (StringUtils.isEmpty(invoice.getCtin())) {
				AppException ae = new AppException();
				ae.setMessage("You can't set  ctin empty..");
				throw ae;
			} else {
				B2BTransactionEntity b2bTranNewCtin = this.getB2bTransactionByCtin(gstinTransactionDTO,
						invoice.getCtin());
				if (Objects.nonNull(b2bTranNewCtin)) {
					this.moveInvoicesToOtherCtin(b2bTran.getId(), b2bTranNewCtin.getId());
				} else {
					b2bTran.setCtin(StringUtils.upperCase(invoice.getCtin()));
					b2bTran.setCtinName(invoice.getCtinName());
					crudService.update(b2bTran);
					int updateInvoiceCount = em.createNamedQuery("B2BDetailEntity.updateErrorFlagByb2bTransaction")
							.setParameter("b2bTransaction", b2bTran).executeUpdate();
					_Logger.info("ctin success fully changed for {} invoices ", updateInvoiceCount);
				}
				if (ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType()))
					this.compareB2B(gstinTransactionDTO, invoice.getCtin().trim());

			}
		} else if (TransactionType.CDN == trantype) {
			CDNDetailEntity cdnDetail = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
			this.checkIsEditibleCdn(cdnDetail);
			CDNTransactionEntity cdnTran = cdnDetail.getCdnTransaction();
			if (StringUtils.isEmpty(StringUtils.upperCase(invoice.getCtin()))) {
				AppException ae = new AppException();
				ae.setMessage("You can't set  ctin empty..");
				throw ae;
			} else {
				CDNTransactionEntity cdnTranNewCtin = this.getCdnTransactionByCtin(gstinTransactionDTO,
						StringUtils.upperCase(invoice.getCtin()));
				if (Objects.nonNull(cdnTranNewCtin)) {
					this.moveInvoicesToOtherCtinCdn(cdnTran.getId(), cdnTranNewCtin.getId());
				} else {
					cdnTran.setOriginalCtin(StringUtils.upperCase(invoice.getCtin()));
					cdnTran.setOriginalCtinName(invoice.getCtinName());
					crudService.update(cdnTran);
					int updateInvoiceCount = em.createNamedQuery("CdnDetailEntity.updateErrorFlagBycdnTransaction")
							.setParameter("cdnTransaction", cdnTran).executeUpdate();
					_Logger.info("ctin success fully changed for {} invoices ", updateInvoiceCount);
				}
			}
			if (ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType()))
				this.compareCDN(gstinTransactionDTO, invoice.getCtin());
		}
		return true;

	}

	private boolean moveInvoicesToOtherCtin(String sourceB2bTranId, String destinationB2bTranId) throws AppException {
		boolean isSameCtin = false;
		List<String> ids = new ArrayList<>();
		String destinationInvoicsQuery = "SELECT b.invoiceNumber FROM b2b_details b where b.b2bTransactionId=:destinationB2bTranId and b.dataSource='EMGST' ";
		ids = em.createNativeQuery(destinationInvoicsQuery).setParameter("destinationB2bTranId", destinationB2bTranId)
				.getResultList();
		;
		if (ids.isEmpty() || ids.size() < 1) {
			ids.add("@");
		}
		if (sourceB2bTranId.equalsIgnoreCase(destinationB2bTranId)) {
			isSameCtin = true;
		}
		if (!isSameCtin) {
			String duplicateInvoiceQuery = "SELECT b.invoiceNumber FROM b2b_details b where b.b2bTransactionId=:sourceB2bTranId  and b.dataSource='EMGST' and b.invoiceNumber in :destinationInvoices ";
			List<String> duplicateInvoices = ids = em.createNativeQuery(duplicateInvoiceQuery)
					.setParameter("sourceB2bTranId", sourceB2bTranId).setParameter("destinationInvoices", ids)

					.getResultList();
			;
			if (!duplicateInvoices.isEmpty() || duplicateInvoices.size() > 0) {
				AppException ae = new AppException();
				ae.setMessage("Following invoices are getting duplicate. invoices-: "
						+ StringUtils.join(duplicateInvoices, ","));
				throw ae;
			}
		}
		String query = "update b2b_details set isError=false,errMsg='',flags='',b2bTransactionId=:newB2bTransactionId where b2bTransactionId=:previousB2bTranId and isSynced=false";

		int updateInvoiceCount = em.createNativeQuery(query).setParameter("newB2bTransactionId", destinationB2bTranId)
				.setParameter("previousB2bTranId", sourceB2bTranId).executeUpdate();

		_Logger.info("ctin success fully changed for {} invoices by native query", updateInvoiceCount);

		return true;
	}

	private B2BTransactionEntity getB2bTransactionByCtin(GstinTransactionDTO gstinTransactionDTO, String ctin) {
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());

		List<B2BTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
				"B2bTransaction.getByGstinMonthyearReturntypeCtin",
				QueryParameter.with("taxPayerGstin", gstin).and("monthYear", gstinTransactionDTO.getMonthYear())
						.and("returnType", gstinTransactionDTO.getReturnType()).and("ctin", ctin).parameters());
		if (!b2bSearch.isEmpty() && b2bSearch.size() > 0) {
			return b2bSearch.get(0);

		}

		return null;
	}

	private boolean moveInvoicesToOtherCtinCdn(String sourceCdnTranId, String destinationCdnTranId)
			throws AppException {

		List<String> ids = new ArrayList<>();
		String destinationInvoicsQuery = "SELECT b.invoiceNumber FROM cdn_details b where b.cdnTransactionId=:destinationCdnTranId and b.dataSource='EMGST' ";
		ids = em.createNativeQuery(destinationInvoicsQuery).setParameter("destinationCdnTranId", destinationCdnTranId)
				.getResultList();
		;
		if (ids.isEmpty() || ids.size() < 1) {
			ids.add("@");
		}

		String duplicateInvoiceQuery = "SELECT b.invoiceNumber FROM cdn_details b where b.cdnTransactionId=:sourceCdnTranId  and b.dataSource='EMGST' and b.invoiceNumber in :destinationInvoices ";
		List<String> duplicateInvoices = ids = em.createNativeQuery(duplicateInvoiceQuery)
				.setParameter("sourceCdnTranId", sourceCdnTranId).setParameter("destinationInvoices", ids)

				.getResultList();
		;
		if (!duplicateInvoices.isEmpty() || duplicateInvoices.size() > 0) {
			AppException ae = new AppException();
			ae.setMessage(
					"Following invoices are getting duplicate. invoices: " + StringUtils.join(duplicateInvoices, ","));
			throw ae;
		}

		String query = "update cdn_details set isError=false,errMsg='',cdnTransactionId=:newCdnTransactionId where cdnTransactionId=:previousCdnTranId and isSynced=false";
		int updateInvoiceCount = em.createNativeQuery(query).setParameter("newCdnTransactionId", destinationCdnTranId)
				.setParameter("previousCdnTranId", sourceCdnTranId).executeUpdate();

		_Logger.info("ctin success fully changed for {} invoices by native query", updateInvoiceCount);

		return true;
	}

	private CDNTransactionEntity getCdnTransactionByCtin(GstinTransactionDTO gstinTransactionDTO, String ctin) {
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());

		List<CDNTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
				"CdnTransaction.getByGstinMonthyearReturntypeCtin",
				QueryParameter.with("taxPayerGstin", gstin).and("monthYear", gstinTransactionDTO.getMonthYear())
						.and("returnType", gstinTransactionDTO.getReturnType()).and("ctin", ctin).parameters());
		if (!b2bSearch.isEmpty() && b2bSearch.size() > 0) {
			return b2bSearch.get(0);

		}

		return null;
	}

	private boolean checkIsEditibleB2b(B2BDetailEntity existingInvoice) throws AppException {
		if (existingInvoice.getIsTransit()) {
			AppException ae = new AppException();
			ae.setMessage("invoice is in transit state so can't be updated for the moment");
			_Logger.error("exception--invoice is in transit state so can't be updated for the moment ");
			throw ae;
		}
		if (existingInvoice.getIsSynced()) {

			AppException ae = new AppException();
			ae.setMessage("invoice has been synced with GSTN so ctin can't be updated for the moment");
			_Logger.error("invoice has been synced with GSTN so ctin can't be updated for the moment ");
			throw ae;
		}

		return true;
	}

	private boolean checkIsEditibleCdn(CDNDetailEntity existingInvoice) throws AppException {
		if (existingInvoice.getIsTransit()) {
			AppException ae = new AppException();
			ae.setMessage("invoice is in transit state so can't be updated for the moment");
			_Logger.error("exception--invoice is in transit state so can't be updated for the moment ");
			throw ae;
		}
		if (existingInvoice.getIsSynced()) {

			AppException ae = new AppException();
			ae.setMessage("invoice has been synced with GSTN so ctin can't be updated for the moment");
			_Logger.error("invoice has been synced with GSTN so ctin can't be updated for the moment ");
			throw ae;
		}

		return true;
	}

	@Override
	public boolean executeBulkOperation(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		boolean isExecuted = false;
		if (TaxpayerAction.ACCEPT.toString().equalsIgnoreCase(detail.getTaxPayerAction())
				&& detail.getType().contains("MISMATCH")) {
			isExecuted = this.acceptAllMismatches(gstinTransactionDto, detail);
		} else if (TaxpayerAction.ACCEPT.toString().equalsIgnoreCase(detail.getTaxPayerAction())
				&& ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(detail.getType())) {
			isExecuted = this.acceptAllFilteredMissingInward(gstinTransactionDto, detail);
		} else if (TaxpayerAction.REJECT.toString().equalsIgnoreCase(detail.getTaxPayerAction())
				&& ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(detail.getType())) {
			isExecuted = this.rejectAllFilteredMissingInward(gstinTransactionDto, detail);
		} else if (TaxpayerAction.PENDING.toString().equalsIgnoreCase(detail.getTaxPayerAction())
				&& ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(detail.getType())) {
			isExecuted = this.pendingAllFilteredMissingInward(gstinTransactionDto, detail);
		} else if (TaxpayerAction.PENDING.toString().equalsIgnoreCase(detail.getTaxPayerAction())
				&& detail.getType().contains("MISMATCH")) {
			isExecuted = this.pendingAllMismatches(gstinTransactionDto, detail);
		} else if (TaxpayerAction.REJECT.toString().equalsIgnoreCase(detail.getTaxPayerAction())
				&& detail.getType().contains("MISMATCH")) {
			isExecuted = this.rejectAllMismatches(gstinTransactionDto, detail);
		}

		return isExecuted;
	}

	private void checkCtinIsEmpty(String ctin) throws AppException {
		if (StringUtils.isEmpty(ctin)) {
			AppException ae = new AppException();
			ae.setMessage("You can't leave ctin empty");
			throw ae;
		}
	}

	private boolean acceptAllFilteredMissingInward(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		String ctinKey = "%" + detail.getCtin() + "%";
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		if (TransactionType.B2B == tranType) {
			List<String> invoiceIds = null;
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				invoiceIds = crudService.findWithNamedQuery("B2bDetailEntity.findIdsNoSimilarDataWithOutFilter",
						QueryParameter.with("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear()).and("taxPayerGstin", gstin)
								.parameters());

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());
				invoiceIds = crudService.findWithNamedQuery("B2bDetailEntity.findIdsNoSimilarDataWithFilter",
						QueryParameter.with("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear()).and("taxPayerGstin", gstin)
								.and("ctin", ctinKey).parameters());
			} else {
				return false;
			}

			for (String invoiceId : invoiceIds) {
				B2BDetailEntity invoiceGstn = crudService.find(B2BDetailEntity.class, invoiceId);
				if (ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(invoiceGstn.getType())) {
					this.checkB2bFilingStatus(invoiceGstn);
					String preType = invoiceGstn.getType();
					invoiceGstn.setType(ReconsileType.NEW.toString());
					invoiceGstn.setFlags("");
					crudService.update(invoiceGstn);
					B2BDetailEntity inv = new B2BDetailEntity(invoiceGstn);
					inv.setPreviousFlags(invoiceGstn.getFlags());
					inv.setPreviousType(preType);
					inv.setIsTransit(false);
					inv.setType(ReconsileType.MATCH.toString());
					inv.setSource(SourceType.PORTAL.toString());
					inv.setDataSource(DataSource.EMGST);
					inv.setFlags("ACCEPTED");

					InfoService.setInfo(inv, userBean);

					crudService.create(inv);
				}
			}

		} else if (TransactionType.CDN == tranType) {
			List<String> invoiceIds = null;
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				invoiceIds = crudService.findWithNamedQuery("CdnDetailEntity.findIdsNoSimilarDataWithOutFilter",
						QueryParameter.with("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear()).and("taxPayerGstin", gstin)
								.parameters());

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				invoiceIds = crudService.findWithNamedQuery("CdnDetailEntity.findIdsNoSimilarDataWithFilter",
						QueryParameter.with("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear()).and("taxPayerGstin", gstin)
								.and("ctin", ctinKey).parameters());
			} else
				return false;
			for (String invoiceId : invoiceIds) {

				CDNDetailEntity invoiceGstn = crudService.find(CDNDetailEntity.class, invoiceId);
				if (ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(invoiceGstn.getType())) {

					String preType = invoiceGstn.getType();
					this.checkCdnFilingStatus(invoiceGstn);

					invoiceGstn.setType(ReconsileType.NEW.toString());
					invoiceGstn.setFlags("");
					crudService.update(invoiceGstn);

					CDNDetailEntity inv = new CDNDetailEntity(invoiceGstn);
					inv.setPreviousFlags(invoiceGstn.getFlags());
					inv.setPreviousType(preType);
					inv.setType(ReconsileType.MATCH.toString());
					inv.setIsTransit(false);
					inv.setSource(SourceType.PORTAL.toString());
					inv.setDataSource(DataSource.EMGST);
					inv.setFlags("ACCEPTED");
					InfoService.setInfo(inv, userBean);
					crudService.create(inv);
				}
			}
		}

		return true;
	}

	private boolean rejectAllFilteredMissingInward(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		String ctinKey = "%" + detail.getCtin() + "%";
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		int updatedCount = 0;

		if (TransactionType.B2B == tranType) {

			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("B2bDetailEntity.rejectAllMissingInwardWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("B2bDetailEntity.rejectAllMissingInwardWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey).executeUpdate();
			}

		} else if (TransactionType.CDN == tranType) {
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("CdnDetailEntity.rejectAllMissingInwardWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("CdnDetailEntity.rejectAllMissingInwardWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey).executeUpdate();
			}

		}
		_Logger.info("rejected missing inward invoice count {}", updatedCount);
		return true;
	}

	private boolean pendingAllFilteredMissingInward(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		String ctinKey = "%" + detail.getCtin() + "%";
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		int updatedCount = 0;

		if (TransactionType.B2B == tranType) {

			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("B2bDetailEntity.pendingAllMissingInwardWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("B2bDetailEntity.pendingAllMissingInwardWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey).executeUpdate();
			}

		} else if (TransactionType.CDN == tranType) {
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("CdnDetailEntity.pendingAllMissingInwardWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());
				updatedCount = em.createNamedQuery("CdnDetailEntity.pendingAllMissingInwardWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey).executeUpdate();
			}

		}
		_Logger.info("pending missing inward invoice count {}", updatedCount);
		return true;
	}

	private boolean rejectAllMismatches(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		String ctinKey = "%" + detail.getCtin() + "%";
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		int updatedCount = 0;

		if (TransactionType.B2B == tranType) {

			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("B2bDetailEntity.rejectAllMismatchWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("reconcileType", detail.getType())

						.executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("B2bDetailEntity.rejectAllMismatchWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey)
						.setParameter("reconcileType", detail.getType())

						.executeUpdate();
			}

		} else if (TransactionType.CDN == tranType) {
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("CdnDetailEntity.rejectAllMismatchWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("reconcileType", detail.getType())
						.executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("CdnDetailEntity.rejectAllMismatchWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey)
						.setParameter("reconcileType", detail.getType())

						.executeUpdate();
			}

		}
		_Logger.info("rejected missing inward invoice count {}", updatedCount);
		return true;

	}

	private boolean pendingAllMismatches(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		String ctinKey = "%" + detail.getCtin() + "%";
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		int updatedCount = 0;

		if (TransactionType.B2B == tranType) {

			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("B2bDetailEntity.pendingAllMismatchWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("reconcileType", detail.getType())

						.executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("B2bDetailEntity.pendingAllMismatchWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey)
						.setParameter("reconcileType", detail.getType())

						.executeUpdate();
			}

		} else if (TransactionType.CDN == tranType) {
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				updatedCount = em.createNamedQuery("CdnDetailEntity.pendingAllMismatchWithoutFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("reconcileType", detail.getType())
						.executeUpdate();

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				updatedCount = em.createNamedQuery("CdnDetailEntity.pendingAllMismatchWithFilter")
						.setParameter("returnType", gstinTransactionDto.getReturnType())
						.setParameter("monthYear", gstinTransactionDto.getMonthYear())
						.setParameter("taxPayerGstin", gstin).setParameter("ctin", ctinKey)
						.setParameter("reconcileType", detail.getType())

						.executeUpdate();
			}

		}
		_Logger.info("rejected missing inward invoice count {}", updatedCount);
		return true;

	}

	private boolean acceptAllMismatches(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		String ctinKey = "%" + detail.getCtin() + "%";
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());
		if (TransactionType.B2B == tranType) {

			List<B2BDetailEntity> invoices = null;
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				invoices = crudService.findWithNamedQuery("B2bDetailEntity.findAllMismatchInvoiceByTypeWithoutFilter",
						QueryParameter.with("taxPayerGstin", gstin)
								.and("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear())
								.and("reconcileType", detail.getType())

								.parameters());

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				invoices = crudService.findWithNamedQuery("B2bDetailEntity.findAllMismatchInvoiceByTypeWithFilter",
						QueryParameter.with("taxPayerGstin", gstin)
								.and("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear())
								.and("reconcileType", detail.getType()).and("ctin", ctinKey)

								.parameters());
			}

			for (B2BDetailEntity invoice : invoices) {

				B2BDetailEntity invoiceEmg = invoice;
				/* crudService.find(B2BDetailEntity.class, invoice.) */;
				invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());
				invoiceEmg.setPreviousType(invoiceEmg.getType());

				List<B2BDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
						"B2bDetailEntity.findMismatchedInvoice",
						QueryParameter.with("taxPayerGstin", invoiceEmg.getB2bTransaction().getTaxpayerGstin())
								.and("returnType", invoiceEmg.getB2bTransaction().getReturnType())
								.and("monthYear", invoiceEmg.getB2bTransaction().getMonthYear())

								.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
								.and("dataSource", /*
													 * invoiceEmg. getDataSource()
													 */DataSource.GSTN)
								.and("ctin", invoiceEmg.getB2bTransaction().getCtin()).parameters(),
						1);

				B2BDetailEntity invoiceGstn = null;
				if (!invoiceGstns.isEmpty()) {
					invoiceGstn = invoiceGstns.get(0);

					this.checkB2bFilingStatus(invoiceGstn);

					B2bDetailEntityWrapper invoiceEmgWrapper = new B2bDetailEntityWrapper(invoiceEmg);
					ObjectMapper mapper = new ObjectMapper();
					SimpleModule module = new SimpleModule();
					module.addSerializer(B2BDetailEntity.class, new B2BRecoDetailSerializer());
					mapper.registerModule(module);
					invoiceEmg
							.setPreviousInvoice(mapper.convertValue(invoiceEmgWrapper, JsonNode.class).get("invoice"));

					/*
					 * invoiceEmg.setPreviousInvoice(
					 * JsonMapper.objectMapper.convertValue(invoiceEmg, JsonNode.class));
					 */
					// save invoice emg here before replacing it
					invoiceEmg.setPreviousType(invoiceEmg.getType());
					invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());

					invoiceEmg.setChecksum(invoiceGstn.getChecksum());
					invoiceEmg.setInvoiceDate(invoiceGstn.getInvoiceDate());
					invoiceEmg.setEtin(invoiceGstn.getEtin());
					invoiceEmg.setInvoiceType(invoiceGstn.getInvoiceType());
					invoiceEmg.setReverseCharge(invoiceGstn.getReverseCharge());
					invoiceEmg.setPos(invoiceGstn.getPos());
					// invoiceEmg.setItems(newItems);
					invoiceEmg.setTaxableValue(invoiceGstn.getTaxableValue());
					invoiceEmg.setTaxAmount(invoiceGstn.getTaxAmount());
					invoiceEmg.setFlags("ACCEPTED");
					invoiceEmg.setSource(SourceType.PORTAL.toString());
					invoiceEmg.setType(ReconsileType.MATCH.toString());
					crudService.update(invoiceEmg);

					List<Item> items = invoiceEmg.getItems();

					this.deleteItemsByInvoiceId(invoiceEmg.getId());

					// List<Item> newItems = new ArrayList<>();
					for (Item itm : invoiceGstn.getItems()) {
						Item tItem = new Item(itm);
						tItem.setInvoiceId(invoiceEmg.getId());
						Optional<Item> sameRtItm = items.stream().filter(i -> i.getTaxRate() == itm.getTaxRate())
								.findFirst();
						if (sameRtItm.isPresent()) {
							tItem.setHsnCode(sameRtItm.get().getHsnCode());
							tItem.setHsnDescription(sameRtItm.get().getHsnDescription());
							tItem.setUnit(sameRtItm.get().getUnit());
							tItem.setQuantity(sameRtItm.get().getQuantity());
						}

						// newItems.add(tItem);
						crudService.create(tItem);
					}
				}
			}
		} else if (TransactionType.CDN == tranType) {

			List<CDNDetailEntity> invoices = null;
			if ("All".equalsIgnoreCase(detail.getOperationType())) {

				invoices = crudService.findWithNamedQuery("CDNDetailEntity.findAllMismatchInvoiceByTypeWithoutFilter",
						QueryParameter.with("taxPayerGstin", gstin)
								.and("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear())
								.and("reconcileType", detail.getType())

								.parameters());

			} else if (OperationType.CTIN_FILTERED.toString().equalsIgnoreCase(detail.getOperationType())) {
				this.checkCtinIsEmpty(detail.getCtin());

				invoices = crudService.findWithNamedQuery("CDNDetailEntity.findAllMismatchInvoiceByTypeWithFilter",
						QueryParameter.with("taxPayerGstin", gstin)
								.and("returnType", gstinTransactionDto.getReturnType())
								.and("monthYear", gstinTransactionDto.getMonthYear())
								.and("reconcileType", detail.getType()).and("ctin", ctinKey).parameters());
			}
			for (CDNDetailEntity invoice : invoices) {

				CDNDetailEntity invoiceEmg = invoice/* crudService.find(CDNDetailEntity.class, invoice.get) */;
				invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());
				invoiceEmg.setPreviousType(invoiceEmg.getType());
				List<CDNDetailEntity> invoiceGstns = crudService.findWithNamedQuery(
						"CdnDetailEntity.findMismatchedInvoice",
						QueryParameter.with("taxPayerGstin", invoiceEmg.getCdnTransaction().getGstin())
								.and("returnType", invoiceEmg.getCdnTransaction().getReturnType())
								.and("monthYear", invoiceEmg.getCdnTransaction().getMonthYear())
								.and("invoiceNumber", invoiceEmg.getInvoiceNumber())
								.and("ctin", invoiceEmg.getCdnTransaction().getOriginalCtin())
								.and("dataSource", invoiceEmg.getDataSource()).parameters(),
						1);
				CDNDetailEntity invoiceGstn = null;
				if (!invoiceGstns.isEmpty()) {
					invoiceGstn = invoiceGstns.get(0);

					this.checkCdnFilingStatus(invoiceGstn);

					CdnDetailEntityWrapper invoiceEmgWrapper = new CdnDetailEntityWrapper(invoiceEmg);
					ObjectMapper mapper = new ObjectMapper();
					SimpleModule module = new SimpleModule();
					module.addSerializer(CDNDetailEntity.class, new CDNRecoDetailSerializer());
					mapper.registerModule(module);
					invoiceEmg
							.setPreviousInvoice(mapper.convertValue(invoiceEmgWrapper, JsonNode.class).get("invoice"));

					/*
					 * invoiceEmg.setPreviousInvoice(
					 * JsonMapper.objectMapper.convertValue(invoiceEmg, JsonNode.class));
					 */
					// save invoice emg here before replacing it

					// invoiceEmg.setItems(newItems);
					invoiceEmg.setPreviousType(invoiceEmg.getType());
					invoiceEmg.setPreviousFlags(invoiceEmg.getFlags());
					invoiceEmg.setChecksum(invoiceGstn.getChecksum());
					invoiceEmg.setInvoiceDate(invoiceGstn.getInvoiceDate());
					invoiceEmg.setInvoiceType(invoiceGstn.getInvoiceType());

					invoiceEmg.setTaxableValue(invoiceGstn.getTaxableValue());
					invoiceEmg.setTaxAmount(invoiceGstn.getTaxAmount());
					invoiceEmg.setRevisedInvNo(invoiceGstn.getRevisedInvNo());
					invoiceEmg.setRevisedInvDate(invoiceGstn.getRevisedInvDate());
					invoiceEmg.setNoteType(invoiceGstn.getNoteType());
					invoiceEmg.setPreGstRegime(invoiceGstn.getPreGstRegime());
					invoiceEmg.setFlags("ACCEPTED");
					invoiceEmg.setSource(SourceType.PORTAL.toString());

					invoiceEmg.setType(ReconsileType.MATCH.toString());
					crudService.update(invoiceEmg);
					List<Item> items = invoiceEmg.getItems();
					this.deleteItemsByInvoiceId(invoiceEmg.getId());

					// List<Item> newItems = new ArrayList<>();
					for (Item itm : invoiceGstn.getItems()) {
						Item tItem = new Item(itm);
						tItem.setInvoiceId(invoiceEmg.getId());
						Optional<Item> sameRtItm = items.stream().filter(i -> i.getTaxRate() == itm.getTaxRate())
								.findFirst();
						if (sameRtItm.isPresent()) {
							tItem.setHsnCode(sameRtItm.get().getHsnCode());
							tItem.setHsnDescription(sameRtItm.get().getHsnDescription());
							tItem.setUnit(sameRtItm.get().getUnit());
							tItem.setQuantity(sameRtItm.get().getQuantity());
						}

						// newItems.add(tItem);
						crudService.create(tItem);
					}
				}
			}
		}

		return true;
	}

	@Override
	public boolean acceptMissingInwardWithItc(GstinTransactionDTO gstinTransactionDto, AcceptWithItcDto invoice)
			throws AppException {
		TransactionType tranType = TransactionType
				.valueOf(StringUtils.upperCase(gstinTransactionDto.getTransactionType()));
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDto.getTaxpayerGstin().getId());

		if (TransactionType.B2B == tranType) {

			B2BDetailEntity invoiceGstn = crudService.find(B2BDetailEntity.class, invoice.getInvoiceId());
			if (ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(invoiceGstn.getType())) {
				this.checkB2bFilingStatus(invoiceGstn);
				String preType = invoiceGstn.getType();
				invoiceGstn.setType(ReconsileType.NEW.toString());
				invoiceGstn.setFlags("");
				crudService.update(invoiceGstn);
				B2BDetailEntity inv = new B2BDetailEntity(invoiceGstn);

				List<Item> items = new ArrayList<>();
				for (Item item : invoiceGstn.getItems()) {
					Item itm = new Item(item);
					ItemDto itmDto = new ItemDto();
					itmDto.setItemId(item.getId());
					if (invoice.getItems().contains(itmDto)) {
						itmDto = invoice.getItems().get(invoice.getItems().indexOf(itmDto));
						if (ItcEligibilityType.NO.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())) {
							itm.setTotalEligibleTax(ItcEligibilityType.NO.toString());
						} else if (ItcEligibilityType.IP.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())
								|| ItcEligibilityType.CP.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())
								|| ItcEligibilityType.IS.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())) {
							itm.setTotalEligibleTax(StringUtils.upperCase(itmDto.getTotalEligibleTax()));
							itm.setItcIgst(itm.getIgst());
							itm.setItcCgst(itm.getCgst());
							itm.setItcSgst(itm.getSgst());
							itm.setItcCess(itm.getCess());
						}
					}
					// itm.setId(UUID.randomUUID().toString());
					items.add(itm);
				}
				inv.setItems(items);
				inv.setPreviousFlags(invoiceGstn.getFlags());
				inv.setPreviousType(preType);
				inv.setIsTransit(false);
				inv.setType(ReconsileType.MATCH.toString());
				inv.setSource(SourceType.PORTAL.toString());
				inv.setDataSource(DataSource.EMGST);
				inv.setFlags("ACCEPTED");

				InfoService.setInfo(inv, userBean);

				crudService.create(inv);
			}

		} else if (TransactionType.CDN == tranType) {

			CDNDetailEntity invoiceGstn = crudService.find(CDNDetailEntity.class, invoice.getInvoiceId());
			if (ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(invoiceGstn.getType())) {

				String preType = invoiceGstn.getType();
				this.checkCdnFilingStatus(invoiceGstn);

				invoiceGstn.setType(ReconsileType.NEW.toString());
				invoiceGstn.setFlags("");
				crudService.update(invoiceGstn);

				CDNDetailEntity inv = new CDNDetailEntity(invoiceGstn);
				List<Item> items = new ArrayList<>();
				for (Item item : invoiceGstn.getItems()) {
					Item itm = new Item(item);
					ItemDto itmDto = new ItemDto();
					itmDto.setItemId(item.getId());
					if (invoice.getItems().contains(itmDto)) {
						itmDto = invoice.getItems().get(invoice.getItems().indexOf(itmDto));
						if (ItcEligibilityType.NO.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())) {
							itm.setTotalEligibleTax(ItcEligibilityType.NO.toString());
						} else if (ItcEligibilityType.IP.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())
								|| ItcEligibilityType.CP.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())
								|| ItcEligibilityType.IS.toString().equalsIgnoreCase(itmDto.getTotalEligibleTax())) {
							itm.setTotalEligibleTax(StringUtils.upperCase(itmDto.getTotalEligibleTax()));
							itm.setItcIgst(itm.getIgst());
							itm.setItcCgst(itm.getCgst());
							itm.setItcSgst(itm.getSgst());
							itm.setItcCess(itm.getCess());
						}
					}
					// itm.setId(UUID.randomUUID().toString());
					items.add(itm);
				}
				inv.setItems(items);
				inv.setPreviousFlags(invoiceGstn.getFlags());
				inv.setPreviousType(preType);
				inv.setType(ReconsileType.MATCH.toString());
				inv.setIsTransit(false);
				inv.setSource(SourceType.PORTAL.toString());
				inv.setDataSource(DataSource.EMGST);
				inv.setFlags("ACCEPTED");
				InfoService.setInfo(inv, userBean);
				crudService.create(inv);
			}

		}

		return true;
	}

	@Override
	public void copyChecksumToEMG(GstinTransactionDTO gstinTransactionDTO) {

		String updateChecksum = "update  \n" + "     \n" + "  b2b_details e,\n" + "  b2b_details g\n" + "set \n"
				+ "e.checksum=g.checksum\n" + "WHERE e.dataSource = 'EMGST'\n" + "  AND g.dataSource = 'GSTN'\n"
				+ "and e.invoiceNumber=g.invoiceNumber\n" + "and e.b2bTransactionId=g.b2bTransactionId\n"
				+ "and e.b2bTransactionId \n" + "in\n" + "(select id from b2b_transaction \n"
				+ "where taxpayerGstinId=:gstinId and returnType=:returnType and monthYear=:monthYear) \n"
				+ "and g.b2bTransactionId \n" + "in\n" + "(select id from b2b_transaction \n"
				+ "where taxpayerGstinId=:gstinId and returnType=:returnType and monthYear=:monthYear) ;";

		Query query = em.createNativeQuery(updateChecksum)
				.setParameter("returnType", gstinTransactionDTO.getReturnType())
				.setParameter("monthYear", gstinTransactionDTO.getMonthYear())
				.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId());

		log.info("update invoice with checksum {}", query.executeUpdate());
	}

	@Override
	public Gstr3BRequest getGstr3BRequest(GstinTransactionDTO gstinTransactionDto) {
		List<TblOutwardCombinedNew> part1AndPart5 = null;
		List<TblOutwardCombinedNew> allExceptPart1AndPart5 = null;
		part1AndPart5 = this.getPart1AndPart5Gstr3BData(gstinTransactionDto);
		allExceptPart1AndPart5 = this.getAllPartExcept1And5Gstr3BData(gstinTransactionDto);
		Gstr3BRequest gstr3b = new Gstr3BRequest(part1AndPart5, allExceptPart1AndPart5);
		gstr3b.setReturnPeriod(gstinTransactionDto.getMonthYear());
		gstr3b.setGstin(gstinTransactionDto.getTaxpayerGstin().getGstin());
		try {
			_Logger.info("gstr converted values {}", new ObjectMapper().writeValueAsString(gstr3b));
		} catch (JsonProcessingException e) {
			_Logger.info("exception in writing gstr3b json object ", e);

		}

		return gstr3b;

	}

	@Override
	public List<TblOutwardCombinedNew> getGstr3BData(GstinTransactionDTO gstinTransactionDto) {
		List<TblOutwardCombinedNew> part1AndPart5 = null;
		List<TblOutwardCombinedNew> allExceptPart1AndPart5 = null;

		part1AndPart5 = this.getPart1AndPart5Gstr3BData(gstinTransactionDto);
		allExceptPart1AndPart5 = this.getAllPartExcept1And5Gstr3BData(gstinTransactionDto);

		Gstr3BRequest gstr3b = new Gstr3BRequest(part1AndPart5, allExceptPart1AndPart5);

		try {
			_Logger.info("gstr converted values {}", new ObjectMapper().writeValueAsString(gstr3b));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return part1AndPart5;

	}

	/*
	 * public List<OutwardSupplyDetail> getGstr3BDataInPojo(GstinTransactionDTO
	 * gstinTransactionDto){ List<OutwardSupplyDetail>datas=em.createNamedQuery(
	 * "TblOutwardCombinedNew.findAllPartGstr3b") .setParameter("gstin",
	 * gstinTransactionDto.getTaxpayerGstin().getId()) .setParameter("monthYear",
	 * gstinTransactionDto.getMonthYear()) .getResultList();
	 * 
	 * return datas;
	 * 
	 * 
	 * }
	 */

	/**
	 * @param gstinTransactionDto
	 * @return get part5 gstr3b data
	 */
	private List<TblOutwardCombinedNew> getPart1AndPart5Gstr3BData(GstinTransactionDTO gstinTransactionDto) {
		List<TblOutwardCombinedNew> datas = em
				.createNamedQuery("TblOutwardCombinedNew.findGroupedPartOneAndPartFiveGstr3b")
				.setParameter("gstin", gstinTransactionDto.getTaxpayerGstin().getId())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).getResultList();

		return datas;

	}

	/**
	 * @param gstinTransactionDto
	 * @return get all part data except one and five
	 */
	private List<TblOutwardCombinedNew> getAllPartExcept1And5Gstr3BData(GstinTransactionDTO gstinTransactionDto) {
		List<TblOutwardCombinedNew> datas = em.createNamedQuery("TblOutwardCombinedNew.findAllPartExceptOneAndFive")
				.setParameter("gstin", gstinTransactionDto.getTaxpayerGstin().getId())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).getResultList();

		return datas;

	}

	public Map<String, String> getMonthYearCalendar() {
		Map<String, String> monthYears = new TreeMap<>();
		/*
		 * monthYears.put("072017","Jul 2017"); monthYears.put("082017","Aug 2017");
		 * monthYears.put("092017","Sep 2017"); monthYears.put("102017","Oct 2017");
		 * monthYears.put("112017","Nov 2017"); monthYears.put("122017","Dec 2017");
		 * monthYears.put("012018","Jan 2018"); monthYears.put("022018","Feb 2018");
		 */

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMyyyy");
		DateTimeFormatter viewFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
		YearMonth gstStartingYearMonth = YearMonth.parse(ApplicationMetadata.GST_YEARMONTH, formatter);
		YearMonth currentYearMonth = YearMonth.now();
		int currentMonth = currentYearMonth.getMonth().getValue();
		YearMonth from = null;
		YearMonth to = null;
		if (currentMonth >= Month.APRIL.getValue()) {
			from = YearMonth.of(currentYearMonth.getYear(), Month.APRIL);
			to = YearMonth.of(currentYearMonth.getYear() + 1, Month.MARCH);
		} else if (currentMonth < Month.APRIL.getValue()) {
			from = YearMonth.of(currentYearMonth.getYear() - 1, Month.APRIL);
			to = YearMonth.of(currentYearMonth.getYear(), Month.MARCH);
		}
		while (from.compareTo(to) <= 0 && from.compareTo(currentYearMonth) <= 0) {
			if (from.compareTo(gstStartingYearMonth) >= 0) {
				monthYears.put(formatter.format(from), viewFormatter.format(from));
				_Logger.info("month id {}  month name {}", formatter.format(from), viewFormatter.format(from));
			}
			from = from.plusMonths(1);
		}

		return monthYears;
	}

	public Map<String, String> getMonthYearCalendar(String fiscalYear) {
		Map<String, String> monthYears = new TreeMap<>();
		/*
		 * monthYears.put("072017","Jul 2017"); monthYears.put("082017","Aug 2017");
		 * monthYears.put("092017","Sep 2017"); monthYears.put("102017","Oct 2017");
		 * monthYears.put("112017","Nov 2017"); monthYears.put("122017","Dec 2017");
		 * monthYears.put("012018","Jan 2018"); monthYears.put("022018","Feb 2018");
		 */
		YearMonth currentYearMonth = null;
		int currentMonth = 0;
		String[] years = fiscalYear.split("-");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMyyyy");
		DateTimeFormatter viewFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
		YearMonth gstStartingYearMonth = YearMonth.parse(ApplicationMetadata.GST_YEARMONTH, formatter);
		if (CalanderCalculatorUtility.isCurrentFiscal(fiscalYear)) {
			currentYearMonth = YearMonth.now();
			currentMonth = currentYearMonth.getMonth().getValue();
		} else {
			currentYearMonth = YearMonth.parse("03" + years[1], formatter);
			currentMonth = currentYearMonth.getMonth().getValue();
		}

		YearMonth from = null;
		YearMonth to = null;
		if (currentMonth >= Month.APRIL.getValue()) {
			from = YearMonth.of(currentYearMonth.getYear(), Month.APRIL);
			to = YearMonth.of(currentYearMonth.getYear() + 1, Month.MARCH);
		} else if (currentMonth < Month.APRIL.getValue()) {
			from = YearMonth.of(currentYearMonth.getYear() - 1, Month.APRIL);
			to = YearMonth.of(currentYearMonth.getYear(), Month.MARCH);
		}
		while (from.compareTo(to) <= 0 && from.compareTo(currentYearMonth) <= 0) {
			if (from.compareTo(gstStartingYearMonth) >= 0) {
				monthYears.put(formatter.format(from), viewFormatter.format(from));
				_Logger.info("month id {}  month name {}", formatter.format(from), viewFormatter.format(from));
			}
			from = from.plusMonths(1);
		}

		return monthYears;
	}

	@Override
	public Map<String, List<GstCalendarDto>> getDueDateCalendar(String gstin, String fyear) {
		Map<String, String> monthYears = this.getMonthYearCalendar(fyear);
		Map<String, List<GstCalendarDto>> gstCalendars = new HashMap<>();
		SimpleDateFormat viewSdf = new SimpleDateFormat("dd MMMM yyyy");
		List<GstCalendarDto> gstr1List = new ArrayList<>();
		List<GstCalendarDto> gstr2List = new ArrayList<>();
		List<GstCalendarDto> gstr3List = new ArrayList<>();

		List<GstCalendarDto> gstr3bList = new ArrayList<>();

		GstinActionDTO gstinActionDTO = new GstinActionDTO();

		gstinActionDTO.setStatus("Filed");
		List<String> returns = Arrays.asList("GSTR1", "GSTR2", "GSTR3", "GSTR3B");
		for (Map.Entry<String, String> ent : monthYears.entrySet()) {
			GstCalendar tempCal = null;// new GstCalendar();

			gstinActionDTO.setParticular(ent.getKey());
			gstinActionDTO.setReturnType("GSTR1");

			GstinAction action = finderService.findGstinAction(gstinActionDTO, gstin);
			if (Objects.isNull(action)) {
				tempCal = AppConfig.getGstCalendarByCode(ent.getKey(), ReturnType.GSTR1.toString());
				if (Objects.nonNull(tempCal)) {

					gstr1List.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
				}
			}
			tempCal = null;
			tempCal = AppConfig.getGstCalendarByCode(ent.getKey(), ReturnType.GSTR2.toString());
			if (Objects.nonNull(tempCal)) {
				gstr2List.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}
			tempCal = null;
			tempCal = AppConfig.getGstCalendarByCode(ent.getKey(), ReturnType.GSTR3.toString());

			if (Objects.nonNull(tempCal)) {
				gstr3List.add(Objects.isNull(tempCal) ? new GstCalendarDto()
						: (GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}

			gstinActionDTO.setReturnType(ReturnType.GSTR3B.toString());
			action = finderService.findGstinAction(gstinActionDTO, gstin);
			if (Objects.isNull(action)) {
				tempCal = null;
				tempCal = AppConfig.getGstCalendarByCode(ent.getKey(), ReturnType.GSTR3B.toString());
				if (Objects.nonNull(tempCal)) {

					gstr3bList.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
				}
			}

		}

		gstCalendars.put(ReturnType.GSTR1.toString(), gstr1List);
		gstCalendars.put(ReturnType.GSTR2.toString(), gstr2List);
		gstCalendars.put(ReturnType.GSTR3.toString(), gstr3List);
		gstCalendars.put(ReturnType.GSTR3B.toString(), gstr3bList);

		return gstCalendars;
	}


	
	@Override
	public Map<String, List<GstCalendarDto>> getDueDateCalendarQuarterly(String gstin,String fyear) {
		// TODO Auto-generated method stub
		Map<String, String> monthYears = this.getMonthYearCalendar(fyear);
	/*	Map<String, Map<String, List<GstCalendarDto>>> gstCalendars = new HashMap<>();*/
		Map<String, List<GstCalendarDto>> gstCalendars = new HashMap<>();
		List<GstCalendarDto> gstr1List = new ArrayList<>();
		List<GstCalendarDto> gstr2List = new ArrayList<>();
		List<GstCalendarDto> gstr3List = new ArrayList<>();

		List<GstCalendarDto> gstr3bList = new ArrayList<>();
		
		GstinActionDTO gstinActionDTO= new GstinActionDTO();

		gstinActionDTO.setStatus("Filed");
		List<String> quaters=CalanderCalculatorUtility.quarterlist(fyear);
	
		for (String quater : quaters) {
			GstCalendar tempCal = null;// new GstCalendar();
		    String monthYear=CalanderCalculatorUtility.getfilingmonthyear(fyear, quater);
			gstinActionDTO.setParticular(monthYear);
			gstinActionDTO.setReturnType("GSTR1");
			GstinAction action=finderService.findGstinAction(gstinActionDTO, gstin);
			if(Objects.isNull(action)) {
			tempCal = AppConfig.getGstCalendarByCode(monthYear, ReturnType.GSTR1.toString());
			tempCal = AppConfig.getGstCalendarByCode(monthYear, ReturnType.GSTR1.toString(),fyear);
			if (Objects.nonNull(tempCal)) {
				gstr1List.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}
			}
		/*	tempCal = null;
			tempCal = AppConfig.getGstCalendarByCode(quater, ReturnType.GSTR2.toString(),fyear);
			if (Objects.nonNull(tempCal)) {
				gstr2List.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}
			tempCal = null;
			tempCal = AppConfig.getGstCalendarByCode(quater, ReturnType.GSTR3.toString(),fyear);

			if (Objects.nonNull(tempCal)) {
				gstr3List.add(Objects.isNull(tempCal) ? new GstCalendarDto()
						: (GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}*/

		}
		for (Map.Entry<String, String> ent : monthYears.entrySet()) {
			GstinAction action = finderService.findGstinAction(gstinActionDTO, gstin);
			GstCalendar tempCal = null;//
			tempCal = null;
			tempCal = AppConfig.getGstCalendarByCode(ent.getKey(), ReturnType.GSTR2.toString());
			if (Objects.nonNull(tempCal)) {
				gstr2List.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}
			tempCal = null;
			tempCal = AppConfig.getGstCalendarByCode(ent.getKey(), ReturnType.GSTR3B.toString());
			gstinActionDTO.setReturnType(ReturnType.GSTR3B.toString());
			gstinActionDTO.setParticular(ent.getKey());
			action = finderService.findGstinAction(gstinActionDTO, gstin);
			if (Objects.isNull(action)) {
			if (Objects.nonNull(tempCal)) {

				gstr3bList.add((GstCalendarDto) EntityHelper.convert(tempCal, GstCalendarDto.class));
			}
			}
		}
		gstCalendars.put(ReturnType.GSTR1.toString(), gstr1List);
		gstCalendars.put(ReturnType.GSTR2.toString(), gstr2List);
		gstCalendars.put(ReturnType.GSTR3.toString(), gstr3List);
		gstCalendars.put(ReturnType.GSTR3B.toString(), gstr3bList);
		
		return gstCalendars;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ginni.easemygst.portal.business.service.ReturnsService#
	 * getGstr3BDataForView(com.ginni.easemygst.portal.business.entity.
	 * GstinTransactionDTO) get gstr 3b data for view
	 */
	@Override
	public Gstr3bDto getGstr3BDataForView(GstinTransactionDTO gstinTransactionDto) {

		List<TblOutwardCombinedNew> part3s = em.createNamedQuery("TblOutwardCombinedNew.findGroupedPart3")
				.setParameter("gstin", gstinTransactionDto.getTaxpayerGstin().getId())
				.setParameter("monthYear", gstinTransactionDto.getMonthYear()).getResultList();// itc eligibility

		List<TblOutwardCombinedNew> part1AndPart5s = this.getPart1AndPart5Gstr3BData(gstinTransactionDto);// supply
																											// details &
																											// interest
																											// and late
																											// fee
		List<TblOutwardCombinedNew> allExcept1And5s = this.getAllPartExcept1And5Gstr3BData(gstinTransactionDto);// supply
																												// details
																												// &
																												// interest
																												// and
																												// late
																												// fee

		// Gstr3BRequest gstr3bReq = new Gstr3BRequest(part1AndPart5s,allExcept1And5s);

		Gstr3bDto gstr3b = new Gstr3bDto(part1AndPart5s, allExcept1And5s, part3s);

		return gstr3b;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ginni.easemygst.portal.business.service.ReturnsService
	 * #updateGstr3b(com.ginni.easemygst.portal.business.entity.GstinTransactionDTO,
	 * com.fasterxml.jackson.databind.JsonNode) update gstr3b data for to be sync in
	 * gstn
	 */
	@Override
	public boolean updateGstr3b(GstinTransactionDTO gstinTransactionDTO, JsonNode requestNode)
			throws JsonProcessingException, IOException, AppException {
		// JsonNode
		// node=request/*JsonMapper.objectMapper.convertValue(gstinTransactionDTO.getTransactionObject(),
		// JsonNode.class)*/;
		List<TblOutwardCombinedNew> datas = new Gstr3bDeserializer().deserialize(requestNode);
		if (datas.size() > 0) {// delete all before updating
			int count = em.createNamedQuery("TblOutwardCombinedNew.deleteByGstinMonthYear")
					.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId())
					.setParameter("monthYear", gstinTransactionDTO.getMonthYear()).executeUpdate();
			_Logger.info("{} gstr3b records deleted", count);
		}
		for (TblOutwardCombinedNew data : datas) {
			data.setGstin(gstinTransactionDTO.getTaxpayerGstin().getId());
			data.setMonth_Id(gstinTransactionDTO.getMonthYear());
			crudService.create(data);
			_Logger.info("{} gstr3b created", data.getTransaction_Type());

		}
		
	this.deleteSavedDatafrom3bsavedsummary(gstinTransactionDTO);
	
		
		_Logger.info("total {}  gstr3b records created", datas.size());

		return true;

	}

	@Override

	public Future<String> syncDataWithGstnAsynchrounously(GstinTransactionDTO gstinTransactionDTO) {

		Future<String> future = executor.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				Map<String, Object> response = new HashMap<>();
				;
				try {
					log.error("save data to gstn executor for gstn {},monthYear {}, returnType {}",
							gstinTransactionDTO.getTaxpayerGstin().getGstin(), gstinTransactionDTO.getMonthYear(),
							gstinTransactionDTO.getReturnType());
					response.putAll(syncData(gstinTransactionDTO));
					log.debug("sync result {}", StringUtils.join(response.values(), "|"));
				} catch (Exception e) {
					log.error("error in save data to gstn for gstn {},monthYear {}, returnType {},error {}",
							gstinTransactionDTO.getTaxpayerGstin().getGstin(), gstinTransactionDTO.getMonthYear(),
							gstinTransactionDTO.getReturnType(), e.getMessage());
					log.error("error occured", e);
				}
				log.debug("end saveDataToGstn");
				return "ok";
			}
		});
		return future;
	}

	@Override
	public void viewTrackReturnsAsync(TaxpayerGstin taxpayerGstin) throws AppException {

		ExecutorService executor = Executors.newFixedThreadPool(20);

		executor.submit(new Runnable() {

			@Override
			public void run() {

				try {
					viewTrackReturns(taxpayerGstin);
				} catch (AppException e) {
				}

			}
		});
		executor.shutdown();

	}

	@Override
	public void viewReturns(String gstin) throws AppException {

		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
		this.viewTrackReturns(taxpayerGstin);
	}

	private void viewTrackReturns(TaxpayerGstin taxpayerGstin) throws AppException {

		List<TrackReturnsInfo> trackReturnsInfos = null;

		if (!Objects.isNull(taxpayerGstin)) {

			trackReturnsInfos = em.createNamedQuery("TrackReturnsInfo.findUnProcessedByGstin", TrackReturnsInfo.class)
					.setParameter("taxpayerGstin", taxpayerGstin).getResultList();
			functionalService.getReturnsTrackFromGSTN(trackReturnsInfos);

		} else {

			trackReturnsInfos = em.createNamedQuery("TrackReturnsInfo.findAllAuthRecords", TrackReturnsInfo.class)
					.getResultList(); // gstins

			trackReturnsInfos.stream().collect(Collectors.groupingBy(TrackReturnsInfo::getTaxpayerGstin))
					.forEach((taxpayer, trackReturns) -> {
						try {
							functionalService.getReturnsTrackFromGSTN(trackReturns);
						} catch (AppException e) {
							_Logger.error("exception while processing track returns {}", trackReturns.toString(), e);
						}
					});
		}

	}

	@Override
	public Map<String, List<ReturnHistoryDTO>> getReturnHistoryByGstin(String gstin, String financialYear,
			String monthYear) throws AppException {

		if (StringUtils.isEmpty(financialYear))
			if (StringUtils.isNotEmpty(monthYear)) {
				financialYear = CalanderCalculatorUtility
						.getFiscalYearByMonthYear(Utility.convertStrToYearMonth(monthYear));
			} else
				throw new AppException("100011", "Financial year or month year is required");
		return this.generateReturnHistory(gstin, financialYear);
	}

	public Map<String, List<ReturnHistoryDTO>> generateReturnHistory(String gstin, String financialYear) {

		String findDues = "select gst_cal.monthId,gst_cal.returnType,if(curdate() between gst_cal.startDate and gst_cal.dueDate,'OPEN',"
				+ "if(gst_cal.startDate>curDate(),'PENDING','OVERDUE')) as status,gst_cal.startDate,gst_cal.dueDate from taxpayer_gstin gstn join "
				+ "(select monthId,returnType,startDate,dueDate from gst_calendar where financial_year=:fyear) as gst_cal where gstn.id not in "
				+ "(select action.gstin from gstin_action action where gst_cal.monthId=action.particular and "
				+ "gst_cal.returnType=action.returnType) and gstn.gstin=:gstin ";

		Query query = em.createNativeQuery(findDues, "GstinAction.ReturnHistoryDTOMapping").setParameter("gstin", gstin)
				.setParameter("fyear", financialYear);

		List<ReturnHistoryDTO> returnHistoryDTOs = query.getResultList();

		if (Objects.isNull(returnHistoryDTOs))
			returnHistoryDTOs = new ArrayList<>();

		returnHistoryDTOs.addAll(em.createNamedQuery("GstinAction.findReturnHistory")
				.setParameter("financialYear", financialYear).setParameter("gstin", gstin).getResultList());

		List<String> returnlist = Arrays.asList("GSTR1", "GSTR2", "GSTR3", "GSTR1A", "GSTR3B");
		Map<String, List<ReturnHistoryDTO>> returnHistoryMonthGroup = returnHistoryDTOs.stream()
				.sorted(new Comparator<ReturnHistoryDTO>() {
					@Override
					public int compare(ReturnHistoryDTO o1, ReturnHistoryDTO o2) {
						if (returnlist.indexOf(o1.getReturnType()) == returnlist.indexOf(o2.getReturnType())) {
							return 0;
						} else if (returnlist.indexOf(o1.getReturnType()) > returnlist.indexOf(o2.getReturnType())) {
							return 1;
						} else {
							return -1;
						}

					}

				}).collect(Collectors.groupingBy(ReturnHistoryDTO::getFullMonthName));

		Set<Entry<String, List<ReturnHistoryDTO>>> set = returnHistoryMonthGroup.entrySet();
		List<Entry<String, List<ReturnHistoryDTO>>> list = new ArrayList<Entry<String, List<ReturnHistoryDTO>>>(set);
		Collections.sort(list, new ReturnHistoryComparator());
		Map<String, List<ReturnHistoryDTO>> sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put((String) entry.getKey(), (List<ReturnHistoryDTO>) entry.getValue());
		}

		return sortedHashMap;

	}

	public Map<String, Object> getTransactionSummary2(GstinTransactionDTO gstinTransactionDTO) throws AppException {
		_Logger.info("getTransactionSummary method strarts ");
		if (!Objects.isNull(gstinTransactionDTO)) {
			boolean isUploaded = false;

			String type = gstinTransactionDTO.getTransactionType();
			String queryName = "";
			Map<String, String> queries = new HashMap<>();

			Map<String, Object> queryParameters = new HashMap<>();

			queryParameters.put("returnType", gstinTransactionDTO.getReturnType());
			queryParameters.put("taxPayerGstin",
					(TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
			queryParameters.put("monthYear", gstinTransactionDTO.getMonthYear());

			if (TransactionType.B2B.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "B2bDetailEntity.findSummaryByMonthYear";
				queries.put(TransactionType.B2B.toString(), queryName);

			}
			if (TransactionType.CDN.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "CdnDetailEntity.findSummaryByMonthYear";
				queries.put(TransactionType.CDN.toString(), queryName);

			}
			if (TransactionType.CDNUR.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "CdnUrDetailEntity.findSummaryByMonthYear";
				queries.put(TransactionType.CDNUR.toString(), queryName);

			}
			if (ReturnType.GSTR1 == ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {
				if (TransactionType.B2CL.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "B2clDetailEntity.findSummaryByMonthYear";
					queries.put(TransactionType.B2CL.toString(), queryName);

				}
				if (TransactionType.B2CS.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "B2csTransaction.findSummaryByMonthYear";
					queries.put(TransactionType.B2CS.toString(), queryName);

				}
				if (TransactionType.EXP.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "ExpDetailEntity.findSummaryByMonthYear";
					queries.put(TransactionType.EXP.toString(), queryName);

				}
				if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
					queryName = "DocIssue.findSummaryByMonthYear";
					queries.put(TransactionType.DOC_ISSUE.toString(), queryName);

				}

			}

			if (TransactionType.AT.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "AtTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.AT.toString(), queryName);

			}
			if (TransactionType.TXPD.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "TxpdTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.TXPD.toString(), queryName);

			}
			if (TransactionType.HSNSUM.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "HsnTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.HSNSUM.toString(), queryName);

			}
			if (TransactionType.NIL.toString().equalsIgnoreCase(type) || "ALL".equalsIgnoreCase(type)) {
				queryName = "NilTransaction.findSummaryByMonthYear";
				queries.put(TransactionType.NIL.toString(), queryName);
			}

			Map<String, Object> response = new HashMap<>();
			for (Map.Entry<String, String> entry : queries.entrySet()) {
				Object[] summary = crudService.runAggregateNamedQuery(entry.getValue(), queryParameters);
				// log.debug("getting summary for transaction {}",
				// entry.getKey());
				boolean isExclamtion = false;
				TransactionalDetailDTO tdDto = new TransactionalDetailDTO();
				TransactionDataDTO resDto = this.getToBeSyncCounts(gstinTransactionDTO, entry.getKey());
				if (!TransactionType.NIL.toString().equalsIgnoreCase(entry.getKey())
						&& !TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(entry.getKey())) {
					tdDto.setTaxableValue(Objects.isNull(summary[0]) ? 0.0 : (Double) summary[0]);
					tdDto.setTaxAmount(Objects.isNull(summary[1]) ? 0.0 : (Double) summary[1]);
					if (TransactionType.CDN.toString().equalsIgnoreCase(entry.getKey())
							|| TransactionType.CDNUR.toString().equalsIgnoreCase(entry.getKey())) {
						String query = TransactionType.CDN.toString().equalsIgnoreCase(entry.getKey())
								? "CdnDetailEntity.findSummaryByMonthYearCredRefund"
								: "CdnUrDetailEntity.findSummaryByMonthYearCredRefund";
						Object[] creditRefundSumm = crudService.runAggregateNamedQuery(query, queryParameters);
						Double creditTaxAmount = Objects.isNull(creditRefundSumm[1]) ? 0.0
								: (Double) creditRefundSumm[1];
						Double creditTaxableValue = Objects.isNull(creditRefundSumm[0]) ? 0.0
								: (Double) creditRefundSumm[0];
						tdDto.setTaxAmount(tdDto.getTaxAmount() - creditTaxAmount);
						tdDto.setTaxableValue(tdDto.getTaxableValue() - creditTaxableValue);

					}
					if (tdDto.getTaxableValue() > 0)
						isUploaded = true;
				}

				response.put(StringUtils.lowerCase(entry.getKey()), tdDto);
			}
			if ("ALL".equalsIgnoreCase(type) && ReturnType.GSTR1 == ReturnType
					.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType()))) {// temporary
																							// initializes
				TransactionDataDTO temp = new TransactionDataDTO();
				temp.setTaxableValue(0.0);
				temp.setTaxAmount(0.0);
				// response.put("doc_issue", temp);
				response.put("isUploaded", isUploaded);

			}
			_Logger.info("getTransactionSummary method ends ");

			return response;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}



	@Override
	public String getComparisionResultReferenceNumber(String gstin, String cpGstin, String invocieNumber,
			String financialYear, String transaction) throws AppException, JsonProcessingException {

		Map<String, B2BDetailEntity> data = this.getInvoices(gstin, cpGstin, invocieNumber, financialYear, transaction);

		String referenceNumber = UUID.randomUUID().toString();
		redisService.setBytes(referenceNumber,new ObjectMapper().writeValueAsBytes(data), false,0);

		return referenceNumber;

	}

	@Override
	public Map<String, B2BDetailEntity> getComparisionResultFromReferenceNumber(String refenceNumber)
			throws AppException, JsonParseException, JsonMappingException, IOException {

		byte[] bytes = redisService.getBytes(refenceNumber);

		return  new ObjectMapper().readValue(bytes,Map.class);
	}

	private Map<String, B2BDetailEntity> getInvoices(String gstin, String cpGstin, String invocieNumber,
			String financialYear, String transaction) throws AppException {

		return this.getSupplierAndBuyerInvs(gstin, cpGstin, invocieNumber, financialYear);

	}

	private Map<String, B2BDetailEntity> getSupplierAndBuyerInvs(String gstin, String cpGstin, String invocieNumber,
			String financialYear) throws AppException {

		Map<String, B2BDetailEntity> supplierAndBuyerData = new HashMap<>();

		supplierAndBuyerData.put("SupplierData", this.getB2BInvs(gstin, cpGstin, financialYear, invocieNumber,
				DataSource.GSTN, ReturnType.GSTR2, "Supplier"));

		supplierAndBuyerData.put("BuyerData", this.getB2BInvs(gstin, cpGstin, financialYear, invocieNumber,
				DataSource.EMGST, ReturnType.GSTR2, "Buyer"));

		return supplierAndBuyerData;
	}

	private B2BDetailEntity getB2BInvs(String gstin, String cpGstin, String financialYear, String invoiceNumber,
			DataSource dataSource, ReturnType returnType, String partyType) throws AppException {

		Query query = em.createNamedQuery("B2bdetailsEntity.findByInvAndCTIN");

		List<B2BDetailEntity> b2bDetailEntities = query.setParameter("ctin", cpGstin)
				.setParameter("financialYear", financialYear).setParameter("invoiceNumber", invoiceNumber)
				.setParameter("returnType", returnType.toString()).setParameter("datasource", dataSource)
				.setParameter("gstin", gstin).getResultList();

		if (Objects.nonNull(b2bDetailEntities) && !b2bDetailEntities.isEmpty()) {
			if (b2bDetailEntities.size() != 1) {
				throw new AppException("4000", "Illegal state of data at emg ");
			} else {

				return b2bDetailEntities.get(0);

			}
		} else {

			throw new AppException("4000", partyType + " data not available on emg portal");
		}

	}
	
  @Override
  public int deleteSavedDatafrom3bsavedsummary(GstinTransactionDTO gstinTransactionDTO){
	return em.createNamedQuery("Summary3bdata.deleteByGstinMonthYear")
			.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId())
			.setParameter("monthYear", gstinTransactionDTO.getMonthYear()).executeUpdate();
}
	
	
	public String getSavedSummary(GstinTransactionDTO gstinTransactionDTO,SummaryType SummaryType){
		String response = "";
		try {
			response = em.createNamedQuery("Summary3bdata.findBYtype", String.class)
					.setParameter("monthYear", gstinTransactionDTO.getMonthYear())
					.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId()).setParameter("summaryType",SummaryType.toString())
					.getSingleResult();
		} catch (NoResultException n) {
		}
		return response;
		
		
	}
	
	
	
    @Override
	public OffsetLiabiltyDTO calculateOffsetLiabilyDetails(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException{
		  TaxSummaryDTO tx=null;
		  String savedResponse=this.getSavedSummary(gstinTransactionDTO, SummaryType.OFFSETLIABILTY);
		  if(!StringUtils.isEmpty(savedResponse)){
		 return JsonMapper.objectMapper.readValue(savedResponse, OffsetLiabiltyDTO.class);
		  }
		  else{		
			  String OutputTaxandITCResponse=this.getSavedSummary(gstinTransactionDTO, SummaryType.OutputTaxandITC);
			  if(!StringUtils.isEmpty(savedResponse)){
			 tx= JsonMapper.objectMapper.readValue(OutputTaxandITCResponse, TaxSummaryDTO.class);
			  }
			  else{ 
			tx=this.getgstr3bsummary(gstinTransactionDTO);
			  }
		}
		TaxAmount taxotherThanRC=tx.getOutputTaxLiabiltyOtherThanRC();
		TaxAmount taxRC=tx.getTaxLiabilityUnderRC();
         TaxAmount netitc=tx.getInputCreditOnPurchasesIncludeRC();
         TaxAmount creditamt=tx.getCreditLeader();
         TaxAmount cashamt= tx.getCashLeader();
         
        BigDecimal paidigst=netitc.getIgst().add(creditamt.getIgst());
 		BigDecimal paidcgst=netitc.getCgst().add(creditamt.getCgst());
 		BigDecimal paidsgst=netitc.getSgst().add(creditamt.getSgst());
 		BigDecimal paidcess=netitc.getCess().add(creditamt.getCess());
     //Unutilised Cash
 		BigDecimal Unutilisedigst=cashamt.getIgst();
 		BigDecimal Unutilisedcgst=cashamt.getCgst();
 		BigDecimal Unutilisedsgst=cashamt.getSgst();
 		BigDecimal Unutilisedcess=cashamt.getCess();
 	/*	BigDecimal Unutilisedigst=new BigDecimal(100);
 		BigDecimal Unutilisedcgst=new BigDecimal(125);
 		BigDecimal Unutilisedsgst=new BigDecimal(125);
 		BigDecimal Unutilisedcess= BigDecimal.ZERO;*/
       
         
         
		OffsetLiabiltyDTO offsetLiabiltyDTO=new OffsetLiabiltyDTO();
		OffsetDetails otherThanRC=new OffsetDetails();
		OffsetDetails revC=new OffsetDetails();
		TaxDetail intergatedtax=new TaxDetail("Igst",taxotherThanRC.getIgst(),paidigst,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,Unutilisedigst);
		TaxDetail centraltax=new TaxDetail("Cgst",taxotherThanRC.getCgst(),BigDecimal.ZERO,paidcgst,BigDecimal.ZERO,BigDecimal.ZERO,Unutilisedcgst);
		TaxDetail statetax=new TaxDetail("Sgst",taxotherThanRC.getSgst(),BigDecimal.ZERO,BigDecimal.ZERO,paidsgst,BigDecimal.ZERO,Unutilisedsgst);
		TaxDetail cesstax=new TaxDetail("Cess",taxotherThanRC.getCess(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,paidcess,Unutilisedcess);
		TaxDetail Revintergatedtax=new TaxDetail(taxRC.getIgst(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
		TaxDetail Revcentraltax=new TaxDetail(taxRC.getCgst(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
		TaxDetail Revstatetax=new TaxDetail(taxRC.getSgst(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
		TaxDetail Revcesstax=new TaxDetail(taxRC.getCess(),BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO);
				
		revC.setCentraltax(Revcentraltax);
		revC.setIntegratedtax(Revintergatedtax);
		revC.setStatetax(Revstatetax);
		revC.setCesstax(Revcesstax);
		otherThanRC.setCentraltax(centraltax);
		otherThanRC.setIntegratedtax(intergatedtax);
		otherThanRC.setStatetax(statetax);
		otherThanRC.setCesstax(cesstax);
		offsetLiabiltyDTO.setOtherThanRC(otherThanRC);
		offsetLiabiltyDTO.setRevCharge(revC);
		
		
		
if(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().compareTo(BigDecimal.ZERO)<0){
			if(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq().compareTo(BigDecimal.ZERO)>0){
				if (offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()
						.compareTo(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs()) < 0) {
	offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setIgst(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq().abs());
	offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setIgst(	offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getIgst().subtract(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getIgst()));
	offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
	.setAddtionalCashReq(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
			.add(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()));
	offsetLiabiltyDTO.getOtherThanRC().getCentraltax()
	.setAddtionalCashReq(BigDecimal.ZERO);
			/*		offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(BigDecimal.ZERO);*/
				} else {
				
					offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setIgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setIgst(	offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getIgst().subtract(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getIgst()));		

					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
							.setAddtionalCashReq(BigDecimal.ZERO);
					offsetLiabiltyDTO.getOtherThanRC().getCentraltax()
					.setAddtionalCashReq(offsetLiabiltyDTO.getOtherThanRC().getCentraltax()
							.getAddtionalCashReq().subtract(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getIgst()));
				}
				
			}
		}
		
//

		if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0) {
			if (offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()
					.compareTo(BigDecimal.ZERO) > 0) {
				if (offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq().compareTo(
						offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs()) < 0) {
					offsetLiabiltyDTO.getOtherThanRC().getStatetax()
							.setIgst(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
							.setIgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getIgst()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getIgst()));
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(
							offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
									.add(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()));
					offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(BigDecimal.ZERO);
					
				} else {

					offsetLiabiltyDTO.getOtherThanRC().getStatetax()
							.setIgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
							.setIgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getIgst()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getIgst()));

					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(BigDecimal.ZERO);
					offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(
							offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getIgst()));
				}

			}
		}

	//
		
		
		
		if (offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0) {
			if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
					.compareTo(BigDecimal.ZERO) > 0) {
				if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().compareTo(
						offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq().abs()) < 0) {
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
							.setCgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getCentraltax()
					.setCgst(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getCgst()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getCgst()));
					offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setAddtionalCashReq(
							offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()
									.add(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()));
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(BigDecimal.ZERO);
					
				} else {

					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
					.setCgst(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getCentraltax()
					.setCgst(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getCgst()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getCgst()));

					offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setAddtionalCashReq(BigDecimal.ZERO);
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(
							offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getCgst()));
				}

			}
		}
		
		//state set off
		
		if (offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0) {
			if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
					.compareTo(BigDecimal.ZERO) > 0) {
				if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().compareTo(
						offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq().abs()) < 0) {
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
							.setSgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getStatetax()
					.setSgst(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getSgst()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getSgst()));
					offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(
							offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()
									.add(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()));
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(BigDecimal.ZERO);
					
				} else {

					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax()
					.setSgst(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq().abs());
					offsetLiabiltyDTO.getOtherThanRC().getStatetax()
					.setSgst(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getSgst()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getSgst()));

					offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(BigDecimal.ZERO);
					offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(
							offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
									.subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getSgst().abs()));
				}

			}
			
		}
		
		
		// cess set off
		if (offsetLiabiltyDTO.getOtherThanRC().getCesstax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0) {
		if(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getCess().compareTo(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getTaxliabilty()) > 0){
			offsetLiabiltyDTO.getOtherThanRC().getCesstax().setCess(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getTaxliabilty());
			offsetLiabiltyDTO.getOtherThanRC().getCesstax().setAddtionalCashReq(BigDecimal.ZERO);
		}
		}
		//utilish cash igst
		if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) > 0) {
		if(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getUnutilisedCash().compareTo(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().abs()) < 0){
			offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq().subtract(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getUnutilisedCash()));
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setUnutilisedCash(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq());
			offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(BigDecimal.ZERO);
		}
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setUnutilisedCash(BigDecimal.ZERO);
		}
	//
		
		if (offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) > 0) {
		if(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getUnutilisedCash().compareTo(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq().abs()) < 0){
			offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setAddtionalCashReq(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq().subtract(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getUnutilisedCash()));
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setUnutilisedCash(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq());
			offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setAddtionalCashReq(BigDecimal.ZERO);
		}
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setUnutilisedCash(BigDecimal.ZERO);
		}
		//

		if (offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) > 0) {
		if(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getUnutilisedCash().compareTo(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq().abs()) < 0){
			offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq().subtract(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getUnutilisedCash()));
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getStatetax().setUnutilisedCash(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq());
			offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(BigDecimal.ZERO);
		}
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getStatetax().setUnutilisedCash(BigDecimal.ZERO);
		}
		//
		if (offsetLiabiltyDTO.getOtherThanRC().getCesstax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) > 0) {
		if(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getUnutilisedCash().compareTo(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getAddtionalCashReq().abs()) < 0){
			offsetLiabiltyDTO.getOtherThanRC().getCesstax().setAddtionalCashReq(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getAddtionalCashReq().subtract(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getUnutilisedCash()));
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getCesstax().setUnutilisedCash(offsetLiabiltyDTO.getOtherThanRC().getCesstax().getAddtionalCashReq());
			offsetLiabiltyDTO.getOtherThanRC().getCesstax().setAddtionalCashReq(BigDecimal.ZERO);
		}
		}
		else{
			offsetLiabiltyDTO.getOtherThanRC().getCesstax().setUnutilisedCash(BigDecimal.ZERO);
		}
		
		
		//
		
		
		
		
	    if (offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0) {
			offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setIgst(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getIgst().add(offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().getAddtionalCashReq()));
			offsetLiabiltyDTO.getOtherThanRC().getIntegratedtax().setAddtionalCashReq(BigDecimal.ZERO);
		}
		 if(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0){
			offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setCgst(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getCgst().add(offsetLiabiltyDTO.getOtherThanRC().getCentraltax().getAddtionalCashReq()));
			offsetLiabiltyDTO.getOtherThanRC().getCentraltax().setAddtionalCashReq(BigDecimal.ZERO);
			
		}
	 if(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()
				.compareTo(BigDecimal.ZERO) < 0){
			offsetLiabiltyDTO.getOtherThanRC().getStatetax().setSgst(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getSgst().add(offsetLiabiltyDTO.getOtherThanRC().getStatetax().getAddtionalCashReq()));
			offsetLiabiltyDTO.getOtherThanRC().getStatetax().setAddtionalCashReq(BigDecimal.ZERO);
			
		}
		
	this.saveoffsetliablity(gstinTransactionDTO,JsonMapper.objectMapper.writeValueAsString(offsetLiabiltyDTO));
		
		return offsetLiabiltyDTO;
	}
	
	@Override
	public TaxSummaryDTO getgstr3bsummary(GstinTransactionDTO gstinTransactionDTO) throws AppException, IOException {
		
	String savedResponse=this.getSavedSummary(gstinTransactionDTO, SummaryType.OutputTaxandITC);
		if (!StringUtils.isEmpty(savedResponse)) {
			return JsonMapper.objectMapper.readValue(savedResponse, TaxSummaryDTO.class);
		}
		TaxSummaryDTO tx = new TaxSummaryDTO();
     try{
		Gstr3bDto gstr3b=this.getGstr3BDataForView(gstinTransactionDTO);
		SupplyDetailDto supplyDetail=gstr3b.getSupplyDetail();
		ItcDetailDto netitc=gstr3b.getItcDetail().getNetItcAvailable();
		OutwardSupplyDetail outSupNilRatedExempted=supplyDetail.getOutSupNilRatedExempted();
		OutwardSupplyDetail outSupNonGst=supplyDetail.getOutSupNonGst();
		OutwardSupplyDetail outSupZeroRated=supplyDetail.getOutSupZeroRated();
		OutwardSupplyDetail outTaxableSup=supplyDetail.getOutTaxableSup();
		OutwardSupplyDetail inwardSupRevCharge=supplyDetail.getInwardSupRevCharge();
		
		BigDecimal igst=BigDecimal.ZERO;
		BigDecimal cgst=BigDecimal.ZERO;
		BigDecimal sgst=BigDecimal.ZERO;
		BigDecimal cess=BigDecimal.ZERO;
	    igst=outSupNilRatedExempted.getIgstAmount().add(outSupNonGst.getIgstAmount()).add(outTaxableSup.getIgstAmount()).add(outSupZeroRated.getIgstAmount());
	    cgst=outSupNilRatedExempted.getCgstAmount().add(outSupNonGst.getCgstAmount()).add(outTaxableSup.getCgstAmount()).add(outSupZeroRated.getCgstAmount());
	    sgst=outSupNilRatedExempted.getSgstAmount().add(outSupNonGst.getSgstAmount()).add(outTaxableSup.getSgstAmount()).add(outSupZeroRated.getSgstAmount());
        cess=outSupNilRatedExempted.getCessAmount().add(outSupNonGst.getCessAmount()).add(outTaxableSup.getCessAmount()).add(outSupZeroRated.getCessAmount());
	    
	     TaxAmount tax=new TaxAmount(igst,cgst,sgst,cess);
	     
	     tx.setOutputTaxLiabiltyOtherThanRC(tax);
         TaxAmount taxrc=new TaxAmount(inwardSupRevCharge.getIgstAmount(),inwardSupRevCharge.getCgstAmount(),inwardSupRevCharge.getSgstAmount(),inwardSupRevCharge.getCessAmount());
	     tx.setTaxLiabilityUnderRC(taxrc);
	     TaxAmount taxnetitc=new TaxAmount(BigDecimal.valueOf(netitc.getIgstAmount()),BigDecimal.valueOf(netitc.getCgstAmount()),BigDecimal.valueOf(netitc.getSgstAmount()),BigDecimal.valueOf(netitc.getCessAmount()));
	     tx.setInputCreditOnPurchasesIncludeRC(taxnetitc);
	     GetLedgerDetailsReq cashLedgerDetailsReq=new GetLedgerDetailsReq();
	     cashLedgerDetailsReq.setGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
	     cashLedgerDetailsReq.setReturnPeriod(gstinTransactionDTO.getMonthYear());
	     CashITCBalance GetCashLedgerDetailsResp =ledgerService.getCashLedgerDetails(cashLedgerDetailsReq);
	     if(!Objects.isNull(GetCashLedgerDetailsResp.getCashBal())){
	   CashBal ocBal=GetCashLedgerDetailsResp.getCashBal();
	   Integer cashLeadercessbal= ocBal.getCessTotBal();
	   Integer cashLeadercgstbal= ocBal.getCgstTotBal();
	   Integer cashLeaderigstbal=  ocBal.getIgstTotBal();
	   Integer cashLeadersgstbal=  ocBal.getSgstTotBal();
	    TaxAmount cashLeader=new TaxAmount(BigDecimal.valueOf(cashLeaderigstbal),BigDecimal.valueOf(cashLeadercgstbal),BigDecimal.valueOf(cashLeadersgstbal),BigDecimal.valueOf(cashLeadercessbal));	
	   tx.setCashLeader(cashLeader);
	     }
     
	   if(!Objects.isNull(GetCashLedgerDetailsResp.getItcBal())){
		   ItcBal clbal=GetCashLedgerDetailsResp.getItcBal();
		   TaxAmount creditLeader=new TaxAmount(BigDecimal.valueOf(clbal.getIgstBal()),BigDecimal.valueOf(clbal.getCgstBal()),BigDecimal.valueOf(clbal.getSgstBal()),BigDecimal.valueOf(clbal.getCessBal()));	
		   tx.setCreditLeader(creditLeader);
	   }
	 
	   Summary3bdata summary3bdata=new Summary3bdata();
	   summary3bdata.setSummarydata(JsonMapper.objectMapper.writeValueAsString(tx));
	   summary3bdata.setMonthYear(gstinTransactionDTO.getMonthYear());
	   summary3bdata.setSummaryType(SummaryType.OutputTaxandITC.toString());
	   summary3bdata.setGstinId(gstinTransactionDTO.getTaxpayerGstin().getId());
	   crudService.create(summary3bdata);

         return tx;
     }
     catch( AppException ap){
			
			if((ap.getCause() instanceof javax.crypto.BadPaddingException) || ap.getCode().equalsIgnoreCase(ExceptionCode._UNAUTHORISED_GSTIN) || ap.getCode().equalsIgnoreCase(ExceptionCode._GSTN_AUTH_FAILED)){
				return tx;
			}
			else{
				return tx;				
			}
			
		}

	}
	
	@Override
	public Object genrateChallan(String dueDate,String filingDate,GstinTransactionDTO gstinTransactionDTO) throws JsonParseException, JsonMappingException, IOException{
		
		List<Summary3bdata> summarydata=em.createNamedQuery("Summary3bdata.finddata")
				.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId())
				.setParameter("monthYear", gstinTransactionDTO.getMonthYear()).setParameter("summaryType",SummaryType.OFFSETLIABILTY.toString()).getResultList();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		//convert String to LocalDate
		LocalDate dueDatefiling = LocalDate.parse(dueDate, formatter);
		LocalDate filedDate = LocalDate.parse(filingDate, formatter);
		int daysCount =(int) ChronoUnit.DAYS.between(dueDatefiling,filedDate);	
		if(daysCount<0){
			daysCount=0;
		}
         
         OffsetLiabiltyDTO offsetLiabiltyDTO=  JsonMapper.objectMapper.readValue(summarydata.get(0).getSummarydata(),OffsetLiabiltyDTO.class);
         
         OffsetDetails otherthanrc=   offsetLiabiltyDTO.getOtherThanRC();
         OffsetDetails rc=offsetLiabiltyDTO.getRevCharge();
         
    BigDecimal centraltax=otherthanrc.getCentraltax().getAddtionalCashReq().add(rc.getCentraltax().getAddtionalCashReq());
    BigDecimal intergatedtax= otherthanrc.getIntegratedtax().getAddtionalCashReq().add(rc.getIntegratedtax().getAddtionalCashReq());
    BigDecimal statetax  =otherthanrc.getStatetax().getAddtionalCashReq().add(rc.getCentraltax().getAddtionalCashReq());
    BigDecimal cesstax= otherthanrc.getCesstax().getAddtionalCashReq().add(rc.getCesstax().getAddtionalCashReq());
BigDecimal liabilityotherthanrc= otherthanrc.getIntegratedtax().getTaxliabilty().add(otherthanrc.getCentraltax().getTaxliabilty().add(otherthanrc.getStatetax().getTaxliabilty().add(otherthanrc.getCesstax().getTaxliabilty())));
BigDecimal liabilityrc= rc.getIntegratedtax().getTaxliabilty().add(rc.getCentraltax().getTaxliabilty().add(rc.getStatetax().getTaxliabilty().add(rc.getCesstax().getTaxliabilty())));
BigDecimal liability=liabilityotherthanrc.add(liabilityrc);       

List<ChallanDTO> list =new ArrayList<>();
         ChallanDTO challanDTOigst=new ChallanDTO("integrated",intergatedtax,daysCount,liability);
         ChallanDTO challanDTOcgst=new ChallanDTO("central",centraltax,daysCount,liability);
         ChallanDTO challanDTOsgst=new ChallanDTO("state",statetax,daysCount,liability);
         ChallanDTO challanDTOcess=new ChallanDTO("cess",cesstax,daysCount,liability);
         list.add(challanDTOigst);
         list.add(challanDTOcgst);
         list.add(challanDTOsgst);
         list.add(challanDTOcess);
         BigDecimal result = list.stream()
        	        .map(ChallanDTO::getTax)
        	        .reduce(BigDecimal.ZERO, BigDecimal::add);
         
         ChallanDTO overall=new ChallanDTO();
         overall.setParticular("total");
         overall.setTax(list.stream()
     	        .map(ChallanDTO::getTax)
     	        .reduce(BigDecimal.ZERO, BigDecimal::add));
     	       overall.setInterest(list.stream()
     	     	        .map(ChallanDTO::getInterest)
     	     	        .reduce(BigDecimal.ZERO, BigDecimal::add));
     	     	      overall.setFee(list.stream()
     	          	        .map(ChallanDTO::getFee)
     	          	        .reduce(BigDecimal.ZERO, BigDecimal::add));
     	          	     overall.setTotal(list.stream()
     	             	        .map(ChallanDTO::getTotal)
     	             	        .reduce(BigDecimal.ZERO, BigDecimal::add));
     	       list.add(overall);
         
     	     
     	      int count = em.createNamedQuery("TblOutwardCombinedNew.deleteByGstinMonthYearPart")
  					.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId())
  					.setParameter("monthYear", gstinTransactionDTO.getMonthYear()).executeUpdate();
  			_Logger.info("{} gstr3b deleted",  count);
     	       
     	     TblOutwardCombinedNew tbl=new TblOutwardCombinedNew();
     	      tbl.setCess(challanDTOcess.getInterest().doubleValue());
     	      tbl.setIgst(challanDTOigst.getInterest().doubleValue());
     	      tbl.setCgst(challanDTOcgst.getInterest().doubleValue());
     	      tbl.setSgst(challanDTOsgst.getInterest().doubleValue());
     	      tbl.setTransaction_Type("PART5A");
     	      tbl.setMonth_Id(gstinTransactionDTO.getMonthYear());
     	      tbl.setGstin(gstinTransactionDTO.getTaxpayerGstin().getId());
     	      crudService.create(tbl);
     	     TblOutwardCombinedNew tb2=new TblOutwardCombinedNew();
     	      tb2.setCess(challanDTOcess.getFee().doubleValue());
    	      tb2.setIgst(challanDTOigst.getFee().doubleValue());
    	      tb2.setCgst(challanDTOcgst.getFee().doubleValue());
    	      tb2.setSgst(challanDTOsgst.getFee().doubleValue());
    	      tb2.setMonth_Id(gstinTransactionDTO.getMonthYear());
    	      tb2.setGstin(gstinTransactionDTO.getTaxpayerGstin().getId());
    	      tb2.setTransaction_Type("PART5B");
    	      crudService.create(tb2);
     	      
         return list;
	}
	
	

	public String saveoffsetliablity(GstinTransactionDTO gstinTransactionDTO,String request) throws JsonParseException, JsonMappingException, IOException{
	List<Summary3bdata> summarydata=em.createNamedQuery("Summary3bdata.finddata")
		.setParameter("gstinId", gstinTransactionDTO.getTaxpayerGstin().getId())
		.setParameter("monthYear", gstinTransactionDTO.getMonthYear()).setParameter("summaryType",SummaryType.OFFSETLIABILTY.toString()).getResultList();
		if (!CollectionUtils.isEmpty(summarydata)) {
			Summary3bdata Summary3bdata = summarydata.get(0);
			Summary3bdata.setSummarydata(request);
			crudService.update(Summary3bdata);
		}
	else{
		   Summary3bdata summary3bdata=new Summary3bdata();
		   summary3bdata.setSummarydata(request);
		   summary3bdata.setMonthYear(gstinTransactionDTO.getMonthYear());
		   summary3bdata.setSummaryType(SummaryType.OFFSETLIABILTY.toString());
		   summary3bdata.setGstinId(gstinTransactionDTO.getTaxpayerGstin().getId());
		   crudService.create(summary3bdata);
	}
         
         return "ok";
	}
	
	
	@Override
	public void generate3B(String gstin,String monthYear) throws AppException {
		
		try (Connection connection = DBUtils.getConnection();
				CallableStatement item3b = connection.prepareCall("{call generate_gstr_3b(?,?)}");

		) {
			item3b.setInt(1,finderService.findTaxpayerGstin(gstin).getId());
			item3b.setString(2, monthYear);

			if (item3b.execute()) {
				log.info("form 3b generated successfully for {}",gstin);
			}

		} catch (Exception e) {
			log.error("exception while generate 3b",e);
			throw new AppException(e);
		}
	}

}

