package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import lombok.Data;

public @Data class HsnSacDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;

	private String type;

	private String code;

	private String description;

	private String displayName;

	private String cessRate;

	private String chapter;

	private String chapterDesc;

	private String subChapter;

	private String subChapterDesc;

	private String taxRate;

}
