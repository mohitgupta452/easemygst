package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.Data;



public @Data class CdnTransactionDto implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private int id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String fillingStatus;

	private String gstin;

	private String monthYear;

	private String returnType;

	private double taxableValue;

	private double taxAmount;
	
	
	private List<CdnDetailDto> cdnDetails;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	
	
public interface CdnTransactionDtoMixIn extends CommonMixin {
		
	}

	

}