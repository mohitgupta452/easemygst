package com.ginni.easemygst.portal.business.dto;

import com.mashape.unirest.http.JsonNode;

import lombok.Data;

public @Data class ZohoFormsEventData {

	public String functionKey;
	
	public JsonNode data;
	
}
