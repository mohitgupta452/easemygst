package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class PartnerDTO implements Serializable {

	private Integer id;

	private String city;

	private String clientBase;
	
	private BillingAddressDTO billingAddress;

	private String contactNo;

	private String emailAddress;

	private String partnerCode;
	
	private String partnerName;

	private String profession;

	private String state;
	
}
