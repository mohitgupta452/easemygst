package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class DocIssueItemDTO {
	
	private int canceled;

	private String snoFrom;

	private String snoTo;

	private int totalNumber;
	
	private int netIssued;
	
}
