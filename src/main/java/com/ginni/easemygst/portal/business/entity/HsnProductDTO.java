package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class HsnProductDTO implements Serializable {

	private String code;

	private String cessRate;

	private String description;

	private String displayName;

	private String taxRate;
	
}
