package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class NonGovStateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;
	
	private boolean isActive;

	
	public interface NonGovStateDTOMixIn extends CommonMixin {
		
	}
	
}
