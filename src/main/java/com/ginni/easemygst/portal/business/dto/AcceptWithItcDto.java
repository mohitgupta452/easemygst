package com.ginni.easemygst.portal.business.dto;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

import lombok.Data;

public @Data class AcceptWithItcDto {

	@JsonProperty("invoiceId")	
	private String invoiceId;
	
	@JsonProperty("items")
	private List<ItemDto> items;
	
	public @Data static class ItemDto{
		
		@JsonProperty("itemId")	
		private String itemId;
		
		@JsonProperty("elg")	
		private String totalEligibleTax;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			EqualsBuilder builder = new EqualsBuilder().append(this.itemId, ((ItemDto) obj).getItemId());
			return builder.isEquals();
		}
		
	}
	
	
}
