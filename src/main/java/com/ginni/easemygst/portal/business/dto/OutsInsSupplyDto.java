package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.entity.CommonMixin;

import lombok.Data;

public @Data class OutsInsSupplyDto {
	
	
	@JsonProperty("category")
	private String category;
	
		
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("igst")
	private Double igst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("sgst")
	private Double sgst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cgst")
	private Double cgst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cess")
	private Double cess;
		
	
public interface OutsInsSupplyDtoMixIn extends CommonMixin {
		
	}
	
	

}
