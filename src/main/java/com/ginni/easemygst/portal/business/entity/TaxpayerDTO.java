package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.constant.Constant.Status;
import com.ginni.easemygst.portal.persistence.entity.ErpConfig;

import lombok.Data;

public @Data class TaxpayerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public TaxpayerDTO() {
		
	}
	
	public TaxpayerDTO(Integer id) {
		this.id = id;
	}

	private Integer id;
	
	private Integer vendorCount;
	private Integer vendorVerifiedCount;
	private Integer customerCount;
	private Integer customerVerifiedCount;

	@JsonIgnore
	private Byte active; 

	private String emailAddress;

	@JsonIgnore
	private Byte emailAuth;
	
	private String displayName;
	
	@JsonIgnore
	private String displayLogo;
	
	private String legalName;

	@JsonIgnore
	private Byte mobileAuth;

	@JsonIgnore
	private String mobileNumber;
	
	private String panCard;

	private String status;

	@JsonIgnore
	private String requestId;
	
	private List<TaxpayerGstinDTO> taxpayerGstins;
	
	public List<TaxpayerGstinDTO> getTaxpayerGstins() {
		Set<TaxpayerGstinDTO> taxpayerGstinDTOs = new HashSet<>();
		if(!Objects.isNull(this.taxpayerGstins)) {
			taxpayerGstinDTOs.addAll(this.taxpayerGstins);
			this.taxpayerGstins.clear();
			taxpayerGstinDTOs.removeIf(u->u.getStatus()!=null && Status.INACTIVE.toString().equalsIgnoreCase(u.getStatus()));//to remove inactive gtins
			this.taxpayerGstins.addAll(taxpayerGstinDTOs);

			Collections.sort(this.taxpayerGstins, new TaxpayerGstinComparator());
		}
		return this.taxpayerGstins;
	}
	
	public interface TaxpayerDTOMixIn extends CommonMixin { 
		@JsonIgnore abstract Object getMobileAuth();
		@JsonIgnore abstract Object getEmailAuth();
	}

	public interface TaxpayerDTOUserMixIn extends TaxpayerDTOMixIn {
		@JsonIgnore abstract Object getUsers();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxpayerDTO other = (TaxpayerDTO) obj;
		if (panCard == null) {
			if (other.panCard != null)
				return false;
		} else if (!panCard.equals(other.panCard))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((panCard == null) ? 0 : panCard.hashCode());
		return result;
	}
	
	
	
}
