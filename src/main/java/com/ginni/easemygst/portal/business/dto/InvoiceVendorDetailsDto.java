package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import com.ginni.easemygst.portal.business.entity.VendorDTO;

import lombok.Data;

public @Data class InvoiceVendorDetailsDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String taxPayerGstinId;

	private InvoiceDetailsDto invoice;

	private String shippingAddress;

	private VendorDTO vendor;
	
	private Byte active;
	
	private String status;
	
	private String bankAccount;
	
	private String termsConditions;

}

