package com.ginni.easemygst.portal.business.util;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.ginni.easemygst.portal.business.dto.ReturnHistoryDTO;

public class ReturnHistoryComparator implements Comparator<Map.Entry<String, List<ReturnHistoryDTO>>> {
	  List<String> months ;
      String arr[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
 	public ReturnHistoryComparator() {
		// TODO Auto-generated constructor stub
 		 Calendar cal = Calendar.getInstance();
        int d = cal.get(Calendar.MONTH);
 	   	int i;
        int j;
        String temp;
        temp = arr[0];
         for (i = 0; i < d; i++)
         {
         	 for (j = 0; j < arr.length - 1; j++)
                  arr[j] = arr[j + 1];
              arr[j] = temp;
         	
         }
         months=Arrays.asList(arr);
	} 
 	 
 	 

	@Override
	public int compare(Entry<String, List<ReturnHistoryDTO>> o1, Entry<String, List<ReturnHistoryDTO>> o2) {
		// TODO Auto-generated method stub
		if(months.indexOf(o1.getKey())==months.indexOf(o2.getKey())){
			return 0;
		}
		else if(months.indexOf(o1.getKey())>months.indexOf(o2.getKey())){
			return 1;
		}
		else{
			 return -1;
		}

}

	
}
