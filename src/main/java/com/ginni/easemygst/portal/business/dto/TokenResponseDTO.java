package com.ginni.easemygst.portal.business.dto;

import java.sql.Timestamp;

import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import lombok.Data;

public @Data class TokenResponseDTO {
	
	 public int id;
	
	 public TaxpayerGstin taxpayerGstin;
	
	 public String returnType;
	
	 public String monthYear;
	
	 public String status;
	
	 public String validity;
	
	 public String token;
	
	 public Timestamp creationTime;
	 
	 public String transaction;


}
