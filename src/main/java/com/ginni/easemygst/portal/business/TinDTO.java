package com.ginni.easemygst.portal.business;

import java.io.Serializable;

import com.ibm.icu.util.TimeZoneTransition;

import lombok.Data;

public @Data class TinDTO implements Serializable {

	private String gstin;
	 
	private boolean flushData=true;
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TinDTO) {
			TinDTO tinDTO = (TinDTO) obj;
			if (this.gstin.equalsIgnoreCase(tinDTO.getGstin()) && this.flushData==tinDTO.isFlushData())
				return true;
		}
		return false;
	}
	
}
