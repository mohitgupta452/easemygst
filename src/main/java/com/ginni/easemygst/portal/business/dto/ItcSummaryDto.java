package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ItcSummaryDto {
	
	
	private String itemId;
	
	@JsonProperty("elg_type")
	private String elgType;
	
}
