package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

public @Data class SkuDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String amount;

	private String from;

	private String to;

	private String type; //this can be only INVOICE or GSTIN

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	public interface SkuDetailDTOMixIn extends CommonMixin {
		
	}

}
