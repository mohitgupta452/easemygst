package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.PartnerDTO;
import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface PartnerService {
	
	boolean storePartner(PartnerDTO partnerDTO) throws AppException;
	
	String submitPartnerCode(String code) throws AppException;
	
	public PartnerDTO validateOtp(String mobileOtp, String emailOtp, String email) throws AppException;
	
	public String resendOtp(String email, String mode) throws AppException;
	
}
