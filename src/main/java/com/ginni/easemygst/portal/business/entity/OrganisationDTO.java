package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

public @Data class OrganisationDTO implements Serializable {

	private static final long serialVersionUID = -1824878068632667530L;

	private Date activationDate;

	private Byte cardUpdate;
	
	private String billingAddress;

	private String code;

	private String customerId;

	private Integer id;

	private Date lastBilling;

	private String name;
	
	private String planCode;
	
	private String planDetails;
	
	private Integer invoiceCount;

	private Date nextBilling;
	
	private Date createdAt;
	
	private Date expiresAt;
	
	private String intervalUnits;
	
	private String discountAmount;
	
	private String totalAmount;
	
	private Date currentTermEndsAt;	

	private String salesPerson;
	
	private String salesPersonId;
	
	private Boolean autoCollect;
	
	private String status;

	private String shippingAddress;

	private String subscriptionId;

	private String subscriptionMode;

	private String subscriptionType;

	private String unitCost;

	private Integer units;
	
	private String gstin;
	
}
