package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

public @Data class TaxAmount implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal igst;
	
	private BigDecimal cgst;
	
	private BigDecimal sgst;
	
	private BigDecimal cess;
	
	private BigDecimal total;
	
	public TaxAmount(){
		this.igst = BigDecimal.ZERO;
		this.cgst = BigDecimal.ZERO;
		this.sgst = BigDecimal.ZERO;
		this.cess = BigDecimal.ZERO;
		this.total =BigDecimal.ZERO;
	}

	public TaxAmount(BigDecimal igst, BigDecimal cgst, BigDecimal sgst, BigDecimal cess) {
		super();
		this.igst = igst;
		this.cgst = cgst;
		this.sgst = sgst;
		this.cess = cess;
		this.total =this.igst.add(this.cgst.add(this.sgst.add(this.getCess())));
	}
	
}