package com.ginni.easemygst.portal.business.dto;


import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.business.entity.CommonMixin;

import lombok.Data;

@SuppressWarnings("restriction")
@JsonInclude(Include.NON_NULL)
public @Data class LedgerDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String gstin;

	private Date fromDate;
	
	private Date toDate;
	
	private String data;
	
	private String monthYear;

	
		
	public interface LedgerDtoMixIn extends CommonMixin {
		
	}
	
}
