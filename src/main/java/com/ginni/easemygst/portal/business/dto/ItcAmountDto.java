package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ItcAmountDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("total_itc_claim")
	private Double totalItcClaim = new Double("0.00");
	
	@JsonProperty("pending_itc_amt")
	private Double pendingItcAmt=0.0;
	
	@JsonProperty("claimed_itc_amt")
	private Double claimedItcAmt=0.0;
	
	@JsonProperty("not_claimed_itc_amt")
	private Double notClaimedItcAmt=0.0;
	
	@JsonProperty("pending_itc_transaction_wise")
	List<PendingItcAmt> pendingItcByTransactions;
	
	public static @Data class PendingItcAmt{
		
		@JsonProperty("transaction")
		private String  transaction;
		@JsonProperty("pending_itc_amt")
		private Double pendingItcAmt=0.0;
	}
	
}
