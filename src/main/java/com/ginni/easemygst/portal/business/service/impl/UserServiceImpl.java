package com.ginni.easemygst.portal.business.service.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.UserOrganisationDTO;
import com.ginni.easemygst.portal.business.entity.ContactUsDTO;
import com.ginni.easemygst.portal.business.entity.OrganisationDTO;
import com.ginni.easemygst.portal.business.entity.PartnerDTO;
import com.ginni.easemygst.portal.business.entity.UserContactDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.entity.UserGstinDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.business.service.UserService.UserStatus;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.notify.EmailFactory;
import com.ginni.easemygst.portal.persistence.entity.ActionLog;
import com.ginni.easemygst.portal.persistence.entity.ContactUs;
import com.ginni.easemygst.portal.persistence.entity.Debugmode;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.persistence.entity.UserContact;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.persistence.entity.UserLastlog;
import com.ginni.easemygst.portal.persistence.entity.UserTaxpayer;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.security.PasswordHash;
import com.ginni.easemygst.portal.subscription.SubscriptionService;
import com.ginni.easemygst.portal.utils.Utility;

@Transactional
@Stateless
public class UserServiceImpl implements UserService {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private UserBean userBean;

	@Inject
	private FinderService finderService;

	@Inject
	private SubscriptionService subscriptionService;
	
	@Inject
	private ApiLoggingService apiLoggingService;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	private final String password_pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";

	@Override
	public UserDTO createUser(UserDTO userDTO, RegisterUsing registerUsing) throws AppException {

		if (registerUsing == null) {
			log.error("Either User is Null or RegisterUsing is null");
			userDTO.setId(-1);
			return userDTO;
		}

		UserDTO retUserDto = new UserDTO();

		// create new easemygst Entity Object
		User user = (User) EntityHelper.convert(userDTO, User.class);

		if (user.getUserContacts() != null && !user.getUserContacts().isEmpty()) {
			for (UserContact userContact : user.getUserContacts()) {
				userContact.setUser(user);
			}
		}

		if (!Objects.isNull(user.getOrganisation())) {
			Organisation organisation = user.getOrganisation();
			organisation.setCode(UUID.randomUUID().toString());

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 14);

			organisation.setSubscriptionId(Utility.randomString(10) + "-" + "TRIAL-PERIOD");
			organisation.setUnits(20);
			organisation.setActivationDate(Calendar.getInstance().getTime());
			organisation.setCreatedAt(Calendar.getInstance().getTime());
			organisation.setExpiresAt(cal.getTime());
			organisation.setCurrentTermEndsAt(cal.getTime());
			organisation.setStatus("trial");
			organisation.setPlanCode("EMGSM");

			organisation.setCreationIpAddress(user.getCreationIpAddress());
			organisation.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
		}

		String userPass = null;
		// Generating Password only for SellerConnect
		if (registerUsing == RegisterUsing.EASEMYGST) {

			user.setActive((byte) 1);
			userPass = userDTO.getPassword();

			try {
				user.setPassword(PasswordHash.createHash(userPass));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				log.error("Error: {} while creating password Hash", e.getMessage());
			}
		}

		log.debug("Creating New User.");
		// user.setCreatedBy(userBean.getUserDto().getFirstName() + "_" +
		// userBean.getUserDto().getLastName());
		// user.setCreationIpAddress(userBean.getIpAddress());
		user.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
		user = crudService.create(user);

		boolean flag = subscriptionService.createCustomer(user);
		if (flag) {
			log.debug("cusomter successfully created in subscription module");
		} else {
			log.error("unable to create cusomter in subscription module");
		}

		if (user == null) {
			log.error("Unable to create new user");
			retUserDto.setId(-1);
			return retUserDto;
		}

		if (!StringUtils.isEmpty(user.getRequestId())) {
			Vendor vendor = finderService.findVendorByRequest(user.getRequestId());
			if (!Objects.isNull(vendor)) {
				vendor.setEmgst((byte) 1);
				vendor.setUpdatedBy(user.getFirstName() + "_" + user.getLastName());
				vendor.setUpdationIpAddress(user.getCreationIpAddress());
				vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
				crudService.update(vendor);
			}
		}

		Map<String, String> params = new HashMap<>();
		params.put("name", user.getFirstName());
		params.put("toEmail", user.getUserContacts().get(0).getEmailAddress());
		EmailFactory factory = new EmailFactory();
		factory.sendWelcomeUserEmail(params);
		log.info("Sending welcome email to new user {}.", params.toString());

		if (registerUsing == RegisterUsing.EASEMYGST) {

			Properties config = new Properties();
			try {
				config.load(DBUtils.class.getClassLoader().getResourceAsStream("config.properties"));
				log.info("config.properties loaded successfully");
			} catch (IOException e) {
				e.printStackTrace();
			}

			String BASE_URL = config.getProperty("main.app.url.email.verification");

			Map<String, String> activateParams = new HashMap<>();
			activateParams.put("name", user.getFirstName());
			activateParams.put("toEmail", user.getUserContacts().get(0).getEmailAddress());
			activateParams.put("username", user.getUsername());
			String rand = Utility.randomString(13);
			rand = rand.replaceAll("Sm", "sCm");
			rand += "Sm" + user.getId();
			activateParams.put("link", BASE_URL + "?reqid=" + rand + "&from="
					+ Base64.getEncoder().encode(user.getUsername().getBytes()).toString());
			factory.sendMailActivationEmail(activateParams);
			log.info("Sending activation email to new user {}.", activateParams.toString());
		}

		log.info("New user having userID = {} is created.", user.getId());

		retUserDto = (UserDTO) EntityHelper.convert(user, UserDTO.class);

		return retUserDto;
	}

	@Override
	public boolean isActiveIdentityExists(int id) {
		User user = finderService.getExistingUser(id, 1);
		if (user != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isIdentityExists(String username) {
		UserDTO user = finderService.getExistingUser(username, 1);
		if (user != null) {
			return true;
		}
		return false;
	}

	@Override
	public UserDTO getUserInfo(String username) {

		UserDTO user = finderService.getExistingUser(username, 1);

		if (!Objects.isNull(user)) {
			return user;
		}

		UserDTO userDTO = new UserDTO();
		userDTO.setId(-1);

		return userDTO;
	}

	@Override
	public UserDTO getUserProfileInfo(int id) {

		User userEm = em.find(User.class, id);

		UserDTO user = null;

		if (!Objects.isNull(userEm)) {
			user = new UserDTO();
			user.setId(userEm.getId());
			user.setFirstName(userEm.getFirstName());
			user.setLastName(userEm.getLastName());
			user.setRequestId(userEm.getRequestId());
			List<UserContactDTO> contactDTOs = new ArrayList<>();
			UserContactDTO contactDTO = new UserContactDTO();
			if (!Objects.isNull(userEm.getUserContacts())) {
				contactDTO.setEmailAddress(userEm.getUserContacts().get(0).getEmailAddress());
				contactDTO.setEmailAuth(userEm.getUserContacts().get(0).getEmailAuth());
				contactDTO.setMobileNumber(userEm.getUserContacts().get(0).getMobileNumber());
				contactDTO.setMobileAuth(userEm.getUserContacts().get(0).getMobileAuth());
				contactDTOs.add(contactDTO);
			}

			user.setUserContacts(contactDTOs);
			PartnerDTO partnerDTO = null;

			if (!Objects.isNull(userEm.getPartner())) {
				partnerDTO = new PartnerDTO();
				partnerDTO.setCity(userEm.getPartner().getCity());
				partnerDTO.setClientBase(userEm.getPartner().getClientBase());
				partnerDTO.setContactNo(userEm.getPartner().getContactNo());
				partnerDTO.setEmailAddress(userEm.getPartner().getEmailAddress());
				partnerDTO.setId(userEm.getPartner().getId());
				partnerDTO.setPartnerCode(userEm.getPartner().getPartnerCode());
				partnerDTO.setPartnerName(userEm.getPartner().getPartnerName());
				partnerDTO.setProfession(userEm.getPartner().getProfession());
			}

			if (!Objects.isNull(userEm.getOrganisation())) {
				OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper.convert(userEm.getOrganisation(),
						OrganisationDTO.class);
				user.setOrganisation(organisationDTO);
			}

			user.setPartner(partnerDTO);
		}

		if (!Objects.isNull(user)) {
			return user;
		}

		UserDTO userDTO = new UserDTO();
		userDTO.setId(-1);

		return userDTO;

	}

	@Override
	public UserDTO getUserProfileInfo(String username) {

		UserDTO user = getUserInfo(username);

		User userEm = em.find(User.class, user.getId());

		if (!Objects.isNull(userEm)) {
			user = new UserDTO();
			user.setId(userEm.getId());
			user.setFirstName(userEm.getFirstName());
			user.setLastName(userEm.getLastName());
			user.setRequestId(userEm.getRequestId());
			List<UserContactDTO> contactDTOs = new ArrayList<>();
			UserContactDTO contactDTO = new UserContactDTO();
			if (!Objects.isNull(userEm.getUserContacts())) {
				contactDTO.setEmailAddress(userEm.getUserContacts().get(0).getEmailAddress());
				contactDTO.setEmailAuth(userEm.getUserContacts().get(0).getEmailAuth());
				contactDTO.setMobileNumber(userEm.getUserContacts().get(0).getMobileNumber());
				contactDTO.setMobileAuth(userEm.getUserContacts().get(0).getMobileAuth());
				contactDTOs.add(contactDTO);
			}

			user.setUserContacts(contactDTOs);
			PartnerDTO partnerDTO = null;

			if (!Objects.isNull(userEm.getPartner())) {
				partnerDTO = new PartnerDTO();
				partnerDTO.setCity(userEm.getPartner().getCity());
				partnerDTO.setClientBase(userEm.getPartner().getClientBase());
				partnerDTO.setContactNo(userEm.getPartner().getContactNo());
				partnerDTO.setEmailAddress(userEm.getPartner().getEmailAddress());
				partnerDTO.setId(userEm.getPartner().getId());
				partnerDTO.setPartnerCode(userEm.getPartner().getPartnerCode());
				partnerDTO.setPartnerName(userEm.getPartner().getPartnerName());
				partnerDTO.setProfession(userEm.getPartner().getProfession());
			}

			if (!Objects.isNull(userEm.getOrganisation())) {
				OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper.convert(userEm.getOrganisation(),
						OrganisationDTO.class);
				user.setOrganisation(organisationDTO);
			}

			user.setPartner(partnerDTO);
		}

		if (!Objects.isNull(user)) {
			return user;
		}

		UserDTO userDTO = new UserDTO();
		userDTO.setId(-1);

		return userDTO;

	}

	@Override
	public Map<String, Object> getUserOrganisations(int id) throws AppException {

		Map<String, Object> output = new HashMap<>();
		List<Organisation> organisations = finderService.getDistinctUserGstinByUser(id);
		List<UserOrganisationDTO> organisationDTOs = new ArrayList<>();

		int myOrg = -1;
		User userEm = em.find(User.class, userBean.getUserDto().getId());
		Organisation myOrganisation = userEm.getOrganisation();
		if (Objects.isNull(userEm.getOrganisation())) {
			output.put("setup", true);
		} else {
			output.put("setup", false);
			myOrg = userEm.getOrganisation().getId();
		}

		if (!Objects.isNull(myOrganisation)) {
			if (!organisations.contains(myOrganisation)) {
				organisations.add(myOrganisation);
			}
		}

		if (!Objects.isNull(organisations)) {

			for (Organisation organisation : organisations) {
				UserOrganisationDTO dto = new UserOrganisationDTO();
				dto.setId(organisation.getId());
				dto.setName(organisation.getName());
				if (myOrg != -1 && organisation.getId().intValue() == myOrg) {
					dto.setYours(true);
					dto.setEmail(userEm.getUsername());
				} else {
					UserDTO userDTO = finderService.findUserByOrganisation(organisation.getId());
					if (!Objects.isNull(userDTO)) {
						dto.setEmail(userDTO.getUsername());
						dto.setYours(false);
					}
				}
				if (userBean.getUserDto().getOrganisation().getId().equals(organisation.getId())) {
					dto.setActive(true);
				} else {
					dto.setActive(false);
					organisationDTOs.add(dto);
				}
			}
			output.put("orgs", organisationDTOs);
		} else {
			output.put("orgs", new ArrayList<>());
		}

		return output;
	}

	@Override
	public UserDTO activateUserEmail(int id) {
		User user = new User();
		user = finderService.getExistingUser(id, 1);

		UserDTO userDTO = new UserDTO();
		userDTO.setId(-1);

		if (user != null) {
			user.setActive((byte) 1);
			user.getUserContacts().get(0).setEmailAuth((byte) 1);
			user.setUpdationIpAddress(userBean.getIpAddress());
			user.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			crudService.update(user);
			log.debug("User id {} activated successfully", id);
			userDTO = (UserDTO) EntityHelper.convert(user, UserDTO.class);
		}

		return userDTO;
	}

	@Override
	public boolean changePassword(String identity, String oldPass, String newPass) throws AppException {

		UserDTO userDTO = finderService.getExistingUser(identity, 1);
		if (userDTO != null) {

			try {

				if (PasswordHash.validatePassword(oldPass, userDTO.getPassword())) {

					final Pattern pattern1 = Pattern.compile(password_pattern);
					if (!pattern1.matcher(newPass).matches()) {
						throw new AppException(ExceptionCode._INVALID_PASSWORD);
					}

					User user = em.getReference(User.class, userDTO.getId());
					try {
						user.setPassword(PasswordHash.createHash(newPass));
					} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
						e.printStackTrace();
					}

					user.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					user.setUpdationIpAddress(userBean.getIpAddress());
					user.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(user);

					UserContact userContact = finderService.getUserContact(userDTO.getId());
					if (!Objects.isNull(userContact)) {
						Map<String, String> params1 = new HashMap<>();
						params1.put("name", user.getFirstName() + " " + user.getLastName());
						params1.put("email", userContact.getEmailAddress());
						params1.put("toEmail", userContact.getEmailAddress());
						EmailFactory factory1 = new EmailFactory();
						factory1.sendPasswordChangedEmail(params1);
						log.info("Sending password changed email to {}.", params1.toString());
					}

					return true;
				} else {
					log.debug("current password is wrong for user {}", identity);
					throw new AppException(ExceptionCode._PASSWORD_NOT_MATCH);
				}
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
		} else {
			log.debug("user {} not exist in system", identity);
			throw new AppException(ExceptionCode._USER_NOT_FOUND);
		}

		return false;
	}

	@Override
	public boolean resetPassword(String identity, String newPass) throws AppException {

		UserDTO userDTO = finderService.getExistingUser(identity, 1);
		if (userDTO != null) {

			User user = em.getReference(User.class, userDTO.getId());
			try {
				user.setPassword(PasswordHash.createHash(newPass));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
			user.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			user.setUpdationIpAddress(userBean.getIpAddress());
			user.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			crudService.update(user);
			return true;
		} else {
			log.debug("user {} not exist in system", identity);
		}

		return false;
	}

	@Override
	public boolean resetPassword(String identity, String newPass, String requestedBy) throws AppException {

		UserDTO userDTO = finderService.getExistingUser(identity, 1);
		if (userDTO != null) {

			User user = em.getReference(User.class, userDTO.getId());
			try {
				user.setPassword(PasswordHash.createHash(newPass));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
			user.setUpdatedBy(requestedBy);
			user.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			crudService.update(user);
			return true;
		} else {
			log.debug("user {} not exist in system", identity);
		}

		return false;
	}

	@Override
	public String forgotPassword(String identity, String ipAddress) throws AppException {

		if (isIdentityExists(identity)) {
			UserDTO userDTO = finderService.getExistingUser(identity, 1);
			UserContact userContact = finderService.getUserContact(userDTO.getId());
			if (!Objects.isNull(userContact)) {
				return userContact.getMobileNumber();
			} else
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		log.debug("no such user exist.");
		throw new AppException(ExceptionCode._USER_NOT_FOUND);

	}

	@Override
	public boolean saveContactUsData(ContactUsDTO contactUsDTO) throws AppException {

		if (!Objects.isNull(contactUsDTO)) {

			ContactUs contactUs = (ContactUs) EntityHelper.convert(contactUsDTO, ContactUs.class);
			contactUs.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			crudService.create(contactUs);

			Map<String, String> params1 = new HashMap<>();
			params1.put("title", contactUs.getTitle());
			params1.put("firstName", contactUs.getFirstName());
			params1.put("lastName", contactUs.getLastName());
			params1.put("fullName", contactUs.getFullName());
			params1.put("emailAddress", contactUs.getEmailAddress());
			params1.put("mobileNumber", contactUs.getMobileNumber());
			params1.put("message", contactUs.getMessage());
			params1.put("remark", contactUs.getRemark());
			params1.put("state", contactUs.getState());
			params1.put("city", contactUs.getCity());
			params1.put("legalName", contactUs.getLegalName());
			params1.put("annualTurnover", contactUs.getAnnualTurnover());
			params1.put("source",contactUsDTO.getSource());
			if (!StringUtils.isEmpty(contactUs.getTitle()) && (contactUs.getTitle().contains("CONTACT_US") || contactUs.getTitle().contains("REQUEST_A_DEMO"))) {
				params1.put("ccEmail", "emgst@turningcloud.com");
			}
			params1.put("toEmail", "sales@easemygst.com");
			EmailFactory factory1 = new EmailFactory();
			factory1.sendContactUsFormEmail(params1);
			log.info("Sending contact us form email to sales@easemygst.com {}.", params1.toString());

			return true;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public UserDTO validateUser(String username, String password, RegisterUsing registerUsing) throws AppException {
		// TODO Auto-generated method stub

		String debuguser = "";
		String debugpass = "";

		long lastTime = 0;

		boolean debugMode = false;
		if (username.toLowerCase().contains("+")) {
			debuguser = username.substring(username.indexOf("+") + 1, username.indexOf("@"));
			debugpass = password;
			username = username.replace('+', ';');
			username = username.replaceAll(";" + debuguser, "");
			debugMode = true;
		}

		UserDTO user = new UserDTO();
		user = finderService.getExistingUser(username, 1);

		try {
			// for google authenticated users.
			if (user != null) {
				user.setActive((byte) 1);
				if (registerUsing.equals(RegisterUsing.GOOGLE)) {
					return user;
				} else {
					List<UserLastlog> lastlogs = finderService.getUserLastLogs(user.getId());
					if (Objects.isNull(lastlogs)) {
						lastlogs = new ArrayList<>();
					} else if (!lastlogs.isEmpty()) {
						lastTime = lastlogs.get(lastlogs.size() - 1).getLastLogin()
								.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
					} else {
						lastTime = Timestamp.from(Instant.now()).getTime();
					}

					Integer myOrgId = null;
					User userEm = em.getReference(User.class, user.getId());

					UserLastlog userLastlog = new UserLastlog();
					userLastlog.setUser(userEm);
					userLastlog.setLastLogin(LocalDateTime.now());
					userLastlog.setLastLoginIpAddress(userBean.getIpAddress());

					UserContact userContact = finderService.getUserContact(user.getId());
					UserContactDTO contactDTO = (UserContactDTO) EntityHelper.convert(userContact,
							UserContactDTO.class);
					List<UserContactDTO> contactDTOs = new ArrayList<>();
					contactDTOs.add(contactDTO);
					user.setUserContacts(contactDTOs);

					List<UserGstin> gstins = null;
					if (!Objects.isNull(userEm.getOrganisation())) {

						if (StringUtils.isEmpty(userEm.getOrganisation().getCustomerId())) {
							boolean flag = subscriptionService.createCustomer(userEm);
							if (flag) {
								log.debug("cusomter successfully created in subscription module");
							} else {
								log.error("unable to create cusomter in subscription module");
							}
						}

						OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper
								.convert(userEm.getOrganisation(), OrganisationDTO.class);
						user.setOrganisation(organisationDTO);
						myOrgId = organisationDTO.getId();

						if (!StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {
							user.getOrganisation().setCode(user.getOrganisation().getPlanCode() != null
									? (user.getOrganisation().getPlanCode().startsWith("EMGE") ? "E" : "S") : "noplan");
						} else {
							user.getOrganisation().setCode("noplan");
						}
						user.getOrganisation().setPlanCode(user.getUsername());

						if (!Objects.isNull(user.getOrganisation())
								&& !StringUtils.isEmpty(userEm.getOrganisation().getStatus())
								&& userEm.getOrganisation().getStatus().equalsIgnoreCase("trial")) {
							Date todaysDate = Calendar.getInstance().getTime();
							int diffInDays = (int) ((userEm.getOrganisation().getExpiresAt().getTime()
									- todaysDate.getTime()) / (1000 * 60 * 60 * 24));
							user.getOrganisation().setSubscriptionMode("yes");
							user.getOrganisation().setUnits(diffInDays + 1);
						}

					} else {
						gstins = finderService.getUserGstin(user.getId());
						for (UserGstin userGstin : gstins) {
							if (!Objects.isNull(userGstin.getOrganisation())) {
								OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper
										.convert(userGstin.getOrganisation(), OrganisationDTO.class);
								user.setOrganisation(organisationDTO);

								UserDTO orgUser = finderService.findUserByOrganisation(organisationDTO.getId());
								if (!StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {
									user.getOrganisation().setCode(user.getOrganisation().getPlanCode() != null
											? (user.getOrganisation().getPlanCode().startsWith("EMGE") ? "E" : "S")
											: "noplan");
								} else {
									user.getOrganisation().setCode("noplan");
								}
								user.getOrganisation().setPlanCode(orgUser.getUsername());

								break;
							}
						}

					}

					if (Objects.isNull(user.getOrganisation())) {
						Organisation organisation = new Organisation();
						organisation.setCode(UUID.randomUUID().toString());
						organisation.setName("Primary");
						organisation.setCreationTime(Timestamp.from(Instant.now()));

						userEm.setOrganisation(organisation);
						userEm = crudService.update(userEm);

						if (!Objects.isNull(gstins)) {
							for (UserGstin userGstin : gstins) {
								if (Objects.isNull(userGstin.getGstReturnBean())) {
									Integer gstinId = userGstin.getTaxpayerGstin().getId();
									List<UserGstin> userGstins = finderService.findUserGstinByTaxpayer(gstinId);
									for (UserGstin userGstin2 : userGstins) {
										userGstin2.setOrganisation(userEm.getOrganisation());
										crudService.update(userGstin2);
									}
								}
							}
						}

						boolean flag = subscriptionService.createCustomer(userEm);
						if (flag) {
							log.debug("cusomter successfully created in subscription module");
						} else {
							log.error("unable to create cusomter in subscription module");
						}

						OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper
								.convert(userEm.getOrganisation(), OrganisationDTO.class);
						user.setOrganisation(organisationDTO);
						myOrgId = organisationDTO.getId();

						if (!StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {
							user.getOrganisation().setCode(user.getOrganisation().getPlanCode() != null
									? (user.getOrganisation().getPlanCode().startsWith("EMGE") ? "E" : "S") : "noplan");
						} else {
							user.getOrganisation().setCode("noplan");
						}
						user.getOrganisation().setPlanCode(user.getUsername());
					}

					if (!Objects.isNull(user.getOrganisation().getExpiresAt())) {
						if (user.getOrganisation().getExpiresAt().before(Calendar.getInstance().getTime())) {
							if (user.getOrganisation().getStatus().equalsIgnoreCase("trial")
									&& user.getOrganisation().getId().equals(myOrgId)) {
								user.getOrganisation().setSubscriptionMode("yes");
								user.getOrganisation().setUnits(-1);
							} else {
								throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
							}
						}
					}
					if (!Objects.isNull(user.getOrganisation().getNextBilling())) {
						if (user.getOrganisation().getNextBilling().before(Calendar.getInstance().getTime())
								|| user.getOrganisation().getNextBilling().equals(Calendar.getInstance().getTime())) {
							if (user.getOrganisation().getStatus().equalsIgnoreCase("trial")
									&& user.getOrganisation().getId().equals(myOrgId)) {
								user.getOrganisation().setSubscriptionMode("yes");
								user.getOrganisation().setUnits(-1);
							} else {
								throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
							}
						}
					}

					if (user.getOrganisation().getId().equals(myOrgId)) {
						user.getOrganisation().setStatus("true");
					} else {
						user.getOrganisation().setStatus("false");
					}

					if (debugMode) {
						Debugmode debugmodes = finderService.findDebugmodeByUsernameAndPassword(debuguser, debugpass);

						if (!Objects.isNull(debugmodes)) {
							log.info("debug mode on  for user {} authenticated successfully", username);
							crudService.create(userLastlog);
							user.setPassword(lastTime + "");
							return user;
						}
					} else if (PasswordHash.validatePassword(password, user.getPassword())) {
						log.info("user {} authenticated successfully", username);
						crudService.create(userLastlog);
						user.setPassword(lastTime + "");
						return user;
					}

					log.info("Invalid password for user {}", username);
					throw new AppException(ExceptionCode._INVALID_CRDENTIALS);
				}
			} else {
				user = finderService.getExistingUser(username);
				if (!Objects.isNull(user)) {
					user.setActive((byte) 0);
					return user;
				}
			}
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}

		log.debug("{} Doesn't Exist.", username);
		throw new AppException(ExceptionCode._USER_NOT_FOUND);
	}

	@Override
	public UserDTO secureValidateUser(String username, String password, RegisterUsing registerUsing)
			throws AppException {
		// TODO Auto-generated method stub

		long lastTime = 0;

		UserDTO user = new UserDTO();
		user = finderService.getExistingUser(username, 1);

		try {
			// for google authenticated users.
			if (user != null) {
				user.setActive((byte) 1);
				if (registerUsing.equals(RegisterUsing.GOOGLE)) {
					return user;
				} else {
					List<UserLastlog> lastlogs = finderService.getUserLastLogs(user.getId());
					if (Objects.isNull(lastlogs)) {
						lastlogs = new ArrayList<>();
					} else if (!lastlogs.isEmpty()) {
						lastTime = lastlogs.get(lastlogs.size() - 1).getLastLogin().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();;
					} else {
						lastTime = Timestamp.from(Instant.now()).getTime();
					}

					UserLastlog userLastlog = new UserLastlog();
					userLastlog.setUser(em.getReference(User.class, user.getId()));
					userLastlog.setLastLogin(LocalDateTime.now());
					userLastlog.setLastLoginIpAddress(userBean.getIpAddress());

					UserContact userContact = finderService.getUserContact(user.getId());
					UserContactDTO contactDTO = (UserContactDTO) EntityHelper.convert(userContact,
							UserContactDTO.class);
					List<UserContactDTO> contactDTOs = new ArrayList<>();
					contactDTOs.add(contactDTO);
					user.setUserContacts(contactDTOs);

					if (PasswordHash.validatePassword(password, user.getPassword())) {
						log.info("user {} authenticated successfully", username);
						crudService.create(userLastlog);
						user.setPassword(lastTime + "");
						return user;
					}

					log.info("Invalid password for user {}", username);
					throw new AppException(ExceptionCode._INVALID_CRDENTIALS);
				}
			} else {
				user = finderService.getExistingUser(username);
				if (!Objects.isNull(user)) {
					user.setActive((byte) 0);
					return user;
				}
			}
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}

		log.debug("{} Doesn't Exist.", username);
		throw new AppException(ExceptionCode._USER_NOT_FOUND);
	}

	@Override
	public UserDTO verifyActivateAccount(String username, String firstName, String lastName, String mobileNumber,
			String password) throws AppException {

		UserDTO userDTO = new UserDTO();
		userDTO = finderService.getExistingUser(username);

		if (userDTO != null) {
			UserContact userContact = finderService.getUserContact(userDTO.getId());
			userContact.setMobileNumber(mobileNumber);
			userContact.setMobileAuth((byte) 1);
			userContact.setUpdationIpAddress(userBean.getIpAddress());
			userContact.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			crudService.update(userContact);

			User user = em.getReference(User.class, userDTO.getId());
			user.setFirstName(firstName);
			user.setLastName(lastName);

			String userPass = password;

			try {
				user.setPassword(PasswordHash.createHash(userPass));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				log.error("Error: {} while creating password Hash", e.getMessage());
			}

			user.setActive((byte) 1);
			user.setStatus(UserStatus.ACTIVE.toString());
			user.setUpdationIpAddress(userBean.getIpAddress());
			user.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			user = crudService.update(user);

			long lastTime = Timestamp.from(Instant.now()).getTime();
			UserLastlog userLastlog = new UserLastlog();
			userLastlog.setUser(em.getReference(User.class, user.getId()));
			userLastlog.setLastLogin(LocalDateTime.now());
			userLastlog.setLastLoginIpAddress(userBean.getIpAddress());
			crudService.create(userLastlog);

			userDTO.setFirstName(firstName);
			userDTO.setLastName(lastName);
			userDTO.setActive((byte) 1);
			userDTO.setPassword(lastTime + "");

			List<UserGstin> gstins = null;
			gstins = finderService.getUserGstin(user.getId());
			boolean flag = false;
			for (UserGstin userGstin : gstins) {
				if (!Objects.isNull(userGstin.getOrganisation())) {
					flag = true;
					OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper
							.convert(userGstin.getOrganisation(), OrganisationDTO.class);
					userDTO.setOrganisation(organisationDTO);

					UserDTO orgUser = finderService.findUserByOrganisation(organisationDTO.getId());
					if (!StringUtils.isEmpty(userDTO.getOrganisation().getSubscriptionId())) {
						userDTO.getOrganisation().setCode(userDTO.getOrganisation().getPlanCode() != null
								? (userDTO.getOrganisation().getPlanCode().startsWith("EMGE") ? "E" : "S") : "noplan");
					} else {
						userDTO.getOrganisation().setCode("noplan");
					}

					userDTO.getOrganisation().setPlanCode(orgUser.getUsername());

					userDTO.getOrganisation().setStatus("false");

					if (!Objects.isNull(userDTO.getOrganisation().getExpiresAt())) {
						if (userDTO.getOrganisation().getExpiresAt().before(Calendar.getInstance().getTime())) {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}
					if (!Objects.isNull(userDTO.getOrganisation().getNextBilling())) {
						if (userDTO.getOrganisation().getNextBilling().before(Calendar.getInstance().getTime())
								|| userDTO.getOrganisation().getNextBilling()
										.equals(Calendar.getInstance().getTime())) {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}

					break;
				}
			}

			if (!flag) {
				if (!Objects.isNull(user.getOrganisation())) {

					if (StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
						boolean flag1 = subscriptionService.createCustomer(user);
						if (flag1) {
							log.debug("cusomter successfully created in subscription module");
						} else {
							log.error("unable to create cusomter in subscription module");
						}
					}

					Organisation organisation = user.getOrganisation();
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, 14);

					organisation.setSubscriptionId(Utility.randomString(10) + "-" + "TRIAL-PERIOD");
					organisation.setUnits(20);
					organisation.setActivationDate(Calendar.getInstance().getTime());
					organisation.setCreatedAt(Calendar.getInstance().getTime());
					organisation.setExpiresAt(cal.getTime());
					organisation.setCurrentTermEndsAt(cal.getTime());
					organisation.setStatus("trial");
					organisation.setPlanCode("EMGSM");

					organisation.setCreationIpAddress(user.getCreationIpAddress());
					organisation.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
					organisation = crudService.update(organisation);

					OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper.convert(organisation,
							OrganisationDTO.class);
					userDTO.setOrganisation(organisationDTO);

					if (!StringUtils.isEmpty(organisation.getSubscriptionId())) {
						userDTO.getOrganisation().setCode(organisation.getPlanCode() != null
								? (organisation.getPlanCode().startsWith("EMGE") ? "E" : "S") : "noplan");
					} else {
						userDTO.getOrganisation().setCode("noplan");
					}
					userDTO.getOrganisation().setPlanCode(user.getUsername());

					if (!Objects.isNull(organisation) && !StringUtils.isEmpty(organisation.getStatus())
							&& organisation.getStatus().equalsIgnoreCase("trial")) {
						Date todaysDate = Calendar.getInstance().getTime();
						int diffInDays = (int) ((organisation.getExpiresAt().getTime() - todaysDate.getTime())
								/ (1000 * 60 * 60 * 24));
						userDTO.getOrganisation().setSubscriptionMode("yes");
						userDTO.getOrganisation().setUnits(diffInDays + 1);
					}

					userDTO.getOrganisation().setStatus("true");

				}
			}

			return userDTO;
		}

		log.debug("{} Doesn't Exist.", username);
		throw new AppException(ExceptionCode._USER_NOT_FOUND);
	}

	@Override
	public boolean createOrganisation(String name) throws AppException {

		if (!Objects.isNull(userBean.getUserDto()) && !Objects.isNull(userBean.getUserDto().getId())) {

			User user = em.find(User.class, userBean.getUserDto().getId());

			if (!Objects.isNull(user.getOrganisation())) {
				throw new AppException(ExceptionCode._DUPLICATE_ORGANISATION);
			}

			Organisation organisation = new Organisation();
			organisation.setCode(UUID.randomUUID().toString());
			organisation.setName(name);
			organisation.setCreatedBy(userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
			organisation.setCreationIpAddress(userBean.getIpAddress());
			organisation.setCreationTime(Timestamp.from(Instant.now()));

			user.setOrganisation(organisation);

			user = crudService.update(user);

			List<UserGstin> gstins = finderService.getUserGstin(user.getId());
			if (!Objects.isNull(gstins)) {
				for (UserGstin userGstin : gstins) {
					if (Objects.isNull(userGstin.getGstReturnBean())) {
						Integer gstinId = userGstin.getTaxpayerGstin().getId();
						List<UserGstin> userGstins = finderService.findUserGstinByTaxpayer(gstinId);
						for (UserGstin userGstin2 : userGstins) {
							userGstin2.setOrganisation(user.getOrganisation());
							crudService.update(userGstin2);
						}
					}
				}
			}

			boolean flag = subscriptionService.createCustomer(user);
			if (flag) {
				log.debug("cusomter successfully created in subscription module");
			} else {
				log.error("unable to create cusomter in subscription module");
			}

			return Boolean.TRUE;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean renameOrganisation(String name) throws AppException {

		if (!Objects.isNull(userBean.getUserDto()) && !Objects.isNull(userBean.getUserDto().getId())) {

			User user = em.find(User.class, userBean.getUserDto().getId());

			if (!Objects.isNull(user.getOrganisation())) {
				Organisation organisation = user.getOrganisation();
				organisation.setName(name);
				organisation
						.setUpdatedBy(userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
				organisation.setUpdationIpAddress(userBean.getIpAddress());
				organisation.setUpdationTime(Timestamp.from(Instant.now()));

				crudService.update(user);

				return Boolean.TRUE;
			}

			return Boolean.FALSE;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public UserDTO checkValidOrganisation(Integer organisationId) throws AppException {

		if (!Objects.isNull(userBean.getUserDto()) && !Objects.isNull(userBean.getUserDto().getId())) {

			User user = em.find(User.class, userBean.getUserDto().getId());
			int myOrgId = user.getOrganisation().getId();
			List<UserGstin> userGstins = finderService.getUserGstinByUserOrg(user.getId(), organisationId);

			if (!Objects.isNull(userGstins) && !userGstins.isEmpty()) {
				UserDTO userDTO = userBean.getUserDto();

				Organisation org = em.find(Organisation.class, organisationId);

				OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper.convert(org, OrganisationDTO.class);
				userDTO.setOrganisation(organisationDTO);

				UserDTO orgUser = finderService.findUserByOrganisation(organisationId);
				if (!StringUtils.isEmpty(userDTO.getOrganisation().getSubscriptionId())) {
					userDTO.getOrganisation().setCode(userDTO.getOrganisation().getPlanCode() != null
							? (userDTO.getOrganisation().getPlanCode().startsWith("EMGE") ? "E" : "S") : "noplan");
				} else {
					userDTO.getOrganisation().setCode("noplan");
				}
				userDTO.getOrganisation().setPlanCode(orgUser.getUsername());

				if (userDTO.getOrganisation().getId().equals(myOrgId)) {
					userDTO.getOrganisation().setStatus("true");
				} else {
					userDTO.getOrganisation().setStatus("false");
				}

				if (org.getStatus().equalsIgnoreCase("trial")) {
					Date todaysDate = Calendar.getInstance().getTime();
					int diffInDays = (int) ((org.getExpiresAt().getTime() - todaysDate.getTime())
							/ (1000 * 60 * 60 * 24));
					userDTO.getOrganisation().setSubscriptionMode("yes");
					userDTO.getOrganisation().setUnits(diffInDays + 1);
				}

				if (!Objects.isNull(userDTO.getOrganisation().getExpiresAt())) {
					if (userDTO.getOrganisation().getExpiresAt().before(Calendar.getInstance().getTime())) {
						if (org.getStatus().equalsIgnoreCase("trial")
								&& userDTO.getOrganisation().getId().equals(myOrgId)) {
							userDTO.getOrganisation().setSubscriptionMode("yes");
							userDTO.getOrganisation().setUnits(-1);
						} else {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}
				}
				if (!Objects.isNull(userDTO.getOrganisation().getNextBilling())) {
					if (userDTO.getOrganisation().getNextBilling().before(Calendar.getInstance().getTime())
							|| userDTO.getOrganisation().getNextBilling().equals(Calendar.getInstance().getTime())) {
						if (org.getStatus().equalsIgnoreCase("trial")
								&& userDTO.getOrganisation().getId().equals(myOrgId)) {
							userDTO.getOrganisation().setSubscriptionMode("yes");
							userDTO.getOrganisation().setUnits(-1);
						} else {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}
				}

				return userDTO;
			} else {
				if (!Objects.isNull(user.getOrganisation())) {
					if (user.getOrganisation().getId().equals(organisationId)) {
						UserDTO userDTO = userBean.getUserDto();

						Organisation org = em.find(Organisation.class, organisationId);

						OrganisationDTO organisationDTO = (OrganisationDTO) EntityHelper.convert(org,
								OrganisationDTO.class);
						userDTO.setOrganisation(organisationDTO);

						UserDTO orgUser = finderService.findUserByOrganisation(organisationId);
						if (!StringUtils.isEmpty(userDTO.getOrganisation().getSubscriptionId())) {
							userDTO.getOrganisation()
									.setCode(userDTO.getOrganisation().getPlanCode() != null
											? (userDTO.getOrganisation().getPlanCode().startsWith("EMGE") ? "E" : "S")
											: "noplan");
						} else {
							userDTO.getOrganisation().setCode("noplan");
						}
						userDTO.getOrganisation().setPlanCode(orgUser.getUsername());

						if (userDTO.getOrganisation().getId().equals(myOrgId)) {
							userDTO.getOrganisation().setStatus("true");
						} else {
							userDTO.getOrganisation().setStatus("false");
						}

						if (org.getStatus().equalsIgnoreCase("trial")) {
							Date todaysDate = Calendar.getInstance().getTime();
							int diffInDays = (int) ((org.getExpiresAt().getTime() - todaysDate.getTime())
									/ (1000 * 60 * 60 * 24));
							userDTO.getOrganisation().setSubscriptionMode("yes");
							userDTO.getOrganisation().setUnits(diffInDays + 1);
						}

						if (!Objects.isNull(userDTO.getOrganisation().getExpiresAt())) {
							if (userDTO.getOrganisation().getExpiresAt().before(Calendar.getInstance().getTime())) {
								if (org.getStatus().equalsIgnoreCase("trial")
										&& userDTO.getOrganisation().getId().equals(myOrgId)) {
									userDTO.getOrganisation().setName("yes");
									userDTO.getOrganisation().setUnits(-1);
								} else {
									throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
								}
							}
						}
						if (!Objects.isNull(userDTO.getOrganisation().getNextBilling())) {
							if (userDTO.getOrganisation().getNextBilling().before(Calendar.getInstance().getTime())
									|| userDTO.getOrganisation().getNextBilling()
											.equals(Calendar.getInstance().getTime())) {
								if (org.getStatus().equalsIgnoreCase("trial")
										&& userDTO.getOrganisation().getId().equals(myOrgId)) {
									userDTO.getOrganisation().setSubscriptionMode("yes");
									userDTO.getOrganisation().setUnits(-1);
								} else {
									throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
								}
							}
						}

						return userDTO;
					}
				}
			}

			return null;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean checkGstins() throws AppException {

		boolean subscriptionRequired = true;

		User user = em.find(User.class, userBean.getUserDto().getId());

		if (!Objects.isNull(user.getOrganisation())) {
			if (!StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {
				Integer units = user.getOrganisation().getUnits();
				if (!Objects.isNull(units)) {
					BigInteger gstinCount = (BigInteger) em.createNativeQuery(
							String.format("select count(distinct gstinId) from user_gstin where organisationId = %s",
									user.getOrganisation().getId()))
							.getSingleResult();

					if (!Objects.isNull(user.getOrganisation().getExpiresAt())) {
						if (user.getOrganisation().getExpiresAt().before(Calendar.getInstance().getTime())
								|| user.getOrganisation().getExpiresAt().equals(Calendar.getInstance().getTime())) {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}
					if (!Objects.isNull(user.getOrganisation().getNextBilling())) {
						if (user.getOrganisation().getNextBilling().before(Calendar.getInstance().getTime())
								|| user.getOrganisation().getNextBilling().equals(Calendar.getInstance().getTime())) {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}

					if (units > gstinCount.intValue()) {
						subscriptionRequired = false;
					}
				}
			}
		}

		if (subscriptionRequired)
			throw new AppException(ExceptionCode._SUBSCRIPTION_QUANTITY_REQUIRED);

		return Boolean.TRUE;
	}

	@Override
	public boolean checkUsers() throws AppException {

		boolean subscriptionRequired = true;

		User user = em.find(User.class, userBean.getUserDto().getId());

		if (!Objects.isNull(user.getOrganisation())) {
			if (!StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {
				Integer units = user.getOrganisation().getUnits();
				if (!Objects.isNull(units)) {
					BigInteger userCount = (BigInteger) em.createNativeQuery(
							String.format("select count(distinct userId) from user_gstin where organisationId = %s",
									user.getOrganisation().getId()))
							.getSingleResult();

					if (!Objects.isNull(user.getOrganisation().getExpiresAt())) {
						if (user.getOrganisation().getExpiresAt().before(Calendar.getInstance().getTime())
								|| user.getOrganisation().getExpiresAt().equals(Calendar.getInstance().getTime())) {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}
					if (!Objects.isNull(user.getOrganisation().getNextBilling())) {
						if (user.getOrganisation().getNextBilling().before(Calendar.getInstance().getTime())
								|| user.getOrganisation().getNextBilling().equals(Calendar.getInstance().getTime())) {
							throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
						}
					}

					if (units > userCount.intValue() - 1) {
						subscriptionRequired = false;
					}
				}
			}
		}

		if (subscriptionRequired)
			throw new AppException(ExceptionCode._SUBSCRIPTION_QUANTITY_REQUIRED);

		return Boolean.TRUE;
	}

	@Override
	public boolean createUserAccount(String username, String name, String legalName) throws AppException {

		if (!Utility.validateEmail(username)) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		UserDTO user = finderService.getExistingUser(username);
		if (!Objects.isNull(user)) {
			throw new AppException(ExceptionCode._DUPLICATE_USER);
		}

		User user1 = new User();
		user1.setUsername(username);
		user1.setActive((byte) 0);
		user1.setFirstName("Pending");
		user1.setLastName("Confirmation");
		user1.setStatus(UserStatus.INACTIVE.toString());
		String randomPass = Utility.randomString(10);
		try {
			user1.setPassword(PasswordHash.createHash(randomPass));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new AppException();
		}

		Organisation organisation = new Organisation();
		organisation.setCode(UUID.randomUUID().toString());
		organisation.setName(legalName);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 14);

		organisation.setSubscriptionId(Utility.randomString(10) + "-" + "TRIAL-PERIOD");
		organisation.setUnits(20);
		organisation.setActivationDate(Calendar.getInstance().getTime());
		organisation.setCreatedAt(Calendar.getInstance().getTime());
		organisation.setExpiresAt(cal.getTime());
		organisation.setCurrentTermEndsAt(cal.getTime());
		organisation.setStatus("trial");
		organisation.setPlanCode("EMGSM");

		organisation.setCreationIpAddress(userBean.getIpAddress());
		organisation.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
		user1.setOrganisation(organisation);

		UserContact userContact = new UserContact();
		userContact.setEmailAddress(username);
		userContact.setMobileAuth((byte) 0);
		userContact.setMobileNumber("");
		userContact.setUser(user1);
		if (!Objects.isNull(userBean.getUserDto())) {
			userContact.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
		}
		userContact.setCreationIpAddress(userBean.getIpAddress());
		userContact.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
		List<UserContact> contacts = new ArrayList<>();
		contacts.add(userContact);
		user1.setUserContacts(contacts);
		if (!Objects.isNull(userBean.getUserDto())) {
			user1.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
		}
		user1.setCreationIpAddress(userBean.getIpAddress());
		user1.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
		user1 = crudService.create(user1);

		Map<String, String> params = new HashMap<>();
		params.put("toEmail", username);
		params.put("username", username);
		params.put("password", randomPass);
		params.put("company", legalName);
		params.put("name", name);
		EmailFactory factory = new EmailFactory();
		factory.sendNewUserAccountForActivationEmail(params);
		log.info("Sending new user for activation email to user {}.", params.toString());

		return Boolean.TRUE;
	}
	
	public boolean isLastestSession(String requestIpAddress) {
		
           		
		
		return false;
		
	}

	@Override
	public List<String> getGstinbyPanCard(String panCard) throws AppException {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<String> gstinList = em.createNamedQuery("TaxpayerGstin.findByPan").setParameter("pan", panCard)
				.getResultList();

		return gstinList;
	}
	
	public String findUserType(String GSTIN, int userId) {

	
		
		UserGstin userGstin=null;
		try{
		   userGstin = em.createNamedQuery("UserGstin.findtypeprimary",UserGstin.class).setParameter("gstin",GSTIN).setParameter("uid",userId).getSingleResult();
		   log.debug(userGstin.toString());
		}
			catch(NoResultException e){
				
				return  "SECONDARY";	
				}

				return userGstin.getType().equalsIgnoreCase("FILE")?"PRIMARY":"SECONDARY";
		
/*		List<TaxpayerGstin> TaxpayerGstin = em.createNamedQuery("TaxpayerGstin.findByGstin")
				.setParameter("gstin", GSTIN).getResultList();
		int taxpayerId = TaxpayerGstin.get(0).getTaxpayer().getId();

		List<UserTaxpayer> UserTaxpayer = em.createNamedQuery("UserTaxpayer.findtype").setParameter("uid", userId)
				.setParameter("tid", taxpayerId).getResultList();*/

/*		return UserTaxpayer.get(0).getType();*/
//UserGstin.findUsertype
	}
	
	@Override
	public String findAccessType(String GSTIN, int userId,String returntype){
		if(StringUtils.isEmpty(returntype)){
			returntype="gstr1";
		}
		
		if(returntype.equalsIgnoreCase("gstr2a")){
			returntype="gstr2";
		}
		
		UserGstin userGstin=null;
		try{
    userGstin = em.createNamedQuery("UserGstin.findUsertype",UserGstin.class)
				.setParameter("gstin",GSTIN).setParameter("uid",userId)
				.setParameter("rtype",returntype).getSingleResult();
		}
		catch(NoResultException e){
			
		return  "";	
		}

		return userGstin.getType();
	}
	

}
