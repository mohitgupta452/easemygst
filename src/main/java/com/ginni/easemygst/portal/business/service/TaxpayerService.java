package com.ginni.easemygst.portal.business.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import javax.ejb.Local;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.TinDTO;
import com.ginni.easemygst.portal.business.dto.InvoiceVendorDetailsDto;
import com.ginni.easemygst.portal.business.dto.MonthSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ResponseDTO;
import com.ginni.easemygst.portal.business.entity.BillingAddressDTO;
import com.ginni.easemygst.portal.business.entity.BrtMappingDTO;
import com.ginni.easemygst.portal.business.entity.DataUploadInfoDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.ErpConfigDTO;
import com.ginni.easemygst.portal.business.entity.FiscalyearDetailDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.HsnSacDTO;
import com.ginni.easemygst.portal.business.entity.ItemCodeDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.entity.UserGstinDTO;
import com.ginni.easemygst.portal.business.entity.VendorDTO;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.ErpLog;
import com.ginni.easemygst.portal.persistence.entity.HsnSac;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.persistence.entity.Vendor;

@Local
public interface TaxpayerService {

	public Integer updateDataUploadInfo(DataUploadInfoDTO infoDTO) throws AppException;

	public Map<String, Object> updateDataUploadInfo(List<DataUploadInfoDTO> infoDTOs) throws AppException;

	public Map<String, Object> getUsers() throws AppException;

	public List<UserGstin> getGstinList() throws AppException;

	public Map<String, Object> getUserGstinList(String gstin, String username) throws AppException;

	public boolean removeUserGstinData(String gstin, String username) throws AppException;

	public String updateGstinUser(String gstin, String request) throws AppException;

	public String addVendorInvoiceAccess(String gstin, String username) throws AppException;

	public List<HsnSac> getHsnSacData(String pan) throws AppException;

	public int addHsnSacData(String pan, HsnSacDTO hsnSacDTO) throws AppException;

	public HsnSacDTO deleteHsnSacData(int id, String pan) throws AppException;

	public int readExcelVendorData(String pan, File file, String mapping, String type) throws AppException;

	public List<Vendor> getVendorData(String pan, String type) throws AppException;

	public VendorDTO createVendor(VendorDTO vendorDTO, String pan) throws AppException;

	public VendorDTO createVendorFromGstin(VendorDTO vendorDTO, String gstin) throws AppException;

	public VendorDTO updateVendor(VendorDTO vendorDTO) throws AppException;

	public VendorDTO deleteVendor(int id, String pan, String type) throws AppException;

	public ItemCodeDTO createItemCode(ItemCodeDTO itemCodeDTO, String gstin) throws AppException;

	/* public Map<String, Object> getTaxpayer(int userId) throws AppException; */

	public boolean updateTaxpayerBillingAddress(String pan, BillingAddressDTO billingAddressDTO) throws AppException;

	public TaxpayerGstinDTO createTaxpayerGstin(TaxpayerGstinDTO taxpayerGstinDTO) throws AppException;

	public TaxpayerGstinDTO updateTaxpayerGstin(TaxpayerGstinDTO taxpayerGstinDTO) throws AppException;

	public String deleteTaxpayerGstin(int id) throws AppException;

	public TaxpayerDTO getTaxpayerGstinDetail(String pan) throws AppException;

/*	public TaxpayerGstinDTO getTaxpayerGstin(String gstin) throws AppException;*/
	
	public boolean updateErpConfig(ErpConfigDTO erpConfigDTO) throws AppException,JsonProcessingException, IOException;
	
	public ErpConfigDTO getErpConfig() throws AppException;

	public List<ErpLog> getErpLogs() throws AppException;

	public boolean addTaxpayerUser(String pan, String username) throws AppException;

	public boolean addGstinUser(String gstin, String username, List<UserGstinDTO> userGstinDTOs) throws AppException;

	public boolean removeTaxpayerUser(String pan, String username) throws AppException;

	public boolean removeGstinUser(String gstin, Integer id) throws AppException;

	public boolean requestGstnOtp(String gsting) throws AppException;

	public String requestGstnAuthToken(String gstin, String otp) throws AppException;

	public String getGstinAuthTokenValidity(String gstin) throws AppException;

	public String saveDummyAuthToken(String gstin, String otp) throws AppException;

	public String requestGstnRefreshToken(TaxpayerGstin taxpayerGstin) throws AppException;

	public List<BrtMappingDTO> getBrtMappingByGstin(String gstin) throws AppException;

	public Set<GstReturnDTO> getGstReturnByGstin(String gstin) throws AppException;

	public Map<String, Object> getGstReturnByFrequencyGstin(String gstin) throws AppException;

	public Set<ResponseDTO> getReturnSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	public List<MonthSummaryDTO> getGstReturnSummaryByGstin(String gstin) throws AppException;

	public Set<ReturnTransactionDTO> getTransactionsByBusinessType(String types) throws AppException;

	public Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> getGstTransactionsByGstin(String gstin)
			throws AppException;

	public Set<ReturnTransactionDTO> getReturnTransactionsByGstin(String gstin, String returnCode) throws AppException;

	public boolean setFinancialYear(String start) throws AppException;

	public Map<String, Object> getFinancialYear() throws AppException;

	/*
	 * public String getTransactionMapping(String pan, String rtype ,String
	 * transaction) throws AppException;
	 * 
	 * public String saveTransactionMapping(String pan, String rtype ,String
	 * transaction, String request) throws AppException;
	 */

	public Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> getGstAllTransactions() throws AppException;

	public Map<String, Object> getInvoiceCreationData(String gstin) throws AppException;

	public Map<String, Object> getInvoiceData(String gstin) throws AppException;

	public Map<String, Object> deleteInvoiceData(String gstin, Integer id) throws AppException;

	public Map<String, Object> createInvoice(InvoiceVendorDetailsDto invoiceVendorDetailsDto, String gstin)
			throws AppException;

	public String updateInvoice(InvoiceVendorDetailsDto invoiceVendorDetailsDto, String gstin) throws AppException;

	public String generateInvoicePdf(String gstin, Integer sno) throws AppException;

	public boolean sendInvoiceEmail(String gstin, Integer sno, Integer cc, Integer attach, EmailDTO emailDTO)
			throws AppException;

	public enum ReturnCalendar {

		BLANK("BLANK"), APPROACHING("APPROACHING"), ONHOLD("ONHOLD"), LOCKED("LOCKED"), FILED("FILED"), OVERDUE(
				"OVERDUE"), CLOSED("CLOSED");

		private String value;

		ReturnCalendar(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	public boolean removeErpConfig(String provider) throws AppException;

	public List<DataUploadInfo> getErpDataUpload() throws AppException;

	public Map<String, Object> getDataUploadInfo(Integer sno) throws AppException;

	public void markGstnUnAuth(String gstin);

	public List<MonthSummaryDTO> updateMonthYearForGstin(TaxpayerGstin taxpayerGstin) throws AppException;

	public List<MonthSummaryDTO> updateMonthYear(TaxpayerGstin taxpayerGstin) throws AppException;

	public void proecessConsolidateCsv() throws AppException;

	public Map<String, Object> getfileprefdeshboard(String gstin, String fyear) throws AppException, JsonProcessingException, IOException;

	Map<String, Object> getTaxpayer(int userId, String fyear) throws AppException;

	boolean savePreference(String gstin, String prefernce, String fyear) throws AppException;

	TaxpayerGstinDTO getTaxpayerGstin(String gstin, String fyear) throws AppException;

	public String registerTaxpayerForEWayBill(String gstin, String username, String password,Boolean isActive);

	Map<String, Object> getAlltinDetail(int userId) throws AppException;

	boolean updateflagTaxpayerGstin(List<TinDTO> modifiedlist) throws AppException;
	
	boolean updateturnoverDetail(FiscalyearDetailDTO fiscalyearDetailDTO) throws AppException;

	DataUploadInfo consolidatedExcelUploadInfo(String datauploadInfoId, String fileName);

}
