package com.ginni.easemygst.portal.business.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.Local;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.business.service.EWayBillService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.EWayBillService.EWB_EVENT;
import com.ginni.easemygst.portal.constant.EwayBillConstant;
import com.ginni.easemygst.portal.data.view.Response;
import com.ginni.easemygst.portal.exception.EwayBillException;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.requestresponse.EWBGenerateRequest;
import com.ginni.easemygst.portal.requestresponse.nic.EWBConsumer;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBAuthRequest;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBBaseRequest;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBBaseResponse;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBErrorResponse;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBGenerateResponse;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBResponseProcessor;
import com.ginni.easemygst.portal.requestresponse.portal.request.EMGEWBRequest;
import com.ginni.easemygst.portal.security.AESEncryption;
import com.paytm.pg.crypto.AesEncryption;

@Local
@Transactional
public class EWayBillServiceImpl implements EWayBillService {

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private RedisService redisService;

	@Inject
	private EWBConsumer ewbConsumer;
	
	@Inject
	private FinderService finderService;

	private static final Logger _LOGGER = LoggerFactory.getLogger(EWayBillServiceImpl.class);

	@Override
	public String registerForEWB(String gstin, String username, String password,Boolean isActive) {

		return taxpayerService.registerTaxpayerForEWayBill(gstin, username, password,isActive);

	}

	private String getValidEWBAuthToken(String gstin) throws AppException, Exception {

		String ewbLocalToken = this.getAuthtokenFromRedis(gstin);

		if (StringUtils.isEmpty(ewbLocalToken)) {
			this.getAuthtokenFromNIC(gstin);
			ewbLocalToken = this.getAuthtokenFromRedis(gstin);
		}
		return ewbLocalToken;
	}

	@Override
	public Response generateEWayBill(JsonNode requestNode) throws Exception {

		EMGEWBRequest emgewbRequests = _MAPPER.convertValue(requestNode, EMGEWBRequest.class);

		Response response = new Response();

		response.setResponseCode("1000");
		response.setResponseMessage("Your request has been processed.");

		ArrayNode responseDataArray = _MAPPER.createArrayNode();

		response.setData(responseDataArray);

		for (EWBGenerateRequest ewbGenerateRequest : emgewbRequests.getEwbGenerateRequests()) {

			String gstin = ewbGenerateRequest.getFromGstin();

			ewbGenerateRequest.setGstin(gstin);

			ewbGenerateRequest.setEvent(EWB_EVENT.GENERATE);

			this.setTokens(gstin, ewbGenerateRequest);

			EWBGenerateResponse ewbGenerateResponse = null;

			try {
				JsonNode responseJson = ewbConsumer.callEWBAPI(ewbGenerateRequest);
				ewbGenerateResponse = _MAPPER.convertValue(responseJson, EWBGenerateResponse.class);
				ewbGenerateResponse.setStatus(EwayBillConstant._STATUS_SUCCESS);
				/*String url = new EwayBillReport().generateAndUploadEwayBillPdfToAzzure(ewbGenerateRequest,
						ewbGenerateResponse);
				ewbGenerateResponse.setEwaybillUrl(url);*/
			} catch (EwayBillException ewayBillException) {
				response.setResponseCode("1002");
				response.setResponseMessage("Request process with some errors");

				ewbGenerateResponse = new EWBGenerateResponse();

				EWBErrorResponse errorResponse = new EWBErrorResponse();

				ewbGenerateResponse.setStatus(EwayBillConstant._STATUS_FAILURE);

				ewbGenerateResponse.setErrorResponse(errorResponse);
				errorResponse.setErrorCode(ewayBillException.getErrorCode());
				errorResponse.setErrorMessage(ewayBillException.getMessage());
			}

			ewbGenerateResponse.setReferenceNumber(ewbGenerateRequest.getReferenceNumber());

			responseDataArray.addPOJO(ewbGenerateResponse);

		}
		return response;
	}

	private void setTokens(String gstin, EWBBaseRequest ewbBaseRequest) throws AppException, Exception {

		String authToken = getValidEWBAuthToken(gstin);

		String sekStr = redisService.getValue(String.format(EwayBillConstant._SEK, gstin));

		ewbBaseRequest.setAuthToken(authToken);
		ewbBaseRequest.setSek(sekStr.getBytes("UTF-8"));
	}

	private void getAuthtokenFromNIC(String gstin) throws EwayBillException, Exception {

		ewbConsumer.callEWBAPI(this.generateAuthTokenRequest(gstin));

	}

	private EWBAuthRequest generateAuthTokenRequest(String gstin) throws Exception {

		EWBAuthRequest authRequest = new EWBAuthRequest();

		authRequest.setEvent(EWB_EVENT.AUTHENTICATE);

		// TaxpayerGstin taxpayerGstin=finderService.findTaxpayerGstin(gstin);

		authRequest.setGstin(gstin);
		authRequest.setPassword("test_dlr534");// taxpayerGstin.getEwayBillPassword());
		authRequest.setUsername("test_dlr534");// taxpayerGstin.getEwayBillUserName());

		AESEncryption aesEncryption = new AESEncryption();
		byte[] ewb_app_key = aesEncryption.generateSecureKeyBytes();

		redisService.setKeyValue(String.format(EwayBillConstant._APP_KEY, gstin),
				aesEncryption.encodeBase64String(ewb_app_key));

		authRequest.setAppKeyBytes(ewb_app_key);
		return authRequest;
	}

	private String getAuthtokenFromRedis(String gstin) {

		return redisService.getValue(String.format(EwayBillConstant._AUTHTOKEN, gstin));
	}

	public void getSentEwayBill(String gstin, String genDate, boolean refresh) {

		if (refresh) {
			// get sent eway bill from nic
		} else {

			//
		}

	}
	
	@Override
	public JsonNode getEWBTinDetails(String gstin) throws AppException {
		
		ObjectNode ewbDetaisJson= _MAPPER.createObjectNode();
		
		TaxpayerGstin taxpayerGstin=finderService.findTaxpayerGstin(gstin);
		
		if(!Objects.isNull(taxpayerGstin))
		{
			ewbDetaisJson.put("username",taxpayerGstin.getEwayBillUserName());
			ewbDetaisJson.put("password",taxpayerGstin.getEwayBillPassword());
/*			ewbDetaisJson.put("isActive",taxpayerGstin.isEWBActive());
*/
		}else
			throw new AppException("1001","Gstin not registered with emg");
		
		return ewbDetaisJson;
		
	}

}
