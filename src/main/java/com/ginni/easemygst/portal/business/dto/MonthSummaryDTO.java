package com.ginni.easemygst.portal.business.dto;

import java.util.List;

import lombok.Data;

public @Data class MonthSummaryDTO {
	
	int id;
	
	String monthName;
	
	String code;
	
	String breadcrumb;
	
	List<ReturnSummaryDTO> returnSummary;

}
