package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.view.serializer.reco.B2BRecoDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;

import lombok.Data;

public @Data class B2bDetailEntityWrapper {

	@JsonSerialize(using=B2BRecoDetailSerializer.class)
	private B2BDetailEntity invoice;
	
	public B2bDetailEntityWrapper(B2BDetailEntity invoice){
		this.invoice=invoice;
	}
}
