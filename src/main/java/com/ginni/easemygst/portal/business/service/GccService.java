package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface GccService {

	public Object getDataFromGcc(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
}
