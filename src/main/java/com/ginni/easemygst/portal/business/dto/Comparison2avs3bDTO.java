package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.ws.rs.DefaultValue;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

import lombok.Data;
public @Data class Comparison2avs3bDTO implements Serializable {
/**
	 * 
	 */
private static final long serialVersionUID = 1L;
private String monthYear;
private String shortName;
@DefaultValue(value = "0.0")
private BigDecimal allotherITC,ITCReverseinGSTR3B,inwarddetailsasperGSTR2A,ineligibleITCasperGSTR3B,netInwarddetailsasperGSTR2A,difference,percentageofExcess=BigDecimal.ZERO;

private BigDecimal sumotherrev;
	
	
public Comparison2avs3bDTO(){
	
}

	
public Comparison2avs3bDTO(String monthYear, Double allotherITC, Double ITCRevers, Double ITCInelg,Double inwardtaxamount) {	
 this.monthYear=monthYear;	
 this.allotherITC=BigDecimal.valueOf(allotherITC).setScale(2,RoundingMode.HALF_UP);
 this.ITCReverseinGSTR3B=BigDecimal.valueOf(ITCRevers).setScale(2,RoundingMode.HALF_UP);
 this.ineligibleITCasperGSTR3B=BigDecimal.valueOf(ITCInelg).setScale(2,RoundingMode.HALF_UP);
 this.inwarddetailsasperGSTR2A=BigDecimal.valueOf(inwardtaxamount).setScale(2,RoundingMode.HALF_UP);
 this.netInwarddetailsasperGSTR2A=inwarddetailsasperGSTR2A.subtract(ineligibleITCasperGSTR3B);
 this.sumotherrev=this.allotherITC.subtract(ITCReverseinGSTR3B);
 this.difference=sumotherrev.subtract(netInwarddetailsasperGSTR2A);
 if(sumotherrev.compareTo(BigDecimal.ZERO) > 0)
 this.percentageofExcess=difference.divide(sumotherrev,2,RoundingMode.HALF_UP).multiply(new BigDecimal(100));
 try {
	 if(!monthYear.equalsIgnoreCase("all"))
	this.shortName=CalanderCalculatorUtility.getShortCommaStyleMonthYear(monthYear);
} catch (AppException e) {
	e.printStackTrace();
}
}




}
