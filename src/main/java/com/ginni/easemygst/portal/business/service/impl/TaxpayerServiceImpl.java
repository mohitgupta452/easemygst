package com.ginni.easemygst.portal.business.service.impl;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.json.Json;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.TinDTO;
import com.ginni.easemygst.portal.business.dto.AssignedGstinList;
import com.ginni.easemygst.portal.business.dto.GstCalendarDto;
import com.ginni.easemygst.portal.business.dto.GstinUserListDTO;
import com.ginni.easemygst.portal.business.dto.InvoiceLineItemDTO;
import com.ginni.easemygst.portal.business.dto.InvoiceVendorDetailsDto;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.MonthSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ResponseDTO;
import com.ginni.easemygst.portal.business.dto.ReturnSummaryDTO;
import com.ginni.easemygst.portal.business.entity.BillingAddressDTO;
import com.ginni.easemygst.portal.business.entity.BrtMappingDTO;
import com.ginni.easemygst.portal.business.entity.DataUploadInfoDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.ErpConfigDTO;
import com.ginni.easemygst.portal.business.entity.FilePreferenceDTO;
import com.ginni.easemygst.portal.business.entity.FiscalyearDetailDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.HsnSacDTO;
import com.ginni.easemygst.portal.business.entity.ItemCodeDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.entity.UserGstinDTO;
import com.ginni.easemygst.portal.business.entity.VendorDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.ConsolidatedCsvProcessorManager;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ReturnsService.GstnAction;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.business.service.UserService.UserStatus;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.business.util.GstReturnComparator;
import com.ginni.easemygst.portal.client.FTPService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.Constant;
import com.ginni.easemygst.portal.constant.Constant.GstReturnFrequency;
import com.ginni.easemygst.portal.constant.Constant.TaxpayerType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.gst.api.GstConsumer;
import com.ginni.easemygst.portal.gst.request.AuthOtpReq;
import com.ginni.easemygst.portal.gst.request.AuthRefreshTokenReq;
import com.ginni.easemygst.portal.gst.request.AuthTokenReq;
import com.ginni.easemygst.portal.gst.response.AuthOtpResp;
import com.ginni.easemygst.portal.gst.response.AuthTokenResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.notify.EmailFactory;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.ErpConfig;
import com.ginni.easemygst.portal.persistence.entity.ErpLog;
import com.ginni.easemygst.portal.persistence.entity.FilePrefernce;
import com.ginni.easemygst.portal.persistence.entity.GstCalendar;
import com.ginni.easemygst.portal.persistence.entity.GstReturn;
import com.ginni.easemygst.portal.persistence.entity.GstinAction;
import com.ginni.easemygst.portal.persistence.entity.GstinTransaction;
import com.ginni.easemygst.portal.persistence.entity.HsnProduct;
import com.ginni.easemygst.portal.persistence.entity.HsnSac;
import com.ginni.easemygst.portal.persistence.entity.HsnSubChapter;
import com.ginni.easemygst.portal.persistence.entity.InvoiceDetail;
import com.ginni.easemygst.portal.persistence.entity.InvoiceVendorDetails;
import com.ginni.easemygst.portal.persistence.entity.ItemCode;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.State;
import com.ginni.easemygst.portal.persistence.entity.Taxpayer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TpMapping;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.persistence.entity.UserContact;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.persistence.entity.UserTaxpayer;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.security.AESEncryption;
import com.ginni.easemygst.portal.security.EncryptDecrypt;
import com.ginni.easemygst.portal.security.GSTRSAEncryptDecryptService;
import com.ginni.easemygst.portal.security.PasswordHash;
import com.ginni.easemygst.portal.transaction.crud.ConsolidatedService;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.InvoiceReport;
import com.ginni.easemygst.portal.utils.NumberToWordsConverter;
import com.ginni.easemygst.portal.utils.Utility;

@Transactional
@Stateless
public class TaxpayerServiceImpl implements TaxpayerService {

	@Inject
	private Logger log;

	@Inject
	private FinderService finderService;

	private static Properties excelConfig = null;

	@Inject
	private CrudService crudService;

	@Inject
	private UserService userService;

	@Inject
	private GSTRSAEncryptDecryptService encryptDecrypt;

	@Inject
	private UserBean userBean;

	@Inject
	private GstConsumer gstConsumer;

	@Inject
	private RedisService redisService;

	@Inject
	private ReturnsService returnService;

	@Inject
	private ConsolidatedCsvProcessorManager consolidatedCsvManager;

	@Inject
	private ApiLoggingService apiLoggingService;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public Map<String, Object> getUsers() throws AppException {

		int userId = userBean.getUserDto().getId();
		User userEm = em.find(User.class, userId);

		Map<String, Object> data = new HashMap<>();
		List<GstinUserListDTO> userListDTOs = new ArrayList<>();
		List<UserGstin> userGstins = finderService.getUserGstinByOrganisation(userEm.getOrganisation().getId());

		data.put("list", userListDTOs);
		data.put("count", 0);

		if (!Objects.isNull(userGstins)) {

			userGstins = userGstins.stream().filter(u -> u.getGstReturnBean() != null).collect(Collectors.toList());

			for (UserGstin userGstin2 : userGstins) {

				if (userGstin2.getUser().getUserContacts() != null
						&& !userGstin2.getUser().getUserContacts().isEmpty()) {
					GstinUserListDTO gstinUserListDTO = new GstinUserListDTO();
					gstinUserListDTO.setEmail(userGstin2.getUser().getUserContacts().get(0).getEmailAddress());
					if (userListDTOs.contains(gstinUserListDTO)) {
						gstinUserListDTO = userListDTOs.get(userListDTOs.indexOf(gstinUserListDTO));
					} else {
						gstinUserListDTO.setName(
								userGstin2.getUser().getFirstName() + " " + userGstin2.getUser().getLastName());
						gstinUserListDTO.setGstinLists(new ArrayList<>());
						gstinUserListDTO.setMobile(userGstin2.getUser().getUserContacts().get(0).getMobileNumber());
						gstinUserListDTO.setVerified(userGstin2.getUser().getActive() == 1 ? "Yes" : "No");
						userListDTOs.add(gstinUserListDTO);
					}

					List<AssignedGstinList> gstinLists = gstinUserListDTO.getGstinLists();
					AssignedGstinList assignedGstinList = new AssignedGstinList();
					assignedGstinList.setGstin(userGstin2.getTaxpayerGstin().getGstin());
					if (gstinLists.contains(assignedGstinList)) {
						assignedGstinList = gstinLists.get(gstinLists.indexOf(assignedGstinList));
					} else {
						assignedGstinList.setGstinDisplayName(userGstin2.getTaxpayerGstin().getDisplayName());
						assignedGstinList.setGstinState(userGstin2.getTaxpayerGstin().getStateBean().getName());
						assignedGstinList.setAccessType("Limited access");
						gstinLists.add(assignedGstinList);
					}

					if (!Objects.isNull(userGstin2.getGstReturnBean()) && userGstin2.getGstReturnBean().getId() == 99) {
						assignedGstinList.setInvoiceAccess(true);
					}
				}

			}

			BigInteger gstinFinalCount = (BigInteger) em.createNativeQuery(String.format(
					"select count(distinct gstinId) from user_gstin where userId = %s and organisationId = %s", userId,
					userEm.getOrganisation().getId())).getSingleResult();

			BigInteger userFinalCount = (BigInteger) em.createNativeQuery(
					String.format("select count(distinct userId) from user_gstin where organisationId = %s",
							userEm.getOrganisation().getId()))
					.getSingleResult();

			int userCount = userFinalCount.intValue() > 0 ? userFinalCount.intValue() - 1 : 0;

			final int gstinCount = gstinFinalCount.intValue();

			userListDTOs.stream().forEach(u -> u.setCount(u.getGstinLists().size()));
			userListDTOs.stream().forEach(u -> u.setEnableAssignTin(u.getCount() != gstinCount));

			data.put("list", userListDTOs);
			data.put("count", userCount);

			return data;
		} else {
			return data;
		}
	}

	@Override
	public List<UserGstin> getGstinList() throws AppException {

		int userId = userBean.getUserDto().getId();
		User userEm = em.find(User.class, userId);

		List<UserGstin> userGstins = finderService.getUserGstinByUserOrg(userId, userEm.getOrganisation().getId());

		if (!Objects.isNull(userGstins)) {

			return userGstins;
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public Map<String, Object> getUserGstinList(String gstin, String username) throws AppException {

		List<UserGstin> userGstins = finderService.findUserGstinByUserTaxpayer(username, gstin);

		Map<String, Object> obj = new HashMap<>();

		if (!Objects.isNull(userGstins)) {

			userGstins = userGstins.stream().filter(u -> u.getGstReturnBean() != null).collect(Collectors.toList());
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			UserDTO userDTO = finderService.getExistingUser(username);

			obj.put("userdata", userGstins);
			obj.put("gstin", taxpayerGstin);
			obj.put("name", userDTO.getFirstName() + " " + userDTO.getLastName());
			obj.put("email", userDTO.getUsername());

			return obj;
		} else {
			return null;
		}
	}

	@Override
	public String updateGstinUser(String gstin, String request) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			User userEm = em.find(User.class, userBean.getUserDto().getId());
			Organisation organisation = userEm.getOrganisation();

			if (Objects.isNull(organisation)) {
				throw new AppException(ExceptionCode._ORGANISATION_NOT_FOUND);
			}

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				JSONObject jsonObject = new JSONObject(request);
				String username = jsonObject.getString("username");
				String type = jsonObject.getString("type");
				JSONArray access = jsonObject.getJSONArray("request");

				if (Objects.isNull(access) || access.length() == 0)
					throw new AppException(ExceptionCode._INVALID_NULL_ACCESS_RIGHT);

				List<UserGstinDTO> userGstinDTOs;
				try {
					userGstinDTOs = JsonMapper.objectMapper.readValue(access.toString(),
							new TypeReference<List<UserGstinDTO>>() {
							});
				} catch (IOException e1) {
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
				}

				UserDTO user = finderService.getExistingUser(username);
				String gstinNumber = "", company = "", name = "", userName = "", toEmail = "";

				if (Objects.isNull(taxpayerGstin)) {
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
				} else if (!Objects.isNull(user)) {

					UserContact userContact = finderService.getUserContact(user.getId());

					if (user.getId().equals(userBean.getUserDto().getId()))
						throw new AppException(ExceptionCode._SELF_ACCESS);

					List<UserGstin> gstins = new ArrayList<>();

					for (UserGstinDTO userGstinDTO : userGstinDTOs) {

						UserGstin userGstin = (UserGstin) EntityHelper.convert(userGstinDTO, UserGstin.class);
						userGstin.setTaxpayerGstin(taxpayerGstin);
						userGstin.setUser(em.getReference(User.class, user.getId()));
						userGstin.setOrganisation(organisation);

						userGstin.setCreatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						userGstin.setCreationIpAddress(userBean.getIpAddress());
						userGstin.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

						gstins.add(userGstin);
					}

					List<UserGstin> existing = finderService.findUserGstinByUserTaxpayer(username, gstin);
					if (!StringUtils.isEmpty(type) && type.equalsIgnoreCase("add") && !Objects.isNull(existing)) {
						throw new AppException(ExceptionCode._INVALID_ACCESS);
					}

					boolean checkuser = false;
					if (!Objects.isNull(existing)) {
						for (UserGstin userGstin : existing) {
							crudService.delete(UserGstin.class, userGstin.getId());
						}
					} else {
						if (finderService.getUserGstinByUserOrg(user.getId(), organisation.getId()).isEmpty()) {
							checkuser = userService.checkUsers();
						} else {
							checkuser = true;
						}
					}

					if (!Objects.isNull(existing) || (Objects.isNull(existing) && checkuser)) {
						for (UserGstin userGstin : gstins) {
							crudService.create(userGstin);
						}
					}

					gstinNumber = taxpayerGstin.getGstin();
					company = taxpayerGstin.getTaxpayer().getLegalName();
					userName = user.getFirstName() + " " + user.getLastName();
					name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();
					toEmail = userContact.getEmailAddress();

					Map<String, String> params = new HashMap<>();
					params.put("toEmail", toEmail);
					params.put("gstinNumber", gstinNumber);
					params.put("company", company);
					params.put("name", name);
					params.put("userName", userName);
					EmailFactory factory = new EmailFactory();
					factory.sendAddGstinUserEmail(params);
					log.info("Sending gstin user added email to user {}.", params.toString());
				} else {

					if (!Utility.validateEmail(username)) {
						throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
					}

					if (userService.checkUsers()) {
						User user1 = new User();
						user1.setUsername(username);
						user1.setActive((byte) 0);
						user1.setFirstName("Pending");
						user1.setLastName("Confirmation");
						user1.setStatus(UserStatus.INACTIVE.toString());
						String randomPass = Utility.randomString(10);
						try {
							user1.setPassword(PasswordHash.createHash(randomPass));
						} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
							throw new AppException();
						}

						UserContact userContact = new UserContact();
						userContact.setEmailAddress(username);
						userContact.setMobileAuth((byte) 0);
						userContact.setMobileNumber("");
						userContact.setUser(user1);
						List<UserContact> contacts = new ArrayList<>();
						contacts.add(userContact);
						user1.setUserContacts(contacts);
						user1.setCreatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						user1.setCreationIpAddress(userBean.getIpAddress());
						user1.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
						user1 = crudService.create(user1);

						for (UserGstinDTO userGstinDTO : userGstinDTOs) {
							UserGstin userGstin = (UserGstin) EntityHelper.convert(userGstinDTO, UserGstin.class);
							userGstin.setTaxpayerGstin(taxpayerGstin);
							userGstin.setUser(user1);
							userGstin.setOrganisation(organisation);

							userGstin.setCreatedBy(
									userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
							userGstin.setCreationIpAddress(userBean.getIpAddress());
							userGstin.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

							crudService.create(userGstin);
						}

						gstinNumber = taxpayerGstin.getGstin();
						company = taxpayerGstin.getTaxpayer().getLegalName();
						userName = "User";
						name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();
						toEmail = user1.getUserContacts().get(0).getEmailAddress();

						Map<String, String> params = new HashMap<>();
						params.put("toEmail", username);
						params.put("username", username);
						params.put("password", randomPass);
						params.put("gstinNumber", gstinNumber);
						params.put("company", company);
						params.put("name", name);
						EmailFactory factory = new EmailFactory();
						factory.sendNewUserTempEmail(params);
						log.info("Sending new user email to user for pan access {}.", params.toString());
					} else {
						throw new AppException(ExceptionCode._SUBSCRIPTION_QUANTITY_REQUIRED);
					}
				}

				return username + ":" + type;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String addVendorInvoiceAccess(String gstin, String username) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				UserDTO user = finderService.getExistingUser(username);
				User userEm = em.find(User.class, user.getId());
				String gstinNumber = "", company = "", name = "", userName = "", toEmail = "";

				if (Objects.isNull(taxpayerGstin)) {
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
				} else if (!Objects.isNull(user)) {

					UserContact userContact = finderService.getUserContact(user.getId());

					UserGstin userGstin = new UserGstin();
					userGstin.setTaxpayerGstin(taxpayerGstin);
					userGstin.setUser(em.getReference(User.class, user.getId()));
					userGstin.setOrganisation(userEm.getOrganisation());

					GstReturn gstReturn = new GstReturn();
					gstReturn.setId(99);
					userGstin.setGstReturnBean(gstReturn);
					userGstin.setType("INVOICE");

					userGstin.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					userGstin.setCreationIpAddress(userBean.getIpAddress());
					userGstin.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.create(userGstin);

					gstinNumber = taxpayerGstin.getGstin();
					company = taxpayerGstin.getTaxpayer().getLegalName();
					userName = user.getFirstName() + " " + user.getLastName();
					name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();
					toEmail = userContact.getEmailAddress();
				}

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", toEmail);
				params.put("gstinNumber", gstinNumber);
				params.put("company", company);
				params.put("name", name);
				params.put("userName", userName);
				EmailFactory factory = new EmailFactory();
				factory.sendAddGstinUserEmail(params);
				log.info("Sending gstin user added email to user {}.", params.toString());

				return username;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public boolean removeUserGstinData(String gstin, String username) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			if (gstin.equalsIgnoreCase("all")) {

				UserDTO userDTO = finderService.getExistingUser(username);
				User userEm = em.find(User.class, userBean.getUserDto().getId());

				if (userDTO.getId().equals(userBean.getUserDto().getId()))
					throw new AppException(ExceptionCode._SELF_ACCESS);

				List<UserGstin> existing = finderService.getUserGstinByUserOrg(userDTO.getId(),
						userEm.getOrganisation().getId());
				if (!Objects.isNull(existing)) {
					for (UserGstin userGstin : existing) {
						crudService.delete(UserGstin.class, userGstin.getId());
					}

					/*
					 * if (!Objects.isNull(userDTO.getOrganisation())) { String redisTaxpayerKey =
					 * userDTO.getId() + "-" + userDTO.getOrganisation().getId();
					 * redisService.removeUserOrgTaxpayer(redisTaxpayerKey); } else { UserDTO
					 * userOrg =
					 * finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().
					 * getId()); int orgId = userOrg.getId(); String redisTaxpayerKey =
					 * userDTO.getId() + "-" + orgId;
					 * redisService.removeUserOrgTaxpayer(redisTaxpayerKey); }
					 */
				}

				UserDTO user = finderService.getExistingUser(username);

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", username);
				params.put("gstinNumber", "All");
				params.put("company", "All GSTINs");
				params.put("name", userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
				params.put("userName", user.getFirstName() + " " + user.getLastName());
				EmailFactory factory = new EmailFactory();
				factory.sendDeleteGstinUserEmail(params);
				log.info("Sending gstin user delete email to user {}.", params.toString());

				return Boolean.TRUE;

			} else {

				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

				if (!Objects.isNull(taxpayerGstin)) {

					List<UserGstin> existing = finderService.findUserGstinByUserTaxpayer(username, gstin);
					if (!Objects.isNull(existing)) {
						for (UserGstin userGstin : existing) {
							crudService.delete(UserGstin.class, userGstin.getId());
						}
					}

					UserDTO user = finderService.getExistingUser(username);

					Map<String, String> params = new HashMap<>();
					params.put("toEmail", username);
					params.put("gstinNumber", gstin);
					params.put("company", taxpayerGstin.getTaxpayer().getLegalName());
					params.put("name",
							userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
					params.put("userName", user.getFirstName() + " " + user.getLastName());
					EmailFactory factory = new EmailFactory();
					factory.sendDeleteGstinUserEmail(params);
					log.info("Sending gstin user delete email to user {}.", params.toString());

					return Boolean.TRUE;
				} else {
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
				}
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Map<String, Object> getTaxpayer(int userId, String fyear) throws AppException {

		log.info("getTaxpayer method starts here..");

		if (userId > 0) {

			boolean myOrg = false;
			int erpId = userBean.getUserDto().getId();
			User userEm = em.find(User.class, userId);
			if (!Objects.isNull(userEm.getOrganisation())) {
				if (userEm.getOrganisation().getId().equals(userBean.getUserDto().getOrganisation().getId())) {
					myOrg = true;
				} else {
					UserDTO userOrg = finderService
							.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
					erpId = userOrg.getId();
				}
			} else {
				UserDTO userOrg = finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
				erpId = userOrg.getId();
			}

			Map<String, Object> taxpayerMap = new HashMap<>();
			List<TaxpayerDTO> taxpayerDTOs = new ArrayList<>();

			if (Objects.isNull(userBean.getUserDto().getOrganisation())) {
				throw new AppException(ExceptionCode._ORGANISATION_NOT_FOUND);
			}

			Organisation organisation = em.find(Organisation.class, userBean.getUserDto().getOrganisation().getId());
			String redisTaxpayerKey = userBean.getUserDto().getId() + "-"
					+ userBean.getUserDto().getOrganisation().getId() + "-" + fyear;
			if (myOrg == true) {
				taxpayerDTOs = redisService.getUserOrgTaxpayer(redisTaxpayerKey);
			} else {
				taxpayerDTOs = null;
			}
			if (Objects.isNull(taxpayerDTOs)) {

				taxpayerDTOs = new ArrayList<>();

				List<UserGstin> userGstins = finderService.getUserGstinByUserOrg(userId,
						userBean.getUserDto().getOrganisation().getId());
				List<TaxpayerDTO> dtos = new ArrayList<>();
				for (UserGstin userGstin : userGstins) {
					TaxpayerGstin taxpayerGstin = userGstin.getTaxpayerGstin();
					log.info("getTaxpayer successfully retrieved the taxpayer gstin {}", taxpayerGstin);
					TaxpayerDTO taxpayerDTO = (TaxpayerDTO) EntityHelper.convert(taxpayerGstin.getTaxpayer(),
							TaxpayerDTO.class);
					if (!taxpayerDTOs.contains(taxpayerDTO)) {
						taxpayerDTO.setStatus("SECONDARY");
						if (dtos.contains(taxpayerDTO))
							taxpayerDTO = dtos.get(dtos.indexOf(taxpayerDTO));
						else {
							taxpayerDTO.setTaxpayerGstins(new ArrayList<>());

							BigInteger vendorCount = (BigInteger) em.createNativeQuery(String.format(
									"select count(1) from vendor where taxpayerId = %s and type = 'supplier' and active = 1",
									taxpayerDTO.getId())).getSingleResult();

							BigInteger vendorVerifiedCount = (BigInteger) em.createNativeQuery(String.format(
									"select count(1) from vendor where taxpayerId = %s and type = 'supplier' and emgst = 1 and active = 1",
									taxpayerDTO.getId())).getSingleResult();

							BigInteger customerCount = (BigInteger) em.createNativeQuery(String.format(
									"select count(1) from vendor where taxpayerId = %s and type = 'buyer' and active = 1",
									taxpayerDTO.getId())).getSingleResult();

							BigInteger customerVerifiedCount = (BigInteger) em.createNativeQuery(String.format(
									"select count(1) from vendor where taxpayerId = %s and type = 'buyer' and emgst = 1 and active = 1",
									taxpayerDTO.getId())).getSingleResult();

							taxpayerDTO.setVendorCount(vendorCount.intValue());
							taxpayerDTO.setVendorVerifiedCount(vendorVerifiedCount.intValue());
							taxpayerDTO.setCustomerCount(customerCount.intValue());
							taxpayerDTO.setCustomerVerifiedCount(customerVerifiedCount.intValue());

							taxpayerDTO.setTaxpayerGstins(new ArrayList<>());

							dtos.add(taxpayerDTO);

						}

						// List<TaxpayerGstinDTO> gstinDTOs =
						// taxpayerDTO.getTaxpayerGstins();
						TaxpayerGstinDTO tempGstinDto = (TaxpayerGstinDTO) EntityHelper.convert(taxpayerGstin,
								TaxpayerGstinDTO.class);

						/*
						 * <<<<<<< HEAD Map<String, List<GstCalendarDto>> dueDateCalendar =
						 * returnService.getDueDateCalendar(taxpayerGstin.getGstin()); =======
						 */
						// setting gstin due date claender for home page
						/*
						 * Map<String, List<GstCalendarDto>> dueDateCalendar =
						 * returnService.getDueDateCalendar(taxpayerGstin.getGstin(),fyear);
						 * 
						 * tempGstinDto.setDueDateCalendar(dueDateCalendar);
						 */

						FilePrefernce fileprefernce = finderService.findpreference(tempGstinDto.getGstin(), fyear);

						if (Objects.isNull(fileprefernce)) {
							tempGstinDto.setChangeCycle(true);
						} else {
							tempGstinDto.setReturnPreference(fileprefernce.getPreference());
						}

						// end setting gstin due date calendar
						// GSTR pending for the month -- setting null for now
						/*
						 * if (!gstinDTOs.contains(tempGstinDto)) { gstinDTOs.add(tempGstinDto);
						 * 
						 * BigInteger userGstinCount = (BigInteger) em.createNativeQuery(String .format(
						 * "select count(distinct userId) from user_gstin where gstinId = %s " ,
						 * tempGstinDto.getId())) .getSingleResult();
						 * tempGstinDto.setUserCount(userGstinCount.intValue() > 0 ?
						 * userGstinCount.intValue() - 1 : 0); tempGstinDto.setVendorCount(taxpayerDTO.
						 * getVendorCount()) ; tempGstinDto.setVendorVerifiedCount(taxpayerDTO.
						 * getVendorVerifiedCount()); tempGstinDto.setCustomerCount(taxpayerDTO.
						 * getCustomerCount()); tempGstinDto.setCustomerVerifiedCount(taxpayerDTO.
						 * getCustomerVerifiedCount()); } else { tempGstinDto =
						 * gstinDTOs.get(gstinDTOs.indexOf(tempGstinDto)); }
						 */

						BigInteger userGstinCount = (BigInteger) em.createNativeQuery(
								String.format("select count(distinct userId) from user_gstin where gstinId = %s ",
										tempGstinDto.getId()))
								.getSingleResult();
						tempGstinDto.setUserCount(userGstinCount.intValue() > 0 ? userGstinCount.intValue() - 1 : 0);
						tempGstinDto.setVendorCount(taxpayerDTO.getVendorCount());
						tempGstinDto.setVendorVerifiedCount(taxpayerDTO.getVendorVerifiedCount());
						tempGstinDto.setCustomerCount(taxpayerDTO.getCustomerCount());
						tempGstinDto.setCustomerVerifiedCount(taxpayerDTO.getCustomerVerifiedCount());

						if (taxpayerDTO.getTaxpayerGstins().contains(tempGstinDto))
							tempGstinDto = taxpayerDTO.getTaxpayerGstins()
									.get(taxpayerDTO.getTaxpayerGstins().indexOf(tempGstinDto));
						else
							taxpayerDTO.getTaxpayerGstins().add(tempGstinDto);

						if (userGstin.getGstReturnBean() == null) {
							tempGstinDto.setStatus("PRIMARY");
							myOrg = true;
							List<UserDTO> userDTOs = tempGstinDto.getUsers();
							if (Objects.isNull(userDTOs)) {
								userDTOs = new ArrayList<>();
								UserDTO userDto = new UserDTO();
								userDto.setUsername("GSTR1");
								userDto.setStatus("FILE");
								userDTOs.add(userDto);
								UserDTO userDto1 = new UserDTO();
								userDto1.setUsername("GSTR2");
								userDto1.setStatus("FILE");
								userDTOs.add(userDto1);
								UserDTO userDto2 = new UserDTO();
								userDto2.setUsername("GSTR1A");
								userDto2.setStatus("FILE");
								userDTOs.add(userDto2);
								UserDTO userDto3 = new UserDTO();
								userDto3.setUsername("GSTR3");
								userDto3.setStatus("FILE");
								userDTOs.add(userDto3);
								tempGstinDto.setUsers(userDTOs);
							}
						} else {
							if (userGstin.getGstReturnBean().getId() == 99) {
								tempGstinDto.setStatus("PRIMARY");
							} else {
								tempGstinDto.setStatus("SECONDARY");
							}
							myOrg = false;
							if (!Objects.isNull(userBean.getUserDto())
									&& userBean.getUserDto().getId().equals(userGstin.getUser().getId())) {
								UserDTO userDto = new UserDTO();
								userDto.setUsername(userGstin.getGstReturnBean().getCode());
								userDto.setStatus(userGstin.getType());
								List<UserDTO> userDTOs = tempGstinDto.getUsers();
								if (Objects.isNull(userDTOs)) {
									userDTOs = new ArrayList<>();
									tempGstinDto.setUsers(userDTOs);
								}
								userDTOs.add(userDto);
							}
						}
					}
					// }
				}
				taxpayerDTOs.addAll(dtos);

				taxpayerDTOs.removeIf(t -> t.getTaxpayerGstins() == null || t.getTaxpayerGstins().isEmpty());

				redisService.setUserOrgTaxpayer(redisTaxpayerKey, taxpayerDTOs, true, 7200);

			}

			BigInteger gstinCount = (BigInteger) em.createNativeQuery(
					String.format("select count(distinct gstinId) from user_gstin where organisationId = %s",
							userBean.getUserDto().getOrganisation().getId()))
					.getSingleResult();

			BigInteger userCount = (BigInteger) em.createNativeQuery(
					String.format("select count(distinct userId) from user_gstin where organisationId = %s",
							userBean.getUserDto().getOrganisation().getId()))
					.getSingleResult();

			log.info("getTaxpayer method ends..");
			taxpayerMap.put("taxpayer", taxpayerDTOs);
			taxpayerMap.put("user_count", userCount.intValue() > 0 ? userCount.intValue() - 1 : 0);
			taxpayerMap.put("gstin_count", gstinCount);
			taxpayerMap.put("myorg", myOrg ? "true" : "false");
			if (!StringUtils.isEmpty(organisation.getSubscriptionId())) {
				taxpayerMap.put("plantype",
						organisation.getPlanCode() != null ? (organisation.getPlanCode().startsWith("EMGE") ? "E" : "S")
								: "noplan");
			} else {
				taxpayerMap.put("plantype", "noplan");
			}
			taxpayerMap.put("card", organisation.getCardUpdate());

			if (organisation.getStatus().equalsIgnoreCase("trial")) {
				Date todaysDate = Calendar.getInstance().getTime();
				int diffInDays = (int) ((organisation.getExpiresAt().getTime() - todaysDate.getTime())
						/ (1000 * 60 * 60 * 24));
				taxpayerMap.put("trial", "yes");
				taxpayerMap.put("trial_days", diffInDays + 1);
			} else {
				taxpayerMap.put("trial", "no");
				taxpayerMap.put("trial_days", -1);
			}

			if (!Objects.isNull(organisation.getExpiresAt())) {
				if (organisation.getExpiresAt().before(Calendar.getInstance().getTime())) {
					if (organisation.getStatus().equalsIgnoreCase("trial") && myOrg) {
						taxpayerMap.put("trial", "yes");
						taxpayerMap.put("trial_days", "-1");
					} else {
						throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
					}
				}
			}
			if (!Objects.isNull(organisation.getNextBilling())) {
				if (organisation.getNextBilling().before(Calendar.getInstance().getTime())
						|| organisation.getNextBilling().equals(Calendar.getInstance().getTime())) {
					if (organisation.getStatus().equalsIgnoreCase("trial") && myOrg) {
						taxpayerMap.put("trial", "yes");
						taxpayerMap.put("trial_days", "-1");
					} else {
						throw new AppException(ExceptionCode._SUBSCRIPTION_EXPIRED);
					}
				}
			}

			ErpConfig erpConfig = finderService.findErpConfigByUser(erpId);

			if (!Objects.isNull(erpConfig)) {
				ErpConfigDTO erpConfigDto = (ErpConfigDTO) EntityHelper.convert(erpConfig, ErpConfigDTO.class);
				taxpayerMap.put("isErp", "true");
				ErpLog erpLog = finderService.getErpLog(erpId, "File data processed%");
				if (!Objects.isNull(erpLog)) {
					long time = erpLog.getCreationTime().getTime();
					Date d = new Date(time);
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
					erpConfigDto.setConnection(sdf.format(d));
				} else {
					erpConfigDto.setConnection("Not Started");
				}
				taxpayerMap.put("erpconfig", erpConfigDto);
			} else {
				taxpayerMap.put("isErp", "false");
				taxpayerMap.put("erpconfig", null);
			}

			return taxpayerMap;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public Map<String, Object> getfileprefdeshboard(String gstin, String fyear)
			throws AppException, JsonProcessingException, IOException {
		log.info("getfileprefdeshboard method starts here..");
		Map<String, Object> prefDetail = new HashMap<>();
		if (gstin != null) {
			String prefernce = "M";
			FilePrefernce fileprefernce = finderService.findpreference(gstin, fyear);

			if (Objects.isNull(fileprefernce)) {
				throw new AppException(ExceptionCode._NO_PREFERNCE_SELECTED);
			}
			prefernce = fileprefernce.getPreference();
			FilePreferenceDTO filePreferenceDTO = new FilePreferenceDTO();
			Map<String, List<GstCalendarDto>> dueDateCalendar = null;
			if (prefernce.equalsIgnoreCase("M")) {
				dueDateCalendar = returnService.getDueDateCalendar(gstin, fyear);
				filePreferenceDTO.setDueDateCalendar(dueDateCalendar);
			} else {
				dueDateCalendar = returnService.getDueDateCalendarQuarterly(gstin, fyear);
				filePreferenceDTO.setDueDateCalendar(dueDateCalendar);

			}
			prefDetail.put("filepref", prefernce);
			prefDetail.put("prefDetail", filePreferenceDTO);
			prefDetail.put("gstin-auth", new ObjectMapper().readTree(this.getGstinAuthTokenValidity(gstin)));

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return prefDetail;

	}

	@Override
	public boolean updateTaxpayerBillingAddress(String pan, BillingAddressDTO billingAddressDTO) throws AppException {

		log.info("updateTaxpayerBillingAddress method starts..");

		if (!StringUtils.isEmpty(pan) && !Objects.isNull(billingAddressDTO)) {

			Taxpayer taxpayer = finderService.findTaxpayer(pan);

			if (!Objects.isNull(taxpayer)) {
				log.info("updateTaxpayerBillingAddress successfully retrieves the billing address for {}", taxpayer);

				/*
				 * BillingAddress billingAddress = taxpayer.getBillingAddress(); BillingAddress
				 * billingAddress2 = (BillingAddress) EntityHelper.convert(billingAddressDTO,
				 * BillingAddress.class); if (!Objects.isNull(billingAddress)) { billingAddress
				 * = (BillingAddress) EntityHelper.convert(billingAddress, billingAddress2); }
				 * else { taxpayer.setBillingAddress(billingAddress2); }
				 * taxpayer.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" +
				 * userBean.getUserDto().getLastName());
				 * taxpayer.setUpdationIpAddress(userBean.getIpAddress());
				 * taxpayer.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
				 * crudService.update(taxpayer);
				 */

				log.info("updateTaxpayerBillingAddress method ends..");

				return Boolean.TRUE;

			} else {
				log.error("updateTaxpayerBillingAddress throws the AppException");
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean requestGstnOtp(String gstin) throws AppException {

		log.info("requestGstnOtp method starts..");

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(false);

			String appKey = "";
			String secureKey = "";
			byte[] appKeyBytes;
			try {
				AESEncryption aesEncryption = new AESEncryption();
				secureKey = aesEncryption.generateSecureKey();
				appKeyBytes = aesEncryption.decodeBase64StringTOByte(secureKey);
				appKey = encryptDecrypt.encrypt(appKeyBytes);
			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

			AuthOtpReq authOtpReq = new AuthOtpReq();
			authOtpReq.setAction(GstMetadata.Authentication.OTPREQUEST.toString());
			authOtpReq.setAppKey(appKey);
			authOtpReq.setUsername(taxpayerGstin.getUsername());

			String output = gstConsumer.authenticate(authOtpReq);
			try {
				AuthOtpResp authOtpResp = JsonMapper.objectMapper.readValue(output, AuthOtpResp.class);
				if (authOtpResp != null && authOtpResp.getStatusCd().equals(GstMetadata.STATUS_CODE_1)) {
					redisService.setValue(gstin + "_APPKEY", secureKey, false, 0);
					taxpayerGstin.setAppKey(secureKey);
					this.updateGstnOtp(taxpayerGstin, (long) 0, (byte) 0);
					taxpayerGstin.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
					taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(taxpayerGstin);
					return true;
				}
				log.info("requestGstnOtp method ends..");

				return false;

			} catch (IOException e) {
				log.error("requestGstnOtp IOException for ..{} ", e.getMessage());

				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String requestGstnAuthToken(String gstin, String otp) throws AppException {

		log.info("requestGstnAuthToken method starts..");

		if (!(StringUtils.isEmpty(gstin) && StringUtils.isEmpty(otp))) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(false);

			AuthTokenReq authTokenReq = new AuthTokenReq();
			AESEncryption aesEncryption;

			String secureKey = redisService.getValue(gstin + "_APPKEY");
			String appKey = "";
			byte[] appKeyBytes;
			try {
				aesEncryption = new AESEncryption();
				appKeyBytes = aesEncryption.decodeBase64StringTOByte(secureKey);
				appKey = encryptDecrypt.encrypt(appKeyBytes);
				authTokenReq.setOtp(aesEncryption.encrypt(otp, appKeyBytes));
			} catch (Exception e) {
				log.error("exception ", e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

			authTokenReq.setAction(GstMetadata.Authentication.AUTHTOKEN.toString());
			authTokenReq.setAppKey(appKey);
			authTokenReq.setUsername(taxpayerGstin.getUsername());

			String output = gstConsumer.authenticate(authTokenReq);
			try {
				AuthTokenResp authTokenResp = JsonMapper.objectMapper.readValue(output, AuthTokenResp.class);
				if (authTokenResp != null && authTokenResp.getStatusCd().equals(GstMetadata.STATUS_CODE_1)) {
					taxpayerGstin.setOtp(otp);
					taxpayerGstin.setAuthToken(authTokenResp.getAuthToken());
					taxpayerGstin.setSek(authTokenResp.getSek());
					taxpayerGstin.setMaxAuthExpiryTime(Timestamp
							.from(Instant.now().plus(Duration.ofMinutes(Long.valueOf(authTokenResp.getExpiry())))));
					taxpayerGstin.setAuthServer(redisService.getValue(ApplicationMetadata.SERVER_IP_ADDRESS));

					Long expiry = (long) Integer
							.parseInt(authTokenResp.getExpiry() != null ? authTokenResp.getExpiry() : "0");
					this.updateAuthExpiryTimestamp(taxpayerGstin, "120");
					Long time = Calendar.getInstance().getTimeInMillis() + ((expiry) * 60000);
					this.updateGstnOtp(taxpayerGstin, time, new Byte((byte) 1));
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					String lastTime = sdf.format(new Date(time)) + " IST";

					// on success notify to view returns
					returnService.viewTrackReturnsAsync(taxpayerGstin);

					return lastTime;
				}
				log.info("requestGstnAuthToken method ends..");

				return null;
			} catch (Exception e) {
				log.info("requestGstnAuthToken throws exception for {} ", e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void updateAuthExpiryTimestamp(TaxpayerGstin gstin, String time) {

		gstin.setAuthExpiryTime(Timestamp.from(Instant.now().plus(Duration.ofMinutes(Long.valueOf(330)))));
		gstin.setAuthExpiry(String.valueOf(gstin.getAuthExpiryTime().getTime()));

	}

	private void updateGstnOtp(TaxpayerGstin taxpayerGstin, Long time, Byte isAuth) {

		long refTime = Calendar.getInstance().getTimeInMillis() + (115 * 60000);
		taxpayerGstin.setOtpExpiry(String.valueOf(refTime));
		taxpayerGstin.setAuthExpiry(String.valueOf(time));
		taxpayerGstin.setAuthenticated(isAuth);
		taxpayerGstin.setUpdatedBy(!Objects.isNull(userBean) || !Objects.isNull(userBean.getUserDto())
				? userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName()
				: "SYSTEM_PROCESS");
		taxpayerGstin.setUpdationIpAddress(!Objects.isNull(userBean) ? userBean.getIpAddress()
				: redisService.getValue(ApplicationMetadata.SERVER_IP_ADDRESS));
		crudService.update(taxpayerGstin);
	}

	public void markGstnUnAuth(String gstin) {
		this.updateGstnOtp(finderService.findTaxpayerGstin(gstin), (long) 0, (byte) 0);
	}

	@Override
	public String saveDummyAuthToken(String gstin, String otp) throws AppException {

		log.info("saveDummyAuthToken method starts..");

		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
		String lastTime = sdf.format(Calendar.getInstance().getTime()) + " IST";

		if (!(StringUtils.isEmpty(gstin) && StringUtils.isEmpty(otp))) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			try {
				if (!Objects.isNull(taxpayerGstin)) {
					taxpayerGstin.setOtp(otp);
					taxpayerGstin.setAuthToken(null);
					taxpayerGstin.setSek(null);
					int expiry = 300000;
					Long time = Calendar.getInstance().getTimeInMillis() + expiry;
					taxpayerGstin.setAuthExpiry(String.valueOf(time));
					taxpayerGstin.setAuthenticated(new Byte((byte) 1));
					taxpayerGstin.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
					taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(taxpayerGstin);
					lastTime = sdf.format(new Date(time)) + " IST";

					return lastTime;
				}
				return lastTime;
			} catch (Exception e) {
				e.printStackTrace();
				log.info("saveDummyAuthToken throws excepion for {}", e.getMessage());
				return lastTime;
			}
		} else {
			log.info("saveDummyAuthToken method ends..");
			return lastTime;
		}
	}

	@Override
	public String getGstinAuthTokenValidity(String gstin) throws AppException {

		log.info("getGstinAuthTokenValidity method starts..");

		if (!(StringUtils.isEmpty(gstin))) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
			byte authenticated = 0;

			try {
				if (!Objects.isNull(taxpayerGstin)) {

					authenticated = taxpayerGstin.getAuthenticated();
				}

				JSONObject json = new JSONObject();
				json.put("authenticated", authenticated);
				json.put("limit", "100");

				log.info("getGstinAuthTokenValidity method ends..");

				return json.toString();

			} catch (Exception e) {
				log.error("getGstinAuthTokenValidity throws exception for {}", e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String requestGstnRefreshToken(TaxpayerGstin taxpayerGstin) throws AppException {

		log.info("requestGstnRefreshToken method starts..");

		if (!Objects.isNull(taxpayerGstin)) {

			gstConsumer.setTaxpayerGstin(taxpayerGstin);
			gstConsumer.addTaxpayerHeaders(false);

			String appKey = "";
			AuthRefreshTokenReq authRefreshTokenReq = new AuthRefreshTokenReq();

			try {

				AESEncryption aesEncryption = new AESEncryption();

				appKey = aesEncryption.generateSecureKey();

				byte[] authSEK = aesEncryption.decrypt(taxpayerGstin.getSek(),
						aesEncryption.decodeBase64StringTOByte(taxpayerGstin.getAppKey()));

				authRefreshTokenReq.setAction(GstMetadata.Authentication.REFRESHTOKEN.toString());
				authRefreshTokenReq.setAppKey(aesEncryption.encryptWithBase64(appKey, authSEK));
				authRefreshTokenReq.setUsername(StringUtils.lowerCase(taxpayerGstin.getUsername()));
				authRefreshTokenReq.setAuthToken(taxpayerGstin.getAuthToken());

				String output = gstConsumer.authenticate(authRefreshTokenReq);

				AuthTokenResp authTokenResp = JsonMapper.objectMapper.readValue(output, AuthTokenResp.class);

				if (authTokenResp != null && authTokenResp.getStatusCd().equals(GstMetadata.STATUS_CODE_1)) {

					this.updateTaxpayerGstinToken(taxpayerGstin, authTokenResp, appKey);
				}

				log.info("requestGstnRefreshToken method ends..");
				return null;
			} catch (Exception e) {
				log.error("requestGstnRefreshToken method throws exception for tin {} ", taxpayerGstin.getGstin(), e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void updateTaxpayerGstinToken(TaxpayerGstin taxpayerGstin, AuthTokenResp authTokenResp, String newAppKey) {

		taxpayerGstin = em.find(TaxpayerGstin.class, taxpayerGstin.getId());

		taxpayerGstin.setAuthToken(authTokenResp.getAuthToken());
		taxpayerGstin.setSek(authTokenResp.getSek());
		taxpayerGstin.setAppKey(newAppKey);
		int expiry = Integer.parseInt(authTokenResp.getExpiry() != null ? authTokenResp.getExpiry() : "0");
		Long time = Calendar.getInstance().getTimeInMillis() + expiry;
		taxpayerGstin.setAuthExpiry(String.valueOf(time));
		this.updateAuthExpiryTimestamp(taxpayerGstin, authTokenResp.getExpiry());
		taxpayerGstin.setAuthenticated(new Byte((byte) 1));
		taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
		taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

		crudService.update(taxpayerGstin);

		log.info("auth token updated for {} time {} ", taxpayerGstin.getGstin(), taxpayerGstin.getAuthExpiryTime());
	}

	@Override
	public TaxpayerGstinDTO createTaxpayerGstin(TaxpayerGstinDTO taxpayerGstinDTO) throws AppException {

		log.info("createTaxpayerGstin method starts..");

		if (!(Objects.isNull(taxpayerGstinDTO) && StringUtils.isEmpty(taxpayerGstinDTO.getGstin())
				&& StringUtils.isEmpty(taxpayerGstinDTO.getUsername()))) {

			if (userService.checkGstins()) {

				if (taxpayerGstinDTO.getGstin().trim().length() != 15)
					throw new AppException(ExceptionCode._INVALID_PARAMETER);

				if (!Objects.isNull(finderService.findTaxpayerGstinAll(taxpayerGstinDTO.getGstin())))
					throw new AppException(ExceptionCode._DUPLICATE);

				String pan = taxpayerGstinDTO.getGstin().substring(2, 12);

				log.info("Add new GSTIN... {}", taxpayerGstinDTO.getGstin());

				// create taxpayer if not already exist
				Taxpayer taxpayer = finderService.findTaxpayer(pan);
				if (Objects.isNull(taxpayer)) {
					taxpayer = new Taxpayer();
					taxpayer.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					taxpayer.setCreationIpAddress(userBean.getIpAddress());

					taxpayer.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
					taxpayer.setActive((byte) 1);
					taxpayer.setStatus(Constant.TaxpayerStatus.ACTIVE.toString());
					taxpayer.setPanCard(pan.toUpperCase());
					taxpayer.setRequestId(UUID.randomUUID().toString());
					taxpayer = crudService.create(taxpayer);

					UserTaxpayer userTaxpayer = new UserTaxpayer();
					userTaxpayer.setTaxpayer(taxpayer);
					userTaxpayer.setType(Constant.TaxpayerType.PRIMARY.toString());
					userTaxpayer.setUser(em.getReference(User.class, userBean.getUserDto().getId()));
					userTaxpayer.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					userTaxpayer.setCreationIpAddress(userBean.getIpAddress());
					userTaxpayer = crudService.create(userTaxpayer);
				} else {
					UserTaxpayer userTaxpayer = finderService.findUserTaxpayerByPan(pan);
					if (!userTaxpayer.getUser().getUsername().equalsIgnoreCase(userBean.getUserDto().getUsername())) {
						AppException appException = new AppException();
						appException.setCode(ExceptionCode._NOT_OWNER);
						String username = userTaxpayer.getUser().getUsername();
						int indexat = username.indexOf("@");
						int len = (indexat - 5) < 1 ? 1 : (indexat - 5);
						username = username.substring(0, len) + "*****"
								+ username.substring(indexat, username.length());
						appException.setMessage(String.format(ApplicationMetadata.NOT_OWNER_MSG, username));
						throw appException;
					}
				}

				TaxpayerGstin taxpayerGstin = (TaxpayerGstin) EntityHelper.convert(taxpayerGstinDTO,
						TaxpayerGstin.class);
				taxpayerGstin
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());

				int stateCode = 0;
				State state = null;
				try {
					stateCode = Integer.valueOf(taxpayerGstin.getGstin().substring(0, 2));
					state = em.getReference(State.class, stateCode);
				} catch (Exception e) {
					throw new AppException(ExceptionCode._INVALID_GSTIN_NUMBER);
				}

				State st = new State();
				st.setId(state.getId());
				taxpayerGstin.setStateBean(st);

				/***********/
				String displayName = null;
				String legalName = taxpayer.getLegalName();
				int size = 0;
				if (!Objects.isNull(taxpayer) && !Objects.isNull(taxpayer.getTaxpayerGstins())) {
					size = taxpayer.getTaxpayerGstins().size();
				}

				if (!StringUtils.isEmpty(taxpayer.getLegalName())) {
					displayName = (!StringUtils.isEmpty(taxpayer.getLegalName())
							&& taxpayer.getLegalName().replaceAll("\\s+", "").length() > 6)
									? taxpayer.getLegalName().replaceAll("\\s+", "").substring(0, 7) + "-"
											+ String.format("%02d", stateCode) + "-" + String.format("%03d", (size + 1))
									: taxpayer.getLegalName() + "-" + String.format("%02d", stateCode) + "-"
											+ String.format("%03d", (size + 1));
				}
				/***********/

				taxpayerGstin.setCreationIpAddress(userBean.getIpAddress());
				taxpayerGstin.setActive((byte) 0);
				taxpayerGstin.setStatus(Constant.TaxpayerStatus.INACTIVE.toString());
				taxpayerGstin.setTaxpayer(taxpayer);
				taxpayerGstin.setGstin(taxpayerGstinDTO.getGstin().toUpperCase());

				redisService.setTaxpayerGstin(taxpayerGstin.getGstin() + "_ADDTIN", taxpayerGstin, true, 600);

				// ******************************************************************//
				// call to gstn api for otp request
				// boolean otpFlag = requestGstnOtp(taxpayerGstin.getGstin());
				// uncomment above to request otp from gstn
				// ******************************************************************//

				taxpayerGstinDTO = (TaxpayerGstinDTO) EntityHelper.convert(taxpayerGstin, TaxpayerGstinDTO.class);
				// following code id added to return email,mobile and display
				// name
				if (!userBean.getUserDto().getUserContacts().isEmpty()) {
					taxpayerGstinDTO.setMobileNumber(userBean.getUserDto().getUserContacts().get(0).getMobileNumber());
					taxpayerGstinDTO.setEmailAddress(userBean.getUserDto().getUserContacts().get(0).getEmailAddress());
				}
				taxpayerGstinDTO.setDisplayName(displayName);
				taxpayerGstinDTO.setOtp(legalName);
				log.info("createTaxpayerGstin method ends..");

				return taxpayerGstinDTO;

			} else {
				log.error("createTaxpayerGstin method throws exception..");
				throw new AppException(ExceptionCode._SUBSCRIPTION_QUANTITY_REQUIRED);
			}
		}

		throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
	}

	@Override
	public TaxpayerGstinDTO updateTaxpayerGstin(TaxpayerGstinDTO taxpayerGstinDTO) throws AppException {

		log.info("TaxpayerGstinDTO method starts..");

		if (!(Objects.isNull(taxpayerGstinDTO) && Objects.isNull(taxpayerGstinDTO.getGstin()))) {

			TaxpayerGstin taxpayerGstinEm = finderService.findTaxpayerGstin(taxpayerGstinDTO.getGstin());
			byte gstnauth = 0;
			// TaxpayerGstin taxpayerGstinEm =
			// crudService.find(TaxpayerGstin.class, taxpayerGstinDTO.getId());

			boolean fromRedis = false;
			if (Objects.isNull(taxpayerGstinEm)) {
				taxpayerGstinEm = redisService.getTaxpayerGstin(taxpayerGstinDTO.getGstin() + "_ADDTIN");
				fromRedis = true;
			} else {
				gstnauth = taxpayerGstinEm.getAuthenticated();
			}

			// get taxpayer
			Taxpayer tPayer = em.getReference(Taxpayer.class, taxpayerGstinEm.getTaxpayer().getId());

			String displayName = taxpayerGstinDTO.getDisplayName();
			if (!fromRedis && !StringUtils.isEmpty(displayName)) {
				if (!Objects.isNull(tPayer.getTaxpayerGstins()) && !tPayer.getTaxpayerGstins().isEmpty()) {
					List<TaxpayerGstin> tpgstns = tPayer.getTaxpayerGstins().stream()
							.filter(i -> displayName.equalsIgnoreCase(i.getDisplayName())).collect(Collectors.toList());
					if (!tpgstns.isEmpty()) {
						AppException ae = new AppException();
						ae.setMessage(
								"TIN with same display name already exist please choose a different display name");
						throw ae;
					}
				}
			}
			TaxpayerGstin taxpayerGstin = (TaxpayerGstin) EntityHelper.convert(taxpayerGstinDTO, TaxpayerGstin.class);
			taxpayerGstin
					.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
			taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

			taxpayerGstin.setActive((byte) 1);
			taxpayerGstin.setStatus(Constant.TaxpayerStatus.ACTIVE.toString());

			if (taxpayerGstinEm.getActive() == 0) {
				if (!StringUtils.isEmpty(taxpayerGstinDTO.getOtp())) {
					tPayer.setLegalName(taxpayerGstinDTO.getOtp());
					tPayer.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					tPayer.setUpdationIpAddress(userBean.getIpAddress());
					tPayer.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					tPayer = crudService.update(tPayer);
				}
			}

			taxpayerGstinEm = EntityHelper.convert(taxpayerGstinEm,taxpayerGstin);
			taxpayerGstinEm.setOtp(null);

			if (fromRedis) {
				taxpayerGstinEm.setTaxpayer(tPayer);
				taxpayerGstinEm.setDisplayName(taxpayerGstinDTO.getDisplayName());
				taxpayerGstinEm.setAddress(taxpayerGstinDTO.getAddress());
				taxpayerGstin.setActive((byte)1);
				taxpayerGstin = crudService.create(taxpayerGstinEm);

				UserGstin userGstin = new UserGstin();
				userGstin.setTaxpayerGstin(taxpayerGstinEm);
				userGstin.setType(Constant.TaxpayerGstinType.FILE.toString());
				userGstin.setUser(em.getReference(User.class, userBean.getUserDto().getId()));
				userGstin.setOrganisation(
						em.getReference(Organisation.class, userBean.getUserDto().getOrganisation().getId()));
				userGstin
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				userGstin.setCreationIpAddress(userBean.getIpAddress());
				userGstin.setCreationTime(Timestamp.from(Instant.now()));
				userGstin.setTaxpayerGstin(taxpayerGstin);
				userGstin = crudService.create(userGstin);

				List<String> financialyear = Arrays.asList("2018-2019", "2017-2018");

				financialyear.forEach(data -> {
					redisService.removeUserOrgTaxpayer(userBean.getUserDto().getId() + "-"
							+ userBean.getUserDto().getOrganisation().getId() + "-" + data);
				});
				// logging user action
				MessageDto messageDto = new MessageDto();
				messageDto.setGstin(taxpayerGstinDTO.getGstin());
				apiLoggingService.saveAction(UserAction.ADD_TIN, messageDto);

				// set month year for new gstin object
				updateMonthYearForGstin(taxpayerGstin);

				boolean isVendorUpdateRequired = false;
				List<UserTaxpayer> userTaxpayers = finderService.findPrimaryTaxpayer(userBean.getUserDto().getId());
				if (!Objects.isNull(userTaxpayers) && userTaxpayers.size() == 1) {
					if (!StringUtils.isEmpty(userBean.getUserDto().getRequestId())) {
						Vendor vendor = finderService.findVendorByRequest(userBean.getUserDto().getRequestId());
						if (!Objects.isNull(vendor)) {
							boolean isVerified = false;
							Taxpayer primaryTaxpayer = vendor.getTaxpayer();
							Taxpayer secondaryTaxpayer = tPayer;
							if (!finderService.checkTpMapping(primaryTaxpayer.getId(), secondaryTaxpayer.getId())) {
								TpMapping tpMapping = new TpMapping();
								tpMapping.setPtaxpayer(primaryTaxpayer);
								tpMapping.setStaxpayer(secondaryTaxpayer);
								tpMapping.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
										+ userBean.getUserDto().getLastName());
								tpMapping.setCreationIpAddress(userBean.getIpAddress());
								tpMapping.setCreationTime(Timestamp.from(Instant.now()));
								crudService.create(tpMapping);
							}

							if (!StringUtils.isEmpty(vendor.getGstin())) {
								if (vendor.getGstin().equalsIgnoreCase(taxpayerGstin.getGstin())) {
									isVendorUpdateRequired = true;
								}
							} else if ((!StringUtils.isEmpty(vendor.getPanNumber())
									&& vendor.getPanNumber().equalsIgnoreCase(tPayer.getPanCard()))
									|| StringUtils.isEmpty(vendor.getPanNumber())) {
								isVendorUpdateRequired = true;
							}
							vendor.setUpdatedBy(
									userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
							vendor.setUpdationIpAddress(userBean.getIpAddress());
							vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
							if (isVendorUpdateRequired) {
								vendor.setPanNumber(tPayer.getPanCard());
								vendor.setGstin(taxpayerGstin.getGstin());
								if (!StringUtils.isEmpty(taxpayerGstin.getAddress())) {
									vendor.setAddress(taxpayerGstin.getAddress());
									vendor.setPincode(taxpayerGstin.getPincode());
								}
								if (isVerified)
									vendor.setEmgst((byte) 1);

								crudService.update(vendor);
							} else {
								if (isVerified)
									vendor.setEmgst((byte) 1);
								crudService.update(vendor);
							}
						}

					}
				}

				// get TP mapping with secondary relationship here and check for
				// GSTIN entry
				List<TpMapping> tpMappings = finderService.findSecondaryTpMapping(taxpayerGstin.getTaxpayer().getId());
				if (!Objects.isNull(tpMappings) && !tpMappings.isEmpty()) {
					for (TpMapping tpMapping : tpMappings) {
						Vendor vendor = finderService.findVendorByTpGstin(tpMapping.getPtaxpayer().getId(),
								taxpayerGstin.getGstin());
						if (Objects.isNull(vendor)) {
							List<Vendor> vendors = finderService.findVendorByTpPan(tpMapping.getPtaxpayer().getId(),
									taxpayerGstin.getTaxpayer().getPanCard());
							if (!Objects.isNull(vendors)) {
								Vendor pvendor = vendors.get(0);

								Vendor newVendor = new Vendor();
								newVendor.setTaxpayer(tpMapping.getPtaxpayer());
								newVendor.setType(pvendor.getType());
								newVendor.setPanNumber(taxpayerGstin.getTaxpayer().getPanCard());
								newVendor.setOrganisation(taxpayerGstin.getTaxpayer().getLegalName());
								newVendor.setBusinessName(taxpayerGstin.getDisplayName());
								newVendor.setGstin(taxpayerGstin.getGstin());
								newVendor.setStateBean(taxpayerGstin.getStateBean());
								newVendor.setAddress(taxpayerGstin.getAddress());
								newVendor.setPincode(taxpayerGstin.getPincode());
								newVendor.setContactPerson(userBean.getUserDto().getFirstName() + " "
										+ userBean.getUserDto().getLastName());
								newVendor.setEmgst((byte) 1);

								newVendor.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
										+ userBean.getUserDto().getLastName());
								newVendor.setCreationIpAddress(userBean.getIpAddress());
								newVendor.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
								crudService.create(newVendor);
							}
						}
					}
				}

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", userBean.getUserDto().getUserContacts().get(0).getEmailAddress());
				params.put("gstin", taxpayerGstin.getGstin());
				params.put("legalName", tPayer.getLegalName());
				params.put("name", userBean.getUserDto().getFirstName());
				EmailFactory factory = new EmailFactory();
				factory.sendAddGstinEmail(params);
				log.info("Sending add gstin email to user {}.", params.toString());

			} else {
				taxpayerGstinEm.setAuthenticated(gstnauth);
				taxpayerGstinEm.setAddress(taxpayerGstin.getAddress());
				taxpayerGstinEm.setUsername(taxpayerGstin.getUsername());
				taxpayerGstinEm.setDisplayName(taxpayerGstin.getDisplayName());

				
				taxpayerGstin = this.updateTaxpayer(taxpayerGstinEm);
				List<String> financialyear = Arrays.asList("2018-2019", "2017-2018");

				financialyear.forEach(data -> {
					redisService.removeUserOrgTaxpayer(userBean.getUserDto().getId() + "-"
							+ userBean.getUserDto().getOrganisation().getId() + "-" + data);
				});
			}

			taxpayerGstinDTO = (TaxpayerGstinDTO) EntityHelper.convert(taxpayerGstin, TaxpayerGstinDTO.class);

			log.info("TaxpayerGstinDTO method ends..");

			return taxpayerGstinDTO;

		} else {
			log.info("TaxpayerGstinDTO method throws AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	public TaxpayerGstin updateTaxpayer(TaxpayerGstin taxpayerGstin){
		log.info("updating gstin {}....."+taxpayerGstin.getDisplayName() +" "+taxpayerGstin.getAddress());
		return em.merge(taxpayerGstin);
	}
	

	@Override
	public String deleteTaxpayerGstin(int id) throws AppException {

		log.info("deleteTaxpayerGstin method starts for {} ", id);

		if (id > 0) {

			TaxpayerGstin taxpayerGstin = em.getReference(TaxpayerGstin.class, id);

			if (Objects.isNull(taxpayerGstin)) {
				log.error("requested gstin not found");
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

			// getting gstin of the requested taxpayer gstin
			String gstin = taxpayerGstin.getGstin();

			if (!Objects.isNull(taxpayerGstin.getTaxpayer())) {
				BigInteger rowCnt = (BigInteger) em
						.createNativeQuery(
								"SELECT count(1) FROM vendor WHERE taxpayerId = " + taxpayerGstin.getTaxpayer().getId())
						.getSingleResult();
				if (!Objects.isNull(rowCnt) && rowCnt.intValue() == 0) {
					List<UserGstin> gstins = finderService.findUserGstinByTaxpayer(id);
					for (UserGstin userGstin : gstins) {
						crudService.delete(UserGstin.class, userGstin.getId());
					}
					crudService.delete(TaxpayerGstin.class, id);
					return gstin;
				}
			}

			taxpayerGstin
					.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
			taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			taxpayerGstin.setActive((byte) 0);
			taxpayerGstin.setStatus(Constant.TaxpayerStatus.INACTIVE.toString());

			taxpayerGstin = crudService.update(taxpayerGstin);

			redisService.removeUserOrgTaxpayer(
					userBean.getUserDto().getId() + "-" + userBean.getUserDto().getOrganisation().getId());

			log.info("deleteTaxpayerGstin method ends..");
			return gstin;

		} else {
			log.error("deleteTaxpayerGstin method throws exception");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	@Deprecated
	public TaxpayerDTO getTaxpayerGstinDetail(String pan) throws AppException {

		log.info("getTaxpayerGstinDetail method starts for {} ", pan);

		if (!StringUtils.isEmpty(pan)) {

			boolean allGstin = true;
			boolean isPrimary = true;
			UserDTO userDto = userBean.getUserDto();
			Taxpayer taxpayer = finderService.findTaxpayerByUser(pan, userDto.getId(),
					Constant.TaxpayerType.PRIMARY.toString());
			if (Objects.isNull(taxpayer)) {
				allGstin = false;
				taxpayer = finderService.findTaxpayerByUser(pan, userDto.getId(),
						Constant.TaxpayerType.SECONDARY.toString());
				if (Objects.isNull(taxpayer)) {
					taxpayer = finderService.findTaxpayer(pan);
					isPrimary = false;
				}
			}

			TaxpayerDTO taxpayerDTO = (TaxpayerDTO) EntityHelper.convert(taxpayer, TaxpayerDTO.class);
			List<UserDTO> userDTOs = new ArrayList<>();
			List<UserTaxpayer> userTaxpayers = finderService.findUserTaxpayerByTaxpayer(taxpayer.getId());
			if (!Objects.isNull(userTaxpayers)) {
				if (!userTaxpayers.isEmpty()) {
					for (UserTaxpayer userTaxpayer : userTaxpayers) {
						User u = userTaxpayer.getUser();
						UserDTO dto = new UserDTO();
						dto.setFirstName(u.getFirstName());
						dto.setId(u.getId());
						dto.setLastName(u.getLastName());
						dto.setUsername(u.getUsername());
						dto.setStatus(userTaxpayer.getType());
						userDTOs.add(dto);
					}
				}
			}
			// taxpayerDTO.setUsers(userDTOs);

			if (isPrimary)
				taxpayerDTO.setStatus(Constant.TaxpayerType.PRIMARY.toString());
			else
				taxpayerDTO.setStatus(Constant.TaxpayerType.SECONDARY.toString());

			if (!allGstin) {
				List<UserGstin> userGstins = finderService.findUserGstinByUserAndPan(pan, userDto.getId());
				if (Objects.isNull(userGstins))
					userGstins = new ArrayList<>();
				List<TaxpayerGstinDTO> gstinDTOs = new ArrayList<>();
				for (UserGstin userGstin : userGstins) {
					TaxpayerGstinDTO taxpayerGstinDTO = (TaxpayerGstinDTO) EntityHelper
							.convert(userGstin.getTaxpayerGstin(), TaxpayerGstinDTO.class);
					if (userGstin.getGstReturnBean() == null) {
						taxpayerGstinDTO.setStatus("PRIMARY");
					} else {
						taxpayerGstinDTO.setStatus("SECONDARY");
					}
					gstinDTOs.add(taxpayerGstinDTO);
				}
				taxpayerDTO.setTaxpayerGstins(gstinDTOs);
			} else {
				for (TaxpayerGstinDTO taxpayerGstinDTO : taxpayerDTO.getTaxpayerGstins()) {
					taxpayerGstinDTO.setStatus("PRIMARY");
				}
			}

			log.info("getTaxpayerGstinDetail method ends for {} ");

			return taxpayerDTO;

		} else {
			log.error("getTaxpayerGstinDetail method throws the app exception");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public List<ErpLog> getErpLogs() throws AppException {

		log.info("getErpLogs method starts");

		int erpId = userBean.getUserDto().getId();
		User userEm = em.find(User.class, erpId);
		if (!Objects.isNull(userEm.getOrganisation())) {
			if (!userEm.getOrganisation().getId().equals(userBean.getUserDto().getOrganisation().getId())) {
				UserDTO userOrg = finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
				erpId = userOrg.getId();
			}
		} else {
			UserDTO userOrg = finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
			erpId = userOrg.getId();
		}

		List<ErpLog> erpLogs = finderService.getErpLogs(erpId);

		log.info("getErpLogs method ends");
		return erpLogs;
	}

	@Override
	public boolean removeErpConfig(String provider) throws AppException {

		log.info("getTaxpayerGstinDetail method starts for {} ", provider);

		if (!StringUtils.isEmpty(provider)) {

			ErpConfig e = finderService.findErpConfigByUserProvider(userBean.getUserDto().getId(), provider);
			if (!Objects.isNull(e)) {
				crudService.delete(ErpConfig.class, e.getId());
				return Boolean.TRUE;
			}

			return Boolean.FALSE;

		} else {
			log.error("getTaxpayerGstinDetail method throws the app exception");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateErpConfig(ErpConfigDTO erpConfigDTO)
			throws AppException, JsonProcessingException, IOException {

		log.info("updateErpConfig method starts for {} ", userBean.getUserDto().getUsername());

		FTPClient client = null;
		boolean flag = false;

		if (!Objects.isNull(erpConfigDTO)) {

			ErpConfig erpConfig = (ErpConfig) EntityHelper.convert(erpConfigDTO, ErpConfig.class);

			erpConfig.setActive((byte) 1);
			User userEm = em.find(User.class, userBean.getUserDto().getId());

			erpConfig.setUser(userEm);

			ErpConfig temp = finderService.findErpConfigByUser(userEm.getId());

			if (!StringUtils.isEmpty(erpConfig.getUniqueKey())) {
				ErpConfig e = finderService.findErpConfigUsingUniqueKey(erpConfig.getUniqueKey(),
						erpConfig.getSerialNo());
				if (!Objects.isNull(e)) {
					throw new AppException(ExceptionCode._DUPLICATE);
				}
			}

			if (erpConfigDTO.getProvider().equalsIgnoreCase("FTP")) {
				FTPService ftpService = new FTPService();
				String connection = erpConfigDTO.getConnection();
				JsonNode objects = JsonMapper.objectMapper.readTree(connection.toString());
				JsonNode hostnameNode = objects.get("hostname");
				String hostname = hostnameNode.asText();
				JsonNode usernameNode = objects.get("username");
				String username = usernameNode.asText();
				JsonNode passwordNode = objects.get("password");
				String password = passwordNode.asText();
				client = ftpService.connect(hostname, username, password);

				if (!Objects.isNull(client)) {

					if (temp != null) {

						erpConfig.setId(temp.getId());
						erpConfig.setUpdatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						erpConfig.setUpdationIpAddress(userBean.getIpAddress());
						erpConfig.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

						crudService.update(erpConfig);
					} else {

						erpConfig.setCreatedBy(
								userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
						erpConfig.setCreationIpAddress(userBean.getIpAddress());
						erpConfig.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

						crudService.create(erpConfig);
					}
					return Boolean.TRUE;
				}
			} else if (erpConfigDTO.getProvider().equalsIgnoreCase("Ginesys")
					|| erpConfigDTO.getProvider().equalsIgnoreCase("navision")) {
				if (temp != null) {

					erpConfig.setId(temp.getId());
					erpConfig.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					erpConfig.setUpdationIpAddress(userBean.getIpAddress());
					erpConfig.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.update(erpConfig);
				} else {
					erpConfig.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					erpConfig.setCreationIpAddress(userBean.getIpAddress());
					erpConfig.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.create(erpConfig);
				}
				return Boolean.TRUE;
			} else if (erpConfigDTO.getProvider().equalsIgnoreCase("tally")) {

				String connection = erpConfigDTO.getConnection();
				JsonNode objects = JsonMapper.objectMapper.readTree(connection.toString());
				JsonNode serverAddressNode = objects.get("serverAddress");
				String serverAddress = serverAddressNode.asText();
				JsonNode portNode = objects.get("port");
				String port = portNode.asText();
				if (temp != null) {
					erpConfig.setId(temp.getId());
					erpConfig.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					erpConfig.setUpdationIpAddress(userBean.getIpAddress());
					erpConfig.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.update(erpConfig);
				} else {
					erpConfig.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					erpConfig.setCreationIpAddress(userBean.getIpAddress());
					erpConfig.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.create(erpConfig);
				}
				return Boolean.TRUE;
			} else if (erpConfigDTO.getProvider().equalsIgnoreCase("SAP")) {

				String connection = erpConfigDTO.getConnection();
				JsonNode objects = JsonMapper.objectMapper.readTree(connection.toString());
				JsonNode serverHostNode = objects.get("serverHost");
				String serverHost = serverHostNode.asText();
				JsonNode systemNumNode = objects.get("systemNo");
				String systemNum = systemNumNode.asText();
				JsonNode clientNode = objects.get("client");
				String clientName = clientNode.asText();
				JsonNode loginUserNode = objects.get("loginUser");
				String loginUser = loginUserNode.asText();
				JsonNode loginPasswordNode = objects.get("logonPassword");
				String loginPassword = loginPasswordNode.asText();
				JsonNode loginLanguageNode = objects.get("logonLanguage");
				String loginLanguage = loginLanguageNode.asText();
				JsonNode peakLimitNode = objects.get("peakLimit");
				String peakLimit = peakLimitNode.asText();
				JsonNode poolCapacityNode = objects.get("poolCapacity");
				String poolCapacity = poolCapacityNode.asText();

				if (temp != null) {
					erpConfig.setId(temp.getId());
					erpConfig.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					erpConfig.setUpdationIpAddress(userBean.getIpAddress());
					erpConfig.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));

					crudService.update(erpConfig);
				} else {

					erpConfig.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					erpConfig.setCreationIpAddress(userBean.getIpAddress());
					erpConfig.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.create(erpConfig);
				}

				return Boolean.TRUE;
			}
		} else {
			log.info("updateErpConfig method throws the AppExeption");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		log.info("updateErpConfig method ends..");

		return flag;
	}

	@Override
	public ErpConfigDTO getErpConfig() throws AppException {

		if (!Objects.isNull(userBean.getUserDto())) {

			int erpId = userBean.getUserDto().getId();
			User userEm = em.find(User.class, erpId);
			if (!Objects.isNull(userEm.getOrganisation())) {
				if (!userEm.getOrganisation().getId().equals(userBean.getUserDto().getOrganisation().getId())) {
					UserDTO userOrg = finderService
							.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
					erpId = userOrg.getId();
				}
			} else {
				UserDTO userOrg = finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
				erpId = userOrg.getId();
			}

			ErpConfig erpConfig = finderService.findErpConfigByUser(erpId);

			if (!Objects.isNull(erpConfig)) {
				ErpConfigDTO erpConfigDto = (ErpConfigDTO) EntityHelper.convert(erpConfig, ErpConfigDTO.class);
				ErpLog erpLog = finderService.getErpLog(userBean.getUserDto().getId(), "File data processed%");
				if (!Objects.isNull(erpLog)) {
					long time = erpLog.getCreationTime().getTime();
					Date d = new Date(time);
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
					erpConfigDto.setConnection(sdf.format(d));
				} else {
					erpConfigDto.setConnection("Not Started");
				}
				return erpConfigDto;
			}

			return null;

		} else {
			log.info("getErpConfig method throws the AppExeption");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public TaxpayerGstinDTO getTaxpayerGstin(String gstin, String fyear) throws AppException {
		log.info("getTaxpayerGstin method starts for {}", gstin);
		if (!StringUtils.isEmpty(gstin)) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
			if (!Objects.isNull(taxpayerGstin)) {
				TaxpayerGstinDTO taxpayerGstinDTO = (TaxpayerGstinDTO) EntityHelper.convert(taxpayerGstin,
						TaxpayerGstinDTO.class);
				List<UserDTO> userDTOs = new ArrayList<>();
				List<UserGstin> userGstins = finderService.findUserGstinByTaxpayer(taxpayerGstin.getId());
				if (!Objects.isNull(userGstins)) {
					if (!userGstins.isEmpty()) {
						for (UserGstin userGstin : userGstins) {
							User u = userGstin.getUser();
							UserDTO dto = new UserDTO();
							dto.setFirstName(u.getFirstName());
							dto.setId(userGstin.getId());
							dto.setLastName(u.getLastName());
							dto.setUsername(u.getUsername());
							dto.setPassword(userGstin.getType());
							if (!Objects.isNull(userGstin.getGstReturnBean()))
								dto.setStatus(userGstin.getGstReturnBean().getName());
							userDTOs.add(dto);
						}
					}
				}
				taxpayerGstinDTO.setUsers(userDTOs);
				log.info("getTaxpayerGstin method ends..");

				taxpayerGstinDTO.setType(taxpayerGstin.getTaxpayer().getPanCard());
				taxpayerGstinDTO.setStatus(taxpayerGstin.getTaxpayer().getLegalName());
				FilePrefernce fileprefernce = finderService.findpreference(gstin, fyear);
				if (Objects.isNull(fileprefernce)) {
					throw new AppException(ExceptionCode._NO_PREFERNCE_SELECTED);
				}
				taxpayerGstinDTO.setUserPreference(fileprefernce.getPreference());

				return taxpayerGstinDTO;
			} else {
				log.error("gstin {} not register with esaemygst", gstin);

				throw new AppException(ExceptionCode._INVALID_GSTIN);
			}
		} else {
			log.error("getTaxpayerGstin method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean addTaxpayerUser(String pan, String username) throws AppException {

		log.info("addTaxpayerUser method starts for {},{}", pan, username);

		if (!(StringUtils.isEmpty(pan) && StringUtils.isEmpty(username))) {

			Taxpayer taxpayer = finderService.findTaxpayer(pan);
			UserDTO user = finderService.getExistingUser(username);
			String panNumber = "", company = "", name = "", userName = "", toEmail = "";

			if (Objects.isNull(taxpayer)) {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			} else if (!Objects.isNull(user)) {

				UserContact userContact = finderService.getUserContact(user.getId());

				if (finderService.findUserTaxpayer(user.getId(), taxpayer.getId()))
					throw new AppException(ExceptionCode._DUPLICATE_PAN_ACCESS);

				if (user.getId().equals(userBean.getUserDto().getId()))
					throw new AppException(ExceptionCode._SELF_ACCESS);

				UserTaxpayer userTaxpayer = new UserTaxpayer();
				userTaxpayer.setTaxpayer(taxpayer);
				userTaxpayer.setUser(em.getReference(User.class, user.getId()));
				userTaxpayer.setType(TaxpayerType.SECONDARY.toString());

				panNumber = taxpayer.getPanCard();
				company = taxpayer.getLegalName();
				toEmail = userContact.getEmailAddress();
				userName = user.getFirstName() + " " + user.getLastName();
				name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();

				userTaxpayer
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				userTaxpayer.setCreationIpAddress(userBean.getIpAddress());
				userTaxpayer.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));

				crudService.create(userTaxpayer);
			} else {
				User user1 = new User();
				user1.setUsername(username);
				user1.setActive((byte) 0);
				user1.setFirstName("Pending");
				user1.setLastName("Confirmation");
				user1.setStatus(UserStatus.INACTIVE.toString());
				String randomPass = Utility.randomString(10);
				try {
					user1.setPassword(PasswordHash.createHash(randomPass));
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
					throw new AppException();
				}

				UserContact userContact = new UserContact();
				userContact.setEmailAddress(username);
				userContact.setEmailAuth((byte) 0);
				userContact.setMobileNumber("");
				userContact.setMobileAuth((byte) 0);
				userContact.setUser(user1);
				List<UserContact> contacts = new ArrayList<>();
				contacts.add(userContact);
				user1.setUserContacts(contacts);

				user1.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				user1.setCreationIpAddress(userBean.getIpAddress());
				user1.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
				user1 = crudService.create(user1);

				UserTaxpayer userTaxpayer = new UserTaxpayer();
				userTaxpayer.setTaxpayer(taxpayer);
				userTaxpayer.setUser(user1);
				userTaxpayer.setType(TaxpayerType.SECONDARY.toString());
				userTaxpayer.setUser(user1);
				userTaxpayer
						.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				userTaxpayer.setCreationIpAddress(userBean.getIpAddress());
				userTaxpayer.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
				crudService.create(userTaxpayer);

				panNumber = taxpayer.getPanCard();
				company = taxpayer.getLegalName();
				toEmail = user1.getUserContacts().get(0).getEmailAddress();
				userName = "User";
				name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", toEmail);
				params.put("username", username);
				params.put("password", randomPass);
				EmailFactory factory = new EmailFactory();
				factory.sendNewUserTempEmail(params);
				log.info("Sending new user email to user for pan access {}.", params.toString());
			}

			Map<String, String> params = new HashMap<>();
			params.put("toEmail", toEmail);
			params.put("panNumber", panNumber);
			params.put("company", company);
			params.put("name", name);
			params.put("userName", userName);
			EmailFactory factory = new EmailFactory();
			factory.sendAddPanUserEmail(params);
			log.info("Sending pan user added email to user {}.", params.toString());

			return Boolean.TRUE;
		}

		log.info("addTaxpayerUser method ends..");
		return Boolean.FALSE;
	}

	@Override
	public boolean removeTaxpayerUser(String pan, String username) throws AppException {

		log.info("removeTaxpayerUser method starts for {},{}", pan, username);

		if (!(StringUtils.isEmpty(pan) && StringUtils.isEmpty(username))) {

			UserTaxpayer userTaxpayer = finderService.findUserTaxpayerByUser(username, pan);

			if (userTaxpayer != null) {
				crudService.delete(UserTaxpayer.class, userTaxpayer.getId());

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", userTaxpayer.getUser().getUserContacts().get(0).getEmailAddress());
				params.put("panNumber", userTaxpayer.getTaxpayer().getPanCard());
				params.put("company", userTaxpayer.getTaxpayer().getLegalName());
				params.put("name", userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
				params.put("userName",
						userTaxpayer.getUser().getFirstName() + " " + userTaxpayer.getUser().getLastName());
				EmailFactory factory = new EmailFactory();
				factory.sendDeletePanUserEmail(params);
				log.info("Sending pan user delete email to user {}.", params.toString());

				return Boolean.TRUE;
			}
		}

		log.info("removeTaxpayerUser method ends..");

		return Boolean.FALSE;
	}

	@Override
	public boolean removeGstinUser(String gstin, Integer id) throws AppException {

		log.info("removeGstinUser method starts for {},{}", gstin, id);

		if (!(StringUtils.isEmpty(gstin) && Objects.isNull(id))) {
			UserGstin userGstin = finderService.findUserGstinById(id);

			if (userGstin != null) {
				crudService.delete(UserGstin.class, userGstin.getId());

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", userGstin.getUser().getUserContacts().get(0).getEmailAddress());
				params.put("gstinNumber", userGstin.getTaxpayerGstin().getGstin());
				params.put("company", userGstin.getTaxpayerGstin().getTaxpayer().getLegalName());
				params.put("name", userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
				params.put("userName", userGstin.getUser().getFirstName() + " " + userGstin.getUser().getLastName());
				EmailFactory factory = new EmailFactory();
				factory.sendDeleteGstinUserEmail(params);
				log.info("Sending gstin user delete email to user {}.", params.toString());

				return Boolean.TRUE;
			}
		}
		log.info("removeGstinUser method ends..");
		return Boolean.FALSE;
	}

	@Override
	public boolean addGstinUser(String gstin, String username, List<UserGstinDTO> userGstinDTOs) throws AppException {

		log.info("addGstinUser method starts..");

		if (!(StringUtils.isEmpty(gstin) && StringUtils.isEmpty(username) && Objects.isNull(userGstinDTOs)
				&& userGstinDTOs.isEmpty())) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
			UserDTO user = finderService.getExistingUser(username);
			String gstinNumber = "", company = "", name = "", userName = "", toEmail = "";

			if (Objects.isNull(taxpayerGstin)) {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			} else if (!Objects.isNull(user)) {

				UserContact userContact = finderService.getUserContact(user.getId());

				for (UserGstinDTO userGstinDTO : userGstinDTOs) {

					if (finderService.findUserGstin(user.getId(), taxpayerGstin.getId(),
							userGstinDTO.getGstReturnBean().getId()))
						throw new AppException(ExceptionCode._DUPLICATE_GSTIN_ACCESS);

					if (user.getId().equals(userBean.getUserDto().getId()))
						throw new AppException(ExceptionCode._SELF_ACCESS);

					UserGstin userGstin = (UserGstin) EntityHelper.convert(userGstinDTO, UserGstin.class);
					userGstin.setTaxpayerGstin(taxpayerGstin);
					userGstin.setUser(em.getReference(User.class, user.getId()));

					userGstin.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					userGstin.setCreationIpAddress(userBean.getIpAddress());
					userGstin.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.create(userGstin);
				}

				gstinNumber = taxpayerGstin.getGstin();
				company = taxpayerGstin.getTaxpayer().getLegalName();
				userName = user.getFirstName() + " " + user.getLastName();
				name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();
				toEmail = userContact.getEmailAddress();

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", toEmail);
				params.put("gstinNumber", gstinNumber);
				params.put("company", company);
				params.put("name", name);
				params.put("userName", userName);
				EmailFactory factory = new EmailFactory();
				factory.sendAddGstinUserEmail(params);
				log.info("Sending gstin user added email to user {}.", params.toString());

				return Boolean.TRUE;
			} else {
				User user1 = new User();
				user1.setUsername(username);
				user1.setActive((byte) 0);
				user1.setFirstName("Pending");
				user1.setLastName("Confirmation");
				user1.setStatus(UserStatus.INACTIVE.toString());
				String randomPass = Utility.randomString(10);
				try {
					user1.setPassword(PasswordHash.createHash(randomPass));
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
					throw new AppException();
				}

				UserContact userContact = new UserContact();
				userContact.setEmailAddress(username);
				userContact.setMobileAuth((byte) 0);
				userContact.setMobileNumber("");
				userContact.setUser(user1);
				List<UserContact> contacts = new ArrayList<>();
				contacts.add(userContact);
				user1.setUserContacts(contacts);
				user1.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				user1.setCreationIpAddress(userBean.getIpAddress());
				user1.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
				user1 = crudService.create(user1);

				for (UserGstinDTO userGstinDTO : userGstinDTOs) {
					UserGstin userGstin = (UserGstin) EntityHelper.convert(userGstinDTO, UserGstin.class);
					userGstin.setTaxpayerGstin(taxpayerGstin);
					userGstin.setUser(user1);
					crudService.create(userGstin);
				}

				gstinNumber = taxpayerGstin.getGstin();
				company = taxpayerGstin.getTaxpayer().getLegalName();
				userName = "User";
				name = userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName();
				toEmail = user1.getUserContacts().get(0).getEmailAddress();

				Map<String, String> params = new HashMap<>();
				params.put("toEmail", username);
				params.put("username", username);
				params.put("password", randomPass);
				params.put("gstinNumber", gstinNumber);
				params.put("company", company);
				params.put("name", name);
				EmailFactory factory = new EmailFactory();
				factory.sendNewUserTempEmail(params);
				log.info("Sending new user email to user for pan access {}.", params.toString());

				return Boolean.TRUE;
			}
		}

		log.info("addGstinUser method ends..");

		return Boolean.FALSE;
	}

	@Override
	public List<BrtMappingDTO> getBrtMappingByGstin(String gstin) throws AppException {

		log.info("getBrtMappingByGstin method starts for {}", gstin);

		if (!StringUtils.isEmpty(gstin)) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstinByGstin(gstin);
			String type = taxpayerGstin.getType();

			String[] temp = {};
			if (!StringUtils.isEmpty(type)) {
				temp = type.split(":");
			}
			log.info("getBrtMappingByGstin method ends for {}", gstin);

			return finderService.getBrtMapping(temp);
		} else {
			log.error("getBrtMappingByGstin method throws the AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Set<GstReturnDTO> getGstReturnByGstin(String gstin) throws AppException {

		log.info("getGstReturnByGstin method starts for {}", gstin);

		if (!StringUtils.isEmpty(gstin)) {
			List<BrtMappingDTO> brtMappingDTOs = getBrtMappingByGstin(gstin);
			SortedSet<GstReturnDTO> gstReturnDTOs = new TreeSet<>(Comparator.comparing(GstReturnDTO::getId));

			for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
				GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
				if (!gstReturnDTOs.contains(gstReturnDTO)) {
					gstReturnDTOs.add(gstReturnDTO);
				}
			}
			log.info("getGstReturnByGstin method ends..");

			return gstReturnDTOs;
		} else {

			log.error("getGstReturnByGstin method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	@Deprecated
	public Map<String, Object> getGstReturnByFrequencyGstin(String gstin) throws AppException {

		log.info("getGstReturnByFrequencyGstin method starts for {}", gstin);

		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

		if (!Objects.isNull(taxpayerGstin)) {
			Map<String, Object> map = new HashMap<>();

			SimpleDateFormat sdfBread = new SimpleDateFormat("MMM: yy");

			List<GstinAction> actions = finderService.findGstinActionByTaxpayerGstinId(taxpayerGstin.getId());

			List<BrtMappingDTO> brtMappingDTOs = getBrtMappingByGstin(gstin);
			TreeSet<GstReturnDTO> monthlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
			TreeSet<GstReturnDTO> quarterlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
			TreeSet<GstReturnDTO> annuallyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());

			map.put(GstReturnFrequency.MONTHLY.toString(), monthlyComp);
			map.put(GstReturnFrequency.QUARTERLY.toString(), quarterlyComp);
			map.put(GstReturnFrequency.ANNUALLY.toString(), annuallyComp);

			for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
				GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
				if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.MONTHLY.toString()))
					monthlyComp.add(gstReturnDTO);
				else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.QUARTERLY.toString()))
					quarterlyComp.add(gstReturnDTO);
				else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.ANNUALLY.toString()))
					annuallyComp.add(gstReturnDTO);

			}

			String start = redisService
					.getValue(userBean.getUserDto().getUsername() + ApplicationMetadata.FINANCIAL_YEAR_START);
			String end = redisService
					.getValue(userBean.getUserDto().getUsername() + ApplicationMetadata.FINANCIAL_YEAR_END);

			SimpleDateFormat fullDate = new SimpleDateFormat("dd MMM yyyy");

			if (StringUtils.isEmpty(start) || StringUtils.isEmpty(end)) {
				Calendar tcal = Calendar.getInstance();
				int month = tcal.get(Calendar.MONTH);
				int year = tcal.get(Calendar.YEAR);
				if (month < 3) {
					start = String.valueOf((year - 1));
					end = String.valueOf(year);
				} else {
					start = String.valueOf(year);
					end = String.valueOf(year + 1);
				}
			}

			String financialYear = start + " - " + end;

			Date gstStartDate = SerializationUtils
					.deserialize(redisService.getBytes(ApplicationMetadata.GST_START_DATE));
			Calendar cal = Calendar.getInstance();
			Calendar prevCal = Calendar.getInstance();
			Date currentDate = cal.getTime();
			cal.set(Integer.parseInt(start), 3, 1);
			Date finStartDate = cal.getTime();
			cal.set(Integer.parseInt(end), 2, 1);
			Date finEndDate = cal.getTime();

			Calendar dateSetter = Calendar.getInstance();
			dateSetter.set(Integer.parseInt(start), 5, 30);
			Date q1EndDate = dateSetter.getTime();
			dateSetter.set(Integer.parseInt(start), 8, 30);
			Date q2EndDate = dateSetter.getTime();
			dateSetter.set(Integer.parseInt(start), 11, 31);
			Date q3EndDate = dateSetter.getTime();
			dateSetter.set(Integer.parseInt(end), 2, 31);
			Date q4EndDate = dateSetter.getTime();

			List<Date> quarterDates = new ArrayList<>();
			quarterDates.add(q1EndDate);
			quarterDates.add(q2EndDate);
			quarterDates.add(q3EndDate);
			quarterDates.add(q4EndDate);

			dateSetter.set(Integer.parseInt(end), 11, 31);
			Date annualEndDate = dateSetter.getTime();

			List<Date> annualDates = new ArrayList<>();
			annualDates.add(annualEndDate);

			List<String> monthYears = new ArrayList<>();
			while (finStartDate.compareTo(finEndDate) <= 0) {
				if (finEndDate.compareTo(currentDate) <= 0) {
					if (gstStartDate.compareTo(finEndDate) <= 0) {

						prevCal.setTime(cal.getTime());
						prevCal.add(Calendar.MONTH, -1);
						boolean startFlag = false;
						Date initDate = prevCal.getTime();
						startFlag = gstStartDate.compareTo(initDate) > 0;

						String month = String.valueOf(finEndDate.getMonth() + 1);
						month = month.length() == 1 ? "0" + month : month;
						month = month + (1900 + finEndDate.getYear());
						String my = Utility.getMonth(finEndDate.getMonth());
						monthYears.add(my);

						String prevMonth = String.valueOf(prevCal.getTime().getMonth() + 1);
						prevMonth = prevMonth.length() == 1 ? "0" + prevMonth : prevMonth;
						prevMonth = prevMonth + (1900 + prevCal.getTime().getYear());

						for (GstReturnDTO gstReturnDTO : monthlyComp) {

							String status = ReturnCalendar.BLANK.toString();
							GstinAction temp = new GstinAction();
							temp.setParticular(month);
							temp.setReturnType(gstReturnDTO.getCode());
							temp.setStatus(ReturnCalendar.FILED.toString());
							Date returnDueDate = gstReturnDTO.getDueDate();
							Date returnStartDate = gstReturnDTO.getStartDate();

							Calendar newCal = Calendar.getInstance();
							newCal.set(Calendar.DATE, returnDueDate.getDate());
							newCal.set(Calendar.YEAR, 1900 + finEndDate.getYear());
							newCal.set(Calendar.MONTH, finEndDate.getMonth());
							newCal.add(Calendar.MONTH, 1);
							if (actions.contains(temp)) {
								temp = actions.get(actions.indexOf(temp));
							} else {
								temp = null;
							}

							Date newTime = newCal.getTime();

							Calendar newCal2 = Calendar.getInstance();
							newCal2.set(Calendar.DATE, returnStartDate.getDate());
							Date newStartTime = newCal2.getTime();

							GstCalendar calendar = AppConfig.getGstCalendarByCode(month, gstReturnDTO.getCode());
							if (!Objects.isNull(calendar)) {
								newTime = calendar.getDueDate();
								newStartTime = calendar.getStartDate();
							}

							double diff = currentDate.getTime() - newTime.getTime();
							double startDiff = currentDate.getTime() - newStartTime.getTime();

							if (!Objects.isNull(temp)
									&& temp.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
								status = ReturnCalendar.FILED.toString();
								Date actionDate = temp.getActionDate();
								map.put(my + "-" + gstReturnDTO.getCode() + "-" + month + "-diff",
										fullDate.format(actionDate));
							} else {
								diff = diff / (24 * 60 * 60 * 1000);
								if (startDiff >= 0.0 && diff <= 0.0) {
									status = ReturnCalendar.APPROACHING.toString();
								} else if (diff > 0.0) {
									status = ReturnCalendar.LOCKED.toString();
									if (gstReturnDTO.getMandatory() == 1) {
										status = ReturnCalendar.OVERDUE.toString();
									}
									if (currentDate.getDate() >= 11 && currentDate.getDate() <= 15
											&& gstReturnDTO.getCode().equalsIgnoreCase(ReturnType.GSTR1.toString())) {
										// status = ReturnCalendar.ONHOLD.toString();
										status = ReturnCalendar.OVERDUE.toString();

									}
								}

								if (gstReturnDTO.getDependent() > 0) {
									GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getDependent());
									GstinAction temp1 = new GstinAction();
									temp1.setReturnType(gstReturn.getCode());
									temp1.setStatus(ReturnCalendar.FILED.toString());
									if (gstReturn.getPrevMonth() == 1)
										temp1.setParticular(prevMonth);
									else
										temp1.setParticular(month);
									if (!actions.contains(temp1)) {
										status = status + "_LOCKED";
									}

									if (gstReturn.getPrevMonth() == 1 && startFlag)
										status = status.replaceAll("_LOCKED", "");
								}

								map.put(my + "-" + gstReturnDTO.getCode() + "-" + month + "-diff",
										Double.valueOf(diff).intValue());
							}

							map.put(my + "-" + gstReturnDTO.getCode() + "-" + month + "-due", fullDate.format(newTime));
							map.put(my + "-" + gstReturnDTO.getCode() + "-" + month, status);
							map.put(my + "-key", month);
							map.put(my + "-value", my + " " + (1900 + finEndDate.getYear()));

							// Introduces this because of pre process to
							// incorporate Ledger in workflow
							if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {

								String name = "";
								if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {
									String temp11 = gstReturnDTO.getPreProcess();
									if (temp11.contains("-")) {
										String[] values = temp11.split("-");
										name = values[0];
									} else {
										name = gstReturnDTO.getPreProcess();
									}
								}

								String status1 = ReturnCalendar.BLANK.toString();
								GstinAction temp1 = new GstinAction();
								temp1.setParticular(month);
								temp1.setReturnType(gstReturnDTO.getCode());

								if (actions.contains(temp1)) {
									temp1 = actions.get(actions.indexOf(temp1));
								} else {
									temp1 = null;
								}

								if (!Objects.isNull(temp1)
										&& temp1.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
									status1 = ReturnCalendar.FILED.toString();
									map.put(my + "-" + name + "-" + month + "-diff", "0");
								} else {
									status1 = status;
									map.put(my + "-" + name + "-" + month + "-diff", Double.valueOf(diff).intValue());
								}

								map.put(my + "-" + name + "-" + month + "-due", fullDate.format(newTime));
								map.put(my + "-" + name + "-" + month, status1);
								map.put(my + "-key", month);
								map.put(my + "-value", my + " " + (1900 + finEndDate.getYear()));
								map.put(my + "-breadcrumb", sdfBread.format(finEndDate).replace(":", "'"));
							}
						}
					}
				}
				cal.add(Calendar.MONTH, -1);
				finEndDate = cal.getTime();
			}

			// finStartDate
			// currentDate

			// to check next financial year add +1 year in finStartDate and
			// check if currentDate lies between
			// finStartDate+1 and finStartDate (1/4/year)

			Calendar tempCal = Calendar.getInstance();
			tempCal.setTime(finStartDate);
			tempCal.add(Calendar.YEAR, 1);
			Date nextStartDate = tempCal.getTime();
			int nextYear = nextStartDate.getYear();
			tempCal.set(1900 + nextYear + 1, 2, 31);
			Date nextEndDate = tempCal.getTime();
			if (nextStartDate.compareTo(currentDate) <= 0 && currentDate.compareTo(nextEndDate) <= 0) {
				map.put("next-year", 1900 + nextYear);
			}
			map.put("next-fy", 1900 + nextYear + "-" + (1900 + nextYear + 1));

			// to check previous financial year add -2 year in finStartDate and
			// check if gstStart is less
			// than finStartDate-2 and finStartDate (31/3/year)

			tempCal.setTime(finStartDate);
			tempCal.add(Calendar.YEAR, -1);
			Date prevStartDate = tempCal.getTime();
			int prevYear = prevStartDate.getYear();
			tempCal.set(1900 + prevYear + 1, 2, 31);
			Date prevEndDate = tempCal.getTime();
			if (gstStartDate.compareTo(prevStartDate) <= 0) {
				map.put("prev-year", 1900 + prevYear);
			} else if (prevStartDate.compareTo(gstStartDate) <= 0 && gstStartDate.compareTo(prevEndDate) <= 0) {
				map.put("prev-year", 1900 + prevYear);
			}
			map.put("prev-fy", 1900 + prevYear + "-" + (1900 + prevYear + 1));

			/*
			 * for (Date qDate : quarterDates) {
			 * 
			 * if (finEndDate.compareTo(qDate) <= 0) { if
			 * (gstStartDate.compareTo(finEndDate) <= 0) { for (GstReturnDTO gstReturnDTO :
			 * quarterlyComp) {
			 * 
			 * } } } }
			 * 
			 * for (Date qDate : annualDates) {
			 * 
			 * for (GstReturnDTO gstReturnDTO : annuallyComp) {
			 * 
			 * } }
			 */

			// Introduces this because of pre process to incorporate Ledger in
			// workflow
			GstReturnDTO tempReturn = new GstReturnDTO();
			for (GstReturnDTO gstReturnDTO : monthlyComp) {
				if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {

					String name = "", description = "";
					if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {
						String temp11 = gstReturnDTO.getPreProcess();
						if (temp11.contains("-")) {
							String[] values = temp11.split("-");
							name = values[0];
							description = values[1];
						} else {
							name = gstReturnDTO.getPreProcess();
							description = gstReturnDTO.getPreProcess();
						}
					}

					java.sql.Date tempDt = gstReturnDTO.getDueDate();
					java.sql.Date tempDt2 = new java.sql.Date(tempDt.getYear(), tempDt.getMonth(),
							tempDt.getDate() - 1);
					tempReturn.setId(gstReturnDTO.getId() * 100);
					tempReturn.setCode(name);
					tempReturn.setDescription(description);
					tempReturn.setDueDate(tempDt2);
					tempReturn.setFrequency(gstReturnDTO.getFrequency());
					tempReturn.setName(name);
				}
			}
			if (tempReturn.getFrequency().equals(GstReturnFrequency.MONTHLY.toString()))
				monthlyComp.add(tempReturn);
			else if (tempReturn.getFrequency().equals(GstReturnFrequency.QUARTERLY.toString()))
				quarterlyComp.add(tempReturn);
			else if (tempReturn.getFrequency().equals(GstReturnFrequency.ANNUALLY.toString()))
				annuallyComp.add(tempReturn);
			// ends here

			map.put("MONTHLY-DETAILS", monthYears);

			map.put("finyear", financialYear);

			log.info("getGstReturnByFrequencyGstin method ends for {}", gstin);

			return map;
		} else {

			log.error("getGstReturnByFrequencyGstin method throws AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings("finally")
	public List<MonthSummaryDTO> updateMonthYearForGstin(TaxpayerGstin taxpayerGstin) throws AppException {

		List<MonthSummaryDTO> monthSummaryDTOs = new ArrayList<>();

		SimpleDateFormat sdfBread = new SimpleDateFormat("MMM: yy");

		if (!Objects.isNull(taxpayerGstin)) {
			try {
				String monthYr = taxpayerGstin.getMonthYear();
				MonthSummaryDTO monthSummDTO = null;
				if (!StringUtils.isEmpty(monthYr)) {
					monthSummDTO = JsonMapper.objectMapper.readValue(monthYr, MonthSummaryDTO.class);
					monthSummaryDTOs.add(monthSummDTO);
					return monthSummaryDTOs;
				}
			} catch (IOException e) {
				log.error("Error {} while processing getReturnSummaryByGstin to get monthSummaryDTO", e.getMessage());
			}

			List<GstinAction> actions = finderService.findGstinActionByTaxpayerGstinId(taxpayerGstin.getId());
			if (Objects.isNull(actions))
				actions = new ArrayList<>();

			List<BrtMappingDTO> brtMappingDTOs = getBrtMappingByGstin(taxpayerGstin.getGstin());
			TreeSet<GstReturnDTO> monthlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
			TreeSet<GstReturnDTO> quarterlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
			TreeSet<GstReturnDTO> annuallyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());

			for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
				GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
				if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.MONTHLY.toString()))
					monthlyComp.add(gstReturnDTO);
				else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.QUARTERLY.toString()))
					quarterlyComp.add(gstReturnDTO);
				else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.ANNUALLY.toString()))
					annuallyComp.add(gstReturnDTO);
			}

			SimpleDateFormat fullDate = new SimpleDateFormat("dd MMM yyyy");

			Date gstStartDate = SerializationUtils
					.deserialize(redisService.getBytes(ApplicationMetadata.GST_START_DATE));
			Calendar cal = Calendar.getInstance();
			Calendar prevCal = Calendar.getInstance();
			Date currentDate = cal.getTime();
			Date startDate = cal.getTime();

			int index = 0;
			List<String> monthYears = new ArrayList<>();
			while (gstStartDate.compareTo(startDate) <= 0) {

				prevCal.setTime(cal.getTime());
				prevCal.add(Calendar.MONTH, -1);
				boolean startFlag = false;
				Date initDate = prevCal.getTime();
				startFlag = gstStartDate.compareTo(initDate) > 0;

				MonthSummaryDTO monthSummaryDTO = new MonthSummaryDTO();
				monthSummaryDTO.setId(index++);

				String month = String.valueOf(startDate.getMonth() + 1);
				month = month.length() == 1 ? "0" + month : month;
				month = month + (1900 + startDate.getYear());

				String prevMonth = String.valueOf(prevCal.getTime().getMonth() + 1);
				prevMonth = prevMonth.length() == 1 ? "0" + prevMonth : prevMonth;
				prevMonth = prevMonth + (1900 + prevCal.getTime().getYear());

				String my = Utility.getMonth(startDate.getMonth());
				monthYears.add(my);
				monthSummaryDTO.setMonthName(my + " " + (1900 + startDate.getYear()));
				monthSummaryDTO.setBreadcrumb(sdfBread.format(startDate).replace(":", "'"));
				monthSummaryDTO.setCode(month);

				List<ReturnSummaryDTO> returnSummaryDTOs = new ArrayList<>();
				monthSummaryDTO.setReturnSummary(returnSummaryDTOs);

				for (GstReturnDTO gstReturnDTO : monthlyComp) {

					ReturnSummaryDTO returnSummaryDTO = new ReturnSummaryDTO();

					String status = ReturnCalendar.BLANK.toString();
					GstinAction temp = new GstinAction();
					temp.setParticular(month);
					temp.setReturnType(gstReturnDTO.getCode());
					temp.setStatus(ReturnCalendar.FILED.toString());
					Date returnDueDate = gstReturnDTO.getDueDate();
					Date returnStartDate = gstReturnDTO.getStartDate();

					Calendar newCal = Calendar.getInstance();
					newCal.set(Calendar.DATE, returnDueDate.getDate());
					newCal.set(Calendar.YEAR, 1900 + startDate.getYear());
					newCal.set(Calendar.MONTH, startDate.getMonth());
					newCal.add(Calendar.MONTH, 1);
					if (actions.contains(temp)) {
						temp = actions.get(actions.indexOf(temp));
					} else {
						temp = null;
					}

					Date newTime = newCal.getTime();

					Calendar newCal2 = Calendar.getInstance();
					newCal2.set(Calendar.DATE, returnStartDate.getDate());
					Date newStartTime = newCal2.getTime();

					GstCalendar calendar = AppConfig.getGstCalendarByCode(month, gstReturnDTO.getCode());
					if (!Objects.isNull(calendar)) {
						newTime = calendar.getDueDate();
						newStartTime = calendar.getStartDate();
					}

					double diff = currentDate.getTime() - newTime.getTime();
					double startDiff = currentDate.getTime() - newStartTime.getTime();

					if (!Objects.isNull(temp) && temp.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
						status = ReturnCalendar.FILED.toString();
						returnSummaryDTO.setDiff(0);
					} else {
						diff = diff / (24 * 60 * 60 * 1000);
						if (startDiff >= 0.0 && diff <= 0.0) {
							status = ReturnCalendar.APPROACHING.toString();
						} else if (diff > 0.0) {
							status = ReturnCalendar.LOCKED.toString();
							if (gstReturnDTO.getMandatory() == 1) {
								status = ReturnCalendar.OVERDUE.toString();
							}
							if (currentDate.getDate() >= 11 && currentDate.getDate() <= 15
									&& gstReturnDTO.getCode().equalsIgnoreCase(ReturnType.GSTR1.toString())) {
								// status = ReturnCalendar.ONHOLD.toString();
								status = ReturnCalendar.OVERDUE.toString();

							}
						}

						if (gstReturnDTO.getDependent() > 0) {
							GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getDependent());
							GstinAction temp1 = new GstinAction();
							temp1.setReturnType(gstReturn.getCode());
							temp1.setStatus(GstnAction.FILED.toString());
							if (gstReturn.getPrevMonth() == 1)
								temp1.setParticular(prevMonth);
							else
								temp1.setParticular(month);
							if (!actions.contains(temp1)) {
								status = status + "_LOCKED";
							}

							if (gstReturn.getPrevMonth() == 1 && startFlag)
								status = status.replaceAll("_LOCKED", "");
						}

						if (gstReturnDTO.getLockedAfter() > 0) {
							GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getLockedAfter());
							GstinAction temp1 = new GstinAction();
							temp1.setReturnType(gstReturn.getCode());
							temp1.setParticular(month);
							temp1.setStatus(GstnAction.FILED.toString());
							if (actions.contains(temp1) && status.indexOf("_LOCKED") == -1) {
								status = status + "_LOCKED";
							}
						}

						returnSummaryDTO.setDiff(Double.valueOf(diff).intValue());
					}

					// Introduces this because of pre process to incorporate
					// Ledger in workflow
					if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {

						ReturnSummaryDTO returnSummaryDTO1 = new ReturnSummaryDTO();

						String status1 = ReturnCalendar.BLANK.toString();
						GstinAction temp1 = new GstinAction();
						temp1.setParticular(month);
						temp1.setReturnType(gstReturnDTO.getCode());

						if (actions.contains(temp1)) {
							temp1 = actions.get(actions.indexOf(temp1));
						} else {
							temp1 = null;
						}

						if (!Objects.isNull(temp1)
								&& temp1.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
							status1 = ReturnCalendar.FILED.toString();
							returnSummaryDTO1.setDiff(Double.valueOf("0").intValue());
						} else {
							status1 = status;
							returnSummaryDTO1.setDiff(Double.valueOf(diff).intValue());
						}

						String name = "", description = "";
						if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {
							String temp11 = gstReturnDTO.getPreProcess();
							if (temp11.contains("-")) {
								String[] values = temp11.split("-");
								name = values[0];
								description = values[1];
							} else {
								name = gstReturnDTO.getPreProcess();
								description = gstReturnDTO.getPreProcess();
							}
						}

						returnSummaryDTO1.setDueDate(fullDate.format(newTime));
						returnSummaryDTO1.setReturnName(name);
						returnSummaryDTO1.setStatus(status1);
						returnSummaryDTO1.setDescription(description);
						returnSummaryDTO1.setName(name);
						returnSummaryDTOs.add(returnSummaryDTO1);
					}
					// ends here

					returnSummaryDTO.setDueDate(fullDate.format(newTime));
					returnSummaryDTO.setReturnName(gstReturnDTO.getCode());
					returnSummaryDTO.setStatus(status);
					returnSummaryDTO.setDescription(gstReturnDTO.getDescription());
					returnSummaryDTO.setName(gstReturnDTO.getName());
					returnSummaryDTOs.add(returnSummaryDTO);
				}
				monthSummaryDTOs.add(monthSummaryDTO);
				cal.add(Calendar.MONTH, -1);
				startDate = cal.getTime();
			}

			MonthSummaryDTO monthSummaryDTO = new MonthSummaryDTO();
			monthSummaryDTO = monthSummaryDTOs.get(monthSummaryDTOs.size() - 1);
			monthSummaryDTOs.clear();
			monthSummaryDTOs.add(monthSummaryDTO);

			try {
				String monthYear = JsonMapper.objectMapper.writeValueAsString(monthSummaryDTO);
				taxpayerGstin.setMonthYear(monthYear);
				taxpayerGstin
						.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
				taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
				crudService.update(taxpayerGstin);
			} catch (JsonProcessingException e) {
				log.error("Error {} while processing getReturnSummaryByGstin to get monthSummaryDTO", e.getMessage());
			} finally {
				log.info("getGstReturnSummaryByGstin method ends..");

				return monthSummaryDTOs;
			}
		} else {
			log.info("getGstReturnSummaryByGstin method throws AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public List<MonthSummaryDTO> getGstReturnSummaryByGstin(String gstin) throws AppException {

		log.info("getGstReturnSummaryByGstin method starts for {}", gstin);

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstinByGstin(gstin);

			return updateMonthYearForGstin(taxpayerGstin);

		} else {
			log.info("getGstReturnSummaryByGstin method throws AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> getGstTransactionsByGstin(String gstin)
			throws AppException {

		log.info("getGstTransactionsByGstin method starts for {}", gstin);

		if (!StringUtils.isEmpty(gstin)) {
			List<BrtMappingDTO> brtMappingDTOs = getBrtMappingByGstin(gstin);
			Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> gstTransactions = new HashMap<>();

			for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
				GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
				SortedSet<ReturnTransactionDTO> returnTransactionDTOs = null;
				if (gstTransactions.containsKey(gstReturnDTO)) {
					returnTransactionDTOs = gstTransactions.get(gstReturnDTO);
				} else {
					returnTransactionDTOs = new TreeSet<>(Comparator.comparing(ReturnTransactionDTO::getId));
					gstTransactions.put(gstReturnDTO, returnTransactionDTOs);
				}
				returnTransactionDTOs.add(brtMappingDTO.getReturnTransaction());
			}
			log.info("getGstTransactionsByGstin method ends..");

			return gstTransactions;
		} else {
			log.error("getGstTransactionsByGstin method throws the AppException..");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Set<ReturnTransactionDTO> getTransactionsByBusinessType(String types) throws AppException {

		log.info("getTransactionsByBusinessType method starts for {}", types);

		if (!StringUtils.isEmpty(types)) {

			String[] temp = {};
			temp = types.split(":");

			List<BrtMappingDTO> brtMappingDTOs = finderService.getBrtMapping(temp);
			Set<ReturnTransactionDTO> transactionSet = new HashSet<>();

			for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
				transactionSet.add(brtMappingDTO.getReturnTransaction());
			}

			log.info("getTransactionsByBusinessType method ends..");

			return transactionSet;
		} else {
			log.error("getTransactionsByBusinessType method throws the AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public SortedSet<ReturnTransactionDTO> getReturnTransactionsByGstin(String gstin, String returnCode)
			throws AppException {

		log.info("getReturnTransactionsByGstin method starts..");

		if (!StringUtils.isEmpty(gstin)) {
			Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> gstTransactions = getGstTransactionsByGstin(gstin);
			for (Map.Entry<GstReturnDTO, SortedSet<ReturnTransactionDTO>> entry : gstTransactions.entrySet()) {
				if (entry.getKey().getCode().equalsIgnoreCase(returnCode)) {
					return entry.getValue();
				}
			}

			log.info("getReturnTransactionsByGstin method ends..");

			return new TreeSet<>();
		} else {
			log.error("getReturnTransactionsByGstin method throws the AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean setFinancialYear(String start) throws AppException {

		log.info("setFinancialYear method starts for {} ", start);

		if (!StringUtils.isEmpty(start)) {
			Integer end = Integer.parseInt(start) + 1;
			redisService.setValue(userBean.getUserDto().getUsername() + ApplicationMetadata.FINANCIAL_YEAR_START, start,
					false, 0);
			redisService.setValue(userBean.getUserDto().getUsername() + ApplicationMetadata.FINANCIAL_YEAR_END,
					end + "", false, 0);
			log.info("setFinancialYear method ends..");
			return true;
		} else {
			log.error("setFinancialYear method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Map<String, Object> getFinancialYear() throws AppException {

		log.info("getFinancialYear method starts..");

		Calendar cal = Calendar.getInstance();
		Date gstStartDate = SerializationUtils.deserialize(redisService.getBytes(ApplicationMetadata.GST_START_DATE));
		Map<String, Object> values = new LinkedHashMap<>();

		Date currentDate = cal.getTime();
		while (gstStartDate.compareTo(currentDate) < 0) {
			int year = 1900 + currentDate.getYear();
			int month = currentDate.getMonth();
			String value = "";
			int start = 0;
			int end = 0;
			if (month < 3) {
				start = year - 1;
				end = year;
			} else {
				start = year;
				end = year + 1;
			}
			value = start + " - " + end;

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.YEAR, end);
			Date finEndDate = cal1.getTime();
			cal1.set(start, 3, 1);
			Date finStartDate = cal1.getTime();
			cal1.set(1900 + finEndDate.getYear(), finEndDate.getMonth(), finEndDate.getDate());

			List<String> monthYears = new ArrayList<>();
			while (finStartDate.compareTo(finEndDate) <= 0) {
				if (gstStartDate.compareTo(finEndDate) <= 0) {
					String month1 = String.valueOf(finEndDate.getMonth() + 1);
					month1 = month1.length() == 1 ? "0" + month1 : month1;
					month1 = month1 + (1900 + finEndDate.getYear());
					String my = Utility.getMonth(finEndDate.getMonth());
					monthYears.add(my);
				}
				cal1.add(Calendar.MONTH, -1);
				finEndDate = cal1.getTime();
			}

			values.put(start + "", value);
			values.put(start + "_month", monthYears);
			cal.add(Calendar.YEAR, -1);
			currentDate = cal.getTime();
		}
		log.info("getFinancialYear method ends..");

		return values;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Set<ResponseDTO> getReturnSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException {

		log.info("getReturnSummary method starts..");

		if (!Objects.isNull(gstinTransactionDTO) && !Objects.isNull(gstinTransactionDTO.getTaxpayerGstin())) {

			Set<ResponseDTO> responseDTOs = new HashSet<>();
			TaxpayerGstin taxpayerGstin = finderService
					.findTaxpayerGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

			String monthYear = gstinTransactionDTO.getMonthYear();
			int month = Integer.parseInt(monthYear.substring(0, 2)) - 1;
			int year = Integer.parseInt(monthYear.substring(2, 6));

			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
			SimpleDateFormat sdfFull = new SimpleDateFormat("dd MMMM yyyy");

			Set<GstReturnDTO> returnDtos = getGstReturnByGstin(gstinTransactionDTO.getTaxpayerGstin().getGstin());

			List<GstinAction> actions = finderService.findGstinActionByTaxpayerGstinId(taxpayerGstin.getId());

			List<GstinTransaction> gstinTransactions = finderService
					.findGstinTransactionByIdMonth(taxpayerGstin.getId(), monthYear);

			Properties fsmConfig = new Properties();
			try {
				fsmConfig.load(
						MethodHandles.lookup().lookupClass().getClassLoader().getResourceAsStream("fsm.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String flowMasterKey = "gstr_flow_application";
			String flow = fsmConfig.getProperty(flowMasterKey);
			String[] tempFlow = flow.split(",");
			List<String> flowList = Arrays.asList(tempFlow);

			int index = 0;

			for (GstinTransaction gstinTransaction : gstinTransactions) {
				if (gstinTransaction.getMonthYear().equals(gstinTransactionDTO.getMonthYear())) {
					String obj = gstinTransaction.getTransactionObject();
					String type = gstinTransaction.getTransactionType().toLowerCase();
					if (!StringUtils.isEmpty(obj) && !(type.contains("mismatch") || type.contains("summary"))) {
						log.debug("getting object length as {}", obj.length());
						if (obj.length() > 10) {
							ResponseDTO responseDTO = new ResponseDTO();
							String key = gstinTransaction.getReturnType().toUpperCase() + "_UPLOAD";
							responseDTO.setKey(key);
							responseDTO.setValue("YES");
							responseDTOs.add(responseDTO);
						}
					}
				}
			}

			for (GstinAction gstinAction : actions) {
				if (gstinAction.getParticular().equals(gstinTransactionDTO.getMonthYear())) {
					ResponseDTO responseDTO = new ResponseDTO();
					String key = gstinAction.getReturnType() + "_" + gstinAction.getStatus();
					int tempIndex = flowList.indexOf(key);
					if (tempIndex > -1)
						index = tempIndex > index ? tempIndex : index;
					responseDTO.setKey(key);
					responseDTO.setValue("YES");
					responseDTOs.add(responseDTO);
				}
			}

			for (int k = index; k > 0; k--) {
				String key = flowList.get(k - 1);
				ResponseDTO responseDTO = new ResponseDTO();
				responseDTO.setKey(key);
				responseDTO.setValue("YES");
				if (!responseDTOs.contains(responseDTO))
					responseDTOs.add(responseDTO);
			}

			for (GstReturnDTO gstReturnDTO : returnDtos) {
				ResponseDTO responseDTO = new ResponseDTO();
				ResponseDTO responseDTOFull = new ResponseDTO();

				Date returnDueDate = gstReturnDTO.getDueDate();
				Calendar newCal = Calendar.getInstance();
				newCal.set(Calendar.DATE, returnDueDate.getDate());
				newCal.set(Calendar.YEAR, year);
				newCal.set(Calendar.MONTH, month);
				newCal.add(Calendar.MONTH, 1);

				Date d = newCal.getTime();
				GstCalendar calendar = AppConfig.getGstCalendarByCode(gstinTransactionDTO.getMonthYear(),
						gstReturnDTO.getCode());
				if (!Objects.isNull(calendar)) {
					d = calendar.getDueDate();
				}

				responseDTO.setKey(gstReturnDTO.getCode());
				responseDTO.setValue(sdf.format(d));
				responseDTOFull.setKey(gstReturnDTO.getCode() + "-full");
				responseDTOFull.setValue(sdfFull.format(d));
				responseDTOs.add(responseDTO);
				responseDTOs.add(responseDTOFull);
			}

			log.info("getReturnSummary method ends..");
			return responseDTOs;

		} else {
			log.error("getReturnSummary method throws the AppException..");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	/*
	 * @Override public String saveTransactionMapping(String pan, String rtype,
	 * String transaction, String request) throws AppException {
	 * 
	 * log.info("saveTransactionMapping method starts..");
	 * 
	 * String map = ""; String returnType = rtype.toLowerCase(); String
	 * transactionType = transaction.toLowerCase(); String keyName = returnType +
	 * "-" + transactionType; String transactionValue = null; ErpConfigDTO
	 * erpConfigDTO = new ErpConfigDTO(); try { if (!StringUtils.isEmpty(request) &&
	 * !StringUtils.isEmpty(pan)) { ErpConfig erpConfig = (ErpConfig)
	 * EntityHelper.convert(erpConfigDTO, ErpConfig.class);
	 * 
	 * erpConfig.setActive((byte) 1); erpConfig.setProvider("FTP");
	 * 
	 * Taxpayer taxpayerEm = finderService.findTaxpayer(pan);
	 * 
	 * erpConfig.setTaxpayer(taxpayerEm);
	 * 
	 * excelConfig = new Properties();
	 * excelConfig.load(DBUtils.class.getClassLoader().getResourceAsStream(
	 * "excel.properties")); transactionValue = excelConfig
	 * .getProperty(String.format("template.return.%s.transaction.%s", returnType,
	 * transactionType));
	 * 
	 * map = transactionMapping(transactionValue, request); JSONObject object = new
	 * JSONObject(); object.put(keyName, map);
	 * erpConfig.setTransactionMappings(object.toString()); List<ErpConfig>
	 * erpConfigs = finderService.findErpConfigByTaxpayer(taxpayerEm.getId()); if
	 * (erpConfigs != null && !erpConfigs.isEmpty()) { ErpConfig temp =
	 * erpConfigs.get(0); erpConfig.setId(temp.getId()); erpConfig.setUpdatedBy(
	 * userBean.getUserDto().getFirstName() + "_" +
	 * userBean.getUserDto().getLastName());
	 * erpConfig.setUpdationIpAddress(userBean.getIpAddress());
	 * erpConfig.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
	 * crudService.update(erpConfig); } else { erpConfig.setCreatedBy(
	 * userBean.getUserDto().getFirstName() + "_" +
	 * userBean.getUserDto().getLastName());
	 * erpConfig.setCreationIpAddress(userBean.getIpAddress());
	 * erpConfig.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
	 * 
	 * crudService.create(erpConfig); }
	 * 
	 * } else { throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); } } catch
	 * (Exception e) { e.printStackTrace(); log.info(
	 * "saveTransactionMapping method throws the Exception for {} ",
	 * e.getMessage()); } log.info("saveTransactionMapping method end.."); return
	 * map; }
	 * 
	 * @Override public String getTransactionMapping(String pan, String rtype,
	 * String transaction) throws AppException {
	 * 
	 * log.info("getTransactionMapping method starts..");
	 * 
	 * String mappings = ""; String returnType = rtype.toLowerCase(); String
	 * transactionType = transaction.toLowerCase(); String keyName = returnType +
	 * "-" + transactionType;
	 * 
	 * String transactionValue = null; try { ErpConfig config =
	 * finderService.findErpConfig(pan); mappings = config.getTransactionMappings();
	 * String headersIndex = ""; if (!StringUtils.isEmpty(mappings)) { JSONObject
	 * object = new JSONObject(mappings); if (object.has(keyName)) { headersIndex =
	 * object.getString(keyName); } }
	 * 
	 * excelConfig = new Properties();
	 * excelConfig.load(DBUtils.class.getClassLoader().getResourceAsStream(
	 * "excel.properties")); transactionValue = excelConfig
	 * .getProperty(String.format("template.return.%s.transaction.%s", returnType,
	 * transactionType));
	 * 
	 * if (StringUtils.isEmpty(headersIndex)) { return transactionValue; } else {
	 * transactionValue = getUserDefinedMapping(transactionValue, headersIndex); }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); log.error(
	 * "getTransactionMapping method throws the Exception for {}", e.getMessage());
	 * 
	 * }
	 * 
	 * log.info("getTransactionMapping method ends..");
	 * 
	 * return transactionValue; }
	 * 
	 * private String transactionMapping(String s1, String s2) throws AppException {
	 * 
	 * log.info("transactionMapping method starts..");
	 * 
	 * String mapping = ""; List<String> arrays1 = Arrays.asList(s1.split("\\,"));
	 * List<String> arrays2 = Arrays.asList(s2.split("\\,")); int j; if
	 * (!(arrays1.isEmpty() && arrays2.isEmpty())) { for (String val : arrays2) { if
	 * (arrays1.indexOf(val) >= 0) { j = arrays1.indexOf(val); mapping += j + ","; }
	 * } mapping = mapping.substring(0, mapping.length() - 1); } else {
	 * log.error("transactionMapping method throws Exception");
	 * 
	 * throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
	 * 
	 * log.info("transactionMapping method ends..");
	 * 
	 * return mapping; }
	 * 
	 * private String getUserDefinedMapping(String s1, String mapping) throws
	 * AppException {
	 * 
	 * log.info("getUserDefinedMapping method starts..");
	 * 
	 * String s2 = ""; List<String> arrays1 = Arrays.asList(s1.split("\\,"));
	 * List<String> arrays3 = Arrays.asList(mapping.split("\\,")); if
	 * (!(arrays1.isEmpty() && arrays3.isEmpty())) { String generatingS2 = ""; for
	 * (String val : arrays3) { if (Integer.parseInt(val) > 0) { generatingS2 =
	 * arrays1.get(Integer.parseInt(val)); s2 += generatingS2 + ","; } } s2 =
	 * s2.substring(0, s2.length() - 1); } else { log.error(
	 * "getUserDefinedMapping method throws AppException.."); throw new
	 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
	 * log.info("getUserDefinedMapping method ends.."); return s2; }
	 */

	@Override
	public Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> getGstAllTransactions() throws AppException {

		log.info("getGstAllTransactions method starts..");

		List<BrtMappingDTO> brtMappingDTOs = finderService.getAllBrtMapping();
		Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> gstTransactions = new HashMap<>();

		for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
			GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
			if (gstReturnDTO.getCode().equals(ReturnType.GSTR1.toString())
					|| gstReturnDTO.getCode().equals(ReturnType.GSTR2.toString())) {
				SortedSet<ReturnTransactionDTO> returnTransactionDTOs = null;
				if (gstTransactions.containsKey(gstReturnDTO)) {
					returnTransactionDTOs = gstTransactions.get(gstReturnDTO);
				} else {
					returnTransactionDTOs = new TreeSet<>(Comparator.comparing(ReturnTransactionDTO::getId));
					gstTransactions.put(gstReturnDTO, returnTransactionDTOs);
				}
				returnTransactionDTOs.add(brtMappingDTO.getReturnTransaction());
			}
		}
		log.info("getGstAllTransactions method ends..");

		return gstTransactions;

	}

	@Override
	public int readExcelVendorData(String pan, File file, String mapping, String type) throws AppException {

		log.info("readExcelVendorData method starts..");

		if (!(StringUtils.isEmpty(pan) && StringUtils.isEmpty(mapping) && Objects.isNull(file))) {

			try {

				// Connection conn = DBUtils.getConnection();
				// PreparedStatement statement =
				// conn.prepareStatement(SQLQuery.SQL_VENDOR_ADD);

				int valuesCount = 0;

				final Pattern pattern1 = Pattern
						.compile("[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}");

				JSONObject mappings = null;
				if (!StringUtils.isEmpty(mapping))
					mappings = new JSONObject(mapping);

				GstExcel gstExcel = new GstExcel(file);

				Taxpayer taxpayer = finderService.findTaxpayer(pan);

				// add dummy transaction
				String code = "vendor";
				if (type.equalsIgnoreCase("buyer"))
					code = "customer";
				String key = code.toLowerCase();

				code = !Objects.isNull(mappings)
						? mappings.has(key + "_sheet") ? mappings.getString(key + "_sheet") : code
						: code;

				if (!Objects.isNull(mappings) && mappings.has(key + "_sheet"))
					gstExcel.loadSheet(code);

				if (!Objects.isNull(gstExcel.getSheet()) && mappings.has(key + "_sheet")) {
					int columnCount = 0;
					Iterator<Row> iterator = gstExcel.getSheet().rowIterator();

					String columns = null;
					columns = !Objects.isNull(mappings)
							? mappings.has(key + "_column") ? mappings.getString(key + "_column") : null
							: null;

					int _BUSINESS_NAME_INDEX = 0;
					int _GSTIN_INDEX = 1;
					int _CONTACT_PERSON_INDEX = 2;
					int _EMAIL_ADDRESS_INDEX = 3;
					int _MOBILE_NUMBER_INDEX = 4;
					int _WEBSITE_INDEX = 5;
					int _ORGANISATION_INDEX = 6;
					int _PAN_NUMBER_INDEX = 7;
					int _ADDRESS_INDEX = 8;
					int _PINCODE_INDEX = 9;
					int _STORE_TYPE_INDEX = 10;
					int _GST_TYPE_INDEX = 11;

					if (!StringUtils.isEmpty(columns)) {
						String[] headers = columns.split(",");
						_BUSINESS_NAME_INDEX = Integer.valueOf(headers[0]);
						_GSTIN_INDEX = Integer.valueOf(headers[1]);
						_CONTACT_PERSON_INDEX = Integer.valueOf(headers[2]);
						_EMAIL_ADDRESS_INDEX = Integer.valueOf(headers[3]);
						_MOBILE_NUMBER_INDEX = Integer.valueOf(headers[4]);
						_WEBSITE_INDEX = Integer.valueOf(headers[5]);
						_ORGANISATION_INDEX = Integer.valueOf(headers[6]);
						_PAN_NUMBER_INDEX = Integer.valueOf(headers[7]);
						_ADDRESS_INDEX = Integer.valueOf(headers[8]);
						_PINCODE_INDEX = Integer.valueOf(headers[9]);
						_STORE_TYPE_INDEX = Integer.valueOf(headers[10]);
						_GST_TYPE_INDEX = Integer.valueOf(headers[11]);
					}

					boolean isErrorFlag = false;
					List<Vendor> vendors = new ArrayList<>();
					StringBuilder sb = new StringBuilder();

					while (iterator.hasNext()) {
						Row row = iterator.next();
						int rowNum = row.getRowNum();
						if (rowNum == 0) {
							rowNum++;
							String cellRange = "A" + rowNum + ":"
									+ gstExcel.getCharForNumber(row.getPhysicalNumberOfCells()) + rowNum;
							List<String> values = gstExcel.excelReadRow(cellRange);
							if (!Objects.isNull(values))
								values.removeIf(item -> item == null || "".equals(item));
							columnCount = values.size();
						} else if (rowNum > 0) {
							rowNum++;
							String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
							List<String> values = gstExcel.excelReadRow(cellRange);

							if (!Objects.isNull(values)) {
								boolean isError = false;

								Vendor vendor = new Vendor();
								vendor.setTaxpayer(taxpayer);
								vendor.setBusinessName(values.get(_BUSINESS_NAME_INDEX));
								vendor.setType(type);
								vendor.setGstin(values.get(_GSTIN_INDEX));
								vendor.setContactPerson(values.get(_CONTACT_PERSON_INDEX));
								vendor.setEmailAddress(values.get(_EMAIL_ADDRESS_INDEX));
								vendor.setMobileNumber(values.get(_MOBILE_NUMBER_INDEX));
								vendor.setWebsite(values.get(_WEBSITE_INDEX));
								vendor.setOrganisation(values.get(_ORGANISATION_INDEX));
								vendor.setPanNumber(values.get(_PAN_NUMBER_INDEX));
								vendor.setAddress(values.get(_ADDRESS_INDEX));
								vendor.setPincode(values.get(_PINCODE_INDEX));
								vendor.setClassification(values.get(_STORE_TYPE_INDEX));
								vendor.setGstType(values.get(_GST_TYPE_INDEX));
								vendor.setActive((byte) 1);
								vendor.setEmail((byte) 0);

								String uniqueValue = "NAME - " + values.get(_BUSINESS_NAME_INDEX);

								boolean isGstin = false;
								if (!StringUtils.isEmpty(vendor.getGstin())) {
									if (!pattern1.matcher(vendor.getGstin()).matches()) {
										isError = true;
										sb.append("data," + (_GSTIN_INDEX + 1) + "," + rowNum + "," + uniqueValue
												+ " - Invalid GSTIN Number, it should be exactly 15 characters|");
									} else {
										try {
											int stateCode = 0;
											State state = null;
											try {
												stateCode = Integer.valueOf(vendor.getGstin().substring(0, 2));
												state = em.getReference(State.class, stateCode);
											} catch (Exception e) {
												throw new NumberFormatException();
											}

											State st = new State();
											st.setId(state.getId());
											vendor.setStateBean(st);
											isGstin = true;
										} catch (NumberFormatException e) {
											isError = true;
											sb.append("data," + (_GSTIN_INDEX + 1) + "," + rowNum + "," + uniqueValue
													+ " - Invalid GSTIN Number, incorrect format found|");
										}
									}
								}

								if (!StringUtils.isEmpty(vendor.getPanNumber())) {
									String panno = "";
									if (isGstin)
										panno = vendor.getGstin().substring(2, 12);
									if (vendor.getPanNumber().length() != 10) {
										isError = true;
										sb.append("data," + (_PAN_NUMBER_INDEX + 1) + "," + rowNum + "," + uniqueValue
												+ " - Invalid PAN Number, it should be exactly 10 characters|");
									} else if (isGstin && !vendor.getPanNumber().equalsIgnoreCase(panno)) {
										isError = true;
										sb.append("data," + (_PAN_NUMBER_INDEX + 1) + "," + rowNum + "," + uniqueValue
												+ " - Invalid PAN Number, not matching with the given GSTIN|");
									}
								}

								if (!StringUtils.isEmpty(vendor.getPincode())) {
									if (vendor.getPincode().length() != 6) {
										isError = true;
										sb.append("data," + (_PINCODE_INDEX + 1) + "," + rowNum + "," + uniqueValue
												+ " - Invalid Pin Code, it should be exactly 6 digits|");
									}
								}

								if (!StringUtils.isEmpty(vendor.getEmailAddress())) {
									if (!Utility.isValidEmailAddress(vendor.getEmailAddress())) {
										isError = true;
										sb.append("data," + (_EMAIL_ADDRESS_INDEX + 1) + "," + rowNum + ","
												+ uniqueValue
												+ " - Invalid Email Address found, please enter valida email address|");
									}
								}

								boolean dflag = validateVendor(vendor);
								if (!dflag) {
									isError = true;
									sb.append("data,-," + rowNum
											+ ",Either of Business Name / Contact Person / Email Address is missing|");
								}

								if (!isError) {
									vendor.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
											+ userBean.getUserDto().getLastName());
									vendor.setCreationIpAddress(userBean.getIpAddress());
									vendor.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
									if (!Objects.isNull(finderService.getExistingUser(vendor.getEmailAddress()))) {
										vendor.setEmgst((byte) 1);
										vendor.setEmail((byte) 1);
									}
									vendors.add(vendor);
									valuesCount++;
								} else {
									isErrorFlag = isErrorFlag || isError;
								}
							}
						}

					}

					if (isErrorFlag) {
						AppException exp = new AppException();
						exp.setCode(ExceptionCode._ERROR);
						exp.setMessage(sb.toString());
						throw exp;
					} else {

						for (Vendor vendor2 : vendors) {
							if (!StringUtils.isEmpty(vendor2.getEmailAddress())) {
								if (!Objects.isNull(finderService.getExistingUser(vendor2.getEmailAddress()))) {
									vendor2.setEmgst((byte) 1);
									vendor2.setEmail((byte) 1);
									String gstin = vendor2.getGstin();
									if (!StringUtils.isEmpty(gstin)) {
										Vendor vendorGstin = finderService.findVendorByTpGstin(taxpayer.getId(), gstin);
										if (!Objects.isNull(vendorGstin)) {
											vendor2.setId(vendorGstin.getId());
											crudService.update(vendor2);
										} else {
											crudService.create(vendor2);
										}
									} else {
										crudService.create(vendor2);
									}
								} else {
									String gstin = vendor2.getGstin();
									if (!StringUtils.isEmpty(gstin)) {
										Vendor vendorGstin = finderService.findVendorByTpGstin(taxpayer.getId(), gstin);
										if (!Objects.isNull(vendorGstin)) {
											vendor2.setId(vendorGstin.getId());
											crudService.update(vendor2);
										} else {
											crudService.create(vendor2);
										}
									} else {
										crudService.create(vendor2);
									}
								}
							} else {
								crudService.create(vendor2);
							}
						}
					}
				}
				log.info("readExcelVendorData method ends..");

				return valuesCount;
			} catch (AppException e) {
				log.error("readExcelVendorData method Exception for {} ", e.getMessage());
				throw e;
			} /*
				 * catch (SQLException e) { log.error(
				 * "readExcelVendorData method SQLException for {} ", e.getMessage()); throw new
				 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST ); }
				 */finally {

			}

		} else {
			log.error("readExcelVendorData method throws AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public List<HsnSac> getHsnSacData(String pan) throws AppException {

		log.info("getHsnSacData method starts..");

		Taxpayer taxpayer = finderService.findTaxpayer(pan);

		if (!Objects.isNull(taxpayer)) {

			List<HsnSac> hsnSacs = finderService.getHsnSacByTaxpayerId(taxpayer.getId());

			log.info("getHsnSacData method ends..");
			return hsnSacs;

		} else {
			log.error("getHsnSacData method throws AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public int addHsnSacData(String pan, HsnSacDTO hsnSacDTO) throws AppException {

		log.info("addHsnSacData method starts..");

		if (!(Objects.isNull(hsnSacDTO) && StringUtils.isEmpty(pan))) {

			log.info("Add new HsnData for pan ... {}", pan);

			Taxpayer taxpayer = finderService.findTaxpayer(pan);
			HsnSac hsnSac = finderService.findHsnSac(taxpayer.getId(), hsnSacDTO.getCode());
			if (!Objects.isNull(hsnSac))
				throw new AppException(ExceptionCode._DUPLICATE);

			if (hsnSacDTO.getType() != null) {
				if (hsnSacDTO.getType().equalsIgnoreCase("product")) {
					HsnProduct hsnProduct = finderService.findHsnProduct(hsnSacDTO.getCode());
					if (!Objects.isNull(hsnProduct)) {
						hsnSacDTO.setSubChapter(hsnProduct.getHsnSubChapter().getCode());
						hsnSacDTO.setSubChapterDesc(hsnProduct.getHsnSubChapter().getDescription());
						hsnSacDTO.setChapter(hsnProduct.getHsnSubChapter().getHsnChapter().getCode());
						hsnSacDTO.setChapterDesc(hsnProduct.getHsnSubChapter().getHsnChapter().getDescription());
					}
				} else if (hsnSacDTO.getType().equalsIgnoreCase("subchapter")) {
					HsnSubChapter hsnSubChapter = finderService.findHsnSubChapter(hsnSacDTO.getCode());
					if (!Objects.isNull(hsnSubChapter)) {
						hsnSacDTO.setChapter(hsnSubChapter.getHsnChapter().getCode());
						hsnSacDTO.setChapterDesc(hsnSubChapter.getHsnChapter().getDescription());
					}
				}
			}

			hsnSac = (HsnSac) EntityHelper.convert(hsnSacDTO, HsnSac.class);
			hsnSac.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			hsnSac.setCreationIpAddress(userBean.getIpAddress());
			hsnSac.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			hsnSac.setTaxpayer(taxpayer);

			hsnSac = crudService.create(hsnSac);

			log.info("addHsnSacData method ends..");

			return hsnSac.getId();

		} else {
			log.error("addHsnSacData method throws AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public HsnSacDTO deleteHsnSacData(int id, String pan) throws AppException {

		log.info("deleteHsnSacData method starts..");

		if (id == 0) {

			Taxpayer taxpayer = finderService.findTaxpayer(pan);
			List<HsnSac> hsnSacs = finderService.getHsnSacByTaxpayerId(taxpayer.getId());
			hsnSacs.stream().forEach(v -> v.setTaxpayer(null));
			crudService.update(taxpayer);

			HsnSacDTO hsnSacDTO = new HsnSacDTO();
			hsnSacDTO.setId(-1);

			return hsnSacDTO;

		} else if (id > 0) {

			HsnSacDTO hsnSacDTO = null;
			HsnSac hsnSac = em.getReference(HsnSac.class, id);
			if (!Objects.isNull(hsnSac)) {
				crudService.delete(HsnSac.class, id);

				hsnSacDTO = new HsnSacDTO();
				hsnSacDTO.setId(id);

				log.info("deleteHsnSacData method ends..");

				return hsnSacDTO;
			} else {
				log.error("deleteHsnSacData method throws the AppExeption");
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			log.error("deleteHsnSacData method throws the AppExeption");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public List<Vendor> getVendorData(String pan, String type) throws AppException {

		log.info("getVendorData method starts..");

		Taxpayer taxpayer = finderService.findTaxpayer(pan);

		if (!Objects.isNull(taxpayer)) {

			List<Vendor> vendorList = finderService.getVendorByTaxpayerId(taxpayer.getId(), type);

			log.info("getVendorData method ends..");
			return vendorList;

		} else {
			log.info("getVendorData method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private boolean validateVendor(Vendor vendor) {
		log.info("validateVendor method starts..");

		if (!StringUtils.isEmpty(vendor.getBusinessName()) && !StringUtils.isEmpty(vendor.getType())
				&& !StringUtils.isEmpty(vendor.getContactPerson()) && !StringUtils.isEmpty(vendor.getEmailAddress())) {
			return true;
		}
		log.info("validateVendor method ends..");

		return false;
	}

	private boolean validateVendor(VendorDTO vendorDTO) {

		log.info("validateVendor method starts..");

		if (!StringUtils.isEmpty(vendorDTO.getBusinessName()) && !StringUtils.isEmpty(vendorDTO.getType())
				&& !StringUtils.isEmpty(vendorDTO.getContactPerson())
				&& !StringUtils.isEmpty(vendorDTO.getEmailAddress())) {
			return true;
		}
		log.info("validateVendor method ends..");
		return false;
	}

	@Override
	public VendorDTO createVendor(VendorDTO vendorDTO, String pan) throws AppException {

		log.info("createVendor method starts..");

		if (!(Objects.isNull(vendorDTO) && validateVendor(vendorDTO))) {

			log.info("Add new vendor for pan ... {}", pan);

			Taxpayer taxpayer = finderService.findTaxpayer(pan);

			Vendor vendor = (Vendor) EntityHelper.convert(vendorDTO, Vendor.class);
			vendor.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			vendor.setCreationIpAddress(userBean.getIpAddress());
			vendor.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			vendor.setActive((byte) 1);
			vendor.setEmail((byte) 0);
			vendor.setStatus(Constant.TaxpayerStatus.ACTIVE.toString());
			vendor.setTaxpayer(taxpayer);

			if (!StringUtils.isEmpty(vendorDTO.getGstin())) {
				int stateCode = Integer.valueOf(vendorDTO.getGstin().substring(0, 2));
				State state = em.getReference(State.class, stateCode);

				if (Objects.isNull(state))
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

				State st = new State();
				st.setId(state.getId());
				vendor.setStateBean(st);

				String gstin = vendorDTO.getGstin();
				Vendor vendorGstin = finderService.findVendorByTpGstin(taxpayer.getId(), gstin);
				if (!Objects.isNull(vendorGstin)) {
					throw new AppException(ExceptionCode._DUPLICATE_GSTIN);
				}
			}

			// send email to not found user
			if (!StringUtils.isEmpty(vendor.getEmailAddress())) {

				String requestId = taxpayer.getRequestId();
				if (StringUtils.isEmpty(requestId)) {
					requestId = UUID.randomUUID().toString();
					taxpayer.setRequestId(requestId);
					taxpayer.setUpdatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					taxpayer.setUpdationIpAddress(userBean.getIpAddress());
					taxpayer.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(taxpayer);
				}

				String vendorRequestId = vendor.getRequestId();
				if (StringUtils.isEmpty(vendorRequestId)) {
					vendorRequestId = UUID.randomUUID().toString();
					vendor.setRequestId(vendorRequestId);
				}

				if (Objects.isNull(finderService.getExistingUser(vendor.getEmailAddress()))) {
					Properties config = new Properties();
					try {
						config.load(DBUtils.class.getClassLoader().getResourceAsStream("config.properties"));
						log.info("config.properties loaded successfully");
					} catch (IOException e) {
						e.printStackTrace();
					}

					StringBuilder sb = new StringBuilder(config.getProperty("main.web.url.signup"));
					sb.append("?requestId=");

					EmailFactory factory = new EmailFactory();
					Map<String, String> activateParams = new HashMap<>();
					activateParams.put("contactPerson", vendor.getContactPerson());
					activateParams.put("toEmail", vendor.getEmailAddress());
					activateParams.put("taxpayerName", taxpayer.getLegalName());
					activateParams.put("signupLink", sb.append(vendorRequestId).toString());
					factory.sendTaxpayerVendorEmail(activateParams);
					log.info("Sending taxpayer email to vendor {}.", activateParams.toString());
					vendor.setRequestId(vendorRequestId);
					vendor.setEmail((byte) 1);
					vendor.setEmailAt(Timestamp.from(Instant.now()));
					vendor = crudService.create(vendor);
				} else {
					log.info("not Sending email to vendor {} as already registered with us.", vendor.getEmailAddress());
					vendor.setEmgst((byte) 1);
					vendor = crudService.create(vendor);
				}
			}

			vendorDTO = (VendorDTO) EntityHelper.convert(vendor, VendorDTO.class);

			boolean isMapping = false;
			Taxpayer vendorTp = null;
			if (!StringUtils.isEmpty(vendorDTO.getPanNumber())) {
				vendorTp = finderService.findTaxpayer(vendorDTO.getPanNumber());
				if (!Objects.isNull(vendorTp)) {
					isMapping = true;
				}
			} else if (!StringUtils.isEmpty(vendorDTO.getGstin())) {
				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(vendorDTO.getGstin());
				if (!Objects.isNull(vendorTp)) {
					isMapping = true;
					vendorTp = taxpayerGstin.getTaxpayer();
				}
			}

			if (isMapping) {
				if (!finderService.checkTpMapping(taxpayer.getId(), vendorTp.getId())) {
					TpMapping tpMapping = new TpMapping();
					tpMapping.setPtaxpayer(taxpayer);
					tpMapping.setStaxpayer(vendorTp);
					tpMapping.setCreatedBy(
							userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
					tpMapping.setCreationIpAddress(userBean.getIpAddress());
					tpMapping.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.create(tpMapping);
				}
			}

			log.info("createVendor method ends..");

			return vendorDTO;

		} else {
			log.error("createVendor method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public VendorDTO createVendorFromGstin(VendorDTO vendorDTO, String gstin) throws AppException {

		log.info("createVendorFromGstin method starts..");

		if (!(Objects.isNull(vendorDTO) && validateVendor(vendorDTO))) {

			log.info("Add new vendor for gstin ... {}", gstin);

			TaxpayerGstin taxpayerGstin1 = finderService.findTaxpayerGstin(gstin);
			Taxpayer taxpayer = taxpayerGstin1.getTaxpayer();

			Vendor vendor = (Vendor) EntityHelper.convert(vendorDTO, Vendor.class);
			vendor.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			vendor.setCreationIpAddress(userBean.getIpAddress());
			vendor.setActive((byte) 1);
			vendor.setEmail((byte) 0);
			vendor.setStatus(Constant.TaxpayerStatus.ACTIVE.toString());
			vendor.setTaxpayer(taxpayer);

			if (!StringUtils.isEmpty(vendorDTO.getGstin())) {
				int stateCode = Integer.valueOf(vendorDTO.getGstin().substring(0, 2));
				State state = em.getReference(State.class, stateCode);

				if (Objects.isNull(state))
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

				State st = new State();
				st.setId(state.getId());
				vendor.setStateBean(st);

				String gstin1 = vendorDTO.getGstin();
				Vendor vendorGstin = finderService.findVendorByTpGstin(taxpayer.getId(), gstin1);
				if (!Objects.isNull(vendorGstin)) {
					throw new AppException(ExceptionCode._DUPLICATE_GSTIN);
				}
			}

			// send email to not found user
			if (!StringUtils.isEmpty(vendor.getEmailAddress())) {

				String requestId = taxpayer.getRequestId();
				if (StringUtils.isEmpty(requestId)) {
					requestId = UUID.randomUUID().toString();
					taxpayer.setRequestId(requestId);
					crudService.update(taxpayer);
				}

				String vendorRequestId = vendor.getRequestId();
				if (StringUtils.isEmpty(vendorRequestId)) {
					vendorRequestId = UUID.randomUUID().toString();
					vendor.setRequestId(vendorRequestId);
				}

				if (Objects.isNull(finderService.getExistingUser(vendor.getEmailAddress()))) {
					Properties config = new Properties();
					try {
						config.load(DBUtils.class.getClassLoader().getResourceAsStream("config.properties"));
						log.info("config.properties loaded successfully");
					} catch (IOException e) {
						e.printStackTrace();
					}

					StringBuilder sb = new StringBuilder(config.getProperty("main.web.url.signup"));
					sb.append("?requestId=");

					EmailFactory factory = new EmailFactory();
					Map<String, String> activateParams = new HashMap<>();
					activateParams.put("contactPerson", vendor.getContactPerson());
					activateParams.put("toEmail", vendor.getEmailAddress());
					activateParams.put("taxpayerName", taxpayer.getLegalName());
					activateParams.put("signupLink", sb.append(vendorRequestId).toString());
					factory.sendTaxpayerVendorEmail(activateParams);
					log.info("Sending taxpayer email to vendor {}.", activateParams.toString());
					vendor.setRequestId(vendorRequestId);
					vendor.setEmail((byte) 1);
					vendor.setEmailAt(Timestamp.from(Instant.now()));
					vendor = crudService.create(vendor);
				} else {
					log.info("not Sending email to vendor {} as already registered with us.", vendor.getEmailAddress());
					vendor.setEmgst((byte) 1);
					vendor = crudService.create(vendor);
				}
			}

			vendorDTO = (VendorDTO) EntityHelper.convert(vendor, VendorDTO.class);

			boolean isMapping = false;
			Taxpayer vendorTp = null;
			if (!StringUtils.isEmpty(vendorDTO.getPanNumber())) {
				vendorTp = finderService.findTaxpayer(vendorDTO.getPanNumber());
				if (!Objects.isNull(vendorTp)) {
					isMapping = true;
				}
			} else if (!StringUtils.isEmpty(vendorDTO.getGstin())) {
				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(vendorDTO.getGstin());
				if (!Objects.isNull(vendorTp)) {
					isMapping = true;
					vendorTp = taxpayerGstin.getTaxpayer();
				}
			}

			if (isMapping) {
				if (!finderService.checkTpMapping(taxpayer.getId(), vendorTp.getId())) {
					TpMapping tpMapping = new TpMapping();
					tpMapping.setPtaxpayer(taxpayer);
					tpMapping.setStaxpayer(vendorTp);
					crudService.create(tpMapping);
				}
			}

			log.info("createVendorFromGstin method ends..");

			return vendorDTO;

		} else {
			log.error("createVendorFromGstin method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public VendorDTO updateVendor(VendorDTO vendorDTO) throws AppException {

		log.info("updateVendor method starts..");

		if (!(Objects.isNull(vendorDTO) && Objects.isNull(vendorDTO.getId()))) {

			Vendor vendorEm = em.getReference(Vendor.class, vendorDTO.getId());

			Vendor vendor = (Vendor) EntityHelper.convert(vendorDTO, Vendor.class);
			vendor.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			vendor.setUpdationIpAddress(userBean.getIpAddress());

			vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			vendor.setActive((byte) 1);
			vendor.setStatus(Constant.TaxpayerStatus.ACTIVE.toString());

			vendorEm = EntityHelper.convert(vendorEm, vendor);

			if (!StringUtils.isEmpty(vendorDTO.getClassification())) {
				vendorEm.setClassification(vendorDTO.getClassification());
			}

			vendor = crudService.update(vendorEm);
			vendorDTO = (VendorDTO) EntityHelper.convert(vendor, VendorDTO.class);

			log.info("updateVendor method ends..");

			return vendorDTO;

		} else {
			log.error("updateVendor method throws the AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public VendorDTO deleteVendor(int id, String pan, String type) throws AppException {

		log.info("deleteVendor method starts..");

		if (id == 0) {

			Taxpayer taxpayer = finderService.findTaxpayer(pan);
			List<Vendor> vendors = finderService.getVendorByTaxpayerId(taxpayer.getId(), type);
			vendors.stream().forEach(v -> v.setActive((byte) 0));
			taxpayer.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			taxpayer.setUpdationIpAddress(userBean.getIpAddress());
			taxpayer.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
			crudService.update(taxpayer);

			VendorDTO vendorDTO = new VendorDTO();
			vendorDTO.setId(-1);

			return vendorDTO;

		} else if (id > 0) {

			VendorDTO vendorDTO = null;
			Vendor vendor = em.getReference(Vendor.class, id);
			if (!Objects.isNull(vendor)) {
				vendor.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				vendor.setUpdationIpAddress(userBean.getIpAddress());

				vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
				vendor.setActive((byte) 0);
				vendor.setStatus(Constant.TaxpayerStatus.INACTIVE.toString());

				vendor = crudService.update(vendor);
				vendorDTO = (VendorDTO) EntityHelper.convert(vendor, VendorDTO.class);
				log.info("deleteVendor method ends..");

				return vendorDTO;
			} else {
				log.error("deleteVendor method throws the AppException");

				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

		} else {
			log.info("deleteVendor method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public ItemCodeDTO createItemCode(ItemCodeDTO itemCodeDTO, String gstin) throws AppException {

		log.info("createItemCode method starts..");

		if (!(Objects.isNull(itemCodeDTO))) {

			log.info("Add new vendor for gstin ... {}", gstin);

			TaxpayerGstin taxpayerGstin1 = finderService.findTaxpayerGstin(gstin);
			Taxpayer taxpayer = taxpayerGstin1.getTaxpayer();

			ItemCode itemCode = (ItemCode) EntityHelper.convert(itemCodeDTO, ItemCode.class);
			// itemCode.setCreatedBy(userBean.getUserDto().getFirstName() + "_"
			// + userBean.getUserDto().getLastName());
			// itemCode.setCreationIpAddress(userBean.getIpAddress());
			itemCode.setActive((byte) 1);
			taxpayer.setCreationTime(Timestamp.from(Instant.now()));
			itemCode.setTaxpayer(taxpayer);

			ItemCode ic = finderService.findItemCodeByTaxpayerId(taxpayer.getId(), itemCode.getDescription());
			if (!Objects.isNull(ic)) {
				throw new AppException(ExceptionCode._DUPLICATE_ITEM);
			}

			itemCode = crudService.create(itemCode);
			itemCodeDTO = (ItemCodeDTO) EntityHelper.convert(itemCode, ItemCodeDTO.class);

			log.info("createItemCode method ends..");

			return itemCodeDTO;

		} else {
			log.error("createItemCode method throws the AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Map<String, Object> getInvoiceCreationData(String gstin) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				Map<String, Object> object = new HashMap<>();

				Integer taxpayerId = taxpayerGstin.getTaxpayer().getId();

				List<HsnSac> hsnSacs = finderService.getHsnSacByTaxpayerId(taxpayerId);
				List<Vendor> vendors = finderService.getVendorByTaxpayerId(taxpayerId);
				List<ItemCode> itemCodes = finderService.getItemCodeByTaxpayerId(taxpayerId);

				object.put("hsn", Objects.isNull(hsnSacs) ? new ArrayList<>() : hsnSacs);
				object.put("vendor", Objects.isNull(vendors) ? new ArrayList<>() : vendors);
				object.put("item", Objects.isNull(itemCodes) ? new ArrayList<>() : itemCodes);

				object.put("hsn_count", Objects.isNull(hsnSacs) ? 0 : hsnSacs.size());
				object.put("vendor_count", Objects.isNull(vendors) ? 0 : vendors.size());
				object.put("item_count", Objects.isNull(itemCodes) ? 0 : itemCodes.size());

				String nextInvoiceNum = getNextInvoiceNumber(taxpayerGstin);

				String footer = taxpayerGstin.getTaxpayer().getLegalName()
						+ (StringUtils.isEmpty(taxpayerGstin.getAddress()) ? "" : taxpayerGstin.getAddress());
				object.put("legalName", taxpayerGstin.getTaxpayer().getLegalName());
				object.put("footer", footer);
				object.put("subFooter", StringUtils.isEmpty(taxpayerGstin.getEmailAddress()) ? ""
						: "email: " + taxpayerGstin.getEmailAddress());

				object.put("bankAccount", taxpayerGstin.getBankAccountDetails());
				object.put("termsConditions", taxpayerGstin.getTermsConditions());

				object.put("inum", nextInvoiceNum);

				return object;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private String getNextInvoiceNumber(TaxpayerGstin taxpayerGstin) throws AppException {

		BigInteger rowCnt = (BigInteger) em
				.createNativeQuery(
						"SELECT count(1) FROM invoice_vendor_details WHERE taxpayerGstinId = " + taxpayerGstin.getId())
				.getSingleResult();

		StringBuilder sb = new StringBuilder();
		String legal = taxpayerGstin.getTaxpayer().getLegalName();
		if (!StringUtils.isEmpty(legal))
			legal = legal.length() > 1 ? legal.substring(0, 2) : legal.substring(0, 1);
		sb.append(legal);

		sb.append(taxpayerGstin.getGstin().substring(0, 2));
		sb.append("/");

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yy");
		String yy = sdf.format(cal.getTime());

		int month = cal.get(Calendar.MONTH);
		int yy2 = Integer.parseInt(yy);
		if (month < 3) {
			yy2 = yy2 - 1;
			sb.append(yy2);
			sb.append("-");
			sb.append(yy);
		} else {
			yy2 = yy2 + 1;
			sb.append(yy);
			sb.append("-");
			sb.append(yy2);
		}
		sb.append("/");

		String ino = String.format("%05d", (rowCnt.intValue() + 1));

		String tempNo = sb.toString() + ino;

		InvoiceVendorDetails details = finderService.findInvoiceByNumber(tempNo, taxpayerGstin.getId());

		if (!Objects.isNull(details)) {
			int loop = 1;
			while (!Objects.isNull(details)) {
				ino = String.format("%05d", (rowCnt.intValue() + loop + 1));
				tempNo = sb.toString() + ino;
				details = finderService.findInvoiceByNumber(tempNo, taxpayerGstin.getId());
				loop++;
			}
		}

		sb.append(ino);

		return sb.toString();
	}

	@Override
	public Map<String, Object> getInvoiceData(String gstin) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
			Map<String, Object> object = new HashMap<>();

			if (!Objects.isNull(taxpayerGstin)) {

				List<InvoiceVendorDetails> vendorDetails = finderService.getInvoiceVendorDetails(taxpayerGstin.getId());
				if (Objects.isNull(vendorDetails) || vendorDetails.isEmpty()) {
					object.put("data", new ArrayList<>());
					object.put("count", 0);
					object.put("value", "0.00");
					return object;
				}

				object.put("data", vendorDetails);

				BigInteger rowCnt = (BigInteger) em.createNativeQuery(
						"SELECT count(1) FROM invoice_vendor_details WHERE active=1 and taxpayerGstinId = "
								+ taxpayerGstin.getId())
						.getSingleResult();

				object.put("count", rowCnt.intValue());

				Double invValue = (Double) em.createNativeQuery(
						"SELECT SUM(invoiceValue) from invoice_details WHERE id IN (SELECT invoiceId FROM invoice_vendor_details WHERE active=1 and taxpayerGstinId = "
								+ taxpayerGstin.getId() + ")")
						.getSingleResult();

				object.put("value", invValue);

				return object;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Map<String, Object> deleteInvoiceData(String gstin, Integer id) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
			Map<String, Object> object = new HashMap<>();

			if (!Objects.isNull(taxpayerGstin)) {

				InvoiceVendorDetails details = finderService.getInvoiceVendorDetailsById(taxpayerGstin.getId(), id);
				if (!Objects.isNull(details)) {
					details.setActive((byte) 0);
					crudService.update(details);
					List<InvoiceVendorDetails> vendorDetails = finderService
							.getInvoiceVendorDetails(taxpayerGstin.getId());
					if (Objects.isNull(vendorDetails) || vendorDetails.isEmpty()) {
						object.put("invoiceNo", details.getInvoice().getInvoiceNumber());
						object.put("data", new ArrayList<>());
						object.put("count", 0);
						object.put("value", "0.00");
						return object;
					}

					object.put("invoiceNo", details.getInvoice().getInvoiceNumber());
					object.put("data", vendorDetails);

					BigInteger rowCnt = (BigInteger) em.createNativeQuery(
							"SELECT count(1) FROM invoice_vendor_details WHERE active=1 and taxpayerGstinId = "
									+ taxpayerGstin.getId())
							.getSingleResult();

					object.put("count", rowCnt.intValue());

					Double invValue = (Double) em.createNativeQuery(
							"SELECT SUM(invoiceValue) from invoice_details WHERE id IN (SELECT invoiceId FROM invoice_vendor_details WHERE active=1 and taxpayerGstinId = "
									+ taxpayerGstin.getId() + ")")
							.getSingleResult();

					object.put("value", invValue);

					return object;
				}

				return null;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private InvoiceVendorDetails setInvoiceValue(InvoiceVendorDetails invoiceVendorDetails) throws AppException {

		final String pattern = "#0.00";
		final DecimalFormat myFormatter = new DecimalFormat(pattern);

		Double invoiceValue = 0.0;
		Double taxableValue = 0.0;
		Double taxAmount = 0.0;

		if (!Objects.isNull(invoiceVendorDetails)) {
			InvoiceDetail invoiceDetail = invoiceVendorDetails.getInvoice();
			if (!Objects.isNull(invoiceDetail)) {
				JsonNode jsonNode = invoiceDetail.getItemDetails();
				if (!Objects.isNull(jsonNode)) {
					if (jsonNode.isArray()) {
						for (JsonNode jsonNode2 : jsonNode) {
							if (jsonNode2.has("taxableValue")) {
								taxableValue += jsonNode2.get("taxableValue").asDouble();
							}
							if (jsonNode2.has("igstAmt")) {
								taxAmount += jsonNode2.get("igstAmt").asDouble();
							}
							if (jsonNode2.has("sgstAmt")) {
								taxAmount += jsonNode2.get("sgstAmt").asDouble();
							}
							if (jsonNode2.has("cgstAmt")) {
								taxAmount += jsonNode2.get("cgstAmt").asDouble();
							}
							if (jsonNode2.has("cessAmt")) {
								taxAmount += jsonNode2.get("cessAmt").asDouble();
							}
						}
					}
				}
			}
			invoiceValue = taxableValue + taxAmount;
			invoiceDetail.setTaxableValue(Double.valueOf(myFormatter.format(taxableValue)));
			invoiceDetail.setTaxAmount(Double.valueOf(myFormatter.format(taxAmount)));
			invoiceDetail.setInvoiceValue(Double.valueOf(myFormatter.format(invoiceValue)));

			/***
			 * [0717GNQ00149UNX]
			 * [0-9]{4}[a-zA-Z]{3}[0-9]{5}[1-9A-Za-z]{1}[N]{1}[0-9a-zA-Z]{1}
			 * 
			 */

			if (!StringUtils.isEmpty(invoiceVendorDetails.getVendor().getGstin())) {
				// this is B2B transaction
				final Pattern pattern1 = Pattern
						.compile("[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}");
				if (!pattern1.matcher(invoiceVendorDetails.getVendor().getGstin()).matches()) {
					throw new AppException(ExceptionCode._INVALID_GSTIN);
				}
				invoiceDetail.setType("B2B");
			} else {
				// this is B2C transaction
				if (invoiceValue > 250000.00) {
					invoiceDetail.setType("B2CL");
				} else {
					invoiceDetail.setType("B2CS");
				}
			}

		}

		return invoiceVendorDetails;
	}

	@Override
	public Map<String, Object> createInvoice(InvoiceVendorDetailsDto invoiceVendorDetailsDto, String gstin)
			throws AppException {

		Map<String, Object> object = new HashMap<>();

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				InvoiceVendorDetails details = finderService.findInvoiceByNumber(
						invoiceVendorDetailsDto.getInvoice().getInvoiceNumber(), taxpayerGstin.getId());

				if (Objects.isNull(details)) {
					InvoiceVendorDetails invoiceVendorDetails = new InvoiceVendorDetails();
					invoiceVendorDetails.setBillingAddress(invoiceVendorDetailsDto.getVendor().getAddress());
					invoiceVendorDetails.setActive((byte) 1);
					invoiceVendorDetails.setCreatedBy(
							userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
					invoiceVendorDetails.setCreationIpAddress(userBean.getIpAddress());
					invoiceVendorDetails.setCreationTime(Timestamp.from(Instant.now()));
					invoiceVendorDetails.setShippingAddress(invoiceVendorDetailsDto.getShippingAddress());
					invoiceVendorDetails.setStatus("NEW");
					invoiceVendorDetails.setTaxpayerGstinId(taxpayerGstin);
					invoiceVendorDetails.setVendorName(invoiceVendorDetailsDto.getVendor().getBusinessName());

					InvoiceDetail invoiceDetail = (InvoiceDetail) EntityHelper
							.convert(invoiceVendorDetailsDto.getInvoice(), InvoiceDetail.class);

					invoiceDetail.setInvoiceVendorDetails(invoiceVendorDetails);
					invoiceVendorDetails.setInvoice(invoiceDetail);

					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.DATE, 30);
					cal.set(Calendar.MONTH, 5);
					cal.set(Calendar.YEAR, 2017);
					if (!Objects.isNull(invoiceDetail.getInvoiceDate())) {
						if (invoiceDetail.getInvoiceDate().before(cal.getTime())) {
							throw new AppException(ExceptionCode._INVALID_INVOICE_DATE);
						}
					}

					if (!Objects.isNull(invoiceDetail.getDueDate())) {
						if (invoiceDetail.getDueDate().before(cal.getTime())) {
							throw new AppException(ExceptionCode._INVALID_INVOICE_DATE);
						}
					}

					Vendor vendor = crudService.find(Vendor.class, invoiceVendorDetailsDto.getVendor().getId());
					vendor.setAddress(invoiceVendorDetailsDto.getVendor().getAddress());
					vendor.setGstin(invoiceVendorDetailsDto.getVendor().getGstin());
					vendor = crudService.update(vendor);

					invoiceVendorDetails.setVendor(vendor);

					// set invoice value and type
					setInvoiceValue(invoiceVendorDetails);

					crudService.create(invoiceDetail);

					String nextInvoiceNum = getNextInvoiceNumber(taxpayerGstin);

					boolean isUpdateRequired = false;
					if (!StringUtils.isEmpty(invoiceVendorDetailsDto.getBankAccount())) {
						taxpayerGstin.setBankAccountDetails(invoiceVendorDetailsDto.getBankAccount());
						isUpdateRequired = true;
					}
					if (!StringUtils.isEmpty(invoiceVendorDetailsDto.getTermsConditions())) {
						taxpayerGstin.setTermsConditions(invoiceVendorDetailsDto.getTermsConditions());
						isUpdateRequired = true;
					}
					if (isUpdateRequired) {
						crudService.update(taxpayerGstin);
					}

					object.put("inum", nextInvoiceNum);
					object.put("invoiceNo", invoiceVendorDetailsDto.getInvoice().getInvoiceNumber());

					return object;
				} else {
					throw new AppException(ExceptionCode._DUPLICATE_INVOICE);
				}

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String updateInvoice(InvoiceVendorDetailsDto invoiceVendorDetailsDto, String gstin) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				InvoiceVendorDetails details = finderService.getInvoiceVendorDetailsById(taxpayerGstin.getId(),
						invoiceVendorDetailsDto.getId());
				if (!Objects.isNull(details)) {
					details.setBillingAddress(invoiceVendorDetailsDto.getVendor().getAddress());
					details.setActive((byte) 1);
					details.setCreatedBy(
							userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
					details.setCreationIpAddress(userBean.getIpAddress());
					details.setCreationTime(Timestamp.from(Instant.now()));
					details.setShippingAddress(invoiceVendorDetailsDto.getShippingAddress());
					details.setStatus("NEW");
					details.setTaxpayerGstinId(taxpayerGstin);
					details.setVendor(em.getReference(Vendor.class, invoiceVendorDetailsDto.getVendor().getId()));
					details.setVendorName(invoiceVendorDetailsDto.getVendor().getBusinessName());

					InvoiceDetail invoiceDetailEm = details.getInvoice();
					InvoiceDetail invoiceDetail = (InvoiceDetail) EntityHelper
							.convert(invoiceVendorDetailsDto.getInvoice(), InvoiceDetail.class);
					invoiceDetailEm = EntityHelper.convert(invoiceDetailEm, invoiceDetail);

					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.DATE, 30);
					cal.set(Calendar.MONTH, 5);
					cal.set(Calendar.YEAR, 2017);
					if (!Objects.isNull(invoiceDetail.getInvoiceDate())) {
						if (invoiceDetail.getInvoiceDate().before(cal.getTime())) {
							throw new AppException(ExceptionCode._INVALID_INVOICE_DATE);
						}
					}

					if (!Objects.isNull(invoiceDetail.getDueDate())) {
						if (invoiceDetail.getDueDate().before(cal.getTime())) {
							throw new AppException(ExceptionCode._INVALID_INVOICE_DATE);
						}
					}

					invoiceDetailEm = crudService.update(invoiceDetailEm);

					Vendor vendor = crudService.find(Vendor.class, invoiceVendorDetailsDto.getVendor().getId());
					vendor.setAddress(invoiceVendorDetailsDto.getVendor().getAddress());
					vendor.setGstin(invoiceVendorDetailsDto.getVendor().getGstin());
					vendor = crudService.update(vendor);

					details.setVendor(vendor);

					// set invoice value and type
					setInvoiceValue(details);

					crudService.update(details);

					boolean isUpdateRequired = false;
					if (!StringUtils.isEmpty(invoiceVendorDetailsDto.getBankAccount())) {
						taxpayerGstin.setBankAccountDetails(invoiceVendorDetailsDto.getBankAccount());
						isUpdateRequired = true;
					}
					if (!StringUtils.isEmpty(invoiceVendorDetailsDto.getTermsConditions())) {
						taxpayerGstin.setTermsConditions(invoiceVendorDetailsDto.getTermsConditions());
						isUpdateRequired = true;
					}
					if (isUpdateRequired) {
						crudService.update(taxpayerGstin);
					}

					return invoiceVendorDetailsDto.getInvoice().getInvoiceNumber();

				}

				return null;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String generateInvoicePdf(String gstin, Integer sno) throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				InvoiceVendorDetails details = finderService.getInvoiceVendorDetailsById(taxpayerGstin.getId(), sno);
				if (!Objects.isNull(details)) {

					final String pattern = "#0.00";

					final DecimalFormat df = new DecimalFormat(pattern);

					SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

					Map<String, Object> reportMap = new HashMap<>();

					reportMap.put("invoiceHeader", details.getInvoice().getType() + " Invoice");
					reportMap.put("invoiceSubHeader", details.getInvoice().getNatureOfTransaction());

					reportMap.put("invoiceByName", taxpayerGstin.getTaxpayer().getLegalName());
					reportMap.put("invoiceByGstin", taxpayerGstin.getGstin());
					reportMap.put("invoiceByAddress",
							StringUtils.isEmpty(taxpayerGstin.getAddress()) ? "" : taxpayerGstin.getAddress());

					reportMap.put("invoiceNumber", details.getInvoice().getInvoiceNumber());
					reportMap.put("invoiceDate", dateFormat.format(details.getInvoice().getInvoiceDate()));
					reportMap.put("poNumber", StringUtils.isEmpty(details.getInvoice().getPoNumber()) ? ""
							: details.getInvoice().getPoNumber());
					reportMap.put("nature", details.getInvoice().getNatureOfTransaction());
					reportMap.put("etin",
							StringUtils.isEmpty(details.getInvoice().getEtin()) ? "" : details.getInvoice().getEtin());

					if (StringUtils.isEmpty(reportMap.get("etin").toString())) {
						reportMap.put("labelEtin", "");
					} else {
						reportMap.put("labelEtin", "E-TIN");
					}

					reportMap.put("invoiceToName", details.getVendor().getOrganisation());
					reportMap.put("invoiceToGstin", details.getVendor().getGstin());
					reportMap.put("invoiceToAddress", details.getVendor().getAddress());
					reportMap.put("invoiceToShipAddress",
							StringUtils.isEmpty(details.getShippingAddress()) ? "" : details.getShippingAddress());

					List<InvoiceLineItemDTO> itemDTOs = new ArrayList<>();

					Double igstAmount = 0.0;
					Double cgstAmount = 0.0;
					Double sgstAmount = 0.0;
					Double cessAmount = 0.0;

					int i = 1;

					if (!Objects.isNull(details)) {
						InvoiceDetail invoiceDetail = details.getInvoice();
						if (!Objects.isNull(invoiceDetail)) {
							JsonNode jsonNode = invoiceDetail.getItemDetails();
							if (!Objects.isNull(jsonNode)) {
								if (jsonNode.isArray()) {
									for (JsonNode jsonNode2 : jsonNode) {
										Double taxAmount = 0.0;
										InvoiceLineItemDTO itemDTO = new InvoiceLineItemDTO();
										if (jsonNode2.has("igstAmt")) {
											taxAmount += jsonNode2.get("igstAmt").asDouble();
											igstAmount += jsonNode2.get("igstAmt").asDouble();
										}
										if (jsonNode2.has("sgstAmt")) {
											taxAmount += jsonNode2.get("sgstAmt").asDouble();
											sgstAmount += jsonNode2.get("sgstAmt").asDouble();
										}
										if (jsonNode2.has("cgstAmt")) {
											taxAmount += jsonNode2.get("cgstAmt").asDouble();
											cgstAmount += jsonNode2.get("cgstAmt").asDouble();
										}
										if (jsonNode2.has("cessAmt")) {
											taxAmount += jsonNode2.get("cessAmt").asDouble();
											cessAmount += jsonNode2.get("cessAmt").asDouble();
										}

										itemDTO.setTaxAmount(df.format(taxAmount));
										itemDTO.setSerialNo(String.format("%02d", i));

										if (jsonNode2.has("description")
												&& !StringUtils.isEmpty(jsonNode2.get("description").asText()))
											itemDTO.setItemDesc(jsonNode2.get("description").asText());
										else
											itemDTO.setItemDesc("");

										if (jsonNode2.has("type")
												&& !StringUtils.isEmpty(jsonNode2.get("type").asText()))
											itemDTO.setRemark(jsonNode2.get("type").asText());
										else
											itemDTO.setRemark("");

										if (jsonNode2.has("hsnCode")
												&& !StringUtils.isEmpty(jsonNode2.get("hsnCode").asText()))
											itemDTO.setHsn(jsonNode2.get("hsnCode").asText());
										else
											itemDTO.setHsn("");

										if (jsonNode2.has("unit")
												&& !StringUtils.isEmpty(jsonNode2.get("unit").asText()))
											itemDTO.setUnit(jsonNode2.get("unit").asText());
										else
											itemDTO.setUnit("");

										if (jsonNode2.has("quantity")
												&& !StringUtils.isEmpty(jsonNode2.get("quantity").asText()))
											itemDTO.setQty(jsonNode2.get("quantity").asText());
										else
											itemDTO.setQty("");

										if (jsonNode2.has("perItemRate")
												&& !StringUtils.isEmpty(jsonNode2.get("perItemRate").asText())) {
											double temp = jsonNode2.get("perItemRate").asDouble();
											itemDTO.setRateItem(df.format(temp));
										} else
											itemDTO.setRateItem("");

										if (jsonNode2.has("discountRate")
												&& !StringUtils.isEmpty(jsonNode2.get("discountRate").asText())) {
											double temp = jsonNode2.get("discountRate").asDouble();
											itemDTO.setDiscount(df.format(temp));
										} else
											itemDTO.setDiscount("");

										if (jsonNode2.has("taxableValue")
												&& !StringUtils.isEmpty(jsonNode2.get("taxableValue").asText())) {
											double temp = jsonNode2.get("taxableValue").asDouble();
											itemDTO.setTaxableValue(df.format(temp));
										} else
											itemDTO.setTaxableValue("");

										if (jsonNode2.has("taxRate")
												&& !StringUtils.isEmpty(jsonNode2.get("taxRate").asText())) {
											double temp = jsonNode2.get("taxRate").asDouble();
											itemDTO.setTaxRate(df.format(temp) + "%");
										} else
											itemDTO.setTaxRate("");

										itemDTOs.add(itemDTO);
									}
								}
							}
						}
					}

					reportMap.put("ds1", itemDTOs);

					reportMap.put("pos", details.getInvoice().getPos());

					reportMap.put("igstAmount", df.format(igstAmount));
					reportMap.put("cgstAmount", df.format(cgstAmount));
					reportMap.put("sgstAmount", df.format(sgstAmount));
					reportMap.put("cessAmount", df.format(cessAmount));

					if (igstAmount > 0.0)
						reportMap.put("labelIgst", "Total IGST Amount (Rs.)");
					else {
						reportMap.put("labelIgst", "");
						reportMap.put("igstAmount", "");
					}

					if (cgstAmount > 0.0)
						reportMap.put("labelCgst", "Total CGST Amount (Rs.)");
					else {
						reportMap.put("labelCgst", "");
						reportMap.put("cgstAmount", "");
					}

					if (sgstAmount > 0.0)
						reportMap.put("labelSgst", "Total SGST Amount (Rs.)");
					else {
						reportMap.put("labelSgst", "");
						reportMap.put("sgstAmount", "");
					}

					if (cessAmount > 0.0)
						reportMap.put("labelCess", "Total CESS Amount (Rs.)");
					else {
						reportMap.put("labelCess", "");
						reportMap.put("cessAmount", "");
					}

					reportMap.put("invoiceValue", df.format(details.getInvoice().getInvoiceValue()));
					reportMap.put("taxableValue", df.format(details.getInvoice().getTaxableValue()));
					reportMap.put("taxAmount", df.format(details.getInvoice().getTaxAmount()));
					reportMap.put("invoiceWords",
							NumberToWordsConverter.convert(details.getInvoice().getInvoiceValue()));

					reportMap.put("invoiceRemarks", StringUtils.isEmpty(details.getInvoice().getRemarks()) ? ""
							: details.getInvoice().getRemarks());
					reportMap.put("bankAccount", StringUtils.isEmpty(taxpayerGstin.getBankAccountDetails()) ? ""
							: taxpayerGstin.getBankAccountDetails());

					String footer = taxpayerGstin.getTaxpayer().getLegalName()
							+ (StringUtils.isEmpty(taxpayerGstin.getAddress()) ? "" : taxpayerGstin.getAddress());
					reportMap.put("footer", StringUtils.isEmpty(footer) ? "" : footer);
					reportMap.put("subFooter", StringUtils.isEmpty(taxpayerGstin.getEmailAddress()) ? ""
							: "email: " + taxpayerGstin.getEmailAddress());

					String filePath = InvoiceReport.generateInvoice(reportMap);

					return filePath;
				}

				return null;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean sendInvoiceEmail(String gstin, Integer sno, Integer cc, Integer attach, EmailDTO emailDTO)
			throws AppException {

		if (!StringUtils.isEmpty(gstin)) {

			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);

			if (!Objects.isNull(taxpayerGstin)) {

				Map<String, String> params = new HashMap<>();
				params.put("attachmentText", "");

				if (cc != null && cc == 1) {
					if (Utility.validateEmail(taxpayerGstin.getEmailAddress()))
						emailDTO.setMailcc(taxpayerGstin.getEmailAddress());
				}

				if (attach != null && attach == 1) {
					String filePath = generateInvoicePdf(gstin, sno);
					if (!StringUtils.isEmpty(filePath)) {
						emailDTO.setAttachments(filePath);
						params.put("attachmentText", "Please find attached herewith Invoice.");
					}
				}

				InvoiceVendorDetails details = finderService.getInvoiceVendorDetailsById(taxpayerGstin.getId(), sno);
				if (!Objects.isNull(details)) {
					params.put("invoiceNumber", details.getInvoice().getInvoiceNumber());
					params.put("vendorLegalName", details.getVendor().getOrganisation());
					params.put("invoiceDate", details.getInvoice().getInvoiceDate().toString());
					params.put("legalName", taxpayerGstin.getTaxpayer().getLegalName());
					params.put("invoiceValue", String.valueOf(details.getInvoice().getInvoiceValue()));
				}

				if (!StringUtils.isEmpty(emailDTO.getRemarks())) {
					params.put("remarks", emailDTO.getRemarks());
				}

				if (!StringUtils.isEmpty(taxpayerGstin.getEmailAddress())) {
					params.put("emailAddress", taxpayerGstin.getEmailAddress());
				}

				EmailFactory emailFactory = new EmailFactory();
				boolean emaiLFlag = emailFactory.sendInvoiceEmail(params, emailDTO);

				return emaiLFlag;

			} else {
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	/**
	 * STARTED - PENDING - PROCESSING - COMPLETED
	 */
	@Override
	public Integer updateDataUploadInfo(DataUploadInfoDTO infoDTO) throws AppException {

		if (!Objects.isNull(infoDTO)) {

			Integer id = infoDTO.getId();
			boolean isOwner = true;

			DataUploadInfo info = null;
			if (!Objects.isNull(id)) {
				info = em.find(DataUploadInfo.class, id);
				info.setUrl(infoDTO.getUrl());
				if (!Objects.isNull(userBean.getUserDto())) {
					info.setUpdatedBy(userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
					info.setUpdationIpAddress(userBean.getIpAddress());
				}
				info.setUpdationTime(Timestamp.from(Instant.now()));
				info.setStatus("PENDING");
				info = crudService.update(info);
			} else {
				// Commenting this to allow files from all the user.

				/*
				 * List<UserGstin> userGstins = finderService
				 * .findUserGstinByUserTaxpayer(userBean.getUserDto(). getUsername(),
				 * infoDTO.getGstin()); if (!Objects.isNull(userGstins) &&
				 * !userGstins.isEmpty()) { for (UserGstin userGstin : userGstins) { if
				 * (userGstin.getGstReturnBean() == null) { isOwner = true; } } }
				 */

				if (isOwner) {
					info = (DataUploadInfo) EntityHelper.convert(infoDTO, DataUploadInfo.class);
					info.setStatus("STARTED");
					if (!Objects.isNull(userBean.getUserDto())) {
						info.setUser(em.find(User.class, userBean.getUserDto().getId()));
						info.setCreatedBy(
								userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
						info.setCreationIpAddress(userBean.getIpAddress());
					}
					info.setCreationTime(Timestamp.from(Instant.now()));
					info = crudService.create(info);
				} else {
					throw new AppException(ExceptionCode._UNAUTHORISED_GSTIN_ACCESS);
				}
			}

			return info.getId();
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	/**
	 * STARTED - PENDING - PROCESSING - COMPLETED
	 */
	@Override
	@Transactional
	public Map<String, Object> updateDataUploadInfo(List<DataUploadInfoDTO> infoDTOs) throws AppException {

		int count = 0;
		Map<String, Object> responseMap = new HashMap<>();
		List<DataUploadInfo> dataUploadInfos = new ArrayList<>();

		if (!Objects.isNull(infoDTOs)) {
			// String gstin,String monthYear,String returnType
			for (DataUploadInfoDTO infoDTO : infoDTOs) {

				boolean submitCheck = false;
				
				TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(infoDTO.getGstin());
				
				if (!Objects.isNull(taxpayerGstin)) {
				
				String financialYear=CalanderCalculatorUtility
						.getFiscalYearByMonthYear(Utility.convertStrToYearMonth(infoDTO.getMonthYear()));

				FilePrefernce filePrefernce = finderService.findpreference(infoDTO.getGstin(),financialYear);

				if (Objects.isNull(filePrefernce))
					throw new AppException("40100", String
							.format("Please select filing preference for gstin %s finanacialyear  %s, at EMG portal", 
									infoDTO.getGstin(),financialYear));

				if (!Objects.isNull(infoDTO) && Objects.isNull(infoDTO.getIsPrimary())) {
					infoDTO.setIsPrimary((byte) 1);
				}

					submitCheck = finderService.getSubmitDetails(infoDTO.getGstin(), infoDTO.getMonthYear(), infoDTO.getReturnType());

				if (!submitCheck) {
					DataUploadInfo info = (DataUploadInfo) EntityHelper.convert(infoDTO, DataUploadInfo.class);
					info.setStatus("STARTED");
					if (!Objects.isNull(userBean.getUserDto())) {
						info.setUser(em.find(User.class, userBean.getUserDto().getId()));
						info.setCreatedBy(
								userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
						info.setCreationIpAddress(userBean.getIpAddress());
					}

					String temp = info.getUrl().substring(info.getUrl().indexOf("mycontainer/") + 12);
					temp = temp.substring(0, temp.indexOf("/"));

					SourceType source = SourceType.ERP;

					if (!Objects.isNull(temp) && !Objects.isNull(SourceType.valueOf(temp.toUpperCase()))) {
						source = SourceType.valueOf(temp.toUpperCase());
					}

					ErpLog erpLog = new ErpLog();
					erpLog.setCreationTime(Timestamp.from(Instant.now()));
					erpLog.setDescription(
							"File name " + info.getFileName() + " received successfully from " + source.toString());
					erpLog.setGstin(info.getGstin());
					erpLog.setMonthYear(info.getMonthYear());
					erpLog.setReturnType(info.getReturnType());
					erpLog.setStatus("SUCCESS");
					erpLog.setTransactionType(info.getTransaction());
					erpLog.setFilename(info.getFileName());

					TaxpayerGstin gstin = finderService.findTaxpayerGstin(info.getGstin());
					if (!Objects.isNull(gstin)) {
						info.setTaxpayerGstin(gstin);
						List<UserGstin> list = finderService.findUserGstinByTaxpayer(gstin.getId());
						if (!Objects.isNull(list)) {
							list.removeIf(l -> l.getGstReturnBean() != null);
							if (!list.isEmpty()) {
								erpLog.setUser(list.get(0).getUser());
								info.setUser(list.get(0).getUser());
							}
						}

						if (info.getIsPrimary() == 0) {
							ErpConfig erpc = finderService.findErpConfigUsingUniqueKey("%" + info.getMachineKey() + "%",
									"%" + info.getSerialNo() + "%");
							if (!Objects.isNull(erpc)) {
								erpLog.setUser(erpc.getUser());
								info.setUser(erpc.getUser());
							}
						}
					}
					if (!Objects.isNull(finderService.findErpConfigUsingUniqueKey("%" + info.getMachineKey() + "%",
							"%" + info.getSerialNo() + "%"))) {
						info.setMatched((byte) 1);
					} else {
						info.setMatched((byte) 0);
					}
					info.setCreationTime(Timestamp.from(Instant.now()));

					info = crudService.create(info);
					dataUploadInfos.add(info);
					erpLog = crudService.create(erpLog);
					count++;
				}else {
					throw new AppException(ExceptionCode._ALREADY_FILED_CUSTOM_MSG,
							String.format("Gsitn %s has been already filed %s for return period %s ",
									infoDTO.getGstin(),infoDTO.getReturnType(),infoDTO.getMonthYear()));
				}
			}else {
				
				throw new AppException("40011",String.format("Gstin : %s not registered with easemygst",infoDTO.getGstin()));
			}
			}
			responseMap.put("count", count);
			responseMap.put("data", dataUploadInfos);

			return responseMap;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	/**
	 * STARTED - PENDING - PROCESSING - COMPLETED
	 */
	@Override
	public Map<String, Object> getDataUploadInfo(Integer sno) throws AppException {

		Map<String, Object> responseMap = new HashMap<>();
		List<DataUploadInfo> dataUploadInfos = new ArrayList<>();

		if (!Objects.isNull(sno)) {

			DataUploadInfo info = em.find(DataUploadInfo.class, sno);
			if (!finderService.isSubmit(info.getGstin(), info.getMonthYear(), info.getReturnType())) {

				ErpLog erpLog = new ErpLog();
				erpLog.setCreationTime(Timestamp.from(Instant.now()));
				if (info.getFileName().contains("#")) {
					erpLog.setDescription(
							"File name " + info.getFileName().split("#")[0] + " processing started successfully");
				} else {
					erpLog.setDescription("File name " + info.getFileName() + " processing started successfully");
				}

				erpLog.setGstin(info.getGstin());
				erpLog.setMonthYear(info.getMonthYear());
				erpLog.setReturnType(info.getReturnType());
				erpLog.setStatus("SUCCESS");
				erpLog.setTransactionType(info.getTransaction());
				erpLog.setFilename(info.getFileName());

				TaxpayerGstin gstin = finderService.findTaxpayerGstin(info.getGstin());
				if (!Objects.isNull(gstin)) {
					info.setTaxpayerGstin(gstin);
					List<UserGstin> list = finderService.findUserGstinByTaxpayer(gstin.getId());
					if (!Objects.isNull(list)) {
						list.removeIf(l -> l.getGstReturnBean() != null);
						if (!list.isEmpty()) {
							erpLog.setUser(list.get(0).getUser());
							info.setUser(list.get(0).getUser());
						}
					}

					if (info.getIsPrimary() == 0) {
						ErpConfig erpc = finderService.findErpConfigUsingUniqueKey("%" + info.getMachineKey() + "%",
								"%" + info.getSerialNo() + "%");
						if (!Objects.isNull(erpc)) {
							erpLog.setUser(erpc.getUser());
							info.setUser(erpc.getUser());
						}
					}
				}

				if (!Objects.isNull(finderService.findErpConfigUsingUniqueKey("%" + info.getMachineKey() + "%",
						"%" + info.getSerialNo() + "%"))) {
					info.setMatched((byte) 1);
				} else {
					info.setMatched((byte) 0);
				}
				info.setUpdationTime(Timestamp.from(Instant.now()));
				info = crudService.update(info);
				erpLog = crudService.create(erpLog);

				dataUploadInfos.add(info);

				responseMap.put("count", dataUploadInfos.size());
				responseMap.put("data", dataUploadInfos);

				return responseMap;

			}
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return responseMap;

	}

	public DataUploadInfo consolidatedExcelUploadInfo(String datauploadInfoId, String fileName) {

		DataUploadInfo previousDatauploadInfoId = em.find(DataUploadInfo.class, Integer.parseInt(datauploadInfoId));

		DataUploadInfo info = new DataUploadInfo();

		info.setGstin(previousDatauploadInfoId.getGstin());
		info.setFileName(fileName);
		info.setMonthYear(previousDatauploadInfoId.getMonthYear());
		info.setReturnType(previousDatauploadInfoId.getReturnType());
		info.setIsPrimary(previousDatauploadInfoId.getIsPrimary());
		info.setStatus("STARTED");
		info.setMachineKey(previousDatauploadInfoId.getMachineKey());
		info.setSerialNo(previousDatauploadInfoId.getSerialNo());

		info.setTaxpayerGstin(previousDatauploadInfoId.getTaxpayerGstin());
		info.setReturnType(previousDatauploadInfoId.getReturnType());
		info.setTransaction(previousDatauploadInfoId.getTransaction());

		info.setCreationTime(Timestamp.from(Instant.now()));

		ErpLog erpLog = new ErpLog();
		erpLog.setCreationTime(Timestamp.from(Instant.now()));

		erpLog.setUser(previousDatauploadInfoId.getUser());
		info.setUser(previousDatauploadInfoId.getUser());
		erpLog.setDescription("File name " + fileName + " processing started successfully");
		erpLog.setGstin(info.getGstin());
		erpLog.setMonthYear(info.getMonthYear());
		erpLog.setReturnType(info.getReturnType());
		erpLog.setStatus("SUCCESS");
		erpLog.setTransactionType(info.getTransaction());
		erpLog.setFilename(info.getFileName());
		info.setUpdationTime(Timestamp.from(Instant.now()));
		info = crudService.create(info);
		erpLog = crudService.create(erpLog);

		return info;
	}

	@Override
	public List<DataUploadInfo> getErpDataUpload() throws AppException {

		int erpId = userBean.getUserDto().getId();
		User userEm = em.find(User.class, erpId);
		if (!Objects.isNull(userEm.getOrganisation())) {
			if (!userEm.getOrganisation().getId().equals(userBean.getUserDto().getOrganisation().getId())) {
				UserDTO userOrg = finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
				erpId = userOrg.getId();
			}
		} else {
			UserDTO userOrg = finderService.findUserByOrganisation(userBean.getUserDto().getOrganisation().getId());
			erpId = userOrg.getId();
		}

		ErpConfig erpConfig = finderService.findErpConfigByUser(erpId);
		if (!Objects.isNull(erpConfig)) {
			String mKey = erpConfig.getUniqueKey();
			String sNo = erpConfig.getSerialNo();
			String[] mKeys = {};
			String[] sNos = {};
			if (!StringUtils.isEmpty(mKey)) {
				mKeys = mKey.split(",");
			}
			if (!StringUtils.isEmpty(mKey)) {
				sNos = sNo.split(",");
			}
			List<DataUploadInfo> dataUploadInfos = finderService.findDataUploadInfoKey(mKeys, sNos);
			return dataUploadInfos;
		} else {
			return new ArrayList<>();
		}
	}

	public List<MonthSummaryDTO> updateMonthYear(TaxpayerGstin taxpayerGstin) throws AppException {

		List<MonthSummaryDTO> monthSummaryDTOs = new ArrayList<>();

		SimpleDateFormat sdfBread = new SimpleDateFormat("MMM: yy");

		if (!Objects.isNull(taxpayerGstin)) {
			// try {
			// String monthYr = taxpayerGstin.getMonthYear();
			// MonthSummaryDTO monthSummDTO = null;
			// if (!StringUtils.isEmpty(monthYr)) {
			// monthSummDTO = JsonMapper.objectMapper.readValue(monthYr,
			// MonthSummaryDTO.class);
			// monthSummaryDTOs.add(monthSummDTO);
			// return monthSummaryDTOs;
			// }
			// } catch (IOException e) {
			// log.error("Error {} while processing getReturnSummaryByGstin to
			// get monthSummaryDTO", e.getMessage());
			// }

			List<GstinAction> actions = finderService.findGstinActionByTaxpayerGstinId(taxpayerGstin.getId());
			if (Objects.isNull(actions))
				actions = new ArrayList<>();

			List<BrtMappingDTO> brtMappingDTOs = null;
			try {
				brtMappingDTOs = getBrtMappingByGstin(taxpayerGstin.getGstin());
			} catch (AppException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			TreeSet<GstReturnDTO> monthlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
			TreeSet<GstReturnDTO> quarterlyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());
			TreeSet<GstReturnDTO> annuallyComp = new TreeSet<GstReturnDTO>(new GstReturnComparator());

			for (BrtMappingDTO brtMappingDTO : brtMappingDTOs) {
				GstReturnDTO gstReturnDTO = brtMappingDTO.getGstReturnBean();
				if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.MONTHLY.toString()))
					monthlyComp.add(gstReturnDTO);
				else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.QUARTERLY.toString()))
					quarterlyComp.add(gstReturnDTO);
				else if (gstReturnDTO.getFrequency().equals(GstReturnFrequency.ANNUALLY.toString()))
					annuallyComp.add(gstReturnDTO);
			}

			SimpleDateFormat fullDate = new SimpleDateFormat("dd MMM yyyy");

			Date gstStartDate = SerializationUtils
					.deserialize(redisService.getBytes(ApplicationMetadata.GST_START_DATE));
			Calendar cal = Calendar.getInstance();
			Calendar prevCal = Calendar.getInstance();
			Date currentDate = cal.getTime();
			Date startDate = cal.getTime();

			int index = 0;
			List<String> monthYears = new ArrayList<>();
			while (gstStartDate.compareTo(startDate) <= 0) {

				prevCal.setTime(cal.getTime());
				prevCal.add(Calendar.MONTH, -1);
				boolean startFlag = false;
				Date initDate = prevCal.getTime();
				startFlag = gstStartDate.compareTo(initDate) > 0;

				MonthSummaryDTO monthSummaryDTO = new MonthSummaryDTO();
				monthSummaryDTO.setId(index++);

				String month = String.valueOf(startDate.getMonth() + 1);
				month = month.length() == 1 ? "0" + month : month;
				month = month + (1900 + startDate.getYear());

				String prevMonth = String.valueOf(prevCal.getTime().getMonth() + 1);
				prevMonth = prevMonth.length() == 1 ? "0" + prevMonth : prevMonth;
				prevMonth = prevMonth + (1900 + prevCal.getTime().getYear());

				String my = Utility.getMonth(startDate.getMonth());
				monthYears.add(my);
				monthSummaryDTO.setMonthName(my + " " + (1900 + startDate.getYear()));
				monthSummaryDTO.setBreadcrumb(sdfBread.format(startDate).replace(":", "'"));
				monthSummaryDTO.setCode(month);

				List<ReturnSummaryDTO> returnSummaryDTOs = new ArrayList<>();
				monthSummaryDTO.setReturnSummary(returnSummaryDTOs);

				for (GstReturnDTO gstReturnDTO : monthlyComp) {

					ReturnSummaryDTO returnSummaryDTO = new ReturnSummaryDTO();

					String status = ReturnCalendar.BLANK.toString();
					GstinAction temp = new GstinAction();
					temp.setParticular(month);
					temp.setReturnType(gstReturnDTO.getCode());
					temp.setStatus(ReturnCalendar.FILED.toString());
					Date returnDueDate = gstReturnDTO.getDueDate();
					Date returnStartDate = gstReturnDTO.getStartDate();

					Calendar newCal = Calendar.getInstance();
					newCal.set(Calendar.DATE, returnDueDate.getDate());
					newCal.set(Calendar.YEAR, 1900 + startDate.getYear());
					newCal.set(Calendar.MONTH, startDate.getMonth());
					newCal.add(Calendar.MONTH, 1);
					if (actions.contains(temp)) {
						temp = actions.get(actions.indexOf(temp));
					} else {
						temp = null;
					}

					Date newTime = newCal.getTime();

					Calendar newCal2 = Calendar.getInstance();
					newCal2.set(Calendar.DATE, returnStartDate.getDate());
					Date newStartTime = newCal2.getTime();

					double diff = currentDate.getTime() - newTime.getTime();
					double startDiff = currentDate.getTime() - newStartTime.getTime();

					if (!Objects.isNull(temp) && temp.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
						status = ReturnCalendar.FILED.toString();
						returnSummaryDTO.setDiff(0);
					} else {
						diff = diff / (24 * 60 * 60 * 1000);
						if (startDiff >= 0.0 && diff <= 0.0) {
							status = ReturnCalendar.APPROACHING.toString();
						} else if (diff > 0.0) {
							status = ReturnCalendar.LOCKED.toString();
							if (gstReturnDTO.getMandatory() == 1) {
								status = ReturnCalendar.OVERDUE.toString();
							}
							if (currentDate.getDate() >= 11 && currentDate.getDate() <= 15
									&& gstReturnDTO.getCode().equalsIgnoreCase(ReturnType.GSTR1.toString())) {
								// status = ReturnCalendar.ONHOLD.toString();
								status = ReturnCalendar.OVERDUE.toString();

							}
						}

						if (gstReturnDTO.getDependent() > 0) {
							GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getDependent());
							GstinAction temp1 = new GstinAction();
							temp1.setReturnType(gstReturn.getCode());
							temp1.setStatus(GstnAction.FILED.toString());
							if (gstReturn.getPrevMonth() == 1)
								temp1.setParticular(prevMonth);
							else
								temp1.setParticular(month);
							if (!actions.contains(temp1)) {
								status = status + "_LOCKED";
							}

							if (gstReturn.getPrevMonth() == 1 && startFlag)
								status = status.replaceAll("_LOCKED", "");
						}

						if (gstReturnDTO.getLockedAfter() > 0) {
							GstReturn gstReturn = finderService.findGstReturn(gstReturnDTO.getLockedAfter());
							GstinAction temp1 = new GstinAction();
							temp1.setReturnType(gstReturn.getCode());
							temp1.setParticular(month);
							temp1.setStatus(GstnAction.FILED.toString());
							if (actions.contains(temp1) && status.indexOf("_LOCKED") == -1) {
								status = status + "_LOCKED";
							}
						}

						returnSummaryDTO.setDiff(Double.valueOf(diff).intValue());
					}

					// Introduces this because of pre process to incorporate
					// Ledger in workflow
					if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {

						ReturnSummaryDTO returnSummaryDTO1 = new ReturnSummaryDTO();

						String status1 = ReturnCalendar.BLANK.toString();
						GstinAction temp1 = new GstinAction();
						temp1.setParticular(month);
						temp1.setReturnType(gstReturnDTO.getCode());

						if (actions.contains(temp1)) {
							temp1 = actions.get(actions.indexOf(temp1));
						} else {
							temp1 = null;
						}

						if (!Objects.isNull(temp1)
								&& temp1.getStatus().equalsIgnoreCase(ReturnCalendar.FILED.toString())) {
							status1 = ReturnCalendar.FILED.toString();
							returnSummaryDTO1.setDiff(Double.valueOf("0").intValue());
						} else {
							status1 = status;
							returnSummaryDTO1.setDiff(Double.valueOf(diff).intValue());
						}

						String name = "", description = "";
						if (!StringUtils.isEmpty(gstReturnDTO.getPreProcess())) {
							String temp11 = gstReturnDTO.getPreProcess();
							if (temp11.contains("-")) {
								String[] values = temp11.split("-");
								name = values[0];
								description = values[1];
							} else {
								name = gstReturnDTO.getPreProcess();
								description = gstReturnDTO.getPreProcess();
							}
						}

						returnSummaryDTO1.setDueDate(fullDate.format(newCal.getTime()));
						returnSummaryDTO1.setReturnName(name);
						returnSummaryDTO1.setStatus(status1);
						returnSummaryDTO1.setDescription(description);
						returnSummaryDTO1.setName(name);
						returnSummaryDTOs.add(returnSummaryDTO1);
					}
					// ends here

					returnSummaryDTO.setDueDate(fullDate.format(newCal.getTime()));
					returnSummaryDTO.setReturnName(gstReturnDTO.getCode());
					returnSummaryDTO.setStatus(status);
					returnSummaryDTO.setDescription(gstReturnDTO.getDescription());
					returnSummaryDTO.setName(gstReturnDTO.getName());
					returnSummaryDTOs.add(returnSummaryDTO);
				}
				monthSummaryDTOs.add(monthSummaryDTO);
				cal.add(Calendar.MONTH, -1);
				startDate = cal.getTime();
			}

			MonthSummaryDTO monthSummaryDTO = new MonthSummaryDTO();
			monthSummaryDTO = monthSummaryDTOs.get(monthSummaryDTOs.size() - 1);
			monthSummaryDTOs.clear();
			monthSummaryDTOs.add(monthSummaryDTO);

			try {
				String monthYear = JsonMapper.objectMapper.writeValueAsString(monthSummaryDTO);
				taxpayerGstin.setMonthYear(monthYear);
				taxpayerGstin
						.setUpdatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
				taxpayerGstin.setUpdationIpAddress(userBean.getIpAddress());
				taxpayerGstin.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
				crudService.update(taxpayerGstin);
			} catch (JsonProcessingException e) {
				log.error("Error {} while processing getReturnSummaryByGstin to get monthSummaryDTO", e.getMessage());
			} finally {
				log.info("getGstReturnSummaryByGstin method ends..");
				return monthSummaryDTOs;
			}
		} else {
			log.info("getGstReturnSummaryByGstin method throws AppException");
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public enum ReturnPrefernce {
		M("M"), Q("Q");
		private String type;

		private ReturnPrefernce(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type;
		};

	}

	@Override
	public boolean savePreference(String gstin, String prefernce, String fyear) throws AppException {
		FilePrefernce fileprefernce = finderService.findpreference(gstin, fyear);
		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
		if (Objects.isNull(fileprefernce)) {
			FilePrefernce filePrefernce = new FilePrefernce();
			filePrefernce.setTaxpayerGstin(taxpayerGstin);
			filePrefernce.setPreference(prefernce.toUpperCase());
			filePrefernce.setFinancialYear(fyear);
			filePrefernce.setCreationIpAddress(userBean.getIpAddress());
			filePrefernce.setCreatedBy(userBean.getUserDto().getUsername());
			crudService.create(filePrefernce);
			/* crudService.update(taxpayerGstin); */
			String redisTaxpayerKey = userBean.getUserDto().getId() + "-"
					+ userBean.getUserDto().getOrganisation().getId() + "-" + fyear;
			if (redisService.isKeyExist(redisTaxpayerKey))
				redisService.removeUserOrgTaxpayer(redisTaxpayerKey);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void proecessConsolidateCsv() {
	}

	/*
	 * private void processCosolidatedByTransaction(TransactionType tran){ try{
	 * GstinTransactionDTO gstinTransactionDto=new GstinTransactionDTO();
	 * gstinTransactionDto.setReturnType(ReturnType.GSTR1.toString());
	 * gstinTransactionDto.setTransactionType(tran.toString());
	 * gstinTransactionDto.setMonthYear("082017");
	 * gstinTransactionDto.setTaxpayerGstin(this.getTaxpayerGstin("33GSPTN0032G1ZN")
	 * );
	 * 
	 * //List result=consolidatedCsvManager.getDataByReturnTypeTransaction(
	 * gstinTransactionDto, ""); List result=new
	 * ConsolidatedService().getDataByReturnTypeTransaction(gstinTransactionDto,
	 * ""); log.info("list values {}",ToStringBuilder.reflectionToString(
	 * gstinTransactionDto));
	 * consolidatedCsvManager.saveData(gstinTransactionDto,result); log.info("end");
	 * }catch(AppException ae){ ae.printStackTrace(); } }
	 */

	@Override
	public Map<String, Object> getAlltinDetail(int userId) throws AppException {

		log.info("all tin detail..");
		Map<String, Object> obj = new HashMap<>();
		List<TinDTO> tinList = new ArrayList<>();
		if (userId > 0) {
			List<UserGstin> userGstins = finderService.getUserGstinByUserOrg(userId,
					userBean.getUserDto().getOrganisation().getId());

			for (UserGstin userGstin : userGstins) {
				TaxpayerGstin taxpayerGstin = userGstin.getTaxpayerGstin();
				log.info("getTaxpayer successfully retrieved the taxpayer gstin {}", taxpayerGstin);
				TinDTO tin = new TinDTO();
				tin.setGstin(taxpayerGstin.getGstin());
				tin.setFlushData(taxpayerGstin.isFlushData());
				tinList.add(tin);
			}
			obj.put("tindata", tinList);
			String redisalltinKey = userBean.getUserDto().getId() + "-"
					+ userBean.getUserDto().getOrganisation().getId() + "-all";
			redisService.setUserallTin(redisalltinKey, tinList, true, 1800);

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return obj;
	}

	@Override
	public boolean updateflagTaxpayerGstin(List<TinDTO> modifiedlist) throws AppException {
		String redisalltinKey = userBean.getUserDto().getId() + "-" + userBean.getUserDto().getOrganisation().getId()
				+ "-all";
		List<TinDTO> result = null;
		List<TinDTO> unmodifiedlist = redisService.getUserAlltin(redisalltinKey);
		if (!Objects.isNull(unmodifiedlist)) {
			result = modifiedlist.stream().filter(aObject -> {
				return !unmodifiedlist.contains(aObject);
			}).collect(Collectors.toList());
		} else {
			result = modifiedlist;
		}
		for (TinDTO tinDTO : result) {
			TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(tinDTO.getGstin());
			taxpayerGstin.setFlushData(tinDTO.isFlushData());
			crudService.update(taxpayerGstin);
		}
		redisService.removeUserOrgTaxpayer(redisalltinKey);
		return true;
	}

	public String registerTaxpayerForEWayBill(String gstin, String username, String password) {

		TaxpayerGstin taxpayerGstin = finderService.findTaxpayerGstin(gstin);
		if (!Objects.isNull(taxpayerGstin))
			this.updateEWBWithExistingTaxpayer(taxpayerGstin, username, password);
		else
			this.registerNewTaxpayerForEwayBill(gstin, username, password);
		return null;
	}

	private TaxpayerGstin updateEWBWithExistingTaxpayer(TaxpayerGstin taxpayerGstin, String username, String password) {

		taxpayerGstin.setEwayBillUserName(username);
		taxpayerGstin.setEwayBillPassword(password);
		return em.merge(taxpayerGstin);
	}

	private TaxpayerGstin registerNewTaxpayerForEwayBill(String gstin, String username, String password) {

		TaxpayerGstin taxpayerGstin = new TaxpayerGstin();
		taxpayerGstin.setGstin(gstin);
		taxpayerGstin.setEwayBillPassword(password);
		taxpayerGstin.setEwayBillPassword(password);
		em.persist(taxpayerGstin);
		return taxpayerGstin;
	}

	public String registerTaxpayerForEWayBill(String gstin, String username, String password, Boolean isActive) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateturnoverDetail(FiscalyearDetailDTO fiscalyearDetailDTO) throws AppException {
		// TODO Auto-generated method stub
		log.info("updateturnoverDetail method starts..");

		if (!(Objects.isNull(fiscalyearDetailDTO) && Objects.isNull(fiscalyearDetailDTO.getGstin()))) {
			TaxpayerGstin taxpayerGstinEm = finderService.findTaxpayerGstin(fiscalyearDetailDTO.getGstin());
			YearMonth yearMonth = YearMonth.parse(fiscalyearDetailDTO.getMonthyear(),
					DateTimeFormatter.ofPattern("MMyyyy"));
			javax.persistence.Query query = em.createNamedQuery("FilePrefernce.updateturnover");
			query.setParameter("currentGt", BigDecimal.valueOf(fiscalyearDetailDTO.getCurrentGt()));
			query.setParameter("previousGt", BigDecimal.valueOf(fiscalyearDetailDTO.getPreviousGt()));
			query.setParameter("fyear", CalanderCalculatorUtility.getFiscalYearByMonthYear(yearMonth));
			query.setParameter("taxpayergstin", taxpayerGstinEm);
			query.executeUpdate();
			return true;

		} else {
			log.info("updateturnoverDetail method throws AppException");

			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

}