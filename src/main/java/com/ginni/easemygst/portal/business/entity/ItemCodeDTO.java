package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import lombok.Data;

public @Data class ItemCodeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private byte active;

	private String code;
	
	private Double unitRate;
	
	private String taxRate;
	
	private String cessRate;

	private String description;

	private String hsnCode;

	private String hsnDescription;

	private String type;

	private String unit;

	public interface ItemCodeDTOMixIn extends CommonMixin {
		
	}
	
}
