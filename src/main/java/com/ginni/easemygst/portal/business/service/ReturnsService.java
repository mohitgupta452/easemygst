package com.ginni.easemygst.portal.business.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.ejb.Local;

import org.codehaus.jackson.JsonGenerationException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.dto.AcceptWithItcDto;
import com.ginni.easemygst.portal.business.dto.BulkItcDto;
import com.ginni.easemygst.portal.business.dto.ChangeStatusDto;
import com.ginni.easemygst.portal.business.dto.CtinInvoiceDto;
import com.ginni.easemygst.portal.business.dto.DataStatusDTO;
import com.ginni.easemygst.portal.business.dto.GstCalendarDto;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto;
import com.ginni.easemygst.portal.business.dto.ItcAmountDto;
import com.ginni.easemygst.portal.business.dto.OffsetLiabiltyDTO;
import com.ginni.easemygst.portal.business.dto.ReturnHistoryDTO;
import com.ginni.easemygst.portal.business.dto.ReturnSummaryDTO;
import com.ginni.easemygst.portal.business.dto.SearchTaxpayerDTO;
import com.ginni.easemygst.portal.business.dto.TaxSummaryDTO;
import com.ginni.easemygst.portal.business.dto.WrapperDashBoardSummaryDto;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

@Local
public interface ReturnsService {
	
	public enum DeleteType{
		ALL,
		SELECTED
	}
	
	//public int readExcel(String gstReturn, String gstin, String monthYear, File file, String mapping) throws AppException;
	
	/*public int getDataFromErp(GstinTransactionDTO gstinTransactionDTO) throws AppException;*/
	
	public List<ReturnSummaryDTO> getReturnSummaryDto(TaxpayerGstinDTO taxpayerGstin, String month) throws AppException;
	
	public String getReconciledTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public Map<String,Object> getTransactionData(GstinTransactionDTO gstinTransactionDTO,FilterDto filter) throws AppException;
	
	public boolean addTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean updateTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean storeTransactionDataGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean updateTransactionDataModifyByNo(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean updateTransactionErpData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean updateTransactionItcData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean updateRecoTypeTransactionData(GstinTransactionDTO gstinTransactionDTO, String recoType) throws AppException;
	
	public boolean updateReconciledTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException ;
	
	public int deleteTransactionData(String gstin,String monthYear,TransactionType transactionType,ReturnType returnType,List<String> invoices,DeleteType deleteType,DataSource dataSource) throws AppException, JsonGenerationException, org.codehaus.jackson.map.JsonMappingException, IOException, Exception;
	
	public int deleteTransaction(String gstin, String monthYear, TransactionType transactionType,ReturnType returnType, List<String> invoices, DeleteType deleteType,DataSource dataSource) throws AppException;
	
	public boolean changeStatusTransactionData(GstinTransactionDTO gstinTransactionDTO,List<ChangeStatusDto> invoices) throws AppException;
	
	public EmailDTO getEmailContent(GstinTransactionDTO gstinTransactionDTO,String invoiceId) throws AppException;
	
	public Map<String,Object> getTransactionSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public List<List<String>> getTransactionByType(GstinTransactionDTO gstinTransactionDTO, DataFetchType dataFetchType) throws AppException;
	
	public Map<String, List<List<String>>> getExceptionTransactions(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public Map<String,String> syncData(GstinTransactionDTO gstinTransactionDTO)throws AppException, JsonParseException, JsonMappingException, IOException, Exception;
	
	public boolean getMonthSummary(String gstin)throws AppException;
	
	public String getGstr3TransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean updateRecoTypeTransactionDataByNo(GstinTransactionDTO gstinTransactionDTO, String recoType) throws AppException;
	
	public int processCompositeData(String gstReturn,String gstin, String monthYear,String request) throws AppException;
	
	public boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException ;
	
	public Object generateHSNSUM();
	
	public boolean genrateHsnSummary(GstinTransactionDTO gstinTransactionDto)throws AppException;
	
	public  Map<String,Object> getInvoiceData(String transactionType,String invoiceId) throws AppException;
	
//	public List<B2BDetailEntity> b2bTextSearch(String searchStr);
	
	/*public List<List<String>> getExcelDataByFetchType(GstinTransactionDTO gstinTranDto,DataFetchType dataFetchType) throws AppException;*/
	
	public <T>Map<String,Object> getItcBulkItems(GstinTransactionDTO gstinTransactionDTO, String elgType,FilterDto filterDto) throws AppException;


	public enum  GstnAction {
	    
		BLANK("BLANK"), FILED("FILED"), SUBMIT("SUBMIT"), GENERATE("GENERATE"), UTILIZECREDIT("UTILIZECREDIT"), UTILIZECASH("UTILIZECASH");
	    
		private String action;

		GstnAction(String action) {
	        this.action = action;
	    }

	    public String getAction() {
	        return action;
	    }

	    public void setAction(String action) {
	        this.action = action;
	    }
	}

	public boolean saveBulkItcData(GstinTransactionDTO gstinTransactionDto, List<BulkItcDto> ids)throws AppException;

	public WrapperDashBoardSummaryDto getDashboardSummary(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	<T>boolean setItcElgNull(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	boolean setReconcileType(GstinTransactionDTO gstinTransactionDTO, ReconsileType recType) throws AppException;

	Map<String, Object> getMismatchedInvoices(String type, String invoiceId) throws AppException;

	boolean compareTransactions(GstinTransactionDTO gstinTransactionDto);
	
	public  boolean updateB2csTransactionData(B2CSTransactionEntity b2cs)throws AppException;

	boolean saveDocIssueTransactionData(GstinTransactionDTO gstinTransactionDTO) throws AppException;
	
	public boolean isAllSynced(String monthYear,String gstin,ReturnType returnType) throws AppException;

	boolean deleteItemsByInvoiceId(String invoiceId);

	public Map<String, String> processReturnStatus(String id) throws AppException;

	public List<ReturnStatus> getReturnStatusAllByGstn(GstinTransactionDTO gstnTransactionDTO) throws AppException;

	public int saveFileDetails(String rtype, String gstin, String monthYear, String request) throws AppException;
	
	public Map<String,Object> searchText(String text,GstinTransactionDTO gstinTransactionDTO,FilterDto filter) throws AppException;

	public Map<String, Object> getLastSyncWithGstn(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	public List<DataStatusDTO> getDataStatusGstn(GstinTransactionDTO gstnTransactionDTO) throws AppException;

	public ItcAmountDto getItcSummary(GstinTransactionDTO gstinTransactionDTO, String type);

	public ItcAmountDto getItcSummaryForAll(GstinTransactionDTO gstinTransactionDTO);

	public SearchTaxpayerDTO searchTaxPayer(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	public Map<String, Object> getMismatchedInvNoInvoices(String type, String invoiceId, FilterDto filterDto)
			throws AppException;

	public <T>Map<String,Object> getBlankHsnBulkItems(GstinTransactionDTO gstinTransactionDTO, String elgType,FilterDto filterDto) throws AppException;
	
	public Map<String,String> getBlankHsnCount(String monthYear,String gstin,String returnType,String count);
	
	public Item rectifyBlankHSN(String itemId,GstinTransactionDTO gstinTransactionDTO,String hsnCode,String hsnDescription,String unit,Double quantity);
	
	public  Map<String,Object> getMissingInvoices(GstinTransactionDTO gstinTransactionDTO,String missingType,FilterDto request)throws AppException;
	
    public Map<String,Object> getSimilarCtinMissingOutward(GstinTransactionDTO gstinTransactionDTO,CtinInvoiceDto detail)throws AppException;;

/*	public List<CDNDetailEntity> getSimilarCtinMissingOutwardCdn(GstinTransactionDTO gstinTransactionDTO,String invoiceId)throws AppException;;
*/	
	public void copyAcceptedInvItems(String gstin,String monthYear,String returnType,String transaction) throws AppException;

	public boolean changeCtinByInvoiceId(GstinTransactionDTO gstinTransactionDTO, ChangeStatusDto invoice) throws AppException;

/*	public boolean acceptAllFilteredMissingInward(GstinTransactionDTO gstinTransactionDto, String ctin,String acceptType) throws AppException;
*/
	public boolean acceptMissingInwardWithItc(GstinTransactionDTO gstinTransactionDto, AcceptWithItcDto invoice)
			throws AppException;

	public int compareSimmilarNonSimilar(GstinTransactionDTO gstinTransactionDto);
	
	public void copyChecksumToEMG(GstinTransactionDTO gstinTransactionDTO);


	public Map<String, Object> getMismatchTransaction(GstinTransactionDTO gstinTransactionDTO, String mismatchType,
			FilterDto filter) throws AppException;
	
	public Map<String,Object> getGstr2ASumm(GstinTransactionDTO gstinTransactionDTO);

	public boolean executeBulkOperation(GstinTransactionDTO gstinTransactionDto, ChangeStatusDto detail) throws AppException;

	public List<TblOutwardCombinedNew> getGstr3BData(GstinTransactionDTO gstinTransactionDto);

	public Gstr3BRequest getGstr3BRequest(GstinTransactionDTO gstinTransactionDto);

	public Gstr3bDto getGstr3BDataForView(GstinTransactionDTO gstinTransactionDto);

/*	public Map<String, List<GstCalendarDto>> getDueDateCalendar(String gstin);*/

	public boolean updateGstr3b(GstinTransactionDTO gstinTransactionDTO,JsonNode request) throws JsonProcessingException, IOException,AppException;

	public Future<String> syncDataWithGstnAsynchrounously(GstinTransactionDTO gstinTransactionDTO);

	public Map<String, Object> getFilteredReturnStatusAllByGstn(GstinTransactionDTO gstnTransactionDTO, FilterDto filter)
			throws AppException;
	
	public void viewTrackReturnsAsync(TaxpayerGstin taxpayerGstin) throws AppException;

	public Map<String, List<ReturnHistoryDTO>> getReturnHistoryByGstin(String gstin, String financialYear,String monthYear) throws AppException;
	
	public void viewReturns(String gstin) throws AppException;

	Map<String, Object> getTransactionSummary2(GstinTransactionDTO gstinTransactionDTO) throws AppException;

	Map<String, List<GstCalendarDto>> getDueDateCalendar(String gstin, String fyear);

	String getComparisionResultReferenceNumber(String gstin, String cpGstin, String invocieNumber, String financialYear,
			String transaction) throws AppException, JsonProcessingException;

	Map<String, B2BDetailEntity> getComparisionResultFromReferenceNumber(String refenceNumber) throws AppException, JsonParseException, JsonMappingException, IOException;


	public Map<String, List<GstCalendarDto>> getDueDateCalendarQuarterly(String gstin,String fyear);
	
    public TaxSummaryDTO getgstr3bsummary(GstinTransactionDTO gstinTransactionDTO) throws AppException, JsonProcessingException, IOException;

	OffsetLiabiltyDTO calculateOffsetLiabilyDetails(GstinTransactionDTO gstinTransactionDTO) throws AppException, JsonProcessingException, IOException;

	int deleteSavedDatafrom3bsavedsummary(GstinTransactionDTO gstinTransactionDTO);

	Object genrateChallan(String dueDate, String filingDate, GstinTransactionDTO gstinTransactionDTO)
			throws JsonParseException, JsonMappingException, IOException;

	String saveoffsetliablity(GstinTransactionDTO gstinTransactionDTO, String request)
			throws JsonParseException, JsonMappingException, IOException;

	void generate3B(String gstin, String monthYear) throws AppException;
}
