package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@SuppressWarnings("serial")

public @Data class MismatchDTO implements Serializable, Comparable<MismatchDTO> {

	private String gstin;
	
	private String gstinName;
	
	@JsonFormat(pattern = "#0.00")
	private Double taxableValue;
	
	@JsonFormat(pattern = "#0.00")
	private Double taxAmount;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MismatchDTO other = (MismatchDTO) obj;
		if (gstin == null) {
			if (other.gstin != null)
				return false;
		} else if (!gstin.equals(other.gstin))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode());
		return result;
	}
	
	@Override
	public int compareTo(MismatchDTO o) {
		return this.getTaxableValue().compareTo(o.getTaxableValue());
	}
	
}
