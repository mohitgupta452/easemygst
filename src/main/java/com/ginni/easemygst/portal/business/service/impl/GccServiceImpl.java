package com.ginni.easemygst.portal.business.service.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.GccService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.gst.api.GccConsumer;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.mashape.unirest.http.JsonNode;

@Stateless
@Transactional
public class GccServiceImpl implements GccService {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private UserBean userBean;

	@Inject
	private GccConsumer gccConsumer;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public Object getDataFromGcc(GstinTransactionDTO gstinTransactionDTO) throws AppException {

	/*	DemoRequest demoRequest = new DemoRequest();
		demoRequest.setCtin(gstinTransactionDTO.getTaxpayerGstin().getGstin());
		demoRequest.setReturnType(gstinTransactionDTO.getReturnType());
		demoRequest.setTransactionType(gstinTransactionDTO.getTransactionType());
		
		try {
			if (gstinTransactionDTO != null && gstinTransactionDTO.getTaxpayerGstin().getGstin() != null) {

				String response = gccConsumer.getData(demoRequest);

				JsonNode jsonNode = new JsonNode(response);
				if(jsonNode.getObject().has("data")) {
					return jsonNode.getObject().get("data");
				}
			}

			return null;
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		
		*/
		return null;
	}
	


}
