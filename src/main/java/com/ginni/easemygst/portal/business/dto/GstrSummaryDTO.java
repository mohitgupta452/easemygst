package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.ginni.easemygst.portal.data.part.Gstr3Summary.Gstr3TransactionSummary;
import com.ginni.easemygst.portal.data.part.TOD;

import lombok.Data;

public @Data class GstrSummaryDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static DecimalFormat df2 = new DecimalFormat(".##");

	@JsonProperty("b2b")
	private TransactionDataDTO b2b = new TransactionDataDTO();

	@JsonProperty("b2cl")
	private TransactionDataDTO b2cl = new TransactionDataDTO();

	@JsonProperty("b2cs")
	private TransactionDataDTO b2cs = new TransactionDataDTO();

	@JsonProperty("cdn")
	private TransactionDataDTO cdn = new TransactionDataDTO();

	@JsonProperty("nil")
	private TransactionDataDTO other = new TransactionDataDTO();

	@JsonProperty("exp")
	private TransactionDataDTO exp = new TransactionDataDTO();

	@JsonProperty("ecom")
	private TransactionDataDTO ecom = new TransactionDataDTO();

	@JsonProperty("at")
	private TransactionDataDTO ata = new TransactionDataDTO();

	@JsonProperty("txp")
	private TransactionDataDTO tpd = new TransactionDataDTO();

	@JsonProperty("hsnsum")
	private TransactionDataDTO hsn = new TransactionDataDTO();

	@JsonProperty("impg")
	private TransactionDataDTO impg = new TransactionDataDTO();

	@JsonProperty("imps")
	private TransactionDataDTO imps = new TransactionDataDTO();

	@JsonProperty("isd")
	private TransactionDataDTO isd = new TransactionDataDTO();

	@JsonProperty("tds")
	private TransactionDataDTO tds = new TransactionDataDTO();

	@JsonProperty("tcs")
	private TransactionDataDTO tcs = new TransactionDataDTO();

	@JsonProperty("itc")//previous name itc
	private TransactionDataDTO itc = new TransactionDataDTO();

	@JsonProperty("tld")
	private TransactionDataDTO tld = new TransactionDataDTO();

	@JsonProperty("txpd")
	private TransactionDataDTO txpd = new TransactionDataDTO();

	@JsonProperty("itcr")
	private TransactionDataDTO itcr = new TransactionDataDTO();
	
	@JsonProperty("tod")
	private TOD tod = new TOD();
	
	@JsonProperty("outs")
	private Gstr3TransactionSummary outwardSupp = new Gstr3TransactionSummary();
	
	@JsonProperty("ins")
	private Gstr3TransactionSummary inwardSupp = new Gstr3TransactionSummary();
	
	@JsonProperty("ttl")
	private Gstr3TransactionSummary totTaxLiability = new Gstr3TransactionSummary();
	
	@JsonProperty("tcscr")
	private Gstr3TransactionSummary tcsCredit = new Gstr3TransactionSummary();
	
	@JsonProperty("tdscr")
	private Gstr3TransactionSummary tdsCredit = new Gstr3TransactionSummary();
	
	@JsonProperty("itccr")
	private Gstr3TransactionSummary itcCredit = new Gstr3TransactionSummary();
	
	@JsonProperty("taxpd")
	private Gstr3TransactionSummary taxPaid = new Gstr3TransactionSummary();
	
	@JsonProperty("rfclm")
	private Gstr3TransactionSummary refundClaim = new Gstr3TransactionSummary();

	@JsonGetter("rdata")
	public ReconcileDTO getReconcileData() {

		ReconcileDTO dto = new ReconcileDTO();

		dto.setAcceptCount(b2b.getReconcileDTO().getAcceptCount() + cdn.getReconcileDTO().getAcceptCount());
		dto.setRejectCount(b2b.getReconcileDTO().getRejectCount() + cdn.getReconcileDTO().getRejectCount());
		dto.setPendingCount(b2b.getReconcileDTO().getPendingCount() + cdn.getReconcileDTO().getPendingCount());
		dto.setErrorCount(b2b.getReconcileDTO().getErrorCount() + cdn.getReconcileDTO().getErrorCount());

		dto.setMatchCount(b2b.getReconcileDTO().getMatchCount() + cdn.getReconcileDTO().getMatchCount());
		dto.setMismatchCount(b2b.getReconcileDTO().getMismatchCount() + cdn.getReconcileDTO().getMismatchCount());
		dto.setMissingCount(b2b.getReconcileDTO().getMissingCount() + cdn.getReconcileDTO().getMissingCount());

		dto.setMatchedTaxableValue(
				b2b.getReconcileDTO().getMatchedTaxableValue() + cdn.getReconcileDTO().getMatchedTaxableValue());
		dto.setMatchedTaxAmount(
				b2b.getReconcileDTO().getMatchedTaxAmount() + cdn.getReconcileDTO().getMatchedTaxAmount());
		dto.setMismatchedTaxableValue(
				b2b.getReconcileDTO().getMismatchedTaxableValue() + cdn.getReconcileDTO().getMismatchedTaxableValue());
		dto.setMismatchedTaxAmount(
				b2b.getReconcileDTO().getMismatchedTaxAmount() + cdn.getReconcileDTO().getMismatchedTaxAmount());

		return dto;
	}
	
	@JsonSetter("rdata")
	public void setReconcileData(ReconcileDTO reconcileDTO) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalIgst")
	public Double getTotalIgst() {
		
		double totalIgst = 0.0;
		
		try {
			Method[] methods = this.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1 ) {
					if(method.getReturnType()==TransactionDataDTO.class) {
						TransactionDataDTO dto = (TransactionDataDTO) method.invoke(this);
						totalIgst += dto.getIgstAmount();
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			return Double.valueOf(df2.format(totalIgst));
		}
	}
	
	@JsonSetter("totalIgst")
	public void setTotalIgst(double igst) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalSgst")
	public Double getTotalSgst() {

		double totalSgst = 0.0;
		
		try {
			Method[] methods = this.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1 ) {
					if(method.getReturnType()==TransactionDataDTO.class) {
						TransactionDataDTO dto = (TransactionDataDTO) method.invoke(this);
						totalSgst += dto.getSgstAmount();
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			return Double.valueOf(df2.format(totalSgst));
		}
	}
	
	@JsonSetter("totalSgst")
	public void setTotalSgst(double sgst) {
		
	}

	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalCgst")
	public Double getTotalCgst() {
		
		double totalCgst = 0.0;
		
		try {
			Method[] methods = this.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1 ) {
					if(method.getReturnType()==TransactionDataDTO.class) {
						TransactionDataDTO dto = (TransactionDataDTO) method.invoke(this);
						totalCgst += dto.getCgstAmount();
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			return Double.valueOf(df2.format(totalCgst));
		}
	}
	
	@JsonSetter("totalCgst")
	public void setTotalCgst(double cgst) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalCess")
	public Double getTotalCess() {
		
		double totalCess = 0.0;
		
		try {
			Method[] methods = this.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1 ) {
					if(method.getReturnType()==TransactionDataDTO.class) {
						TransactionDataDTO dto = (TransactionDataDTO) method.invoke(this);
						totalCess += dto.getCessAmount();
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			return Double.valueOf(df2.format(totalCess));
		}
	}
	
	@JsonSetter("totalCess")
	public void setTotalCess(double cess) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonFormat(pattern = "#0.00")
	@JsonGetter("totalTax")
	public Double getTotalTax() {
		
		double totalTax = 0.0;
		
		try {
			Method[] methods = this.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1 ) {
					if(method.getReturnType()==TransactionDataDTO.class) {
						TransactionDataDTO dto = (TransactionDataDTO) method.invoke(this);
						totalTax += dto.getCessAmount();
						totalTax += dto.getIgstAmount();
						totalTax += dto.getCgstAmount();
						totalTax += dto.getSgstAmount();
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			return Double.valueOf(df2.format(totalTax));
		}
	}
	
	@JsonSetter("totalTax")
	public void setTotalTax(double tax) {
		
	}
	
	@SuppressWarnings("finally")
	@JsonGetter("invoiceCount")
	public Integer getInvoiceCount() {
		
		int count = 0;
		
		try {
			Method[] methods = this.getClass().getMethods();
			for (Method method : methods) {
				if (method.getName().indexOf("get") == 0 && method.getName().indexOf("Class")==-1 ) {
					if(method.getReturnType()==TransactionDataDTO.class) {
						TransactionDataDTO dto = (TransactionDataDTO) method.invoke(this);
						ReconcileDTO rdto = dto.getReconcileDTO();
						count += rdto.getInvoiceCount();
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			return count;
		}
	}
	
	@JsonSetter("invoiceCount")
	public void setInvoiceCount(int count) {
		
	}
	
}
