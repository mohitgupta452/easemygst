package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.view.serializer.reco.B2BRecoDetailSerializer;
import com.ginni.easemygst.portal.data.view.serializer.reco.CDNRecoDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;

import lombok.Data;

public @Data class CdnDetailEntityWrapper {

	@JsonSerialize(using=CDNRecoDetailSerializer.class)
	private CDNDetailEntity invoice;
	
	public CdnDetailEntityWrapper(CDNDetailEntity invoice){
		this.invoice=invoice;
	}
}
