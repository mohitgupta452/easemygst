package com.ginni.easemygst.portal.business.dto;


import java.io.Serializable;
import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;


@JsonInclude(Include.NON_NULL)
public@Data class ActionLogDto implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private int id; 
	
	private String action;

	private Timestamp actionTime;

	private String gstin;

	private String message;

	@JsonIgnore
	private int organizationId;

	private String userId;

	private boolean isSeen;
	
	private String category;
	
	private String iconCategory="USER";
	
	@JsonIgnore
	private boolean isActive;
	
	private String notificationTime;
	
	
	private String circularNumber;

	private Date creationDateTime;

	private String date;

	private String documentNumber;

	@JsonProperty("fileName")
	private String url;
	
private String subject;
	
	private String content;
	
	public String getIconCategory(){
		if(StringUtils.isEmpty(this.iconCategory)||"0".equalsIgnoreCase(iconCategory))
			return "INFO";
		else
			return this.iconCategory;
	}
	

	

}