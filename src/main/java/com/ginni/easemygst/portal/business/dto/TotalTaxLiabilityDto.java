package com.ginni.easemygst.portal.business.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public @lombok.Data class TotalTaxLiabilityDto {
	
	@JsonProperty("category")
	private String ty;
	
	@JsonProperty("period")
	private String mon;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("txval")
	private Double txval=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("iamt")
	private Double iamt=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("camt")
	private Double camt=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("samt")
	private Double samt=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cess")
	private Double cess=0.0;
	
	@JsonProperty("source")//not in api but available in view
	private String source;

	
}
