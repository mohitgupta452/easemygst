package com.ginni.easemygst.portal.business.util;

import java.util.Comparator;

import com.ginni.easemygst.portal.business.entity.GstReturnDTO;

public class GstReturnComparator implements Comparator<GstReturnDTO> {

	@Override
    public int compare(GstReturnDTO g1, GstReturnDTO g2) {
        return g1.getDueDate().compareTo(g2.getDueDate());
    }
	
}
