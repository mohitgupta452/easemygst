package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class HsnSubChapterDTO implements Serializable {

	private String code;

	private String cessRate;

	private String description;

	private String displayName;

	private String taxRate;
	
	private List<HsnProductDTO> hsnProducts;
	
}
