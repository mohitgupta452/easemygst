package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class BusinessTypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;

	private byte active;

	private String code;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String description;

	private String name;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	public interface BusinessTypeDTOMixIn extends CommonMixin {
		
	}

}