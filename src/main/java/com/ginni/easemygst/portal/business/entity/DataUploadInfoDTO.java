package com.ginni.easemygst.portal.business.entity;

import java.sql.Timestamp;

import lombok.Data;

public @Data class DataUploadInfoDTO {

	private Integer id;

	private String fileFormat;
	
	private String fileName;

	private String gstin;

	private String monthYear;

	private String returnType;

	private String status;

	private String transaction;

	private String url;
	
	private String encKey;
	
	private String encIv;
	
	private String machineKey;
	
	private String serialNo;
	
	private Byte isPrimary;
		
	private Timestamp creationTime;
	
}
