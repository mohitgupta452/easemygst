package com.ginni.easemygst.portal.business.service;

import java.util.TreeMap;

import javax.ejb.Local;

import com.ginni.easemygst.portal.helper.AppException;

@Local
public interface AccountService {

	TreeMap<String, String> initiatePayment(String request) throws AppException;
	
	boolean checkPaymentStatus(String paymentId) throws AppException;
	
}
