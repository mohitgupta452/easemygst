package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;

import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.persistence.entity.ActionLog;
import com.mashape.unirest.http.JsonNode;

@Local
public interface ApiLoggingService {
	
	boolean logEmgstRequest(ContainerRequestContext  request);
	
	boolean saveAction(GstinTransactionDTO gstinTransactionDto,ActionLog actionLog,MessageDto messageDto);

	boolean saveAction(ActionLog actionLog, MessageDto messageDto);
	
	boolean saveAction(UserAction action, MessageDto messageDto);

	boolean saveAction(UserAction action, MessageDto messageDto, String gstin);

	boolean saveGovtNotification(JsonNode request);

	
	
	
}
