package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class VendorDTO implements Serializable {

	private Integer id;

	private String address;

	private String businessName;

	private String city;

	private String contactPerson;

	private String emailAddress;

	private String gstin;

	private String mobileNumber;

	private String organisation;

	private String panNumber;

	private String pincode;

	private StateDTO stateBean;

	private String type;
	
	private String classification;
	
	private String gstType;
	
	private String status;
	
	private Byte emgst;
	
	private String website;
	
}
