package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class GstrFlowDTO {

	private String transactionId;
	
	private String referenceId;
	
	private boolean isError;
	
	private String errorMsg;
	
}
