package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;



public@Data  class CdnDetailDto implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private int id;

	
	
	
	private String createdBy;

	private int creationIpAddress;

	private Timestamp creationTime;

	private String ctin;

	private String flags;

	
	private String invoiceData;

	private String invoiceNumber;

	private byte isAmmendment;

	private byte isLocked;

	private byte isMarked;

	private byte isSynced;

	private byte isTransit;

	private byte isValid;

	private String source;

	private String sourceId;

	private String synchId;

	private String transitId;

	private String type;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

public interface CdnDetailDtoMixIn extends CommonMixin {
		
	}

	

}