package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.business.dto.GstCalendarDto;
import com.ginni.easemygst.portal.business.service.impl.TaxpayerServiceImpl.ReturnPrefernce;
import com.ginni.easemygst.portal.persistence.entity.FilePrefernce;
import com.ginni.easemygst.portal.utils.Utility;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public @Data class TaxpayerGstinDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public TaxpayerGstinDTO() {
		
	}
	
	public TaxpayerGstinDTO(Integer id, String gstin, String displayName, String monthYear) {
		this.id = id;
		this.gstin = gstin;
		this.displayName = displayName;
		this.monthYear = monthYear;
	}

	private Integer id;

	private Byte active;

	@SuppressWarnings("unused")
	private Byte authenticated;

	private String authToken;

	private String sek;

	private String appKey;

	private String otp;

	private String otpExpiry;

	private String authExpiry;

	private String gstin;

	private String status;
	
	private String monthYear;

	private String type;

	private String displayName;

	private String username;
	
	private String address;
	
	private String pincode;

	private Date startDate;

	private Date endDate;

	private String bankAccountDetails;
	
	private String termsConditions;
	/*
	private Double previousGt;

	private Double currentGt;*/
	
	private String returnPreference;
	
	private boolean changeCycle=false;


	private StateDTO stateBean;

	private List<UserDTO> users;
	
	private String mobileNumber;
	
	private String emailAddress;
	
	private int userCount;
	
	private int vendorCount;
	
	private int vendorVerifiedCount;
	
	private int customerCount;
	
	private int customerVerifiedCount;
	
	private List<FilePrefernce> filePrefernces;
	
/*	private Map<String,List<GstCalendarDto>>dueDateCalendar;
	
	Map<String, Map<String, List<GstCalendarDto>>> dueDateCalendarQuarter;*/
	
	private String userPreference;
	
	public String getAuthExpiry() {
		if (StringUtils.isEmpty(this.authExpiry)) {
			return null;
		} else {
			long value = Long.valueOf(this.authExpiry);
			long curr = Calendar.getInstance().getTimeInMillis();
			if (value - curr > 0) {
				Date dt = new Date(value);
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm a");
				return sdf.format(dt);
			} else {
				return null;
			}
		}
	}

	public void setAuthExpiry(String expiry) {
		this.authExpiry = expiry;
	}
	
	public Byte getAuthenticated() {
		this.appKey = null;
		if (StringUtils.isEmpty(this.authExpiry)) {
			return (byte) 0;
		} else {
			long value = Long.valueOf(this.authExpiry);
			long curr = Calendar.getInstance().getTimeInMillis();
			if (value - curr > 0) {
				this.appKey = Utility.convertTimeToDaysHours(value-curr);
				return (byte) 1;
			} else {
				return (byte) 0;
			}
		}
	}
	
	public void setAuthenticated(Byte data) {
		this.authenticated = data;
	}

	public interface TaxpayerGstinDTOMixIn extends CommonMixin {
		@JsonIgnore
		abstract Object getAuthToken();

		@JsonIgnore
		abstract Object getAppKeyBytes();

		@JsonIgnore
		abstract Object getSek();

		@JsonIgnore
		abstract Object getOtpExpiry();
	}

	public interface TaxpayerGstinDTOUserMixIn extends CommonMixin {
		@JsonIgnore
		abstract Object getUsers();
		abstract Object getUserCount();
		abstract Object getVendorCount();
		abstract Object getVendorVerifiedCount();
		abstract Object getCustomerCount();
		abstract Object getCustomerVerifiedCount();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxpayerGstinDTO other = (TaxpayerGstinDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



}
