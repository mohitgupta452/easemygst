package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public @Data class TransactionDataDTO implements Serializable {

	@JsonFormat(pattern = "#0.00")
	private Double taxableValue=0.0 ;
	
	@JsonFormat(pattern = "#0.00")
	private Double taxAmount =0.0;
	
	@JsonFormat(pattern = "#0.00")
	private Double igstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double cgstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double sgstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double cessAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double amTaxableValue ;
	
	@JsonFormat(pattern = "#0.00")
	private Double amTaxAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double nilAmount ;;
	
	@JsonFormat(pattern = "#0.00")
	private Double exemptedAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double nonGstAmount ;
	
	@JsonFormat(pattern = "#0.00")
	private Double compositeAmount ;
	
	private boolean isError = false;
	@JsonIgnore
	private ReconcileDTO reconcileDTO = new ReconcileDTO();
	
	private List<MismatchDTO> mismatchDTO;
	
	private long toBeSyncCount;
	
	private long notToBeSyncCount;
	
	private long invoices;
	
	
	private long totalDocIssued;
	
	private long totalCanceled;
	
	private long netDocIssued;
	
	private long errorCount;
	
	private MismatchCountDto mismatchCounts=new MismatchCountDto();
	
	private ItcAmountDto itcAmounts=new ItcAmountDto();
	
	private boolean isExclamation=false;
	
}
