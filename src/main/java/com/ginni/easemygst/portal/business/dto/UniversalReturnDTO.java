package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import lombok.Data;

public @Data class UniversalReturnDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String code;
	
	private String description;
	
	private String name;
	
	private Object value;
	
	private String status;
	
}
