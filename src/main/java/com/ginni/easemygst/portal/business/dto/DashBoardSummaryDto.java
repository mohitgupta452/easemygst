package com.ginni.easemygst.portal.business.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class DashBoardSummaryDto {
	
	@JsonProperty("total_taxable_value")
	private Double totalTaxableValue = new Double("0.00");
	
	@JsonProperty("total_tax_amount")
	private Double totalTaxAmount = new Double("0.00");
	
	@JsonProperty("total_itc_claim")
	private Double totalItcClaim = new Double("0.00");
	
	
	@JsonProperty("taxable_value")
	private Double taxableValue = new Double("0.00");
	
	@JsonProperty("tax_amount")
	private Double taxAmount = new Double("0.00");
	
	@JsonProperty("invoices")
	private long invoices;
	
	@JsonProperty("mismatch_roundoff")
	private long mismatchRoundoff;
	
	@JsonProperty("mismatch_invoice_no")
	private long mismatchInvoiceNo;
	
	@JsonProperty("mismatch_invoice_date")
	private long mismatchInvoiceDate;
	
	@JsonProperty("mismatch_major")
	private long mismatchMajor;
	
		
	@JsonProperty("missing_outward")
	private long missing_outward;
	
	@JsonProperty("missing_inward")
	private long missing_inward;
	
	@JsonProperty("error_count")
	private long errorCount;
	
	@JsonProperty("pending_itc_amt")
	private Double pendingItcAmt=0.0;
	
	@JsonProperty("claimed_itc_amt")
	private Double claimedItcAmt=0.0;
	
	
	@JsonProperty("not_claimed_itc_amt")
	private Double notClaimedItcAmt=0.0;
	
	@JsonProperty("transaction_summary")
	private Map<String,Object>transactionSummary;
	
	
	
	

}
