package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

public  @Data class GstinComparison2aVS3bDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String gstin;
	
	private String state;
	
	private boolean isAuthReq=false;
	
	private List<Comparison2avs3bDTO> Comparison2avs3bDTOlist;
	
	
}
