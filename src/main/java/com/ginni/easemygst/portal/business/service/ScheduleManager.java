package com.ginni.easemygst.portal.business.service;

import javax.ejb.Local;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

@Local
public interface ScheduleManager {
	
	public void processData(GetGstr2Resp getGstr2Resp,TransactionType type,TaxpayerGstin gstin,String monthYear,ReturnType returntype,GstinTransactionDTO gstinTransactionDTO );
	public void processUrls();
	public void scheduleRefreshToken();
	public void trackReturns() throws AppException;
}
