package com.ginni.easemygst.portal.business.entity;

import java.io.Serializable;

import lombok.Data;

public @Data class PaymentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private Integer paymentId;

	private String channel;

	private String paymentData;
	
	private String responseData;

	private String paymentMode;

	private String status;
	
	private Double amount;
	
	private String description;

	private String transactionId;
	
}
