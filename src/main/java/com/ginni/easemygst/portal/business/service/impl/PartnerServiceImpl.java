package com.ginni.easemygst.portal.business.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.PartnerDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.PartnerService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.notify.EmailFactory;
import com.ginni.easemygst.portal.notify.SMSFactory;
import com.ginni.easemygst.portal.persistence.entity.Partner;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.utils.Utility;

@Transactional
@Stateless
public class PartnerServiceImpl implements PartnerService {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private FinderService finderService;
	
	@Inject
	private UserBean userBean;

	@Inject
	private RedisService redisService;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Override
	public boolean storePartner(PartnerDTO partnerDTO) throws AppException {

		if (!Objects.isNull(partnerDTO) && !StringUtils.isEmpty(partnerDTO.getPartnerName())
				&& !StringUtils.isEmpty(partnerDTO.getEmailAddress())
				&& !StringUtils.isEmpty(partnerDTO.getContactNo())) {

			if (Objects.isNull(finderService.findPartner(partnerDTO.getEmailAddress(), partnerDTO.getContactNo()))) {

				log.debug("storing user details attributes validation successful");

				String mobileOtp = Utility.randomOtp(6);
				String emailOtp = Utility.randomOtp(6);

				log.debug("storing data to redis server for 10 mins");
				redisService.setPartner(partnerDTO.getEmailAddress() + "_PARTNERUSER", partnerDTO, true, 600);
				redisService.setValue(partnerDTO.getEmailAddress() + "_PARTNETMOBILEOTP", mobileOtp, true, 600);
				redisService.setValue(partnerDTO.getEmailAddress() + "_PARTNETEMAILOTP", emailOtp, true, 600);

				log.debug("sending otp {} to partner mobile number {}", mobileOtp, partnerDTO.getContactNo());
				SMSFactory factory = new SMSFactory();
				Map<String, String> params = new HashMap<>();
				params.put("code", mobileOtp);
				params.put("toNumber", partnerDTO.getContactNo());
				factory.smsSecureCode(params);

				params.put("partnerName", partnerDTO.getPartnerName());
				params.put("code", emailOtp);
				params.put("toEmail", partnerDTO.getEmailAddress());
				EmailFactory factory1 = new EmailFactory();
				factory1.sendPartnerVerificationEmail(params);
				log.info("Sending partner verification email to new partner {}.", params.toString());

				return Boolean.TRUE;
			} else {
				throw new AppException(ExceptionCode._DUPLICATE_PARTNER);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String resendOtp(String email, String mode) throws AppException {
		log.debug("getting data from redis using key {}", email);
		PartnerDTO partnerDTO = redisService.getPartner(email + "_PARTNERUSER");
		if (!StringUtils.isEmpty(mode) && mode.equalsIgnoreCase("mobile")) {
			String mobileOtp = Utility.randomOtp(6);
			if (partnerDTO != null) {
				log.debug("storing resend otp to redis server for 10 mins");
				redisService.setValue(partnerDTO.getEmailAddress() + "_PARTNETMOBILEOTP", mobileOtp, true, 600);

				log.debug("sending otp {} to partner mobile number {}", mobileOtp, partnerDTO.getContactNo());
				SMSFactory factory = new SMSFactory();
				Map<String, String> params = new HashMap<>();
				params.put("code", mobileOtp);
				params.put("toNumber", partnerDTO.getContactNo());
				factory.smsSecureCode(params);
				return partnerDTO.getContactNo();
			}
		} else {
			String emailOtp = Utility.randomOtp(6);
			if (partnerDTO != null) {
				log.debug("storing resend otp to redis server for 10 mins");
				redisService.setValue(partnerDTO.getEmailAddress() + "_PARTNETEMAILOTP", emailOtp, true, 600);

				log.debug("sending otp {} to partner email address {}", emailOtp, partnerDTO.getEmailAddress());
				Map<String, String> params = new HashMap<>();
				params.put("partnerName", partnerDTO.getPartnerName());
				params.put("code", emailOtp);
				params.put("toEmail", partnerDTO.getEmailAddress());
				EmailFactory factory1 = new EmailFactory();
				factory1.sendPartnerVerificationEmail(params);
				log.info("Sending partner verification email to new partner {}.", params.toString());
				return partnerDTO.getEmailAddress();
			}
		}
		
		throw new AppException(ExceptionCode._INSUFFICIENT_PARAMETERS);
	}

	@Override
	public PartnerDTO validateOtp(String mobileOtp, String emailOtp, String email) throws AppException {
		log.debug("getting data from redis using key {}", email);
		PartnerDTO partnerDTO = redisService.getPartner(email + "_PARTNERUSER");
		String mobileOtpOrg = redisService.getValue(email + "_PARTNETMOBILEOTP");
		String emailOtpOrg = redisService.getValue(email + "_PARTNETEMAILOTP");
		if (mobileOtp != null && emailOtp != null && mobileOtpOrg != null && emailOtpOrg != null) {
			if (mobileOtp.equals(mobileOtpOrg) && emailOtp.equals(emailOtpOrg)) {
				log.debug("provided otp validated successfully");
				log.debug("processing partner account");
				partnerDTO = processPartnerAccount(partnerDTO);
				return partnerDTO;
			} else {
				log.debug("provided otp is not correct");
				throw new AppException(ExceptionCode._INVALID_OTP);
			}
		}
		throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
	}

	private PartnerDTO processPartnerAccount(PartnerDTO partnerDTO) throws AppException {

		if (Objects.isNull(finderService.findPartner(partnerDTO.getEmailAddress(), partnerDTO.getContactNo()))) {
			Partner partner = (Partner) EntityHelper.convert(partnerDTO, Partner.class);
			partner.setActive((byte) 1);
			if (!StringUtils.isEmpty(partnerDTO.getClientBase())
					&& !StringUtils.isEmpty(partnerDTO.getClientBase())) {
				partner.setPartnerCode("EMYGST-" + Utility.randomString(8));
				partner.setPartnerCode(partner.getPartnerCode().toUpperCase());
			} 
			partner.setCreationTime(Timestamp.from(java.time.Instant.now()));
			partner.setCreatedBy(partner.getPartnerName());
			partner = crudService.create(partner);

			if (!StringUtils.isEmpty(partnerDTO.getClientBase())
					&& !StringUtils.isEmpty(partnerDTO.getClientBase())) {
				Map<String, String> params = new HashMap<>();
				params.put("name", partner.getPartnerName());
				params.put("toEmail", partner.getEmailAddress());
				params.put("partnerCode", partner.getPartnerCode());
				EmailFactory factory = new EmailFactory();
				factory.sendWelcomePartnerEmail(params);
				log.info("Sending welcome email to new partner {}.", params.toString());
			}

			Map<String, String> params1 = new HashMap<>();
			if (!StringUtils.isEmpty(partner.getClientBase())
					&& !StringUtils.isEmpty(partner.getClientBase())) {
				params1.put("partnerName", partner.getPartnerName());
				params1.put("partnerEmail", partner.getEmailAddress());
				params1.put("partnerMobile", partner.getContactNo());
				params1.put("clientBase", partner.getClientBase());
				params1.put("profession", partner.getProfession());
				params1.put("toEmail", "info@easemygst.com");
				EmailFactory factory1 = new EmailFactory();
				factory1.sendPartnerFormEmail(params1);
				log.info("Sending partner form email to info@easemygst.com {}.", params1.toString());
			} 

			partnerDTO = (PartnerDTO) EntityHelper.convert(partner, PartnerDTO.class);

			return partnerDTO;
		} else {
			throw new AppException(ExceptionCode._DUPLICATE_PARTNER);
		}
	}

	@Override
	public String submitPartnerCode(String code) throws AppException {

		if (!StringUtils.isEmpty(code)) {

			Partner partner = finderService.findPartnerByCode(code);
			if (!Objects.isNull(partner)) {

				User user = em.find(User.class, userBean.getUserDto().getId());
				
				if (Objects.isNull(user.getPartner())) {

					user.setPartner(partner);
					user = crudService.update(user);

					Map<String, String> params = new HashMap<>();
					params.put("partnerName", partner.getPartnerName());
					params.put("toEmail", partner.getEmailAddress());
					params.put("name", userBean.getUserDto().getFirstName() + " " + userBean.getUserDto().getLastName());
					params.put("email", userBean.getUserDto().getUserContacts().get(0).getEmailAddress());
					EmailFactory factory = new EmailFactory();
					factory.sendPartnerSubscribedEmail(params);
					log.info("Sending partner subscribed email to partner {}.", params.toString());

					return partner.getPartnerName();
				} else {
					return null;
				}

			} else {
				throw new AppException(ExceptionCode._INVALID_PARAMETER);
			}

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

}
