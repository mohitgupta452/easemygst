package com.ginni.easemygst.portal.business.entity;

import java.util.Comparator;

public class TaxpayerGstinComparator implements Comparator<TaxpayerGstinDTO> {
	@Override
	public int compare(TaxpayerGstinDTO t1, TaxpayerGstinDTO t2) {
		if (t1.getGstin().equalsIgnoreCase(t2.getGstin())) {
			return t1.getGstin().compareTo(t2.getGstin());
		}
		return t1.getGstin().compareTo(t2.getGstin());
	}
}
