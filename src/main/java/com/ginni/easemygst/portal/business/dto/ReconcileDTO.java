package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class ReconcileDTO implements Serializable {

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("matchTaxableValue")
	private Double matchedTaxableValue = new Double("0.0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("matchTaxAmount")
	private Double matchedTaxAmount = new Double("0.0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("mismatchTaxableValue")
	private Double mismatchedTaxableValue = new Double("0.0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("mismatchTaxAmount")
	private Double mismatchedTaxAmount = new Double("0.0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("itcClaimedAmount")
	private Double itcClaimedAmount = new Double("0.0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("itcNotClaimedAmount")
	private Double itcNotClaimedAmount = new Double("0.0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("itcPendingAmount")
	private Double itcPendingAmount = new Double("0.0");
	
	@JsonProperty("matchCount")
	private Integer matchCount = new Integer("0");
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("matchPercentage")
	private Double matchPercentage = new Double("0.00");
	
	@JsonProperty("mismatchCount")
	private Integer mismatchCount = new Integer("0");
	
	@JsonProperty("missingCount")
	private Integer missingCount = new Integer("0");
	
	@JsonProperty("invoiceCount")
	private Integer invoiceCount = new Integer("0");
	
	@JsonProperty("pendingCount")
	private Integer pendingCount = new Integer("0");
	
	@JsonProperty("rejectCount")
	private Integer rejectCount = new Integer("0");
	
	@JsonProperty("acceptCount")
	private Integer acceptCount = new Integer("0");
	
	@JsonProperty("errorCount")
	private Integer errorCount = new Integer("0");
	
	@JsonProperty("dateCount")
	private Integer dateCount = new Integer("0");
	
	@JsonProperty("roundOffCount")
	private Integer roundOffCount = new Integer("0");
	
	@JsonProperty("majorCount")
	private Integer majorCount = new Integer("0");
	
	@JsonProperty("invoiceNoCount")
	private Integer invoiceNoCount = new Integer("0");
	
}
