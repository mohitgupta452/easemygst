package com.ginni.easemygst.portal.business.dto;

import java.util.List;

import lombok.Data;

public @Data class GstinUserListDTO {

	private String name;
	
	private String email;
	
	private String mobile;
	
	private String verified;
	
	private int count;
	
	private boolean enableAssignTin;
	
	private List<AssignedGstinList> gstinLists;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GstinUserListDTO other = (GstinUserListDTO) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}
	
}
