package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;

public @Data class ZohoSubscriptionEventDTO {

	private String event_id;
	
	private String event_type;
	
	private JsonNode data;
	
	private String event_time_formatted;
	
	private String event_source;
	
	private String event_time;
	
}
