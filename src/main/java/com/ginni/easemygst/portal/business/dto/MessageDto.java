package com.ginni.easemygst.portal.business.dto;


import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

import java.sql.Timestamp;



public@Data class MessageDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String gstin;
	
	private String returnType;

	private String monthYear;
	
	private String userNamePrimary;
	
	private String userNameSecondary;

	private String emailIdPrimary;
	
	private String emialIdSecondary;
	
	private String partnerName;
	
	private String itcAmount;
	
	
	private String invoiceNumber;
	
	private String userDisplayName;
	
	private String customerName;

	private String vendorName;
	
	private String transactionType;
	
	
	private String userDisplayNamePrevious;
	
	private String userDisplayNameNew;
	
	private String noOfInvoices;

	
	

	

	
}