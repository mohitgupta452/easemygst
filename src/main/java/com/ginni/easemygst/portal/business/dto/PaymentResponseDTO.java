package com.ginni.easemygst.portal.business.dto;

import lombok.Data;

public @Data class PaymentResponseDTO {

	private String paymentUrl;
	
}
