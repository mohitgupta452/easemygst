package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ReconcilationRequestDto {

	@JsonProperty("buyerInvoiceId")
	private String buyerInvoiceId;

	@JsonProperty("supplierInvoiceId")
	private String supplierInvoiceId;

	@JsonProperty("flag")
	private String taxPayerAction;
	
	
	
	
		
	}
