package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.persistence.FilterDto;

import lombok.Data;

public @Data class CtinInvoiceDto {

	private String invoiceId;
	
	private double diffPercent;
	
	private double diffAmount;
	
	private FilterDto filter;
	
	
	
	
}
