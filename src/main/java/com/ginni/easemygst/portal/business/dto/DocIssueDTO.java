package com.ginni.easemygst.portal.business.dto;

import java.util.List;

import lombok.Data;

public @Data class DocIssueDTO {
	
	private int inum;
	
	private List<DocIssueItemDTO> items;
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocIssueDTO other = (DocIssueDTO) obj;
		if (inum != other.inum)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + inum;
		return result;
	} 

	

}
