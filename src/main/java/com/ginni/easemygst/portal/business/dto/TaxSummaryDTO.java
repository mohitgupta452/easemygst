package com.ginni.easemygst.portal.business.dto;

import java.io.Serializable;

import lombok.Data;

public @Data class TaxSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TaxAmount outputTaxLiabiltyOtherThanRC;
	
	private TaxAmount taxLiabilityUnderRC;
	
	private TaxAmount inputCreditOnPurchasesIncludeRC;
	
	private TaxAmount  cashLeader;
	
	private TaxAmount  creditLeader;
	
/*	private OffsetLiabiltyDTO offsetliabilty;*/
	
	private boolean isAuth=false;
	
	public TaxSummaryDTO(){
		this.creditLeader=new TaxAmount();
		this.cashLeader=new TaxAmount();
	}

}
