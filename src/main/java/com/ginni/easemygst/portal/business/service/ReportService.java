package com.ginni.easemygst.portal.business.service;

import java.io.IOException;
import java.util.Map;

import javax.ejb.Local;

import com.ginni.easemygst.portal.helper.AppException;
@Local
public interface ReportService {

	Map<String, Object> get2aVS3breport(String panCard,String fyear,boolean refresh) throws AppException, IOException;

/*	Map<String, Object> get1VS3breport(String panCard, String fyear,boolean refresh) throws AppException, IOException;*/

	String generate1vs3breportPdf(String panCard, String fyear, String fileType);

	String generate2avs3breportPdf(String panCard, String fyear, String fileType);

	Map<String, Object> get1VS3breport(String panCard, String fyear, boolean refresh, String refGstin)
			throws AppException, IOException;
	
	
}
