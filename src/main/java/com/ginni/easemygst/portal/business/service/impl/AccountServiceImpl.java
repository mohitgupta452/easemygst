package com.ginni.easemygst.portal.business.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.Properties;
import java.util.TreeMap;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.service.AccountService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.payment.PaytmConstants;
import com.ginni.easemygst.portal.persistence.entity.Payment;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.paytm.pg.merchant.CheckSumServiceHelper;

@Stateless
@Transactional
@EJB(name = "account", beanInterface = AccountService.class)
public class AccountServiceImpl implements AccountService {

	@Inject
	private Logger log;

	@Inject
	private FinderService finderService;

	@Inject
	private CrudService crudService;

	@Inject
	private UserBean userBean;

	@Override
	public TreeMap<String, String> initiatePayment(String request) throws AppException {

		try {

			Properties config = new Properties();
			try {
				config.load(DBUtils.class.getClassLoader().getResourceAsStream("config.properties"));
				log.info("config.properties loaded successfully");
			} catch (IOException e) {
				e.printStackTrace();
			}

			String url = config.getProperty("main.web.url.payment.response");

			if (!(userBean.getUserDto().getUserContacts() != null
					&& !userBean.getUserDto().getUserContacts().isEmpty()))
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

			String paymentId = "EMG" + UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
			String customerId = "EMGST1" + String.format("%09d", userBean.getUserDto().getId());

			TreeMap<String, String> parameters = new TreeMap<>();
			parameters.put("ORDER_ID", paymentId);
			parameters.put("CUST_ID", customerId);
			parameters.put("INDUSTRY_TYPE_ID", PaytmConstants.INDUSTRY_TYPE_ID);
			parameters.put("CHANNEL_ID", PaytmConstants.CHANNEL_ID);
			parameters.put("TXN_AMOUNT", String.valueOf(new JSONObject(request).getDouble("amount")));
			parameters.put("MID", PaytmConstants.MID);
			parameters.put("CHANNEL_ID", PaytmConstants.CHANNEL_ID);
			parameters.put("INDUSTRY_TYPE_ID", PaytmConstants.INDUSTRY_TYPE_ID);
			parameters.put("WEBSITE", PaytmConstants.WEBSITE);
			parameters.put("MOBILE_NO", userBean.getUserDto().getUserContacts().get(0).getMobileNumber());
			parameters.put("EMAIL", userBean.getUserDto().getUserContacts().get(0).getEmailAddress());
			parameters.put("CALLBACK_URL", url);

			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.convertValue(parameters, JsonNode.class);

			String checkSum;

			checkSum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(PaytmConstants.MERCHANT_KEY,
					parameters);

			parameters.put("CHECKSUMHASH", checkSum);
			parameters.put("PAYTM_URL", PaytmConstants.PAYTM_URL);

			Payment payment = new Payment();
			payment.setAmount(new JSONObject(request).getDouble("amount"));
			payment.setPaymentMode("WEB");
			payment.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			payment.setCreationIpAddress(userBean.getIpAddress());
			payment.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
			payment.setDescription("Test Payment");
			payment.setPaymentData(jsonNode.toString());
			payment.setChannel("PayTM");
			payment.setPaymentId(paymentId);
			crudService.create(payment);

			return parameters;
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@Override
	public boolean checkPaymentStatus(String paymentId) throws AppException {

		boolean paymentFlag = false;

		try {
			if (!StringUtils.isEmpty(paymentId)) {

				Payment payment = finderService.getPaymentFromId(paymentId);
				if (!Objects.isNull(payment)) {

					TreeMap<String, String> requestMap = new TreeMap<>();
					requestMap.put("MID", PaytmConstants.MID);
					requestMap.put("ORDERID", paymentId);

					String checkSum;

					checkSum = CheckSumServiceHelper.getCheckSumServiceHelper()
							.genrateCheckSum(PaytmConstants.MERCHANT_KEY, requestMap);

					requestMap.put("CHECKSUMHASH", checkSum);

					StringBuilder output = new StringBuilder();
					HttpURLConnection connection = null;

					try {

						JSONObject obj = new JSONObject(requestMap);
						String urlParameters = obj.toString();
						urlParameters = URLEncoder.encode(urlParameters);

						URL url = new URL("https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus?");
						connection = (HttpURLConnection) url.openConnection();
						connection.setRequestMethod("POST");
						connection.setRequestProperty("contentType", "application/json");
						connection.setUseCaches(false);
						connection.setDoOutput(true);
						// response.setContentType("application/json");

						DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
						wr.writeBytes("JsonData=");
						wr.writeBytes(urlParameters);
						wr.close();

						InputStream is = connection.getInputStream();
						BufferedReader rd = new BufferedReader(new InputStreamReader(is));
						String line = "";
						while ((line = rd.readLine()) != null) {
							output.append(line);
						}
						rd.close();

						JSONObject jsonObject = new JSONObject(output.toString());
						if (jsonObject.has("RESPCODE") && jsonObject.getString("RESPCODE").equals("01")) {
							payment.setTransactionId(jsonObject.getString("TXNID"));
							payment.setStatus("SUCCESS");
							payment.setResponseData(output.toString());
							paymentFlag = Boolean.TRUE;
							crudService.update(payment);
						} else {
							payment.setStatus("FAILURE");
							paymentFlag = Boolean.FALSE;
							payment.setResponseData(output.toString());
							crudService.update(payment);
						}

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						log.info("call to check payment status completed for paymentId {} output {}", paymentId,
								output.toString());
						return paymentFlag;
					}
				}
			}
		} catch (Exception e) {
			return Boolean.FALSE;
		}

		return Boolean.FALSE;
	}

}
