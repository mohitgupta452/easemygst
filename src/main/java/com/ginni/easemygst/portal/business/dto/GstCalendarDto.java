package com.ginni.easemygst.portal.business.dto;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;


@JsonIgnoreProperties(ignoreUnknown=true)
public @Data  class GstCalendarDto implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat viewSdf = new SimpleDateFormat("dd MMMM yyyy");
	
	private static final DateTimeFormatter viewFormatter = DateTimeFormatter.ofPattern("MMMM");
	
	private static final DateTimeFormatter viewFormatterShortMonth = DateTimeFormatter.ofPattern("MMM");

	
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMyyyy");
	
	private static final DateTimeFormatter viewFormatterMonthWithSpace = DateTimeFormatter.ofPattern("MMMM yyyy");
	
	private static final DateTimeFormatter viewFormatterComma = DateTimeFormatter.ofPattern("yy");

	private int id;
	
	private Date startDate;
	
	private Date dueDate;

	private Date quarterduedate;
	
	private String monthId;

	private String monthName;

	private String quarterName;

	private String returnType;

	private String shortName;

	private String yearName;
	
	private String dueDateText="To Be Notified";
	
	private String dueDateTextQuarter="To Be Notified";
	
	private String monthNameWithSpace;
	
	private String monthNameWithComma;
	

	

	
	public String getDueDateTextQuarter() {
		if(this.quarterduedate!=null)
			return viewSdf.format(quarterduedate);
		return dueDateTextQuarter;
	}
	
	public String getDueDateText() {
		if(this.dueDate!=null)
			return viewSdf.format(dueDate);
		return dueDateText;
	}

	public String getMonthName() {
		if(this.monthId!=null){
			YearMonth month=YearMonth.parse(monthId, formatter);
			return viewFormatter.format(month);
		}
		return "Not Available";
	}
	
     public String getShortName(){
	
    	 if(this.getMonthName()!=null){
    	return  this.getMonthName().substring(0,3);
    	 }
    	return "Not Available";
	}
	
	
	public String getMonthNameWithSpace() {
		if(this.monthId!=null){
			YearMonth month=YearMonth.parse(monthId, formatter);
			
			return viewFormatterMonthWithSpace.format(month);
		}
		return "Not Available";
	}
	
	public String getMonthNameWithComma() {
		if(this.monthId!=null){
			YearMonth month=YearMonth.parse(monthId, formatter);
			
			return viewFormatterShortMonth.format(month)+"' "+viewFormatterComma.format(month);
		}
		return "Not Available";
	}

}