package com.ginni.easemygst.portal.business.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.business.service.GstnService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ScheduleManager;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.GstMetadata.Returns;
import com.ginni.easemygst.portal.gst.request.GetGstr2Req;
import com.ginni.easemygst.portal.gst.response.FileUrlResp;
import com.ginni.easemygst.portal.gst.response.Get2AFileResp;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.notify.EmailFactory;
import com.ginni.easemygst.portal.persistence.entity.EncodedData;
import com.ginni.easemygst.portal.persistence.entity.Taxpayer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.TrackReturnsInfo;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.GstnServiceExecutor;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.Utility;

@Transactional
@Stateless
public class ScheduleManagerImpl implements ScheduleManager {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	@Inject
	private FinderService finderService;

	@Inject
	private UserBean userBean;

	@Inject
	private GstnService gstnService;

	@Inject
	private FunctionalService functionService;

	@Inject
	private TaxpayerService taxPayerService;

	@Inject
	private RedisService redisService;
	
	@Inject
	private ReturnsService returnsService;

	// @Resource
	// private TimerService timerService;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Inject
	private FunctionalService functionalService;

	private String server_ipa;

	@PostConstruct
	private void init() {
		server_ipa = redisService.getValue(ApplicationMetadata.SERVER_IP_ADDRESS);
	}

	@Transactional
	// @Schedule(hour = "*", minute = "0/10", persistent = false)
	public void sendEmailToVendor() {

		List<Vendor> vendors = finderService.findVendorEmailFlag();

		if (!Objects.isNull(vendors)) {

			for (Vendor vendor : vendors) {

				if (!StringUtils.isEmpty(vendor.getEmailAddress())) {

					Taxpayer taxpayer = vendor.getTaxpayer();
					String requestId = taxpayer.getRequestId();
					if (StringUtils.isEmpty(requestId)) {
						requestId = UUID.randomUUID().toString();
						taxpayer.setRequestId(requestId);
						crudService.update(taxpayer);
					}

					String vendorRequestId = vendor.getRequestId();
					if (StringUtils.isEmpty(vendorRequestId)) {
						vendorRequestId = UUID.randomUUID().toString();
						vendor.setRequestId(vendorRequestId);
					}

					Properties config = new Properties();
					try {
						config.load(
								ScheduleManagerImpl.class.getClassLoader().getResourceAsStream("config.properties"));
						log.info("config.properties loaded successfully");
					} catch (IOException e) {
						e.printStackTrace();
					}

					StringBuilder sb = new StringBuilder(config.getProperty("main.web.url.signup"));
					sb.append("?requestId=");

					EmailFactory factory = new EmailFactory();
					Map<String, String> activateParams = new HashMap<>();
					activateParams.put("contactPerson", vendor.getContactPerson());
					activateParams.put("toEmail", vendor.getEmailAddress());
					activateParams.put("taxpayerName", taxpayer.getLegalName());
					activateParams.put("signupLink", sb.append(vendorRequestId).toString());
					factory.sendTaxpayerVendorEmail(activateParams);
					log.info("Sending taxpayer email to vendor {}.", activateParams.toString());
					vendor.setRequestId(vendorRequestId);
					vendor.setEmail((byte) 1);
					vendor.setEmailAt(Timestamp.from(Instant.now()));
					vendor.setUpdatedBy("SYSTEM");
					vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(vendor);
				}

			}

		}

	}

	// @PostConstruct
	// public void init(int minutes) {
	// TimerConfig timerConfig = new TimerConfig();
	// timerConfig.setInfo("SingleActionTimerDemo_Info");
	// timerService.createSingleActionTimer(5000, timerConfig); // after 5
	// // seconds
	// }

	// public void setTimer(int minutes){
	// TimerConfig timerConfig = new TimerConfig();
	// timerConfig.setInfo("SingleActionTimerDemo_Info");
	// timerService.createSingleActionTimer(5000, timerConfig);
	// }

	// code to get urls and process them
	 @Lock(LockType.READ)
	 @Schedule(hour="*",minute="*/3",persistent=true)
	public void processUrls() {
		log.info("entering in process urls schedular{}");
		// query to find whose creation time and updation time both 30 minutes

		Query q = em.createNativeQuery(
				"SELECT r.id,r.gstin,r.returnType,r.monthYear,r.transaction,r.token,r.creationTime,r.updationTime,r.urlcount,r.validity,r.status from token_response r where r.status='PENDING' and r.creationTime < DATE_SUB(NOW(),INTERVAL 30 MINUTE) and r.updationTime < DATE_SUB(NOW(),INTERVAL 30 MINUTE) and r.urlcount=0",
				TokenResponse.class);
		List<TokenResponse> listResponse = q.getResultList();

		try {
			if (!listResponse.isEmpty()) {
				log.info("getting results from tokenresponse {}" + listResponse.size());
				for (TokenResponse tokenResp : listResponse) {

					// data set into DTOs
					YearMonth yearmonth=YearMonth.parse(tokenResp.getMonthYear(), DateTimeFormatter.ofPattern("MMyyyy"));
					String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
					GstinTransactionDTO gstnDto = new GstinTransactionDTO();
					gstnDto.setTaxpayerGstin(taxPayerService.getTaxpayerGstin(tokenResp.getTaxpayerGstin().getGstin(),fyear));
					gstnDto.setReturnType(tokenResp.getReturnType());
					gstnDto.setMonthYear(tokenResp.getMonthYear());
					gstnDto.setTransactionType(tokenResp.getTransaction());
					// this.getUrlResponse(gstnDto);
					// creating request to get Urls
					GetGstr2Req getGstr2Req = new GetGstr2Req();
					getGstr2Req.setAction(Returns.FILEDET.toString());
					getGstr2Req.setToken(tokenResp.getToken());
					getGstr2Req.setGstin(gstnDto.getTaxpayerGstin().getGstin());
					getGstr2Req.setReturnType(ReturnType.valueOf(tokenResp.getReturnType()));
					getGstr2Req.setMonthYear(tokenResp.getMonthYear());

					String response = gstnService.getGstr2Data(getGstr2Req);
					log.info("url responsse received {} " + response);
					GetGstr2Req getGstrReq = new GetGstr2Req();
					Get2AFileResp urlResp = JsonMapper.objectMapper.readValue(response, Get2AFileResp.class);
					InputStream endpointResp = null;

					getGstrReq.setAction(Returns.FILE_GET_DOWNLOAD.toString());
					getGstrReq.setMonthYear(tokenResp.getMonthYear());

					// deleting existing data

					Map<String, Object> queryParam = new HashMap<>();
					queryParam.put("referenceId", tokenResp.getId());
					List<EncodedData> encodedDataList = crudService.findWithNamedQuery("EncodedData.findByReferenceId",
							queryParam);
					if (!encodedDataList.isEmpty()) {
						log.info("deleting existing encoded data {}" + encodedDataList.size());
						for (EncodedData encode : encodedDataList) {
							crudService.delete(EncodedData.class, encode.getId());
						}
					}
					try {
						if (urlResp.getUrls() != null) {
							if (!urlResp.getUrls().isEmpty()) {
								tokenResp.setUrlcount(urlResp.getUrls().size());
								crudService.update(tokenResp);
								for (FileUrlResp url : urlResp.getUrls()) {
									try {
										getGstrReq.setGstin(gstnDto.getTaxpayerGstin().getGstin());
										getGstrReq.setEndpoint(URLDecoder.decode(url.getUl(), "UTF-8"));
										endpointResp = gstnService.getGstr2AUrlData(getGstrReq);
										if (endpointResp != null) {
											String encodedString = Utility.unzipData(endpointResp, urlResp.getEk());
											EncodedData encodedData = new EncodedData();
											encodedData.setEncodedData(encodedString);
											TokenResponse tokenResponse = new TokenResponse();
											tokenResponse.setId(tokenResp.getId());
											encodedData.setReferenceId(tokenResponse);
											encodedData.setInvoiceCount(Integer.parseInt(url.getIc()));
											encodedData.setStatus("PENDING");
											crudService.create(encodedData);
										} else {
											log.error("exception occured inputstream is null {}");
										}
									} catch (Exception e) {
										log.error("exception while inserting into encoded_data", e);
									}
								}
							} else
								log.info("No urls found to process {}");
						} else
							log.info("urls not recevied from gstn {}");
					} catch (Exception e) {
						log.error("exception occured after getting url response{} " + e);
					}
				}
			} else
				log.info("No Request found to be processed");
		} catch (Exception e) {
			log.error("exception occured {}" + e);
		}
	}

	// @Lock(LockType.READ)

	 @Lock(LockType.READ)
	@SuppressWarnings("unchecked")
	 @Schedule(hour="*",minute="*/6",persistent=true)
	public void processEncodedData() {
		log.info("processEncodedData method Starts {}");
		List<EncodedData> encodedDataList = crudService.findWithNamedQuery("EncodedData.findByPending");

		try {
			if (!encodedDataList.isEmpty()) {
				Executors.newSingleThreadExecutor().execute(new Runnable() {
					@Override
					public void run() {
						int result = 0;
						String processId = null;
						try {
							processId = UUID.randomUUID().toString();
							// List<EncodedData> dataInProcess =
							// crudService.findWithNamedQuery("EncodedData.findByINPROCESS");
							// if(!dataInProcess.isEmpty()){
							// log.debug(" block marking status to pending {} "+dataInProcess.size());
							// for(EncodedData encodedData:dataInProcess){
							// encodedData.setStatus("PENDING");
							// crudService.update(encodedData);
							// }
							// }
							log.info("marking status to INPROCESS  for {} " + encodedDataList.size());
							for (EncodedData encode : encodedDataList) {
								encode.setStatus("INPROCESS");
								encode.setProcessId(processId);
								crudService.update(encode);
							}
							int trycount = 0;
							CopyOnWriteArrayList<EncodedData> listcopyencode = new CopyOnWriteArrayList<>(
									encodedDataList);
							Set<Integer> referenceId = new HashSet<Integer>();
							for (int i = 0; i <= listcopyencode.size(); i++) {
								EncodedData encodeData = new EncodedData();
								if (!listcopyencode.isEmpty()) {
									encodeData = listcopyencode.get(0);
									referenceId.add(encodeData.getReferenceId().getId());
									GetGstr2Resp getResp = JsonMapper.objectMapper.readValue(
											Utility.decodedString(encodeData.getEncodedData()), GetGstr2Resp.class);
									GstinTransactionDTO gstnDTO = new GstinTransactionDTO();
									gstnDTO.setTransactionType(encodeData.getReferenceId().getTransaction());
									gstnDTO.setMonthYear(encodeData.getReferenceId().getMonthYear());
									gstnDTO.setReturnType(encodeData.getReferenceId().getReturnType());
									getResp.setToken("dummy");
									getResp.setIsDelete(encodeData.getIsDelete());
									try {
										result = new GstnServiceExecutor().insertUpdateGstnData(getResp,
												TransactionType.valueOf(gstnDTO.getTransactionType()),
												encodeData.getReferenceId().getTaxpayerGstin(), gstnDTO.getMonthYear(),
												ReturnType.valueOf(gstnDTO.getReturnType()));
									} catch (Exception e) {
										log.error("exception while inserting data changing status to pending {}  " + e);
										if (trycount < 3) {
											encodeData.setStatus("PENDING");
											crudService.update(encodeData);
										}
										trycount++;
									}
									Map<String, Object> queryParam = new HashMap<>();
									queryParam.put("reference", (TokenResponse) EntityHelper
											.convert(encodeData.getReferenceId(), TokenResponse.class));

									if (result > 0) {
										log.debug("result count {}" + result);
										encodeData.setStatus("COMPLETED");
										crudService.update(encodeData);
										result = 0;
									}
									this.updateFlag(queryParam);
									Map<String, Object> querParam = new HashMap<>();
									querParam.put("processId", processId);
									List<EncodedData> dataIP = crudService.findWithNamedQuery("EncodedData.findByIP",
											querParam);
									listcopyencode = new CopyOnWriteArrayList<>(dataIP);
								} else
									log.debug("No data present to process {} ");
							}

							String query = "SELECT distinct ed.*  from encoded_data ed,token_response t where t.id=ed.referenceId and t.urlcount in (select count(*) from encoded_data where status='COMPLETED') ";
							List<EncodedData> listEncode = em.createNativeQuery(query, EncodedData.class)
									.getResultList();
							int count = 0;
							for (EncodedData encode : listEncode) {
								Map<String, Object> queryParam = new HashMap<>();
								queryParam.put("id", encode.getReferenceId().getId());
								List<TokenResponse> listtoken = crudService.findWithNamedQuery("TokenResponse.findById",
										queryParam);
								TokenResponse token = listtoken.get(0);
								token.setStatus("COMPLETED");
								crudService.update(token);
							}
							// if (!referenceId.isEmpty()) {
							// for (Integer id : referenceId) {
							// Map<String, Object> queryParam = new HashMap<>();
							// queryParam.put("id", id);
							// List<TokenResponse> listtoken = crudService
							// .findWithNamedQuery("TokenResponse.findById", queryParam);
							// TokenResponse token = listtoken.get(0);
							// token.setStatus("COMPLETED");
							// crudService.update(token);
							// }
							// }
							log.info("task completed for count {} " + count);
						} catch (Exception e) {
							log.error("exception while inserting data {} " + e);
						}

					}

					private void updateFlag(Map<String, Object> queryParam) {
						List<EncodedData> listencode = crudService.findWithNamedQuery("EncodedData.findByReference",
								queryParam);
						log.debug("existing data found for referenceId {} " + listencode.size());
						for (EncodedData encode : listencode) {
							encode.setIsDelete(false);
							crudService.update(encode);
						}
					}
				});
			} else {
				log.error("No encoded data found {}");
			}
		} catch (Exception e) {
			log.error("exception while processingdata {}" + e);
		}
	}

	@Timeout
	@AccessTimeout(value = 20, unit = TimeUnit.MINUTES)
	public void processData(GetGstr2Resp getGstr2Resp, TransactionType type, TaxpayerGstin gstin, String monthYear,
			ReturnType returntype, GstinTransactionDTO gstinTransactionDTO) {

		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				try {
					int count = new GstnServiceExecutor().insertUpdateGstnData(getGstr2Resp, type, gstin, monthYear,
							returntype);
					gstinTransactionDTO.setTotalSync(count);
					String successMsg = count + " records fetch from gstn successfully";
					if (count != 0)
						functionalService.saveDataToTokenResponse(gstinTransactionDTO, type.toString());
				} catch (Exception e) {
					log.error("exception occured {} " + e);
				}
			}
		});
	}

	// public void updateFlag(Map<String,Object> queryParam){
	//
	// }

	// public Map<Integer,String> unZipFile(String outputFolder,InputStream
	// is,String iv,String encKey){

	// Map<Integer,String> responseMap=null;
	// try{
	// GZIPInputStream zis = new GZIPInputStream(is);
	// //get the zipped file list entry
	// Integer i=1;
	// responseMap=new HashMap<>();
	//// zis.re
	// while(ze!=null){
	//
	// String fileName = ze.getName();
	// File newFile = new File(outputFolder + File.separator + fileName);
	// responseMap.put(i, newFile.getName());
	// log.info("file unzip "+ newFile.getAbsoluteFile());
	//
	// //create all non exists folders
	// //else you will hit FileNotFoundException for compressed folder
	// new File(newFile.getParent()).mkdirs();
	//
	// FileOutputStream fos = new FileOutputStream(newFile);
	//
	// int len;
	// while ((len = zis.read(buffer)) > 0) {
	// fos.write(buffer, 0, len);
	// }
	//
	// fos.close();
	// ze = zis.getNextEntry();
	// i++;
	// }
	//
	// zis.closeEntry();
	// zis.close();
	// }catch(IOException ex){
	// ex.printStackTrace();
	// }
	// log.info(CLASS_NAME+" - completed unzipping of files with reponse
	// "+responseMap);
	// return responseMap;
	// }

	// @Schedule(hour="*",minute="*/40",second="*")
	// public void getDataFiles(){
	// System.out.println("____________________________________________");
	// if(!StringUtils.isEmpty(redisService.getToken("tokengetFile"))){
	// GetGstr2Req getGstr2Req=new GetGstr2Req();
	// getGstr2Req.setAction(Returns.FILEDET.toString());
	// getGstr2Req.setToken("f7d8e7b120e4462bb35b8c4ce964ee39");
	// GstinTransactionDTO gstnDto=new GstinTransactionDTO();
	// try {
	// gstnDto.setTaxpayerGstin(taxPayerService.getTaxpayerGstin(redisService.getToken("gstin")));
	// } catch (AppException e1) {
	// log.error("error {} "+e1);
	// }
	// getGstr2Req.setGstin(gstnDto.getTaxpayerGstin().getGstin());
	// getGstr2Req.setReturnType(ReturnType.valueOf(redisService.getToken("returnType")));
	// getGstr2Req.setMonthYear(redisService.getToken("monthYear"));
	// try {
	// String response= gstnService.getGstr2Data(getGstr2Req);
	// redisService.setKeyValue("urls", response);
	// } catch (AppException e) {
	// e.printStackTrace();
	// }
	// }
	// }

	@Transactional
	// @Schedule(hour = "4", persistent = false)
	public void sendFollowEmailToVendor() {

		List<Vendor> vendors = finderService.findVendorFollowEmailFlag();

		if (!Objects.isNull(vendors)) {

			for (Vendor vendor : vendors) {

				if (!StringUtils.isEmpty(vendor.getEmailAddress())) {

					Taxpayer taxpayer = vendor.getTaxpayer();
					String requestId = taxpayer.getRequestId();
					if (StringUtils.isEmpty(requestId)) {
						requestId = UUID.randomUUID().toString();
						taxpayer.setRequestId(requestId);
						taxpayer.setUpdatedBy("SYSTEM");
						taxpayer.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
						crudService.update(taxpayer);
					}

					String vendorRequestId = vendor.getRequestId();
					if (StringUtils.isEmpty(vendorRequestId)) {
						vendorRequestId = UUID.randomUUID().toString();
						vendor.setRequestId(vendorRequestId);
					}

					Properties config = new Properties();
					try {
						config.load(
								ScheduleManagerImpl.class.getClassLoader().getResourceAsStream("config.properties"));
						log.info("config.properties loaded successfully");
					} catch (IOException e) {
						e.printStackTrace();
					}

					StringBuilder sb = new StringBuilder(config.getProperty("main.web.url.signup"));
					sb.append("?requestId=");

					EmailFactory factory = new EmailFactory();
					Map<String, String> activateParams = new HashMap<>();
					activateParams.put("contactPerson", vendor.getContactPerson());
					activateParams.put("toEmail", vendor.getEmailAddress());
					activateParams.put("taxpayerName", taxpayer.getLegalName());
					activateParams.put("signupLink", sb.append(vendorRequestId).toString());
					factory.sendTaxpayerVendorFollowEmail(activateParams);
					log.info("Sending taxpayer email to vendor {}.", activateParams.toString());
					vendor.setRequestId(vendorRequestId);
					vendor.setFollowEmail((byte) 1);
					vendor.setFollowEmailAt(Timestamp.from(Instant.now()));
					vendor.setUpdatedBy("SYSTEM");
					vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					vendor.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
					crudService.update(vendor);
				}

			}

		}

	}

	// @SuppressWarnings("unchecked")
	//// //@Schedule(hour="*",minute="*/5",persistent=true)
	//// public void readIPData() throws AppException{
	////
	////// Map<String,Object> queryParam=new HashMap<>();
	////// queryParam.put("gstn",
	// (TaxpayerGstin)EntityHelper.convert(taxPayerService.getTaxpayerGstin("27GSPMH0032G1Z0"),TaxpayerGstin.class));
	//// List<ReturnStatus>
	// returnStatusa=crudService.findWithNamedQuery("ReturnStatus.findALLIP");
	////
	//// log.info("Scjhedular statrted at "+LocalDateTime.now());
	////
	//// try{
	//// //for Gstr1 Data
	//// returnStatusa.stream().filter(data->data.getReturnType().equalsIgnoreCase("GSTR1"))
	//// .collect(Collectors.toList()).forEach(gstr1Data->{
	//// try {
	//// this.checkReturnStatusGstr1(gstr1Data);
	//// } catch (Exception e) {
	//// log.error("error {}"+e);
	//// }
	//// });
	////
	//
	//
	// List<ReturnStatus>
	// gstr2Return=returnStatusa.stream().filter(data->data.getReturnType().equalsIgnoreCase("GSTR2")).
	// collect(Collectors.toList());
	//
	// System.out.println("gstr2Return data {}"+gstr2Return);
	//
	// //for Gstr2 Data
	//// returnStatusa.stream().filter(data->data.getReturnType().equalsIgnoreCase("GSTR2")).
	//// collect(Collectors.toList()).forEach(gstr2Data->{
	//// log.info("in gstr2 Data{}"+gstr2Data.toString());
	//// GstinTransactionDTO gstndto=new GstinTransactionDTO();
	//// gstndto.setMonthYear(gstr2Data.getMonthYear());
	//// gstndto.setReturnType(gstr2Data.getReturnType());
	//// try{
	//// gstndto.setTaxpayerGstin(taxPayerService.getTaxpayerGstin(gstr2Data.getTaxpayerGstin().getGstin()));
	//// functionService.checkGstr2ReturnStatus(gstndto);
	//// }catch(Exception e){
	//// log.error("error in gstr2{}"+e);
	//// }
	//// });
	//
	// } catch(Exception e){
	// throw new AppException(ExceptionCode._ERROR);
	// }
	//
	// }

	// public void checkReturnStatusGstr1(ReturnStatus returnStatus) throws
	// AppException{
	// GstinTransactionDTO gstndto=new GstinTransactionDTO();
	// GetCheckStatusResp checkStatusResp=null;
	// GetCheckStatusGstr2Resp checkStatusRespGstr2=null;
	// gstndto.setReturnType(returnStatus.getReturnType());
	// gstndto.setMonthYear(returnStatus.getMonthYear());
	// try {
	// gstndto.setTaxpayerGstin(taxPayerService.getTaxpayerGstin(returnStatus.getTaxpayerGstin().getGstin()));
	// } catch (AppException e1) {
	// // TODO Auto-generated catch block
	// log.error("exception while setting taxpayerGstin {} "+e1);
	// }
	// String checkStatusResponse=null;
	// try{
	// GetReturnStatusReq getReturnStatusReq = new GetReturnStatusReq();
	// getReturnStatusReq.setAction(Returns.RETSTATUS.toString());
	// getReturnStatusReq.setGstin(gstndto.getTaxpayerGstin().getGstin());
	// getReturnStatusReq.setMonthYear(returnStatus.getMonthYear());
	// getReturnStatusReq.setRef_id(returnStatus.getReferenceId());
	//
	// if(returnStatus.getReturnType().equalsIgnoreCase("GSTR1")){
	// checkStatusResponse = gstnService.getGst1ReturnStatus(getReturnStatusReq);
	// if (!StringUtils.isEmpty(checkStatusResponse))
	// checkStatusResp = JsonMapper.objectMapper.readValue(checkStatusResponse,
	// GetCheckStatusResp.class);
	// if (checkStatusResp != null) {
	// functionService.updateErrorReports(gstndto, checkStatusResp,
	// returnStatus.getTransitId());
	// returnStatus.setFlag(checkStatusResp.getStatus());
	// returnStatus.setResponse(checkStatusResponse);
	// }
	// }else if(returnStatus.getReturnType().equalsIgnoreCase("GSTR2")){
	// checkStatusResponse = gstnService.getGstr2ReturnStatus(getReturnStatusReq);
	// if (!StringUtils.isEmpty(checkStatusResponse))
	// checkStatusRespGstr2 = JsonMapper.objectMapper.readValue(checkStatusResponse,
	// GetCheckStatusGstr2Resp.class);
	//
	// if (checkStatusRespGstr2 != null) {
	// functionService.updateErrorReportsGstr2(gstndto, checkStatusRespGstr2,
	// returnStatus.getTransitId());
	// returnStatus.setFlag(checkStatusRespGstr2.getStatus());
	// returnStatus.setResponse(checkStatusResponse);
	// }
	// }
	// returnStatus.setStatus("PROCESSED");
	// returnStatus.setUpdatedBy(
	// userBean.getUserDto().getFirstName() + "_" +
	// userBean.getUserDto().getLastName());
	// returnStatus.setUpdationIpAddress(userBean.getIpAddress());
	// returnStatus.setUpdationTime(new Timestamp(Instant.now().toEpochMilli()));
	// crudService.update(returnStatus);
	//
	// }catch(Exception e){
	// log.error("exception while {}"+e);
	// throw new AppException(ExceptionCode._ERROR);
	// }
	// }

	// @Override
	// public void run() {
	// RedisService redisService=new RedisService();
	// redisService.init();
	// if(!StringUtils.isEmpty(redisService.getToken("tokengetFile"))){
	// GetGstr2Req getGstr2Req=new GetGstr2Req();
	// getGstr2Req.setAction(Returns.B2B.toString());
	// getGstr2Req.setToken(redisService.getToken("tokengetFile"));
	// getGstr2Req.setGstin(redisService.getToken("gstin"));
	// getGstr2Req.setReturnType(ReturnType.valueOf(redisService.getToken("returnType")));
	// getGstr2Req.setMonthYear(redisService.getToken("monthYear"));
	// //getGstr2Req.setToken(redisService.get);
	// try {
	// gstnService.getGstr2Data(getGstr2Req);
	// } catch (AppException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	//
	// }

	@Transactional
	@Schedule(minute = "*/20",hour="*",persistent = true)
	public void scheduleRefreshToken() {

		String getTaxpayerGstinsToBeExpiry = "select "
				+ "new com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin(id,gstin,authToken,appKey,sek,username)"
				+ " from TaxpayerGstin t where t.authenticated=1 and"
				+ " ABS(TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP(),t.authExpiryTime)) < 35 "
				+ "and TIMESTAMPDIFF(MINUTE,t.maxAuthExpiryTime,CURRENT_TIMESTAMP()) < 10 and authServer=:server_ipa";

		List<TaxpayerGstin> taxpayerGstins = em.createQuery(getTaxpayerGstinsToBeExpiry, TaxpayerGstin.class)
				.setParameter("server_ipa", server_ipa).getResultList();
		
		log.info("refresh token scheduler starts {}",ToStringBuilder.reflectionToString(taxpayerGstins));

		ExecutorService executor = Executors.newFixedThreadPool(20);

		for (final TaxpayerGstin taxpayerGstin : taxpayerGstins) {
			executor.submit(new Runnable() {
				@Override
				public void run() {
					try {
						taxPayerService.requestGstnRefreshToken(taxpayerGstin);
					} catch (AppException e) {

						log.error("exception while update refresh token for gstin {}", taxpayerGstin.getGstin(), e);
					}
				}
			});
		}
	}

	// @Override
	// public void run() {
	// RedisService redisService=new RedisService();
	// redisService.init();
	// if(!StringUtils.isEmpty(redisService.getToken("tokengetFile"))){
	// GetGstr2Req getGstr2Req=new GetGstr2Req();
	// getGstr2Req.setAction(Returns.B2B.toString());
	// getGstr2Req.setToken(redisService.getToken("tokengetFile"));
	// getGstr2Req.setGstin(redisService.getToken("gstin"));
	// getGstr2Req.setReturnType(ReturnType.valueOf(redisService.getToken("returnType")));
	// getGstr2Req.setMonthYear(redisService.getToken("monthYear"));
	// //getGstr2Req.setToken(redisService.get);
	// try {
	// gstnService.getGstr2Data(getGstr2Req);
	// } catch (AppException e) {
	// // TODO Auto-generated catch blockRz
	// e.printStackTrace();
	// }
	// }
	//
	// }

	// mid nyt scheduler
	@Transactional
	@Schedule(hour = "*/24", persistent = true)
	public void trackReturns() throws AppException {

		log.info("track return calculate scheduler starts");
		String truencateTrackReturns = "TRUNCATE TABLE `track_returns_info`";

		String qry = "insert into track_returns_info(taxpayerGstinId,monthYear,returnType,isAuthenticated) "
				+ "select gstn.id,gst_cal.monthId,gst_cal.returnType,gstn.authenticated from taxpayer_gstin gstn "
				+ "join (select monthId,returnType from gst_calendar) as gst_cal where gstn.id not in "
				+ "(select action.gstin from gstin_action action where gst_cal.monthId=action.particular and gst_cal.returnType=action.returnType)"
				+ "and gst_cal.returnType in ('gstr1','gstr3b')";

		em.createNativeQuery(truencateTrackReturns).executeUpdate();
		int trackableRecordCount = em.createNativeQuery(qry).executeUpdate();

		log.info("total record insert in view track table {}", trackableRecordCount);
           returnsService.viewTrackReturnsAsync(null);
		log.info("track return calculate scheduler end");

	}

}

// schedule to save gstr1 data for every gstin

// schedule to get complete data of gstr1 for every gstin

// schedule to save gstr2 data for every gstin

// schedule to get complete data of gstr2 for every gstin

// schedule to save gstr1a data for every gstin

// schedule to get complete data of gstr1a for every gstin
