package com.ginni.easemygst.portal.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
public @Data class SubscriptionChangedDTO {

	private String created_at;
	
	private String cancelled_at;
	
	private String next_billing_at;
	
	private String last_billing_at;
	
	private String subscription_id;
	
	private String expires_at;
	
	private String interval_unit;
	
	private JsonNode plan;
	
	private JsonNode customer;
	
	private JsonNode custom_fields;
	
	private String current_term_ends_at;
	
	private String salesperson_name;
	
	private String salesperson_id;
	
	private boolean auto_collect;
	
	private String status;
	
}
