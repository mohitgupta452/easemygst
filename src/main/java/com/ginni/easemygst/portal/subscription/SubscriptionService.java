package com.ginni.easemygst.portal.subscription;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.json.JSONArray;

import com.ginni.easemygst.portal.business.dto.ZohoFormsEventData;
import com.ginni.easemygst.portal.business.dto.ZohoSubscriptionEventDTO;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.Plan;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.subscription.dto.CustomerDTO;
import com.ginni.easemygst.portal.subscription.dto.SubscriptionPlanDTO;

@Local
public interface SubscriptionService {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	SimpleDateFormat formsDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

	String ZOHO_AUTH_TOKEN = "zoho_oauth_token";

	String ORGANISATION_ID = "649510026";

	String EMAIL_ADDRESS = "sumeet@easemygst.com";

	String PASSWORD = "Cloud@12";

	String SCOPE = "ZohoSubscriptions/subscriptionsapi";

	String AUTH_URL = "https://accounts.zoho.com/apiauthtoken/nb/create";

	String ALL_PLAN_URL = "https://subscriptions.zoho.com/api/v1/plans";

	String ADD_CUSTOMER_URL = "https://subscriptions.zoho.com/api/v1/customers";

	String ADD_SUBSCRIPTION_URL = "https://subscriptions.zoho.com/api/v1/hostedpages/newsubscription";
	
	String UPDATE_CARD_URL = "https://subscriptions.zoho.com/api/v1/hostedpages/updatecard";

	String UPDATE_SUBSCRIPTION_URL = "https://subscriptions.zoho.com/api/v1/subscriptions";

	String CANCEL_SUBSCRIPTION_URL = "https://subscriptions.zoho.com/api/v1/subscriptions";

	String LIST_INVOICES_URL = "https://subscriptions.zoho.com/api/v1/invoices";
	
	String EMAIL_INVOICE_URL = "https://subscriptions.zoho.com/api/v1/invoices/%s/email";

	String RETREIVE_CUSTOMER_URL = "https://subscriptions.zoho.com/api/v1/customers";

	String LIST_ADDON_URL = "https://subscriptions.zoho.com/api/v1/addons";
	
	String[] SUBSCRIPTION_CHANGED = {"subscription_created","subscription_activation","subscription_upgraded","subscription_downgraded","subscription_ahead","subscription_renewed","subscription_reactivated","billing_date_changed","subscription_cancellation_scheduled","subscription_cancelling","subscription_cancelled","subscription_expiring","subscription_expired","subscription_deleted"};
	
	String[] PAYMENT_RECEIVED = {"payment_thankyou"};
	
	String[] CARD_REQUIRED = {"payment_declined","card_expiring","card_expired","card_deleted"};

	boolean updateSubscription(SubscriptionPlanDTO subscriptionDto, String subscriptionId) throws AppException;
	
	boolean updateSubscription(String planCode) throws AppException;
	
	int updateSubscription(Integer units) throws AppException;

	void getAllPlans() throws AppException;
	
	List<Plan> getAllPlansFromDatabase() throws AppException;
	
	List<Plan> getUserFilteredPlans() throws AppException;

	String addCustomer(CustomerDTO customerDTO) throws AppException;
	
	boolean createCustomer(User user) throws AppException;
	
	boolean updateCustomer(String customerJson) throws AppException;

	String addSubscription(SubscriptionPlanDTO subscriptionDto) throws AppException;
	
	String getNewSubscriptionUrl(String planCode, Integer quantity) throws AppException;

	void cancelSubscription(String subscriptionId) throws AppException;
	
	String getUpdateCardUrl() throws AppException;

	String getInvoicesByCustId() throws AppException;

	void getCustomerData(String customerId) throws AppException;

	void getAllAddons() throws AppException;

	boolean parseEventData(ZohoSubscriptionEventDTO dto) throws AppException;

	enum filter_by {
		All("Status.All"), Sent("Status.Sent"), Draft("Status.Draft"), OverDue("Status.OverDue"), Paid(
				"Status.Paid"), PartiallyPaid("Status.PartiallyPaid"), Void("Status.Void"), Unpaid("Status.UnPaid");

		private String filterBy;

		filter_by(String filterBy) {
			this.filterBy = filterBy;
		}

		public String filterBy() {
			return filterBy;
		}
	}

	enum transaction_type {
		All("TransactionType.All"), INVOICE("TransactionType.INVOICE"), PAYMENT("TransactionType.PAYMENT"), CREDIT(
				"TransactionType.CREDIT"), REFUND("TransactionType.REFUND");

		private String transactionType;

		transaction_type(String transactionType) {
			this.transactionType = transactionType;
		}

		public String transactionType() {
			return transactionType;
		}
	}

	Map<String, Object> getSubscriptionInfo() throws AppException;

	boolean emailAnInvoice(String invoiceId) throws AppException;

	boolean parseZohoFormsData(ZohoFormsEventData data) throws AppException;

}
