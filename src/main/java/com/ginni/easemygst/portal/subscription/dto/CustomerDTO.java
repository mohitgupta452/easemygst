package com.ginni.easemygst.portal.subscription.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class CustomerDTO {
	
	String display_name;
	
	String saluation;
	
	String first_name;
	
	String last_name;
	
	String email;
	
	String company_name;
	
	String phone;
	
	String mobile;
	
	String department;
	
	String designation;
	
	String website;
	
	String customer_id;
	
	String gst_no;
	
	String place_of_contact;
	
	BillingAddressDTO billing_address;
	
	ShippingAddressDTO shipping_address;

}
