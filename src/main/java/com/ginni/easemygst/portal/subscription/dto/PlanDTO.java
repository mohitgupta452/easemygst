package com.ginni.easemygst.portal.subscription.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public @Data class PlanDTO {
	
	String plan_code;
	
	String name;
	
	Integer recurring_price;
	
	Integer interval;
	
	Integer quantity;
	
	String interval_unit;
	
	Integer billing_cycles;
	
	Integer trial_period;
	
	Integer setup_fee;
	
	String product_id;
	
	String tax_id;
	
	String product_type;
	
	String hsn_or_sac;
	
	String description;
	
	String status;
	
	String created_time;
	
	String updated_time;

}
