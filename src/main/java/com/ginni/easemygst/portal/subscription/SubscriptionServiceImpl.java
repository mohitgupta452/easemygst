package com.ginni.easemygst.portal.subscription;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.SubscriptionChangedDTO;
import com.ginni.easemygst.portal.business.dto.ZohoFormsEventData;
import com.ginni.easemygst.portal.business.dto.ZohoSubscriptionEventDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.business.service.impl.ScheduleManagerImpl;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.persistence.entity.Plan;
import com.ginni.easemygst.portal.persistence.entity.State;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.User;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.subscription.dto.BillingAddressDTO;
import com.ginni.easemygst.portal.subscription.dto.CustomerDTO;
import com.ginni.easemygst.portal.subscription.dto.PlanDTO;
import com.ginni.easemygst.portal.subscription.dto.ShippingAddressDTO;
import com.ginni.easemygst.portal.subscription.dto.SubscriptionPlanDTO;
import com.mashape.unirest.http.JsonNode;

@Transactional
@Stateless
public class SubscriptionServiceImpl implements SubscriptionService {

	public static final String ZOHO_AUTH_VALIDITY = "zoho_auth_validity";

	public static final String ZOHO_AUTH_TOKEN = "zoho_auth_token";

	@Inject
	private Logger log;

	@Inject
	private UserBean userBean;

	@Inject
	private RedisService redisService;

	@Inject
	private CrudService crudService;

	@Inject
	private UserService userService;

	@Inject
	private FinderService finderService;
	
	@Inject
	private ApiLoggingService apiLoggingService;

	private Properties config;

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	private Properties parsePropertiesString(String str) {
		final Properties p = new Properties();
		try {
			p.load(new StringReader(str));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}

	private Map<String, String> getAuthHeaders() {

		Properties config = new Properties();
		try {
			config.load(ScheduleManagerImpl.class.getClassLoader().getResourceAsStream("config.properties"));
			log.info("config.properties loaded successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}

		String orgId = config.getProperty("main.zoho.subscription.organisation.id");

		Map<String, String> outputHeaders = new HashMap<>();
		outputHeaders.put("X-com-zoho-subscriptions-organizationid", orgId);

		/*
		boolean isApiCallRequired = true;

		if (!StringUtils.isEmpty(redisService.getValue(ZOHO_AUTH_TOKEN))) {
			outputHeaders.put("Authorization", redisService.getValue(ZOHO_AUTH_TOKEN));
			isApiCallRequired = false;
		}

		if (isApiCallRequired) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("SCOPE", SCOPE);
			jsonObject.put("EMAIL_ID", EMAIL_ADDRESS);
			jsonObject.put("PASSWORD", PASSWORD);

			String output = RestClient.callZohoApi(AUTH_URL, "POST", new JsonNode(jsonObject.toString()),
					new HashMap<>());
			Properties authProp = new Properties();
			authProp = parsePropertiesString(output);
			outputHeaders.put("Authorization", authProp.getProperty("AUTHTOKEN"));
			redisService.setValue(ZOHO_AUTH_TOKEN, authProp.getProperty("AUTHTOKEN"), false, 86400);
		}*/

		return outputHeaders;
	}

	@Override
	public void getAllPlans() throws AppException {

		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		String output = RestClient.callApi(ALL_PLAN_URL, "GET", new JsonNode(""), headers);
		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("plans")) {
					JSONArray jsonArray = object.getJSONArray("plans");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						try {
							PlanDTO plan = JsonMapper.objectMapper.readValue(jsonObject.toString(), PlanDTO.class);
							Plan planEm = finderService.findPlanByCode(plan.getPlan_code());
							Plan planNew = new Plan();
							planNew.setBillingCycles(plan.getBilling_cycles());
							planNew.setCreatedTime(plan.getCreated_time());
							planNew.setDescription(plan.getDescription());
							planNew.setHsnOrSac(plan.getHsn_or_sac());
							planNew.setPlanInterval(plan.getInterval());
							planNew.setPlanIntervalUnit(plan.getInterval_unit());
							planNew.setName(plan.getName());
							planNew.setPlanCode(plan.getPlan_code());
							planNew.setProductId(plan.getProduct_id());
							planNew.setProductType(plan.getProduct_type());
							planNew.setRecurringPrice(plan.getRecurring_price());
							planNew.setSetupFee(plan.getSetup_fee());
							planNew.setStatus(plan.getStatus());
							planNew.setTaxId(plan.getTax_id());
							planNew.setTrialPeriod(plan.getTrial_period());
							planNew.setUpdatedTime(plan.getUpdated_time());
							if (Objects.isNull(planEm)) {
								crudService.create(planNew);
							} else {
								planNew.setId(planEm.getId());
								crudService.update(planNew);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}
	}

	@Override
	public List<Plan> getAllPlansFromDatabase() throws AppException {
		getAllPlans();
		List<Plan> plans = finderService.findAllPlan();
		return plans;
	}

	@Override
	public List<Plan> getUserFilteredPlans() throws AppException {

		if (!Objects.isNull(userBean)) {
			User user = crudService.find(User.class, userBean.getUserDto().getId());
			if (!Objects.isNull(user.getOrganisation())
					&& !StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {
				Organisation organisation = user.getOrganisation();

				String planCode = organisation.getPlanCode();
				if (!StringUtils.isEmpty(planCode)) {

					if (planCode.length() == 5) {
						String planType = planCode.substring(0, 4);
						List<Plan> plans = finderService.findPlanByType(planType);
						return plans;
					}

				}
			}

			return new ArrayList<>();
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	// don't need to use this function we can add both functionality using
	// single api
	@Override
	public String addCustomer(CustomerDTO customerDTO) throws AppException {

		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		String customerId = null;
		String customerData = null;

		try {
			customerData = JsonMapper.objectMapper.writeValueAsString(customerDTO);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String output = RestClient.callApi(ADD_CUSTOMER_URL, "POST", new JsonNode(customerData), headers);
		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("customer")) {
					JSONObject innerJSON = new JSONObject(object.get("customer").toString());
					customerId = innerJSON.get("customer_id").toString();
				}
			} else {
				// invalid response. please check logs for more information.
			}
		}
		return customerId;
	}

	@Override
	public boolean createCustomer(User user) throws AppException {

		if (!Objects.isNull(user)) {

			CustomerDTO customerDTO = new CustomerDTO();

			customerDTO.setDisplay_name(user.getUsername());
			customerDTO.setFirst_name(user.getFirstName());
			customerDTO.setLast_name(user.getLastName());
			customerDTO.setEmail(user.getUsername());
			customerDTO.setCompany_name(user.getFirstName() + " " + user.getLastName());
			if (!Objects.isNull(user.getUserContacts()) && !user.getUserContacts().isEmpty()) {
				customerDTO.setMobile(user.getUserContacts().get(0).getMobileNumber());
			}

			String customerId = addCustomer(customerDTO);
			if (!StringUtils.isEmpty(customerId)) {
				Organisation organisation = user.getOrganisation();
				if (!Objects.isNull(organisation)) {
					organisation.setCustomerId(customerId);
					crudService.update(organisation);
					return Boolean.TRUE;
				}
			}

			return Boolean.FALSE;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public boolean updateCustomer(String customerJson) throws AppException {

		if (!StringUtils.isEmpty(customerJson)) {

			String customerId = null;
			String customerData = null;
			Organisation organisation = null;

			if (!Objects.isNull(userBean)) {
				User user = crudService.find(User.class, userBean.getUserDto().getId());
				if (!Objects.isNull(user.getOrganisation())
						&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
					organisation = user.getOrganisation();
					customerId = user.getOrganisation().getCustomerId();
				}
			}

			ShippingAddressDTO shippingAddressDTO = new ShippingAddressDTO();
			BillingAddressDTO billingAddressDTO = new BillingAddressDTO();

			JSONObject requestJson = new JSONObject(customerJson);

			CustomerDTO customerDTO = new CustomerDTO();
			if (requestJson.has("gstin")) {
				customerDTO.setGst_no(requestJson.getString("gstin"));
				organisation.setGstin(customerDTO.getGst_no());
			}
			if (requestJson.has("pos")) {
				int stateCode = 0;
				State state = null;
				try {
					stateCode = Integer.valueOf(requestJson.getString("pos"));
					state = em.getReference(State.class, stateCode);
					customerDTO.setPlace_of_contact(state.getCode());
				} catch (Exception e) {
					throw new AppException(ExceptionCode._INVALID_PARAMETER);
				}
			}
			if (requestJson.has("billing")) {
				JSONObject billingJson = requestJson.getJSONObject("billing");
				billingAddressDTO.setCity(billingJson.getString("city"));
				billingAddressDTO.setCountry("India");
				billingAddressDTO.setState(billingJson.getString("state"));
				billingAddressDTO.setStreet(billingJson.getString("street"));
				if (!StringUtils.isEmpty(billingJson.getString("zip")))
					billingAddressDTO.setZip(billingJson.getLong("zip"));
				customerDTO.setBilling_address(billingAddressDTO);
				organisation.setBillingAddress(requestJson.getJSONObject("billing").toString());
			}
			if (requestJson.has("shipping")) {
				JSONObject shippingJson = requestJson.getJSONObject("shipping");
				shippingAddressDTO.setCity(shippingJson.getString("city"));
				shippingAddressDTO.setCountry("India");
				shippingAddressDTO.setState(shippingJson.getString("state"));
				shippingAddressDTO.setStreet(shippingJson.getString("street"));
				if (!StringUtils.isEmpty(shippingJson.getString("zip")))
					shippingAddressDTO.setZip(shippingJson.getLong("zip"));
				customerDTO.setShipping_address(shippingAddressDTO);
				organisation.setShippingAddress(requestJson.getJSONObject("shipping").toString());
			}

			Map<String, String> headers = new HashMap<>();
			headers.putAll(getAuthHeaders());

			try {
				customerData = JsonMapper.objectMapper.writeValueAsString(customerDTO);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String output = RestClient.callApi(ADD_CUSTOMER_URL + "/" + customerId, "PUT", new JsonNode(customerData),
					headers);
			JSONObject object = new JSONObject(output);

			if (!object.has("status_cd")) {
				if (object.has("code") && object.getInt("code") == 0) {
					if (object.has("customer")) {
						crudService.update(organisation);
						return Boolean.TRUE;
					}
				} else {
					// invalid response. please check logs for more information.
				}
			}

			return Boolean.FALSE;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	// use to add customer with particular subscription
	@Override
	public String addSubscription(SubscriptionPlanDTO subscriptionDto) throws AppException {

		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		String subscriptionCheckoutUrl = null;
		String subscriptionData = null;

		try {
			subscriptionData = JsonMapper.objectMapper.writeValueAsString(subscriptionDto);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String output = RestClient.callApi(ADD_SUBSCRIPTION_URL, "POST", new JsonNode(subscriptionData), headers);
		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("hostedpage")) {
					subscriptionCheckoutUrl = object.getJSONObject("hostedpage").get("url").toString();
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}

		return subscriptionCheckoutUrl;
	}

	@Override
	public String getNewSubscriptionUrl(String planCode, Integer quantity) throws AppException {

		String customerId = "";

		if (!Objects.isNull(userBean)) {
			User user = crudService.find(User.class, userBean.getUserDto().getId());

			if (!Objects.isNull(user.getOrganisation())
					&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
				customerId = user.getOrganisation().getCustomerId();

				SubscriptionPlanDTO subscriptionPlanDTO = new SubscriptionPlanDTO();
				subscriptionPlanDTO.setCustomer_id(customerId);
				PlanDTO planDTO = new PlanDTO();
				planDTO.setPlan_code(planCode);
				planDTO.setQuantity(quantity);
				subscriptionPlanDTO.setPlan(planDTO);

				config = new Properties();
				try {
					config.load(ScheduleManagerImpl.class.getClassLoader().getResourceAsStream("config.properties"));
					log.info("config.properties loaded successfully");
				} catch (IOException e) {
					e.printStackTrace();
				}

				subscriptionPlanDTO.setRedirect_url(config.getProperty("main.web.url.user.home"));

				return addSubscription(subscriptionPlanDTO);
			}
		}

		return null;
	}

	// parameters need to update subscription is customer_id and subscription_id
	@Override
	public boolean updateSubscription(SubscriptionPlanDTO subscriptionDto, String subscriptionId) throws AppException {

	/*	Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());
		String subscriptionData = null;

		try {
			subscriptionData = JsonMapper.objectMapper.writeValueAsString(subscriptionDto);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String output = RestClient.callZohoApi(UPDATE_SUBSCRIPTION_URL + "/" + subscriptionId, "PUT",
				new JsonNode(subscriptionData), headers);

		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("subscription")) {
					subscriptionId = object.getJSONObject("subscription").get("subscription_id").toString();
					return Boolean.TRUE;
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}*/

		return Boolean.TRUE;
	}

	@Override
	public boolean updateSubscription(String planCode) throws AppException {

		if (!StringUtils.isEmpty(planCode)) {

			String customerId = "";
			String subscriptionId = "";
			if (!Objects.isNull(userBean)) {
				User user = crudService.find(User.class, userBean.getUserDto().getId());
				if (!Objects.isNull(user.getOrganisation())
						&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
					customerId = user.getOrganisation().getCustomerId();
					subscriptionId = user.getOrganisation().getSubscriptionId();

					SubscriptionPlanDTO subscriptionDto = new SubscriptionPlanDTO();
					PlanDTO planDTO = new PlanDTO();
					planDTO.setPlan_code(planCode);
					subscriptionDto.setPlan(planDTO);
					subscriptionDto.setEnd_of_term(true);

					return updateSubscription(subscriptionDto, subscriptionId);
				}
			}

			return Boolean.FALSE;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public int updateSubscription(Integer units) throws AppException {

		if (!Objects.isNull(units)) {

			String customerId = "";
			String subscriptionId = "";
			if (!Objects.isNull(userBean)) {
				User user = crudService.find(User.class, userBean.getUserDto().getId());
				if (!Objects.isNull(user.getOrganisation())
						&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
					customerId = user.getOrganisation().getCustomerId();
					subscriptionId = user.getOrganisation().getSubscriptionId();

					BigInteger gstinCount = (BigInteger) em.createNativeQuery(String.format(
							"select count(distinct gstinId) from user_gstin where userId = %s and organisationId = %s",
							user.getId(), user.getOrganisation().getId())).getSingleResult();

					BigInteger userCount = (BigInteger) em.createNativeQuery(String.format(
							"select count(distinct userId) from user_gstin where userId = %s and organisationId = %s",
							user.getId(), user.getOrganisation().getId())).getSingleResult();

					int minUnits = gstinCount.intValue() > userCount.intValue() ? gstinCount.intValue()
							: userCount.intValue();

					Integer currentUnits = user.getOrganisation().getUnits();

					SubscriptionPlanDTO subscriptionDto = new SubscriptionPlanDTO();
					PlanDTO planDTO = new PlanDTO();
					planDTO.setQuantity(units);
					subscriptionDto.setPlan(planDTO);

					if (currentUnits == null)
						throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
					else if (currentUnits.equals(units) || units < minUnits)
						throw new AppException(ExceptionCode._INVALID_GSTIN_UNIT);
					else if (currentUnits > units)
						subscriptionDto.setEnd_of_term(true);
					else if (currentUnits < units)
						subscriptionDto.setEnd_of_term(false);

					int i = 0;
					if (subscriptionDto.isEnd_of_term()) {
						i = 1;
					} else {
						i = 2;
					}

					boolean flag = updateSubscription(subscriptionDto, subscriptionId);
					if (flag)
						return i;
					else
						return 0;
				}
			}

			return 0;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	// cancel existing subscription using subscriptionId with cancel_at_end true
	// means valid for this term
	@Override
	public void cancelSubscription(String subscriptionId) throws AppException {

		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("cancel_at_end", true);
		String output = RestClient.callZohoApi(CANCEL_SUBSCRIPTION_URL + "/" + subscriptionId + "/" + "cancel", "POST",
				new JsonNode(jsonObject.toString()), headers);

		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("subscription")) {
					System.out.println("successfully updated subscription id "
							+ object.getJSONObject("subscription").get("subscription_id"));
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}
	}

	// get all invoices of particular customer with filter_by condition
	private JSONArray getInvoicesByCustId(String customerId) throws AppException {

		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("filter_by", filter_by.All.filterBy());
		jsonObject.put("customer_id", customerId);

		String output = RestClient.callZohoApi(LIST_INVOICES_URL, "GET", new JsonNode(jsonObject.toString()), headers);

		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("invoices")) {
					return object.getJSONArray("invoices");
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}

		return null;
	}

	@Override
	public String getInvoicesByCustId() throws AppException {

		String customerId = "";
		if (!Objects.isNull(userBean)) {
			User user = crudService.find(User.class, userBean.getUserDto().getId());
			if (!Objects.isNull(user.getOrganisation())
					&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
				customerId = user.getOrganisation().getCustomerId();
				return getInvoicesByCustId(customerId).toString();
			}
		}

		return "[]";
	}

	@Override
	public boolean emailAnInvoice(String invoiceId) throws AppException {

		String request = "{\"to_mail_ids\": [\"USERNAME\"],\"subject\": \"Invoice for the subscription.\",\"body\": \"Please find attached invoice for your subscription.\"}";
		if (!Objects.isNull(userBean)) {
			User user = crudService.find(User.class, userBean.getUserDto().getId());
			if (!Objects.isNull(user.getOrganisation())
					&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
				request = request.replace("USERNAME", userBean.getUserDto().getUsername());
				JSONObject jsonObject = new JSONObject(request);

				Map<String, String> headers = new HashMap<>();
				headers.putAll(getAuthHeaders());

				String url = String.format(EMAIL_INVOICE_URL, invoiceId);
				String output = RestClient.callZohoApi(url, "POSTP", new JsonNode(jsonObject.toString()), headers);

				JSONObject object = new JSONObject(output);

				if (!object.has("status_cd")) {
					if (object.has("code") && object.getInt("code") == 0) {
						return Boolean.TRUE;
					}
				} else {
					// invalid response. please check logs for more information.
				}
			}
		}

		return Boolean.FALSE;
	}

	// get customer Data using Customer Id
	@Override
	public void getCustomerData(String customerId) throws AppException {
		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		String output = RestClient.callZohoApi(RETREIVE_CUSTOMER_URL + "/" + customerId, "GET", new JsonNode(""),
				headers);

		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("customer")) {
					// System.out.println("successfully updated subscription id
					// "+ object.getJSONArray("invoices"));
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}
	}

	// list all addons
	@Override
	public void getAllAddons() throws AppException {
		Map<String, String> headers = new HashMap<>();
		headers.putAll(getAuthHeaders());

		String output = RestClient.callZohoApi(LIST_ADDON_URL, "GET", new JsonNode(""), headers);

		JSONObject object = new JSONObject(output);

		if (!object.has("status_cd")) {
			if (object.has("code") && object.getInt("code") == 0) {
				if (object.has("addons")) {
					// System.out.println("successfully updated subscription id
					// "+ object.getJSONArray("invoices"));
				}
			}
		} else {
			// invalid response. please check logs for more information.
		}
	}

	@Override
	public boolean parseEventData(ZohoSubscriptionEventDTO dto) {

		if (!Objects.isNull(dto)) {

			if (Arrays.asList(SUBSCRIPTION_CHANGED).contains(dto.getEvent_type())) {
				recordSubscriptionChangedData(dto.getData().get("subscription"));
			} else if (Arrays.asList(PAYMENT_RECEIVED).contains(dto.getEvent_type())) {
				recordPaymentReceivedData(dto.getData().get("payment"));
			} else if (Arrays.asList(CARD_REQUIRED).contains(dto.getEvent_type())) {
				if (dto.getData().has("payment"))
					recordCardRequiredData(dto.getData().get("payment"));
				else if (dto.getData().has("card"))
					recordCardRequiredData(dto.getData().get("card"));
			}
		}

		return Boolean.TRUE;
	}

	@Override
	public boolean parseZohoFormsData(ZohoFormsEventData dto) {

		if (!Objects.isNull(dto)) {

			if (dto.getFunctionKey().equalsIgnoreCase("UPDATE_SUBSCRIPTION")) {
				updateUserSubscription(dto.getData());
			} else if (dto.getFunctionKey().equalsIgnoreCase("PASSWORD_RESET")) {
				resetUserPassword(dto.getData());
			} else if (dto.getFunctionKey().equalsIgnoreCase("UPDATE_ACCOUNT_MANAGER")) {
				updateAccountManager(dto.getData());
			} else if (dto.getFunctionKey().equalsIgnoreCase("INVITE_USER")) {
				inviteNewUser(dto.getData());
			} else if (dto.getFunctionKey().equalsIgnoreCase("UNAUTH_GSTIN")) {
				unauthGstin(dto.getData());
			} else if (dto.getFunctionKey().equalsIgnoreCase("GOVT_NOTIFICATIONS")) {
				apiLoggingService.saveGovtNotification(dto.getData());
			} 
		}

		return Boolean.TRUE;
	}
	
	private void unauthGstin(JsonNode data) {

		String gstin = data.getObject().getString("gstin");
		try {
			TaxpayerGstin tg = finderService.findTaxpayerGstin(gstin);
			if(!Objects.isNull(tg)) {
				tg.setAuthExpiry(null);
				tg.setAuthenticated((byte)0);
				crudService.update(tg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void inviteNewUser(JsonNode data) {

		String username = data.getObject().getString("username");
		String name = data.getObject().getString("name");
		String company = data.getObject().getString("company");
		try {
			userService.createUserAccount(username, name, company);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void updateUserSubscription(JsonNode data) {

		if (!Objects.isNull(data)) {
			try {
				Organisation organisation = null;
				organisation = finderService.findOrganisationByUsername(data.getObject().getString("customerId"));
				log.info("getting subscription id for cust id - " + data.getObject().getString("customerId"));

				if (!Objects.isNull(organisation)) {

					if (data.getObject().has("createdAt") && !StringUtils.isEmpty(data.getObject().getString("createdAt"))) {
						Date createdAt = formsDateFormat.parse(data.getObject().getString("createdAt"));
						organisation.setCreatedAt(createdAt);
					}

					if (data.getObject().has("lastBillingDate") && !StringUtils.isEmpty(data.getObject().getString("lastBillingDate"))) {
						Date lastBillingAt = formsDateFormat.parse(data.getObject().getString("lastBillingDate"));
						organisation.setLastBilling(lastBillingAt);
					}

					if (data.getObject().has("subscriptionId") && !StringUtils.isEmpty(data.getObject().getString("subscriptionId"))) {
						organisation.setSubscriptionId(data.getObject().getString("subscriptionId"));
					}

					if (data.getObject().has("expiresAt") && !StringUtils.isEmpty(data.getObject().getString("expiresAt"))) {
						Date expiresAt = formsDateFormat.parse(data.getObject().getString("expiresAt"));
						organisation.setExpiresAt(expiresAt);
						organisation.setCurrentTermEndsAt(expiresAt);

						Calendar cal = Calendar.getInstance();
						cal.setTime(expiresAt);
						cal.add(Calendar.DATE, 1);
						organisation.setNextBilling(cal.getTime());
					}

					if (data.getObject().has("activationDate") && !StringUtils.isEmpty(data.getObject().getString("activationDate"))) {
						Date activationDate = formsDateFormat.parse(data.getObject().getString("activationDate"));
						organisation.setActivationDate(activationDate);
					}

					if (data.getObject().has("invoiceCount") && !StringUtils.isEmpty(data.getObject().getString("invoiceCount"))) {
						organisation.setInvoiceCount(Integer.parseInt(data.getObject().getString("invoiceCount")));
					}

					if (data.getObject().has("discountAmount") && !StringUtils.isEmpty(data.getObject().getString("discountAmount"))) {
						organisation.setDiscountAmount(data.getObject().getString("discountAmount"));
					}

					if (data.getObject().has("units") && !StringUtils.isEmpty(data.getObject().getString("units"))) {
						organisation.setUnits(Integer.parseInt(data.getObject().getString("units")));
					}

					if (data.getObject().has("planCode") && !StringUtils.isEmpty(data.getObject().getString("planCode"))) {
						organisation.setPlanCode(data.getObject().getString("planCode"));
					}

					if (data.getObject().has("totalAmount") && !StringUtils.isEmpty(data.getObject().getString("totalAmount"))) {
						organisation.setTotalAmount(data.getObject().getString("totalAmount"));
					}

					if (data.getObject().has("gstin") && !StringUtils.isEmpty(data.getObject().getString("gstin"))) {
						organisation.setGstin(data.getObject().getString("gstin"));
					}

					if (data.getObject().has("unitCost") && !StringUtils.isEmpty(data.getObject().getString("unitCost"))) {
						organisation.setUnitCost(data.getObject().getString("unitCost"));
					}

					if (data.getObject().has("status") && !StringUtils.isEmpty(data.getObject().getString("status"))) {
						organisation.setStatus(data.getObject().getString("status"));
						if (organisation.getStatus().equalsIgnoreCase("trial")) {
							organisation.setPlanCode("EMGSM");
						}
					}

					organisation.setCardUpdate(Byte.valueOf((byte) 0));
					
					organisation.setAutoCollect(false);

					if (data.getObject().has("gstin") && !StringUtils.isEmpty(data.getObject().getString("gstin"))) {
						organisation.setGstin(data.getObject().getString("gstin"));
					}

					organisation = crudService.update(organisation);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void resetUserPassword(JsonNode data) {

		String username = data.getObject().getString("username");
		UserDTO userDto = finderService.getExistingUser(username);
		if (!Objects.isNull(userDto)) {
			try {
				userService.resetPassword(username, data.getObject().getString("password"),
						data.getObject().getString("requestedBy"));
			} catch (JSONException | AppException e) {
				e.printStackTrace();
			}
		}
	}

	private void updateAccountManager(JsonNode data) {

		try {
			Organisation organisation = null;
			if (Objects.isNull(organisation)) {
				String username = data.getObject().getString("username");
				UserDTO userDto = finderService.getExistingUser(username);
				if (!Objects.isNull(userDto)) {
					User user = crudService.find(User.class, userDto.getId());
					organisation = user.getOrganisation();
				}
			}

			if (!Objects.isNull(organisation)) {

				organisation.setSalesPerson(data.getObject().getString("managerName"));

				organisation.setSalesPersonId(data.getObject().getString("managerContact"));

				organisation = crudService.update(organisation);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void recordSubscriptionChangedData(com.fasterxml.jackson.databind.JsonNode data) {

		if (!Objects.isNull(data)) {
			try {
				SubscriptionChangedDTO changedDTO = JsonMapper.objectMapper.readValue(data.toString(),
						SubscriptionChangedDTO.class);

				Organisation organisation = null;
				organisation = finderService.getOrganisationBySubscription(changedDTO.getSubscription_id());
				log.info("getting subscription id - " + changedDTO.getSubscription_id());
				if (Objects.isNull(organisation)) {
					organisation = finderService
							.getOrganisationByCustomer(changedDTO.getCustomer().get("customer_id").asText());
					if (Objects.isNull(organisation)) {
						String username = changedDTO.getCustomer().get("display_name").asText();
						UserDTO userDto = finderService.getExistingUser(username);
						if (!Objects.isNull(userDto)) {
							User user = crudService.find(User.class, userDto.getId());
							organisation = user.getOrganisation();
						}
					}
				}
				log.info("getting customer id - " + changedDTO.getCustomer().get("customer_id").asText());
				if (!Objects.isNull(organisation)) {

					Date createdAt = dateFormat.parse(changedDTO.getCreated_at());
					organisation.setCreatedAt(createdAt);

					Date nextBillingAt = dateFormat.parse(changedDTO.getNext_billing_at());
					organisation.setNextBilling(nextBillingAt);

					if (!StringUtils.isEmpty(changedDTO.getLast_billing_at())) {
						Date lastBillingAt = dateFormat.parse(changedDTO.getLast_billing_at());
						organisation.setLastBilling(lastBillingAt);
					}

					organisation.setSubscriptionId(changedDTO.getSubscription_id());

					if (!StringUtils.isEmpty(changedDTO.getExpires_at())) {
						Date expiresAt = dateFormat.parse(changedDTO.getExpires_at());
						organisation.setExpiresAt(expiresAt);
					}

					ObjectMapper mapper = new ObjectMapper();
					Map<String, Object> map = new HashMap<String, Object>();
					map = mapper.readValue(changedDTO.getCustomer().toString(),
							new TypeReference<Map<String, Object>>() {
							});

					organisation.setIntervalUnits(changedDTO.getInterval_unit());

					organisation.setPlanDetails(changedDTO.getPlan().asText());

					organisation.setUnits(changedDTO.getPlan().get("quantity").asInt());

					organisation.setPlanCode(changedDTO.getPlan().get("plan_code").asText());

					organisation.setDiscountAmount(changedDTO.getPlan().get("discount").asText());

					organisation.setTotalAmount(changedDTO.getPlan().get("total").asText());

					organisation.setUnitCost(changedDTO.getPlan().get("price").asText());

					Date currentTermEndsAt = dateFormat.parse(changedDTO.getCurrent_term_ends_at());
					organisation.setCurrentTermEndsAt(currentTermEndsAt);
					organisation.setExpiresAt(currentTermEndsAt);

					if (!StringUtils.isEmpty(changedDTO.getCancelled_at())) {
						Date cancelledAt = dateFormat.parse(changedDTO.getCancelled_at());
						organisation.setExpiresAt(cancelledAt);
					}

					organisation.setSalesPerson(changedDTO.getSalesperson_name());

					organisation.setSalesPersonId(changedDTO.getSalesperson_id());

					organisation.setAutoCollect(changedDTO.isAuto_collect());

					organisation.setStatus(changedDTO.getStatus());

					organisation.setCardUpdate(Byte.valueOf((byte) 0));

					if (!Objects.isNull(changedDTO.getCustom_fields())) {
						if (changedDTO.getCustom_fields().isArray()) {
							Iterator<com.fasterxml.jackson.databind.JsonNode> iterator = changedDTO.getCustom_fields()
									.elements();
							while (iterator.hasNext()) {
								com.fasterxml.jackson.databind.JsonNode jnode = iterator.next();
								if (jnode.has("placeholder")
										&& jnode.get("placeholder").asText().equals("cf_invoice_count")) {
									String value = jnode.get("value").asText();
									organisation.setInvoiceCount(Integer.parseInt(value));
								}
							}
						}
					}

					if (map.containsKey("billing_address")) {
						JSONObject jsonObject = new JSONObject(mapper.writeValueAsString(map.get("billing_address")));
						organisation.setBillingAddress(jsonObject.toString());
					}

					if (map.containsKey("shipping_address")) {
						JSONObject jsonObject = new JSONObject(mapper.writeValueAsString(map.get("shipping_address")));
						organisation.setShippingAddress(jsonObject.toString());
					}

					if (map.containsKey("gst_no")) {
						organisation.setGstin(map.get("gst_no").toString());
					}
					// log.info("getting organisation {}",
					// JsonMapper.objectMapper.writeValueAsString(organisation));
					organisation = crudService.update(organisation);
					// log.info(" organisation updated successfully {}",
					// JsonMapper.objectMapper.writeValueAsString(organisation));
				}
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void recordPaymentReceivedData(com.fasterxml.jackson.databind.JsonNode data) {

		if (!Objects.isNull(data)) {
			String customerId = data.get("customer_id").asText();

			Organisation organisation = null;
			organisation = finderService.getOrganisationByCustomer(customerId);

			if (!Objects.isNull(organisation)) {

				organisation.setCardUpdate(Byte.valueOf((byte) 0));

				crudService.update(organisation);
			}
		}

	}

	private void recordCardRequiredData(com.fasterxml.jackson.databind.JsonNode data) {

		if (!Objects.isNull(data)) {
			String customerId = data.get("customer_id").asText();

			Organisation organisation = null;
			organisation = finderService.getOrganisationByCustomer(customerId);

			if (!Objects.isNull(organisation)) {

				organisation.setCardUpdate(Byte.valueOf((byte) 1));

				crudService.update(organisation);
			}
		}

	}

	@Override
	public String getUpdateCardUrl() throws AppException {

		if (!Objects.isNull(userBean)) {
			User user = crudService.find(User.class, userBean.getUserDto().getId());

			String cardUrl = null;

			if (!Objects.isNull(user.getOrganisation())
					&& !StringUtils.isEmpty(user.getOrganisation().getCustomerId())) {
				String subscriptionId = user.getOrganisation().getSubscriptionId();

				config = new Properties();
				try {
					config.load(ScheduleManagerImpl.class.getClassLoader().getResourceAsStream("config.properties"));
					log.info("config.properties loaded successfully");
				} catch (IOException e) {
					e.printStackTrace();
				}

				Map<String, String> headers = new HashMap<>();
				headers.putAll(getAuthHeaders());

				String url = config.getProperty("main.web.url.user.home.card");
				String request = "{\"subscription_id\": \"" + subscriptionId + "\", \"redirect_url\": \"" + url + "\"}";

				String output = RestClient.callApi(UPDATE_CARD_URL, "POST", new JsonNode(request), headers);
				JSONObject object = new JSONObject(output);

				if (!object.has("status_cd")) {
					if (object.has("code") && object.getInt("code") == 0) {
						if (object.has("hostedpage")) {
							cardUrl = object.getJSONObject("hostedpage").get("url").toString();
						}
					}
				} else {
					// invalid response. please check logs for more information.
				}

				return cardUrl;
			}
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getSubscriptionInfo() throws AppException {

		if (!Objects.isNull(userBean)) {
			User user = crudService.find(User.class, userBean.getUserDto().getId());
			if (!Objects.isNull(user.getOrganisation())
					&& !StringUtils.isEmpty(user.getOrganisation().getSubscriptionId())) {

				if (!StringUtils.isEmpty(user.getOrganisation().getStatus())
						&& user.getOrganisation().getStatus().equalsIgnoreCase("trial")) {
					return null;
				}

				ObjectMapper m = new ObjectMapper();
				Map<String, Object> props = m.convertValue(user.getOrganisation(), Map.class);

				BigInteger gstinCount = (BigInteger) em.createNativeQuery(
						String.format("select count(distinct gstinId) from user_gstin where organisationId = %s",
								user.getOrganisation().getId()))
						.getSingleResult();

				BigInteger userCount = (BigInteger) em.createNativeQuery(
						String.format("select count(distinct userId) from user_gstin where organisationId = %s",
								user.getOrganisation().getId()))
						.getSingleResult();

				int units = user.getOrganisation().getUnits();
				Integer invoiceCount = user.getOrganisation().getInvoiceCount();
				int defaultInvoiceCountValue = 500;
				if (!Objects.isNull(invoiceCount)) {
					defaultInvoiceCountValue = invoiceCount;
				}
				Integer invoiceAvailable = units * defaultInvoiceCountValue;

				props.put("minCount",
						gstinCount.intValue() > userCount.intValue() ? gstinCount.intValue() : userCount.intValue());
				props.put("gstinCount", gstinCount.intValue());
				props.put("userCount", userCount.intValue() > 0 ? userCount.intValue() - 1 : 0);
				props.put("invoiceAvailable", invoiceAvailable);
				props.put("invoiceUploaded", 0);

				SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");
				SimpleDateFormat psdf = new SimpleDateFormat("yyyy-MM-dd");

				try {
					if (props.containsKey("lastBilling") && !Objects.isNull(props.get("lastBilling"))
							&& !StringUtils.isEmpty(props.get("lastBilling").toString())) {
						props.put("lastBilling", sdf1.format(psdf.parse(props.get("lastBilling").toString())));
					}
					if (props.containsKey("createdAt") && !Objects.isNull(props.get("createdAt"))
							&& !StringUtils.isEmpty(props.get("createdAt").toString())) {
						props.put("createdAt", sdf1.format(psdf.parse(props.get("createdAt").toString())));
					}
					if (props.containsKey("expiresAt") && !Objects.isNull(props.get("expiresAt"))
							&& !StringUtils.isEmpty(props.get("expiresAt").toString())) {
						props.put("expiresAt", sdf1.format(psdf.parse(props.get("expiresAt").toString())));
					}
					if (props.containsKey("currentTermEndsAt") && !Objects.isNull(props.get("currentTermEndsAt"))
							&& !StringUtils.isEmpty(props.get("currentTermEndsAt").toString())) {
						props.put("currentTermEndsAt",
								sdf1.format(psdf.parse(props.get("currentTermEndsAt").toString())));
					}
					if (props.containsKey("nextBilling") && !Objects.isNull(props.get("nextBilling"))
							&& !StringUtils.isEmpty(props.get("nextBilling").toString())) {
						props.put("nextBilling", sdf1.format(psdf.parse(props.get("nextBilling").toString())));
					}
				} catch (ParseException e) {
					log.error("parsing exception ",e);
				}

				Plan plan = finderService.findPlanByCode(props.get("planCode").toString());
				props.put("plan", plan);

				SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");

				props.put("lastUpdated", sdf.format(Calendar.getInstance().getTime()));

				return props;
			}
		}

		return null;
	}

}
