package com.ginni.easemygst.portal.subscription.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class ShippingAddressDTO{
	
	String attention;
	
	String street;
	
	String city;
	
	String state;
	
	Long zip;
	
	String country;
	
	Long fax;
	
}
