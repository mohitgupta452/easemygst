package com.ginni.easemygst.portal.subscription.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class BillingAddressDTO{
	
	String attention;
	
	String street;
	
	String city;
	
	String state;
	
	Long zip;
	
	String country;
	
	Long fax;
	
}
