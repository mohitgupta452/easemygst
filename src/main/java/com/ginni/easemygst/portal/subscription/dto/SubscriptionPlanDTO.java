package com.ginni.easemygst.portal.subscription.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class SubscriptionPlanDTO{
	
	PlanDTO plan;
	
	AddonDTO addons;
	
	String coupon_code;
	
	String customer_id;
	
	CustomerDTO customer;
	
	String redirect_url;
	
	boolean end_of_term;
	
}
