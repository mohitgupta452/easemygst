package com.ginni.easemygst.portal.subscription.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class AddonDTO {
	
	String addonCode;
	
	String addonDesc;
	
	Integer price;
	
	Integer quantity;
	
	String taxId;
}
