package com.ginni.easemygst.portal.ws.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/emgst")
public class EMGPortalJaxRsConfiguration extends Application {

}

