package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.service.ReportService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

@RequestScoped
@Path("/reportws")
@Produces("application/json")
@Consumes("application/json")
public class ReportWebservice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;
	
	@Inject
	private ReportService reportservice;
	
	@SuppressWarnings("finally")
	@GET
	@Path("/get/1vs3b/{panCard}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getgstr3bvsinvoices( @QueryParam("financialYear") String fyear,
			@QueryParam("refresh") boolean refresh,
			@PathParam("panCard") String panCard,@QueryParam("gstin") String gstin) throws JsonProcessingException {
		log.info("1vs3b method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		Map<String, Object> gstinDetail = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			if(StringUtils.isEmpty(fyear))
				fyear=CalanderCalculatorUtility.getCurrentFinancialYear();
			gstinDetail=reportservice.get1VS3breport(panCard,fyear,refresh,gstin);
			respBuilder.put("data", gstinDetail);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		} catch (AppException e) {
          e.printStackTrace();
		
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("1vs3b Exception exception", e);
		} finally {
			log.info("1vs3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	

	@SuppressWarnings("finally")
	@GET
	@Path("/get/2avs3b/{panCard}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getgstr2vsinvoices11( @QueryParam("financialYear") String fyear,@PathParam("panCard") String panCard,@QueryParam("refresh") boolean refresh) throws JsonProcessingException {
		log.info("2avs3b method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		Map<String, Object> gstinDetail = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if(StringUtils.isEmpty(fyear))
				fyear=CalanderCalculatorUtility.getCurrentFinancialYear();
			gstinDetail=reportservice.get2aVS3breport(panCard,fyear,refresh);
			respBuilder.put("data", gstinDetail);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		} catch (AppException e) {
          e.printStackTrace();
		
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("2avs3b Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("2avs3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
	@GET
	@Path("/download/compresionreport/{panCard}/{filetype}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response downloadOutwardCompresionReport(@DefaultValue("2017-2018")@QueryParam("financialYear") String fyear,@PathParam("panCard") String panCard,@PathParam("filetype") String fileType,@QueryParam("refresh") boolean refresh) throws Exception {
         		log.info("downloadPdfcompresionReport method starts..");
         		Map<String, Object> respBuilder = new HashMap<>();
         		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
        		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
        try{
				String filePath = reportservice.generate1vs3breportPdf(panCard,fyear,fileType);
				File file = new File(filePath);
				respBuilder.put("data",file.getName());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
        }
        catch (Exception e) {
  			log.error("downloadPdfcompresionReport exception {} ", e.getMessage());
  			e.printStackTrace();
  		} finally {
  			log.info("downloadPdfcompresionReport method ends..");
  			Response response = Response.status(200).entity(respBuilder).build();
  			return response;
  		}
	}
	
	
	@GET
	@Path("/download/compresionreport/inward/{panCard}/{filetype}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response downloadInwardCompresionReport(@DefaultValue("2017-2018")@QueryParam("financialYear") String fyear,@PathParam("panCard") String panCard,@PathParam("filetype") String fileType,@QueryParam("refresh") boolean refresh) throws Exception {
		log.info("downloadPdfcompresionReport method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String filePath = reportservice.generate2avs3breportPdf(panCard, fyear, fileType);
			File file = new File(filePath);
			respBuilder.put("data", file.getName());
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		} catch (Exception e) {
			log.error("downloadPdfcompresionReport exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("downloadPdfcompresionReport method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

}
