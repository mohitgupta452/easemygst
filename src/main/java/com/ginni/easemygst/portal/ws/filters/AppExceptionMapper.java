package com.ginni.easemygst.portal.ws.filters;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.data.view.Response.Error;
import com.ginni.easemygst.portal.exception.EwayBillException;
import com.ginni.easemygst.portal.helper.AppException;

//@Provider
//@Produces(MediaType.APPLICATION_JSON)
//@PreMatching
//@Priority(Priorities.HEADER_DECORATOR)
public class AppExceptionMapper implements ExceptionMapper<Exception>
{
	
	private final static com.fasterxml.jackson.databind.ObjectMapper _Mapper =new com.fasterxml.jackson.databind.ObjectMapper();

	@Inject
	private Logger log;
	@Override
	public Response toResponse(Exception exception) {
		
		com.ginni.easemygst.portal.data.view.Response response = new com.ginni.easemygst.portal.data.view.Response();
		
		
		log.error("exception descrition from eacption mapper",exception);
		
		String code=null;
		String message=null;
		
		if(exception instanceof AppException)
		{
			AppException appException = (AppException)exception;
			
			code=appException.getCode();
			message=appException.getMessage();
		}
		else if(exception instanceof EwayBillException)
		{
			EwayBillException billException =(EwayBillException) exception;
			code=billException.getErrorCode();
			message=billException.getMessage();
		}
		
		response.setResponseCode("1001");
		response.setResponseMessage("Unable to process your request, Please check error for more information");
		
		Error error = new Error();
		
		error.setErrorCode(code);
		error.setErrorMessage(message);
		response.setError(error);
		
		return Response.status(200).entity(response).build();
	}

}
