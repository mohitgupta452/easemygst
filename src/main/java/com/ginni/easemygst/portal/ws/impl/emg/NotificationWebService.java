package com.ginni.easemygst.portal.ws.impl.emg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.service.NotificationService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.persistence.entity.Publicnotification;

@Path("notifyws/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NotificationWebService {
	
	@Inject
	private Logger log;
	
	@Inject
	private NotificationService notificationService;
	
	@Inject
	private UserBean userBean;
	
	@GET
	@Path("broadcast")
	public Map<String,Object> getEmgPublicAnnouncments() {
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
		List<Publicnotification> list= notificationService.getPublicNotification(userBean.getUserDto().getId());
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		respBuilder.put(ApplicationMetadata.RESP_DATA, list);
		
		}
		catch(Exception e) {
			log.error("exception",e);
		}
		return respBuilder;
	}
	
	@GET
	@Path("/broadcast/sse/")
	public void getEmgAnnouncmentsEventSource() {
		
		
	}

}
