package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ejb.Asynchronous;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.spi.AsynchronousResponse;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.AcceptWithItcDto;
import com.ginni.easemygst.portal.business.dto.BulkItcDto;
import com.ginni.easemygst.portal.business.dto.ChangeStatusDto;
import com.ginni.easemygst.portal.business.dto.CtinInvoiceDto;
import com.ginni.easemygst.portal.business.dto.DataStatusDTO;
import com.ginni.easemygst.portal.business.dto.Gstr3bDto;
import com.ginni.easemygst.portal.business.dto.GstrFlowDTO;
import com.ginni.easemygst.portal.business.dto.ItcAmountDto;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.ResponseDTO;
import com.ginni.easemygst.portal.business.dto.ReturnHistoryDTO;
import com.ginni.easemygst.portal.business.dto.ReturnSummaryDTO;
import com.ginni.easemygst.portal.business.dto.SearchTaxpayerDTO;
import com.ginni.easemygst.portal.business.dto.TaxSummaryDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.dto.UniversalReturnDTO;
import com.ginni.easemygst.portal.business.dto.WrapperDashBoardSummaryDto;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.ReturnStatusDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ReturnsService.DeleteType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.business.service.ScheduleManager;
import com.ginni.easemygst.portal.business.service.SecurityManager;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.TaxAmount;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.gst.response.GetCheckStatusResp;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.ExternalServiceExecutor;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.helper.SaveDataToGstnExecutorImpl;
import com.ginni.easemygst.portal.helper.SyncDataExecutor;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.notify.EmailFactory;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.FilePrefernce;
import com.ginni.easemygst.portal.persistence.entity.ReturnStatus;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.reports.ExagoReportHelper;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.ConsolidatedProcessFactory;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.ManipulateFile;

/**
 * 
 * @author Sumeet
 *
 */
/**
 * @author tcinv1052
 *
 */
@RequestScoped
@Path("/returnws")
@Produces("application/json")
@Consumes("application/json")
public class ReturnWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private ReturnsService returnService;

	@Inject
	private RedisService redisService;

	@Inject
	private FunctionalService functionalService;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private FinderService finderService;

	@Inject
	private UserBean userBean;

	@Inject
	private AppConfig appConfig;

	@Inject
	private SecurityManager securityManager;

	@Inject
	private ScheduleManager scheduleManager;

	@Inject
	private ApiLoggingService apiLoggingService;

	// private ExecutorService executor=Executors.newFixedThreadPool(2);

	@Inject
	private Executor executor;

	@SuppressWarnings("finally")
	@POST
	@Path("/upload/xls/{rtype}/{gstin}/{monthYear}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadFile(MultipartFormDataInput input, @PathParam("gstin") String gstin,
			@PathParam("monthYear") String monthYear, @PathParam("rtype") String rtype) throws Exception {

		if (!(StringUtils.isEmpty(rtype) && StringUtils.isEmpty(gstin) && StringUtils.isEmpty(monthYear))) {

			log.info("uploadFile method starts for {} {} {}..", gstin, monthYear, rtype);

			Map<String, Object> respBuilder = new HashMap<>();
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			int count = 0;

			try {
				if (!finderService.isSubmit(gstin, monthYear, rtype)) {

					Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService
							.getReturnTransactionsByGstin(gstin, rtype);

					ObjectNode mapping = null;

					if (!StringUtils.isEmpty(input.getFormDataPart("mapping", String.class, null)))
						mapping = new ObjectMapper().readValue(input.getFormDataPart("mapping", String.class, null),
								ObjectNode.class);

					String delete = input.getFormDataPart("deleteall", String.class, null);
					if (Objects.isNull(delete))
						delete = "no";

					String provider = null;

					if (!StringUtils.isEmpty(input.getFormDataPart("provider", String.class, null)))
						provider = input.getFormDataPart("provider", String.class, null);
					String fileName = ManipulateFile.uploadMultipartFile(input);

					//
					if (StringUtils.isNotEmpty(provider) && SourceType.TALLY.toString().equalsIgnoreCase(provider))
						mapping = (ObjectNode) finderService.getSheetMapping(SourceType.valueOf(provider.toUpperCase()),
								ReturnType.valueOf(rtype.toUpperCase()));

					count = new GstExcel(new File(fileName)).uploadExcel(rtype, monthYear, mapping, provider,
							finderService.findTaxpayerGstin(gstin), returnTransactionDTOs, userBean, delete);

					log.info("uploadFile successfully uploaded file {}..", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
							String.format(ApplicationMetadata.EXCEL_UPLOAD, count));
				}
			} catch (AppException e) {
				log.error("uploadFile AppException exception ", e);
				respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			} catch (Exception e) {
				log.error("uploadFile Exception exception", e);
			} finally {
				log.info("uploadFile method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;
			}
		}
		return null;
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/save/outward/bills/{rtype}/{gstin}/{monthYear}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveOutwardBillsData(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			@PathParam("rtype") String rtype, String request) throws Exception {

		log.info("saveOutwardBillsData method starts for {} {} {}..", gstin, monthYear, rtype, request);
		int count;
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			count = returnService.processCompositeData(rtype, gstin, monthYear, request);
			if (count > 0) {

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("saveOutwardBillsData fail to process file {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("saveOutwardBillsData AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveOutwardBillsData Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("saveOutwardBillsData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally", "unchecked", "rawtypes" })
	@GET
	@Path("/get/brt/mapping/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBrtMapping(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("getBrtMapping method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> map = taxpayerService.getGstTransactionsByGstin(gstin);
			if (!Objects.isNull(map)) {
				SortedSet<UniversalReturnDTO> universalReturnDTOs = new TreeSet<>(
						Comparator.comparing(UniversalReturnDTO::getId));
				Iterator it = map.entrySet().iterator();
				while (it.hasNext()) {
					UniversalReturnDTO dto = new UniversalReturnDTO();
					Map.Entry pair = (Map.Entry) it.next();
					GstReturnDTO gstReturnDTO = (GstReturnDTO) pair.getKey();
					SortedSet<ReturnTransactionDTO> temp = (SortedSet<ReturnTransactionDTO>) pair.getValue();
					dto.setId(gstReturnDTO.getId());
					dto.setName(gstReturnDTO.getName());
					dto.setDescription(gstReturnDTO.getDescription());
					dto.setCode(gstReturnDTO.getCode());
					dto.setValue(temp);
					universalReturnDTOs.add(dto);
				}
				log.info("getBrtMapping successfully got mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, universalReturnDTOs);
			} else {
				log.info("getBrtMapping fail to retrieve mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getBrtMapping AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getBrtMapping Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getBrtMapping method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally", "unchecked", "rawtypes" })
	@GET
	@Path("/get/brt/mapping/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBrtMapping(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("getBrtMapping method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			TaxpayerGstinDTO taxpayerGstin = taxpayerService.getTaxpayerGstin(gstin,fyear);
			List<ReturnSummaryDTO> returnSummaryDTOs = returnService.getReturnSummaryDto(taxpayerGstin, monthYear);
			Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> map = taxpayerService.getGstTransactionsByGstin(gstin);
			if (!Objects.isNull(map)) {
				SortedSet<UniversalReturnDTO> universalReturnDTOs = new TreeSet<>(
						Comparator.comparing(UniversalReturnDTO::getId));
				Iterator it = map.entrySet().iterator();
				while (it.hasNext()) {
					UniversalReturnDTO dto = new UniversalReturnDTO();
					Map.Entry pair = (Map.Entry) it.next();
					GstReturnDTO gstReturnDTO = (GstReturnDTO) pair.getKey();
					SortedSet<ReturnTransactionDTO> temp = (SortedSet<ReturnTransactionDTO>) pair.getValue();
					dto.setId(gstReturnDTO.getId());
					dto.setName(gstReturnDTO.getName());
					dto.setDescription(gstReturnDTO.getDescription());
					dto.setCode(gstReturnDTO.getCode());

					ReturnSummaryDTO rs = new ReturnSummaryDTO();
					rs.setName(gstReturnDTO.getCode());
					if (returnSummaryDTOs.contains(rs)) {
						rs = returnSummaryDTOs.get(returnSummaryDTOs.indexOf(rs));
						dto.setStatus(rs.getStatus());
					}

					dto.setValue(temp);
					universalReturnDTOs.add(dto);
				}
				log.info("getBrtMapping successfully got mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, universalReturnDTOs);
			} else {
				log.info("getBrtMapping fail to retrieve mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getBrtMapping AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getBrtMapping Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getBrtMapping method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/toggle/gstn/flag/{index}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response toggleGstnFlag(@PathParam("index") String index) throws JsonProcessingException {

		log.info("toggleGstnFlag method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			flag = true;
			redisService.setValue("gstn_flag", index, false, 0);
			if (flag) {
				log.info("toggleGstnFlag successfully toggle the gstin flag for index {}", index);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, index);
			} else {
				log.info("toggleGstnFlag unable to toggle the gstin flag for index {}", index);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("toggleGstnFlag Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("toggleGstnFlag method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/returns/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnTransaction(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("getReturnTransaction method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Set<GstReturnDTO> returnDTOs = taxpayerService.getGstReturnByGstin(gstin);
			if (!Objects.isNull(returnDTOs)) {
				log.info("getReturnTransaction successfully got mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnDTOs);
			} else {
				log.info("getReturnTransaction fail to retrieve mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReturnTransaction AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReturnTransaction Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnTransaction method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/returns/v1.1/table/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnHistory(@PathParam("gstin") String gstin,
			@QueryParam("financialYear") String financialYear, @QueryParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("getReturnTransactionTable method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, List<ReturnHistoryDTO>> maps = returnService.getReturnHistoryByGstin(gstin, financialYear,
					monthYear);
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("returnHistory", maps);
			responseMap.put("financialYears", CalanderCalculatorUtility.getFinancialYearsFromGstStart());

			if (!Objects.isNull(responseMap)) {
				log.info("getReturnTransactionTable successfully got mapping table for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, responseMap);
			} else {
				log.info("getReturnTransactionTable fail to retrieve mapping table for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}

		} catch (Exception e) {
			log.error("getReturnTransactionTable Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnTransactionTable method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/returns/table/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnTransactionTable(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("getReturnTransactionTable method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> maps = taxpayerService.getGstReturnByFrequencyGstin(gstin);
			if (!Objects.isNull(maps)) {
				log.info("getReturnTransactionTable successfully got mapping table for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, maps);
			} else {
				log.info("getReturnTransactionTable fail to retrieve mapping table for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}

		} catch (Exception e) {
			log.error("getReturnTransactionTable Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnTransactionTable method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/return/transaction/{gstin}/{rtype}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnTransactionGstin(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype)
			throws JsonProcessingException {

		log.info("getTransactionGstin method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getReturnTransactionsByGstin(gstin,
					rtype);
			if (!Objects.isNull(returnTransactionDTOs)) {
				log.info("getTransactionGstin successfully got transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnTransactionDTOs);
			} else {
				log.info("getTransactionGstin fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactionGstin AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactionGstin Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getTransactionGstin method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/email/{recoType}/{gstin}/{rtype}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEmailContent(@PathParam("recoType") String recoType, @PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("type") String type, @PathParam("monthYear") String monthYear,
			JsonNode request) throws JsonProcessingException {

		log.info("getEmailContent method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		EmailDTO emailDTO = null;

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setRemark(recoType);
			String invoiceId = request.get("invoiceId").asText();
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			emailDTO = returnService.getEmailContent(gstinTransactionDTO, invoiceId);
			if (!Objects.isNull(emailDTO)) {
				log.info("getEmailContent successfully got email content for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, emailDTO);
			} else {
				log.info("getEmailContent fail to retrieve email content for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getEmailContent AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getEmailContent Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getEmailContent method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/*
	 * @SuppressWarnings("finally")
	 * 
	 * @GET
	 * 
	 * @Path("/get/reconsile/{gstin}/{rtype}/{type}/{monthYear}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Response
	 * getReconcile(@PathParam("gstin") String gstin, @PathParam("rtype") String
	 * rtype,
	 * 
	 * @PathParam("type") String type, @PathParam("monthYear") String monthYear)
	 * throws JsonProcessingException {
	 * 
	 * log.info("getReconcile method starts.."); Map<String, Object> respBuilder
	 * = new HashMap<>(); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._FAILURE);
	 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.FAILURE_MSG); String responseString = "";
	 * 
	 * try { GstinTransactionDTO gstinTransactionDTO = new
	 * GstinTransactionDTO(); gstinTransactionDTO.setMonthYear(monthYear);
	 * gstinTransactionDTO.setReturnType(rtype);
	 * gstinTransactionDTO.setTransactionType(type);
	 * gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(
	 * gstin)); String string =
	 * returnService.getTransactionData(gstinTransactionDTO); if
	 * (!Objects.isNull(string)) { log.info(CLASS_NAME +
	 * " - getReconcile successfully got transactions for {}..", gstin);
	 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._SUCCESS);
	 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.SUCCESS_MSG);
	 * respBuilder.put(ApplicationMetadata.RESP_DATA, string);
	 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
	 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } else { log.info(CLASS_NAME +
	 * " - getReconcile fail to retrieve transactions for {}..", gstin);
	 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._FAILURE);
	 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.FAILURE_MSG); } } catch (AppException e) { log.error(
	 * "getReconcile AppException exception {} ", e.getMessage());
	 * respBuilder.put(ApplicationMetadata.RESP_ERROR,
	 * AppException.getMessageJson(e.getMessage())); } catch (Exception e) {
	 * log.error("getReconcile Exception exception {} ", e.getMessage());
	 * e.printStackTrace(); } finally { responseString =
	 * JsonMapper.objectMapper.writeValueAsString(respBuilder); log.info(
	 * "getReconcile method ends.."); Response response =
	 * Response.status(200).entity(responseString).build(); return response; } }
	 */

	@SuppressWarnings("finally")
	@GET
	@Path("dashboard/summary/{type}/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDashboardSummary(@QueryParam("financialYear") String fyear,@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear, @PathParam("type") String type) throws JsonProcessingException {

		log.info("getDashboardSummary method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if(StringUtils.isEmpty(fyear))
				fyear=CalanderCalculatorUtility.getCurrentFinancialYear();
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(StringUtils.upperCase(type));
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			WrapperDashBoardSummaryDto response = returnService.getDashboardSummary(gstinTransactionDTO);

			for (Map.Entry<String, String> ent : response.getCurrentFiscal().entrySet()) {
				log.info("values of map {}", ent.getValue());
			}
			if (!Objects.isNull(response)) {
				log.info("getDashboardSummary successfully got return summary for {}.{}   {}.", gstin, rtype,
						monthYear);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, response);
			} else {
				log.info("getDashboardSummary fail to retrieve return summary for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getDashboardSummary AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getDashboardSummary Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getDashboardSummary method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/return/summary/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnSummary(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getReturnSummary method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			Set<ResponseDTO> response = taxpayerService.getReturnSummary(gstinTransactionDTO);
			if (!Objects.isNull(response)) {
				log.info("getReturnSummary successfully got return summary for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, response);
			} else {
				log.info("getReturnSummary fail to retrieve return summary for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReturnSummary AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReturnSummary Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnSummary method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * @param gstin
	 * @param rtype
	 * @param type
	 * @param monthYear
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 * 
	 *             this service is used to get data for all returns and
	 *             transactions
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/get/transaction/{gstin}/{rtype}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTransactions(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear, FilterDto request)
			throws JsonProcessingException {

		// if(searchStr != null)
		// return
		// Response.status(200).entity(returnService.b2bTextSearch(searchStr)).build();
		log.info("getTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.getTransactionData(gstinTransactionDTO, request);
			if (!Objects.isNull(datas)) {

				if (datas != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					boolean isSub = ! finderService.getSubmitDetails(gstin, monthYear, rtype);
					respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, isSub);
				}

				log.info("getTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactions AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactions Exception exception {} ", e);
			e.printStackTrace();
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/invoice/{gstin}/{rtype}/{type}/{monthYear}/{invoiceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoice(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear,
			@PathParam("invoiceId") String invoiceId) throws JsonProcessingException {

		log.info("getTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.getInvoiceData(type, invoiceId);
			if (!Objects.isNull(datas)) {

				if (datas != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, true);

					/*
					 * if (obj.has("summary")) {
					 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
					 * obj.get("summary")); } if (obj.has("isEdit")) {
					 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
					 * obj.get("isEdit")); }
					 */
				}

				log.info("getTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* searching service */
	@SuppressWarnings("finally")
	@PUT
	@Path("/search/invoice/{gstin}/{rtype}/{type}/{monthYear}/{searchString}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchInvoices(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear,
			@PathParam("searchString") String searchString, FilterDto filterDto) throws JsonProcessingException {

		// if(searchStr != null)
		// return
		// Response.status(200).entity(returnService.b2bTextSearch(searchStr)).build();
		log.info("searhInvoices method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.searchText(searchString, gstinTransactionDTO, filterDto);
			if (!Objects.isNull(datas)) {

				if (datas != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					boolean isSub = !finderService.getSubmitDetails(gstin, monthYear, rtype);
					respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, isSub);
				}

				log.info("searhInvoices successfully search transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("searhInvoices fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("searhInvoices AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("searhInvoices Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("searhInvoices method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* end searching service */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/mismatched/invoices/{gstin}/{rtype}/{type}/{monthYear}/{invoiceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMistatcedInvoices(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear,
			@PathParam("invoiceId") String invoiceId) throws JsonProcessingException {

		log.info("getMistatcedInvoices method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.getMismatchedInvoices(type, invoiceId);
			if (!Objects.isNull(datas)) {

				if (datas != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, true);

					/*
					 * if (obj.has("summary")) {
					 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
					 * obj.get("summary")); } if (obj.has("isEdit")) {
					 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
					 * obj.get("isEdit")); }
					 */
				}

				log.info("getTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@PUT
	@Path("/get/mismatchInvNo/invoices/{gstin}/{rtype}/{type}/{monthYear}/{invoiceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMistatcedInvNoInvoices(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear,
			@PathParam("invoiceId") String invoiceId, FilterDto filterDto) throws JsonProcessingException {

		log.info("getMistatcedInvNoInvoices method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.getMismatchedInvNoInvoices(type, invoiceId, filterDto);
			if (!Objects.isNull(datas)) {

				if (datas != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, true);

					/*
					 * if (obj.has("summary")) {
					 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
					 * obj.get("summary")); } if (obj.has("isEdit")) {
					 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
					 * obj.get("isEdit")); }
					 */
				}

				log.info("getMistatcedInvNoInvoices successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getMistatcedInvNoInvoices fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getMistatcedInvNoInvoicess AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getMistatcedInvNoInvoices Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getMistatcedInvNoInvoices method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstr3/transaction/{gstin}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstr3Transactions(@PathParam("gstin") String gstin, @PathParam("type") String type,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getGstr3Transactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			String string = returnService.getGstr3TransactionData(gstinTransactionDTO);
			if (!Objects.isNull(string)) {

				JSONObject obj = new JSONObject(string);
				if (obj.has("data")) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, obj.get("data"));
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					if (obj.has("summary")) {
						respBuilder.put(ApplicationMetadata.RESP_SUMMARY, obj.get("summary"));
					}

				}

				log.info("getGstr3Transactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getGstr3Transactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getGstr3Transactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getGstr3Transactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getGstr3Transactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* start temporary service to null itc eligibility */
	@SuppressWarnings("finally")
	@GET
	@Path("/set/itcnull/{gstin}/{rtype}/{type}/{monthYear}") // for itc bulk get
	@Produces(MediaType.APPLICATION_JSON)
	public Response setItcElgNull(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear) throws JsonProcessingException {
		log.info("getReconcileTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// String string =
			// returnService.getReconciledTransactionData(gstinTransactionDTO);
			boolean isUpdated = returnService.setItcElgNull(gstinTransactionDTO);

			if (isUpdated) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				long temp = Calendar.getInstance().getTimeInMillis();
				String lastTime = sdf.format(new Date(temp)) + " IST";

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				// respBuilder.put(ApplicationMetadata.RESP_DATA, items);
				respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);

				log.info("getReconcileTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getReconcileTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("getReconcileTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReconcileTransactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReconcileTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* end temporary service to null itc eligibility */

	/* start temporary service to null itc eligibility */
	@SuppressWarnings("finally")
	@GET
	@Path("/set/reconciletype/{subType}/{gstin}/{rtype}/{type}/{monthYear}") // for
																				// itc
																				// bulk
																				// get
	@Produces(MediaType.APPLICATION_JSON)
	public Response setReconcileTypeInvoices(@PathParam("subType") String subType, @PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("type") String type, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {
		log.info("getReconcileTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// String string =
			// returnService.getReconciledTransactionData(gstinTransactionDTO);
			boolean isUpdated = returnService.setReconcileType(gstinTransactionDTO,
					ReconsileType.valueOf(StringUtils.upperCase(subType)));

			if (isUpdated) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				long temp = Calendar.getInstance().getTimeInMillis();
				String lastTime = sdf.format(new Date(temp)) + " IST";

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				// respBuilder.put(ApplicationMetadata.RESP_DATA, items);
				respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);

				log.info("getReconcileTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getReconcileTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("getReconcileTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReconcileTransactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReconcileTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* end temporary service to null itc eligibility */
	/* start temporary service to compare transaction by ctin */
	@SuppressWarnings("finally")
	@GET
	@Path("/set/reconciletype/{subType}/{gstin}/{rtype}/{type}/{monthYear}/{ctin}") // for
	// itc
	// bulk
	// get
	@Produces(MediaType.APPLICATION_JSON)
	public Response compareTransactionByCtin(@PathParam("subType") String subType, @PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("type") String type, @PathParam("monthYear") String monthYear,
			@PathParam("ctin") String ctin) throws JsonProcessingException {
		log.info("getReconcileTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setCtin(ctin);
			;
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

			// String string =
			// returnService.getReconciledTransactionData(gstinTransactionDTO);
			boolean isUpdated = returnService.setReconcileType(gstinTransactionDTO,
					ReconsileType.valueOf(StringUtils.upperCase(subType)));

			if (isUpdated) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				long temp = Calendar.getInstance().getTimeInMillis();
				String lastTime = sdf.format(new Date(temp)) + " IST";

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				// respBuilder.put(ApplicationMetadata.RESP_DATA, items);
				respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);

				log.info("getReconcileTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getReconcileTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReconcileTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReconcileTransactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReconcileTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* end temporary service to null itc eligibility */

	@SuppressWarnings("finally")
	@POST
	@Path("/get/transaction/{subType}/{gstin}/{rtype}/{type}/{monthYear}") // for
																			// itc
																			// bulk
																			// get
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReconcileTransactions(@PathParam("subType") String subType, @PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("type") String type, @PathParam("monthYear") String monthYear,
			FilterDto filterDto) throws JsonProcessingException {
		log.info("getReconcileTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setStatus(subType);
			// String string =
			// returnService.getReconciledTransactionData(gstinTransactionDTO);
			Map<String, Object> items = returnService.getItcBulkItems(gstinTransactionDTO, subType, filterDto);
			ItcAmountDto itcAmounts = returnService.getItcSummary(gstinTransactionDTO, StringUtils.upperCase(type));

			if (!Objects.isNull(items)) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				long temp = Calendar.getInstance().getTimeInMillis();
				String lastTime = sdf.format(new Date(temp)) + " IST";

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, items);
				respBuilder.put(ApplicationMetadata.ITC_AMOUNTS, itcAmounts);

				respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);

				log.info("getReconcileTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getReconcileTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReconcileTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReconcileTransactions Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReconcileTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/transaction/forhsn/{gstin}/{rtype}/{type}/{monthYear}") // for
																		// itc
																		// bulk
																		// get
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTransactionsForHsn(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear, FilterDto filterDto)
			throws JsonProcessingException {
		log.info("getTransactionsForHsn method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// String string =
			// returnService.getReconciledTransactionData(gstinTransactionDTO);
			Map<String, Object> items = returnService.getBlankHsnBulkItems(gstinTransactionDTO, "FOR_HSN", filterDto);

			items.put("total_count",
					returnService.getBlankHsnCount(monthYear, gstin, rtype, type).get(type.toUpperCase()));

			// ItcAmountDto
			// itcAmounts=returnService.getItcSummary(gstinTransactionDTO,
			// StringUtils.upperCase(type));

			if (!Objects.isNull(items)) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				long temp = Calendar.getInstance().getTimeInMillis();
				String lastTime = sdf.format(new Date(temp)) + " IST";

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, items);
				// respBuilder.put(ApplicationMetadata.ITC_AMOUNTS, itcAmounts);

				respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);

				log.info("getTransactionsForHsn successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getTransactionsForHsn fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactionsForHsn AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactionsForHsn Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getTransactionsForHsn method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@POST
	@Path("/update/blankhsn/{itemId}")
	public Response rectifyBlankHSN(@PathParam("itemId") String itemId, ObjectNode request) {

		log.info("update empty hsn method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			

			String unit = null;
			if (request.has("unit"))
				unit = request.get("unit").asText();

			Double quantity = null;

			if (request.has("quantity") && StringUtils.isNotEmpty(request.get("quantity").asText()))
				quantity = Double.valueOf(request.get("quantity").asText());

			String hsnCode = null;
			if (request.has("hsnCode"))
				hsnCode = request.get("hsnCode").asText();

			String hsnDescription = null;
			if (request.has("hsnDescription"))
				hsnDescription = request.get("hsnDescription").asText();

			respBuilder.put(ApplicationMetadata.RESP_DATA,
					returnService.rectifyBlankHSN(itemId, null, hsnCode, hsnDescription, unit, quantity));
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		} catch (Exception e) {
			log.error(" Exception exception {} ", e);

		}
		//
		finally {
			log.info("update empty hsn method end.. data {}", respBuilder);

			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	@GET
	@Path("/get/blankhsn/count/{gstin}/{rtype}/{monthYear}") // for
	public Response getBlankHSNCount(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear) {

		log.info("get blank hsn count method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			respBuilder.put(ApplicationMetadata.RESP_DATA,
					returnService.getBlankHsnCount(monthYear, gstin, rtype, "ALL"));
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		} catch (Exception e) {
			log.error(" Exception exception {} ", e);

		} finally {
			log.info("get blank hsn count method end.. data {}", respBuilder);

			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	// service for combined itc

	/**
	 * @param subType
	 * @param gstin
	 * @param rtype
	 * @param type
	 * @param monthYear
	 * @param filterDto
	 * @return
	 * @throws JsonProcessingException
	 *             get itc data for all transaction by itc type
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/get/itctransaction/{subType}/{gstin}/{rtype}/{type}/{monthYear}") // for
																				// itc
																				// bulk
																				// get
	@Produces(MediaType.APPLICATION_JSON)
	public Response getItcItemsForAllTransaction(@PathParam("subType") String subType, @PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("type") String type, @PathParam("monthYear") String monthYear,
			FilterDto filterDto) throws JsonProcessingException {
		log.info("getItcItemsForAllTransaction method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setStatus(subType);
			// String string =
			// returnService.getReconciledTransactionData(gstinTransactionDTO);
			Map<String, Object> items = returnService.getItcBulkItems(gstinTransactionDTO, subType, filterDto);
			ItcAmountDto itcAmounts = returnService.getItcSummaryForAll(gstinTransactionDTO);

			if (!Objects.isNull(items)) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				long temp = Calendar.getInstance().getTimeInMillis();
				String lastTime = sdf.format(new Date(temp)) + " IST";
				items.put(ApplicationMetadata.ITC_AMOUNTS, itcAmounts);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, items);

				respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);

				log.info("getItcItemsForAllTransaction successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getItcItemsForAllTransaction fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getItcItemsForAllTransaction AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getItcItemsForAllTransaction Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getItcItemsForAllTransaction method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	// end

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/save/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTransaction(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, String request)
			throws JsonProcessingException {

		log.info("saveTransaction method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			// need to change below line while integrating with GSTN
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);

			GstExcel.checkReturnSubmitCondition(gstinTransactionDTO.getTaxpayerGstin().getId(), monthYear, rtype);

			JsonNode jsonNode = JsonMapper.objectMapper.readTree(request);
			// flag = returnService.saveTransactionData(gstinTransactionDTO);
			// flag = returnService.saveTransactionData(gstinTransactionDTO);

			if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type)) {
				flag = returnService.saveDocIssueTransactionData(gstinTransactionDTO);
			} else {
				gstinTransactionDTO.setUserBean(userBean);
				TransactionFactory transactionFactory = new TransactionFactory(
						gstinTransactionDTO.getTransactionType());

				Object ct = transactionFactory.saveTransactionData(gstinTransactionDTO, null);
				if (Objects.nonNull(ct))
					flag = (Boolean) ct;
				else
					flag = false;
			}

			if (flag == true) {

				/*
				 * String string =
				 * returnService.getTransactionData(gstinTransactionDTO); if
				 * (!Objects.isNull(string)) {
				 * 
				 * JSONObject obj = new JSONObject(string); if (obj.has("data"))
				 * { SimpleDateFormat sdf = new SimpleDateFormat(
				 * "dd MMM yyyy hh:mm:ss a"); long temp =
				 * Calendar.getInstance().getTimeInMillis(); String lastTime =
				 * sdf.format(new Date(temp)) + " IST";
				 * 
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA,
				 * obj.get("data"));
				 * respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP,
				 * lastTime); if (obj.has("summary")) {
				 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
				 * obj.get("summary")); } if (obj.has("isEdit")) {
				 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
				 * obj.get("isEdit")); }
				 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
				 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } }
				 */
				log.info("saveTransaction successfully added {}", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				if (TransactionType.DOC_ISSUE.toString().equalsIgnoreCase(type)) {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

				} else
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("saveTransaction fail to add..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("saveTransaction AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException | JsonProcessingException e) {
			log.error("saveTransaction JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveTransaction method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/genrate/hsnsum/{rtype}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response genrateHsnSummary(@PathParam("rtype") String rtype, @PathParam("gstin") String gstin,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("genrateHsnSum method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = true;

		try {YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		        String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			// need to change below line while integrating with GSTN
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(TransactionType.HSNSUM.toString());
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setUserBean(userBean);
			if (!finderService.isSubmit(gstinTransactionDTO)) {

				returnService.genrateHsnSummary(gstinTransactionDTO);
				if (flag == true) {

					log.info("hsnsum successfully genrated for gstin {}", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				} else {
					log.info("genrateHsnSum fail to genrate..");
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				}
			}
		} catch (AppException e) {
			log.error("genrateHsnSum AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, AppException.getMessage(e.getMessage()));
		} catch (JsonException e) {
			log.error("genrateHsnSum JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("genrateHsnSum method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/saveByNo/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTransactionByNo(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, String request)
			throws JsonProcessingException {

		log.info("saveTransactionByNo method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			// need to change below line while integrating with GSTN
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flag = returnService.updateTransactionDataModifyByNo(gstinTransactionDTO);
			if (flag == true) {
				/*
				 * String string =
				 * returnService.getTransactionData(gstinTransactionDTO); if
				 * (!Objects.isNull(string)) {
				 * 
				 * JSONObject obj = new JSONObject(string); if (obj.has("data"))
				 * { SimpleDateFormat sdf = new SimpleDateFormat(
				 * "dd MMM yyyy hh:mm:ss a"); long temp =
				 * Calendar.getInstance().getTimeInMillis(); String lastTime =
				 * sdf.format(new Date(temp)) + " IST";
				 * 
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA,
				 * obj.get("data"));
				 * respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP,
				 * lastTime); if (obj.has("summary")) {
				 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
				 * obj.get("summary")); } if (obj.has("isEdit")) {
				 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
				 * obj.get("isEdit")); }
				 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
				 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } }
				 * 
				 * log.info(
				 * "saveTransactionByNo successfully added the transaction number {}"
				 * , gstin); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 */
			} else {
				log.info("saveTransactionByNo fail to add the transaction number..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("saveTransactionByNo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, AppException.getMessage(e.getMessage()));
		} catch (JsonException e) {
			log.error("saveTransactionByNo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveTransactionByNo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/* save reconciled data */

	/**
	 * @param rtype
	 * @param type
	 * @param gstin
	 * @param monthYear
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 *             save bulk itc in all transactions
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/reconciled/save/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveReconciledTransaction(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, String request)
			throws JsonProcessingException {

		log.info("saveReconciledTransaction method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			// need to change below line while integrating with GSTN
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flag = returnService.updateReconciledTransactionData(gstinTransactionDTO);
			if (flag == true) {

				/*
				 * String string =
				 * returnService.getTransactionData(gstinTransactionDTO); if
				 * (!Objects.isNull(string)) {
				 * 
				 * JSONObject obj = new JSONObject(string); if (obj.has("data"))
				 * { SimpleDateFormat sdf = new SimpleDateFormat(
				 * "dd MMM yyyy hh:mm:ss a"); long temp =
				 * Calendar.getInstance().getTimeInMillis(); String lastTime =
				 * sdf.format(new Date(temp)) + " IST";
				 * 
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA,
				 * obj.get("data"));
				 * respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP,
				 * lastTime); if (obj.has("summary")) {
				 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
				 * obj.get("summary")); } if (obj.has("isEdit")) {
				 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
				 * obj.get("isEdit")); }
				 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
				 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } }
				 * 
				 * log.info(
				 * "saveReconciledTransaction successfully added the reconciled data{}"
				 * , gstin); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 */ } else {
				log.info("saveReconciledTransaction fail to add the reconciled data..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("saveReconciledTransaction AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, AppException.getMessage(e.getMessage()));
		} catch (JsonException e) {
			log.error("saveReconciledTransaction JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveReconciledTransaction method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/*
	 * delete transaction api rtpe :{gstr1,gstr2} type:{b2b.b2c..etc}
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/delete/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteTransaction(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, ObjectNode request)
			throws IOException, AppException {

		log.info("deleteTransaction method starts for {} {} {}..", rtype, gstin, monthYear);
		Map<String, Object> respBuilder = new HashMap<>();

		int flag = 0;

		String deleteType = request.get("deleteType").asText();

		ObjectMapper mapper = new ObjectMapper();

		List<String> invoices = null;

		try {

			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			if (request.has("invoices"))
				invoices = mapper.readValue(request.get("invoices").toString(), new TypeReference<List<String>>() {
				});

			flag = returnService.deleteTransactionData(gstin, monthYear, TransactionType.valueOf(type.toUpperCase()),
					ReturnType.valueOf(rtype), invoices, DeleteType.valueOf(deleteType), DataSource.EMGST);

			// logging user action
			MessageDto messageDto = new MessageDto();
			messageDto.setGstin(gstin);
			messageDto.setNoOfInvoices(String.valueOf(flag));
			messageDto.setTransactionType(type);
			messageDto.setReturnType(rtype);
			if (DeleteType.SELECTED.toString().equalsIgnoreCase(deleteType))
				apiLoggingService.saveAction(UserAction.DELETE_INVOICE, messageDto, gstin);
			else if (DeleteType.ALL.toString().equalsIgnoreCase(deleteType))
				apiLoggingService.saveAction(UserAction.DELETE_ALL_INVOICES, messageDto, gstin);

			// end logging user action

			log.info("deleteTransaction successfully deleted the transaction {}", gstin);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_DELETE);
			log.info("deleteTransaction method ends..");
			}
		} catch (AppException e) {
			log.error("updateTransactionMarkFlag AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			log.error("eaception", e);
		} catch (JsonException e) {
			log.error("updateTransactionMarkFlag JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			log.error("eaception", e);
		} catch (Exception e) {
			log.error("updateTransactionMarkFlag JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.FAILURE_MSG);
			log.error("eaception", e);
		} finally {
			log.info("updateTransactionMarkFlag method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/update/markflag/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateTransactionMarkFlag(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, String request)
			throws JsonProcessingException {

		log.info("updateTransactionMarkFlag method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flag = returnService.updateRecoTypeTransactionData(gstinTransactionDTO,
					TaxpayerAction.UPDATE_MARKER.toString());
			if (flag == true) {
				/*
				 * String string =
				 * returnService.getTransactionData(gstinTransactionDTO); if
				 * (!Objects.isNull(string)) {
				 * 
				 * JSONObject obj = new JSONObject(string); if (obj.has("data"))
				 * { SimpleDateFormat sdf = new SimpleDateFormat(
				 * "dd MMM yyyy hh:mm:ss a"); long temp =
				 * Calendar.getInstance().getTimeInMillis(); String lastTime =
				 * sdf.format(new Date(temp)) + " IST";
				 * 
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA,
				 * obj.get("data"));
				 * respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP,
				 * lastTime); if (obj.has("summary")) {
				 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
				 * obj.get("summary")); } if (obj.has("isEdit")) {
				 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
				 * obj.get("isEdit")); }
				 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
				 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } }
				 * 
				 * log.info(
				 * "updateTransactionMarkFlag successfully updated the transaction mark flag {}"
				 * , gstin); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 */
			} else {
				log.info("updateTransactionMarkFlag fail to update the transacrion mark flag..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
			}
		} catch (AppException e) {
			log.error("updateTransactionMarkFlag AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("updateTransactionMarkFlag JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("updateTransactionMarkFlag method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * @param recoType
	 * @param rtype
	 * @param type
	 * @param gstin
	 * @param monthYear
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 *             save itc claimend
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/save/{recoType}/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveBulkTransaction(@PathParam("recoType") String recoType, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			String request) throws JsonProcessingException {

		log.info("saveBulkTransaction method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flag = returnService.updateRecoTypeTransactionData(gstinTransactionDTO, recoType);
			if (flag == true) {

				/*
				 * String string =
				 * returnService.getTransactionData(gstinTransactionDTO); if
				 * (!Objects.isNull(string)) {
				 * 
				 * JSONObject obj = new JSONObject(string); if (obj.has("data"))
				 * { SimpleDateFormat sdf = new SimpleDateFormat(
				 * "dd MMM yyyy hh:mm:ss a"); long temp =
				 * Calendar.getInstance().getTimeInMillis(); String lastTime =
				 * sdf.format(new Date(temp)) + " IST";
				 * 
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA,
				 * obj.get("data"));
				 * respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP,
				 * lastTime); if (obj.has("summary")) {
				 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
				 * obj.get("summary")); } if (obj.has("isEdit")) {
				 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
				 * obj.get("isEdit")); }
				 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
				 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } } log.info(
				 * "saveBulkTransaction successfully added the bulk transaction {}"
				 * , gstin); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 */
			} else {
				log.info("saveBulkTransaction fail to add the bulk transaction..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}}
		} catch (AppException e) {
			log.error("saveBulkTransaction AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("saveBulkTransaction JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveBulkTransaction method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/saveByNo/{recoType}/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveBulkTransactionByNo(@PathParam("recoType") String recoType, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			String request) throws JsonProcessingException {

		log.info("saveBulkTransactionByNo method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flag = returnService.updateRecoTypeTransactionDataByNo(gstinTransactionDTO, recoType);
			if (flag == true) {

				/*
				 * String string =
				 * returnService.getTransactionData(gstinTransactionDTO); if
				 * (!Objects.isNull(string)) {
				 * 
				 * JSONObject obj = new JSONObject(string); if (obj.has("data"))
				 * { SimpleDateFormat sdf = new SimpleDateFormat(
				 * "dd MMM yyyy hh:mm:ss a"); long temp =
				 * Calendar.getInstance().getTimeInMillis(); String lastTime =
				 * sdf.format(new Date(temp)) + " IST";
				 * 
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA,
				 * obj.get("data"));
				 * respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP,
				 * lastTime); if (obj.has("summary")) {
				 * respBuilder.put(ApplicationMetadata.RESP_SUMMARY,
				 * obj.get("summary")); } if (obj.has("isEdit")) {
				 * respBuilder.put(ApplicationMetadata.RESP_IS_EDIT,
				 * obj.get("isEdit")); }
				 * respBuilder.put(ApplicationMetadata.RESP_KEYWORDS,
				 * ApplicationMetadata.RESP_KEYWORDS_VALUE); } }
				 * 
				 * log.info("saveBulkTransactionByNo successfully added {}",
				 * gstin); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 */
			} else {
				log.info("saveBulkTransactionByNo fail to add..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		} catch (AppException e) {
			log.error("saveBulkTransactionByNo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("saveBulkTransactionByNo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveBulkTransactionByNo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * @param rtype
	 * @param type
	 * @param gstin
	 * @param monthYear
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 *             following service is used to accept,recject,cancel the
	 *             invoices in gstr2
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/change/status/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeTransactionStatus(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, JsonNode request)
			throws JsonProcessingException {

		log.info("changeTransactionStatus method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// gstinTransactionDTO.setTransactionObject(request);

			List<ChangeStatusDto> invoices = JsonMapper.objectMapper.readValue(request.get("invoices").toString(),
					new TypeReference<List<ChangeStatusDto>>() {
					});
			flag = returnService.changeStatusTransactionData(gstinTransactionDTO, invoices);
			if (flag == true) {
				log.info("changeTransactionStatus successfully changed the transaction status {}", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("changeTransactionStatus Unable to change the transaction status..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		}
		catch (AppException e) {
			log.error("changeTransactionStatus AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException | JsonMappingException e) {
			log.error("changeTransactionStatus JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("changeTransactionStatus method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/missinginward/acceptallfiltered/{rtype}/{type}/{gstin}/{monthYear}/{ctin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptAllMissingInwardFilteredInvoices(@PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			@PathParam("ctin") String ctin, JsonNode request) throws JsonProcessingException {

		log.info("acceptAllMissingInwardFilteredInvoices method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// gstinTransactionDTO.setTransactionObject(request);

			String acceptType = "SINGLE";
			if (!Objects.isNull(request) && request.has("acceptType"))
				acceptType = request.get("acceptType").asText();

			flag = false;// returnService.acceptAllFilteredMissingInward(gstinTransactionDTO,
							// ctin,acceptType);

			if (flag == true) {
				log.info("acceptAllMissingInwardFilteredInvoices successfully changed the transaction status {}",
						gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("acceptAllMissingInwardFilteredInvoices Unable to change the transaction status..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}

		catch (AppException e) {
			log.error("acceptAllMissingInwardFilteredInvoices AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("acceptAllMissingInwardFilteredInvoices JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("acceptAllMissingInwardFilteredInvoices method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/performbulkoperation/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptAllMissingInwardFilteredInvoices(@PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			ChangeStatusDto detail) throws JsonProcessingException {

		log.info("acceptAllMissingInwardFilteredInvoices method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {				if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// gstinTransactionDTO.setTransactionObject(request);

			flag = returnService.executeBulkOperation(gstinTransactionDTO, detail);

			/*
			 * flag =
			 * returnService.acceptAllFilteredMissingInward(gstinTransactionDTO,
			 * ctin,acceptType);
			 */

			if (flag == true) {
				log.info("acceptAllMissingInwardFilteredInvoices successfully changed the transaction status {}",
						gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("acceptAllMissingInwardFilteredInvoices Unable to change the transaction status..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}}

		catch (AppException e) {
			log.error("acceptAllMissingInwardFilteredInvoices AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("acceptAllMissingInwardFilteredInvoices JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("acceptAllMissingInwardFilteredInvoices method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/save/bulkitc/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveBulkItc(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, JsonNode request)
			throws JsonProcessingException {

		log.info("saveBulkItc method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {				if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// gstinTransactionDTO.setTransactionObject(request);

			List<BulkItcDto> items = JsonMapper.objectMapper.readValue(request.get("items").toString(),
					new TypeReference<List<BulkItcDto>>() {
					});
			// ItcEligibilityType
			// elgType=ItcEligibilityType.valueOf(StringUtils.upperCase(request.get("elg_type").asText()));
			flag = returnService.saveBulkItcData(gstinTransactionDTO, items);
			if (flag == true) {
				log.info("saveBulkItc successfully changed the transaction status {}", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("saveBulkItc Unable to change the transaction status..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		}
		catch (AppException e) {
			log.error("saveBulkItc AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException | JsonMappingException e) {
			log.error("saveBulkItc JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveBulkItc method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/submit/gstr/{rtype}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitGstr(@PathParam("rtype") String rtype, @PathParam("gstin") String gstin,
			@PathParam("monthYear") String monthYear, String request) throws JsonProcessingException {

		log.info("submitGstr method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		GstrFlowDTO flowDTO = null;

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flowDTO = functionalService.submitGstr(gstinTransactionDTO);
			if (!Objects.isNull(flowDTO)) {
				log.info("submitGstr successfully submitted the gstr for  {}", gstin);
				if (flowDTO.isError()) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, flowDTO.getErrorMsg());
				} else {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA,
							String.format(ApplicationMetadata.GSTR_RETURN_SUBMIT, rtype, flowDTO.getTransactionId()));
				}
			} else {
				log.info("submitGstr fails to submit the gstr..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("submitGstr AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("submitGstr JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("submitGstr method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/generate/gstr/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response generateGstr(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			String request) throws JsonProcessingException {

		log.info("generateGstr method starts for {} {}..", gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		GstrFlowDTO flowDTO = null;

		try {				if (!finderService.isSubmit(gstin, monthYear,ReturnType.GSTR3.toString())) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(ReturnType.GSTR3.toString());
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			flowDTO = functionalService.generateGstr(gstinTransactionDTO);
			if (!Objects.isNull(flowDTO)) {
				log.info("generateGstr successfully generated the gstr {}", gstin);
				if (flowDTO.isError()) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, flowDTO.getErrorMsg());
				} else {
					log.info("generateGstr successfully added {}", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, String.format(ApplicationMetadata.GSTR_GENERATE,
							ReturnType.GSTR3.toString(), flowDTO.getTransactionId()));
				}
			} else {
				log.info("generateGstr fail to add..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}}
		} catch (AppException e) {
			log.error("generateGstr AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("generateGstr JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("generateGstr method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/file/gstr/{rtype}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fileGstr(@PathParam("rtype") String rtype, @PathParam("gstin") String gstin,
			@PathParam("monthYear") String monthYear, String request) throws JsonProcessingException {

		log.info("fileGstr method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		GstrFlowDTO flowDTO = null;

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionObject(request);
			// flowDTO = functionalService.fileGstr(gstinTransactionDTO);
			if (!Objects.isNull(flowDTO)) {
				log.info("fileGstr successfully completed {}", gstin);
				if (flowDTO.isError()) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, flowDTO.getErrorMsg());
				} else {
					log.info("fileGstr successfully added {}", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA,
							String.format(ApplicationMetadata.GSTR_RETURN_FILED, rtype, flowDTO.getTransactionId()));
				}
			} else {
				log.info("fileGstr fail to add..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("fileGstr AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("fileGstr JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("fileGstr method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/send/email")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendEmail(String request) throws JsonProcessingException {

		log.info("sendEmail method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			EmailDTO emailDTO = JsonMapper.objectMapper.readValue(request, EmailDTO.class);

			EmailFactory emailFactory = new EmailFactory();
			emailFactory.sendEmailUsingEmailDTO(emailDTO);
			if (!Objects.isNull(emailDTO)) {
				log.info("sendEmail email has been sent successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_EMAIL);
			} else {
				log.info("sendEmail unable to send the email");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("sendEmail Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("sendEmail method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstr/summary/{rtype}/{gstin}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstrSummary(@PathParam("rtype") String rtype, @PathParam("gstin") String gstin,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getGstrSummary method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

			Map<String, Object> gstrSummaryDTO = null;
			if (type.equalsIgnoreCase("all")) {
				gstrSummaryDTO = redisService.getMapStringObject(gstin + "-" + monthYear + "-" + rtype);
			} else if (type.equalsIgnoreCase("allnew")) {
				type = "all";
				gstinTransactionDTO.setTransactionType("all");
			}

			if (Objects.isNull(gstrSummaryDTO)) {

				// calling procedure compareB2bSimmilarNonSimmilar
				if (rtype.equalsIgnoreCase("gstr2") && monthYear.equalsIgnoreCase("072017")
						&& StringUtils.isEmpty(redisService.getValue("procsimnosim-" + gstin))) {
					int count = returnService.compareSimmilarNonSimilar(gstinTransactionDTO);
					log.info("similar/no simmilar comparison via procedure end {}", count);
					redisService.setValue("procsimnosim-" + gstin, "YES", false, 0);
				}
				// end calling compareB2bSimmilarNonSimmilar procedure

				gstrSummaryDTO = returnService.getTransactionSummary(gstinTransactionDTO);
				if (type.equalsIgnoreCase("all")) {
					redisService.setMapStringObject(gstin + "-" + monthYear + "-" + rtype, gstrSummaryDTO, true, 7200);
				}
			}

			respBuilder.put("gstr2ASumm", returnService.getGstr2ASumm(gstinTransactionDTO));
			Set<ResponseDTO> response = taxpayerService.getReturnSummary(gstinTransactionDTO);
			Map<String, Object> lastGstnSynced = returnService.getLastSyncWithGstn(gstinTransactionDTO);

			if (!Objects.isNull(gstrSummaryDTO)) {

				Map<String, Object> gtInfo = new HashMap<>();
				FilePrefernce fileprefernce = finderService.findpreference(gstinTransactionDTO.getTaxpayerGstin().getGstin(), fyear);
				gtInfo.put("previousGt",fileprefernce.getPreviousGt());
				gtInfo.put("currentGt", fileprefernce.getCurrentGt());
				log.info("getGstrSummary successfully completed for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, gstrSummaryDTO);
				respBuilder.put("gt_info", gtInfo);
				respBuilder.put("isConsolidate",finderService.findProvider(userBean.getUserDto().getOrganisation().getId()));
				respBuilder.put(ApplicationMetadata.SYNC_WITH_GSTN_TIMINGS, lastGstnSynced);
				boolean isSub = ! finderService.getSubmitDetails(gstin, monthYear, rtype);
				respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, isSub);

				if (TransactionType.ALL.toString().equalsIgnoreCase(gstinTransactionDTO.getTransactionType())
						&& ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.getReturnType()))
					respBuilder.put("itc_amount", returnService.getItcSummaryForAll(gstinTransactionDTO));

				if (!Objects.isNull(response)) {
					ResponseDTO responseDto = new ResponseDTO();
					if (!Objects.isNull(gstrSummaryDTO.get("isUploaded"))
							&& (boolean) gstrSummaryDTO.get("isUploaded")) {
						responseDto.setKey(StringUtils.upperCase(rtype) + "_UPLOAD");
						responseDto.setValue("YES");
						response.add(responseDto);
					}
					/*
					 * else{
					 * responseDto.setKey(StringUtils.upperCase(rtype)+"_UPLOAD"
					 * ); responseDto.setValue("NO"); }
					 */
					respBuilder.put(ApplicationMetadata.RESP_RET_SUMM, response);
				}

				/*
				 * respBuilder.put(ApplicationMetadata.RESP_RET_SUMM, response);
				 * respBuilder.put(ApplicationMetadata.RESP_RET_TRANS,
				 * returnTransactionDTOs);
				 */

				SortedSet<UniversalReturnDTO> uniReturnDTOs = appConfig.getUniversalReturnDTO();
				if (Objects.isNull(uniReturnDTOs)) {
					Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> map1 = taxpayerService
							.getGstTransactionsByGstin(gstin);
					if (!Objects.isNull(map1)) {
						SortedSet<UniversalReturnDTO> universalReturnDTOs = new TreeSet<>(
								Comparator.comparing(UniversalReturnDTO::getId));
						Iterator it = map1.entrySet().iterator();
						while (it.hasNext()) {
							UniversalReturnDTO dto = new UniversalReturnDTO();
							Map.Entry pair = (Map.Entry) it.next();
							GstReturnDTO gstReturnDTO = (GstReturnDTO) pair.getKey();
							SortedSet<ReturnTransactionDTO> temp = (SortedSet<ReturnTransactionDTO>) pair.getValue();
							dto.setId(gstReturnDTO.getId());
							dto.setName(gstReturnDTO.getName());
							dto.setDescription(gstReturnDTO.getDescription());
							dto.setCode(gstReturnDTO.getCode());
							dto.setValue(temp);
							universalReturnDTOs.add(dto);
						}
						log.info("getBrtMapping successfully got mapping for {}..", gstin);
						respBuilder.put("brtmap", universalReturnDTOs);
						appConfig.setUniversalReturnDTO(universalReturnDTOs);
					}
				} else {
					log.info("getBrtMapping successfully got from redis {}..", gstin);
					respBuilder.put("brtmap", uniReturnDTOs);
				}

			} else {
				log.info("getGstrSummary fail to retrieve for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getGstrSummary AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getGstrSummary Exception exception {} ", e);
			e.printStackTrace();
		} finally {
			log.info("getGstrSummary method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/sync/data/{gstin}/{rtype}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	@Asynchronous
	public void syncDataFromGstn(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear,
			@Suspended final AsyncResponse asyncResponse) throws JsonProcessingException, AppException {

		log.info("syncDataFromGstn method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		Map<String, String> tempResp = new HashMap<>();

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			boolean flag = false;
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);

			TaxpayerGstinDTO dto = new TaxpayerGstinDTO();
			dto.setGstin(gstin);
			// gstinTransactionDTO.setTaxpayerGstin(dto);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			
			if(!"09AAHCR8454M1ZL".equalsIgnoreCase(gstin) && !"09AAACF8356R2ZS".equalsIgnoreCase(gstin)) {
			securityManager.checkTrialPeriodConstraint();
			securityManager.checkIfSaveNotAllowedForGstin(gstinTransactionDTO);
			}

			//
			/*
			 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
			 * ExceptionCode._SUCCESS);
			 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
			 * ApplicationMetadata.SAVE_TO_GSTN_IS_IN_PROGRESS);
			 * executor.execute(new Runnable() {
			 * 
			 * @Override public void run() { Map<String,Object>response=new
			 * HashMap<>();; try { log.error(
			 * "save data to gstn executor for gstn {},monthYear {}, returnType {}"
			 * , gstinTransactionDTO.getTaxpayerGstin().getGstin(),
			 * gstinTransactionDTO.getMonthYear(),
			 * gstinTransactionDTO.getReturnType());
			 * response.putAll(returnService.syncData(gstinTransactionDTO));
			 * log.debug("sync result {}"
			 * ,StringUtils.join(response.values(),"|")); } catch (Exception e)
			 * { log.error(
			 * "error in save data to gstn for gstn {},monthYear {}, returnType {},error {}"
			 * , gstinTransactionDTO.getTaxpayerGstin().getGstin(),
			 * gstinTransactionDTO.getMonthYear(),
			 * gstinTransactionDTO.getReturnType(), e.getMessage()); log.error(
			 * "error occured",e);
			 * 
			 * } log.debug("end saveDataToGstn"); } });
			 */
			//

			/*
			 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
			 * ExceptionCode._SUCCESS);
			 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
			 * ApplicationMetadata.SAVE_TO_GSTN_IS_IN_PROGRESS);
			 */

			tempResp = returnService.syncData(gstinTransactionDTO);

			log.info("syncDataFromGstn successfully got mapping for {}..", gstin);
			respBuilder.putAll(tempResp);
			}
		} catch (AppException e) {
			log.error("syncDataFromGstn AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("syncDataFromGstn Exception exception {} ", e);
			e.printStackTrace();
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.FAILURE_MSG);
		} finally {
			log.info("syncDataFromGstn method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			asyncResponse.resume(response);

		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/return/status/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkReturnStatus(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException, AppException {

		log.info("checkReturnStatus method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			TaxpayerGstinDTO dto = new TaxpayerGstinDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			dto.setGstin(gstin);
			gstinTransactionDTO.setTaxpayerGstin(dto);
			GetCheckStatusResp returnStatus = functionalService.checkReturnStatus(gstinTransactionDTO);
			if (!Objects.isNull(returnStatus)) {
				log.info("checkReturnStatus successfully got mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatus.getStatus());
			} else {
				log.info("checkReturnStatus fail to retrieve mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("checkReturnStatus Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("checkReturnStatus method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@GET
	@Path("/hsn")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHSNSUM() {
		return Response.status(200).entity(returnService.generateHSNSUM()).build();

	}

	@SuppressWarnings("finally")
	@GET
	@Path("/sync/data/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response syncDataFromGstnAll(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException, AppException {

		log.info("syncDataFromGstnAll method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		Map<String, String> tempResp = new HashMap<>();

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {				if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			boolean flag = false;
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);

			TaxpayerGstinDTO dto = new TaxpayerGstinDTO();
			dto.setGstin(gstin);
			// gstinTransactionDTO.setTaxpayerGstin(dto);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

			if(!"09AAHCR8454M1ZL".equalsIgnoreCase(gstin) && !"09AAACF8356R2ZS".equalsIgnoreCase(gstin)) {
			securityManager.checkTrialPeriodConstraint();
			securityManager.checkIfSaveNotAllowedForGstin(gstinTransactionDTO);
			}
			//
			/*
			 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
			 * ExceptionCode._SUCCESS);
			 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
			 * ApplicationMetadata.SAVE_TO_GSTN_IS_IN_PROGRESS);
			 * executor.execute(new Runnable() {
			 * 
			 * @Override public void run() { Map<String,Object>response=new
			 * HashMap<>();; try { log.error(
			 * "save data to gstn executor for gstn {},monthYear {}, returnType {}"
			 * , gstinTransactionDTO.getTaxpayerGstin().getGstin(),
			 * gstinTransactionDTO.getMonthYear(),
			 * gstinTransactionDTO.getReturnType());
			 * response.putAll(returnService.syncData(gstinTransactionDTO));
			 * log.debug("sync result {}"
			 * ,StringUtils.join(response.values(),"|")); } catch (Exception e)
			 * { log.error(
			 * "error in save data to gstn for gstn {},monthYear {}, returnType {},error {}"
			 * , gstinTransactionDTO.getTaxpayerGstin().getGstin(),
			 * gstinTransactionDTO.getMonthYear(),
			 * gstinTransactionDTO.getReturnType(), e.getMessage()); log.error(
			 * "error occured",e);
			 * 
			 * } log.debug("end saveDataToGstn"); } });
			 */
			//

			tempResp = returnService.syncData(gstinTransactionDTO);
			log.info("syncDataFromGstn successfully got mapping for {}..", gstin);
			respBuilder.putAll(tempResp);
		}
		} catch (AppException e) {
			log.error("syncDataFromGstn AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("syncDataFromGstn Exception exception {} ", e);
			e.printStackTrace();
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.FAILURE_MSG);
		} finally {
			log.info("syncDataFromGstn method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/process/returnstatus/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response processReturnStatus(@PathParam("id") String id) throws JsonProcessingException, AppException {

		log.info("processReturnStatus method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		Map<String, String> tempResp = new HashMap<>();

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			tempResp = returnService.processReturnStatus(id);
			if (!Objects.isNull(tempResp)) {
				log.info("processReturnStatus successfully got mapping for {}..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, tempResp.get(ApplicationMetadata.RESP_MSG_KEY));
			} else {
				log.info("processReturnStatus fail to retrieve mapping for {}..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("processReturnStatus AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("processReturnStatus Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("processReturnStatus method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/returnstatus/{gstin}/{monthYear}/{returnType}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnStatusAllByGstn(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			@PathParam("returnType") String returnType, FilterDto filter) throws JsonProcessingException, AppException {

		log.info("getReturnStatusAllByGstn method starts..");

		Map<String, Object> respBuilder = new HashMap<>();

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstnTransactionDTO = new GstinTransactionDTO();
			gstnTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstnTransactionDTO.setMonthYear(monthYear);
			gstnTransactionDTO.setReturnType(returnType);
			Map<String, Object> returnStatusList = returnService.getFilteredReturnStatusAllByGstn(gstnTransactionDTO,
					filter);
			// List<ReturnStatus> returnStatusList =
			// returnService.getReturnStatusAllByGstn(gstnTransactionDTO);
			if (!Objects.isNull(returnStatusList)) {
				log.info("getReturnStatusAllByGstn successfully got mapping for {}..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatusList);

			} else {
				log.info("getReturnStatusAllByGstn fail to retrieve mapping for {}..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReturnStatusAllByGstn AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReturnStatusAllByGstn Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnStatusAllByGstn method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/filedet/{gstin}/{rtype}/{monthYear}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFileDetails(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			@PathParam("rtype") String rtype, String request) throws Exception {

		log.info("getFileDetails method starts for {} {} {}..", gstin, monthYear, rtype, request);
		int count;
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			count = returnService.saveFileDetails(rtype, gstin, monthYear, request);
			if (count > 0) {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, finderService.findTaxpayerGstin(gstin).getMonthYear());
			} else {
				log.info("getFileDetails fail to process file {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getFileDetails AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getFileDetails Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getFileDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/details/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstr3Details(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException, AppException {

		log.info("getGstr3Details method starts..");

		Map<String, Object> respBuilder = new HashMap<>();

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstnTransactionDTO = new GstinTransactionDTO();
			gstnTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstnTransactionDTO.setMonthYear(monthYear);
			GetGstr3Resp returnStatusList = functionalService.getGstr3Details(gstnTransactionDTO);
			if (!Objects.isNull(returnStatusList)) {
				log.info("getGstr3Details successfully got mapping for {}..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatusList);

			} else {
				log.info("getGstr3Details fail to retrieve mapping for {}..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getGstr3Details AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getGstr3Details Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getGstr3Details method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/submit/liabilities")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitGstr3Liailities() throws Exception {

		log.info("submitGstr3Liailities method starts for {} {} {}..");
		int count = 0;
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			// count = functionalService.submitGstr3Liabilities();
			if (count > 0) {

				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		// } catch (AppException e) {
		// log.error("getFileDetails AppException exception {} ",
		// e.getMessage());
		// respBuilder.put(ApplicationMetadata.RESP_ERROR,
		// e.getMessageJsonFromObject(e.getCode()));
		// }
		catch (Exception e) {
			log.error("getFileDetails Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getFileDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/setoff/liabilities/{gstin}/{monthYear}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setoffLiailities(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws Exception {

		log.info("setoffLiailities method starts for {} {} {}..", gstin, monthYear);
		GetGstr3Resp getgstr3Resp;
		Map<String, Object> respBuilder = new HashMap<>();
		YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);

		GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		gstinTransactionDTO.setMonthYear(monthYear);
		gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
		gstinTransactionDTO.setReturnType(ReturnType.GSTR3.toString());

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			getgstr3Resp = functionalService.setOffGstr3Liability(gstinTransactionDTO);
			if (!Objects.isNull(getgstr3Resp)) {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("setoffLiailities AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("setoffLiailities Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getFileDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/submit/refund/{gstin}/{monthYear}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitRefund(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws Exception {

		log.info("submitRefund method starts for {} {} {}..", gstin, monthYear);
		GetGstr3Resp getgstr3Resp;
		Map<String, Object> respBuilder = new HashMap<>();
		YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
		String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
		GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		gstinTransactionDTO.setMonthYear(monthYear);
		gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
		gstinTransactionDTO.setReturnType(ReturnType.GSTR3.toString());

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			getgstr3Resp = functionalService.submitRefund(gstinTransactionDTO);
			if (!Objects.isNull(getgstr3Resp)) {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("submitRefund AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("submitRefund Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("submitRefund method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/datastatus/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDataStatusGstn(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException, AppException {

		log.info("getDataStatusGstn method starts..");

		Map<String, Object> respBuilder = new HashMap<>();

		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstnTransactionDTO = new GstinTransactionDTO();
			gstnTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstnTransactionDTO.setMonthYear(monthYear);
			gstnTransactionDTO.setReturnType("GSTR2A");
			List<DataStatusDTO> returnStatusList = returnService.getDataStatusGstn(gstnTransactionDTO);
			Map<String, Object> lastGstnSynced = returnService.getLastSyncWithGstn(gstnTransactionDTO);
			// if (!Objects.isNull(returnStatusList))
			log.info("getReturnStatusAllByGstn successfully got mapping for {}..");
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatusList);
			respBuilder.put(ApplicationMetadata.SYNC_WITH_GSTN_TIMINGS, lastGstnSynced);
			// } else {
			// log.info("getReturnStatusAllByGstn fail to retrieve mapping for
			// {}..");
			// respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
			// ExceptionCode._FAILURE);
			// respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
			// ApplicationMetadata.FAILURE_MSG);
			// }
		} catch (AppException e) {
			log.error("getReturnStatusAllByGstn AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReturnStatusAllByGstn Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnStatusAllByGstn method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/search/taxpayer/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchTaxPayer(@PathParam("gstin") String gstin) throws JsonProcessingException, AppException {

		log.info("searchTaxPayer method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			TaxpayerGstinDTO dto = new TaxpayerGstinDTO();
			dto.setGstin(gstin);
			gstinTransactionDTO.setTaxpayerGstin(dto);
			SearchTaxpayerDTO searchTaxpayerDTO = returnService.searchTaxPayer(gstinTransactionDTO);
			if (!Objects.isNull(searchTaxpayerDTO)) {
				log.info("searchTaxPayer successfully got mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, searchTaxpayerDTO);
			} else {
				log.info("searchTaxPayer fail to retrieve mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("searchTaxPayer Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("searchTaxPayer method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@POST
	@Path("/get/missinginward/{missingType}/{gstin}/{returnType}/{transactionType}/{monthYear}")
	public Response getMissingInwardInvoices(@PathParam("monthYear") String monthYear, @PathParam("gstin") String gstin,
			@PathParam("returnType") String returnType, @PathParam("transactionType") String transactionType,
			@PathParam("missingType") String missingType, FilterDto filter) {

		// if(searchStr != null)
		// return
		// Response.status(200).entity(returnService.b2bTextSearch(searchStr)).build();
		log.info("getTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(returnType);
			gstinTransactionDTO.setTransactionType(transactionType);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> resp = null;

			resp = returnService.getMissingInvoices(gstinTransactionDTO, missingType, filter);

			if (!Objects.isNull(resp)) {

				if (resp != null) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, resp);
				}

				log.info("getTransactions successfully got transactions for {}..", gstin);

			} else {
				log.info("getTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactions AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactions Exception exception {} ", e);
			e.printStackTrace();
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/mismatch/transaction/{gstin}/{rtype}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMismatchTransactions(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("type") String type, @PathParam("monthYear") String monthYear, FilterDto request)
			throws JsonProcessingException {

		// if(searchStr != null)
		// return
		// Response.status(200).entity(returnService.b2bTextSearch(searchStr)).build();
		log.info("getMismatchTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.getMismatchTransaction(gstinTransactionDTO, null, request);
			if (!Objects.isNull(datas)) {

				if (datas != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";

					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
					respBuilder.put(ApplicationMetadata.RESP_TIMESTAMP, lastTime);
					boolean isSub = !finderService.getSubmitDetails(gstin, monthYear, rtype);
					respBuilder.put(ApplicationMetadata.RESP_IS_EDIT, isSub);
				}

				log.info("getMismatchTransactions successfully got transactions for {}..", gstin);

				respBuilder.put(ApplicationMetadata.RESP_KEYWORDS, ApplicationMetadata.RESP_KEYWORDS_VALUE);
			} else {
				log.info("getMismatchTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getMismatchTransactions AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getMismatchTransactions Exception exception {} ", e);
			e.printStackTrace();
		} finally {
			log.info("getMismatchTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@POST
	@Path("/get/similar/ctininvoices/{gstin}/{returnType}/{transactionType}/{monthYear}")
	public Response getMatchedCtinInvoices(@PathParam("monthYear") String monthYear, @PathParam("gstin") String gstin,
			@PathParam("returnType") String returnType, @PathParam("transactionType") String transactionType,
			CtinInvoiceDto ctinInvoiceDto) {

		// if(searchStr != null)
		// return
		// Response.status(200).entity(returnService.b2bTextSearch(searchStr)).build();
		log.info("getTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(returnType);
			gstinTransactionDTO.setTransactionType(transactionType);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// FilterDto filter=JsonMapper.objectMapper.readValue(request,
			// FilterDto.class);
			Map<String, Object> datas = returnService.getSimilarCtinMissingOutward(gstinTransactionDTO, ctinInvoiceDto);

			if (!Objects.isNull(datas)) {

				if (datas != null) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, datas);
				}

				log.info("getTransactions successfully got transactions for {}..", gstin);

			} else {
				log.info("getTransactions fail to retrieve transactions for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getTransactions AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getTransactions Exception exception {} ", e);
			e.printStackTrace();
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@GET
	@Path("/copymatchitem/{gstin}/{returnType}/{transactionType}/{monthYear}")
	public Response getMatchedCtinInvoices(@PathParam("monthYear") String monthYear, @PathParam("gstin") String gstin,
			@PathParam("returnType") String returnType, @PathParam("transactionType") String transactionType) {

		// if(searchStr != null)
		// return
		// Response.status(200).entity(returnService.b2bTextSearch(searchStr)).build();
		log.info("getTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {

			returnService.copyAcceptedInvItems(gstin, monthYear, returnType, transactionType);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		} catch (Exception e) {

			log.error("copy accepted AppException exception {} ", e);
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/change/ctin/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeCtinByInvoiceId(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, ChangeStatusDto request)
			throws JsonProcessingException {

		log.info("changeCtinByInvoiceId method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {				if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// gstinTransactionDTO.setTransactionObject(request);

			/*
			 * List<ChangeStatusDto> invoices =
			 * JsonMapper.objectMapper.readValue(request.get("invoices").
			 * toString(), new TypeReference<List<ChangeStatusDto>>() { });
			 */
			flag = returnService.changeCtinByInvoiceId(gstinTransactionDTO, request);
			if (flag == true) {
				log.info("changeCtinByInvoiceId successfully changed the transaction status {}", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("changeCtinByInvoiceId Unable to change the transaction status..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		}
		catch (AppException e) {
			log.error("changeCtinByInvoiceId AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("changeCtinByInvoiceId JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("changeCtinByInvoiceId method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/transaction/acceptwithitc/{rtype}/{type}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptMissingInwardWithItc(@PathParam("rtype") String rtype, @PathParam("type") String type,
			@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear, AcceptWithItcDto request)
			throws JsonProcessingException {

		log.info("acceptMissingInwardWithItc method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {				if (!finderService.isSubmit(gstin, monthYear, rtype)) {

			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			// gstinTransactionDTO.setTransactionObject(request);

			/*
			 * List<ChangeStatusDto> invoices =
			 * JsonMapper.objectMapper.readValue(request.get("invoices").
			 * toString(), new TypeReference<List<ChangeStatusDto>>() { });
			 */
			flag = returnService.acceptMissingInwardWithItc(gstinTransactionDTO, request);
			if (flag == true) {
				log.info("acceptMissingInwardWithItc successfully changed the transaction status {}", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("acceptMissingInwardWithItc Unable to change the transaction status..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		}
		catch (AppException e) {
			log.error("acceptMissingInwardWithItc AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("acceptMissingInwardWithItc JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("acceptMissingInwardWithItc method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@GET
	@Path("/copycheck/{gstin}/{returnType}/{transactionType}/{monthYear}")
	public Response copyChecksum(@PathParam("monthYear") String monthYear, @PathParam("gstin") String gstin,
			@PathParam("returnType") String returnType, @PathParam("transactionType") String transactionType) {

		log.info("getTransactions method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(returnType);
			gstinTransactionDTO.setTransactionType(transactionType);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

			returnService.copyChecksumToEMG(gstinTransactionDTO);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		} catch (Exception e) {

			log.error("copy accepted AppException exception {} ", e);
		} finally {
			log.info("getTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	// ************temp method to check gstr3b status
	@SuppressWarnings("finally")
	@GET
	@Path("/return/status/gstr3b/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkReturnStatusGstr3b(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException, AppException {

		log.info("checkReturnStatus method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			TaxpayerGstinDTO dto = new TaxpayerGstinDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTransactionType("GSTR3B");

			dto.setGstin(gstin);
			gstinTransactionDTO.setTaxpayerGstin(dto);
			List<ReturnStatusDTO> returnStatuss = functionalService.checkReturnStatusByTransaction(gstinTransactionDTO,
					"GSTR3B");
			if (!Objects.isNull(returnStatuss)) {
				log.info("checkReturnStatus successfully got mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatuss);
			} else {
				log.info("checkReturnStatus fail to retrieve mapping for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("checkReturnStatus Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("checkReturnStatus method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	// *********end temp menthod

	// get gstr3b

	/**
	 * @param gstin
	 * @param monthYear
	 * @return
	 * @throws JsonProcessingException
	 * 
	 *             get data of gstr3b as per gstin and month year
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstr3b/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstr3b(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("saveGstr3b method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = false;
			String tempResponse = "";
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

			Gstr3bDto data = returnService.getGstr3BDataForView(gstinTransactionDTO);
			gstinTransactionDTO.setReturnType(ReturnType.GSTR3B.toString());
			Map<String, Object> lastGstnSynced = returnService.getLastSyncWithGstn(gstinTransactionDTO);
			respBuilder.put(ApplicationMetadata.SYNC_WITH_GSTN_TIMINGS, lastGstnSynced);

			if (!Objects.isNull(data)) {
				log.info("saveGstr3b successfully got the gstr1 data {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, data);

			} else {
				log.info("saveGstr3b fail to retrieve teh gstr1 data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, data);

			}
		} catch (AppException e) {
			log.error("saveGstr3b AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveGstr3b Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("saveGstr3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	// end get gstr3b

	@SuppressWarnings("finally")
	@POST
	@Path("/save/gstr3b/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveGstr3b(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,
			JsonNode request) throws JsonProcessingException {

		log.info("saveGstr3b method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {				if (!finderService.isSubmit(gstin, monthYear, "gstr3b")) {

			boolean flag = false;
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

			flag = returnService.updateGstr3b(gstinTransactionDTO, request);

			if (flag) {
				log.info("saveGstr3b successfully got the gstr1 data {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("saveGstr3b fail to retrieve teh gstr1 data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}
		} catch (AppException e) {
			log.error("saveGstr3b AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveGstr3b Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("saveGstr3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	// end get gstr3b
	
	
	@POST
	@Path("/upload/consolidated/xls/{datauploadInfoId}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadConsolidatedExcel(MultipartFormDataInput input, @PathParam("datauploadInfoId") String datauploadInfoId) {
		
		if (!StringUtils.isEmpty(datauploadInfoId)) {

			log.info("uploadFile method starts for {}",datauploadInfoId);

			Map<String, Object> respBuilder = new HashMap<>();
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			int count = 0;

			try {
				
					String filePath = ManipulateFile.uploadMultipartFile(input);
					
					String filename=ManipulateFile.getUploadedFileName(input);
					
                    DataUploadInfo dataUploadInfo=taxpayerService.consolidatedExcelUploadInfo(datauploadInfoId,filename);
                    
					filePath= ConsolidatedProcessFactory.processConsolidatedExcel(filePath, datauploadInfoId);
					
					new ExternalServiceExecutor().processConsolidatedExcelAsync(dataUploadInfo, filePath);
					
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
							"Your file has been received, Please check the status in erp logs");
			} catch (Exception e) {
				log.error("uploadFile Exception exception", e);
			} finally {
				log.info("uploadFile method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;
			}
		}
		return null;
	}
		

	
	@SuppressWarnings("finally")
	@GET
	@Path("/get/taxsummary/gstr3b/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gettaxsummaryGstr3b(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear) throws JsonProcessingException {
		log.info("saveGstr3b method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
                 
			respBuilder.put(ApplicationMetadata.RESP_DATA,returnService.getgstr3bsummary(gstinTransactionDTO) );
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		
		} catch (AppException e) {
			log.error("saveGstr3b AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveGstr3b Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("saveGstr3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
	
	@SuppressWarnings("finally")
	@GET
	@Path("/get/offsetliabilty/gstr3b/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getoffsetliabiltyGstr3b(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getoffsetliabiltyGstr3b method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
                 
			respBuilder.put(ApplicationMetadata.RESP_DATA,returnService.calculateOffsetLiabilyDetails(gstinTransactionDTO) );
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		
		} catch (AppException e) {
			log.error("getoffsetliabiltyGstr3b AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getoffsetliabiltyGstr3b Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getoffsetliabiltyGstr3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
	@SuppressWarnings("finally")
	@POST
	@Path("/save/offsetliabilty/gstr3b/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getoffsetliabiltyGstr3b(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear,String request) throws JsonProcessingException {

		log.info("getoffsetliabiltyGstr3b method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
                 
			respBuilder.put(ApplicationMetadata.RESP_DATA,returnService.saveoffsetliablity(gstinTransactionDTO, request)  );
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		
		} catch (AppException e) {
			log.error("getoffsetliabiltyGstr3b AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getoffsetliabiltyGstr3b Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getoffsetliabiltyGstr3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
	@SuppressWarnings("finally")
	@POST
	@Path("/get/challan/gstr3b/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response calculateChallan(JsonNode requestJson) throws JsonProcessingException {

		log.info("calculateChallan method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		
		
		try {
			YearMonth yearmonth=YearMonth.parse(requestJson.get("monthYear").asText(), DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(requestJson.get("monthYear").asText());
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(requestJson.get("gstin").asText(),fyear));
			respBuilder.put(ApplicationMetadata.RESP_DATA,returnService.genrateChallan(requestJson.get("dueDate").asText(),requestJson.get("filingDate").asText(),gstinTransactionDTO));
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		
		} /*catch (AppException e) {
			log.error("getoffsetliabiltyGstr3b AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		}*/ catch (Exception e) {
			log.error("calculateChallan Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("calculateChallan method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	@SuppressWarnings("finally")
	@GET
	@Path("/generate3b/{monthYear}/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response generateReport3B(@PathParam("monthYear") String monthYear,
			@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("generate3B method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String url = null;

		try {

			    returnService.generate3B(gstin, monthYear);
				log.info("generate3B created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG_3B);

				// logging user action
				MessageDto messageDto = new MessageDto();
				messageDto.setGstin(gstin);
				messageDto.setMonthYear(monthYear);
				apiLoggingService.saveAction(UserAction.GENRATE_GSTR3B, messageDto, "all");

				// end logging user action
		} catch (AppException e) {
			log.error("generateReport3B AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("generateReport3B JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("generateReport3B method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
	
	
	
	
	
	
	
	}
