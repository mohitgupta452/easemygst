package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.Serializable;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.entity.DataUploadInfoDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;

@RequestScoped
@Path("/utilws")
@Produces("application/json")
@Consumes("application/json")
public class UtilityWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;
	
	@Inject
	private TaxpayerService taxpayerService;
	
	/**
	 * This method will get all pan from taxpayer table requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/update/dataupload/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateDataUploadInfo(String request) throws JsonProcessingException {

		log.info("updateDataUploadInfo method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			DataUploadInfoDTO infoDTO = JsonMapper.objectMapper.readValue(request,DataUploadInfoDTO.class);
			Integer id = taxpayerService.updateDataUploadInfo(infoDTO);
			if (!Objects.isNull(id)) {
				log.info("updateDataUploadInfo updated entry successfully for id {}", id);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, id);
			} else {
				log.info("updateDataUploadInfo Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateDataUploadInfo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("updateDataUploadInfo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("updateDataUploadInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
}
