package com.ginni.easemygst.portal.ws.impl.emg;


import javax.ws.rs.Path;


import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.EWayBillService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;

@Path("/ewaybillws")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EwayBillWebService {

	@Inject
	private EWayBillService eWayBillService;

	@Inject
	private Logger log;

	@Path("/register")
	@POST
	public Map<String, Object> registerForEWB(JsonNode requestJson) {

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String gstin = null;
			String username = null;
			String password=null;
			Boolean isActive = null;
			if (requestJson.has("gstin") && StringUtils.isNoneEmpty(requestJson.get("gstin").asText()))
				gstin = requestJson.get("gstin").asText();
			else throw new AppException("1001","gstin is mandatory");
			
			if (requestJson.has("username") && StringUtils.isNoneEmpty(requestJson.get("username").asText()))
				username = requestJson.get("username").asText();
			else throw new AppException("1001","username is mandatory");

			if (requestJson.has("password") && StringUtils.isNoneEmpty(requestJson.get("password").asText()))
				password = requestJson.get("password").asText();
			else throw new AppException("1001","password is mandatory");
			
			if (requestJson.has("isActive"))
				isActive = Boolean.valueOf(requestJson.get("isActive").asText());

			eWayBillService.registerForEWB(gstin, username, password,isActive);

			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, String.format("User %s has been registered", username));
		} 
		catch(AppException appException) {
			log.error("exception while register for ewb", appException);
			respBuilder.put(ApplicationMetadata.RESP_ERROR,appException.getMessageJsonFromObject());
		}
		catch (Exception e) {
			log.error("exception while register for ewb", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, "Unable to process your request");

		}

		return respBuilder;

	}
	
	
	
	

/*
	public void getSentEwayBills() {

	@Path("/get/sent/ewaybill/{gstin}")
	public void getSentEwayBills(@PathParam("gstin") String gstin, @QueryParam("date") String gendate,
			@QueryParam("refresh") boolean refresh) {

	}

	public void getRecivedEwayBills() {

	}*/
	

	/*@Path("/upload/ewaybills")
	public void uploadEwayBills() {

	}
	
	public void addEwayBills() {


	@Path("/add/ewaybill")
	public void addEwayBillFromPortal() {

	}*/

}
