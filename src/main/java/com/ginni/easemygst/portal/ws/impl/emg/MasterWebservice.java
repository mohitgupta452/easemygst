package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.entity.BusinessTypeDTO;
import com.ginni.easemygst.portal.business.entity.HsnSacDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;

@RequestScoped
@Path("/masterws")
@Produces("application/json")
@Consumes("application/json")
public class MasterWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private MasterService masterService;
	
	@Inject
	private FinderService finderService;

	@SuppressWarnings("finally")
	@GET
	@Path("/get/master/state")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllStates() throws JsonProcessingException {

		log.info("getAllStates method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<StateDTO> stateDTOs = finderService.getAllState();
			if (!Objects.isNull(stateDTOs)) {
				log.info("getAllStates received size for all states {}..", stateDTOs.size());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, stateDTOs);
			} else {
				log.info("getAllStates fail to retrieve states..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getAllStates AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getAllStates Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getAllStates method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/master/business")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllBusinesses() throws JsonProcessingException {

		log.info("getAllBusinesses method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<BusinessTypeDTO> businessTypeDTOs = finderService.getAllBusinessType();
			if (!Objects.isNull(businessTypeDTOs)) {
				log.info("getAllBusinesses received size for all business is {}..", businessTypeDTOs.size());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, businessTypeDTOs);
			} else {
				log.info("getAllBusinesses fail to retrieve types for the business..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getAllBusinesses AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getAllBusinesses Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getAllBusinesses method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/hsnsac/{type}/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHsnSac(@PathParam("type") String type, @PathParam("code") String code)
			throws JsonProcessingException {

		log.info("getHsnSac method starts.." + type + " " + code);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<HsnSacDTO> hsnSacDTOs = masterService.getHsnSacContent(type, code);
			if (!Objects.isNull(hsnSacDTOs)) {
				log.info("getHsnSac successfully retrieved for the HsnSac");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, hsnSacDTOs);
			} else {
				log.info("getHsnSac fails to retrieve data for HsnSac..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getHsnSac AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getHsnSac Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getHsnSac method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	@SuppressWarnings("finally")
	@POST
	@Path("/search/hsn")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchHsnCode(String key) throws JsonProcessingException {

		log.info("searchHsnCode method starts.." + key);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String output = "[]";
			if (!StringUtils.isEmpty(key)) {
				JSONObject out = new JSONObject(key);
				if (out.has("key")) {
					key = out.getString("key");
					output = masterService.searchHsnCode(key);
				}
			}
			if (!Objects.isNull(output)) {
				log.info("searchHsnCode Search for hsn code is successful");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);
			} else {
				log.info("searchHsnCode unable to search for the hsncode..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("searchHsnCode AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("searchHsnCode Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("searchHsnCode method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	@SuppressWarnings({ "finally", "unused" })
	@GET
	@Path("/search/gstin/{username}/{searchId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchGstin(@PathParam("username") String username, @PathParam("searchId") String id)
			throws JsonProcessingException {

		log.info("searchGstin method starts.." + username + id);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			/*List<GstinDataDTO> gstinDataDTO = masterService.searchGstin(username, id);*/
			if (false) {
				log.info("searchGstin search for gstin is successful for {}",username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				//respBuilder.put(ApplicationMetadata.RESP_DATA, gstinDataDTO);
			} else {
				log.info("searchGstin unable to search the gstin for {}",username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} /*catch (AppException e) {
			log.error("searchGstin AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} */catch (Exception e) {
			log.error("searchGstin Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("searchGstin method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

}
