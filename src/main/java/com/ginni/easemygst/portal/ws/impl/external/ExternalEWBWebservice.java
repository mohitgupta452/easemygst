package com.ginni.easemygst.portal.ws.impl.external;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.EWayBillService;

@Path("/ewbws/")
public class ExternalEWBWebservice {
	
	@Inject
	private EWayBillService eWayBillService;
	
	@Inject
	private Logger log;
	
	@POST
	@Path("generate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public com.ginni.easemygst.portal.data.view.Response generateEwayBill(JsonNode request){
		
		try {
		return eWayBillService.generateEWayBill(request);
		}catch(Exception exception) {
			log.error("exception",exception);
		}
		return null;
		
	}

}
