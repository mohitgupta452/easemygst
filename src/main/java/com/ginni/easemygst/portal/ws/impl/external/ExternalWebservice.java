package com.ginni.easemygst.portal.ws.impl.external;

import java.io.File;
import java.io.Serializable;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ginni.easemygst.portal.business.dto.ZohoFormsEventData;
import com.ginni.easemygst.portal.business.dto.ZohoSubscriptionEventDTO;
import com.ginni.easemygst.portal.business.entity.DataUploadInfoDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ConsolidatedCsvProcessorManager;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.ExternalServiceExecutor;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.subscription.SubscriptionService;
import com.ginni.easemygst.portal.transaction.crud.ConsolidatedService;
import com.ginni.easemygst.portal.transaction.crud.ConsolidatedService.DATA_SEPARATOR;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.ManipulateFile;
import com.mashape.unirest.http.JsonNode;

import lombok.Getter;

@RequestScoped
@Path("/extws")
@Produces("application/json")
@Consumes("application/json")
public class ExternalWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;
	
	@Inject
	private AppConfig appConfig;

	@Inject
	private SubscriptionService subscriptionService;

	@Inject
	private TaxpayerService taxpayerService;
	
	
	@Inject
	private CrudService crudService;
	

	@Inject
	private ConsolidatedCsvProcessorManager consolidatedCsvManager;


	@SuppressWarnings("finally")
	@POST
	@Path("/push/zohosubs/event/data")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response pushZohoSubscriptionEventData(@FormParam("payload") String request) throws JsonProcessingException {

		log.info("pushZohoSubscriptionEventData method starts.. {}", request);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			ZohoSubscriptionEventDTO dto = JsonMapper.objectMapper.readValue(request, ZohoSubscriptionEventDTO.class);
			flag = subscriptionService.parseEventData(dto);
			if (flag) {
				log.info("pushZohoSubscriptionEventData parsed successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("pushZohoSubscriptionEventData fails to parse complete");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} finally {
			log.info("pushZohoSubscriptionEventData method ends..");
			Response response = Response.status(200).build();
			return response;
		}
	}
	
	@SuppressWarnings("finally")
	@POST
	@Path("/push/zohoforms/data")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response pushZohoFormsEventData(String request, @QueryParam("FUNCTION_KEY") String functionKey) throws JsonProcessingException {

		log.info("pushZohoFormsEventData method starts.. {}", request);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			ZohoFormsEventData data = new ZohoFormsEventData();
			data.setFunctionKey(functionKey);
			data.setData(new JsonNode(request));
			log.info("data requested for {} and data is {}", functionKey, request);
			flag = subscriptionService.parseZohoFormsData(data);
			if (flag) {
				log.info("pushZohoFormsEventData parsed successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("pushZohoFormsEventData fails to parse complete");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} finally {
			log.info("pushZohoFormsEventData method ends..");
			Response response = Response.status(200).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/update/dataupload/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateDataUploadInfo(String request) throws JsonProcessingException {

		log.info("updateDataUploadInfo method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			DataUploadInfoDTO infoDTO = JsonMapper.objectMapper.readValue(request, DataUploadInfoDTO.class);
			Integer id = taxpayerService.updateDataUploadInfo(infoDTO);
			if (!Objects.isNull(id)) {
				log.info("updateDataUploadInfo updated entry successfully for id {}", id);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, id);
			} else {
				log.info("updateDataUploadInfo Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateDataUploadInfo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("updateDataUploadInfo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("updateDataUploadInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	
	@SuppressWarnings("finally")
	@POST
	@Path("/update/bulk/dataupload/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateBulkDataUploadInfo(String request) throws JsonProcessingException {

		log.info("updateBulkDataUploadInfo method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<DataUploadInfoDTO> dataUploadInfoDTOs = JsonMapper.objectMapper.readValue(request, new TypeReference<List<DataUploadInfoDTO>>() {
			});
			Map<String,Object> responseMap= taxpayerService.updateDataUploadInfo(dataUploadInfoDTOs);
			
			new ExternalServiceExecutor().myPostConstruct(responseMap.get("data"));
			
			int count=(Integer)responseMap.get("count");
			if (count > 0) {
				log.info("updateBulkDataUploadInfo updated entry successfully with count {}", count);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, count);
			} else {
				log.info("updateBulkDataUploadInfo Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateBulkDataUploadInfo AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("updateBulkDataUploadInfo JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
		} finally {
			log.info("updateBulkDataUploadInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("upload/csv/{gstin}/{returnType}/{monthYear}")
	public Response uploadCsv(MultipartFormDataInput input, @PathParam("gstin") String gstin,@PathParam("returnType") String returnType,@PathParam("monthYear") String monthYear){
		log.info("updateBulkDataUploadInfo method starts..");
		
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try{
			
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			String file=ManipulateFile.uploadCsvFile(input);
			
			GstinTransactionDTO gstinTransactionDTO=new GstinTransactionDTO();
			gstinTransactionDTO.setReturnType(returnType);
			gstinTransactionDTO.setTransactionType(TransactionType.ALL.toString());
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			
			if (!StringUtils.isEmpty(file)) {
				int id =consolidatedCsvManager.saveUploadedInfo(gstinTransactionDTO, file);

				//consolidatedCsvManager.importExcelToDatabase(file,id);
				new ConsolidatedService().importExcelToDatabase(file, id,DATA_SEPARATOR.PIPE);
				log.info("updateBulkDataUploadInfo successfully updated entry successfully with count {}, id {}", file,id);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put("id", id);

				//respBuilder.put(ApplicationMetadata.RESP_DATA, count);
			} else {
				log.info("updateBulkDataUploadInfo Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		}catch(Exception e){
			
		}
		finally{
			log.info("updateBulkDataUploadInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	

}
