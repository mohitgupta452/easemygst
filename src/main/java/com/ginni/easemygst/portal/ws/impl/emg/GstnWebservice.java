package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Generated;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Comparison2avs3bDTO;
import com.ginni.easemygst.portal.business.dto.ComparisonDTO;
import com.ginni.easemygst.portal.business.dto.GstinComparison2aVS3bDTO;
import com.ginni.easemygst.portal.business.dto.GstinComparisonDTO;
import com.ginni.easemygst.portal.business.dto.GstrFlowDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ScheduleManager;
import com.ginni.easemygst.portal.business.service.SecurityManager;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata.ReturnsModule;
import com.ginni.easemygst.portal.gst.response.GSTNRespStatus;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.Organisation;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;


@RequestScoped
@Path("/gstnws")
@Produces("application/json")
@Consumes("application/json")
public class GstnWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;
	
	@Inject
	private FunctionalService functionalService;

	@Inject
	private TaxpayerService taxpayerService;
	
	@Inject
	private FinderService finderService;
	
	@Inject
	private UserBean userBean;
	
	@Inject
	private SecurityManager securityManager;
	
	
	@Inject
	private ApiLoggingService apiLoggingService;
	
	@Inject
	private ReturnsService returnsService;
	
	@Inject
	private ScheduleManager scheduleManager;
	
	@Inject
	private UserService userservice;
	
	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;
	
	/************************************ WILL DELETE THIS *******************/
	//get data from gstn server 
	@SuppressWarnings("finally")
	@GET
	@Path("/get/data/{gstin}/{rtype}/{type}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstr1Data(@PathParam("type") String type, @PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getGstr1Data method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String successMsg=ApplicationMetadata.SUCCESS_MSG;

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			boolean flag = false;
			String tempResponse = "";
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			type = type.equalsIgnoreCase("all") ? null : type;
			gstinTransactionDTO.setTransactionType(type);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setReturnType(ReturnType.GSTR2.toString());
			if(ReturnType.GSTR1==ReturnType.valueOf(rtype.toUpperCase())&&false)
				tempResponse = functionalService.syncGstr1TransactionData(gstinTransactionDTO);
			
			else if(ReturnType.GSTR2==ReturnType.valueOf(rtype.toUpperCase()))
				tempResponse = functionalService.syncGstr2ActionRequiredTransactionData(gstinTransactionDTO);
			else {
				GetGstr2Resp getGstr2Resp=null;
			   if(ReturnType.GSTR2A==ReturnType.valueOf(rtype.toUpperCase()))	
			     getGstr2Resp = functionalService.syncGstr2ATransactionData(gstinTransactionDTO);
			   if(ReturnType.GSTR1A==ReturnType.valueOf(rtype.toUpperCase()))
				 getGstr2Resp = functionalService.syncGst1ATransactionData(gstinTransactionDTO);
			   if(ReturnType.GSTR1==ReturnType.valueOf(rtype.toUpperCase())){
					 gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString());
					 getGstr2Resp = functionalService.syncGstr1ActionRequiredTransactionDataNew(gstinTransactionDTO);
			   }
			   
			   log.debug("get gstn data return in gstn web service");
			
				if (getGstr2Resp.getError() != null) {
					String errorCode = getGstr2Resp.getError().getCode();
					String msg = getGstr2Resp.getError().getMessage();
					if (GSTNRespStatus.GSTN_Auth_Failed1.equalsIgnoreCase(errorCode)
							|| GSTNRespStatus.GSTN_Auth_Failed2.equalsIgnoreCase(errorCode))
						msg = "Please authenticate to GSTN";
					throw new AppException(errorCode, msg);
				} else {
					scheduleManager.processData(getGstr2Resp, TransactionType.valueOf(type), finderService.findTaxpayerGstin(gstin), monthYear, ReturnType.GSTR2, gstinTransactionDTO);
//					int count = new GstnServiceExecutor().insertUpdateGstnData(getGstr2Resp,
//							TransactionType.valueOf(type), finderService.findTaxpayerGstin(gstin),
//							gstinTransactionDTO.getMonthYear(), ReturnType.GSTR2);
//					gstinTransactionDTO.setTotalSync(count);
//					successMsg = count + " records fetch from gstn successfully";
//					if(count!=0)
//					functionalService.saveDataToTokenResponse(gstinTransactionDTO,type);
					tempResponse = "Get data after 10-15 minutes";
				}
			}
			if (!StringUtils.isEmpty(tempResponse)) {
				log.info("getGstr1Data successfully got the gstr1 data {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,tempResponse);
				respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

			} else {
				log.info("getGstr1Data fail to retrieve teh gstr1 data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

			}
		} catch (AppException e) {
			log.error("getGstr1Data AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getGstrData Exception exception {} ", e);
		} finally {
			log.info("getGstr1Data method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	//get data from gstn server 
	
	//get data from gstn server 
		@SuppressWarnings("finally")
		@GET
		@Path("/get/urls/{gstin}/{rtype}/{type}/{monthYear}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getUrlsFromGstn(@PathParam("type") String type, @PathParam("gstin") String gstin,
				@PathParam("rtype") String rtype, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

			log.info("getGstr1Data method starts..");

			Map<String, Object> respBuilder = new HashMap<>();
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			String successMsg=ApplicationMetadata.SUCCESS_MSG;

			try {
				YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				boolean flag = false;
				String tempResponse = "";
				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				gstinTransactionDTO.setMonthYear(monthYear);
				type = type.equalsIgnoreCase("all") ? null : type;
				gstinTransactionDTO.setTransactionType(type);
				gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
				gstinTransactionDTO.setReturnType(ReturnType.GSTR2.toString());
				if(ReturnType.GSTR1==ReturnType.valueOf(rtype.toUpperCase())&&false)
					tempResponse = functionalService.syncGstr1TransactionData(gstinTransactionDTO);
				
				else if(ReturnType.GSTR2==ReturnType.valueOf(rtype.toUpperCase()))
					tempResponse = functionalService.syncGstr2ActionRequiredTransactionData(gstinTransactionDTO);
				else {
					GetGstr2Resp getGstr2Resp=null;
				   if(ReturnType.GSTR2A==ReturnType.valueOf(rtype.toUpperCase()))	
				     getGstr2Resp = functionalService.syncGstr2ATransactionData(gstinTransactionDTO);
				   if(ReturnType.GSTR1A==ReturnType.valueOf(rtype.toUpperCase()))
					 getGstr2Resp = functionalService.syncGst1ATransactionData(gstinTransactionDTO);
				   if(ReturnType.GSTR1==ReturnType.valueOf(rtype.toUpperCase())){
						 gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString());
						 functionalService.processUrls(gstinTransactionDTO);
				   }
				   
				   log.debug("get gstn data return in gstn web service");
				
					/*if (getGstr2Resp.getError() != null) {
						String errorCode = getGstr2Resp.getError().getCode();
						String msg = getGstr2Resp.getError().getMessage();
						if (GSTNRespStatus.GSTN_Auth_Failed1.equalsIgnoreCase(errorCode)
								|| GSTNRespStatus.GSTN_Auth_Failed2.equalsIgnoreCase(errorCode))
							msg = "Please authenticate to GSTN";
						throw new AppException(errorCode, msg);
					} else {
						scheduleMannager.processData(getGstr2Resp, TransactionType.valueOf(type), finderService.findTaxpayerGstin(gstin), monthYear, ReturnType.GSTR2, gstinTransactionDTO);
//						int count = new GstnServiceExecutor().insertUpdateGstnData(getGstr2Resp,
//								TransactionType.valueOf(type), finderService.findTaxpayerGstin(gstin),
//								gstinTransactionDTO.getMonthYear(), ReturnType.GSTR2);
//						gstinTransactionDTO.setTotalSync(count);
//						successMsg = count + " records fetch from gstn successfully";
//						if(count!=0)
//						functionalService.saveDataToTokenResponse(gstinTransactionDTO,type);
						tempResponse = "Get data after 10-15 minutes";
					}*/
				}
				if (!StringUtils.isEmpty(tempResponse)) {
					log.info("getGstr1Data successfully got the gstr1 data {}..", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,tempResponse);
					respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

				} else {
					log.info("getGstr1Data fail to retrieve teh gstr1 data for {}..", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

				}
			} catch (AppException e) {
				log.error("getGstr1Data AppException exception {} ", e);
				respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			} catch (Exception e) {
				log.error("getGstrData Exception exception {} ", e);
			} finally {
				log.info("getGstr1Data method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;
			}
		}
	
	
		@SuppressWarnings("finally")
		@GET
		@Path("/get/data/{gstin}/{rtype}/{monthYear}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getGstnData(@PathParam("gstin") String gstin,
				@PathParam("rtype") String rtype, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

			log.info("getGstr1Data method starts..");

			Map<String, Object> respBuilder = new HashMap<>();
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

			try {
				boolean flag = false;
				String tempResponse = "";
				YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				gstinTransactionDTO.setMonthYear(monthYear);
				gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
				if(ReturnType.GSTR1A==ReturnType.valueOf(rtype.toUpperCase()))
					tempResponse = functionalService.syncGstr1TransactionData(gstinTransactionDTO);
				else if(ReturnType.GSTR2A==ReturnType.valueOf(rtype.toUpperCase()))
				 functionalService.syncGstr2ATransactionData(gstinTransactionDTO);
				else  if(ReturnType.GSTR1==ReturnType.valueOf(rtype.toUpperCase())){
					 functionalService.syncGstr2ATransactionData(gstinTransactionDTO);
					 
				}
				
				
				if (!StringUtils.isEmpty(tempResponse)) {
					log.info("getGstr1Data successfully got the gstr1 data {}..", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

				} else {
					log.info("getGstr1Data fail to retrieve teh gstr1 data for {}..", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

				}
			} catch (AppException e) {
				log.error("getGstr1Data AppException exception {} ", e.getMessage());
				respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			} catch (Exception e) {
				log.error("getGstr1Data Exception exception {} ", e.getMessage());
				e.printStackTrace();
			} finally {
				log.info("getGstr1Data method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;
			}
		}

	/************************************ WILL DELETE THIS *******************/

	/************************************ WILL DELETE THIS *******************/
	@SuppressWarnings("finally")
	@GET
	@Path("/get/summary/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstr1Summary(@PathParam("gstin") String gstin,
			@PathParam("rtype") String rtype, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getGstr1Summary method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			String data = null;
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			   respBuilder.put("data",functionalService.compareReturnSummary(gstinTransactionDTO));
				log.info("getGstr1Summary successfully get the gstr1 summary for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		} catch (AppException e) {
			log.error("getGstr1Summary AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getGstr1Summary Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getGstr1Summary method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/************************************ WILL DELETE THIS *******************/

	/************************************ WILL DELETE THIS *******************/
	@SuppressWarnings("finally")
	@GET
	@Path("/save/data/{gstin}/{rtype}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveGstr1Data(@PathParam("gstin") String gstin, @PathParam("rtype") String rtype,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("saveGstr1Data method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			
			if (!Objects.isNull(userBean)) {
				int orgId = userBean.getUserDto().getOrganisation().getId();
				Organisation org = em.find(Organisation.class, orgId);
				if (!Objects.isNull(org)
						&& (StringUtils.isEmpty(org.getStatus()) || org.getStatus().equalsIgnoreCase("trial"))) {
					throw new AppException(ExceptionCode._NOT_ALLOWED_IN_TRIAL);
				}
			}
			
			boolean flag = true;
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			gstinTransactionDTO.setTransactionType("CDNUR");
			if(rtype.equalsIgnoreCase("GSTR1"))
			 functionalService.saveGstr1DataToGstn(gstinTransactionDTO);
			else
			 functionalService.saveGstr2DataToGstn(gstinTransactionDTO);
			if (flag) {
				log.info("saveGstr1Data successfully saved the gstr1 data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("saveGstr1Data fails to save data for gstr1  {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("saveGstr1Data AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveGstr1Data Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("saveGstr1Data method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	

	/************************************ WILL DELETE THIS *******************/

	/************************************ WILL DELETE THIS *******************/
	@SuppressWarnings("finally")
	@GET
	@Path("/check/return/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkReturnStatus(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("checkGstr1Return method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = true;
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(ReturnsModule.GSTR1.toString());
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			String returnStatus = null;//functionalService.checkReturnStatus(gstinTransactionDTO);

			if (!Objects.isNull(returnStatus)) {
				log.info("checkGstr1Return successfully completed for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatus);

			} else {
				log.info("checkGstr1Return fail to retrieve for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("checkGstr1Return AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("checkGstr1Return Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getGstr1Data method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/************************************ WILL DELETE THIS *******************/

	/************************************ WILL DELETE THIS *******************/

	@SuppressWarnings("finally")
	@GET
	@Path("/gstn/submit/gstr/{rtype}/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitGstrToGstn(@PathParam("rtype") String rtype, @PathParam("gstin") String gstin,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {
           
		log.info("submitGstrToGstn method starts for {} {} {}..", rtype, gstin, monthYear);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		GstrFlowDTO flowDTO = null;

		try {
			
			if(true)
			throw new AppException(ExceptionCode._FAILURE);
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(rtype);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			if(rtype.equalsIgnoreCase("GSTR1"))
			 flowDTO = functionalService.submitGstrToGstn(gstinTransactionDTO);
			else if(rtype.equalsIgnoreCase("GSTR2"))
			  flowDTO = functionalService.submitGstr2ToGstn(gstinTransactionDTO);
			if (!Objects.isNull(flowDTO)) {
				log.info("submitGstrToGstn successfully submitted from gstr to gstn {}", gstin);
				if (flowDTO.isError()) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, flowDTO.getErrorMsg());
				} else {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA,
							String.format(ApplicationMetadata.GSTR_RETURN_SUBMIT, rtype, flowDTO.getTransactionId()));
				}
			} else {
				log.info("submitGstrToGstn fail to submit from gstr to gstn..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("submitGstrToGstn AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("submitGstrToGstn JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("submitGstr method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	/************************************ WILL DELETE THIS *******************/
	
	@SuppressWarnings("finally")
	@GET
	@Path("/check/return/action/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkActionStatus(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("checkGstr1Return method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = true;
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setReturnType(ReturnsModule.GSTR1.toString());
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			functionalService.checkACtionStatus(gstinTransactionDTO);
		String	returnStatus="";
			if (!Objects.isNull(returnStatus)) {
				log.info("checkGstr1Return successfully completed for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnStatus);

			} else {
				log.info("checkGstr1Return fail to retrieve for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("checkGstr1Return AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("checkGstr1Return Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getGstr1Data method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	//save gstr3b
	@SuppressWarnings("finally")
	@GET
	@Path("/save/gstr3b/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveGstr3b(@PathParam("gstin") String gstin,
			 @PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("saveGstr3b method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!finderService.isSubmit(gstin, monthYear,ReturnType.GSTR3B.toString())) {

			boolean flag = false;
			String tempResponse = "";
			YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
			String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
			gstinTransactionDTO.setMonthYear(monthYear);
			gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
			//taxpayerService.checkTrialPeriodConstraint();
			securityManager.checkIfSaveNotAllowedForGstin(gstinTransactionDTO);
			securityManager.checkTrialPeriodConstraint();;

			tempResponse = functionalService.syncGstr3BTransactionData(gstinTransactionDTO);

			if (!StringUtils.isEmpty(tempResponse)) {
				log.info("saveGstr3b successfully got the gstr1 data {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, tempResponse);
				respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);
				//logging user action
				MessageDto messageDto=new MessageDto();
				messageDto.setGstin(gstin);
				messageDto.setMonthYear(monthYear);
				apiLoggingService.saveAction(UserAction.SAVE_GSTR3B, messageDto);
					
				//end logging user action

			} else {
				log.info("saveGstr3b fail to retrieve teh gstr1 data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, tempResponse);

			}}
		} catch (AppException e) {
			log.error("saveGstr3b AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveGstr3b Exception exception {} ", e);
			e.printStackTrace();
		} finally {
			log.info("saveGstr3b method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	//end save gstr3b 
	
	@GET
	@Path("/refresh/returnshistory/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response trackReturns(@PathParam("gstin") String gstin) {
		
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
		     returnsService.viewReturns(gstin);
		     respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		} catch (AppException e) {
			log.error("AppException exception", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("Exception{} ", e);
		} finally {
			log.info("track returns method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	@Path("/get/gstr6/{gstin}/{type}/{monthYear}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
		public Response getGstr6Data(@PathParam("type") String type, @PathParam("gstin") String gstin,
				@PathParam("monthYear") String monthYear) throws JsonProcessingException {

			log.info("getGstr6Data method starts..");

			Map<String, Object> respBuilder = new HashMap<>();
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			String successMsg=ApplicationMetadata.SUCCESS_MSG;

			try {
				YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				boolean flag = false;
				String tempResponse = "";
				GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
				gstinTransactionDTO.setMonthYear(monthYear);
				type = type.equalsIgnoreCase("all") ? null : type;
				gstinTransactionDTO.setTransactionType(type);
				gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
				gstinTransactionDTO.setReturnType(ReturnType.GSTR6.toString());
				
				String  fileurl=functionalService.syncGstr6TransactionData(gstinTransactionDTO);
				
				if (StringUtils.isNotEmpty(fileurl)) {
					
					log.info("getGstr6Data successfully got the gstr1 data {}..", gstin);
					
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put("fileUrl", fileurl);

				} else {
					log.info("getGstr6Data fail to retrieve teh gstr1 data for {}..", gstin);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

				}
			} catch (AppException e) {
				log.error("getGstr1Data AppException exception {} ", e);
				respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			} catch (Exception e) {
				log.error("getGstrData Exception exception {} ", e);
			} finally {
				log.info("getGstr1Data method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;
			}
		}
	
}
