package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.ManipulateFile;


@RequestScoped
@Path("/test")
@Produces("application/json")
@Consumes("application/json")
public class TestWebService implements Serializable{
	
	@Inject
	private FinderService finderService;
	@Inject
	private Logger log;
	@Inject
	private TaxpayerService taxpayerService;
	@Inject
	private UserBean userBean;

	@SuppressWarnings("finally")
	@POST
	@Path("/upload/xls/{rtype}/{gstin}/{monthYear}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadData(MultipartFormDataInput input,@PathParam("gstin") String gstin,@PathParam("monthYear") String monthYear, @PathParam("rtype") String rtype) throws Exception{
		
		if (!(StringUtils.isEmpty(rtype) && StringUtils.isEmpty(gstin) && StringUtils.isEmpty(monthYear))) {
			
			log.info("uploadFile method starts for {} {} {}..", gstin, monthYear, rtype);

			Map<String, Object> respBuilder = new HashMap<>();
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			int count = 0;
			
			try {
				log.info("before upload data");
				String fileName = ManipulateFile.uploadMultipartFile(input);
				GstExcel gstexcel = new GstExcel(new File(fileName));
				log.info("before return Transaction");
//				Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getReturnTransactionsByGstin(gstin, rtype);
				B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();
//				TaxpayerGstin taxpayerGstin = new TaxpayerGstin();
//				taxpayerGstin.setGstin(gstin);
				log.info("before Call factory");
				Transaction factory1 = new TransactionFactory("B2B");
				gstexcel.loadSheet(0);
				String sheetName1 = gstexcel.getWb().getSheetName(0);
				log.info("before Call gstExcel");
				
				count=gstexcel.uploadData(gstexcel, TransactionType.B2B.toString(), rtype,monthYear, finderService.findTaxpayerGstin(gstin), sheetName1, SourceType.SAP);
				
				log.info("uploadFile successfully uploaded file {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.EXCEL_UPLOAD, count));
	
			} catch (AppException e) {
				log.error("uploadFile AppException exception ", e);
				respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			} catch (Exception e) {
				log.error("uploadFile Exception exception", e);
			} finally {
				log.info("uploadFile method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;
			}
		}
		return null;
	}
	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayhello() {
		Response response = Response.status(Response.Status.OK).entity("Valid Resource : ").build();
		return response;
	}
	
}
