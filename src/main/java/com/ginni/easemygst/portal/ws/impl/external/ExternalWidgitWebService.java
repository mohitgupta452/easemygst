package com.ginni.easemygst.portal.ws.impl.external;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.view.Response;
import com.ginni.easemygst.portal.helper.AppException;

@Path("/widgitws")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ExternalWidgitWebService {

	@Inject
	private ReturnsService returnsService;

	@Inject
	private Logger log;

	@Path("get/url/{gstin}/{transactionType}")
	@POST
	public Object getwidgit(@PathParam("gstin") String gstin, @PathParam("transactionType") String transactionType,
			JsonNode requestJson) {

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		String cpGstin = requestJson.get("cpgstin").asText();

		String invoiceNumber = requestJson.get("invoiceNumber").asText();

		String financialYear = requestJson.get("financialYear").asText();

		Map<String, Object> responseMap = new HashMap<>();

		responseMap.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
		responseMap.put(ApplicationMetadata.RESP_MSG_KEY, "Your request has been processed");

		try {
			responseMap.put("url",
					"http://dev.easemygst.com/comparison/index.html?referenceNumber="
							+ returnsService.getComparisionResultReferenceNumber(gstin, cpGstin, invoiceNumber,
									financialYear, transactionType));
		} catch (JsonProcessingException | AppException e) {
			log.error("exception ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessage());
		}

		return responseMap;

	}

	@Path("/comparison/result/{referenceNumber}")
	@GET
	public Object getwidgit(@PathParam("referenceNumber") String referenceNumber) {

		Map<String, Object> responseMap = new HashMap<>();

		responseMap.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
		responseMap.put(ApplicationMetadata.RESP_MSG_KEY, "Your request has been processed");

		try {
			responseMap.put("data", returnsService.getComparisionResultFromReferenceNumber(referenceNumber));
		} catch (AppException | IOException e) {
			responseMap.put(ApplicationMetadata.RESP_ERROR, e.getMessage());

		}

		return responseMap;

	}
	
	/*@GET
	@Path("/compare/viewurl")
	public String getHtmlUrl(@PathParam("gstin") String gstin,
			@PathParam("transactionType")String transactionType,JsonNode requestJson) {
		
		String cpGstin = requestJson.get("cpgstin").asText();

		String invoiceNumber = requestJson.get("invoiceNumber").asText();

		String financialYear = requestJson.get("financialYear").asText();
		
		String machineKey = requestJson.get("machineKey").asText();
		
		String serialKey= requestJson.get("serialKey").asText();
		
		
		
		return "htmlml view usrl "+UUID.randomUUID().toString();//
		
	}*/

}
