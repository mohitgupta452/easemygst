/**
 * 
 */
package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;
import javax.json.JsonException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.SigninBean;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.CityDTO;
import com.ginni.easemygst.portal.business.entity.ContactUsDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.HelpContentDTO;
import com.ginni.easemygst.portal.business.entity.NonGovStateDTO;
import com.ginni.easemygst.portal.business.entity.PartnerDTO;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService;
import com.ginni.easemygst.portal.business.service.NotificationService;
import com.ginni.easemygst.portal.business.service.NotificationService.NotificationCategory;
import com.ginni.easemygst.portal.business.service.PartnerService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.business.service.UserService.RegisterUsing;
import com.ginni.easemygst.portal.client.UrlFetch;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.AuthenticationService;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.notify.EmailFactory;
import com.ginni.easemygst.portal.reports.ExagoReportHelper;
import com.ginni.easemygst.portal.security.EncryptDecrypt;
import com.ginni.easemygst.portal.security.GSTRSAEncryptDecryptService;
import com.ginni.easemygst.portal.security.TokenService;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.ExcelTemplate;
import com.ginni.easemygst.portal.utils.ManipulateFile;
import com.mashape.unirest.http.JsonNode;

/**
 * @author Sumeet
 *
 */
@Path("/signinws")
@Produces("application/json")
@Consumes("application/json")
public class SigninWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private SigninBean signinBean;

	@Inject
	private UserService userService;

	@Inject
	private ReturnsService returnService;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private PartnerService partnerService;

	@Inject
	private TokenService tokenService;

	@Inject
	private GSTRSAEncryptDecryptService encryptDecrypt;

	@Context
	private HttpServletRequest httpRequest;

	@Inject
	private MasterService masterService;

	@Inject
	private RedisService redisService;

	@Inject
	private FinderService finderService;
	
	@Inject
	private AuthenticationService authService;
	
	@Inject
	private AppConfig appConfig;
	
	@Inject
	private NotificationService notificationService;
	
	
	

	/**
	 * This will store user details in UserDTO using SignUpBean and it will also
	 * generate OTP for mobile number authentication
	 * 
	 * {"firstName": "Sumeet", "lastName": "Agrawal", "password": "Cloud",
	 * "username": "sumeetagrawal01@gmail.com", "userContacts":
	 * [{"mobileNumber": "9643101022","emailAddress":"sumeetagrawal01@gmail.com"
	 * }]}
	 * 
	 * @param request
	 * @return Response
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/store/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response storeUserDetails(String request) throws JsonProcessingException {

		log.info("storeUserDetails method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			UserDTO userDTO = JsonMapper.objectMapper.readValue(request, UserDTO.class);
			if (userDTO != null)
				userDTO.setCreationIpAddress(httpRequest.getRemoteAddr());
			String otp = signinBean.storeUserDetails(userDTO);
			if (otp != null) {
				log.info("storeUserDetails opt successfully sent to user mobile number..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String mob = userDTO.getUserContacts().get(0).getMobileNumber();
				mob = "XXXXXX" + mob.substring(6, 10);
				String msg = String.format(ApplicationMetadata.STORE_SUCCESS_MSG, userDTO.getFirstName(), mob);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			}
		} catch (AppException e) {
			log.error("storeUserDetails AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("storeUserDetails JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("storeUserDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will authenticate OTP and create user account It will also provide
	 * an auth token, which will be later use to call all the other services.
	 * 
	 * @param otp,
	 *            username
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/verify/mobile/otp/{username}/{otp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyMobileOtp(@PathParam("otp") String otp, @PathParam("username") String username)
			throws JsonProcessingException {

		log.info("verifyMobileOtp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean isValid = signinBean.validateOtp(otp, username);
			if (isValid) {
				UserDTO userDTO = signinBean.getUserAuthToken();
				log.info("verifyMobileOtp validation is successful for the otp {}", otp);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SIGNUP_SUCCESS_MSG);
				if (userDTO != null) {
					respBuilder.put(ApplicationMetadata.RESP_AUTH_TOKEN, userDTO.getStatus());
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					long temp = Calendar.getInstance().getTimeInMillis();
					String lastTime = sdf.format(new Date(temp)) + " IST";
					List<String> financialyear=Arrays.asList("2018-2019","2017-2018");
					respBuilder.put("key", "NONE");
					respBuilder.put("username", userDTO.getFirstName() + " " + userDTO.getLastName());
					respBuilder.put("firstname", userDTO.getFirstName());
					respBuilder.put("lastname", userDTO.getLastName());
					respBuilder.put("lastlogin", lastTime);
					respBuilder.put("active", 1);
					respBuilder.put("orgname", userDTO.getOrganisation().getName());
					respBuilder.put("orgemail", userDTO.getUsername());
					respBuilder.put("myorg", "true");
					respBuilder.put("plantype", "noplan");
					respBuilder.put("trial", "yes");
					respBuilder.put("trial_days", "14");
					respBuilder.put("card", userDTO.getOrganisation().getCardUpdate());
            		respBuilder.put("financialyears",financialyear);
			       respBuilder.put("cfyear",CalanderCalculatorUtility.getCurrentFinancialYear());

				}
			} else {
				log.info("verifyMobileOtp Otp is not valid..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("verifyMobileOtp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("verifyMobileOtp JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("verifyMobileOtp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will regenerate OTP and send otp again to user mobile number.
	 * 
	 * @param username
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/resend/mobile/otp/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response resendMobileOtp(@PathParam("username") String username) throws JsonProcessingException {

		log.info("resendMobileOtp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String mob = signinBean.resendOtp(username);
			log.info("resendMobileOtp successfully validation..");
			if (!StringUtils.isEmpty(mob)) {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				mob = "XXXXXX" + mob.substring(6, 10);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.RESEND_OTP_SUCCESS, mob));
			}
		} catch (AppException e) {
			log.error("verifyMobileOtp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("resendMobileOtp JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("resendMobileOtp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This is required for
	 * 
	 * @param username
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/mobile/otp/{username}/{mobileNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMobileOtp(@PathParam("username") String username, @PathParam("mobileNumber") String mobileNumber)
			throws JsonProcessingException {

		log.info("getMobileOtp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String mob = signinBean.getMobileOtp(username, mobileNumber);
			log.info("getMobileOtp successfully validation..");
			if (!StringUtils.isEmpty(mob)) {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				mob = "XXXXXX" + mob.substring(6, 10);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.MOBILE_OTP_SUCCESS, mob));
			}
		} catch (AppException e) {
			log.error("getMobileOtp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getMobileOtp JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getMobileOtp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This is required for
	 * 
	 * @param username
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/verify/mobile/account")
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyAccountAndActivate(String request) throws JsonProcessingException {

		log.info("verifyAccountAndActivate method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = true;

		String firstname = "", lastname = "";
		String username = "", otp = "", password = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("firstname") && jsonObject.has("lastname")
				&& jsonObject.has("username") && jsonObject.has("otp")) {
			username = jsonObject.getString("username");
			firstname = jsonObject.getString("firstname");
			lastname = jsonObject.getString("lastname");
			otp = jsonObject.getString("otp");
			password = jsonObject.getString("password");
		}

		try {
			UserDTO userDTO = signinBean.verifyAccountActivate(username, firstname, lastname, otp, password);
			log.info("verifyAccountAndActivate successfully validation..");
			if (!Objects.isNull(userDTO)) {
				if (userDTO != null && userDTO.getId() > 0) {
					userDTO.setCreationIpAddress(httpRequest.getRemoteAddr());
					String token = tokenService.generateToken(userDTO);
					log.info("verifyAccountAndActivate successfully executed for user {}..", userDTO.getUsername());
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
							String.format(ApplicationMetadata.SIGNIN_SUCCESS_MSG, userDTO.getFirstName()));
					if (token != null) {
						flag = false;
						SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
						// getting last login time here
						long temp = Long.valueOf(userDTO.getPassword());
						String lastTime = sdf.format(new Date(temp)) + " IST";
						List<String> financialyear=Arrays.asList("2018-2019","2017-2018");
						respBuilder.put(ApplicationMetadata.RESP_AUTH_TOKEN, token);
						respBuilder.put("username", userDTO.getFirstName() + " " + userDTO.getLastName());
						respBuilder.put("firstname", userDTO.getFirstName());
						respBuilder.put("lastname", userDTO.getLastName());
						respBuilder.put("lastlogin", lastTime);
						respBuilder.put("active", 1);
						respBuilder.put("orgname", userDTO.getOrganisation().getName());
						respBuilder.put("orgemail", userDTO.getOrganisation().getPlanCode());
						respBuilder.put("myorg", userDTO.getOrganisation().getStatus());
						respBuilder.put("plantype", userDTO.getOrganisation().getCode());
	            		respBuilder.put("financialyears",financialyear);
						respBuilder.put("cfyear",CalanderCalculatorUtility.getCurrentFinancialYear());
						if(!StringUtils.isEmpty(userDTO.getOrganisation().getSubscriptionMode()) && userDTO.getOrganisation().getSubscriptionMode().equalsIgnoreCase("yes")) {
							respBuilder.put("trial", userDTO.getOrganisation().getSubscriptionMode());
							respBuilder.put("trial_days", userDTO.getOrganisation().getUnits());
						} else {
							respBuilder.put("trial", "no");
							respBuilder.put("trial_days", -1);
						}
						respBuilder.put("card", userDTO.getOrganisation().getCardUpdate());
						if (!Objects.isNull(userDTO.getIsChangeRequired()) && userDTO.getIsChangeRequired() == 1)
							respBuilder.put("isChangeRequired", 1);
						else
							respBuilder.put("isChangeRequired", 0);
					}
				}
			}
			if (flag) {
				log.info("verifyAccountAndActivate fails to complete user {}..", userDTO.getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("verifyAccountAndActivate AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("verifyAccountAndActivate JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("verifyAccountAndActivate method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will generate random password and send the same to user registered
	 * mobile number.
	 * 
	 * @param username
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/forgot/password/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response forgotPassword(@PathParam("username") String username) throws JsonProcessingException {

		log.info("forgotPassword method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String mobile = signinBean.forgotPassword(username, httpRequest.getRemoteAddr());
			if (!StringUtils.isEmpty(mobile)) {
				log.info("forgotPassword successfully executed for user {}..", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				mobile = "XXXXXX" + mobile.substring(6, 10);
				String msg = String.format(ApplicationMetadata.FORGOT_PASSWORD_MSG, mobile);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("forgotPassword fails to authenticate user {}..", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("forgotPassword AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("forgotPassword JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("forgotPassword method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will generate random password and send the same to user registered
	 * mobile number.
	 * 
	 * @param username
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/verify/forgot/password/otp/{username}/{otp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response forgotPasswordVerification(@PathParam("username") String username, @PathParam("otp") String otp)
			throws JsonProcessingException {

		log.info("forgotPasswordVerification method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			UserDTO userDTO = signinBean.validateForgotOtp(otp, username);
			if (!Objects.isNull(userDTO)) {
				userDTO.setCreationIpAddress(httpRequest.getRemoteAddr());
				UserDTO userDTO1 = signinBean.getNewUserAuthToken(userDTO);
				log.info("forgotPasswordVerification validation is successful");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FORGOT_SUCCESS_MSG);
				if (userDTO1 != null) {
					respBuilder.put(ApplicationMetadata.RESP_AUTH_TOKEN, userDTO1.getStatus());
					respBuilder.put("username", userDTO1.getFirstName() + " " + userDTO1.getLastName());
					respBuilder.put("firstname", userDTO1.getFirstName());
					respBuilder.put("lastname", userDTO1.getLastName());
				}
			} else {
				log.info("forgotPasswordVerification Otp is not valid..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("forgotPasswordVerification AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("forgotPasswordVerification JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("forgotPasswordVerification method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will authenticate user credentials username and password.
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/auth")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response authAccount(String request) throws JsonProcessingException {

		log.info("authAccount method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = true;

		try {
			UserDTO userDTO = JsonMapper.objectMapper.readValue(request, UserDTO.class);
			if (userDTO != null && userDTO.getUsername() != null && userDTO.getPassword() != null) {
				userDTO = userService.validateUser(userDTO.getUsername(), userDTO.getPassword(),
						RegisterUsing.EASEMYGST);
				if (userDTO != null && userDTO.getId() > 0) {
					flag = false;
					if (userDTO.getActive() == 0) {
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
								String.format(ApplicationMetadata.SIGNIN_SUCCESS_MSG, userDTO.getFirstName()));
						respBuilder.put("active", 0);
					} else {
						userDTO.setCreationIpAddress(httpRequest.getRemoteAddr());
						String token = tokenService.generateToken(userDTO);
						log.info("authAccount successfully executed for user {}..", userDTO.getUsername());
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
								String.format(ApplicationMetadata.SIGNIN_SUCCESS_MSG, userDTO.getFirstName()));
						if (token != null) {
							SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
							// getting last login time here
							long temp = Long.valueOf(userDTO.getPassword());
							String lastTime = sdf.format(new Date(temp)) + " IST";
							List<String> financialyear=Arrays.asList("2018-2019","2017-2018");
							respBuilder.put(ApplicationMetadata.RESP_AUTH_TOKEN, token);
							respBuilder.put("username", userDTO.getFirstName() + " " + userDTO.getLastName());
							respBuilder.put("firstname", userDTO.getFirstName());
							respBuilder.put("lastname", userDTO.getLastName());
							respBuilder.put("lastlogin", lastTime);
							respBuilder.put("active", 1);
							respBuilder.put("orgname", userDTO.getOrganisation().getName());
							respBuilder.put("orgemail", userDTO.getOrganisation().getPlanCode());
							respBuilder.put("myorg", userDTO.getOrganisation().getStatus());
							respBuilder.put("plantype", userDTO.getOrganisation().getCode());
							respBuilder.put("card", userDTO.getOrganisation().getCardUpdate());
							respBuilder.put("financialyears",financialyear);
							respBuilder.put("cfyear",CalanderCalculatorUtility.getCurrentFinancialYear());
							if(!StringUtils.isEmpty(userDTO.getOrganisation().getSubscriptionMode()) && userDTO.getOrganisation().getSubscriptionMode().equalsIgnoreCase("yes")) {
								respBuilder.put("trial", userDTO.getOrganisation().getSubscriptionMode());
								respBuilder.put("trial_days", userDTO.getOrganisation().getUnits());
							} else {
								respBuilder.put("trial", "no");
								respBuilder.put("trial_days", -1);
							}
							if (!Objects.isNull(userDTO.getIsChangeRequired()) && userDTO.getIsChangeRequired() == 1)
								respBuilder.put("isChangeRequired", 1);
							else
								respBuilder.put("isChangeRequired", 0);
						}
					}
				}
			}
			if (flag) {
				log.info("authAccount fails to authenticate user {}..", userDTO.getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("authAccount AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("authAccount JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("authAccount method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * This will authenticate user credentials username and password.
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/secure/auth")
	@Produces(MediaType.APPLICATION_JSON)
	public Response secureAuthAccount(String request) throws JsonProcessingException {

		log.info("secureAuthAccount method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = true;

		try {
			UserDTO userDTO = JsonMapper.objectMapper.readValue(request, UserDTO.class);
			if (userDTO != null && userDTO.getUsername() != null && userDTO.getPassword() != null) {
				userDTO = userService.secureValidateUser(userDTO.getUsername(), userDTO.getPassword(),
						RegisterUsing.EASEMYGST);
				if (userDTO != null && userDTO.getId() > 0) {
					flag = false;
					if (userDTO.getActive() == 0) {
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
								String.format(ApplicationMetadata.SIGNIN_SUCCESS_MSG, userDTO.getFirstName()));
						respBuilder.put("active", 0);
					} else {
						userDTO.setCreationIpAddress(httpRequest.getRemoteAddr());
						String token = tokenService.generateToken(userDTO);
						log.info("secureAuthAccount successfully executed for user {}..", userDTO.getUsername());
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
								String.format(ApplicationMetadata.SIGNIN_SUCCESS_MSG, userDTO.getFirstName()));
						if (token != null) {
							SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
							// getting last login time here
							long temp = Long.valueOf(userDTO.getPassword());
							String lastTime = sdf.format(new Date(temp)) + " IST";
							respBuilder.put(ApplicationMetadata.RESP_AUTH_TOKEN, token);
							respBuilder.put("username", userDTO.getFirstName() + " " + userDTO.getLastName());
							respBuilder.put("firstname", userDTO.getFirstName());
							respBuilder.put("lastname", userDTO.getLastName());
							respBuilder.put("lastlogin", lastTime);
							respBuilder.put("active", 1);
							if (!Objects.isNull(userDTO.getIsChangeRequired()) && userDTO.getIsChangeRequired() == 1)
								respBuilder.put("isChangeRequired", 1);
							else
								respBuilder.put("isChangeRequired", 0);
						}
					}
				}
			}
			if (flag) {
				log.info("secureAuthAccount fails to authenticate user {}..", userDTO.getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("secureAuthAccount AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("secureAuthAccount JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("secureAuthAccount method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/contact/us")
	@Produces(MediaType.APPLICATION_JSON)
	public Response contactUs(String request) throws JsonProcessingException {

		log.info("contactUs method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			ContactUsDTO contactUsDTO = JsonMapper.objectMapper.readValue(request, ContactUsDTO.class);
			boolean isSaved = userService.saveContactUsData(contactUsDTO);
			if (isSaved) {
				log.info("contactUs successfully completed for the request {}..", request);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.CONTACT_SUCCESS_MSG);
			} else {
				log.info("contactUs fails to complete for the request..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("contactUs JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("contactUs method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/register/partner")
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerPartner(String request) throws JsonProcessingException {

		log.info("registerPartner method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			PartnerDTO partnerDTO = JsonMapper.objectMapper.readValue(request, PartnerDTO.class);
			flag = partnerService.storePartner(partnerDTO);
			if (flag) {
				log.info("registerPartner parter is registered successfully for the request {}..", request);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.PARTNER_VERIFY_SUCCESS_MSG, partnerDTO.getContactNo(),
								partnerDTO.getEmailAddress()));
			} else {
				log.info("registerPartner unable to register the partner..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.info("registerPartner fails to complete..");
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			log.error("registerPartner AppException exception {} ", e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			log.error("registerPartner JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("registerPartner method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/resend/partner/{email}/{mode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response resendPartner(@PathParam("email") String email, @PathParam("mode") String mode)
			throws JsonProcessingException {

		log.info("resendPartner method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String data = email;

		try {
			data = partnerService.resendOtp(email, mode);
			if (!StringUtils.isEmpty(data)) {
				log.info("resendPartner Otp is resend successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.PARTNER_OTP_SUCCESS_MSG, data));
			} else {
				log.info("resendPartner fails to resend the otp..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.info("resendPartner fails to complete..");
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			log.error("resendPartner AppException exception {} ", e.getMessage());
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("resendPartner JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("resendPartner method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/validate/partner/{email}/{emailOtp}/{mobileOtp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validatePartner(@PathParam("email") String email, @PathParam("emailOtp") String emailOtp,
			@PathParam("mobileOtp") String mobileOtp) throws JsonProcessingException {

		log.info("validatePartner method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		PartnerDTO partnerDTO = null;

		try {
			partnerDTO = partnerService.validateOtp(mobileOtp, emailOtp, email);
			if (!Objects.isNull(partnerDTO)) {
				log.info("validatePartner otp is validated for the partner service");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				if (!StringUtils.isEmpty(partnerDTO.getClientBase())
						&& !StringUtils.isEmpty(partnerDTO.getClientBase())) {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.PARTNER_SUCCESS_MSG);
				} else {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.USER_SUCCESS_MSG);
				}
			} else {
				log.info("validatePartner otp is notvaliad for the partner service..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.info("validatePartner fails to complete..");
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			log.error("validatePartner AppException exception {} ", e.getMessage());
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("validatePartner JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("validatePartner method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	@GET
	@Path("/download/template/xls/{key}/{sheet}")
	@Produces("application/vnd.ms-excel")
	public Response getXlsTemplate(@PathParam("key") String key, @PathParam("sheet") String sheet) throws Exception {

		if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(sheet)) {
			File file = ExcelTemplate.getExcelTemplate(key, sheet);

			ResponseBuilder rb = Response.ok((Object) file);
			rb.header("Content-Disposition", "attachment; filename=" + file.getName());
			return rb.build();
		} else {
			throw new Exception();
		}
	}

	@GET
	@Path("/download/xls/{gstin}/{returnType}/{transaction}/{monthYear}/{type}/{token}")
	@Produces("application/vnd.ms-excel")
	public Response getXlsTemplate(@PathParam("returnType") String returnType, @PathParam("type") String type,
			@PathParam("transaction") String transaction, @PathParam("monthYear") String monthYear,
			@PathParam("token") String token, @PathParam("gstin") String gstin) throws Exception {

		try {
			if (!StringUtils.isEmpty(returnType) && !StringUtils.isEmpty(transaction) && !StringUtils.isEmpty(token)
					&& !StringUtils.isEmpty(monthYear) && !StringUtils.isEmpty(type)) {

				boolean authenticationStatus = tokenService.isValidSignature(token);

				if (authenticationStatus) {
					YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
					String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);

					returnType = returnType.toLowerCase();
					transaction = transaction.toLowerCase();

					List<List<String>> data = new ArrayList<>();
					Map<String, List<List<String>>> map = new HashMap<>();
					GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
					gstinTransactionDTO.setMonthYear(monthYear);
					gstinTransactionDTO.setReturnType(returnType);
					gstinTransactionDTO.setTransactionType(transaction);
					gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));
					if (type.equalsIgnoreCase("error"))
						data = returnService.getTransactionByType(gstinTransactionDTO, DataFetchType.INVALID);
					else if (type.equalsIgnoreCase("complete"))
						data = returnService.getTransactionByType(gstinTransactionDTO, DataFetchType.ALL);
					else if (type.equalsIgnoreCase("exception"))
						map = returnService.getExceptionTransactions(gstinTransactionDTO);

					File file = null;
					if (type.equalsIgnoreCase("exception")) {
						file = ExcelTemplate.getExcelTemplateData(returnType, transaction, map);
					} else {
						if (type.equalsIgnoreCase("error"))
						file = ExcelTemplate.getExcelTemplateErrorData(returnType, transaction, data);
						else if(type.equalsIgnoreCase("sample")){
							file=ExcelTemplate.getSampleData(gstinTransactionDTO);
							if(Objects.isNull(file))
								log.error("no file found with that name----{}");

						}
						else
							file = ExcelTemplate.getExcelTemplateData(returnType, transaction, data);	
					}

					if (file == null)
						throw new Exception();

					
					String fileName = file.getName();
					String name ="sample".equalsIgnoreCase(type)?fileName: fileName.split(":")[1];
					ResponseBuilder rb = Response.ok((Object) file);
					rb.header("Content-Disposition", "attachment; filename=" + name);
					return rb.build();
				} else {
					throw new WebApplicationException(Status.UNAUTHORIZED);
				}
			} else {
				throw new Exception();
			}
		} catch (AppException e) {
			log.error("getXlsTemplate AppException exception ", e);
		} catch (JsonException e) {
			log.error("getXlsTemplate JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			log.error("getXlsTemplate Exception exception {} ", e.getMessage());
			e.printStackTrace();
		}
		
		return null;

	}

	/*@GET
	@Path("/download/sample/xls/{gstin}/{returnType}/{transaction}/{monthYear}/{type}/{token}")
	@Produces("application/vnd.ms-excel")
	public Response getSampleXlsTemplate(@PathParam("returnType") String returnType, @PathParam("type") String type,
			@PathParam("transaction") String transaction, @PathParam("monthYear") String monthYear,
			@PathParam("token") String token, @PathParam("gstin") String gstin) throws Exception {

		try {
			if (!StringUtils.isEmpty(returnType) && !StringUtils.isEmpty(transaction) && !StringUtils.isEmpty(token)
					&& !StringUtils.isEmpty(monthYear) && !StringUtils.isEmpty(type)) {

				boolean authenticationStatus = tokenService.isValidSignature(token);

				if (authenticationStatus) {

					returnType = returnType.toLowerCase();
					transaction = transaction.toLowerCase();

					List<List<String>> data = new ArrayList<>();
					Map<String, List<List<String>>> map = new HashMap<>();
					GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
					gstinTransactionDTO.setMonthYear(monthYear);
					gstinTransactionDTO.setReturnType(returnType);
					gstinTransactionDTO.setTransactionType(transaction);
					gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin));
					
					

					File file = null;
							file = ExcelTemplate.getSampleData(gstinTransactionDTO);	

					if (file == null)
						throw new Exception();

					String fileName = file.getName();
					String name = fileName.split(":")[1];
					ResponseBuilder rb = Response.ok((Object) file);
					rb.header("Content-Disposition", "attachment; filename=" + name);
					return rb.build();
				} else {
					throw new WebApplicationException(Status.UNAUTHORIZED);
				}
			} else {
				throw new Exception();
			}
		} catch (AppException e) {
			log.error("getXlsTemplate AppException exception {} ", e.getMessage());
		} catch (JsonException e) {
			log.error("getXlsTemplate JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		}
		return null;

	}*/
	
	
	
	@SuppressWarnings("finally")
	@POST
	@Path("/get/uploaded/data/key/{token}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getUploadedDataKey(@PathParam("token") String token, String request) throws Exception {

		log.info("getUploadedDataKey method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		try {
			if (!StringUtils.isEmpty(token)) {

				boolean authenticationStatus = tokenService.isValidSignature(token);

				if (authenticationStatus) {
					String keyVal = UUID.randomUUID() + "";
					String value = request.replaceAll("\\\\", "");
					if (!StringUtils.isEmpty(value)) {
						if (value.charAt(0) != '[') {
							value = value.substring(1, value.length() - 1);
						}
					}
					redisService.setValue(keyVal, value, true, 3000);
					log.info("getUploadedDataKey keys are successfully uploaded the request {}", request);
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, keyVal);
				} else {
					log.info("getUploadedDataKey Unable to upload the keys..");
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				}

			} else {
				throw new Exception();
			}
		} catch (AppException e) {
			log.error("getUploadedDataKey AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getUploadedDataKey JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getUploadedDataKey method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@GET
	@Path("/download/excel/{token}/{requestDataKey}")
	@Produces("application/vnd.ms-excel")
	public Response downloadDataInExcel(@PathParam("token") String token,
			@PathParam("requestDataKey") String requestDataKey) throws Exception {

		log.info("downloadDataInExcel method starts..");

		if (!StringUtils.isEmpty(token)) {

			boolean authenticationStatus = tokenService.isValidSignature(token);
			String requestData = redisService.getValue(requestDataKey);
			if (authenticationStatus) {

				// converting list of list
				List<String> headersList = new ArrayList<>();
				List<List<String>> data = new ArrayList<>();
				JSONArray jsonArray = new JSONArray(requestData);

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject json = jsonArray.getJSONObject(i);
					Iterator<String> keys = json.keys();
					List<String> valuesList = new ArrayList<>();
					while (keys.hasNext()) {
						String key = keys.next();
						if (i == 0) {
							headersList.add(key);
							log.info("Key :" + key);
						}
						valuesList.add(json.get(key).toString());
						log.info("Values :" + json.get(key));
					}
					data.add(valuesList);
				}
				data.add(0, headersList);
				log.info("downloadDataInExcel data.." + data.toString());

				File file = ExcelTemplate.getExcelFromListData(data);
				log.info("downloadDataInExcel generated file..");

				ResponseBuilder rb = Response.ok((Object) file);
				String fileName = file.getName();
				String name = fileName.split(":")[1];
				rb.header("Content-Disposition", "attachment; filename=" + name);
				return rb.build();
			} else {
				log.error("downloadDataInExcel exits unauthorised access");
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
		} else {
			log.error("downloadDataInExcel exits unauthorised access");
			throw new Exception();
		}
	}

	@GET
	@Path("/download/o365/template")
	@Produces("application/vnd.ms-excel")
	public Response downloadO365Template() throws Exception {

		log.info("downloadO365Template method starts..");

		final String O365_TEMPLATE = "excel/Final-EasemyGST-O365-Template-V5.xlsx";

		File file = encryptDecrypt.getResourceFile(O365_TEMPLATE, "Final-EasemyGST-O365-Template-V5.xlsx");

		ResponseBuilder rb = Response.ok((Object) file);
		String fileName = file.getName();
		rb.header("Content-Disposition", "attachment; filename=" + fileName);

		log.info("downloadO365Template method ends..");
		return rb.build();
	}
	
	@GET
	@Path("/download/syncprocesspdf")
	@Produces("application/pdf")
	public Response downloadSyncProcessPdf() throws Exception {

		log.info("downloadSyncProcessPdf method starts..");

		final String O365_TEMPLATE = "excel/ProcesstoSyncEMG-PPT.pdf";

		File file = encryptDecrypt.getResourceFile(O365_TEMPLATE, "excel/ProcesstoSyncEMG-PPT.pdf");

		ResponseBuilder rb = Response.ok((Object) file);
		String fileName = file.getName();
		rb.header("Content-Disposition", "attachment; filename=" + fileName);

		log.info("downloadSyncProcessPdf method ends..");
		return rb.build();
	}

	@GET
	@Path("/download/invoice/{token}/{gstin}/{sno}")
	@Produces("application/pdf")
	public Response downloadPdfInvoice(@PathParam("token") String token, @PathParam("gstin") String gstin,
			@PathParam("sno") Integer sno) throws Exception {

		log.info("downloadPdfInvoice method starts..");

		if (!StringUtils.isEmpty(token)) {

			boolean authenticationStatus = tokenService.isValidSignature(token);
			if (authenticationStatus) {
				String filePath = taxpayerService.generateInvoicePdf(gstin, sno);
				File file = new File(filePath);

				ResponseBuilder rb = Response.ok((Object) file);
				String fileName = file.getName();
				rb.header("Content-Disposition", "attachment; filename=" + fileName);
				return rb.build();
			} else {
				log.error("downloadPdfInvoice exits unauthorised access");
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
		} else {
			log.error("downloadPdfInvoice exits unauthorised access");
			throw new Exception();
		}
	}

	@POST
	@Path("/send/email/{token}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendEmail(@PathParam("token") String token, String request) throws Exception {

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		if (!StringUtils.isEmpty(token)) {

			log.info("sendEmail starts..");

			boolean authenticationStatus = tokenService.isValidSignature(token);
			JSONObject jsonObject = new JSONObject(request);
			if (authenticationStatus) {

				// converting list of list
				List<String> headersList = new ArrayList<>();
				List<List<String>> data = new ArrayList<>();

				String value = jsonObject.get("requestData").toString().replaceAll("\\\\", "");
				value = value.substring(1, value.length() - 1);
				JSONArray jsonArray = new JSONArray(value);

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject json = jsonArray.getJSONObject(i);
					Iterator<String> keys = json.keys();
					List<String> valuesList = new ArrayList<>();
					while (keys.hasNext()) {
						String key = keys.next();
						if (i == 0) {
							headersList.add(key);
						}
						valuesList.add(json.get(key).toString());
					}
					data.add(valuesList);
				}
				data.add(0, headersList);

				jsonObject.remove("requestData");
				String emailRequestData = jsonObject.toString();

				File file = ExcelTemplate.getExcelFromListData(data);
				EmailDTO emailDTO = JsonMapper.objectMapper.readValue(emailRequestData, EmailDTO.class);

				EmailFactory emailFactory = new EmailFactory();
				emailDTO.setAttachments(file.getPath());
				boolean emaiLFlag = emailFactory.sendEmailUsingEmailDTO(emailDTO);

				if (emaiLFlag) {
					log.info("sendEmail Email is successfully send");
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				} else {
					log.info("sendEmail Fails to send the email..");
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				}

				log.info("sendEmail method ends..");
				Response response = Response.status(200).entity(respBuilder).build();
				return response;

			} else {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
		} else {
			throw new Exception();
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/headers/template/xls/{key}/{sheet}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getXlsTemplateHeaders(@PathParam("key") String key, @PathParam("sheet") String sheet)
			throws Exception {

		log.info("getXlsTemplateHeaders method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(sheet)) {
				Map<String, String> headers = ExcelTemplate.getExcelSheetTemplateHeaders(key, sheet);

				if (!Objects.isNull(headers)) {
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, headers);
				}
				/*
				 * if (sheet.equalsIgnoreCase("all")) { Map<String, String>
				 * headers = ExcelTemplate.getExcelSheetTemplateHeaders(key,
				 * sheet);
				 * 
				 * if (!Objects.isNull(headers)) {
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA, headers); } }
				 * else { String string =
				 * ExcelTemplate.getExcelTemplateHeaders(key, sheet);
				 * 
				 * if (!StringUtils.isEmpty(string)) {
				 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
				 * ExceptionCode._SUCCESS);
				 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
				 * ApplicationMetadata.SUCCESS_MSG);
				 * respBuilder.put(ApplicationMetadata.RESP_DATA, string); } }
				 */
			} else {
				throw new Exception();
			}
		} finally {
			log.info("getXlsTemplateHeaders method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/states")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllNonGovtStates() throws JsonProcessingException {

		log.info("getAllNonGovStates method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<NonGovStateDTO> stateDTOs = finderService.getAllNonGovState();
			if (!Objects.isNull(stateDTOs)) {
				log.info("getAllNonGovStates received size {}..", stateDTOs.size());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, stateDTOs);
			} else {
				log.info("getAllNonGovStates fail to retrieve states..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getAllNonGovStates AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getAllNonGovStates Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("ggetAllNonGovStates method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/cities/{stateCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCitiesByState(@PathParam("stateCode") String stateCode) throws JsonProcessingException {

		log.info("getAllCitiesByStates method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<CityDTO> cityDTOs = finderService.getAllCities(Integer.parseInt(stateCode));
			if (!Objects.isNull(cityDTOs)) {
				log.info("getAllCitiesByStates received size for the all cities {}..", cityDTOs.size());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, cityDTOs);
			} else {
				log.info("getAllCitiesByStates  fail to retrieve cities ..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getAllCitiesByStates  AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getAllCitiesByStates  Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getAllCitiesByStates  method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/url/fetch/{method}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDataFromUrlFetch(@PathParam("method") String method, String request)
			throws JsonProcessingException, AppException {

		log.info("getDataFromUrlFetch method starts.." + method);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		JSONObject data = new JSONObject(request);
		String output = "";

		try {
			if (!StringUtils.isEmpty(method)) {
				if (method.equalsIgnoreCase("post")) {
					output = UrlFetch.callURL(data.getString("url"), data.get("data").toString());
				} else if (method.equalsIgnoreCase("get")) {
					output = UrlFetch.callURL(data.getString("url"));
				}
			}
			if (!StringUtils.isEmpty(output)) {
				log.info("getDataFromUrlFetch data is successfully fetched form the url");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);
			} else {
				log.info("getDataFromUrlFetch unable to fetch the data from the url..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("getDataFromUrlFetch JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getEmployeeData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/save/helptext")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveHelpText(String request) throws JsonProcessingException {

		log.info("saveHelpText method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!StringUtils.isEmpty(request)) {
				HelpContentDTO helpContentDTO = JsonMapper.objectMapper.readValue(request, HelpContentDTO.class);
				flag = masterService.saveHelpContent(helpContentDTO);
			}
			if (flag) {
				log.info("saveHelpText successfully completed");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("saveHelpText fails to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("saveHelpText  AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("saveHelpText  Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("saveHelpText  method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/helptext/{link}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHelpText(@PathParam("link") String link) throws JsonProcessingException {

		log.info("getHelpText method starts.." + link);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			HelpContentDTO contentDTO = masterService.getHelpContent(link);
			if (!Objects.isNull(contentDTO)) {
				log.info("getHelpText successfully completed");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, contentDTO);
			} else {
				log.info("getHelpText fails to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getHelpText  AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getHelpText  Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getHelpText  method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}

	}
	
	@SuppressWarnings({ "unused", "finally" })
	@GET
	@Path("/get/notification/{token}/")
	@Produces("text/event-stream")
	
	public String getNotificationsTemp(@PathParam("token") String token) throws JsonProcessingException {

		log.info("getNotifications method starts ");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		StringBuffer data = new StringBuffer();
		try {
			if (!StringUtils.isEmpty(token)) {
				boolean authenticationStatus = tokenService.isValidSignature(token);
				JSONObject userAuth = new JSONObject();
				if (authenticationStatus) {
					Map<String,Object>countMap=new HashMap<>();
					List<String>categories=new ArrayList<>();
					categories.add(NotificationCategory.EMGST_NOTIFICATION.toString());
					List<Boolean>isSeens=new ArrayList<>();
					isSeens.add(Boolean.FALSE);
					countMap=notificationService.getNotificationCount( categories, isSeens);
					String countData=JsonMapper.objectMapper.writeValueAsString(countMap);
					
					userAuth.put("authenticated", 1);
					data.append("data: {\n");
					data.append("data:\"user\":" + userAuth.toString() + ",\n");
					data.append("data:\"notificationCounts\":"+countData+",\n");
					data.append("data:\"brodcastMessage\":"+new ObjectMapper().convertValue(notificationService.getSSEBroadcastMessaage(),com.fasterxml.jackson.databind.JsonNode.class)+"\n");
					data.append("retry: 600000\n");
					data.append("data: }\n\n");
				} else {
					userAuth.put("authenticated", 1);
					data.append("data: {\n");
					data.append("data:\"user\":" + userAuth.toString() + "\n");
					data.append("retry: 600000\n");
					data.append("data: }\n\n");
				}
				log.info("getNotifications successfully completed");
			} else {
				log.info("getNotifications fails to complete..");
				data.append("data: {\n");
				data.append("retry: 600000\n");
				data.append("data: }\n\n");
			}
		} finally {
			log.info("getNotifications  method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return data.toString();
		}
	}

	@SuppressWarnings({ "unused", "finally" })
	@GET
	@Path("/get/notification/{token}/{gstin}/{monthYear}")
	@Produces("text/event-stream")
	public String getNotifications(@Context
			HttpServletRequest request,@PathParam("token") String token, @PathParam("gstin") String gstin,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getNotifications method starts for gstin {} monthYear {}", gstin);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		StringBuffer data = new StringBuffer();
		try {
			if (!StringUtils.isEmpty(token) && !StringUtils.isEmpty(gstin) && !StringUtils.isEmpty(monthYear)) {
				boolean authenticationStatus = tokenService.isValidSignature(token);
				JSONObject userAuth = new JSONObject();
				if (authenticationStatus) {
					authService.validateToken(token, request.getRemoteAddr());
					Map<String,Object>countMap=new HashMap<>();
					List<String>categories=new ArrayList<>();
					categories.add(NotificationCategory.EMGST_NOTIFICATION.toString());
					List<Boolean>isSeens=new ArrayList<>();
					isSeens.add(Boolean.FALSE);
					countMap=notificationService.getNotificationCount( categories, isSeens);
					String countData=JsonMapper.objectMapper.writeValueAsString(countMap);
					if (gstin.equalsIgnoreCase("all")) {
						userAuth.put("authenticated", 1);
						data.append("data: {\n");
						data.append("data:\"user\":" + userAuth.toString() + ",\n");
						data.append("data:\"gstin\":" + "0" + ",\n");
						data.append("data:\"notificationCounts\":"+countData+",\n");
						data.append("data:\"brodcastMessage\":"+new ObjectMapper().convertValue(notificationService.getSSEBroadcastMessaage(),com.fasterxml.jackson.databind.JsonNode.class)+"\n");
						data.append("retry: 600000\n");
						data.append("data: }\n\n");
					} else if (monthYear.equalsIgnoreCase("all")) {
						userAuth.put("authenticated", 1);
						String gstinAuthOutput = taxpayerService.getGstinAuthTokenValidity(gstin);
						data.append("data: {\n");
						data.append("data:\"user\":" + userAuth.toString() + ",\n");
						data.append("data:\"gstin\":" + gstinAuthOutput + ",\n");
						data.append("data:\"notificationCounts\":"+countData+",\n");
						data.append("data:\"brodcastMessage\":"+new ObjectMapper().convertValue(notificationService.getSSEBroadcastMessaage(),com.fasterxml.jackson.databind.JsonNode.class)+"\n");
						data.append("retry: 600000\n");
						data.append("data: }\n\n");
					} else {
						userAuth.put("authenticated", 1);
						String gstinAuthOutput = taxpayerService.getGstinAuthTokenValidity(gstin);
						data.append("data: {\n");
						data.append("data:\"user\":" + userAuth.toString() + ",\n");
						data.append("data:\"gstin\":" + gstinAuthOutput + ",\n");
						data.append("data:\"notificationCounts\":"+countData+",\n");
						data.append("data:\"brodcastMessage\":"+new ObjectMapper().convertValue(notificationService.getSSEBroadcastMessaage(),com.fasterxml.jackson.databind.JsonNode.class)+"\n");
						data.append("retry: 600000\n");
						data.append("data: }\n\n");
					}
				} else {
					userAuth.put("authenticated", 0);
					data.append("data: {\n");
					data.append("data:\"user\":" + userAuth.toString() + "\n");
					data.append("retry: 600000\n");
					data.append("data: }\n\n");
				}
				log.info("getNotifications successfully completed");
			} else {
				log.info("getNotifications fails to complete..");
				data.append("data: {\n");
				data.append("retry: 600000\n");
				data.append("data: }\n\n");
			}
		} catch (AppException e) {
			log.error("getNotifications  AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} finally {
			log.info("getNotifications  method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return data.toString();
		}
	}
	
	
	@SuppressWarnings("finally")
	@GET
	@Path("/authUrl")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getauthUrl() throws JsonProcessingException {

		log.info("getOauth method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		URI uri=null;

		try {
//			QBService qbService=new QBServiceImpl();
//			String url=qbService.authUrl();
//			uri=new URI(url);
			//ResponseBuilder resp=Response.temporaryRedirect(uri);
			
		
//			if (resp!=null) {
//				log.info("getOauth updated entry successfully for id {}");
//				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
//				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
//			} else {
//				log.info("getOauth Unable to add the user");
//				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
//				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
//			}
		}  catch (JsonException e) {
			log.error("getOauth JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getOauth method ends..");
			Response response =  Response.temporaryRedirect(uri).build();
			return response;
		}
	}
	
	@SuppressWarnings("finally")
	@POST
	@Path("/account/create/new")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNewAccount(String request) throws JsonProcessingException {

		log.info("createNewAccount method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		String username = "", name = "", company = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("username") && jsonObject.has("name") && jsonObject.has("company")) {
			username = jsonObject.getString("username");
			name = jsonObject.getString("name");
			company = jsonObject.getString("company");
		}

		try {
			flag = userService.createUserAccount(username, name, company);
			if (flag) {
				log.info("createNewAccount completed successfully {}", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("createNewAccount fails to create user {}..", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("createNewAccount AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("createNewAccount JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("createNewAccount method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	@SuppressWarnings("finally")
	@GET
	@Path("/get/report/url/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportUrl(@PathParam("username") String username) throws JsonProcessingException {

		log.info("getReportUrl method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		Map<String,Object> dataMap=null;
		try {
			ExagoReportHelper reportHelper = new ExagoReportHelper();
			UserDTO userDto = userService.getUserProfileInfo(username);
			dataMap = reportHelper.getReportUrlWithParam(userDto, true, "", "");
			if (Objects.isNull(dataMap)) {
				log.info("getReportUrl created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, dataMap);
			} else {
				log.info("getReportUrl Partner Code fails to submit");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReportUrl AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getReportUrl JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getReportUrl method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
	@GET
	@Path("/download/notification/{fileName}/{token}")
	@Produces("application/pdf")
	public Response downloadNotificationFile(@PathParam("fileName") String fileName,@PathParam("token") String  token) throws Exception {
		log.info("downloadNotificationFile method starts..");
		boolean authenticationStatus = tokenService.isValidSignature(token);
		if(!authenticationStatus)
			throw new WebApplicationException(Status.UNAUTHORIZED);
		GstinTransactionDTO gstinTransactionDto=new GstinTransactionDTO();
		File file = ExcelTemplate.getPublicNotificationFile(gstinTransactionDto, fileName);
		ResponseBuilder rb = Response.ok((Object) file);
		rb.header("Content-Disposition", "inline; filename=" + fileName);
		log.info("downloadNotificationFile method ends..");
		return rb.build();
	}
	
	
	@GET
	@Path("/download/{fileName}/{token}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadInwardCompresionReport(@PathParam("fileName") String fileName,@PathParam("token") String  token) throws Exception {
         		log.info("download method starts..");
        		boolean authenticationStatus = tokenService.isValidSignature(token);
         		if(!authenticationStatus)
        			throw new WebApplicationException(Status.UNAUTHORIZED);
				File file = new File(
						ManipulateFile.getParentDirBasePath() + File.separator +fileName);
				log.debug(file.getAbsolutePath());
				ResponseBuilder rb = Response.ok((Object) file);
				rb.header("Content-Disposition", "attachment; filename=" + fileName);
				log.info("download method ends..");
				return rb.build();
	}
	

}
