package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

/**
 * 
 * @author Charu Chandra Joshi
 *
 */
@RequestScoped
@Path("/dashboardws")
@Produces("application/json")
@Consumes("application/json")
public class DashboardWebService implements Serializable  {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private TaxpayerService taxpayerService;

	
	@SuppressWarnings("finally")
	@GET
	@Path("/get/prefernce/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilePreference(@PathParam("gstin") String gstin, @QueryParam("financialYear") String fyear) throws JsonProcessingException {
		log.info("getFilePreference method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			if(StringUtils.isEmpty(fyear))
				fyear=CalanderCalculatorUtility.getCurrentFinancialYear();
			Map<String, Object> preferenceMap=	taxpayerService.getfileprefdeshboard(gstin,fyear);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			respBuilder.put(ApplicationMetadata.RESP_SUMMARY,preferenceMap);
		} catch (AppException e) {
			log.error("getFilePreference AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getFilePreference JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
		} finally {
			log.info("getFilePreference method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	
}
