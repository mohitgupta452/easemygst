package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.dto.CashSummaryDTO;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.LedgerDto;
import com.ginni.easemygst.portal.business.dto.MonthSummaryDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.FunctionalService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.filing.EsignFilling;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

@RequestScoped
@Path("/ledgerws")
@Produces("application/json")
@Consumes("application/json")
public class LedgerWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private ReturnsService returnService;

	@Inject
	private FunctionalService functionalService;
	
	@Inject 
	private RedisService redisService;

	/**
	 * This method is used to get tin dashboard summary
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/ledger/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLedger(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("getLedger method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!Objects.isNull(gstin)) {
				YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
				String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
				List<MonthSummaryDTO> monthSummaryDTOs = taxpayerService.getGstReturnSummaryByGstin(gstin);
				if (!Objects.isNull(monthSummaryDTOs)) {
					MonthSummaryDTO monthSummaryDTO = monthSummaryDTOs.get(0);
					GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
					gstinTransactionDTO.setMonthYear(monthSummaryDTO.getCode());
					gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

					log.info("getLedger getting GSTR1 data for {}", monthSummaryDTO.getMonthName());
					gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString());
					GstrSummaryDTO outSummaryDTO = null;//returnService.getTransactionSummary(gstinTransactionDTO);

					log.info("getLedger getting GSTR2 data for {}", monthSummaryDTO.getMonthName());
					gstinTransactionDTO.setReturnType(ReturnType.GSTR2.toString());
					GstrSummaryDTO inSummaryDTO = null;//returnService.getTransactionSummary(gstinTransactionDTO);

					CashSummaryDTO cashSummaryDTO = new CashSummaryDTO();

					log.info("getLedger successfully completed {}", monthSummaryDTO.getMonthName());
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, monthSummaryDTOs);
					respBuilder.put(ApplicationMetadata.RESP_OUTWARD, outSummaryDTO);
					respBuilder.put(ApplicationMetadata.RESP_INWARD, inSummaryDTO);
					respBuilder.put("cash", cashSummaryDTO);
				}
			} else {
				log.info("getLedger fail to complete the request..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getLedger JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getLedger method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method is used to get tin dashboard summary
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/utilize/credit/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response utilizeCredit(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("utilizeCredit method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!Objects.isNull(gstin)) {

				List<MonthSummaryDTO> monthSummaryDTOs = taxpayerService.getGstReturnSummaryByGstin(gstin);
				if (!Objects.isNull(monthSummaryDTOs)) {
					YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
					String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
					MonthSummaryDTO monthSummaryDTO = monthSummaryDTOs.get(0);
					GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
					gstinTransactionDTO.setMonthYear(monthSummaryDTO.getCode());
					gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

					// call function to utilize credit
					String output = functionalService.utilizeCredit(gstinTransactionDTO);

					JSONObject json = new JSONObject(output);

					if (json.has("error") && json.get("error").equals("false")) {
						log.info("utilizeCredit successfully completed {}",
								monthSummaryDTO.getMonthName());
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
						respBuilder.put(ApplicationMetadata.RESP_DATA, monthSummaryDTOs);
						respBuilder.put(ApplicationMetadata.RESP_OUTWARD, json.get("outward").toString());
						respBuilder.put(ApplicationMetadata.RESP_INWARD, json.get("inward").toString());
						respBuilder.put("diff", json.get("diff").toString());
						respBuilder.put("balLiab", json.get("balLiab").toString());
						respBuilder.put("balCredit", json.get("balCredit").toString());
					} else {
						log.info("utilizeCredit fail to complete with errors..");
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
					}
				}
			} else {
				log.info("utilizeCredit fail to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("utilizeCredit JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("utilizeCredit method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method is used to get tin dashboard summary
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/refund/credit/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response refundCredit(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("refundCredit method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!Objects.isNull(gstin)) {
				log.info("refundCredit credit is successfully refunded {}", monthYear);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("refundCredit Unable to refund the credit..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("refundCredit JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("refundCredit method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method is used to get tin dashboard summary
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/refund/cash/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response refundCash(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("refundCash method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!Objects.isNull(gstin)) {
				log.info("refundCash cash is successfully refunded {}", monthYear);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("refundCash unable to refund the cash..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("refundCash JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("refundCash method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method is used to get tin dashboard summary
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/utilize/cash/{gstin}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response utilizeCash(@PathParam("gstin") String gstin, @PathParam("monthYear") String monthYear)
			throws JsonProcessingException {

		log.info("utilizeCash method starts ..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!Objects.isNull(gstin)) {

				List<MonthSummaryDTO> monthSummaryDTOs = taxpayerService.getGstReturnSummaryByGstin(gstin);
				if (!Objects.isNull(monthSummaryDTOs)) {
					YearMonth yearmonth=YearMonth.parse(monthYear, DateTimeFormatter.ofPattern("MMyyyy"));
					String fyear=CalanderCalculatorUtility.getFiscalYearByMonthYear(yearmonth);
					MonthSummaryDTO monthSummaryDTO = monthSummaryDTOs.get(0);
					GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
					gstinTransactionDTO.setMonthYear(monthSummaryDTO.getCode());
					gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin,fyear));

					// call function to utilize cash
					String output = functionalService.utilizeCash(gstinTransactionDTO);

					JSONObject json = new JSONObject(output);

					if (json.has("error") && json.get("error").equals("false")) {
						log.info("utilizeCash successfully completed {}",
								monthSummaryDTO.getMonthName());
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
						respBuilder.put(ApplicationMetadata.RESP_DATA, monthSummaryDTOs);
						respBuilder.put(ApplicationMetadata.RESP_OUTWARD, json.get("outward").toString());
						respBuilder.put(ApplicationMetadata.RESP_INWARD, json.get("inward").toString());
						respBuilder.put("diff", json.get("diff").toString());
						respBuilder.put("balance", json.get("balance").toString());
					} else {
						log.info("utilizeCash fail to complete with errors..");
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
					}
				}
			} else {
				log.info("utilizeCash fail to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("utilizeCash JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("utilizeCash method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	// will return dummy data
	@SuppressWarnings("finally")
	@GET
	@Path("/get/cashledger/{gstin}/{fromDate}/{toDate}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCashLedgerDetails(@PathParam("gstin") String gstin, @PathParam("fromDate") String fromDate,
			@PathParam("toDate") String toDate) throws JsonProcessingException {

		log.info("getCashLedgerDetails method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		LedgerDto cld = new LedgerDto();

		SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
		cld.setGstin(gstin);
		try {
			cld.setFromDate(sdf.parse(fromDate));
			cld.setToDate(sdf.parse(toDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		try {
			if (!Objects.isNull(gstin)) {

				// call function to cash
				String output = functionalService.getCashLedgerDetails(cld);
				log.info("getCashLedgerDetails completed from date  {} to date {}", fromDate, toDate);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);

			} else {
				log.info("getCashLedgerDetails unable to get the cash ledger details..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getCashLedgerDetails JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getCashLedgerDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	// will return dummy data
	@SuppressWarnings("finally")
	@GET
	@Path("/get/itcledger/{gstin}/{fromDate}/{toDate}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getItcLedgerDetails(@PathParam("gstin") String gstin, @PathParam("fromDate") String fromDate,
			@PathParam("toDate") String toDate) throws JsonProcessingException {

		log.info("getItcLedgerDetails..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		LedgerDto cld = new LedgerDto();

		SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
		cld.setGstin(gstin);
		try {
			cld.setFromDate(sdf.parse(fromDate));
			cld.setToDate(sdf.parse(toDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		try {
			if (!Objects.isNull(gstin)) {

				// call function to cash
				String output = functionalService.getItcLedgerDetails(cld);
				log.info("getItcLedgerDetails completed from date  {} to date {}", fromDate, toDate);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);

			} else {
				log.info("getItcLedgerDetails unable to get Itc Ledger details..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getItcLedgerDetails JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getItcLedgerDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	// will return dummy data
	@SuppressWarnings("finally")
	@GET
	@Path("/get/liabilityledger/{gstin}/{taxPeriod}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLiabilityLedgerDetails(@PathParam("gstin") String gstin,
			@PathParam("taxPeriod") String taxPeriod) throws JsonProcessingException {

		log.info("getLiabilityLedgerDetails method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		LedgerDto cld = new LedgerDto();
		cld.setGstin(gstin);
		cld.setMonthYear(taxPeriod);

		try {
			if (!Objects.isNull(gstin)) {

				// call function to liability
				String output = functionalService.getLiabilityLedgerDetails(cld);
				log.info("getLiabilityLedgerDetails completed monthyear  {} ", taxPeriod);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);

			} else {
				log.info("getLiabilityLedgerDetails unable to get the liability ledger details..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getLiabilityLedgerDetails JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getLiabilityLedgerDetails method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	@SuppressWarnings("finally")
	@GET
	@Path("/initiate/filling/{gstin}/{returnType}/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response initiateFilling(@PathParam("gstin") String gstin, @PathParam("returnType") String returnType,
			@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("initiateFilling method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		
		try {	
			
			if(true)
				throw new AppException(ExceptionCode._FAILURE);
				
			
			GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
		TaxpayerGstinDTO gstinDTO=new TaxpayerGstinDTO();
		gstinDTO.setGstin(gstin);
		gstinTransactionDTO.setTaxpayerGstin(gstinDTO);
		gstinTransactionDTO.setMonthYear(monthYear);
		gstinTransactionDTO.setReturnType(returnType);
		String returnSummary=functionalService.syncGstr1SummaryData(gstinTransactionDTO);

		redisService.setValue(gstin+returnType+monthYear, returnSummary, true,36000);
			//call to aadhar esign service
			String docId = EsignFilling.uploadData(gstin,monthYear,returnType,returnSummary);
			if (!StringUtils.isEmpty(docId)) {
				Map<String, String> output = new HashMap<>();
				output.put("documentId", docId);
				log.info("initiateFilling otp has been sent successfully..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);
			} else {
				log.info("initiateFilling request otp from gstn fails..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} /*catch (AppException e) {
			log.error("getEsignOtp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} */catch (Exception e) {
			log.error("initiateFilling Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("initiateFilling method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	@SuppressWarnings("finally")
	@GET
	@Path("/file/return/{gstin}/{returnType}/{monthYear}/{docId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fileReturn(@PathParam("gstin") String gstin, @PathParam("returnType") String returnType,
			@PathParam("monthYear") String monthYear, @PathParam("docId") String docId) throws JsonProcessingException {

		log.info("fileReturn method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		
		try {
			
			if(true)
				throw new AppException(ExceptionCode._FAILURE);
			
			
			//call to aadhar esign service
			String referenceId = EsignFilling.getSignedData(docId,gstin,monthYear,returnType);
			if (!StringUtils.isEmpty(referenceId)) {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("fileReturn request otp from gstn fails..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} /*catch (AppException e) {
			log.error("getEsignOtp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} */
		catch (AppException e) {
			log.error("file return exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		}
		
		catch (Exception e) {
			log.error("fileReturn Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("fileReturn method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

}
