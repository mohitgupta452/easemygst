package com.ginni.easemygst.portal.ws.impl.admin;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.AdminService;
import com.ginni.easemygst.portal.business.service.ReportService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;

/**
 * 
 * @author Charu Chandra Joshi
 *
 */
@RequestScoped
@Path("/adminws")
@Produces("application/json")
@Consumes("application/json")
public class AdminwebService {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;
	
	@Inject
	private AdminService adminService;
	
	
	@SuppressWarnings("finally")
	@POST
	@Path("/unsync/{returntype}/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response unsync(@PathParam("returntype") String returntype,JsonNode requestJson) throws JsonProcessingException {
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {
			if(!Objects.isNull(requestJson)){
			if(requestJson.has("transitionType")&& requestJson.has("monthYear")&& requestJson.has("gstin")){
			adminService.unSync(requestJson.get("transitionType").asText(),returntype,requestJson.get("gstin").asText(),requestJson.get("monthYear").asText());
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			}
			else{
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.INVALID_JSON_REQUEST);
			}
			}
			else{
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.MISSING_REQUEST_PAYLOAD);
			}
		} catch (AppException e) {
          e.printStackTrace();
		
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	
}
