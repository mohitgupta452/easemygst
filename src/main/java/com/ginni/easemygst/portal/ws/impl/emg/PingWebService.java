package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.helper.config.AppConfig;

@Path("/pingws")
public class PingWebService implements Serializable {

	private static final long serialVersionUID = 1L;

	@GET
	@Path("/ping")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String pingEmg() throws JsonProcessingException {
		
		return "{\"profile\" :\""+AppConfig.getActiveProfile()+"\"}";

	}

}
