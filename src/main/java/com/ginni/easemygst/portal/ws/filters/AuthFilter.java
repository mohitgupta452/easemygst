package com.ginni.easemygst.portal.ws.filters;

import java.io.Serializable;
import java.lang.invoke.MethodHandles;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.auth.RestAccessPoint;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.helper.AuthenticationService;
import com.ginni.easemygst.portal.helper.InsufficentRightException;
import com.ginni.easemygst.portal.helper.config.AppConfig;

@Provider
public class AuthFilter implements ContainerRequestFilter, Serializable {

	private static final long serialVersionUID = 1L;

	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	public static final String AUTHENTICATION_HEADER = "Authorization";

	public static final String ALLOWED_PATH_1 = "/signinws/";
	public static final String ALLOWED_PATH_2 = "/pingws/";
	public static final String ALLOWED_PATH_3 = "/adminws/";
	public static final String ALLOWED_PATH_EXTERNAL = "/extws/";
	public static final String ALLOWED_PATH_EWAYBIL = "/ewbws/";
	public static final String ALLOWED_PATH_MASTER = "/masterws/";
	public static final String ALLOWED_PATH_WIDGIT = "/widgitws/";
	public static final String BYPASS_PATH1="/taxpws/gstin/request/token/";
	

	// public static final String fixedKey = "[B@311d617d";

	// public static final String fixedToken =
	// "eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJFeHRlcm5hbFdvcmxkIiwiaXNzIjoiRWFzZU15R1NUIiwiaXBhIjoiMTI3LjAuMC4xIiwiZXhwIjoxNTU5MjA2MjU4LCJwcm4iOiJleHRlcm5hbC13ZWItc2VydmljZSIsImp0aSI6IjEifQ.QBrCwklrqcjb42fnePomzGgnRz-yZwZf9RR1l2YiShZXEvJHdwqbKrvbFGPxko1UNrb8DC83L8PXh1StdLKlRQ";

	@Inject
	private AuthenticationService authService;
	
	@Context
	private HttpServletRequest httpRequest;
	
	@Inject
	private AppConfig appConfig;
	
	@Inject
	private RestAccessPoint restAccess;
	
	@Inject
	private UserBean userBean;
	
	@Inject
	private UserService userService;

	@Override
	public void filter(ContainerRequestContext containerRequest) throws WebApplicationException  {

		String path = containerRequest.getUriInfo().getPath();
		String ipAddress = httpRequest.getRemoteAddr();
		String gstin="";
		String returnType="";
		//logService.logEmgstRequest(containerRequest);


		if (path.indexOf(ALLOWED_PATH_1) == -1 && path.indexOf(ALLOWED_PATH_2) == -1 &&
				path.indexOf(ALLOWED_PATH_3) == -1 &&
				path.indexOf(ALLOWED_PATH_EXTERNAL) == -1 && path.indexOf(ALLOWED_PATH_MASTER) == -1
				&&  path.indexOf(ALLOWED_PATH_WIDGIT) == -1 &&  path.indexOf(ALLOWED_PATH_EWAYBIL) == -1) {

			String token = containerRequest.getHeaderString(AUTHENTICATION_HEADER);

			if (token != null) {
				boolean authenticationStatus = authService.validateToken(token, ipAddress);
				if (!authenticationStatus) {
					throw new WebApplicationException(Status.UNAUTHORIZED);
				}
				else {
					if (path.indexOf(BYPASS_PATH1) == -1){
					gstin = restAccess.findgstin(path);
					returnType = restAccess.findreturnType(path);
					int userId = userBean.getUserDto().getId();
			    	String type = userService.findUserType(gstin, userId);
					String acesstype = userService.findAccessType(gstin, userId, returnType);
					if (gstin != "" && !type.equals("PRIMARY")) {
						String apitype = restAccess.CheckApiType(path);
						if (!restAccess.checkAcessbilty(apitype, acesstype)) {
                                  throw new InsufficentRightException("Insufficient access rights");
						}
					}
				}
				
				}
			} else {

				throw new WebApplicationException(Status.UNAUTHORIZED);
			}

		}
	}

}
