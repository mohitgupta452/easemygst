package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.PartnerService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.entity.Plan;
import com.ginni.easemygst.portal.reports.ExagoReportHelper;
import com.ginni.easemygst.portal.security.TokenService;
import com.ginni.easemygst.portal.subscription.SubscriptionService;
import com.mashape.unirest.http.JsonNode;

/**
 * 
 * @author Sumeet
 *
 */
@RequestScoped
@Path("/userws")
@Produces("application/json")
@Consumes("application/json")
public class UserWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private RedisService redisService;

	@Inject
	private UserService userService;

	@Inject
	private PartnerService partnerService;

	@Inject
	private SubscriptionService subscriptionService;

	@Context
	private HttpServletRequest httpRequest;

	@Inject
	private TokenService tokenService;

	@Inject
	private UserBean userBean;

	@Inject
	private ApiLoggingService ApiLoggingService;

	/**
	 * This will remove user identity from temp store, if any and consider user
	 * has been logged out completely
	 * 
	 * @param username,
	 *            Authorization
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/account/logout/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountLogout(@PathParam("username") String username, @HeaderParam("Authorization") String auth)
			throws JsonProcessingException {

		log.info("accountLogout method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			redisService.setExpiry(auth, 1);
			redisService.setExpiry(auth + "_KEY", 1);
			log.info("accountLogout user logout successfully {}", username);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
		} catch (JsonException e) {
			log.error("accountLogout JSONException exception {}", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accountLogout method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will change user password after validation old password for the
	 * requested user
	 * 
	 * @param username,
	 *            new-password, old-password
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/change/password")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountChangePassword(String request) throws JsonProcessingException {

		log.info("accountChangePassword method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		String username = "", oldpassword = "", newpassword = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("username") && jsonObject.has("oldpassword")
				&& jsonObject.has("newpassword")) {
			username = jsonObject.getString("username");
			oldpassword = jsonObject.getString("oldpassword");
			newpassword = jsonObject.getString("newpassword");
		}

		try {
			flag = userService.changePassword(username, oldpassword, newpassword);
			if (flag) {

				// ApiLoggingService.saveAction(UserAction.CHANGE_PASSWORD,
				// null);
				log.info("accountChangePassword change password successfully {}", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.CHANGE_PASSWORD_MSG);
			} else {
				log.info("accountChangePassword fails to authenticate user {}..", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("forgotPasswordVerification AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("accountChangePassword JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accountChangePassword method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will change user password after validation old password for the
	 * requested user
	 * 
	 * @param username,
	 *            new-password, old-password
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/reset/password")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountResetPassword(String request) throws JsonProcessingException {

		log.info("accountResetPassword method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		String username = "", newpassword = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("username") && jsonObject.has("newpassword")) {
			username = jsonObject.getString("username");
			newpassword = jsonObject.getString("newpassword");
		}

		try {
			flag = userService.resetPassword(username, newpassword);
			if (flag) {
				log.info("accountResetPassword change password successfully {}", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.CHANGE_PASSWORD_MSG);
			} else {
				log.info("accountResetPassword fails to authenticate user {}..", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("forgotPasswordVerification AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("accountResetPassword JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accountResetPassword method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will change user password after validation old password for the
	 * requested user
	 * 
	 * @param username,
	 *            new-password, old-password
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/create/new")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNewAccount(String request) throws JsonProcessingException {

		log.info("createNewAccount method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		String username = "", name = "", company = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("username") && jsonObject.has("name") && jsonObject.has("company")) {
			username = jsonObject.getString("username");
			name = jsonObject.getString("name");
			company = jsonObject.getString("company");
		}

		try {
			flag = userService.createUserAccount(username, name, company);
			if (flag) {
				log.info("createNewAccount completed successfully {}", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String str = String.format(ApplicationMetadata.NEW_USER_ACCOUNT, username);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, str);
			} else {
				log.info("createNewAccount fails to create user {}..", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("createNewAccount AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("createNewAccount JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("createNewAccount method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will get the user profile
	 * 
	 * @param username,
	 *            new-password, old-password
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/user/profile")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserProfile(String request) throws JsonProcessingException {

		log.info("getUserProfile method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		UserDTO user = null;

		try {
			user = userService.getUserProfileInfo(userBean.getUserDto().getId());
			if (!Objects.isNull(user)) {
				log.info("getUserProfile successfully got data for user {}", userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, user);
			} else {
				log.info("getUserProfile fails to get data for user {}..", userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getUserProfile JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getUserProfile method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will get the user profile
	 * 
	 * @param username,
	 *            new-password, old-password
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/my/organisations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserOrganisations() throws JsonProcessingException {

		log.info("getUserOrganisations method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		Map<String, Object> data = null;

		try {
			data = redisService.getMapStringObject("myorg-" + userBean.getUserDto().getId());
			if (true/* Objects.isNull(data) */) {
				data = userService.getUserOrganisations(userBean.getUserDto().getId());
				// redisService.setMapStringObject("myorg-" +
				// userBean.getUserDto().getId(), data, true, 28800);
			}
			if (!Objects.isNull(data)) {
				log.info("getUserOrganisations successfully got organisations for user {}",
						userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, data);
			} else {
				log.info("getUserOrganisations fails to get organisations for user {}..",
						userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getUserOrganisations JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getUserOrganisations method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will get the user profile
	 * 
	 * @param username,
	 *            new-password, old-password
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/check/units/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkUnits(@PathParam("type") String type) throws JsonProcessingException {

		log.info("checkUnits method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			if (!StringUtils.isEmpty(type) && (type.equalsIgnoreCase("gstin") || type.equalsIgnoreCase("users"))) {
				if (type.equalsIgnoreCase("gstin"))
					flag = userService.checkGstins();
				else
					flag = userService.checkUsers();
			}
			if (flag) {
				log.info("checkUnits successfully implemented", userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("checkUnits fails to complete", userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("checkUnits AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("checkUnits JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("checkUnits method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/org/token/{organisationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrganisationToken(@PathParam("organisationId") Integer organisationId)
			throws JsonProcessingException {

		log.info("getOrganisationToken method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			UserDTO userDTO = userService.checkValidOrganisation(organisationId);
			userDTO.setCreationIpAddress(httpRequest.getRemoteAddr());
			String token = tokenService.generateToken(userDTO);
			log.info("getOrganisationToken successfully executed for user {}..", userDTO.getUsername());
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
					String.format(ApplicationMetadata.SIGNIN_SUCCESS_MSG, userDTO.getFirstName()));
			if (token != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				// getting last login time here
				long temp = Long.valueOf(userDTO.getPassword());
				String lastTime = sdf.format(new Date(temp)) + " IST";
				respBuilder.put(ApplicationMetadata.RESP_AUTH_TOKEN, token);
				respBuilder.put("username", userDTO.getFirstName() + " " + userDTO.getLastName());
				respBuilder.put("firstname", userDTO.getFirstName());
				respBuilder.put("lastname", userDTO.getLastName());
				respBuilder.put("lastlogin", lastTime);
				respBuilder.put("active", 1);
				respBuilder.put("orgname", userDTO.getOrganisation().getName());
				respBuilder.put("orgemail", userDTO.getOrganisation().getPlanCode());
				respBuilder.put("myorg", userDTO.getOrganisation().getStatus());
				respBuilder.put("plantype", userDTO.getOrganisation().getCode());
				respBuilder.put("card", userDTO.getOrganisation().getCardUpdate());
				if (!StringUtils.isEmpty(userDTO.getOrganisation().getSubscriptionMode())
						&& userDTO.getOrganisation().getSubscriptionMode().equalsIgnoreCase("yes")) {
					respBuilder.put("trial", userDTO.getOrganisation().getSubscriptionMode());
					respBuilder.put("trial_days", userDTO.getOrganisation().getUnits());
				} else {
					respBuilder.put("trial", "no");
					respBuilder.put("trial_days", -1);
				}
				if (!Objects.isNull(userDTO.getIsChangeRequired()) && userDTO.getIsChangeRequired() == 1)
					respBuilder.put("isChangeRequired", 1);
				else
					respBuilder.put("isChangeRequired", 0);
			} else {
				log.info("getOrganisationToken fails to get token for user {}..", userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getOrganisationToken AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getOrganisationToken JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getOrganisationToken method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/submit/partner/code/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitPartnerCode(@PathParam("code") String code) throws JsonProcessingException {

		log.info("submitPartnerCode method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String name = null;

		try {
			name = partnerService.submitPartnerCode(code);
			if (!StringUtils.isEmpty(name)) {
				log.info("submitPartnerCode Partner Code successfully submitted");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.PARTNER_SUBMIT_SUCCESS_MSG, name, code);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("submitPartnerCode Partner Code fails to submit");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				String msg = String.format(ApplicationMetadata.PARTNER_FAILURE_MSG, code);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			}
		} catch (AppException e) {
			log.error("submitPartnerCode AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("submitPartnerCode JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("submitPartnerCode method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/report/url")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportUrl() throws JsonProcessingException {

		log.info("getReportUrl method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String url = null;

		try {
			ExagoReportHelper reportHelper = new ExagoReportHelper();
			url = reportHelper.getReportUrl(userBean.getUserDto(), false);
			if (!StringUtils.isEmpty(url)) {
				log.info("getReportUrl created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, url);
			} else {
				log.info("getReportUrl Partner Code fails to submit");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReportUrl AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getReportUrl JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
		} catch (Exception e) {
			log.error("getReportUrl  exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
		} finally {
			log.info("getReportUrl method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}
	
	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/get/report/url/param")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportUrlWithParam(String request) throws JsonProcessingException {

		log.info("getReportUrlWithParam method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		Map<String,Object> dataMap = null;

		try {
			ExagoReportHelper reportHelper = new ExagoReportHelper();
			JSONObject jsonObject = new JSONObject(request);
			String gstin = jsonObject.getString("gstin");
			String my = jsonObject.getString("month");
			dataMap = reportHelper.getReportUrlWithParam(userBean.getUserDto(), false, gstin, my);
			if (!Objects.isNull(dataMap)) {
				log.info("getReportUrlWithParam created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, dataMap);
			} else {
				log.info("getReportUrlWithParam Partner Code fails to submit");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReportUrlWithParam AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getReportUrlWithParam JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
		} catch (Exception e) {
			log.error("getReportUrlWithParam  exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
		} finally {
			log.info("getReportUrlWithParam method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * 
	 * 
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/create/org")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountCreateOrganisation(String request) throws JsonProcessingException {

		log.info("accountCreateOrganisation method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		String name = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("name")) {
			name = jsonObject.getString("name");
		}

		try {
			flag = userService.createOrganisation(name);
			if (flag) {
				log.info("accountCreateOrganisation created organisation successfully {}", name);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.CREATE_ORGANISATION, name));
			} else {
				log.info("accountCreateOrganisation fails to create organisation {}..", name);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("accountCreateOrganisation AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("accountCreateOrganisation JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accountCreateOrganisation method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * 
	 * 
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/account/rename/org")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountRenameOrganisation(String request) throws JsonProcessingException {

		log.info("accountRenameOrganisation method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		String name = "";
		JsonNode jsonNode = new JsonNode(request);
		JSONObject jsonObject = jsonNode.getObject();
		if (jsonObject != null && jsonObject.has("name")) {
			name = jsonObject.getString("name");
		}

		try {
			flag = userService.renameOrganisation(name);
			if (flag) {
				log.info("accountRenameOrganisation renamed organisation successfully {}", name);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.RENAME_ORGANISATION, name));
			} else {
				log.info("accountRenameOrganisation fails to rename organisation {}..", name);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("accountRenameOrganisation AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("accountRenameOrganisation JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accountRenameOrganisation method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/subscription/plan")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubscriptionPlans() throws JsonProcessingException {

		log.info("getSubscriptionPlans method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		List<Plan> plans = null;

		try {
			plans = subscriptionService.getAllPlansFromDatabase();
			if (!Objects.isNull(plans)) {
				log.info("getSubscriptionPlans successfully got plans");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, plans);
			} else {
				log.info("getSubscriptionPlans fails to get plans");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} finally {
			log.info("getSubscriptionPlans method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/my/subscription/plan")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMySubscriptionPlans() throws JsonProcessingException {

		log.info("getMySubscriptionPlans method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		List<Plan> plans = null;

		try {
			plans = subscriptionService.getUserFilteredPlans();
			if (!Objects.isNull(plans)) {
				log.info("getMySubscriptionPlans successfully got plans");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, plans);
			} else {
				log.info("getMySubscriptionPlans fails to get plans");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} finally {
			log.info("getMySubscriptionPlans method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/invoices")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerInvoices() throws JsonProcessingException {

		log.info("getCustomerInvoices method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String jsonArray = null;

		try {
			jsonArray = subscriptionService.getInvoicesByCustId();
			if (!Objects.isNull(jsonArray)) {
				log.info("getCustomerInvoices successfully got invoices");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, jsonArray);
			} else {
				log.info("getCustomerInvoices fails to get invoices");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getCustomerInvoices AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getCustomerInvoices JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getCustomerInvoices method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/email/invoice/{invoice_id}/{invoice_no}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response emailInvoice(@PathParam("invoice_id") String invoice_id, @PathParam("invoice_no") String invoice_no)
			throws JsonProcessingException {

		log.info("emailInvoice method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			flag = subscriptionService.emailAnInvoice(invoice_id);
			if (flag) {
				log.info("emailInvoice successfully got invoices");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.EMAIL_INVOICE_SUCCESS, invoice_no,
						userBean.getUserDto().getUsername());
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("emailInvoice fails to get invoices");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.EMAIL_INVOICE_FAILURE);
			}
		} catch (AppException e) {
			log.error("emailInvoice AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("emailInvoice JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("emailInvoice method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/subscription/url/{planCode}/{quantity}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubscriptionPlanUrl(@PathParam("planCode") String planCode,
			@PathParam("quantity") Integer quantity) throws JsonProcessingException {

		log.info("getSubscriptionPlanUrl method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String url = null;

		try {
			url = subscriptionService.getNewSubscriptionUrl(planCode, quantity);
			if (!StringUtils.isEmpty(url)) {
				log.info("getSubscriptionPlanUrl successfully got plan url");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, url);
			} else {
				log.info("getSubscriptionPlanUrl fails to get plan url");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getSubscriptionPlanUrl AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getSubscriptionPlanUrl JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getSubscriptionPlanUrl method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/updatecard/url")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdateCardUrl() throws JsonProcessingException {

		log.info("getUpdateCardUrl method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String url = null;

		try {
			url = subscriptionService.getUpdateCardUrl();
			if (!StringUtils.isEmpty(url)) {
				log.info("getUpdateCardUrl successfully got plan url");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, url);
			} else {
				log.info("getUpdateCardUrl fails to get plan url");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getUpdateCardUrl AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getUpdateCardUrl JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getUpdateCardUrl method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/subscription/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSubscriptionInfo() throws JsonProcessingException {

		log.info("getSubscriptionInfo method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		Map<String, Object> organisation = null;

		try {
			organisation = subscriptionService.getSubscriptionInfo();
			if (!Objects.isNull(organisation)) {
				log.info("getSubscriptionInfo successfully got plan data");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, organisation);
			} else {
				log.info("getSubscriptionInfo fails to get plan data");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getSubscriptionInfo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getSubscriptionInfo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getSubscriptionInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * 
	 * 
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/update/customer/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomerInfo(String request) throws JsonProcessingException {

		log.info("updateCustomerInfo method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			flag = subscriptionService.updateCustomer(request);
			if (flag) {
				log.info("updateCustomerInfo updated customer information successfully {}", request);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.CUSTOMER_BILLING_SUCCESS_MSG);
			} else {
				log.info("updateCustomerInfo fails to update customer information {}..", request);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.CUSTOMER_BILLING_FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateCustomerInfo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("updateCustomerInfo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("updateCustomerInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/change/plan/{planCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeSubscriptionPlan(@PathParam("planCode") String planCode) throws JsonProcessingException {

		log.info("changeSubscriptionPlan method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			flag = subscriptionService.updateSubscription(planCode);
			if (flag) {
				log.info("changeSubscriptionPlan successfully got plan data");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.PLAN_CHANGE_SUCCESS);
			} else {
				log.info("changeSubscriptionPlan fails to get plan data");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.PLAN_CHANGE_FAILURE);
			}
		} catch (AppException e) {
			log.error("changeSubscriptionPlan AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("changeSubscriptionPlan JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("changeSubscriptionPlan method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/change/plan/units/{planUnits}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeSubscriptionPlan(@PathParam("planUnits") Integer planUnits) throws JsonProcessingException {

		log.info("changeSubscriptionPlan method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		int flag = 0;

		try {
			flag = subscriptionService.updateSubscription(planUnits);
			if (flag > 0) {
				log.info("changeSubscriptionPlan successfully got plan data");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				if (flag == 2)
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
							String.format(ApplicationMetadata.PLAN_UNITS_CHANGE_SUCCESS, planUnits));
				else
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
							String.format(ApplicationMetadata.PLAN_UNITS_END_CHANGE_SUCCESS, planUnits));
			} else {
				log.info("changeSubscriptionPlan fails to get plan data");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.PLAN_UNITS_CHANGE_FAILURE);
			}
		} catch (AppException e) {
			log.error("changeSubscriptionPlan AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("changeSubscriptionPlan JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("changeSubscriptionPlan method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

}
