/**
 * 
 */
package com.ginni.easemygst.portal.ws.impl.emg;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.TinDTO;
import com.ginni.easemygst.portal.business.dto.ActionLogDto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.InvoiceVendorDetailsDto;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.MonthSummaryDTO;
import com.ginni.easemygst.portal.business.dto.UniversalReturnDTO;
import com.ginni.easemygst.portal.business.entity.BillingAddressDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.ErpConfigDTO;
import com.ginni.easemygst.portal.business.entity.FiscalyearDetailDTO;
import com.ginni.easemygst.portal.business.entity.GstReturnDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.HsnSacDTO;
import com.ginni.easemygst.portal.business.entity.ItemCodeDTO;
import com.ginni.easemygst.portal.business.entity.ReturnTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.entity.UserGstinDTO;
import com.ginni.easemygst.portal.business.entity.VendorDTO;
import com.ginni.easemygst.portal.business.service.AccountService;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.NotificationService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ScheduleManager;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.Constant;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.ExternalServiceExecutor;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.FilterDto;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.ErpLog;
import com.ginni.easemygst.portal.persistence.entity.HsnSac;
import com.ginni.easemygst.portal.persistence.entity.UserGstin;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.reports.ExagoReportHelper;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.ManipulateFile;

/**
 * 
 * @author Sumeet
 *
 */
@RequestScoped
@Path("/taxpws")
@Produces("application/json")
@Consumes("application/json")
public class TaxpayerWebservice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private TaxpayerService taxpayerService;

	@Inject
	private ReturnsService returnService;

	@Inject
	private AccountService accountService;

	@Inject
	private RedisService redisService;

	@Inject
	private UserBean userBean;

	@Inject
	private ApiLoggingService apiLoggingService;

	@Inject
	private ScheduleManager scheduleManager;

	@Inject
	private NotificationService notificationService;

	/**
	 * This method will set financial year context
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/set/financial/year/{year1}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setFinancialYearContext(@PathParam("year1") String start) throws JsonProcessingException {

		log.info("setFinancialYearContext method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!StringUtils.isEmpty(start)) {
				taxpayerService.setFinancialYear(start);
				log.info("setFinancialYearContext successfully completed {}", start);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				log.info("setFinancialYearContext fail to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("setFinancialYearContext JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("setFinancialYearContext method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will set financial year context
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/financial/year")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFinancialYears() throws JsonProcessingException {

		log.info("getFinancialYears method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> value = taxpayerService.getFinancialYear();
			if (!Objects.isNull(value)) {
				log.info("getFinancialYears successfully completed {}", value);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, value);
			} else {
				log.info("getFinancialYears fail to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getFinancialYears JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getFinancialYears method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will update pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/pan/billing/update/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateTaxpayeBillingAddress(@PathParam("pan") String pan, String request)
			throws JsonProcessingException {

		log.info("updateTaxpayeBillingAddress method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			BillingAddressDTO billingAddressDTO = JsonMapper.objectMapper.readValue(request, BillingAddressDTO.class);
			flag = taxpayerService.updateTaxpayerBillingAddress(pan, billingAddressDTO);
			if (flag) {
				log.info("updateTaxpayeBillingAddress successfully updated {}", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.PAN_BILLING_UPDATE, pan);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("updateTaxpayeBillingAddress fails to update..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateTaxpayeBillingAddress AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("updateTaxpayeBillingAddress JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("updateTaxpayeBillingAddress method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will update pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/user/add/credits")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCredits(String request) throws JsonProcessingException {

		log.info("addCredits method starts..");
		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		TreeMap<String, String> params = new TreeMap<>();

		try {
			params = accountService.initiatePayment(request);
			if (!Objects.isNull(params)) {
				log.info("addCredits successfully updated for the request {}", request);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, params);
			} else {
				log.info("addCredits fails to update for the request..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("addCreditsPan AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("addCredits JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("addCredits method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/pan/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPan(@QueryParam("financialYear") String fyear) throws JsonProcessingException {

		log.info("getPan method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (StringUtils.isEmpty(fyear))
				fyear = CalanderCalculatorUtility.getCurrentFinancialYear();
			Map<String, Object> taxpayerDto = taxpayerService.getTaxpayer(userBean.getUserDto().getId(), fyear);
			if (taxpayerDto != null) {
				log.info("getPan successfully got {} pans", taxpayerDto.size());
				String flag = redisService.getValue("gstn_flag");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerDto.get("taxpayer"));
				taxpayerDto.remove("taxpayer");
				respBuilder.put(ApplicationMetadata.RESP_SUMMARY, taxpayerDto);
				YearMonth yearMonth = YearMonth.now();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM");
				DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("MMyyyy");
				YearMonth previousYearMonth = yearMonth.plusMonths(-1);
				respBuilder.put("previous_month", formatter.format(previousYearMonth));
				respBuilder.put("previous_month_code", formatter1.format(previousYearMonth));
				respBuilder.put("gstn_flag", flag);
			} else {
				log.info("getPan fails to get the list for pans..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getPan AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getPan JSONException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getPan method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will configure ERP for GSTIN#
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/gstin/config/erp/{provider}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response configErp(@PathParam("provider") String provider, String request) throws JsonProcessingException {

		log.info("configErp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			ErpConfigDTO erpConfigDTO = JsonMapper.objectMapper.readValue(request, ErpConfigDTO.class);
			erpConfigDTO.setProvider(provider);
			flag = taxpayerService.updateErpConfig(erpConfigDTO);
			if (flag) {
				log.info("configErp successfully updated for the provider {}", provider);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.ERP_SUCCESS_MSG);
				// logging user action

				apiLoggingService.saveAction(UserAction.CONNECT_ERP, null);

				// end logging user action
			} else {
				log.info("configErp fails to update the user erpconfig..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.ERP_FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("configErp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("configErp JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("configErp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will configure ERP for GSTIN#
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/get/erp")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getErp() throws JsonProcessingException {

		log.info("getErp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		ErpConfigDTO erpConfigDTO = null;

		try {
			erpConfigDTO = taxpayerService.getErpConfig();
			if (!Objects.isNull(erpConfigDTO)) {
				log.info("getErp successfully updated for the provider {}", erpConfigDTO.getProvider());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, erpConfigDTO);
			} else {
				log.info("getErp successfully didnt found provider {}");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			}
		} catch (AppException e) {
			log.error("getErp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getErp JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getErp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will configure ERP for GSTIN#
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/remove/erp/{provider}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeErp(@PathParam("provider") String provider) throws JsonProcessingException {

		log.info("removeErp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			flag = taxpayerService.removeErpConfig(provider);
			if (flag) {
				log.info("removeErp successfully updated for the provider");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.ERP_REMOVE_SUCCESS_MSG);

				// logging user action

				apiLoggingService.saveAction(UserAction.DISCONNECT_ERP, null);

				// end logging user action
			} else {
				log.info("removeErp successfully didnt found provider {}");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.ERP_REMOVE_FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("removeErp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("removeErp JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("removeErp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will configure ERP for GSTIN#
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/logs/erp")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getErpLogs() throws JsonProcessingException {

		log.info("getErpLogs method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<ErpLog> erpLogs = taxpayerService.getErpLogs();
			if (!Objects.isNull(erpLogs)) {
				log.info("getErpLogs successfully updated for the provider");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, erpLogs);
			} else {
				log.info("getErpLogs successfully didnt found provider {}");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, new ArrayList<>());
			}
		} catch (AppException e) {
			log.error("getErpLogs AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getErpLogs JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getErpLogs method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will configure ERP for GSTIN#
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/data/erp")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getErpDataStatus() throws JsonProcessingException {

		log.info("getErpDataStatus method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<DataUploadInfo> erpDatas = taxpayerService.getErpDataUpload();
			if (!Objects.isNull(erpDatas)) {
				log.info("getErpDataStatus successfully updated for the provider");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, erpDatas);
			} else {
				log.info("getErpDataStatus successfully didnt found provider {}");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, new ArrayList<>());
			}
		} catch (AppException e) {
			log.error("getErpDataStatus AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getErpDataStatus JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getErpDataStatus method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/process/dataupload/info/{sno}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response processDataUploadInfo(@PathParam("sno") Integer sno) throws JsonProcessingException {

		log.info("processDataUploadInfo method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> responseMap = taxpayerService.getDataUploadInfo(sno);

			new ExternalServiceExecutor().myPostConstruct(responseMap.get("data"));

			int count = (Integer) responseMap.get("count");
			if (count > 0) {
				log.info("processDataUploadInfo updated entry successfully with count {}", count);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, count);
			} else {
				log.info("processDataUploadInfo Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("processDataUploadInfo AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("processDataUploadInfo JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("processDataUploadInfo method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/gstin/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinAdd(String request) throws JsonProcessingException {

		log.info("gstinAdd method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			TaxpayerGstinDTO taxpayerGstinDTO = JsonMapper.objectMapper.readValue(request, TaxpayerGstinDTO.class);
			log.info("gstinAdd serialize data {}", request);
			taxpayerGstinDTO = taxpayerService.createTaxpayerGstin(taxpayerGstinDTO);
			if (taxpayerGstinDTO != null) {
				log.info("gstinAdd successfully added {}", taxpayerGstinDTO.getGstin());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_ADD, taxpayerGstinDTO.getGstin());
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerGstinDTO);
			} else {
				log.info("gstinAdd fails to add the gstin Data..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("gstinAdd AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("gstinAdd JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("gstinAdd method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will update pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/gstin/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinUpdate(String request) throws JsonProcessingException {

		log.info("gstinUpdate method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag=false;

		try {
			TaxpayerGstinDTO taxpayerGstinDTO=JsonMapper.objectMapper.readValue(request,TaxpayerGstinDTO.class);
			
			 if(!Objects.isNull(taxpayerService.updateTaxpayerGstin(taxpayerGstinDTO)))
					 flag=true;
			if (flag) {
				// logging user action
				MessageDto messageDto = new MessageDto();
				messageDto.setGstin(taxpayerGstinDTO.getGstin());
				/* apiLoggingService.saveAction(UserAction.ADD_TIN, messageDto); */
				// end logging user action
				log.info("gstinUpdate successfully updated for Gstin {}", taxpayerGstinDTO.getGstin());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_UPDATE, taxpayerGstinDTO.getGstin());
				
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("gstinUpdate fails to update..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("gstinUpdate AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("gstinUpdate  exception {} ", e);
		} finally {
			log.info("gstinUpdate method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@POST
	@Path("/gstin/update/turnover")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinUpdateTurnover(String request) throws JsonProcessingException {

		log.info("gstinUpdate method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			FiscalyearDetailDTO fiscalyearDetailDTO = JsonMapper.objectMapper.readValue(request,
					FiscalyearDetailDTO.class);
			boolean flag = taxpayerService.updateturnoverDetail(fiscalyearDetailDTO);
			if (flag) {
				// logging user action
				MessageDto messageDto = new MessageDto();
				messageDto.setGstin(fiscalyearDetailDTO.getGstin());
				/* apiLoggingService.saveAction(UserAction.ADD_TIN, messageDto); */
				// end logging user action
				log.info("gstinUpdate successfully updated for Gstin {}", fiscalyearDetailDTO.getGstin());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_UPDATE, fiscalyearDetailDTO.getGstin());
				if (!Objects.isNull(fiscalyearDetailDTO.getPreviousGt())) {
					msg = String.format(ApplicationMetadata.TURNOVER_UPDATE, fiscalyearDetailDTO.getGstin());
					// logging user action
					messageDto = new MessageDto();
					messageDto.setGstin(fiscalyearDetailDTO.getGstin());
					apiLoggingService.saveAction(UserAction.ADD_TURNOVER_DETAILS, messageDto);
					// end logging user action
				}
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("gstinUpdate fails to update..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("gstinUpdate AppException exception {} ", e);
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("gstinUpdate  exception {} ", e);
		} finally {
			log.info("gstinUpdate method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will delete pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/gstin/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinDelete(@PathParam("id") Integer id) throws JsonProcessingException {

		log.info("gstinDelete method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String gstin = taxpayerService.deleteTaxpayerGstin(id);
			if (!StringUtils.isEmpty(gstin)) {
				log.info("gstinDelete successfully deleted for gstin {}", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_DELETE, gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("gstinDelete fails to delete for gstin");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("gstinDelete AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("gstinDelete JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("gstinDelete method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstin/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstin(@QueryParam("financialYear") String fyear, @PathParam("gstin") String gstin)
			throws JsonProcessingException {

		log.info("getGstin method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (StringUtils.isEmpty(fyear))
				fyear = CalanderCalculatorUtility.getCurrentFinancialYear();
			TaxpayerGstinDTO taxpayerGstinDTO = taxpayerService.getTaxpayerGstin(gstin, fyear);
			if (!Objects.isNull(taxpayerGstinDTO)) {
				log.info("getGstin successfully got the list for {} gstins", taxpayerGstinDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerGstinDTO);
			} else {
				log.info("getGstin fails to get the list for gstins");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getGstin AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getGstin JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getGstin method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/users")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsers() throws JsonProcessingException {

		log.info("getUsers method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> data = taxpayerService.getUsers();
			if (!Objects.isNull(data)) {
				log.info("getUsers successfully got the list of users");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, data.get("list"));
				respBuilder.put("count", data.get("count"));
			} else {
				log.info("getUsers fails to get list for gstins");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getUsers AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getUsers JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getUsers method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/provide/gstin/access/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response provideGstinAccess(@PathParam("gstin") String gstin, String request)
			throws JsonProcessingException {

		log.info("provideGstinAccess method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {

			String username = taxpayerService.updateGstinUser(gstin, request);
			if (!Objects.isNull(username)) {
				log.info("provideGstinAccess user is added successfully {}", request);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_ADD_USER, username, gstin);
				if (username.contains(":")) {
					boolean isUpdate = false;
					String[] s = username.split(":");
					if (!s[1].equalsIgnoreCase("add")) {
						msg = String.format(ApplicationMetadata.GSTIN_UPDATE_USER, username, gstin);
						isUpdate = true;
					}

					// logging user action
					MessageDto messageDto = new MessageDto();
					messageDto.setEmialIdSecondary(username);
					messageDto.setGstin(gstin);
					if (isUpdate)
						apiLoggingService.saveAction(UserAction.EDIT_USER_ACCESS, messageDto);
					else
						apiLoggingService.saveAction(UserAction.ADD_USER, messageDto);

					// end logging user action
				}
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("provideGstinAccess Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("provideGstinAccess AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("provideGstinAccess JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("provideGstinAccess method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstin/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstinList() throws JsonProcessingException {

		log.info("getGstinList method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<UserGstin> userGstins = taxpayerService.getGstinList();
			if (!Objects.isNull(userGstins)) {
				log.info("getGstinList got list of objects {}", userGstins.size());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, userGstins);
			} else {
				log.info("getGstinList Unable to get list");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getGstinList AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getGstinList JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getGstinList method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/usergstin/data/{gstin}/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserGstinData(@PathParam("gstin") String gstin, @PathParam("username") String username)
			throws JsonProcessingException {

		log.info("getUserGstinData method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> obj = taxpayerService.getUserGstinList(gstin, username);
			if (!Objects.isNull(obj)) {
				log.info("getUserGstinData got list of objects {}", obj.toString());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, obj);
			} else {
				log.info("getUserGstinData Unable to get list");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getUserGstinData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getUserGstinData JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getUserGstinData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/remove/usergstin/data/{username}/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeUserGstinData(@PathParam("gstin") String gstin, @PathParam("username") String username)
			throws JsonProcessingException {

		log.info("removeUserGstinData method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = taxpayerService.removeUserGstinData(gstin, username);
			if (flag) {
				log.info("removeUserGstinData successfully removed user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_REVOKE_USER, gstin, username);
				if (gstin.equalsIgnoreCase("all")) {
					msg = String.format(ApplicationMetadata.GSTIN_REVOKE_ALL_USER, username);
				}
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);

				// logging user action
				MessageDto messageDto = new MessageDto();
				messageDto.setEmialIdSecondary(username);
				messageDto.setGstin(gstin);
				if ("all".equalsIgnoreCase(gstin))
					apiLoggingService.saveAction(UserAction.ACCESS_REVOKED_FOR_ALL_TIN, messageDto);
				else
					apiLoggingService.saveAction(UserAction.ACCESS_REVOKED_FOR_PARTICULAR_TIN, messageDto);

				// end logging user action
			} else {
				log.info("removeUserGstinData Unable to get list");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("removeUserGstinData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("removeUserGstinData JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("removeUserGstinData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/add/usergstin/invoice/{username}/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addUserGstinInvoiceAccess(@PathParam("gstin") String gstin, @PathParam("username") String username)
			throws JsonProcessingException {

		log.info("addUserGstinInvoiceAccess method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			String user = taxpayerService.addVendorInvoiceAccess(gstin, username);
			if (!StringUtils.isEmpty(user)) {
				log.info("addUserGstinInvoiceAccess successfully removed user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.INVOICE_ADD_GSTIN_USER, username, gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("addUserGstinInvoiceAccess Unable to get list");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("addUserGstinInvoiceAccess AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("addUserGstinInvoiceAccess JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("addUserGstinInvoiceAccess method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstin/detail/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGstinDetail(@QueryParam("financialYear") String fyear, @PathParam("gstin") String gstin)
			throws JsonProcessingException {

		log.info("getGstinDetail method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (StringUtils.isEmpty(fyear))
				fyear = CalanderCalculatorUtility.getCurrentFinancialYear();
			TaxpayerGstinDTO taxpayerGstinDTO = taxpayerService.getTaxpayerGstin(gstin, fyear);
			if (!Objects.isNull(taxpayerGstinDTO)) {
				log.info("getGstinDetail successfully got the list for {} gstins", taxpayerGstinDTO.getUsers());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerGstinDTO);
			} else {
				log.info("getGstinDetail fails to get list for gstins");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getGstinDetail AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getGstinDetail JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getGstinDetail method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/get/gstin/access/{gstin}/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	@Deprecated
	public Response accessUserGstin(@QueryParam("financialYear") String fyear, @PathParam("gstin") String gstin,
			@PathParam("username") String username, String request) throws JsonProcessingException {

		log.info("accessUserGstin method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<UserGstinDTO> userGstinDTOs = JsonMapper.objectMapper.readValue(request,
					new TypeReference<List<UserGstinDTO>>() {
					});
			boolean flag = taxpayerService.addGstinUser(gstin, username, userGstinDTOs);
			TaxpayerGstinDTO taxpayerGstinDTO = taxpayerService.getTaxpayerGstin(gstin, fyear);
			if (flag && !Objects.isNull(taxpayerGstinDTO)) {
				log.info("accessUserGstin user is added successfully {}", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_ADD_USER, username, gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerGstinDTO);
			} else {
				log.info("accessUserGstin Unable to add the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("accessUserGstin AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("accessUserGstin JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accessUserGstin method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/gstin/revoke/{gstin}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Deprecated
	public Response revokeUserGstin(@QueryParam("financialYear") String fyear, @PathParam("gstin") String gstin,
			@PathParam("id") Integer id) throws JsonProcessingException {

		log.info("revokeUserGstin method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = taxpayerService.removeGstinUser(gstin, id);
			if (StringUtils.isEmpty(fyear))
				fyear = CalanderCalculatorUtility.getCurrentFinancialYear();
			TaxpayerGstinDTO taxpayerGstinDTO = taxpayerService.getTaxpayerGstin(gstin, fyear);
			if (flag && !Objects.isNull(taxpayerGstinDTO)) {
				log.info("revokeUserGstin user is successfully removed ");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.GSTIN_DELETE_USER, gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerGstinDTO);
			} else {
				log.info("revokeUserGstin Unable to remove the user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("revokeUserGstin AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("revokeUserGstin JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("revokeUserGstin method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/pan/access/{pan}/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	@Deprecated
	public Response accessUserTaxpayer(@PathParam("pan") String pan, @PathParam("username") String username)
			throws JsonProcessingException {

		log.info("accessUserTaxpayer method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = taxpayerService.addTaxpayerUser(pan, username);
			TaxpayerDTO taxpayerDTO = taxpayerService.getTaxpayerGstinDetail(pan);
			if (flag && !Objects.isNull(taxpayerDTO)) {
				log.info("accessUserTaxpayer successfully added user {}", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.PAN_ADD_USER, username, pan);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerDTO);
			} else {
				log.info("accessUserTaxpayer fails to add user");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("accessUserTaxpayer AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("accessUserTaxpayer JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("accessUserTaxpayer method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will get all pan from taxpayer table requested user as primary
	 * for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/pan/revoke/{pan}/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	@Deprecated
	public Response revokeUserTaxpayer(@PathParam("pan") String pan, @PathParam("username") String username)
			throws JsonProcessingException {

		log.info("revokeUserTaxpayer method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			boolean flag = taxpayerService.removeTaxpayerUser(pan, username);
			TaxpayerDTO taxpayerDTO = taxpayerService.getTaxpayerGstinDetail(pan);
			if (flag && !Objects.isNull(taxpayerDTO)) {
				log.info("revokeUserTaxpayer successfully removed user {} ", username);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.PAN_DELETE_USER, pan);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerDTO);
			} else {
				log.info("revokeUserTaxpayer fails to remove user {}");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("revokeUserTaxpayer AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("revokeUserTaxpayer JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("revokeUserTaxpayer method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will call GSTN OTP Request for the particular PAN by the
	 * requested user
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/request/otp/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinRequestOtp(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("gstinRequestOtp method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = false;

		try {
			flag = taxpayerService.requestGstnOtp(gstin);
			log.debug("return {}", flag);
			if (flag) {
				log.info("gstinRequestOtp otp has been sent successfully..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.GSTIN_OTP);
			} else {
				log.info("gstinRequestOtp Unable to send gstin request otp..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("gstinRequestOtp AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("gstinRequestOtp Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("gstinRequestOtp method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will call GSTN Auth token request for the particular PAN by the
	 * requested user
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/request/token/{gstin}/{otp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinRequestToken(@PathParam("gstin") String gstin, @PathParam("otp") String otp)
			throws JsonProcessingException {

		log.info("gstinRequestToken method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String output = null;

		try {
			if (!otp.equals("102030")) {
				output = taxpayerService.requestGstnAuthToken(gstin, otp);
			} else {
				Long time = Calendar.getInstance().getTimeInMillis();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				output = sdf.format(new Date(time)) + " IST";
			}
			if (!StringUtils.isEmpty(output)) {
				log.info("gstinRequestToken token has been successfully for gstin..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.GSTIN_AUTH);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);
			} else {
				log.info("gstinRequestToken request token from gstin fails..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("gstinRequestToken AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("gstinRequestToken Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("gstinRequestToken method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will GSTN Auth refresh token request for the particular GSTIN by
	 * the requested user
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/gstin/refresh/token/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gstinRequestRefreshToken(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("gstinRequestRefreshToken method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String output = null;

		try {
			scheduleManager.scheduleRefreshToken();
			output = "success";
			if (!StringUtils.isEmpty(output)) {
				log.info("gstinRequestRefreshToken token has been successfully for gstin..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.GSTIN_AUTH_REFRESH);
				respBuilder.put(ApplicationMetadata.RESP_DATA, output);
			} else {
				log.info("gstinRequestRefreshToken request token from gstin fails..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (Exception e) {
			log.error("gstinRequestRefreshToken Exception exception {} ", e);
		} finally {
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method is used to get tin dashboard summary
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/get/dashboard/summary/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDashboardSummary(@QueryParam("financialYear") String fyear, @PathParam("gstin") String gstin)
			throws JsonProcessingException {

		log.info("getDashboardSummary method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (!Objects.isNull(gstin)) {

				List<MonthSummaryDTO> monthSummaryDTOs = taxpayerService.getGstReturnSummaryByGstin(gstin);
				Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> map = taxpayerService
						.getGstTransactionsByGstin(gstin);
				if (!Objects.isNull(monthSummaryDTOs)) {
					if (StringUtils.isEmpty(fyear))
						fyear = CalanderCalculatorUtility.getCurrentFinancialYear();
					MonthSummaryDTO monthSummaryDTO = monthSummaryDTOs.get(0);
					GstinTransactionDTO gstinTransactionDTO = new GstinTransactionDTO();
					gstinTransactionDTO.setMonthYear(monthSummaryDTO.getCode());
					gstinTransactionDTO.setTaxpayerGstin(taxpayerService.getTaxpayerGstin(gstin, fyear));

					log.info("getDashboardSummary getting GSTR1 data for {}", monthSummaryDTO.getMonthName());
					gstinTransactionDTO.setReturnType(ReturnType.GSTR1.toString());
					GstrSummaryDTO outSummaryDTO = new GstrSummaryDTO();// returnService.getTransactionSummary(gstinTransactionDTO);

					log.info("getDashboardSummary getting GSTR2 data for {}", monthSummaryDTO.getMonthName());
					gstinTransactionDTO.setReturnType(ReturnType.GSTR2.toString());
					GstrSummaryDTO inSummaryDTO = new GstrSummaryDTO();// returnService.getTransactionSummary(gstinTransactionDTO);

					if (!Objects.isNull(map)) {
						SortedSet<UniversalReturnDTO> universalReturnDTOs = new TreeSet<>(
								Comparator.comparing(UniversalReturnDTO::getId));
						Iterator it = map.entrySet().iterator();
						while (it.hasNext()) {
							UniversalReturnDTO dto = new UniversalReturnDTO();
							Map.Entry pair = (Map.Entry) it.next();
							GstReturnDTO gstReturnDTO = (GstReturnDTO) pair.getKey();
							SortedSet<ReturnTransactionDTO> temp = (SortedSet<ReturnTransactionDTO>) pair.getValue();
							dto.setId(gstReturnDTO.getId());
							dto.setName(gstReturnDTO.getName());
							dto.setDescription(gstReturnDTO.getDescription());
							dto.setCode(gstReturnDTO.getCode());
							dto.setValue(temp);
							universalReturnDTOs.add(dto);
						}
						log.info("getDashboardSummary successfully got mapping for {}..", gstin);
						respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
						respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
						respBuilder.put("brtmap", universalReturnDTOs);
					}

					log.info("getDashboardSummary successfully completed {}", monthSummaryDTO.getMonthName());
					respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
					respBuilder.put(ApplicationMetadata.RESP_DATA, monthSummaryDTOs);
					respBuilder.put(ApplicationMetadata.RESP_OUTWARD, outSummaryDTO);
					respBuilder.put(ApplicationMetadata.RESP_INWARD, inSummaryDTO);
				}
			} else {
				log.info("getDashboardSummary fail to complete..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (JsonException e) {
			log.error("getDashboardSummary JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getDashboardSummary method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/transactions/{types}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnTransaction(@PathParam("types") String types) throws JsonProcessingException {

		log.info("getReturnTransaction method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Set<ReturnTransactionDTO> returnTransactionDTOs = taxpayerService.getTransactionsByBusinessType(types);
			if (!Objects.isNull(returnTransactionDTOs)) {
				log.info("getReturnTransaction successfully got mapping for {}..", types);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, returnTransactionDTOs);
			} else {
				log.info("getReturnTransaction fail to retrieve mapping for {}..", types);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getReturnTransaction AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("getReturnTransaction Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("getReturnTransaction method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/*
	 * @SuppressWarnings("finally")
	 * 
	 * @GET
	 * 
	 * @Path("/get/erpColumnMapping/{pan}/{rtype}/{transaction}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Response
	 * getTransactionMappings(@PathParam("pan") String pan, @PathParam("rtype")
	 * String rtype,
	 * 
	 * @PathParam("transaction") String transaction) throws JsonProcessingException
	 * {
	 * 
	 * log.info("getTransactionMappings method starts..");
	 * 
	 * Map<String, Object> respBuilder = new HashMap<>();
	 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
	 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.FAILURE_MSG); String headers = "";
	 * 
	 * try { headers = taxpayerService.getTransactionMapping(pan, rtype,
	 * transaction); if (!StringUtils.isEmpty(headers)) { log.info(
	 * "getTransactionMappings successfully got mapping for {}..", pan, rtype,
	 * transaction); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._SUCCESS); respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.SUCCESS_MSG);
	 * respBuilder.put(ApplicationMetadata.RESP_DATA, headers); } else {
	 * log.info("getTransactionMappings fail to retrieve mapping for {}..", pan,
	 * rtype, transaction); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._FAILURE); respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.FAILURE_MSG); } } catch (AppException e) { log.error(
	 * "getTransactionMappings AppException exception {} ", e.getMessage());
	 * respBuilder.put(ApplicationMetadata.RESP_ERROR,
	 * e.getMessageJsonFromObject(e.getCode())); } catch (Exception e) {
	 * log.error("getTransactionMappings Exception exception {} ", e.getMessage());
	 * e.printStackTrace(); } finally { log.info(
	 * "getTransactionMappings method ends.."); Response response =
	 * Response.status(200).entity(respBuilder).build(); return response; } }
	 * 
	 * @SuppressWarnings("finally")
	 * 
	 * @POST
	 * 
	 * @Path("/save/erpColumnMapping/{pan}/{rtype}/{transaction}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Response
	 * saveErpColumnMappings(@PathParam("pan") String pan, @PathParam("rtype")
	 * String rtype,
	 * 
	 * @PathParam("transaction") String transaction, String request) throws
	 * JsonProcessingException {
	 * 
	 * log.info("saveErpColumnMappings method starts..");
	 * 
	 * Map<String, Object> respBuilder = new HashMap<>();
	 * respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
	 * respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.FAILURE_MSG); String headers = "";
	 * 
	 * try { headers = taxpayerService.saveTransactionMapping(pan, rtype,
	 * transaction, request); if (!StringUtils.isEmpty(headers)) { log.info(
	 * "saveErpColumnMappings successfully got mapping for {}..", pan, rtype,
	 * transaction); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._SUCCESS); respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.SUCCESS_MSG);
	 * respBuilder.put(ApplicationMetadata.RESP_DATA, headers); } else {
	 * log.info("saveErpColumnMappings fail to retrieve mapping for {}..", pan,
	 * rtype, transaction); respBuilder.put(ApplicationMetadata.RESP_CODE_KEY,
	 * ExceptionCode._FAILURE); respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
	 * ApplicationMetadata.FAILURE_MSG); } } catch (AppException e) { log.error(
	 * "saveErpColumnMappings AppException exception {} ", e.getMessage());
	 * respBuilder.put(ApplicationMetadata.RESP_ERROR,
	 * e.getMessageJsonFromObject(e.getCode())); } catch (Exception e) {
	 * log.error("saveErpColumnMappings Exception exception {} ", e.getMessage());
	 * e.printStackTrace(); } finally { log.info(
	 * "saveErpColumnMappings method ends.."); Response response =
	 * Response.status(200).entity(respBuilder).build(); return response; } }
	 */

	@SuppressWarnings({ "finally", "unchecked" })
	@GET
	@Path("/get/allTransactions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllTransactions() throws JsonProcessingException {

		log.info("getAllTransactions method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<GstReturnDTO, SortedSet<ReturnTransactionDTO>> transactions = taxpayerService.getGstAllTransactions();
			if (!Objects.isNull(transactions)) {
				SortedSet<UniversalReturnDTO> universalReturnDTOs = new TreeSet<>(
						Comparator.comparing(UniversalReturnDTO::getId));
				Iterator it = transactions.entrySet().iterator();
				while (it.hasNext()) {
					UniversalReturnDTO dto = new UniversalReturnDTO();
					Map.Entry pair = (Map.Entry) it.next();
					GstReturnDTO gstReturnDTO = (GstReturnDTO) pair.getKey();
					SortedSet<ReturnTransactionDTO> temp = (SortedSet<ReturnTransactionDTO>) pair.getValue();
					dto.setId(gstReturnDTO.getId());
					dto.setName(gstReturnDTO.getName());
					dto.setDescription(gstReturnDTO.getDescription());
					dto.setCode(gstReturnDTO.getCode());
					dto.setValue(temp);
					universalReturnDTOs.add(dto);
				}
				log.info("getAllTransactions successfully got mapping for {}..", transactions);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, universalReturnDTOs);
			} else {
				log.info("getAllTransactions fails to get list for the transactions..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getAllTransactions AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getAllTransactions JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getAllTransactions method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/upload/xls/vendor/supplier/{pan}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadVendorSupplierData(MultipartFormDataInput input, @PathParam("pan") String pan)
			throws Exception {

		log.info("uploadVendorSupplierData method starts for {} {} {}..", pan);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		int count = 0;

		String mapping = input.getFormDataPart("mapping", String.class, null);
		try {
			String fileName = ManipulateFile.uploadMultipartFile(input);
			count = taxpayerService.readExcelVendorData(pan, new File(fileName), mapping, "supplier");
			if (count > 0) {
				log.info("uploadVendorSupplierData successfully uploaded file {}..", pan);
				List<Vendor> vendors = taxpayerService.getVendorData(pan, "supplier");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.EXCEL_UPLOAD, count));
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendors);
			} else {
				log.info("uploadVendorSupplierData fail to process file {}..", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("uploadVendorSupplierData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("uploadVendorSupplierData Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("uploadVendorSupplierData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/upload/xls/vendor/buyer/{pan}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadVendorBuyerData(MultipartFormDataInput input, @PathParam("pan") String pan) throws Exception {

		log.info("uploadVendorBuyerData method starts for {} {} {}..", pan);

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		int count = 0;

		String mapping = input.getFormDataPart("mapping", String.class, null);
		try {
			String fileName = ManipulateFile.uploadMultipartFile(input);
			count = taxpayerService.readExcelVendorData(pan, new File(fileName), mapping, "buyer");
			if (count > 0) {
				log.info("uploadVendorBuyerData successfully uploaded file {}..", pan);
				List<Vendor> vendors = taxpayerService.getVendorData(pan, "buyer");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY,
						String.format(ApplicationMetadata.EXCEL_UPLOAD, count));
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendors);
			} else {
				log.info("uploadVendorBuyerData fail to process file {}..", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("uploadVendorBuyerData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("uploadVendorBuyerData Exception exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("uploadVendorBuyerData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@GET
	@Path("/get/vendor/supplier/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVendorSupplier(@PathParam("pan") String pan) throws JsonProcessingException {

		log.info("getVendorSupplier method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<Vendor> vendors = taxpayerService.getVendorData(pan, "supplier");
			if (!StringUtils.isEmpty(pan)) {
				log.info("getVendorSupplier successfully got mapping for {}..", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendors);
			} else {
				log.info("getVendorSupplier fails to get list..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getVendorSupplier AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getVendorSupplier JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getVendorSupplier method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@GET
	@Path("/get/vendor/buyer/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVendorBuyer(@PathParam("pan") String pan) throws JsonProcessingException {

		log.info("getVendorSupplier method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<Vendor> vendors = taxpayerService.getVendorData(pan, "buyer");
			if (!StringUtils.isEmpty(pan)) {
				log.info("getVendorSupplier successfully got mapping for {}..", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendors);
			} else {
				log.info("getVendorSupplier fails to get list..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getVendorSupplier AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getVendorSupplier JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getVendorSupplier method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/supplier/add/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorSupplierAdd(String request, @PathParam("pan") String pan) throws JsonProcessingException {

		log.info("vendorSupplierAdd method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = JsonMapper.objectMapper.readValue(request, VendorDTO.class);
			log.info("vendorSupplierAdd serialize data {}", request);
			vendorDTO = taxpayerService.createVendor(vendorDTO, pan);
			if (vendorDTO != null) {
				/*
				 * MessageDto messageDto=new MessageDto();
				 * 
				 * messageDto.setCustomerName(vendorDTO.getBusinessName());
				 * apiLoggingService.saveAction(UserAction.ADD_VENDOR, messageDto);
				 */

				log.info("vendorSupplierAdd successfully added {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.VENDOR_ADD, vendorDTO.getBusinessName(), pan);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				List<Vendor> vendorDTOs = taxpayerService.getVendorData(pan, "supplier");
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendorDTOs);
			} else {
				log.info("vendorSupplierAdd fail to add the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorSupplierAdd AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("vendorSupplierAdd JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("vendorSupplierAdd method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will update pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/supplier/update/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorSupplierUpdate(@PathParam("pan") String pan, String request) throws JsonProcessingException {

		log.info("vendorSupplierUpdate method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = JsonMapper.objectMapper.readValue(request, VendorDTO.class);
			vendorDTO = taxpayerService.updateVendor(vendorDTO);
			if (vendorDTO != null) {
				log.info("vendorSupplierUpdate successfully updated {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.VENDOR_UPDATE, vendorDTO.getBusinessName());
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				List<Vendor> vendorDTOs = taxpayerService.getVendorData(pan, "supplier");
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendorDTOs);
			} else {
				log.info("vendorSupplierUpdate fails to update..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorSupplierUpdate AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("vendorSupplierUpdate JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("vendorSupplierUpdate method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will delete pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/supplier/delete/{pan}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorSupplierDelete(@PathParam("id") Integer id, @PathParam("pan") String pan)
			throws JsonProcessingException {

		log.info("vendorSupplierDelete method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = taxpayerService.deleteVendor(id, pan, "supplier");
			if (vendorDTO != null) {
				log.info("vendorSupplierDelete successfully deleted the vendor {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				if (vendorDTO.getId() > 0) {
					String msg = String.format(ApplicationMetadata.VENDOR_DELETE, vendorDTO.getBusinessName());
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				} else {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				}
				List<Vendor> vendors = taxpayerService.getVendorData(pan, "supplier");
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendors);
			} else {
				log.info("vendorSupplierDelete fails to delete the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorSupplierDelete AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("vendorSupplierDelete JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("vendorSupplierDelete method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/buyer/add/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorBuyerAdd(String request, @PathParam("pan") String pan) throws JsonProcessingException {

		log.info("vendorBuyerAdd method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = JsonMapper.objectMapper.readValue(request, VendorDTO.class);
			log.info("vendorBuyerAdd serialize data {}", request);
			vendorDTO = taxpayerService.createVendor(vendorDTO, pan);
			if (vendorDTO != null) {
				/*
				 * MessageDto messageDto=new MessageDto();
				 * messageDto.setCustomerName(vendorDTO.getBusinessName());
				 * apiLoggingService.saveAction(UserAction.ADD_CUSTOMER, messageDto);
				 */
				log.info("vendorBuyerAdd successfully added {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.CUSTOMER_ADD, vendorDTO.getBusinessName(), pan);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				List<Vendor> vendorDTOs = taxpayerService.getVendorData(pan, "buyer");
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendorDTOs);
			} else {
				log.info("vendorBuyerAdd fail to add the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorBuyerAdd AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("vendorBuyerAdd JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("vendorBuyerAdd method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will update pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/buyer/update/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorBuyerUpdate(@PathParam("pan") String pan, String request) throws JsonProcessingException {

		log.info("vendorBuyerUpdate method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = JsonMapper.objectMapper.readValue(request, VendorDTO.class);
			vendorDTO = taxpayerService.updateVendor(vendorDTO);
			if (vendorDTO != null) {
				log.info("vendorBuyerUpdate successfully updated {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.CUSTOMER_UPDATE, vendorDTO.getBusinessName());
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				List<Vendor> vendorDTOs = taxpayerService.getVendorData(pan, "buyer");
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendorDTOs);
			} else {
				log.info("vendorBuyerUpdate fails to update..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorBuyerUpdate AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (Exception e) {
			log.error("vendorBuyerUpdate JSONException exception {} ", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("vendorBuyerUpdate method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will delete pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/buyer/delete/{pan}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorBuyerDelete(@PathParam("id") Integer id, @PathParam("pan") String pan)
			throws JsonProcessingException {

		log.info("vendorBuyerDelete method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = taxpayerService.deleteVendor(id, pan, "buyer");
			if (vendorDTO != null) {
				log.info("vendorBuyerDelete successfully deleted the vendor {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				if (vendorDTO.getId() > 0) {
					String msg = String.format(ApplicationMetadata.CUSTOMER_DELETE, vendorDTO.getBusinessName());
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				} else {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				}
				List<Vendor> vendors = taxpayerService.getVendorData(pan, "buyer");
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendors);
			} else {
				log.info("vendorBuyerDelete fails to delete the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorBuyerDelete AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("vendorBuyerDelete JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("vendorBuyerDelete method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@GET
	@Path("/get/hsndata/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHsndata(@PathParam("pan") String pan) throws JsonProcessingException {

		log.info("getHsndata method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<HsnSac> hsnSacDTOs = taxpayerService.getHsnSacData(pan);
			if (!StringUtils.isEmpty(pan)) {
				log.info("getHsndata successfully got mapping for {}..", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, hsnSacDTOs);
			} else {
				log.info("getHsndata fails to get list for Hsndata..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getHsndata AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getHsndata JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getHsndata method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@POST
	@Path("/add/hsndata/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addHsndata(@PathParam("pan") String pan, String request) throws JsonProcessingException {

		log.info("addHsndata method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			HsnSacDTO hsnSacDTO = JsonMapper.objectMapper.readValue(request, HsnSacDTO.class);
			int index = taxpayerService.addHsnSacData(pan, hsnSacDTO);
			if (index > 0) {
				log.info("addHsndata successfully got mapping for {}..", pan);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.HSN_SUCCESS_MSG);
			} else {
				log.info("addHsndata Unable to add the list for Hsndata..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("addHsndata AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("addHsndata JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("addHsndata method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will delete pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/hsndata/delete/{pan}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hsndataDelete(@PathParam("id") Integer id, @PathParam("pan") String pan)
			throws JsonProcessingException {

		log.info("hsndataDelete method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			HsnSacDTO hsnSacDTO = taxpayerService.deleteHsnSacData(id, pan);
			if (hsnSacDTO != null) {
				log.info("hsndataDelete successfully deleted the hsndata for {}", hsnSacDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				List<HsnSac> hsnSacDTOs = taxpayerService.getHsnSacData(pan);
				respBuilder.put(ApplicationMetadata.RESP_DATA, hsnSacDTOs);
			} else {
				log.info("hsndataDelete fails to delete the hsndata..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("hsndataDelete AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("hsndataDelete JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("hsndataDelete method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@GET
	@Path("/get/invoice/creation/data/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoiceCreationData(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("getInvoiceCreationData method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> obj = taxpayerService.getInvoiceCreationData(gstin);
			if (!StringUtils.isEmpty(gstin)) {
				log.info("getInvoiceCreationData successfully got data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, obj);
			} else {
				log.info("getInvoiceCreationData fails to get list for invoice creation data..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getInvoiceCreationData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getInvoiceCreationData JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getInvoiceCreationData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/vendor/add/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vendorAdd(String request, @PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("vendorAdd method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			VendorDTO vendorDTO = JsonMapper.objectMapper.readValue(request, VendorDTO.class);
			log.info("vendorAdd serialize data {}", request);
			vendorDTO = taxpayerService.createVendorFromGstin(vendorDTO, gstin);
			if (vendorDTO != null) {
				log.info("vendorAdd successfully added {}", vendorDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String type = vendorDTO.getType().contains("supplier") ? "Vendor" : "Customer";
				String msg = String.format(ApplicationMetadata.VENDOR_CUSTOMER_GSTIN_ADD, type,
						vendorDTO.getBusinessName(), gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, vendorDTO);
			} else {
				log.info("vendorAdd fail to add the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("vendorAdd AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("vendorAdd JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("vendorAdd method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/item/add/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response itemAdd(String request, @PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("itemAdd method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			ItemCodeDTO itemCodeDTO = JsonMapper.objectMapper.readValue(request, ItemCodeDTO.class);
			log.info("itemAdd serialize data {}", request);
			itemCodeDTO = taxpayerService.createItemCode(itemCodeDTO, gstin);
			if (itemCodeDTO != null) {
				log.info("itemAdd successfully added {}", itemCodeDTO.getId());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.ITEM_GSTIN_ADD, itemCodeDTO.getDescription(), gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, itemCodeDTO);
			} else {
				log.info("itemAdd fail to add the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("itemAdd AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("itemAdd JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			log.info("itemAdd method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@GET
	@Path("/get/invoice/data/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoiceData(@PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("getInvoiceData method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> object = taxpayerService.getInvoiceData(gstin);
			if (!Objects.isNull(object)) {
				log.info("getInvoiceData successfully got data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, object.get("data"));
				respBuilder.put("invoice_count", object.get("count"));
				respBuilder.put("invoice_value", object.get("value"));
			} else {
				log.info("getInvoiceData fails to get list for invoice creation data..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getInvoiceData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getInvoiceData JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getInvoiceData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings({ "finally" })
	@GET
	@Path("/delete/invoice/data/{gstin}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteInvoiceData(@PathParam("gstin") String gstin, @PathParam("id") Integer id)
			throws JsonProcessingException {

		log.info("deleteInvoiceData method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> object = taxpayerService.deleteInvoiceData(gstin, id);
			if (!Objects.isNull(object)) {
				log.info("deleteInvoiceData successfully got data for {}..", gstin);
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.INVOICE_DELETE_MSG, object.get("invoiceNo"), gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				respBuilder.put(ApplicationMetadata.RESP_DATA, object.get("data"));
				respBuilder.put("invoice_count", object.get("count"));
				respBuilder.put("invoice_value", object.get("value"));
			} else {
				log.info("deleteInvoiceData fails to get list for invoice creation data..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("deleteInvoiceData AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("deleteInvoiceData JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("deleteInvoiceData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/create/invoice/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createInvoice(String request, @PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("createInvoice method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		Map<String, Object> object = null;

		try {
			InvoiceVendorDetailsDto invoiceVendorDetailsDto = JsonMapper.objectMapper.readValue(request,
					InvoiceVendorDetailsDto.class);
			log.info("createInvoice serialize data {}", request);
			object = taxpayerService.createInvoice(invoiceVendorDetailsDto, gstin);
			if (!Objects.isNull(object)) {
				log.info("createInvoice successfully added {}",
						invoiceVendorDetailsDto.getInvoice().getInvoiceNumber());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.INVOICE_ADD_MSG, object.get("invoiceNo"), gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
				object.remove("invoiceNo");
				respBuilder.put(ApplicationMetadata.RESP_DATA, object);
			} else {
				log.info("createInvoice fail to add the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			e.printStackTrace();
			log.debug("createInvoice AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.debug("createInvoice JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("createInvoice JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();

		} finally {
			log.info("createInvoice method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This method will add pan to taxpayer table and tagged requested user as
	 * primary for this pan
	 * 
	 * @param request
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@POST
	@Path("/update/invoice/{gstin}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateInvoice(String request, @PathParam("gstin") String gstin) throws JsonProcessingException {

		log.info("updateInvoice method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String invoiceNo = null;

		try {
			InvoiceVendorDetailsDto invoiceVendorDetailsDto = JsonMapper.objectMapper.readValue(request,
					InvoiceVendorDetailsDto.class);
			log.info("updateInvoice serialize data {}", request);
			invoiceNo = taxpayerService.updateInvoice(invoiceVendorDetailsDto, gstin);
			if (!StringUtils.isEmpty(invoiceNo)) {
				log.info("updateInvoice successfully added {}",
						invoiceVendorDetailsDto.getInvoice().getInvoiceNumber());
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.INVOICE_UPDATE_MSG, invoiceNo, gstin);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("updateInvoice fail to add the vendor..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateInvoice AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("updateInvoice JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} catch (Exception e) {
			log.error("updateInvoice JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("updateInvoice method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/invoice/email/{gstin}/{sno}/{cc}/{attach}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response invoiceEmail(@PathParam("gstin") String gstin, @PathParam("sno") Integer sno,
			@PathParam("cc") Integer cc, @PathParam("attach") Integer attach, String request) throws Exception {

		log.info("invoiceEmail method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean emaiLFlag = false;

		try {
			EmailDTO emailDTO = JsonMapper.objectMapper.readValue(request, EmailDTO.class);
			emaiLFlag = taxpayerService.sendInvoiceEmail(gstin, sno, cc, attach, emailDTO);
			if (emaiLFlag) {
				log.info("invoiceEmail Email is successfully send");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				String msg = String.format(ApplicationMetadata.INVOICE_EMAIL, emailDTO.getMailto());
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, msg);
			} else {
				log.info("invoiceEmail Fails to send the email..");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}

			log.info("invoiceEmail method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;

		} catch (AppException e) {
			log.error("invoiceEmail AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("invoiceEmail JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} catch (Exception e) {
			log.error("invoiceEmail JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("invoiceEmail method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	/**
	 * This will used to tag partner with user
	 * 
	 * @param code
	 * @return
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("finally")
	@GET
	@Path("/generate/report3b/{monthYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response generateReport3B(@PathParam("monthYear") String monthYear) throws JsonProcessingException {

		log.info("generateReport3B method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		String url = null;

		try {

			ExagoReportHelper reportHelper = new ExagoReportHelper();
			url = reportHelper.getReportUrl(userBean.getUserDto(), monthYear);
			if (!StringUtils.isEmpty(url)) {
				log.info("generateReport3B created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG_3B);

				// logging user action
				MessageDto messageDto = new MessageDto();
				messageDto.setGstin("All gstins");
				messageDto.setMonthYear(monthYear);
				apiLoggingService.saveAction(UserAction.GENRATE_GSTR3B, messageDto, "all");

				// end logging user action

			} else {
				log.info("generateReport3B Partner Code fails to submit");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("generateReport3B AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("generateReport3B JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("generateReport3B method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/save/return/preference/{gstin}/{preference}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveReturnFilingPreference(@QueryParam("financialYear") String fyear,
			@PathParam("gstin") String gstin, @PathParam("preference") String preference)
			throws JsonProcessingException {

		log.info("saveReturnFilingPreference method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			if (StringUtils.isEmpty(fyear))
				fyear = CalanderCalculatorUtility.getCurrentFinancialYear();
			boolean isSuccess = taxpayerService.savePreference(gstin, preference, fyear);
			if (isSuccess) {
				log.info("saveReturnFilingPreference created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, String.format(ApplicationMetadata.PREFERENCE_SAVED,
						gstin, Constant.Returnpreference.valueOf(preference.toUpperCase()), fyear));
			} else {
				log.info("saveReturnFilingPreference Partner Code fails to submit");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("saveReturnFilingPreference AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("saveReturnFilingPreference JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("saveReturnFilingPreference method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/notification/{action}/{notificationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateNotification(@PathParam("action") String action,
			@PathParam("notificationId") String notificationId) throws JsonProcessingException {

		log.info("updateNotification method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		try {

			int id = notificationService.updateNotificationAction(action, notificationId);
			if (id > 0) {

				if ("DEACTIVE".equalsIgnoreCase(action)) {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.NOTIFICATION_DEACTIVATED);

				}
				if ("SEEN".equalsIgnoreCase(action)) {
					respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.NOTIFICATION_SEEN);

				}
				log.info("updateNotification created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			} else {
				log.info("updateNotification fails to update");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("updateNotification AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("updateNotification JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("updateNotification method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/notification/{notificationCategory}/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotifications(@PathParam("notificationCategory") String notificationCategory,
			@PathParam("type") String type, FilterDto filter) throws JsonProcessingException {

		log.info("getNotifications method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<String> categories = new ArrayList<>();
			categories.add(notificationCategory);
			List<Boolean> isSeens = new ArrayList<>();
			isSeens.add(Boolean.FALSE);
			List<ActionLogDto> notifications = null;
			Map<String, Object> respMap = null;
			if ("SELECTED".equalsIgnoreCase(type))
				respMap = notificationService.getNotifications(userBean.getUserDto(), isSeens, categories);
			else
				respMap = notificationService.getNotificationsAll(userBean.getUserDto(), isSeens, categories, filter);
			// respMap=taxpayerService.getFilteredNotificationsAll(userBean.getUserDto(),
			// isSeens, categories,filter);

			if (!Objects.isNull(respMap)) {

				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, respMap);

				log.info("getNotifications created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			} else {
				log.info("getNotifications fails to update");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getNotifications AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getNotifications JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getNotifications method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/notification/count/{notificationCategory}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotificationsCount(@PathParam("notificationCategory") String notificationCategory)
			throws JsonProcessingException {

		log.info("getNotificationsCount method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			List<String> categories = new ArrayList<>();
			categories.add(notificationCategory);
			List<Boolean> isSeens = new ArrayList<>();
			isSeens.add(Boolean.FALSE);
			Map<String, Object> countMap = new HashMap<>();

			countMap = notificationService.getNotificationCount(categories, isSeens);
			if (!Objects.isNull(countMap)) {
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
				respBuilder.put(ApplicationMetadata.RESP_DATA, countMap);

				log.info("getNotificationsCount created successfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			} else {
				log.info("getNotificationsCount fails to update");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}
		} catch (AppException e) {
			log.error("getNotificationsCount AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
			e.printStackTrace();
		} catch (JsonException e) {
			log.error("getNotificationsCount JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getNotificationsCount method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@GET
	@Path("/get/erpdata/flush")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getalltin() throws JsonProcessingException {

		log.info("getPan method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);

		try {
			Map<String, Object> taxpayerDto = taxpayerService.getAlltinDetail(userBean.getUserDto().getId());
			respBuilder.put(ApplicationMetadata.RESP_DATA, taxpayerDto);
			respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
			respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);

		} catch (AppException e) {
			log.error("getalltin AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("getalltin JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("getalltin method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

	@SuppressWarnings("finally")
	@PUT
	@Path("/get/erpdata/flush")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeErpData(String request) throws JsonProcessingException {

		log.info("removeErpData method starts..");

		Map<String, Object> respBuilder = new HashMap<>();
		respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
		respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
		boolean flag = true;
		try {
			List<TinDTO> tinDTOlist = JsonMapper.objectMapper.readValue(request, new TypeReference<List<TinDTO>>() {
			});
			log.info(tinDTOlist.toString());
			flag = taxpayerService.updateflagTaxpayerGstin(tinDTOlist);
			if (flag) {
				log.info("flush data sucessfully");
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._SUCCESS);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.SUCCESS_MSG);
			} else {
				respBuilder.put(ApplicationMetadata.RESP_CODE_KEY, ExceptionCode._FAILURE);
				respBuilder.put(ApplicationMetadata.RESP_MSG_KEY, ApplicationMetadata.FAILURE_MSG);
			}

		} catch (AppException e) {
			log.error("flush data AppException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, e.getMessageJsonFromObject(e.getCode()));
		} catch (JsonException e) {
			log.error("flush data JSONException exception {} ", e.getMessage());
			respBuilder.put(ApplicationMetadata.RESP_ERROR, ApplicationMetadata.INVALID_JSON_REQUEST);
			e.printStackTrace();
		} finally {
			log.info("removeErpData method ends..");
			Response response = Response.status(200).entity(respBuilder).build();
			return response;
		}
	}

}
