package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the hsn_sac database table.
 * 
 */
@Entity
@Table(name = "hsn_sac")
@NamedQueries({ @NamedQuery(name = "HsnSac.findAll", query = "SELECT h FROM HsnSac h"),
		@NamedQuery(name = "HsnSac.findByCode", query = "SELECT h FROM HsnSac h where h.type=:type and h.code like :code"),
		@NamedQuery(name = "HsnSac.findByIdCode", query = "SELECT h FROM HsnSac h where h.code=:code and h.taxpayer.id=:id"),
		@NamedQuery(name = "HsnSac.findByTaxpayerId", query = "SELECT h FROM HsnSac h WHERE h.taxpayer.id=:id") })
public class HsnSac implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String cessRate;

	private String chapter;

	@Lob
	private String chapterDesc;

	private String code;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	@Lob
	private String description;

	private String displayName;

	private String subChapter;

	@Lob
	private String subChapterDesc;

	private String taxRate;

	private String type;

	// bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "taxpayerId")
	private Taxpayer taxpayer;

	public HsnSac() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCessRate() {
		return this.cessRate;
	}

	public void setCessRate(String cessRate) {
		this.cessRate = cessRate;
	}

	public String getChapter() {
		return this.chapter;
	}

	public void setChapter(String chapter) {
		this.chapter = chapter;
	}

	public String getChapterDesc() {
		return this.chapterDesc;
	}

	public void setChapterDesc(String chapterDesc) {
		this.chapterDesc = chapterDesc;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getSubChapter() {
		return this.subChapter;
	}

	public void setSubChapter(String subChapter) {
		this.subChapter = subChapter;
	}

	public String getSubChapterDesc() {
		return this.subChapterDesc;
	}

	public void setSubChapterDesc(String subChapterDesc) {
		this.subChapterDesc = subChapterDesc;
	}

	public String getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Taxpayer getTaxpayer() {
		return this.taxpayer;
	}

	public void setTaxpayer(Taxpayer taxpayer) {
		this.taxpayer = taxpayer;
	}

}