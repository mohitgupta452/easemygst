package com.ginni.easemygst.portal.persistence.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse;

@Entity
@Table(name="encoded_data")

//@NamedNativeQueries({
//    @NamedNativeQuery(
//            name    =   "updateStatus",
//            query   =   "update token_response set status='COMPLETED' Where id in (select id from(SELECT distinct ed.referenceId as id from encoded_data ed,token_response t where t.id=ed.referenceId and t.urlcount in (select count(*) from encoded_data where status='COMPLETED')) as dummy )"
//    )
//})
@NamedQueries({@NamedQuery(name="EncodedData.findByReferenceId",query="SELECT ed from EncodedData ed where ed.referenceId.id=:referenceId"),
	@NamedQuery(name="EncodedData.findByPending",query="SELECT ed from EncodedData ed where status='PENDING'"),
	@NamedQuery(name="EncodedData.findByIP",query="SELECT ed from EncodedData ed where ed.status='INPROCESS' and ed.processId=:processId"),
	@NamedQuery(name="EncodedData.findByReference",query="SELECT ed from EncodedData ed where ed.referenceId=:reference"),
	@NamedQuery(name="EncodedData.findByINPROCESS",query="SELECT ed from EncodedData ed where ed.status='INPROCESS'")})
public class EncodedData {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="referenceId")
	private TokenResponse referenceId;
	
	private String encodedData;
	
	private Integer invoiceCount;
	
	private String status;
	
	private Boolean isDelete=true;
	
	private String processId;
	

	public Integer getInvoiceCount() {
		return invoiceCount;
	}

	public void setInvoiceCount(Integer invoiceCount) {
		this.invoiceCount = invoiceCount;
	}

	public TokenResponse getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(TokenResponse referenceId) {
		this.referenceId = referenceId;
	}

	public String getEncodedData() {
		return encodedData;
	}

	public void setEncodedData(String encodedData) {
		this.encodedData = encodedData;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}


}
