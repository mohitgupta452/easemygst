package com.ginni.easemygst.portal.persistence.convertor;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.data.transaction.CDN.CdnData;

public class CdnDataJsonConverter implements AttributeConverter<CdnData,String>{

	private final static ObjectMapper _Mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(CdnData attribute) {
		try {
			return _Mapper.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;	
	}

	@Override
	public CdnData convertToEntityAttribute(String dbData) {
		try {
			return _Mapper.readValue(dbData,CdnData.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
