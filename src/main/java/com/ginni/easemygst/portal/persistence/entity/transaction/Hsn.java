package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.view.serializer.HsnDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the hsn database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="HsnTransaction.findAll", query="SELECT t FROM Hsn t"),
	@NamedQuery(name = "HsnTransaction.getByGstinMonthyearReturntype", query = "select b from Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "HsnTransaction.findByPagination", query = "select b from Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "HsnTransaction.findCountByMonthYear", query = "select count(b.id) from Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "HsnTransaction.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	
	@NamedQuery(name="HsnTransaction.removeById",query="delete from Hsn b where b.id in :ids "),
	@NamedQuery(name="HsnTransaction.deleteByMonthYear",query="delete from Hsn b where b.monthYear =:monthYear and b.taxpayerGstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType"),
	@NamedQuery(name="HsnTransaction.updateFlagByMonthYear",query="update  Hsn b set b.flags='DELETE',b.toBeSync=true where b.monthYear=:monthYear and b.taxpayerGstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),

	
	@NamedQuery(name = "HsnTransaction.findSummaryByMonthYear", query = "select SUM(b.taxableValue),SUM(b.taxAmount) from Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name="Hsn.findNotSynced",query="select a from Hsn a where a.taxpayerGstin=:gstn and a.monthYear in (:monthYear) and a.isError=false and a.returnType=:returnType and a.isTransit=false"),
	@NamedQuery(name = "HsnTransaction.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "Hsn.findErrorInvoice",query = "Select b from Hsn  b where b.taxpayerGstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) "),
	@NamedQuery(name= "Hsn.findErrorInvoicesByTransitId",query="SELECT b from Hsn b where b.taxpayerGstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="Hsn.getFlagsCount",query="select new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from Hsn b where b.monthYear=:monthYear and b.returnType=:returnType and b.taxpayerGstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError "),
	@NamedQuery(name = "HsnTransaction.findErrorDataByMonthYear", query = "select b from Hsn b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "HsnTransaction.UnsyncByMonthYear", query = "update Hsn b set b.toBeSync=true where b.taxpayerGstin=:gstinid and b.monthYear=:monthYear and b.returnType=:returntype"),
	//' 




})
@JsonSerialize(using=HsnDetailSerializer.class)

public class Hsn extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private double cess;

	private double cgst;

	private String code;

	private String description;

	private double igst;

	private String monthYear;

	private double quantity;

	private double sgst;

	private String source;
	
	private String sourceId;

	private double taxableValue;

	private double taxAmount;

	@JoinColumn(name="taxpayerGstinId")
	@ManyToOne
	private TaxpayerGstin taxpayerGstin;

	private String unit;

	private double value;
	
	private String returnType;
	
	private Boolean toBeSync;
	
	private Boolean isSynced;
	
	private Boolean isTransit;
	
	private String flags;
	
	private transient String error_msg;
	
	private transient String error_cd;
	
	private Boolean isError;
	
	private String errMsg;
	
	private String transitId;
	

	public Hsn() {
	}


	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}


	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}


	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public Boolean getIsSynced() {
		return isSynced;
	}

	public void setIsSynced(Boolean isSynced) {
		this.isSynced = isSynced;
	}

	public Boolean getIsTransit() {
		return isTransit;
	}

	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	public String getTransitId() {
		return transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}
	
	

	public double getCess() {
		return cess;
	}


	public void setCess(double cess) {
		this.cess = cess;
	}


	public double getCgst() {
		return cgst;
	}


	public void setCgst(double cgst) {
		this.cgst = cgst;
	}


	public double getIgst() {
		return igst;
	}


	public void setIgst(double igst) {
		this.igst = igst;
	}


	public double getSgst() {
		return sgst;
	}


	public void setSgst(double sgst) {
		this.sgst = sgst;
	}


	public double getTaxableValue() {
		return taxableValue;
	}


	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}


	public double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}


	public double getValue() {
		return value;
	}


	public void setValue(double value) {
		this.value = value;
	}


	public Hsn(String code, String description, String unit, double quantity,double value, double taxableValue,
			double taxAmount,String source,String id,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError) {
		this.code=code;
		this.description=description;
		this.unit=unit;
		this.quantity=quantity;
		this.value=value;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.source = source;
		this.setId(id);
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;


	}
	
	
	public Hsn(String id,TaxpayerGstin taxpayerGstin,String monthYear,String returnType,String code,
			String description,double taxableValue, double taxAmount,String unit, double quantity,double value,
			double igst,double cgst,double sgst,double cess 
			) {
		this.setId(id);
		this.taxpayerGstin=taxpayerGstin;
		this.returnType=returnType;
		this.monthYear=monthYear;
		this.code=code;
		this.description=description;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.unit=unit;
		this.quantity=quantity;
		this.value=value;
		this.igst=igst;
		this.cgst=cgst;
		this.sgst=sgst;
		this.cess=cess;
		
		
		


	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

}