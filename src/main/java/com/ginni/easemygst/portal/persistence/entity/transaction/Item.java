package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.view.serializer.CustomDateSerializer;
import com.ginni.easemygst.portal.transaction.factory.Transaction;


/**
 * The persistent class for the items database table.
 * 
 */
@Entity
@Table(name="items")
@NamedQueries({
@NamedQuery(name="Item.findAll", query="SELECT i FROM Item i"),
@NamedQuery(name="Item.findByInvoiceId", query="SELECT i FROM Item i where i.invoiceId in :invoices and (i.totalEligibleTax is NULL or i.totalEligibleTax=:elgType)"),
@NamedQuery(name="Item.DeleteByInvoiceId", query="DELETE  FROM Item i where i.invoiceId =:invoiceId"),



})
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid-item")
	@GenericGenerator(name = "system-uuid-item", strategy = "uuid2")
	private String id;
	
	private double cess;

	private double cgst;

	private String hsnCode;

	private String hsnDescription;

	private double igst;

	private double quantity;

	private int serialNumber;

	private double sgst;

	private double taxableValue;
	
	@Transient
	private double taxAmount;

	private double taxRate;

	private String totalEligibleTax;

	private String unit;
	
	private double itcCess;

	private double itcCgst;

	private double itcIgst;

	private double itcSgst;
	
	private String invoiceId;
	
	@Transient
	private String ctin;
	
	@Transient
	private String ctinName;
	
	@Transient
	private String invoiceNum;
	
	@Transient
	//@JsonFormat(pattern = Transaction.jsonDateFormat, timezone = Transaction.jsonDateTimeZone)
	@JsonSerialize(using=CustomDateSerializer.class)
	private Date invoiceDate;
	
	
	

	public Item() {
	}

	public double getCess() {
		return this.cess;
	}

	public void setCess(double cess) {
		this.cess = cess;
	}

	public double getCgst() {
		return this.cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public String getHsnCode() {
		return this.hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getHsnDescription() {
		return this.hsnDescription;
	}

	public void setHsnDescription(String hsnDescription) {
		this.hsnDescription = hsnDescription;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getIgst() {
		return this.igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public double getItcCess() {
		return this.itcCess;
	}

	public void setItcCess(double itcCess) {
		this.itcCess = itcCess;
	}

	public double getItcCgst() {
		return this.itcCgst;
	}

	public void setItcCgst(double itcCgst) {
		this.itcCgst = itcCgst;
	}

	public double getItcIgst() {
		return this.itcIgst;
	}

	public void setItcIgst(double itcIgst) {
		this.itcIgst = itcIgst;
	}

	public double getItcSgst() {
		return this.itcSgst;
	}

	public void setItcSgst(double itcSgst) {
		this.itcSgst = itcSgst;
	}

	public double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public double getSgst() {
		return this.sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public String getTotalEligibleTax() {
		return totalEligibleTax;
	}

	public void setTotalEligibleTax(String totalEligibleTax) {
		this.totalEligibleTax = totalEligibleTax;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	public String getCtin() {
		return ctin;
	}

	public void setCtin(String ctin) {
		this.ctin = ctin;
	}

	public String getCtinName() {
		return ctinName;
	}

	public void setCtinName(String ctinName) {
		this.ctinName = ctinName;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
		
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Item(Item item) {
		super();
		
		//this.id = item.getId();
		this.cess = item.getCess();
		this.cgst = item.getCgst();
		this.hsnCode = item.getHsnCode();
		this.hsnDescription = item.getHsnDescription();
		this.igst = item.getIgst();
		this.quantity = item.getQuantity();
		this.serialNumber = item.getSerialNumber();
		this.sgst = item.getSgst();
		this.taxableValue = item.getTaxableValue();
		this.taxAmount = item.getTaxAmount();
		this.taxRate = item.getTaxRate();
		this.totalEligibleTax = item.getTotalEligibleTax();
		this.unit = item.getTotalEligibleTax();
		this.itcCess = item.getCess();
		this.itcCgst = item.getItcCgst();
		this.itcIgst = item.getItcIgst();
		this.itcSgst = item.getItcSgst();
		this.invoiceId =item.getInvoiceId();
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.id, ((Item) obj).getId());
		return builder.isEquals();
	}
	

}