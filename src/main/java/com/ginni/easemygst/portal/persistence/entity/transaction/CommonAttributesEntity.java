package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public abstract class CommonAttributesEntity {
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;
	
	private String createdBy;
	
	private Timestamp creationTime;
	
	private String creationIPAddress;
	
	private String updatedBy;
	
	private Timestamp updationTime=Timestamp.from(Instant.now());
	
	private String updationIPAddress;
	

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getCreationIPAddress() {
		return creationIPAddress;
	}

	public void setCreationIPAddress(String creationIPAddress) {
		this.creationIPAddress = creationIPAddress;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getUpdationIPAddress() {
		return updationIPAddress;
	}

	public void setUpdationIPAddress(String updationIPAddress) {
		this.updationIPAddress = updationIPAddress;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<Item> getItems() {
		return null;
	}

	public void setItems(List<Item> items) {
		
	}
	
}
