package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
	@NamedQuery(name = "User.findIdentityByUsername", query = "SELECT new com.ginni.easemygst.portal.business.entity.UserDTO(u.id, u.username, u.password, u.firstName, u.lastName, u.isChangeRequired, u.requestId, u.preferredGsp) FROM User u WHERE u.username=:username and u.active=:active"),
	@NamedQuery(name = "User.findUserIdentityByUsername", query = "SELECT new com.ginni.easemygst.portal.business.entity.UserDTO(u.id, u.username, u.password, u.firstName, u.lastName, u.isChangeRequired, u.requestId, u.preferredGsp) FROM User u WHERE u.username=:username"),
	@NamedQuery(name = "User.findUserIdentityByOrg", query = "SELECT new com.ginni.easemygst.portal.business.entity.UserDTO(u.id, u.username, u.password, u.firstName, u.lastName, u.isChangeRequired, u.requestId, u.preferredGsp) FROM User u WHERE u.organisation.id=:orgId"),
	@NamedQuery(name = "User.findIdentityById", query = "SELECT u FROM User u WHERE u.id=:id and u.active=:active"),
	@NamedQuery(name="User.findIdByUsername",query="select u from User u where u.username=:username"),
    @NamedQuery(name="User.findIdByorganisationId",query="select u.id from User u where u.organisation.id=:organisationId")

})
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Byte active;

	private Byte isChangeRequired;
	
	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;
	
	private String creationIpAddress;

	private String facebookId;

	private String firstName;

	private String googleId;

	private String lastName;

	private String password;

	private String status;
	
	@JsonIgnore
	private String secureKey;
	
	private String preferredGsp;
	
	private String aadharNo;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;
	
	@JsonIgnore
	private String updationIpAddress;

	private String username;
	
	private Byte billToPartner;
	
	private String requestId;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="organisationId")
	private Organisation organisation;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JoinColumn(name="partnerId")
	private Partner partner;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="skuPartner")
	private SkuPartner skuPartner;

	//bi-directional many-to-one association to UserContact
	@OneToMany(mappedBy="user", cascade = CascadeType.ALL, orphanRemoval = true,fetch=FetchType.EAGER)
	private List<UserContact> userContacts;

	public User() {
	
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Byte getActive() {
		return this.active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Byte getIsChangeRequired() {
		return isChangeRequired;
	}

	public void setIsChangeRequired(Byte isChangeRequired) {
		this.isChangeRequired = isChangeRequired;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getFacebookId() {
		return this.facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPreferredGsp() {
		return preferredGsp;
	}

	public void setPreferredGsp(String preferredGsp) {
		this.preferredGsp = preferredGsp;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getGoogleId() {
		return this.googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSecureKey() {
		return secureKey;
	}

	public void setSecureKey(String secureKey) {
		this.secureKey = secureKey;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public String getUsername() {
		if(!StringUtils.isEmpty(this.username)) return this.username.toLowerCase();
		return this.username;
	}

	public void setUsername(String username) {
		if(!StringUtils.isEmpty(this.username)) this.username = this.username.toLowerCase();
		this.username = username;
	}

	public List<UserContact> getUserContacts() {
		return this.userContacts;
	}

	public void setUserContacts(List<UserContact> userContacts) {
		this.userContacts = userContacts;
	}

	public UserContact addUserContact(UserContact userContact) {
		getUserContacts().add(userContact);
		userContact.setUser(this);

		return userContact;
	}

	public UserContact removeUserContact(UserContact userContact) {
		getUserContacts().remove(userContact);
		userContact.setUser(null);

		return userContact;
	}
	
	public SkuPartner getSkuPartner() {
		return skuPartner;
	}

	public void setSkuPartner(SkuPartner skuPartner) {
		this.skuPartner = skuPartner;
	}

	public Byte getBillToPartner() {
		return billToPartner;
	}

	public void setBillToPartner(Byte billToPartner) {
		this.billToPartner = billToPartner;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}
	
}