package com.ginni.easemygst.portal.persistence.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface CrudService {

	public <T> T create(T t);
	public <T> T find(Class<T> type, Object id);
	public void delete(@SuppressWarnings("rawtypes") Class type, Object id);
	public <T> T update(T t);
	
	public <T> List<T> createQueryResult(String queryString, Class<T> type);
	
	public <T> List<T> createQuery(String queryString);
	
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName);
	
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters);
	
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String queryName, int resultLimit);
	
	public <T> List<T> findByNativeQuery(String sql, Class<T> type);
	
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit);
	
	public<T> List<T> findPaginatedResults(String queryName,Map<String,Object>parameters ,int pageNo,int size);
	
	
	public long findResultCounts(String queryName,Map<String,Object>parameters);
	
	public Object[] runAggregateNamedQuery(String queryName,Map<String,Object>parameters);


}