package com.ginni.easemygst.portal.persistence.entity;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the file_prefernce database table.
 * 
 */
@Entity
@Table(name="file_prefernce")
@NamedQueries({
@NamedQuery(name="FilePrefernce.findAll", query="SELECT f FROM FilePrefernce f"),
@NamedQuery(name="FilePrefernce.updateturnover", query="update FilePrefernce f set f.previousGt=:previousGt ,f.currentGt=:currentGt  where f.financialYear=:fyear and f.taxpayerGstin=:taxpayergstin "),

@NamedQuery(name="FilePrefernce.findPreference", query="SELECT f FROM FilePrefernce f where f.taxpayerGstin.gstin=:gstin and f.financialYear=:financialYear")
})


public class FilePrefernce implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String financialYear;

	private String preference;


	private BigDecimal previousGt;

	private BigDecimal currentGt;

	@ManyToOne
	@JoinColumn(name="taxpayerGstinId")
	private TaxpayerGstin taxpayerGstin;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	public FilePrefernce() {
	}

	public BigDecimal getPreviousGt() {
		return previousGt;
	}

	public void setPreviousGt(BigDecimal previousGt) {
		this.previousGt = previousGt;
	}

	public BigDecimal getCurrentGt() {
		return currentGt;
	}

	public void setCurrentGt(BigDecimal currentGt) {
		this.currentGt = currentGt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getFinancialYear() {
		return this.financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public String getPreference() {
		return this.preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}


	@PrePersist
	 void createdAt() {
	   this.creationTime = new Timestamp(new Date().getTime());
	 }

	@PreUpdate
	 void updatedAt() {
	   this.updationTime = new Timestamp(new Date().getTime());
	}
	
}