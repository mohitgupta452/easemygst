package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.view.serializer.TXPDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * The persistent class for the txpd_transaction database table.
 * 
 */
@Entity
@Table(name="txpd_transaction")
@NamedQueries({
	@NamedQuery(name="TxpdTransaction.findAll", query="SELECT t FROM TxpdTransaction t"),
	@NamedQuery(name = "TxpdTransaction.getByGstinMonthyearReturntype", query = "select b from TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "TxpdTransaction.findByPagination", query = "select b from TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "TxpdTransaction.findCountByMonthYear", query = "select count(b.id) from TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "TxpdTransaction.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	
	@NamedQuery(name="TxpdTransaction.removeById",query="delete from TxpdTransaction b where b.id in :ids and b.isSynced=false and b.isTransit=false "),
	@NamedQuery(name="TxpdTransaction.deleteByMonthYear",query="delete from TxpdTransaction b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false  and b.returnType =:returnType"),
	@NamedQuery(name="TxpdTransaction.updateFlagByMonthYear",query="update  TxpdTransaction b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
	@NamedQuery(name="TxpdTransaction.updateFlagById",query="update TxpdTransaction b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),

	
	@NamedQuery(name = "TxpdTransaction.findSummaryByMonthYear", query = "select SUM(b.advanceAdjusted),SUM(b.taxAmount) from TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "TxpdTransaction.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
	@NamedQuery(name="TxpdTransaction.findNotSynced",query="select a from TxpdTransaction a where a.gstin=:gstn and a.monthYear in (:monthYear) and a.returnType=:returnType and a.isTransit=false and a.isError=false"),
	@NamedQuery(name = "TxpdTransaction.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id) FROM TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "TxpdTransaction.findErrorInvoice",query = "Select b from TxpdTransaction  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) and b.pos=:pos "),
	@NamedQuery(name= "TxpdTransaction.findErrorInvoicesByTransitId",query="SELECT b from TxpdTransaction b where b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="TxpdTransaction.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from TxpdTransaction b where  b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
	@NamedQuery(name = "TxpdTransaction.findErrorDataByMonthYear", query = "select b from TxpdTransaction b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),

})
@JsonSerialize(using=TXPDetailSerializer.class)
public class TxpdTransaction extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private double advanceAdjusted;

	private String flags;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;
	
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="invoiceId",referencedColumnName="id")
	private List<Item> items;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSubmited;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String monthYear;

	private String pos;
	
	private String stateName;
	
	@Enumerated(EnumType.STRING)
	private SupplyType supplyType;


	private String returnType;

	private String source;

	private String sourceId;

	private String synchId;

	private double taxAmount;

	private String transitId;

	private String type;
	
	private String ammendmentMonthYear;
	
	private Boolean toBeSync;
	
	private transient String error_msg;
	
	private transient String error_cd;
	
	private String errMsg;
	
	private Boolean isError=false;

	public TxpdTransaction() {
	}

	@Column(name="originalMonthYear")
	private String originalMonth ;
	
	/*private String originalPos;
	
	private String originalSupplyType;*/
	
	/*public String getOriginalPos() {
		return originalPos;
	}

	public void setOriginalPos(String originalPos) {
		this.originalPos = originalPos;
	}

	public String getOriginalSupplyType() {
		return originalSupplyType;
	}

	public void setOriginalSupplyType(String originalSupplyType) {
		this.originalSupplyType = originalSupplyType;
	}*/

	public String getOriginalMonth() {
		return originalMonth;
	}

	public void setOriginalMonth(String originalMonth) {
		this.originalMonth = originalMonth;
	}


	
	public double getAdvanceAdjusted() {
		return this.advanceAdjusted;
	}

	public void setAdvanceAdjusted(double advanceAdjusted) {
		this.advanceAdjusted = advanceAdjusted;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSubmited() {
		return this.isSubmited;
	}

	public void setIsSubmited(boolean isSubmited) {
		this.isSubmited = isSubmited;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getPos() {
		return this.pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAmmendmentMonthYear() {
		return ammendmentMonthYear;
	}

	public void setAmmendmentMonthYear(String ammendmentMonthYear) {
		this.ammendmentMonthYear = ammendmentMonthYear;
	}
	
	
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public SupplyType getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(SupplyType supplyType) {
		this.supplyType = supplyType;
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}
	
	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}
	
	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public TxpdTransaction(SupplyType supplyType, String id, String pos, String stateName, double advanceAdjusted,
			double taxAmount, String flags, String type, String source,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError
			,boolean isAmmendment,String originalMonth/*,String originalPos,String originalSupplyType*/
			) {
		this.supplyType=supplyType;
		this.setId(id);
		this.pos=pos;
		this.stateName=stateName;
		this.advanceAdjusted=advanceAdjusted;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.source = source;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
		this.isAmmendment=isAmmendment;
		this.originalMonth=originalMonth;
		/*this.originalPos=originalPos;
		this.originalSupplyType=originalSupplyType;*/
	}
	
	
	//constructor for save to gstn
		public TxpdTransaction(TaxpayerGstin gstin,String returnType,String monthYear,SupplyType supplyType, String id, String pos, String stateName, double advanceAdjusted,
				double taxAmount, String flags, String type,Item item,boolean isAmendment) {
			this.supplyType=supplyType;
			this.setId(id);
			this.pos=pos;
			this.stateName=stateName;
			this.advanceAdjusted=advanceAdjusted;
			this.taxAmount = taxAmount;
			this.flags = flags;
			this.type = type;
			this.returnType=returnType;
			this.monthYear=monthYear;
			this.gstin=gstin;
			this.isAmmendment=isAmendment;
			if(Objects.isNull(items)){
				this.items=new ArrayList<>();
				this.getItems().add(item);
				
			}else{
				this.getItems().add(item);
			}
		}
		//constructor for save to gstn
		public TxpdTransaction(TaxpayerGstin gstin,String returnType,String monthYear,SupplyType supplyType, String id, String pos, String stateName, double advanceAdjusted,
				double taxAmount, String flags, String type,Item item,boolean isAmendment,String originalMonth) {
			this.supplyType=supplyType;
			this.setId(id);
			this.pos=pos;
			this.stateName=stateName;
			this.advanceAdjusted=advanceAdjusted;
			this.taxAmount = taxAmount;
			this.flags = flags;
			this.type = type;
			this.returnType=returnType;
			this.monthYear=monthYear;
			this.gstin=gstin;
			this.isAmmendment=isAmendment;
			this.originalMonth=originalMonth;
			if(Objects.isNull(items)){
				this.items=new ArrayList<>();
				this.getItems().add(item);
				
			}else{
				this.getItems().add(item);
			}
		}	
		
	public TransactionType getTransactionType() {
		return TransactionType.TXPD;
	}
	
}