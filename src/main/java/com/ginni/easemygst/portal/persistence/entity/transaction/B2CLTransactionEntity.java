package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the b2cl_transaction database table.
 * 
 */
@Entity
@Table(name="b2cl_transaction")
@NamedQueries({ @NamedQuery(name = "B2clTransaction.findAll", query = "SELECT b FROM B2CLTransactionEntity b"),
	@NamedQuery(name="B2CLTransactionEntity.removeByMonthYear",query="delete from B2CLTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.gstin=:gstin and b2b.returnType =:returnType"),
	@NamedQuery(name = "B2clTransaction.getByEmbeded", query = "select b from B2CLTransactionEntity b where  b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "B2clTransaction.getByGstinMonthyearReturntype", query = "select b from B2CLTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear") 
	
})

public class B2CLTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String monthYear;

	private String returnType;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="taxpayerGstinId")
	private TaxpayerGstin gstin;

	private Double taxableValue=0.0;

	private Double taxAmount=0.0;

	@JsonIgnore
	@OneToMany(mappedBy="b2clTransaction",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<B2CLDetailEntity> b2clDetails;
	
	private transient String error_msg;
	
	private transient String error_cd;
	

	public B2CLTransactionEntity() {
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}
	
	public double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public List<B2CLDetailEntity> getB2clDetails() {
		return b2clDetails;
	}

	public void setB2clDetails(List<B2CLDetailEntity> b2clDetails) {
		this.b2clDetails = b2clDetails;
	}

	
	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder()
			   .append(this.getGstin(), ((B2CLTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((B2CLTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((B2CLTransactionEntity) obj).getMonthYear())
			   ;               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode());
		return result;
	}
}