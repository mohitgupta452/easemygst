package com.ginni.easemygst.portal.persistence;

import lombok.Data;

public @Data class DataFilter {
	private boolean isActive;
	
	private String filter1Name;

	private String filter1Value;
	
	private String operator;
	
	private String filter2Name;
	
	private String filter2Value;
	
	//following fields are use to handle notification filters
	private String by;
	
	private String fromDate;
	
	private String toDate;
	
	private String noOfDays;
	
	private String userName;
	
	private String userType;
	
	private String gstin;
	
	//end

	
	
}
