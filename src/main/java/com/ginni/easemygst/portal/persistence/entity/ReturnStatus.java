package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The persistent class for the return_status database table.
 * 
 */
@Entity
@Table(name="return_status")
@NamedQueries({ @NamedQuery(name="ReturnStatus.findAll", query="SELECT r FROM ReturnStatus r"),
	@NamedQuery(name="ReturnStatus.findByData", query="SELECT r FROM ReturnStatus r WHERE r.taxpayerGstin.gstin=:gstin "
			+ "and r.monthYear=:monthYear and r.returnType=:returnType and r.status=:status"),
	@NamedQuery(name="ReturnStatus.findByFlag", query="SELECT r FROM ReturnStatus r WHERE r.taxpayerGstin.gstin=:gstin "
			+ "and r.monthYear=:monthYear and r.returnType=:returnType and r.flag=:flag"),
	@NamedQuery(name="ReturnStatus.findTransByTransitId", query="SELECT r FROM ReturnStatus r WHERE r.transitId=:transitId "
			+ "and r.monthYear=:monthYear and r.returnType=:returnType and r.taxpayerGstin=:gstn"),
	@NamedQuery(name="ReturnStatus.findByDataTrans", query="SELECT r FROM ReturnStatus r WHERE r.taxpayerGstin.gstin=:gstin "
			+ "and r.monthYear=:monthYear and r.returnType=:returnType and r.status=:status and r.transaction=:transType"),
	@NamedQuery(name="ReturnStatus.findIPTrans",query="SELECT r from ReturnStatus r  where r.flag='IP' and r.taxpayerGstin=:gstn"),
	@NamedQuery(name="ReturnStatus.findALLIP",query="SELECT r from ReturnStatus r where r.flag='IP'"),
	@NamedQuery(name="ReturnStatus.findIPByGstn",query="SELECT r from ReturnStatus r where r.flag='IP' and r.taxpayerGstin=:gstn"
			+ " and r.monthYear=:monthYear and r.returnType=:returnType"),
	@NamedQuery(name="ReturnStatus.findById",query="SELECT r from ReturnStatus r where r.id=:id"),
	@NamedQuery(name="ReturnStatus.findIPDataByGstn",query="SELECT new com.ginni.easemygst.portal.persistence.entity.ReturnStatus(r.id,r.event,r.monthYear,r.referenceId,r.returnType,r.status,r.transitId,r.response,r.transaction,r.totalSync,r.totalNotSync,r.totalCount,r.flag,r.creationTime) from ReturnStatus r where  r.taxpayerGstin=:gstn and r.monthYear=:monthYear and r.returnType=:returnType order by r.updationTime desc"),
	@NamedQuery(name="ReturnStatus.findByTransitId",query="SELECT r from ReturnStatus r where r.transitId=:transitId and r.returnType=:returnType and r.monthYear=:monthYear and r.status=:status and r.taxpayerGstin.gstin=:gstin"),
	@NamedQuery(name="ReturnStatus.findByGstnReturnTypeMonthYear", query="SELECT r FROM ReturnStatus r WHERE r.taxpayerGstin.gstin=:gstin "
		+ "and r.monthYear=:monthYear and r.returnType=:returnType order by r.creationTime desc"),
})
public class ReturnStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
    
	@JsonIgnore
	private String createdBy;
    
	@JsonIgnore 
	private String creationIpAddress;
    
    @JsonIgnore 
	private Timestamp creationTime;

	private String event;

	private String monthYear;

	private String referenceId;

	private String returnType;

	private String status;
	@JsonIgnore
	private String transactionId;
	
	private String transitId;
	
	private String transaction;
	
	@JsonIgnore
	private String response;
	
	@JsonIgnore
	private String updatedBy;
	
	@JsonIgnore
	private String updationIpAddress;
    
	@JsonIgnore
	private Timestamp updationTime;
    
	private String flag;
	
	private int totalSync;
	
	private int totalNotSync;
	
	private int totalCount;
	
	@JsonProperty("syncDate")
	@Transient private String syncDate;
	
	@JsonProperty("syncTime")
	@Transient private String syncTime;
	
	//bi-directional many-to-one association to TaxpayerGstin
	@JsonIgnore 
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin taxpayerGstin;
	
	private String gsp;
	
	@OneToMany(mappedBy = "returnStatus",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<GstnInvsCountMetadata> gstnInvsCountMetadatas= new ArrayList<>();
	
	public ReturnStatus(){
	}
		
	public ReturnStatus(int id,String event,String monthYear,String referenceId,String returnType,String status,String transitId,String response,
			  String transaction,int totalSync,int totalNotSync,int totalCount,String flag,Date createTime) {
		
		//TimeZone timeZone=TimeZone.getTimeZone("GMT");
		SimpleDateFormat timeFmt=new SimpleDateFormat("hh:mm a");
		//timeFmt.setTimeZone(timeZone);
		
		this.id=id;
		this.event=event;
		this.monthYear=monthYear;
		this.referenceId=referenceId;
		this.returnType=returnType;
		this.status=status;
		this.transitId=transitId;
		this.response=response;
		this.transaction=transaction;
		this.totalSync=totalSync;
		this.totalNotSync=totalNotSync;
		this.totalCount=totalCount;
		this.flag=flag;
		this.syncDate=new SimpleDateFormat("dd MMM yyyy").format(createTime);
		this.syncTime=timeFmt.format(createTime);
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return this.taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getEvent() {
		return this.event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getTransitId() {
		return transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public int getTotalSync() {
		return totalSync;
	}

	public void setTotalSync(int totalSync) {
		this.totalSync = totalSync;
	}

	public int getTotalNotSync() {
		return totalNotSync;
	}

	public void setTotalNotSync(int totalNotSync) {
		this.totalNotSync = totalNotSync;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getSyncDate() {
		return syncDate;
	}

	public void setSyncDate(String syncDate) {
		this.syncDate = syncDate;
	}

	public String getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(String syncTime) {
		this.syncTime = syncTime;
	}

	public String getGsp() {
		return gsp;
	}

	public void setGsp(String gsp) {
		this.gsp = gsp;
	}

	public List<GstnInvsCountMetadata> getGstnInvsCountMetadatas() {
		return gstnInvsCountMetadatas;
	}

	public void setGstnInvsCountMetadatas(List<GstnInvsCountMetadata> gstnInvsCountMetadatas) {
		this.gstnInvsCountMetadatas = gstnInvsCountMetadatas;
	}
	
}

