package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the sku database table.
 * 
 */
@Entity
@NamedQuery(name="Sku.findAll", query="SELECT s FROM Sku s")
public class Sku implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Byte active;
	
	private Byte isDefault;

	private String code;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	@Lob
	private String description;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	//bi-directional many-to-one association to SkuDetail
	@OneToMany(mappedBy="sku")
	private List<SkuDetail> skuDetails;

	//bi-directional many-to-one association to SkuPartner
	@OneToMany(mappedBy="sku")
	@JsonIgnore
	private List<SkuPartner> skuPartners;

	public Sku() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Byte getActive() {
		return this.active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Byte getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Byte isDefault) {
		this.isDefault = isDefault;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public List<SkuDetail> getSkuDetails() {
		return this.skuDetails;
	}

	public void setSkuDetails(List<SkuDetail> skuDetails) {
		this.skuDetails = skuDetails;
	}

	public SkuDetail addSkuDetail(SkuDetail skuDetail) {
		getSkuDetails().add(skuDetail);
		skuDetail.setSku(this);

		return skuDetail;
	}

	public SkuDetail removeSkuDetail(SkuDetail skuDetail) {
		getSkuDetails().remove(skuDetail);
		skuDetail.setSku(null);

		return skuDetail;
	}

	public List<SkuPartner> getSkuPartners() {
		return this.skuPartners;
	}

	public void setSkuPartners(List<SkuPartner> skuPartners) {
		this.skuPartners = skuPartners;
	}

	public SkuPartner addSkuPartner(SkuPartner skuPartner) {
		getSkuPartners().add(skuPartner);
		skuPartner.setSku(this);

		return skuPartner;
	}

	public SkuPartner removeSkuPartner(SkuPartner skuPartner) {
		getSkuPartners().remove(skuPartner);
		skuPartner.setSku(null);

		return skuPartner;
	}

}