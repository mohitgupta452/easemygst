package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.CDNDetailSerializer;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;


/**
 * The persistent class for the cdn_details database table.
 * 
 */
@Entity
@Table(name="cdn_details")
@NamedQueries({
@NamedQuery(name="CdnDetail.findAll", query="SELECT c FROM CDNDetailEntity c"),
@NamedQuery(name = "CdnDetailEntity.findByPagination", query = "SELECT b FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear"),
@NamedQuery(name = "CdnDetailEntity.findCountByMonthYear", query = "SELECT count(b.id) FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and (b.dataSource=:dataSource or (b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN and b.type <> 'NEW')) and b.flags <> 'DELETE'"),
@NamedQuery(name = "CdnDetailEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.isError=true" ),

@NamedQuery(name="CDNDetailEntity.removeById",query="delete from CDNDetailEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false and b.dataSource=:dataSource"),
@NamedQuery(name="CDNDetailEntity.deleteByMonthYear",query="delete from CDNDetailEntity b where b.cdnTransaction in (select cdn from CDNTransactionEntity cdn where cdn.monthYear =:monthYear and cdn.gstin.gstin =:gstin and cdn.returnType =:returnType)and b.isSynced=false and b.isTransit=false  and b.dataSource=:dataSource"),
@NamedQuery(name="CDNDetailEntity.updateFlagByMonthYear",query="update  CDNDetailEntity b set b.flags='DELETE', b.toBeSync=true where b.cdnTransaction in (select cdn from CDNTransactionEntity cdn where cdn.monthYear =:monthYear and cdn.gstin.gstin =:gstin and cdn.returnType =:returnType) and b.isSynced=true and b.dataSource=:dataSource and b.isTransit=false"),
@NamedQuery(name="CDNDetailEntity.updateFlagById",query="update CDNDetailEntity b set b.flags='DELETE', b.toBeSync=true  where b.id in :ids and b.isSynced=true and b.dataSource=:dataSource and b.isTransit=false"),
@NamedQuery(name="CdnDetailEntity.updateFlagsValue",query="update CDNDetailEntity b set b.flags='' where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN "),
@NamedQuery(name="CdnDetailEntity.findGstnInvoices",query="SELECT b from CDNDetailEntity b  where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN "),

@NamedQuery(name = "CdnDetailEntity.findCountByMonthYearForSummary", query = "SELECT count(b.id) FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear  and b.dataSource=:dataSource  and (b.flags <> 'DELETE' or b.flags is null )" ),


@NamedQuery(name = "CdnDetailEntity.findSummaryByMonthYear", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or  b.flags is null ) and  b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.noteType='D')"),

@NamedQuery(name = "CdnDetailEntity.findSummaryByctin", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM CDNDetailEntity b where b.cdnTransaction.gstin.gstin=:gstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or  b.flags is null ) and  b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN and b.noteType=:notetype"  ),

@NamedQuery(name = "CdnDetailEntity.findSummaryByMonthYearCredRefund", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or  b.flags is null ) and  b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.noteType='C' or b.noteType='R')"),

@NamedQuery(name="CdnDetailEntity.findNotSynced",query="select b from CDNDetailEntity b where b.toBeSync=true and b.cdnTransaction.gstin=:gstn and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear in (:monthYear) and (b.dataSource=:dataSource or b.flags='pending' or b.flags='rejected') and b.isTransit=false and b.isError=false"),
@NamedQuery(name = "CdnDetailEntity.findMismatchedInvoice", query = "SELECT b FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.invoiceNumber=:invoiceNumber and b.dataSource=:dataSource and b.cdnTransaction.originalCtin=:ctin"),
@NamedQuery(name = "CdnDetailEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear  and (b.flags<> 'DELETE' or b.flags is null) and b.cdnTransaction.fillingStatus='Y' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) group by b.type"),
@NamedQuery(name = "CdnDetailEntity.findItcBulk", query = "SELECT b.cdnTransaction.originalCtin, b.revisedInvNo,b.revisedInvDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.cdnTransaction.originalCtinName FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),
@NamedQuery(name = "CdnDetailEntity.findItcBulkCount", query = "SELECT count(i.id) FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),

@NamedQuery(name = "CdnDetailEntity.findBlankHsnBulk", query = "SELECT b.cdnTransaction.originalCtin, b.revisedInvNo,b.revisedInvDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.cdnTransaction.originalCtinName,i.unit,i.quantity FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),
@NamedQuery(name = "CdnDetailEntity.countBlankHsnBulk", query = "SELECT count(i.id) FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),


@NamedQuery(name = "CdnDetailEntity.findMissingInvoices", query = "SELECT b from CDNDetailEntity  b   where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.dataSource=:dataSource and b.invoiceNumber not in (SELECT b1.invoiceNumber from CDNDetailEntity b1   where b1.cdnTransaction.gstin=:taxPayerGstin and b1.cdnTransaction.returnType=:returnType and b1.cdnTransaction.monthYear=:monthYear and b1.dataSource=:dataSource1)" ),
@NamedQuery(name = "CdnDetailEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
@NamedQuery(name = "CDNDetailEntity.findErrorInvoice",query = "Select b from CDNDetailEntity  b where b.cdnTransaction.gstin=:gstn and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear in (:monthYear) and b.revisedInvNo=:invno and b.cdnTransaction.originalCtin=:ctin"),
@NamedQuery(name= "CDNDetailEntity.findErrorInvoicesByTransitId",query="SELECT b from CDNDetailEntity b where b.cdnTransaction.gstin=:gstn and b.cdnTransaction.monthYear in (:monthYear) and b.cdnTransaction.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
@NamedQuery(name="CDNDetailEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from CDNDetailEntity b where  b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.gstin=:gstin and b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
@NamedQuery(name = "CdnDetailEntity.findItcSummary", query = "SELECT i.totalEligibleTax, sum(i.itcIgst)+sum(i.itcCgst)+sum(i.itcSgst)+sum(i.itcCess),sum(i.igst)+sum(i.cgst)+sum(i.sgst)+sum(i.cess) FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null) group by i.totalEligibleTax"),
@NamedQuery(name = "CDNDetailEntity.findErrorInvoiceGstr2",query = "Select b from CDNDetailEntity  b where b.cdnTransaction.gstin=:gstn and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.invoiceNumber=:invno and b.transitId=:transitId and b.cdnTransaction.originalCtin=:ctin"),
@NamedQuery(name = "CdnDetailEntity.findErrorDataByMonthYear", query = "SELECT b FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear  and b.isError=true and  b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <> 'DELETE' or b.flags is null ) " ),
@NamedQuery(name = "CdnDetailEntity.findMismatchedInvNoInvoice", query = "SELECT b FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.dataSource=:dataSource and b.type=:type and b.cdnTransaction.originalCtin=:ctin"),


@NamedQuery(name="CdnDetailEntity.countNoSimilarData",query="SELECT count(b.id) from CDNDetailEntity b WHERE b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) "),
@NamedQuery(name="CdnDetailEntity.countSimilarData",query="SELECT count(b.id) from CDNDetailEntity b WHERE b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and  b.subType='MISSING_INWARD_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )"),
@NamedQuery(name = "CdnDetailEntity.countSimmNoSimSummary", query = "SELECT b.subType,COUNT(b.id)  FROM CDNDetailEntity b where b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags<> 'DELETE' or b.flags is null)   and b.cdnTransaction.fillingStatus='Y' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) group by b.subType"),



@NamedQuery(name="CdnDetailEntity.findNoSimilarData",query="SELECT b from CDNDetailEntity b WHERE b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR'"),
@NamedQuery(name="CdnDetailEntity.findSimilarData",query="SELECT b from CDNDetailEntity b WHERE b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_SIMMILAR'"),


@NamedQuery(name="CdnDetailEntity.findInvoiceByCtin",query="SELECT bt from CDNDetailEntity bt WHERE bt.cdnTransaction.gstin=:taxPayerGstin and bt.cdnTransaction.returnType=:returnType and bt.cdnTransaction.monthYear=:monthYear and bt.cdnTransaction.originalCtin=:ctin and bt.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and bt.type='MISSING_OUTWARD' and (bt.flags <> 'DELETE' or bt.flags is null ) and abs(bt.taxAmount-:taxAmount)<=:diff "),
@NamedQuery(name="CdnDetailEntity.findInvoiceByCtinCount",query="SELECT count(bt.id) from CDNDetailEntity bt WHERE bt.cdnTransaction.gstin=:taxPayerGstin and bt.cdnTransaction.returnType=:returnType and bt.cdnTransaction.monthYear=:monthYear and bt.cdnTransaction.originalCtin=:ctin and bt.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and bt.type='MISSING_OUTWARD' and (bt.flags <> 'DELETE' or bt.flags is null ) and abs(bt.taxAmount-:taxAmount)<=:diff "),

//

@NamedQuery(name="CdnDetailEntity.countNoSimilarDataWithFilter",query="SELECT count(b.id) from CDNDetailEntity b WHERE b.cdnTransaction.originalCtin  like :ctin and  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' "),
@NamedQuery(name="CdnDetailEntity.countSimilarDataWithFilter",query="SELECT count(b.id) from CDNDetailEntity b WHERE b.cdnTransaction.originalCtin like :ctin and  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_SIMMILAR'"),

@NamedQuery(name="CdnDetailEntity.findNoSimilarDataWithFilter",query="SELECT b from CDNDetailEntity b WHERE b.cdnTransaction.originalCtin like :ctin and b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR'"),
@NamedQuery(name="CdnDetailEntity.findSimilarDataWithFilter",query="SELECT b from CDNDetailEntity b WHERE b.cdnTransaction.originalCtin like :ctin and b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_SIMMILAR' "),


//

@NamedQuery(name = "CdnDetailEntity.findItcBulkWithFilter", query = "SELECT b.cdnTransaction.originalCtin, b.revisedInvNo,b.revisedInvDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.cdnTransaction.originalCtinName FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.originalCtin like :ctin and b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),
@NamedQuery(name = "CdnDetailEntity.findItcBulkCountWithFilter", query = "SELECT count(i.id) FROM CDNDetailEntity b JOIN b.items i where b.cdnTransaction.originalCtin like :ctin and b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)" ),

@NamedQuery(name="CdnDetailEntity.updateErrorFlagBycdnTransaction",query="update CDNDetailEntity b set b.isError=false,b.errMsg='',b.flags='' where  b.cdnTransaction=:cdnTransaction and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and b.isTransit=false and b.isSynced=false"),

@NamedQuery(name="CdnDetailEntity.findIdsNoSimilarDataWithFilter",query="SELECT b.id from CDNDetailEntity b WHERE b.cdnTransaction.originalCtin like :ctin and b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )"),
@NamedQuery(name="CdnDetailEntity.findIdsNoSimilarDataWithOutFilter",query="SELECT b.id from CDNDetailEntity b WHERE b.cdnTransaction.originalCtin like :ctin and b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )"),

@NamedQuery(name="CDNDetailEntity.findAutoMatch",query="select b.id from CDNDetailEntity b where b.cdnTransaction.gstin=:taxpayerGstin and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.returnType=:returnType and b.type='MATCH'  and b.toBeSync=true"),

//reject all query
@NamedQuery(name="CdnDetailEntity.rejectAllMissingInwardWithoutFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
@NamedQuery(name="CdnDetailEntity.pendingAllMissingInwardWithoutFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
@NamedQuery(name="CdnDetailEntity.rejectAllMissingInwardWithFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin  and b.cdnTransaction.originalCtin=:ctin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
@NamedQuery(name="CdnDetailEntity.pendingAllMissingInwardWithFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.originalCtin=:ctin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
//reject all/pending all mismatches query
@NamedQuery(name="CdnDetailEntity.rejectAllMismatchWithoutFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null ) and  (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST "),
@NamedQuery(name="CdnDetailEntity.pendingAllMismatchWithoutFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type=:reconcileType and   (b.flags <> 'DELETE' or b.flags is null ) and  (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST "),
@NamedQuery(name="CdnDetailEntity.rejectAllMismatchWithFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin  and b.cdnTransaction.originalCtin=:ctin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type=:reconcileType and   (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST "),
@NamedQuery(name="CdnDetailEntity.pendingAllMismatchWithFilter",query="update CDNDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.originalCtin=:ctin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.type=:reconcileType and   (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST  "),
//find mismatch invoices by mismatch type
@NamedQuery(name="CdnDetailEntity.findAllMismatchInvoiceByTypeWithoutFilter",query="select b from  CDNDetailEntity b  WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.cdnTransaction.originalCtin=:ctin  and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST  "),
@NamedQuery(name="CdnDetailEntity.findAllMismatchInvoiceByTypeWithFilter",query="select b from  CDNDetailEntity b  WHERE  b.cdnTransaction.gstin=:taxPayerGstin and b.cdnTransaction.returnType=:returnType and b.cdnTransaction.monthYear=:monthYear and b.cdnTransaction.fillingStatus='Y' and b.cdnTransaction.originalCtin=:ctin   and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST  "),


})
@JsonSerialize(using=CDNDetailSerializer.class)
public class CDNDetailEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cdnTransactionId")
	//@Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED)
	private CDNTransactionEntity cdnTransaction;


    //@Audited 
	private String invoiceNumber;

  //  @Audited 
	private String flags;

	private boolean isAmmendment;

	private boolean isLocked;

	private  boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;
	
	@Transient
	private TransactionType transactionType;

	@Column(name="syncId")
	//@Audited 
	private String synchId;

	private String transitId;

	private String type="NEW";
	
	private String subType="NEW";

	@Transient
	private String originalCtin;
	
	@Transient
	private String originalCtinName;
	
	@Transient
	private String octin;
	
	@Transient
	private String octinName;
	
	//@Audited
	private double taxableValue;
	
	//@Audited
	private double taxAmount;
	
	//@Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	private List<Item> items;
	
	//@Audited
	@Temporal(TemporalType.DATE)
	private Date invoiceDate;
	
	//@Audited
	private String revisedInvNo;
	
	//@Audited
	private String noteType;
	
	//@Audited
	@Temporal(TemporalType.DATE)
	private Date revisedInvDate;
	
	//@Audited
	private String reasonForNote;
	
	//@Audited
	private String preGstRegime;
	
	//@Audited
	private String invoiceType;
	
	//@Audited
	@Enumerated(EnumType.STRING)
	private DataSource dataSource=DataSource.EMGST;
	
	private Boolean toBeSync;
	
	private String cInvoiceNumber;
	
	private Date cInvoiceDate;
	
	private double cTaxAmount;
	
	private double cTaxableValue;
	
	private JsonNode previousInvoice;
	
	private transient String error_msg;
	
	private transient String error_cd;
	
	//@Audited
	private String errMsg;
	
	//@Audited
	private Boolean isError=false;
	
	private String checksum;
	
	private String previousFlags;
	
	private String previousType;
	
	private String originalNoteNumber ;
	
	private Date originalNoteDate;
	
	public String getcInvoiceNumber() {
		return cInvoiceNumber;
	}

	public void setcInvoiceNumber(String cInvoiceNumber) {
		this.cInvoiceNumber = cInvoiceNumber;
	}

	public Date getcInvoiceDate() {
		return cInvoiceDate;
	}

	public void setcInvoiceDate(Date cInvoiceDate) {
		this.cInvoiceDate = cInvoiceDate;
	}

	public double getcTaxAmount() {
		return cTaxAmount;
	}

	public void setcTaxAmount(double cTaxAmount) {
		this.cTaxAmount = cTaxAmount;
	}
	
	public double getcTaxableValue() {
		return cTaxableValue;
	}

	public void setcTaxableValue(double cTaxableValue) {
		this.cTaxableValue = cTaxableValue;
	}

	public JsonNode getPreviousInvoice() {
		return previousInvoice;
	}

	public void setPreviousInvoice(JsonNode previousInvoice) {
		this.previousInvoice = previousInvoice;
	}

	
	public List<Item> getItems() {
		return items;
	}


	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getInvoiceType() {
		return invoiceType;
	}


	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}


	public String getRevisedInvNo() {
		return revisedInvNo;
	}

	public void setRevisedInvNo(String revisedInvNo) {
		this.revisedInvNo = revisedInvNo;
	}


	public String getOriginalNoteNumber() {
		return originalNoteNumber;
	}

	public void setOriginalNoteNumber(String originalNoteNumber) {
		this.originalNoteNumber = originalNoteNumber;
	}

	public Date getOriginalNoteDate() {
		return originalNoteDate;
	}

	public void setOriginalNoteDate(Date originalNoteDate) {
		this.originalNoteDate = originalNoteDate;
	}

	public Date getRevisedInvDate() {
		return revisedInvDate;
	}


	public void setRevisedInvDate(Date revisedInvDate) {
		this.revisedInvDate = revisedInvDate;
	}


	public Date getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public CDNDetailEntity() {
	}


	public CDNTransactionEntity getCdnTransaction() {
		return cdnTransaction;
	}

	public void setCdnTransaction(CDNTransactionEntity cdnTransaction) {
		this.cdnTransaction = cdnTransaction;
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getOriginalCtin() {
		if(!Objects.isNull(cdnTransaction))
			originalCtin=cdnTransaction.getOriginalCtin();
		return originalCtin;
	}

	public void setOriginalCtin(String originalCtin) {
		this.originalCtin = originalCtin;
	}


	public String getOriginalCtinName() {
		if(!Objects.isNull(cdnTransaction))
			originalCtinName=cdnTransaction.getOriginalCtinName();
		return originalCtinName;
	}

	public void setOriginalCtinName(String originalCtinName) {
		this.originalCtinName = originalCtinName;
	}
	
	public String getOctin() {
		/*if(!Objects.isNull(b2bTransaction))
			octin=b2bTransaction.getTaxpayerGstin().getGstin();*/
		return octin;
	}

	public void setOctin(String octin) {
		
		this.octin = octin;
	}

	public String getOctinName() {
		/*if(!Objects.isNull(b2bTransaction))
			octinName=b2bTransaction.getTaxpayerGstin().getDisplayName();*/
		return octinName;
	}

	public void setOctinName(String octinName) {
		this.octinName = octinName;
	}

	public double getTaxableValue() {
		return taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return taxAmount;
	}




	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getNoteType() {
		return noteType;
	}


	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}

	public String getReasonForNote() {
		return reasonForNote;
	}


	public void setReasonForNote(String reasonForNote) {
		this.reasonForNote = reasonForNote;
	}


	public String getPreGstRegime() {
		return preGstRegime;
	}


	public void setPreGstRegime(String preGstRegime) {
		this.preGstRegime = preGstRegime;
	}
	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(this.cdnTransaction,this.getId(),this.getInvoiceNumber());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder()
				.append(this.getCdnTransaction(), ((CDNDetailEntity) obj).getCdnTransaction())
				.append(this.getInvoiceNumber(), ((CDNDetailEntity) obj).getInvoiceNumber())
				.append(this.getRevisedInvNo(), ((CDNDetailEntity) obj).getRevisedInvNo())
				;
		return builder.isEquals();
	}

	public CDNDetailEntity(CDNTransactionEntity cdnTransaction, String id, String invoiceNumber, Date invoiceDate,
			String revisedInvNo, Date revisedInvDate, String noteType, double taxableValue, double taxAmount,
			String flags, String type, String source, boolean isMarked, boolean isSynced, boolean toBeSync,
			boolean isTransit, boolean isError, Date cInvoiceDate, String cInvoiceNumber, double cTaxAmount,String previousType,
			boolean isAmmendment,String originalNoteNumber,Date originalNoteDate
			) {

		this.setId(id);;
		this.invoiceNumber=invoiceNumber;
		this.cdnTransaction=cdnTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.source=source;
		this.isMarked=isMarked;
		this.revisedInvNo=revisedInvNo;
		this.revisedInvDate=revisedInvDate;
		this.noteType=noteType;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
		this.cInvoiceDate=cInvoiceDate;
		this.cInvoiceNumber=cInvoiceNumber;
		this.cTaxAmount=cTaxAmount;
		this.previousType=previousType;
		this.isAmmendment=isAmmendment;
		this.originalNoteNumber=originalNoteNumber;
		this.originalNoteDate=originalNoteDate;
		
		
		
	}
	
	//constructor for save to gstn
	
	public CDNDetailEntity(CDNTransactionEntity cdnTransaction, String id, String invoiceNumber, Date invoiceDate,
			String revisedInvNo, Date revisedInvDate, String noteType, double taxableValue, double taxAmount,
			String flags, String type,String reasonForNote,String preGstRegime,Item item,boolean isAmendment) {

		this.setId(id);;
		this.invoiceNumber=invoiceNumber;
		this.cdnTransaction=cdnTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.revisedInvNo=revisedInvNo;
		this.revisedInvDate=revisedInvDate;
		this.noteType=noteType;
		this.reasonForNote=reasonForNote;
		this.preGstRegime=preGstRegime;
		this.isAmmendment=isAmendment;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}
		
	}
	
	//constructor for save to gstn cdna
	
	public CDNDetailEntity(CDNTransactionEntity cdnTransaction, String id, String invoiceNumber, Date invoiceDate,
			String revisedInvNo, Date revisedInvDate, String noteType, double taxableValue, double taxAmount,
			String flags, String type,String reasonForNote,String preGstRegime,Item item,boolean isAmnedment,Date originalNoteDate,
			String originalNumber) {

		this.setId(id);;
		this.invoiceNumber=invoiceNumber;
		this.cdnTransaction=cdnTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.revisedInvNo=revisedInvNo;
		this.revisedInvDate=revisedInvDate;
		this.noteType=noteType;
		this.reasonForNote=reasonForNote;
		this.preGstRegime=preGstRegime;
		this.isAmmendment=isAmnedment;
		this.originalNoteDate=originalNoteDate;
		this.originalNoteNumber=originalNumber;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}
		
	}

	public CDNDetailEntity(String id,Date invoiceDate,String invocieNumber,double taxAmount,double taxableValue)
	{
		this.setId(id);
		this.setInvoiceDate(invoiceDate);
		this.invoiceNumber=invocieNumber;
		this.taxAmount=taxAmount;
		this.taxableValue=taxableValue;
	}


	public CDNDetailEntity(CDNDetailEntity cdn) {
		super();
		this.cdnTransaction = cdn.getCdnTransaction();
		this.invoiceNumber = cdn.getInvoiceNumber();
		this.flags = cdn.getFlags();
		this.isAmmendment = cdn.getIsAmmendment();
		this.isLocked = cdn.getIsLocked();
		this.isMarked = cdn.getIsMarked();
		this.isSynced = cdn.getIsSynced();
		this.toBeSync = cdn.toBeSync;
		this.isTransit = cdn.getIsTransit();
		this.isValid = cdn.getIsValid();
		this.source = cdn.getSource();
		this.sourceId = cdn.getSourceId();
		this.synchId = cdn.getSynchId();
		this.transitId = cdn.getTransitId();
		this.type = cdn.getType();
		this.originalCtin = cdn.getOriginalCtin();
		this.originalCtinName = cdn.getOriginalCtinName();
		this.taxableValue = cdn.getTaxableValue();
		this.taxAmount = cdn.getTaxAmount();
		List<Item>items=new ArrayList<>();
		for(Item item:cdn.getItems()){
			Item itm=new Item(item);
			//itm.setId(UUID.randomUUID().toString());
			items.add(itm);
		}
		this.items = items;
		this.revisedInvNo = cdn.getRevisedInvNo();
		this.noteType = cdn.getNoteType();
		this.revisedInvDate = cdn.getRevisedInvDate();
		this.reasonForNote = cdn.getReasonForNote();
		this.preGstRegime = cdn.getPreGstRegime();
		this.invoiceType = cdn.getInvoiceType();
		this.invoiceDate =cdn.getInvoiceDate();
		this.dataSource = cdn.getDataSource();
		this.checksum=cdn.getChecksum();
		this.subType=cdn.getSubType();
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	public Boolean getToBeSync() {
		return toBeSync;
	}


	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	
	public String getPreviousFlags() {
		return previousFlags;
	}



	public void setPreviousFlags(String previousFlags) {
		this.previousFlags = previousFlags;
	}



	public String getPreviousType() {
		return previousType;
	}



	public void setPreviousType(String previousType) {
		this.previousType = previousType;
	}

	
	public TransactionType getTransactionType() {
		return TransactionType.CDN;
	}

}