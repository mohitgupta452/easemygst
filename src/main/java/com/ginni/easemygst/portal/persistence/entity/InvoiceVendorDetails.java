package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the invoice_vendor_detais database table.
 * 
 */
@Entity
@Table(name="invoice_vendor_details")
@NamedQueries({ @NamedQuery(name="InvoiceVendorDetails.findAll", query="SELECT i FROM InvoiceVendorDetails i"),
	@NamedQuery(name="InvoiceVendorDetails.findByTaxpayerId", query="SELECT i FROM InvoiceVendorDetails i WHERE i.taxpayerGstinId.id=:id and i.active=1"),
	@NamedQuery(name="InvoiceVendorDetails.findByTaxpayerAndId", query="SELECT i FROM InvoiceVendorDetails i WHERE i.taxpayerGstinId.id=:taxpayerGstinId and i.id=:id and i.active=1"),
	@NamedQuery(name="InvoiceVendorDetails.findInvoiceByNumber", query="SELECT i FROM InvoiceVendorDetails i WHERE i.taxpayerGstinId.id=:gstin and i.invoice.invoiceNumber=:invoiceNo")})
public class InvoiceVendorDetails  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String billingAddress;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	@OneToOne
	@JoinColumn(name="taxpayerGstinId")
	@JsonIgnore
	private TaxpayerGstin taxpayerGstinId;

	@OneToOne
	@JoinColumn(name="invoiceId")
	private InvoiceDetail invoice;

	private String shippingAddress;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private String updationIpAddress;

	@JsonIgnore
	private Timestamp updationTime;

	@ManyToOne
	@JoinColumn(name="vendorId")
	private Vendor vendor;

	private String vendorName;
	
	private Byte active;
	
	private String status;
	
	public InvoiceVendorDetails() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBillingAddress() {
		return this.billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getShippingAddress() {
		return this.shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}
	
	
	public TaxpayerGstin getTaxpayerGstinId() {
		return taxpayerGstinId;
	}

	public void setTaxpayerGstinId(TaxpayerGstin taxpayerGstinId) {
		this.taxpayerGstinId = taxpayerGstinId;
	}

	public InvoiceDetail getInvoice() {
		return invoice;
	}

	public void setInvoice(InvoiceDetail invoice) {
		this.invoice = invoice;
	}

	public Timestamp getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}