package com.ginni.easemygst.portal.persistence.procedure;

public class TransactionProcedures {
	
	private final static String b2bInsertUpdateProcedure=
			"create procedure insertUpdate(in id1 varchar(50),in invoiceNumber1 varchar(50), in financialYear1 varchar(10),in etin1 varchar(20),in taxAmount1 double(20,5),\n" + 
			" in taxableValue1 double(20,5),in creationTime1 timestamp,in createdBy1 varchar(100),in creationIpAddress1 varchar(20),in updatedBy1 varchar(100),in updationIpAddress1 varchar(20), \n" + 
			" in updationTime1 timestamp,in b2bTransactionId1 varchar(50),in isAmmendment1 bit(1),in isValid1 bit(1), in isTransit1 bit, in isMarked1 bit(1), in isSynced1 bit(1), in isLocked1 bit(1), \n" + 
			" in synchId1 varchar(20), in transitId1 varchar(10),in source1 varchar(20), in sourceId1 varchar(20),in flags1 varchar(20), "
			+ "in type1 varchar(20), in reverseCharge1 bit(1),in newInvoiceDate Date,in newInvoiceType varchar(20),in newPos varchar(30),in operation varchar(10),in newDatasource varchar(20),"
			+ "in newChecksum varchar(70),"
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalInvoiceNumber varchar(70),"
			+ "in newOriginalInvoiceDate date"
			+ ")\n" + 
			"BEGIN\n" 
			+ "if operation = 'UPDATE' then\n"+
			"update  b2b_details   set `taxAmount`=taxAmount1,`toBeSync`=b'1',`taxableValue`=taxablevalue1,`etin`=etin1,\n" + 
			" `updatedBy`=updatedBy1, `updationIpAddress`=updationIpAddress1,`updationTime`=updationTime1,"
			+ "`source`=source1, `sourceId`=sourceId1,`reverseCharge`=reverseCharge1,`invoiceDate`=newInvoiceDate,"
			+ "`invoiceType`=newInvoiceType,`pos`=newPos,`dataSource`=newDatasource,isError=b'0',errMsg='',flags='',checksum=newChecksum ,isAmmendment=newIsAmmendment,originalInvoiceNumber=newOriginalInvoiceNumber,originalInvoiceDate=newOriginalInvoiceDate"
			+ " where "
			+ " id=id1 and isLocked!=b'1';\n" + 
			"else\n" + 
			"INSERT INTO `b2b_details` (`id`, `invoiceNumber`, `financialYear`, `etin`, `taxAmount`,\n" + 
			" `taxableValue`, `creationTime`, `createdBy`, `creationIpAddress`,`b2bTransactionId`,`source`, `sourceId`,`reverseCharge`,"
			+ "`invoiceDate`,`invoiceType`,`pos`,`type`,`dataSource`,`toBeSync`,`checksum`,isAmmendment,originalInvoiceNumber,originalInvoiceDate )\n" + 
			" VALUES (id1, invoiceNumber1, financialYear1, etin1, taxAmount1,\n" + 
			" taxableValue1, creationTime1, createdBy1, creationIpAddress1,b2bTransactionId1,source1, sourceId1,"
			+ "reverseCharge1,newInvoiceDate,newInvoiceType,newPos,'NEW',newDatasource,b'1',newChecksum,newIsAmmendment,newOriginalInvoiceNumber,newOriginalInvoiceDate);\n" + 
			"END IF;\n" + 
			" END";
	
	private final static String dropB2bProcedure="drop procedure if exists insertUpdate";
	
	private final static String _DELETEALLB2B="create procedure deleteAllB2B(in )";
	
	
	private final static String _CDNProcedure=
			"create procedure cdnInsertUpdate(in newId varchar(50) ,\n" + 
			"  in newinvoiceNumber varchar(40),\n" + 
			"  in newNoteType varchar(20),\n" + 
			"  in newCreationTime timestamp ,\n" + 
			"  in newCreatedBy varchar(100) ,\n" + 
			"  in newCreationIpAddress varchar(20) ,\n" + 
			"  in newUpdatedBy varchar(100) ,\n" + 
			"  in newUpdationIpAddress varchar(20) ,\n" + 
			"  in newUpdationTime timestamp ,\n" + 
			"  in newCdnTransactionId varchar(50) ,\n" + 
			"  in newSource varchar(45) ,\n" + 
			"  in newTaxAmount Double(20,5), \n"+
			"  in newTaxableValue Double(20,5),"+
			"  in newPreGstRegime varchar(16),\n" + 
			"  in newReasonForNote varchar(250),\n" 
			+ "in newRevisedInvNo varchar(30),"
			+ "in newRevisedInvDate date,"
			+ "in newSourceId varchar(36),"
			+ "in newInvoiceDate date,"
			+ "in newInvoiceType varchar(20),"
			+ "in operation varchar(10),"
			+ "in newDataSource varchar(10),"
			+ "in newChecksum varchar(70),"
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalNoteNumber varchar(70),"
			+ "in newOriginalNoteDate date"
			+")BEGIN\n" 
			+ "if operation = 'UPDATE' then\n"+
			"update  cdn_details set `taxAmount`=newTaxAmount, `taxableValue`=newTaxableValue, "
		+ "`updationTime`=newUpdationTime, `updatedBy`=newUpdatedBy, `updationIpAddress`=newUpdationIpAddress,`cdnTransactionId`=newCdnTransactionId"
		+ ",`source`=newSource, `sourceId`=newSourceId,`invoiceDate`=newInvoiceDate, `revisedInvNo`=newRevisedInvNo,`noteType`=newNoteType,"
		+ " `revisedInvDate`=newRevisedInvDate,`preGstRegime`=newPreGstRegime,`reasonForNote`=newReasonForNote,`invoiceType`=newInvoiceType,`toBeSync`=b'1'"+  
			",isError=b'0',errMsg='',flags='',checksum=newChecksum,isAmmendment=newIsAmmendment,originalNoteNumber=newOriginalNoteNumber,originalNoteDate=newOriginalNoteDate where id=newId and isLocked!=b'1';\n" + 
			"else\n" + 
			"INSERT INTO `cdn_details` (`id`, `invoiceNumber`, `taxAmount`, `taxableValue`, "
			+ "`creationTime`, `createdBy`, `creationIpAddress`, `cdnTransactionId`,`source`, `sourceId`, `invoiceDate`, `revisedInvNo`,"
			+ "`noteType`,`revisedInvDate`,`reasonForNote`,`preGstRegime`,`invoiceType`,type,dataSource,`toBeSync`,checksum,isAmmendment,originalNoteNumber,originalNoteDate) VALUES ("+
			" newId,newInvoiceNumber,newTaxAmount,newTaxableValue,newCreationTime,newCreatedBy,newCreationIpAddress,newCdnTransactionId,\n" + 
			" newSource,newSourceId,newInvoiceDate,newRevisedInvNo,newNoteType,newRevisedInvDate,"
			+ "newReasonForNote,newPreGstRegime,newInvoiceType,'NEW',newDataSource,b'1',newChecksum,newIsAmmendment,newOriginalNoteNumber,newOriginalNoteDate"
			+ ");\n" + 
			"END IF;\n" + 
			" END";
	
	private final static String _DROPCDNPROC="drop procedure if exists cdnInsertUpdate";

	private final static String _CDNURProcedure=
			"create procedure cdnUrInsertUpdate(in newId varchar(50) ,\n" + 
			"  in newinvoiceNumber varchar(40),\n" + 
			"  in newNoteType varchar(20),\n" + 
			"  in newCreationTime timestamp ,\n" + 
			"  in newCreatedBy varchar(100) ,\n" + 
			"  in newCreationIpAddress varchar(20) ,\n" + 
			"  in newUpdatedBy varchar(100) ,\n" + 
			"  in newUpdationIpAddress varchar(20) ,\n" + 
			"  in newUpdationTime timestamp ,\n" + 
			"  in newCdnTransactionId varchar(50) ,\n" + 
			"  in newSource varchar(45) ,\n" + 
			"  in newTaxAmount Double(20,5), \n"+
			"  in newTaxableValue Double(20,5),"+
			"  in newPreGstRegime varchar(16),\n" + 
			"  in newReasonForNote varchar(250),\n" 
			+ "in newRevisedInvNo varchar(30),"
			+ "in newRevisedInvDate date,"
			+ "in newSourceId varchar(36),"
			+ "in newInvoiceDate date,"
			+ "in newInvoiceType varchar(20),"
			+ "in operation varchar(20),"
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalNoteNumber varchar(70),"
			+ "in newOriginalNoteDate date"
			+
			
			
			" )BEGIN\n" 
			+ "if operation = 'UPDATE' then\n"+
			" update `cdnur_details` set `taxAmount`=newTaxAmount, `taxableValue`=newTaxableValue, "
			+ "`updationTime`=newUpdationTime, `updatedBy`=newUpdatedBy, `updationIpAddress`=newUpdationIpAddress, "
			+ "`cdnUrTransactionId`=newCdnTransactionId,`source`=newSource, `sourceId`=newSourceId, `invoiceDate`=newInvoiceDate, `revisedInvNo`=newRevisedInvNo,"
			+ "`noteType`=newNoteType,`revisedInvDate`=newRevisedInvDate,`reasonForNote`=newReasonForNote,`preGstRegime`=newPreGstRegime,"
			+ "`invoiceType`=newInvoiceType,`toBeSync`=b'1',isError=b'0',errMsg='',flags='',isAmmendment=newIsAmmendment,originalNoteNumber=newOriginalNoteNumber,originalNoteDate=newOriginalNoteDate where id=newId;"+
			"else\n" + 
			"INSERT INTO `cdnur_details` (`id`, `invoiceNumber`, `taxAmount`, `taxableValue`, "
			+ "`creationTime`, `createdBy`, `creationIpAddress`, `cdnUrTransactionId`,`source`, `sourceId`, `invoiceDate`, `revisedInvNo`,"
			+ "`noteType`,`revisedInvDate`,`reasonForNote`,`preGstRegime`,`invoiceType`,type,`toBeSync`,isAmmendment,originalNoteNumber,originalNoteDate) VALUES ("+
			" newId,newInvoiceNumber,newTaxAmount,newTaxableValue,newCreationTime,newCreatedBy,newCreationIpAddress,newCdnTransactionId,\n" + 
			" newSource,newSourceId,newInvoiceDate,newRevisedInvNo,newNoteType,newRevisedInvDate,"
			+ "newReasonForNote,newPreGstRegime,newInvoiceType,'NEW',b'1',newIsAmmendment,newOriginalNoteNumber,newOriginalNoteDate"
			+ ");\n" + 
			"END IF;\n" + 
			" END";
	
	private final static String _DROPCDNURPROC="drop procedure if exists cdnUrInsertUpdate";
	
	
	
	private final static String b2clProc="CREATE procedure b2clInsertUpdate (\n" + 
			"  in newId varchar(50) ,\n" + 
			"  in newB2clTransactionId varchar(50),\n" + 
			"  in newSource varchar(45),\n" + 
			"  in newsourceId varchar(100),\n" + 
			"  in newType varchar(100),\n" + 
			"  in newPos varchar(2),\n" + 
			"  in newStateName varchar(30),\n" + 
			"  in newInvoiceNumber varchar(40),\n" + 
			"  in newTaxableValue double,\n" + 
			"  in newTaxAmount double,\n" + 
			"  in newEtin varchar(20),\n" + 
			"  in newCreatedBy varchar(100),\n" + 
			"  in newCreationTime timestamp,\n" + 
			"  in newCreationIpAddress varchar(100),\n" + 
			"  in newUpdatedBy varchar(100),\n" + 
			"  in newUpdationTime timestamp,\n" + 
			"  in newUpdationIpAddress varchar(20),\n"
			+ "in newInvoiceDate date,"
			+ "in operation varchar(10),"
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalInvoiceNumber varchar(70),"
			+ "in newOriginalInvoiceDate date"
			
			+ 
			")\n" + 
			"BEGIN\n" 
			+ "if operation = 'UPDATE' then\n"+
			"update  b2cl_details   set `source`=newSource, `sourceId`=newSourceId,`stateName`=newStateName, `invoiceNumber`=newInvoiceNumber,"
		+ " `taxableValue`=newTaxableValue, `taxAmount`=newTaxAmount,`pos`=newPos," + 
			"`etin`=newEtin,`updatedBy`=newUpdatedBy, `updationTime`=newUpdationTime, `updationIpAddress`=newUpdationIpAddress,`invoiceDate`=newInvoiceDate,`toBeSync`=b'1'"+
			" ,isError=b'0',errMsg='',flags='' ,isAmmendment=newIsAmmendment,originalInvoiceNumber=newOriginalInvoiceNumber,originalInvoiceDate=newOriginalInvoiceDate where id=newId and isLocked!=b'1';\n" + 
			"else\n" + 
			"INSERT INTO `b2cl_details` (`id`, `b2clTransactionId`,`source`, `sourceId`, `stateName`, `invoiceNumber`, `taxableValue`, `taxAmount`,\n" + 
			" `etin`, `createdBy`, `creationTime`, `creationIpAddress`,`invoiceDate`,`pos`,toBeSync,isAmmendment,originalInvoiceNumber,originalInvoiceDate) \n" + 
			" VALUES (newId,newB2clTransactionId,newSource,newSourceId,newStateName,\n" + 
			" newInvoiceNumber,newTaxableValue,newTaxAmount,newEtin,newCreatedBy,newCreationTime,newCreationIpAddress,\n" + 
			"newInvoiceDate,newPos,b'1',newIsAmmendment,newOriginalInvoiceNumber,newOriginalInvoiceDate);\n" + 
			"END IF;\n" + 
			 "END";
	
	private final static String b2clDropProc="drop procedure if exists b2clInsertUpdate;";
	
	private final static String expProc="CREATE procedure expInsertUpdate (\n" + 
			"  in newId varchar(50),\n" + 
			"  in newInvoiceNumber varchar(40),\n" + 
			"  in newShippingBillNo varchar(40),\n" + 
			"  in newCreationTime timestamp,\n" +
			"  in newCreatedBy varchar(100),\n" + 
			"  in newCreationIpAddress varchar(20),\n" + 
			"  in newUpdatedBy varchar(100),\n" + 
			"  in newUpdationIpAddress varchar(20),\n" + 
			"  in newUpdationTime timestamp,\n" + 
			"  in newExpTransactionId varchar(50),\n" + 
			"  in newSource varchar(45),\n" + 
			"  in newSourceId varchar(100),\n" + 
			"  in newType varchar(100) ,\n" + 
			"  in newExportType varchar(10),"
			+ "in newInvoiceDate date,"
			+ "in newShippingBillDate date,"
			+ "in newShippingBillPortCode varchar(40),"
			+ "in  newTaxableValue double(20,5),"
			+ "in newTaxAmount double(20,5),"
			+ "in operation varchar(10),"
			
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalInvoiceNumber varchar(70),"
			+ "in newOriginalInvoiceDate date"
			
			+ ")\n" + 
			"BEGIN\n" + 
			" if operation = 'UPDATE' then\n"+
			" update `exp_details` set `updationTime`=newUpdationTime, `updatedBy`=newUpdatedBy,"
			+ "`updationIpAddress`=newUpdationIpAddress,`expTransactionId`=newExpTransactionId, `source`=newSource, "
			+ "`sourceId`=newSourceId,`exportType`=newExportType, `taxAmount`=newTaxAmount, `taxableValue`=newTaxableValue,"
			+ "`invoiceDate`=newInvoiceDate,`shippingBillDate`=newShippingBillDate,`shippingBillPortCode`=newShippingBillPortCode,"
			+ "`shippingBillNo`=newShippingBillNo,`toBeSync`=b'1'\n" + 
			" ,isError=b'0',errMsg='',flags='',isAmmendment=newIsAmmendment,originalInvoiceNumber=newOriginalInvoiceNumber,originalInvoiceDate=newOriginalInvoiceDate where id=newId and isLocked!=b'1';\n" 
			+"else\n" + 
			"INSERT INTO `exp_details` (`id`, `invoiceNumber`,`creationTime`, `createdBy`, `creationIpAddress`, \n" + 
			"`expTransactionId`, `source`, `sourceId`,`exportType`, `taxAmount`, `taxableValue`,`invoiceDate`,`shippingBillDate`,`shippingBillPortCode`,"
			+ "`shippingBillNo`,toBeSync,isAmmendment,originalInvoiceNumber,originalInvoiceDate)\n" + 
			" VALUES (newId,newInvoiceNumber,newCreationTime,newCreatedBy,newCreationIpAddress,\n" + 
			" newExpTransactionId,newSource,newSourceId,newExportType,newTaxAmount,newTaxableValue,newInvoiceDate,newShippingBillDate,newShippingBillPortCode,"
			+ "newShippingBillNo,b'1',newIsAmmendment,newOriginalInvoiceNumber,newOriginalInvoiceDate);\n" + 
			"END IF;\n" + 
			" END";
	
	private final static String dropExpPROC="drop procedure if exists expInsertUpdate;";  
	
	private final static String b2csProc=
			"CREATE procedure b2csInsertUpdate (\n" + 
			"  in newId varchar(50),\n" + 
			"  in newReturnType varchar(10),\n" + 
			"  in newMonthYear varchar(10) ,\n" + 
			"  in newGstin int(11) ,\n" + 
			"  in newPos varchar(2),\n" + 
			"  in newStateName varchar(30),\n" + 
			"  in newSource varchar(45),\n" + 
			"  in newSourceId varchar(100),\n" + 
			"  in newSupplyType varchar(100),\n" + 
			"  in newEtin varchar(20),\n" + 
			"  in newCreatedBy varchar(100),\n" + 
			"  in newCreationTime timestamp,\n" + 
			"  in newCreationIpAddress varchar(20),\n" + 
			"  in newUpdatedBy varchar(100),\n" + 
			"  in newUpdationTime timestamp,\n" + 
			"  in newUpdationIpAddress varchar(20),"
			+ "in newTaxAmount double(20,5),"
			+ "in newTaxableValue double(20,5),"
			+ "in newB2CSType varchar(20)," 
			+ "in newInvoiceNumber varchar(100),"
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalMonth varchar(10),"
			+ "in newOriginalPos varchar(4)"

			+ "\n" +
			")\n" + 
			"BEGIN\n" + 
			"INSERT INTO `b2cs_transaction` (`id`, `returnType`, `monthYear`, `gstin`, `pos`, `stateName`, `source`, `sourceId`,"
			+ " `supplyType`, `etin`, `createdBy`, `creationTime`, `creationIpAddress`, `updatedBy`, `updationTime`, `updationIpAddress`,"
			+ "`taxAmount`,`taxableValue`,b2csType,invoiceNumber,toBeSync,isAmmendment,originalMonth,originalPos) "
			+ "VALUES (newId,newReturnType,newMonthYear,newGstin,newPos,newStateName,newSource,newSourceId,"
			+ "newSupplyType,newEtin,newCreatedBy,newCreationTime,newCreationIpAddress,newUpdatedBy,newUpdationTime,newUpdationIpAddress,"
			+ "newTaxAmount,newTaxableValue,newB2CSType,newInvoiceNumber,b'1',newIsAmmendment,newOriginalMonth,newOriginalPos);\n"+
			" END";
	private final static String dropB2CSPROC="drop procedure if exists b2csInsertUpdate;";  

	
	private final static String atProc=
			"CREATE procedure atInsertUpdate (\n" + 
			"  in newId varchar(50),\n" + 
			"  in newGstin int(11),\n" + 
			"  in newReturnType varchar(10),\n" + 
			"  in newMonthYear varchar(10),\n" + 
			"  in newAmmendmentMonthYear varchar(20),\n" + 
			"  in newPos varchar(2),\n" + 
			"  in newStateName varchar(30),\n" + 
			"  in newSupplyType varchar(20),\n" + 
			"  in newSource varchar(45),\n" + 
			"  in newSourceId varchar(100),\n" + 
			"  in newType varchar(100),\n" + 
			"  in newAdvanceReceived double(20,5),\n" + 
			"  in newCreatedBy varchar(100),\n" + 
			"  in newCreationTime timestamp,\n" + 
			"  in newCreationIpAddress varchar(20),\n" + 
			"  in newUpdatedBy varchar(100),\n" + 
			"  in newUpdationTime timestamp,\n" + 
			"  in newUpdationIpAddress varchar(20),"
			+ "in newTaxAmount double(20,5),\n" 
			
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalMonth varchar(10)"
			/*+ "in newOriginalPos varchar(4),"
			+ "in newOriginalSupplyType varchar(100)"*/

			+ 
			")\n" + 
			"BEGIN\n" + 
			"INSERT INTO `at_transaction` (`id`, `gstin`, `returnType`, `monthYear`, `pos`, `stateName`, `supplyType`,\n" + 
			"`source`, `sourceId`,`createdBy`, `creationTime`, `creationIpAddress`,`advanceReceived`,`ammendmentMonthYear`,`taxAmount`,toBeSync,isAmmendment,originalMonthYear)\n" + 
			" VALUES (newId,newGstin,newReturnType,newMonthYear,newPos,newStateName,newSupplyType,newSource,newSourceId,"
			+ "newCreatedBy,newCreationTime,newCreationIpAddress,newAdvanceReceived,newAmmendmentMonthYear,newTaxAmount,b'1',newIsAmmendment,newOriginalMonth);\n" + 
			" END";
	private final static String dropATPROC="drop procedure if exists atInsertUpdate;"; 
	
	private static final String txpdProc="create procedure txpdInsertUpdate("
			+ "in newId varchar(40),"
			+ "in newGstin varchar(15),"
			+ "in newReturnType varchar(15),"
			+ "in newMonthYear varchar(10),"
			+ "in newPos varchar(20),"
			+ "in newAdvanceAdjusted double(20,5),"
			+ "in newTaxAmount double(20,5),"
			+ "in newSource varchar(20),"
			+ "in newSourceId varchar(20),"
			+ "in newCreatedBy varchar(50),"
			+ "in newCreationTime date,"
			+ "in newCreationIpAddress varchar(30),"
			+ "in newAmmendMentMonthYear varchar(20),"
			+ "in newStateName varchar(40),"
			+ "in newSupplyType varchar(20),"
			
			+ "in newIsAmmendment bit(1),"
			+ "in newOriginalMonth varchar(10)"
			/*+ "in newOriginalPos varchar(4),"
			+ "in newOriginalSupplyType varchar(100)"*/
			
			+ ")"
			+ " BEGIN \n"+
			" INSERT INTO `txpd_transaction` (`id`, `gstin`, `returnType`, `monthYear`, `pos`, `advanceAdjusted`,"+ 
			"`taxAmount`,`source`, `sourceId`, `createdBy`, `creationTime`, `creationIpAddress`,`ammendmentMonthYear`,"
			+ " `stateName`,`supplyType`,toBeSync,isAmmendment,originalMonthYear)"
			+ " VALUES (newId,newGstin,newReturnType,newMonthYear,newPos,newAdvanceAdjusted,newTaxAmount,newSource,"
			+ " newSourceId,newCreatedby,newCreationTime,newCreationIpAddress,newAmmendmentMonthYear,newStateName,newSupplyType,b'1',newIsAmmendment,newOriginalMonth);"
			+ " END";

	private final static String dropTXPDPROC="drop procedure if exists txpdInsertUpdate;";  


	private final static String dropNILPROC="drop procedure if exists nilInsertUpdate;";  

	private final static String NilPROC="create procedure nilInsertUpdate(in newId varchar(50),"
			+ "in newGstin varchar(20),"
			+ "in newReturnType varchar(10),"
			+ "in newMonthYear varchar(10),"
			+ "in newSupplyType varchar(50),"
			
			+ "in newTotExmptAmt Double,"
			+ "in newTotNilAmt Double,"
			+ "in newTotNonGst Double,"
			+ "in newTotComp Double,"

			
			+ "in newSource varchar(20),"
			+ "in newSourceId varchar(50),"
			+ "in newFlags varchar(20),"
			+ "in newCreationTime timestamp,"
			+ "in newCreatedBy varchar(100),"
			+ "in newCreationIpAddress varchar(20),"
			+ "in newUpdatedBy varchar(50),"
			+ "in newUpdationTime timestamp,"
			+ "in newUpdationIpAddress varchar(20)"
			+ ")\n"
			+ "BEGIN\n"
			+ "INSERT INTO `nil_transaction` (`id`, `gstin`, `returnType`, `monthYear`, `supplyType`,totExptAmt,totNilAmt,totNgsupAmt,totCompAmt, `isValid`, `isTransit`, `isMarked`, `isSynced`, `isLocked`,\n"+ 
			 "`synchId`, `transitId`, `source`, `sourceId`, `flags`, `type`, `createdBy`, `creationTime`, `creationIpAddress`, `updatedBy`, `updationTime`, `updationIpAddress`,toBeSync)\n"+ 
			  "VALUES (newId,newGstin,newReturnType,newMonthYear,newSupplyType,newTotExmptAmt,newTotNilAmt,newTotNonGst,newTotComp, b'0', b'0', b'0', b'0', b'0',"
			  + " NULL, NULL,newSource,newSourceId, '', NULL,newCreatedBy,newCreationTime,newCreationIpAddress,"
			  + " NULL, NULL, NULL,b'1')"
			  + ";\n"
			  + "END";
	

	private final static String dropItemProc="drop procedure if exists itemInsertUpdate;";  
			 
	private final static String itemProc="create procedure itemInsertUpdate("
			+ "in newId varchar(40), in newSerialNumber varchar(45),in newHsnCode varchar(45), in newHsnDescription varchar(500)," + 
			" in newUnit varchar(45), in newQuantity double(20,5), in newTaxableValue double(20,5), in newTaxRate double(20,5),"
			+ "in newIgst double(20,5),in newSgst double(20,5),in newCess double(20,5)," + 
			"in newCgst double(20,5), in newTotalEligibleTax varchar(20), in newItcIgst double(20,5),in newItcCgst double(20,5),"
			+ "in newItcSgst double(20,5),in newItcCess double(20,5),in newInvoiceId varchar(45),in newOperation varchar(10))"
			+ "BEGIN\n"
			+ "if newOperation = 'update' then\n"
			+ "delete from items where invoiceId=newInvoiceId;\n"
			+ "END IF;\n"
			+"INSERT INTO `items` (`id`, `serialNumber`, `hsnCode`, `hsnDescription`,"
			+ " `unit`, `quantity`, `taxableValue`, `taxRate`, `igst`, `sgst`, `cess`, "
			+ "`cgst`, `totalEligibleTax`, `itcIgst`, `itcCgst`, `itcSgst`, `itcCess`, `invoiceId`)"
			+ " VALUES (newId,newSerialNumber,newHsnCode,newHsndescription,newUnit,newQuantity,newTaxableValue,newTaxRate,newIgst,newSgst,"
			+ "newCess,newCgst,newTotalEligibleTax,newItcIgst,newItcCgst,newItcSgst,newItcCess,newInvoiceId);\n"
			+ "END";
	
	private final static String impsProc=
			"CREATE procedure impsInsertUpdate (\n" + 
			"  in newId varchar(50),\n" + 
			"  in newGstin int(11),\n" + 
			"  in newReturnType varchar(10),\n"+
			"  in newMonthYear varchar(10),\n" + 
			 " in newFinancialYear varchar(10)," + 
			"  in newInvoiceNumber varchar(40),\n" + 
			"  in newInvoiceDate Date,\n" + 
			"  in newPos varchar(2),\n" + 
			"  in newStateName varchar(30),\n" + 
			"  in newSource varchar(45),\n" + 
			"  in newSourceId varchar(100),\n" + 
			"  in newType varchar(100),\n" + 
			"  in newTaxableValue double(20,5),\n" + 
			"  in newCreatedBy varchar(100),\n" + 
			"  in newCreationTime timestamp,\n" + 
			"  in newCreationIpAddress varchar(20),\n" + 
			"  in newUpdatedBy varchar(100),\n" + 
			"  in newUpdationTime timestamp,\n" + 
			"  in newUpdationIpAddress varchar(20),"
			+ "in operation varchar(20),"
			+ "in newTaxAmount double(20,5)\n" + 
			")\n" + 
			"BEGIN\n"+
			 " if operation = 'update' then \n"
			 + "update imps_transaction set `invoiceDate`=newInvoiceDate, `pos`=newPos, `stateName`=newStateName,\n" + 
			" `source`=newSource, `sourceId`=newSourceId,\n" + 
			" `type`=newType, `updatedBy`=newUpdatedBy,`updationTime`=newUpdationTime, `updationIpAddress`=newUpdationIpAddress,`taxableValue`=newTaxableValue,"
			+ " `taxAmount`=newTaxAmount,`toBeSync`=b'1' ,isError=b'0',errMsg='',flags='' where id=newId ;"
			+ " else "+
			"INSERT INTO `imps_transaction` (`id`, `gstin`, `returnType`, `monthYear`,`invoiceNumber`,`invoiceDate`, `pos`, `stateName`,\n" + 
			" `isAmmendment`, `isValid`, `isTransit`, `isMarked`, `isSynced`, `isLocked`, `syncId`, `transitId`, `source`, `sourceId`,\n" + 
			" `flags`, `type`, `createdBy`, `creationTime`, `creationIpAddress`, `updatedBy`, `updationTime`, `updationIpAddress`,`taxableValue`,"
			+ "`taxAmount`,toBeSync"
			+ ")\n" + 
			" VALUES (newId,newGstin,newReturnType,newMonthYear,newInvoiceNumber,newInvoiceDate,newPos,newStateName, b'0', b'0', b'0', b'0', b'0', b'0',\n" + 
			" NULL, NULL,newSource,newSourceId, NULL,newType,newCreatedBy,newCreationTime,newCreationIpAddress,newUpdatedBy,newUpdationTime,"
			+ "newUpdationIpAddress,newTaxableValue,newTaxAmount,b'1');\n"
			+ " END IF ; " + 
			" END";
	
	private final static String dropImpsPROC="drop procedure if exists impsInsertUpdate;";  
	
	
	private final static String impgProc="create procedure impgInsertUpdate("
			+ "in newId varchar(40),"
			+ "in newBillOfEntryNumber varchar(20),"
			+ "in newBillOfEntryDate varchar(30),"
			+ "in newBillOfEntryValue double(20,5),"
			+ "in newCtin varchar(16),"
			+ "in newMonthYear varchar(10),"
			+ "in newPortCode varchar(20),"
			+ "in newReturnType varchar(10),"
			+ "in newSource varchar(30),"
			+ "in newSourceId varchar(30),"
			+ "in newSez varchar(2),"
			+ "in newTaxableValue double(20,5),"
			+ "in newTaxAmount double(20,5),"
			+ "in newCreatedBy varchar(50),"
			+ "in newCreationTime timestamp,"
			+ "in newCreationIPAddress varchar(20),"
			+ "in newUpdationTime timestamp,"
			+ "in newUpdationIPAddress varchar(20),"
			+ "in newUpdatedBy varchar(50),"
			+ "in newGstin int(10),"
			+ "in newOperation varchar(10))"
			+ " BEGIN \n"
			+ " if newOperation = 'update' then \n"
			+ "update `impg_transaction` set `billOfEntryDate`=newBillOfEntrydate,`billOfEntryValue`=newBillOfEntryValue,"
			+ "`portCode`=newPortCode,`taxableValue`=newTaxableValue,`taxAmount`=newTaxAmount,`source`=newSource,`sourceId`=newSourceId,"
			+ "`updatedBy`=newUpdatedBy,`updationTime`=newUpdationTime,`updationIpAddress`=newUpdationIPAddress,`sez`=newSez,`toBeSync`=b'1' "
			+ " ,isError=b'0',errMsg='',flags=''where id=newId; "
			+ " else "+
	          " INSERT INTO `impg_transaction` (`id`, `gstin`, `returnType`, `monthYear`, `ctin`,`billOfEntryNumber`,"+
			"`billOfEntryDate`, `billOfEntryValue`, `portCode`, `taxableValue`, `taxAmount`,"+
			"`source`, `sourceId`,`createdBy`, `creationTime`, `creationIpAddress`, `updatedBy`, `updationTime`, `updationIpAddress`,`sez`,toBeSync)"+
	        " VALUES (newId,newGstin,newReturnType,newMonthYear,newCtin,newBillOfEntryNumber,newBillOfEntryDate,newBillOfEntryValue,newPortCode,"
	        + "newTaxableValue,newTaxAmount,newSource,newSourceId,newCreatedBy,newCreationTime,newCreationIPAddress,newUpdatedBy,newUpdationTime,newUpdationIPAddress"
	        + ",newSez,b'1');"
	        + " END IF ;"
			+ " END;";
	
	private static final String dropImpgProc="drop procedure if exists impgInsertUpdate;";


	private final static String _B2BURProcedure=
			"create procedure b2burInsertUpdate("
			+ " in newId varchar(38),"
			+ " in newGstin int(20),"
			+ " in newReturnType varchar(20),"
			+ " in newPos varchar(5),"
			+ " in newStateName varchar(20),"
			+ " in newInvoiceNumber varchar(20),"
			+ " in newInvoiceDate timestamp,"
			+ " in newTaxableValue double(20,5),"
			+ " in newTaxAmount double(20,5),"
			+ " in newSource varchar(20),"
			+ " in newSourceId varchar(20),"
			+ " in newCreatedBy varchar(50),"
			+ " in newCreationTime timestamp,"
			+ " in newCreationIpAddress varchar(20),"
			+ " in newUpdationTime timestamp,"
			+ " in newUpdatedBy varchar(40),"
			+ " in newUpdationIpAddress varchar(20),"
			+ " in operation varchar(10),"
			+ " in newSupplyType varchar(20),"
			+ " in newMonthYear varchar(10)"
			+ ")"
			+ " BEGIN \n"
			+ " if operation = 'update' then \n"
			+ "update `b2bur_transaction` set  `pos`=newPos, `stateName`=newStateName, "
			+ "`supplyType`=newSupplyType,`invoiceDate`=newInvoiceDate, `taxableValue`=newTaxableValue, `taxAmount`=newTaxAmount,"
			+ "`source`=newSource, `sourceId`=newSourceId,`updatedBy`=newUpdatedBy,"
			+ " `updationIpAddress`=newUpdationIpAddress,`updationTime`=newUpdationTime,`toBeSync`=b'1' ,isError=b'0',errMsg='',flags='' where id=newId ;"
			+ " else "
			+ " INSERT INTO `b2bur_transaction` (`id`, `gstin`, `returnType`, `monthYear`, `pos`, `stateName`, "
			+ "`supplyType`, `invoiceNumber`, `invoiceDate`, `taxableValue`, `taxAmount`,`source`, `sourceId`,`createdBy`, `creationTime`,"
			+ " `creationIpAddress`,`updationTime`,toBeSync) VALUES (newId,newGstin,newReturnType,newMonthYear,newPos,newStateName,newSupplyType,newInvoiceNumber,"
			+ "newInvoiceDate,newTaxableValue,newTaxAmount,newSource,newSourceId,newCreatedBy,newCreationTime,newCreationIpAddress,newUpdationTime,b'1'); "
			+ "END IF;"
			+ " END; ";

	
	private final static String _DROPB2BURPROC="drop procedure if exists b2burInsertUpdate";

	
	public static String getDeleteallb2b() {
		return _DELETEALLB2B;
	}

	public static String getImpsproc() {
		return impsProc;
	}

	public static String getDropimpsproc() {
		return dropImpsPROC;
	}

	public static String getImpgproc() {
		return impgProc;
	}

	public static String getDropimpgproc() {
		return dropImpgProc;
	}

	public static String getB2burprocedure() {
		return _B2BURProcedure;
	}

	public static String getDropb2burproc() {
		return _DROPB2BURPROC;
	}

	public static String getB2binsertupdateprocedure() {
		return b2bInsertUpdateProcedure;
	}

	public static String getDropb2bprocedure() {
		return dropB2bProcedure;
	}

	public static String getCdnprocedure() {
		return _CDNProcedure;
	}

	public static String getDropcdnproc() {
		return _DROPCDNPROC;
	}

	public static String getCdnurprocedure() {
		return _CDNURProcedure;
	}

	public static String getDropcdnurproc() {
		return _DROPCDNURPROC;
	}

	public static String getTxpdproc() {
		return txpdProc;
	}

	public static String getDroptxpdproc() {
		return dropTXPDPROC;
	}
	
	public static String getB2clproc() {
		return b2clProc;
	}

	public static String getB2cldropproc() {
		return b2clDropProc;
	}

	public static String getExpproc() {
		return expProc;
	}

	public static String getDropexpproc() {
		return dropExpPROC;
	}

	public static String getB2csproc() {
		return b2csProc;
	}

	public static String getDropb2csproc() {
		return dropB2CSPROC;
	}

	public static String getAtproc() {
		return atProc;
	}

	public static String getDropatproc() {
		return dropATPROC;
	}

	public static String getDropnilproc() {
		return dropNILPROC;
	}

	public static String getNilproc() {
		return NilPROC;
	}

	public static String getDropitemproc() {
		return dropItemProc;
	}

	public static String getItemproc() {
		return itemProc;
	}
	
	public static String getImpsProc() {
		return impsProc;
	}

	public static String getDropImpsProc() {
		return dropImpsPROC;
	}	
	
	
	
}
