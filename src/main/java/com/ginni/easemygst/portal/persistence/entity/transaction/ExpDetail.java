package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.EXPDetailSerializer;


/**
 * The persistent class for the exp_details database table.
 * 
 */
@Entity
@Table(name="exp_details")
@NamedQueries({
@NamedQuery(name="ExpDetail.findAll", query="SELECT e FROM ExpDetail e"),
@NamedQuery(name = "ExpDetailEntity.findByPagination", query = "SELECT b FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear"),
@NamedQuery(name = "ExpDetailEntity.findCountByMonthYear", query = "SELECT count(b.id) FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear and b.flags <> 'DELETE'"),
@NamedQuery(name = "ExpDetailEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null) " ),


@NamedQuery(name="ExpDetail.removeById",query="delete from ExpDetail b where b.id in :ids and b.isSynced=false and b.isTransit=false "),
@NamedQuery(name="ExpDetail.deleteByMonthYear",query="delete from ExpDetail b where b.expTransaction in (select exp from EXPTransactionEntity exp where exp.monthYear =:monthYear and exp.gstin.gstin =:gstin and exp.returnType =:returnType) and b.isSynced=false and b.isTransit=false"),
@NamedQuery(name="ExpDetail.updateFlagByMonthYear",query="update  ExpDetail b set b.flags='DELETE',b.toBeSync=true where b.expTransaction in (select exp from EXPTransactionEntity exp where exp.monthYear =:monthYear and exp.gstin.gstin =:gstin and exp.returnType =:returnType) and b.isSynced=true and b.isTransit=false"),
@NamedQuery(name="ExpDetail.updateFlagById",query="update ExpDetail b set b.flags='DELETE',b.toBeSync=true  where b.id in :ids and b.isSynced=true and b.isTransit=false"),


@NamedQuery(name = "ExpDetailEntity.findSummaryByMonthYear", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear and b.flags <> 'DELETE'"),
@NamedQuery(name = "ExpDetailEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear group by b.type"),
@NamedQuery(name = "ExpDetailEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
@NamedQuery(name="ExpDetail.findNotSynced",query="select a from ExpDetail a where a.expTransaction.gstin=:gstn and a.expTransaction.monthYear in (:monthYear) and a.toBeSync=true and a.expTransaction.returnType=:returnType and a.isTransit=false and a.isError=false"),
@NamedQuery(name = "ExpDetail.findErrorInvoice",query = "Select b from ExpDetail  b where b.expTransaction.gstin=:gstn and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear in (:monthYear) and b.invoiceNumber=:invno "),
@NamedQuery(name= "ExpDetail.findErrorInvoicesByTransitId",query="SELECT b from ExpDetail b where b.expTransaction.gstin=:gstn and b.expTransaction.monthYear in (:monthYear) and b.expTransaction.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
@NamedQuery(name="ExpDetail.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from ExpDetail b where  b.expTransaction.monthYear=:monthYear and b.expTransaction.returnType=:returnType and b.expTransaction.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError" ),
@NamedQuery(name = "ExpDetail.findErrorDataByMonthYear", query = "SELECT b FROM ExpDetail b where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear  and b.isError=true and  (b.flags <> 'DELETE' or b.flags is null ) " ),

@NamedQuery(name = "ExpDetail.findBlankHsnBulk", query = "SELECT  b.invoiceNumber,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,i.unit,i.quantity,b.invoiceDate FROM ExpDetail b JOIN b.items i where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null) " ),
@NamedQuery(name = "ExpDetail.countBlankHsnBulk", query = "SELECT  count(i.id) FROM ExpDetail b JOIN b.items i where b.expTransaction.gstin=:taxPayerGstin and b.expTransaction.returnType=:returnType and b.expTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null) " ),


})
@JsonSerialize(using=EXPDetailSerializer.class)
public class ExpDetail extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String exportType;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="expTransactionId")
	private EXPTransactionEntity expTransaction;

	private String flags;

	private String invoiceNumber;
	
	private Date invoiceDate;
	
	private String shippingBillPortCode;
	
	private String shippingBillNo;
	
	private Date shippingBillDate;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	private List<Item> items;
	
	private Double taxableValue=0.0;

	private Double taxAmount=0.0;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;
	
	@Column(name="syncId")
	private String synchId;

	private String transitId;

	private String type;
	
	private Boolean toBeSync;
	
	private transient String error_msg;
	
	private transient String error_cd;
	
	private Boolean isError=false;
	
	private String errMsg;
	
private String originalInvoiceNumber ;
	
	private Date originalInvoiceDate;

	public ExpDetail() {
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}


	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}


	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}


	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}


	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public Date getShippingBillDate() {
		return shippingBillDate;
	}


	public void setShippingBillDate(Date shippingBillDate) {
		this.shippingBillDate = shippingBillDate;
	}


	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}



	public String getExportType() {
		return this.exportType;
	}

	public void setExportType(String exportType) {
		this.exportType = exportType;
	}

	public Double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public Double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public EXPTransactionEntity getExpTransaction() {
		return expTransaction;
	}

	public void setExpTransaction(EXPTransactionEntity expTransaction) {
		this.expTransaction = expTransaction;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public String getShippingBillPortCode() {
		return shippingBillPortCode;
	}


	public void setShippingBillPortCode(String shippingBillPortCode) {
		this.shippingBillPortCode = shippingBillPortCode;
	}


	public String getShippingBillNo() {
		return shippingBillNo;
	}


	public void setShippingBillNo(String shippingBillNo) {
		this.shippingBillNo = shippingBillNo;
	}


	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}
	
	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getOriginalInvoiceNumber() {
		return originalInvoiceNumber;
	}

	public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
		this.originalInvoiceNumber = originalInvoiceNumber;
	}

	public Date getOriginalInvoiceDate() {
		return originalInvoiceDate;
	}

	public void setOriginalInvoiceDate(Date originalInvoiceDate) {
		this.originalInvoiceDate = originalInvoiceDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.invoiceNumber, ((ExpDetail) obj).getInvoiceNumber()).append(this.expTransaction, ((ExpDetail) obj).getExpTransaction());
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exportType == null) ? 0 : exportType.hashCode());
		return result;
	}


	public List<Item> getItems() {
		return items;
	}


	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public ExpDetail(String exportType ,String id, String invoiceNumber, EXPTransactionEntity b2bTransaction,
			Date invoiceDate, double taxableValue, double taxAmount, String flags, String type, String source,
			String shippingBillPortCode, String shippingBillNo,Date shippingBillDate,boolean isSynced,
			boolean toBeSync,boolean isTransit,boolean isError,
			boolean isAmmendment,String originalInvoiceNumber,Date originalInvoiceDate
			) {
	
		this.setId(id);
		this.invoiceNumber=invoiceNumber;
		this.expTransaction=b2bTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.source=source;
		this.shippingBillDate=shippingBillDate;
		this.shippingBillPortCode=shippingBillPortCode;
		this.shippingBillNo=shippingBillNo;
		this.exportType=exportType;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
		this.isAmmendment=isAmmendment;
		this.originalInvoiceNumber=originalInvoiceNumber;
		this.originalInvoiceDate=originalInvoiceDate;
		
	}
	
	public ExpDetail(String exportType, String id, String invoiceNumber, EXPTransactionEntity expTransaction,
			Date invoiceDate, Double taxableValue, Double taxAmount, String flags, String type,
			String shippingBillPortCode, String shippingBillNo, Date shippingBillDate,Item item,boolean isAmmendment) {
		this.exportType=exportType;
		this.setId(id);
		this.setInvoiceNumber(invoiceNumber);
		this.expTransaction=expTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.shippingBillDate=shippingBillDate;
		this.shippingBillPortCode=shippingBillPortCode;
		this.shippingBillNo=shippingBillNo;
		this.isAmmendment=isAmmendment;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}
	}
	
	public ExpDetail(String exportType, String id, String invoiceNumber, EXPTransactionEntity expTransaction,
			Date invoiceDate, Double taxableValue, Double taxAmount, String flags, String type,
			String shippingBillPortCode, String shippingBillNo, Date shippingBillDate,Item item,
			boolean isAmmendment,String originalInvoiceNumber,Date originalInvoiceDate) {
		this.exportType=exportType;
		this.setId(id);
		this.setInvoiceNumber(invoiceNumber);
		this.expTransaction=expTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.shippingBillDate=shippingBillDate;
		this.shippingBillPortCode=shippingBillPortCode;
		this.shippingBillNo=shippingBillNo;
		this.isAmmendment=isAmmendment;
		this.originalInvoiceNumber=originalInvoiceNumber;
		this.originalInvoiceDate=originalInvoiceDate;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}
	}
	
	public TransactionType getTransactionType() {
		return TransactionType.EXP;
	}
}