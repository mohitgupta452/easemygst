package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.ginni.easemygst.portal.persistence.service.QueryParameter;

import java.sql.Timestamp;
import java.time.YearMonth;
import java.util.Date;


/**
 * The persistent class for the gstin_subscription_duration database table.
 * 
 */
@Entity
@Table(name="gstin_subscription_duration")
@NamedQueries({
@NamedQuery(name="GstinSubscriptionDuration.findAll", query="SELECT g FROM GstinSubscriptionDuration g"),
@NamedQuery(name="GstinSubscriptionDuration.findByGstinMonthYearOrganization",query="select g from GstinSubscriptionDuration g where g.taxpayerGstin=:gstin  and  :monthYear>=g.monthYearFrom and :monthYear<=g.monthYearTo"),
@NamedQuery(name="GstinSubscriptionDuration.findByGstin",query="select g from GstinSubscriptionDuration g where g.taxpayerGstin=:gstin "),

})
public class GstinSubscriptionDuration implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="gstinId")
	private int taxpayerGstin;

	
	/*private Timestamp monthYearFrom;

	private Timestamp monthYearTo;*/
	
	@Temporal(TemporalType.DATE)
	private Date monthYearFrom;

	@Temporal(TemporalType.DATE)
	private Date monthYearTo;

	@Column(name="organizationId")
	private int organization;

	private Timestamp updationTime;

	public GstinSubscriptionDuration() {
	}

	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getTaxpayerGstin() {
		return taxpayerGstin;
	}



	public void setTaxpayerGstin(int taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	
	/*public Timestamp getMonthYearFrom() {
		return this.monthYearFrom;
	}

	public void setMonthYearFrom(Timestamp monthYearFrom) {
		this.monthYearFrom = monthYearFrom;
	}

	public Timestamp getMonthYearTo() {
		return this.monthYearTo;
	}

	public void setMonthYearTo(Timestamp monthYearTo) {
		this.monthYearTo = monthYearTo;
	}
*/
	
	public Date getMonthYearFrom() {
		return this.monthYearFrom;
	}

	public void setMonthYearFrom(Date monthYearFrom) {
		this.monthYearFrom = monthYearFrom;
	}

	public Date getMonthYearTo() {
		return this.monthYearTo;
	}

	public void setMonthYearTo(Date monthYearTo) {
		this.monthYearTo = monthYearTo;
	}
	
	public int getOrganization() {
		return this.organization;
	}

	public void setOrganization(int ognization) {
		this.organization = ognization;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

}