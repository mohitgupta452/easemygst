package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import javax.persistence.*;

import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the taxpayer_summary database table.
 * 
 */
@Entity
@Table(name="taxpayer_summary")
@NamedQuery(name="TaxpayerSummary.findAll", query="SELECT t FROM TaxpayerSummary t")
public class TaxpayerSummary extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private double cess;

	private double cgst;

	private String checksum;

	@JoinColumn(name="cptSummId")
	@OneToMany
	private List<TaxpayerSummary> cptSummaries;

	private String ctin;

	private double igst;

	private int invoiceCount;

	private String monthYear;

	private double sgst;

	private String source;

	private String summaryType;

	private double taxAmount;

	@JoinColumn(name="taxpayerGstinId")
	@ManyToOne
	private TaxpayerGstin taxpayerGstin;

	private String transactionType;

	private double value;

	public TaxpayerSummary() {
	}

	public double getCess() {
		return this.cess;
	}

	public void setCess(double cess) {
		this.cess = cess;
	}

	public double getCgst() {
		return this.cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public String getChecksum() {
		return this.checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public List<TaxpayerSummary> getCptSummaries() {
		return cptSummaries;
	}

	public void setCptSummaries(List<TaxpayerSummary> cptSummaries) {
		this.cptSummaries = cptSummaries;
	}

	public String getCtin() {
		return this.ctin;
	}

	public void setCtin(String ctin) {
		this.ctin = ctin;
	}

	public double getIgst() {
		return this.igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public int getInvoiceCount() {
		return this.invoiceCount;
	}

	public void setInvoiceCount(int invoiceCount) {
		this.invoiceCount = invoiceCount;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public double getSgst() {
		return this.sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSummaryType() {
		return this.summaryType;
	}

	public void setSummaryType(String summaryType) {
		this.summaryType = summaryType;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}


	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}


	public double getValue() {
		return this.value;
	}

	public void setValue(double value) {
		this.value = value;
	}

}