package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import lombok.Data;

import java.util.Date;


/**
 * The persistent class for the report_data database table.
 * 
 */
@Entity
@Table(name="report_data")

@NamedQueries({
	@NamedQuery(name="ReportData.findAll", query="SELECT r FROM ReportData r"),
	@NamedQuery(name="ReportData.findgstnresponsedata", query="SELECT r.gstnresponsedata FROM ReportData r where r.monthYear=:monthYear and r.returnType=:returnType and r.gstinId=:gstin and r.type=:type and r.status='FILED'"),
	@NamedQuery(name="ReportData.findallgstnresponsedata", query="SELECT r.gstnresponsedata FROM ReportData r where r.monthYear=:monthYear and r.returnType=:returnType and r.gstinId=:gstin and r.type=:type order by r.status"),
	}
	)
public @Data class ReportData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDateTime;

	@Lob
	private String gstnresponsedata;

	private String monthYear;

	private String returnType;

	private Integer gstinId;
	
	private String type;
	
	private String status;
	

	public ReportData() {
	}


	@PrePersist
	public void prepersist(){
		
		this.creationDateTime=new Timestamp(new Date().getTime());	
		
	}
	
	
}