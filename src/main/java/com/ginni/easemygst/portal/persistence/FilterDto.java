package com.ginni.easemygst.portal.persistence;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

public @Data class FilterDto implements Serializable{
	
	int pageNo;
	
	int size;
	
	boolean isActive;

	Map<String,DataFilter>filterDetail;
}
