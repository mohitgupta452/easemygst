package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_outward_combined_new database table.
 * 
 */
@Entity
@Table(name="tbl_outward_combined_new")
@NamedQueries({
@NamedQuery(name="TblOutwardCombinedNew.findAll", query="SELECT t FROM TblOutwardCombinedNew t"),
@NamedQuery(name="TblOutwardCombinedNew.findGroupedPartOneAndPartFiveGstr3b", query="SELECT new com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew(sum(t.taxable_Value),sum(t.igst),sum(t.cgst),sum(t.sgst),sum(t.cess),t.transaction_Type) FROM TblOutwardCombinedNew t where t.gstin=:gstin and t.month_Id=:monthYear and (t.transaction_Type  like '%PART1%' or  t.transaction_Type  like '%PART5%' ) group by t.transaction_Type" ),
@NamedQuery(name="TblOutwardCombinedNew.findAllPartExceptOneAndFive", query="SELECT t FROM TblOutwardCombinedNew t where t.gstin=:gstin and t.month_Id=:monthYear and t.transaction_Type not like '%PART1%' and  t.transaction_Type not like '%PART5%'" ),
@NamedQuery(name="TblOutwardCombinedNew.findGroupedPart3", query="SELECT new com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew(sum(t.taxable_Value),sum(t.igst),sum(t.cgst),sum(t.sgst),sum(t.cess),t.transaction_Type) FROM TblOutwardCombinedNew t where t.gstin=:gstin and t.month_Id=:monthYear and (t.transaction_Type  like '%PART3%') group by t.transaction_Type" ),
@NamedQuery(name="TblOutwardCombinedNew.findGroupedPart2", query="SELECT new com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew(sum(t.taxable_Value),sum(t.igst),sum(t.cgst),sum(t.sgst),sum(t.cess),t.transaction_Type) FROM TblOutwardCombinedNew t where t.gstin=:gstin and t.month_Id=:monthYear and (t.transaction_Type  like '%PART2%') group by t.transaction_Type" ),
@NamedQuery(name="TblOutwardCombinedNew.deleteByGstinMonthYear",query="delete from TblOutwardCombinedNew b where b.gstin=:gstinId and b.month_Id=:monthYear"),
@NamedQuery(name="TblOutwardCombinedNew.deleteByGstinMonthYearPart",query="delete from TblOutwardCombinedNew b where b.gstin=:gstinId and b.month_Id=:monthYear and b.transaction_Type='PART5A' or b.transaction_Type='PART5B' "),


})
public class TblOutwardCombinedNew implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private double cess;

	private double cgst;

	@Column(name="`Entry Type`")
	private String entry_Type;

	private int gstin;

	private double igst;

	@Column(name="`Month Id`")
	private String month_Id;

	private double sgst;

	@Column(name="`Supply Type`")
	private String supply_Type;

	@Column(name="`Taxable Value`")
	private double taxable_Value;

	@Column(name="`To State`")
	private String to_State;

	@Column(name="`Transaction Type`")
	private String transaction_Type;

	public TblOutwardCombinedNew() {
		
		
	}
	
	public TblOutwardCombinedNew(double taxable_Value,double igst,double cgst,double sgst,double cess,String transaction_Type ){
		this.igst=igst;
		this.cgst=cgst;
		this.sgst=sgst;
		this.cess=cess;
		this.taxable_Value=taxable_Value;
		this.transaction_Type=transaction_Type;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCess() {
		return this.cess;
	}

	public void setCess(double cess) {
		this.cess = cess;
	}

	public double getCgst() {
		return this.cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public String getEntry_Type() {
		return this.entry_Type;
	}

	public void setEntry_Type(String entry_Type) {
		this.entry_Type = entry_Type;
	}

	public int getGstin() {
		return this.gstin;
	}

	public void setGstin(int gstin) {
		this.gstin = gstin;
	}

	public double getIgst() {
		return this.igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public String getMonth_Id() {
		return this.month_Id;
	}

	public void setMonth_Id(String month_Id) {
		this.month_Id = month_Id;
	}

	public double getSgst() {
		return this.sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public String getSupply_Type() {
		return this.supply_Type;
	}

	public void setSupply_Type(String supply_Type) {
		this.supply_Type = supply_Type;
	}

	public double getTaxable_Value() {
		return this.taxable_Value;
	}

	public void setTaxable_Value(double taxable_Value) {
		this.taxable_Value = taxable_Value;
	}

	public String getTo_State() {
		return this.to_State;
	}

	public void setTo_State(String to_State) {
		this.to_State = to_State;
	}

	public String getTransaction_Type() {
		return this.transaction_Type;
	}

	public void setTransaction_Type(String transaction_Type) {
		this.transaction_Type = transaction_Type;
	}

}