package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsonSchema.types.ArraySchema.Items;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.view.serializer.B2CSDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

/**
 * The persistent class for the b2cs_transaction database table.
 * 
 */
@Entity
@Table(name = "b2cs_transaction")
@NamedQueries({ @NamedQuery(name = "B2csTransaction.findAll", query = "SELECT b FROM B2CSTransactionEntity b"),
		@NamedQuery(name = "B2csTransaction.getByGstinMonthyearReturntype", query = "select b from B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
		@NamedQuery(name = "B2csTransaction.findByPagination", query = "select b from B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
		@NamedQuery(name = "B2csTransaction.findCountByMonthYear", query = "select count(b.id) from B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
		@NamedQuery(name = "B2csTransaction.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),

		@NamedQuery(name = "B2CSTransactionEntity.removeById", query = "delete from B2CSTransactionEntity b where b.id in :ids b.isSynced=false and b.isTransit=false"),
		@NamedQuery(name = "B2CSTransactionEntity.deleteByMonthYear", query = "delete from B2CSTransactionEntity b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false  and b.returnType =:returnType"),
		@NamedQuery(name = "B2CSTransactionEntity.updateFlagByMonthYear", query = "update  B2CSTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
		@NamedQuery(name = "B2CSTransactionEntity.updateFlagById", query = "update B2CSTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),

		@NamedQuery(name = "B2csTransaction.findSummaryByMonthYear", query = "select SUM(b.taxableValue),SUM(b.taxAmount) from B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
		@NamedQuery(name = "B2csTransaction.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
		@NamedQuery(name = "B2csTransaction.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync"),

		@NamedQuery(name = "B2CSTransactionEntity.findNotSynced", query = "select b from B2CSTransactionEntity b where b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and  b.isError=false"),
		@NamedQuery(name = "B2CSTransactionEntity.findErrorInvoice", query = "Select b from B2CSTransactionEntity  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) and b.pos=:pos "),
		@NamedQuery(name = "B2CSTransactionEntity.findErrorInvoicesByTransitId", query = "SELECT b from B2CSTransactionEntity b where b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
		@NamedQuery(name = "B2CSTransactionEntity.getFlagsCount", query = "select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id))  from B2CSTransactionEntity b where b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
		@NamedQuery(name = "B2csTransaction.findErrorDataByMonthYear", query = "select b from B2CSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),
		@NamedQuery(name = "B2csTransaction.findBlankHsnBulk", query = "SELECT b.invoiceNumber,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,i.unit,i.quantity FROM B2CSTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)"),
		@NamedQuery(name = "B2csTransaction.countBlankHsnBulk", query = "SELECT count(i.id) FROM B2CSTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)"),

})

@JsonSerialize(using = B2CSDetailSerializer.class)
public class B2CSTransactionEntity extends CommonAttributesEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String monthYear;

	private String returnType;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "gstin")
	private TaxpayerGstin gstin;

	private String etin;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "invoiceId", referencedColumnName = "id")
	private List<Item> items;

	private String flags;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;

	private String pos;

	private String stateName;

	private Double taxableValue = 0.0;

	private Double taxAmount = 0.0;

	private String syncId;

	private String transitId;

	private String type;

	private String b2csType;

	private String invoiceNumber;

	@Enumerated(EnumType.STRING)
	private SupplyType supplyType;

	private Boolean toBeSync;

	private transient String error_msg;

	private transient String error_cd;

	private Boolean isError = false;

	private String errMsg;

	private String originalMonth;

	private String originalPos;

	public String getOriginalMonth() {
		return originalMonth;
	}

	public void setOriginalMonth(String originalMonth) {
		this.originalMonth = originalMonth;
	}

	public String getOriginalPos() {
		return originalPos;
	}

	public void setOriginalPos(String originalPos) {
		this.originalPos = originalPos;
	}

	@javax.persistence.Transient
	private TransactionType transactionType;

	public B2CSTransactionEntity() {
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public TaxpayerGstin getGstin() {
		return gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public String getEtin() {
		return etin;
	}

	public void setEtin(String etin) {
		this.etin = etin;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = (Integer.parseInt(pos) < 10 && pos.length() == 1) ? "0" + pos : pos;
	}

	public double getTaxableValue() {
		return taxableValue;
	}

	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getSyncId() {
		return syncId;
	}

	public void setSyncId(String syncId) {
		this.syncId = syncId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SupplyType getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(SupplyType supplyType) {
		this.supplyType = supplyType;
	}

	public String getB2csType() {
		return b2csType;
	}

	public void setB2csType(String b2csType) {
		this.b2csType = b2csType;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	@Transient
	public Item getItem() {
		if (this.items != null && !items.isEmpty())
			return this.items.get(0);
		return null;
	}

	@Transient
	public double getTaxRate() {
		if (this.getItem() != null)
			return this.getItem().getTaxRate();
		return 0;
	}

	public B2CSTransactionEntity(SupplyType supplyType, String id, String pos, String stateName, double taxableValue,
			double taxAmount, String etin, String flags, String type, String source, String b2csType,
			String invoiceNumber, boolean isSynced, boolean toBeSync, boolean isTransit, boolean isError,
			boolean isAmmendment, String originalMonth, String originalPos

	) {
		/*
		 * try{ this.supplyType=SupplyType.valueOf(supplyType); } catch(Exception e){
		 * this.supplyType=SupplyType.INTERSTATE_CONSUMER; }
		 */
		this.setId(id);
		this.pos = pos;
		this.stateName = stateName;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.etin = etin;
		this.flags = flags;
		this.type = type;
		this.source = source;
		this.supplyType = supplyType;
		this.b2csType = b2csType;
		this.invoiceNumber = invoiceNumber;
		this.isSynced = isSynced;
		this.toBeSync = toBeSync;
		this.isTransit = isTransit;
		this.isError = isError;
		this.isAmmendment = isAmmendment;
		this.originalMonth = originalMonth;
		this.originalPos = originalPos;

	}

	// constructor for gstn
	public B2CSTransactionEntity(String returnType, String monthYear, SupplyType supplyType, String id, String pos,
			String stateName, double taxableValue, double taxAmount, String etin, String flags, String type,
			String b2csType, String invoiceNumber, Item item, boolean isAmendment) {

		this.setId(id);
		this.pos = pos;
		this.stateName = stateName;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.etin = etin;
		this.flags = flags;
		this.type = type;
		this.supplyType = supplyType;
		this.b2csType = b2csType;
		this.invoiceNumber = invoiceNumber;
		this.returnType = returnType;
		this.monthYear = monthYear;
		this.isAmmendment = isAmendment;
		if (Objects.isNull(items)) {
			this.items = new ArrayList<>();
			this.getItems().add(item);

		} else {
			this.getItems().add(item);
		}

	}

	// constructor for gstn b2csa
	public B2CSTransactionEntity(String returnType, String monthYear, SupplyType supplyType, String id, String pos,
			String stateName, double taxableValue, double taxAmount, String etin, String flags, String type,
			String b2csType, String invoiceNumber, Item item, boolean isAmendment, String originalPos,
			String originalMonth) {

		this.setId(id);
		this.pos = pos;
		this.stateName = stateName;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.etin = etin;
		this.flags = flags;
		this.type = type;
		this.supplyType = supplyType;
		this.b2csType = b2csType;
		this.invoiceNumber = invoiceNumber;
		this.returnType = returnType;
		this.monthYear = monthYear;
		this.isAmmendment = isAmendment;
		this.originalMonth = originalMonth;
		this.originalPos = originalPos;
		if (Objects.isNull(items)) {
			this.items = new ArrayList<>();
			this.getItems().add(item);

		} else {
			this.getItems().add(item);
		}
	}

	public static B2CSTransactionEntity contains(String pos, String b2csType, Double taxrate,
			List<B2CSTransactionEntity> B2CSTransactionEntitylist, String hsnCode) {

		for (B2CSTransactionEntity b2csTransactionEntity : B2CSTransactionEntitylist) {
			if (Integer.parseInt(pos) == Integer.parseInt(b2csTransactionEntity.getPos())
					&& b2csTransactionEntity.getTaxRate() == taxrate
					&& b2csType.equals(b2csTransactionEntity.getB2csType())
					&& StringUtils.equals(hsnCode, b2csTransactionEntity.getItem().getHsnCode())) {
				return b2csTransactionEntity;
			}

		}
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (obj == this) {
			return true;
		}
		B2CSTransactionEntity bts = (B2CSTransactionEntity) obj;

		return bts.getPos().equalsIgnoreCase(pos) && bts.getTaxRate() == this.getItem().getTaxRate()
				&& bts.getB2csType().equals(b2csType);
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public TransactionType getTransactionType() {
		return TransactionType.B2CS;
	}
}