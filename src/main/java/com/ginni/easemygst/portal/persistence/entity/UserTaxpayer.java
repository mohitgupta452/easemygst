package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the user_taxpayer database table.
 * 
 */
@Entity
@Table(name="user_taxpayer")
@NamedQueries({ @NamedQuery(name="UserTaxpayer.findAll", query="SELECT u FROM UserTaxpayer u WHERE u.taxpayer.active=1"),
	@NamedQuery(name="UserTaxpayer.findtype", query="SELECT u FROM UserTaxpayer u WHERE u.user.id=:uid and u.taxpayer.id=:tid"),
	@NamedQuery(name = "UserTaxpayer.findIdentityByUsername", query = "SELECT u FROM UserTaxpayer u WHERE u.user.username=:username and u.taxpayer.panCard=:pan and u.taxpayer.active=1"),
	@NamedQuery(name = "UserTaxpayer.findPrimaryTaxpayer", query = "SELECT u FROM UserTaxpayer u WHERE u.type='PRIMARY' and u.user.id=:id and u.taxpayer.active=1"),
	@NamedQuery(name = "UserTaxpayer.findSecondaryTaxpayer", query = "SELECT u FROM UserTaxpayer u WHERE u.type='SECONDARY' and u.user.id=:id and u.taxpayer.active=1"),
	@NamedQuery(name = "UserTaxpayer.findIdentityByPan", query = "SELECT u FROM UserTaxpayer u WHERE u.type='PRIMARY' and u.taxpayer.panCard=:pan"),
	@NamedQuery(name = "UserTaxpayer.findUserTaxpayer", query = "SELECT u FROM UserTaxpayer u WHERE u.type='SECONDARY' and u.user.id=:id and u.taxpayer.id=:tid"),
	@NamedQuery(name = "UserTaxpayer.findByTaxpayerId", query = "SELECT u FROM UserTaxpayer u WHERE u.taxpayer.id=:id")})
public class UserTaxpayer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;

	@JsonIgnore
	private String creationIpAddress;
	
	private String type;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;
	
	@JsonIgnore
	private String updationIpAddress;

	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JoinColumn(name="taxpayerId")
	private Taxpayer taxpayer;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="userId")
	private User user;

	public UserTaxpayer() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Taxpayer getTaxpayer() {
		return this.taxpayer;
	}

	public void setTaxpayer(Taxpayer taxpayer) {
		this.taxpayer = taxpayer;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}