package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the cdn_transaction database table.
 * 
 */
@Entity
@Table(name="cdnur_transaction")
@NamedQueries({
@NamedQuery(name="CdnUrTransaction.findAll", query="SELECT c FROM CDNURTransactionEntity c"),
@NamedQuery(name="CDNURTransactionEntity.removeByMonthYear",query="delete from CDNURTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.gstin=:gstin and b2b.returnType =:returnType"),
@NamedQuery(name = "CdnUrTransaction.getByEmbeded", query = "select b from CDNURTransactionEntity b where b.originalCtin=:originalCtin and b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
@NamedQuery(name = "CdnUrTransaction.getByGstinMonthyearReturntype", query = "select b from CDNURTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"), 
@NamedQuery(name = "CdnUrTransaction.getByGstinMonthyearReturntypeCtin", query = "select b from CDNURTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.originalCtin=:ctin") 

})

public class CDNURTransactionEntity  extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fillingStatus;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;

	@Transient
	private String gstinId;
	
	private String monthYear;

	private String returnType;
	
	private String originalCtin;

	private String originalCtinName;

	
	@OneToMany(mappedBy="cdnUrTransaction",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<CDNURDetailEntity> cdnUrDetails;

	public CDNURTransactionEntity() {
	}

	public String getFillingStatus() {
		return this.fillingStatus;
	}

	public void setFillingStatus(String fillingStatus) {
		this.fillingStatus = fillingStatus;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	
	public List<CDNURDetailEntity> getCdnUrDetails() {
		return cdnUrDetails;
	}
	
	public String getOriginalCtin() {
		return originalCtin;
	}

	public void setOriginalCtin(String originalCtin) {
		this.originalCtin = originalCtin;
	}

	public String getOriginalCtinName() {
		return originalCtinName;
	}

	public void setOriginalCtinName(String originalCtinName) {
		this.originalCtinName = originalCtinName;
	}

	public void setCdnUrDetails(List<CDNURDetailEntity> cdnDetails) {
		this.cdnUrDetails = cdnDetails;
	}

	public String getGstinId() {
		return gstinId=gstin.getGstin();
	}

	@Override
	public boolean equals(Object obj) {     
		
	   EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((CDNURTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((CDNURTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((CDNURTransactionEntity) obj).getMonthYear())
			   ;               
	   if(this.returnType.equals("gstr2"))
		   builder.append(this.originalCtin,((CDNURTransactionEntity) obj).getOriginalCtin());
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode());
		return result;
	}

}