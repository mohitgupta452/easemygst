package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the vendor database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name="Vendor.findAll", query="SELECT v FROM Vendor v"),
	@NamedQuery(name="Vendor.checkEmailFlag", query="SELECT v FROM Vendor v WHERE v.active=1 and v.email<>1 and v.emailAddress not in (SELECT u.username FROM User u WHERE u.username <> null)"),
	@NamedQuery(name="Vendor.checkFollowEmailFlag", query="SELECT v FROM Vendor v WHERE v.active=1 and v.email=1 and v.followEmail<>1 and v.emailAt < :timing and v.requestId not in (SELECT u.requestId FROM User u WHERE u.requestId <> null)"),
	@NamedQuery(name="Vendor.findByRequestId", query="SELECT v FROM Vendor v WHERE v.requestId=:requestId"),
	@NamedQuery(name="Vendor.findByGstinId", query="SELECT v FROM Vendor v WHERE v.active=1 and v.gstin=:gstin and v.taxpayer.id=:id"),
	@NamedQuery(name="Vendor.findByPanId", query="SELECT v FROM Vendor v WHERE v.active=1 and v.panNumber=:pan and v.taxpayer.id=:id"),
	@NamedQuery(name="Vendor.findByTaxpayerId", query="SELECT v FROM Vendor v WHERE v.active=1 and v.taxpayer.id=:id"),
	@NamedQuery(name="Vendor.findByTaxpayerIdType", query="SELECT v FROM Vendor v WHERE v.active=1 and v.taxpayer.id=:id and v.type=:type")})
public class Vendor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String address;

	private String businessName;

	private String city;

	private String contactPerson;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	private String emailAddress;

	private String gstin;

	private String mobileNumber;

	private String organisation;

	private String panNumber;

	private String pincode;

	//bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name="state")
	private State stateBean;

	private String type;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private String updationIpAddress;

	@JsonIgnore
	private Timestamp updationTime;

	private String website;
	
	private String classification;
	
	private String gstType;
	
	private String status;
	
	private Byte emgst;
	
	@JsonIgnore
	private Byte active;
	
	@JsonIgnore
	private Byte email;
	
	@JsonIgnore
	private Timestamp emailAt;
	
	@JsonIgnore
	private Byte followEmail;
	
	@JsonIgnore
	private Timestamp followEmailAt;
	
	@JsonIgnore
	private String requestId;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="taxpayerId")
	private Taxpayer taxpayer;
	
	public Vendor() {
	}

	public Taxpayer getTaxpayer() {
		return this.taxpayer;
	}

	public void setTaxpayer(Taxpayer taxpayer) {
		this.taxpayer = taxpayer;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBusinessName() {
		return this.businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getGstin() {
		return this.gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getOrganisation() {
		return this.organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getPanNumber() {
		return this.panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	public State getStateBean() {
		return this.stateBean;
	}

	public void setStateBean(State stateBean) {
		this.stateBean = stateBean;
	}

	
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Byte getEmgst() {
		return emgst;
	}

	public void setEmgst(Byte emgst) {
		this.emgst = emgst;
	}

	public Byte getFollowEmail() {
		return followEmail;
	}

	public void setFollowEmail(Byte followEmail) {
		this.followEmail = followEmail;
	}

	public Timestamp getFollowEmailAt() {
		return followEmailAt;
	}

	public void setFollowEmailAt(Timestamp followEmailAt) {
		this.followEmailAt = followEmailAt;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Byte getEmail() {
		return email;
	}

	public void setEmail(Byte email) {
		this.email = email;
	}

	public Timestamp getEmailAt() {
		return emailAt;
	}

	public void setEmailAt(Timestamp emailAt) {
		this.emailAt = emailAt;
	}

}