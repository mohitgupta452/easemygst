package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.B2BDetailSerializer;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;

/**
 * The persistent class for the b2b_details database table.
 * 
 */
@Entity
@Table(name = "b2b_details")
@NamedQueries({ @NamedQuery(name = "B2bDetailEntity.findAll", query = "SELECT b FROM B2BDetailEntity b"),

		@NamedQuery(name = "B2BDetailEntity.removeById", query = "delete from B2BDetailEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false and b.dataSource=:dataSource"),
		@NamedQuery(name = "B2BDetail.deleteByMonthYear", query = "delete from B2BDetailEntity b where b.b2bTransaction in (select b2b from B2BTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.taxpayerGstin.gstin =:gstin and b2b.returnType =:returnType)and b.isSynced=false and b.isTransit=false and b.dataSource=:dataSource"),
		@NamedQuery(name = "B2BDetail.updateFlagByMonthYear", query = "update  B2BDetailEntity b set b.flags='DELETE' ,b.toBeSync=true where b.b2bTransaction in (select b2b from B2BTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.taxpayerGstin.gstin =:gstin and b2b.returnType =:returnType) and b.isSynced=true and b.dataSource=:dataSource and b.isTransit=false"),
		@NamedQuery(name = "B2BDetailEntity.updateFlagById", query = "update B2BDetailEntity b set b.flags='DELETE' ,b.toBeSync=true where b.id in :ids and b.isSynced=true and b.dataSource=:dataSource and b.isTransit=false"),

		@NamedQuery(name = "B2bDetailEntity.updateFlagsValue", query = "update B2BDetailEntity b set b.type='NEW' where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN "),
		@NamedQuery(name = "B2bDetailEntity.findGstnInvoices", query = "SELECT b from B2BDetailEntity b  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN "),

		@NamedQuery(name = "B2bDetailEntity.findByPagination", query = "SELECT b FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear"),
		@NamedQuery(name = "B2bDetailEntity.findCountByMonthYear", query = "SELECT count(b.id) FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear  and (b.dataSource=:dataSource or (b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN and b.type <> 'NEW')) and b.flags <> 'DELETE' "),
		@NamedQuery(name = "B2bDetailEntity.findCountByMonthYearForSummary", query = "SELECT count(b.id) FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear  and b.dataSource=:dataSource  and (b.flags <> 'DELETE' or b.flags is null )"),
		@NamedQuery(name = "B2bDetailEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),
		@NamedQuery(name = "B2bDetailEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id) FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync"),

		@NamedQuery(name = "B2bDetailEntity.findSummaryByMonthYear", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or  b.flags is null ) and  b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST"),
		
		@NamedQuery(name = "B2bDetailEntity.findCtinSummary", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin.gstin=:gstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or  b.flags is null ) and  b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.GSTN"),

		
		@NamedQuery(name = "B2bDetailEntity.findMismatchedInvoice", query = "SELECT b FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.invoiceNumber=:invoiceNumber and b.dataSource=:dataSource and  b.b2bTransaction.ctin=:ctin"),
		@NamedQuery(name = "B2bDetailEntity.findIdByMonthYear", query = "SELECT b.id FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear"),
		@NamedQuery(name = "B2bDetailEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and (b.flags<> 'DELETE' or b.flags is null) and b.b2bTransaction.fillingStatus='Y' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) group by b.type"),
		@NamedQuery(name = "B2bDetailEntity.findNotSynced", query = "select b from B2BDetailEntity b where b.toBeSync=true and b.b2bTransaction.taxpayerGstin=:gstn and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear in (:monthYear) and (b.dataSource=:dataSource or b.flags='rejected' or b.flags='pending') and b.isTransit=false and b.isError=false"),
		@NamedQuery(name = "B2bDetailEntity.findItcBulk", query = "SELECT b.b2bTransaction.ctin, b.invoiceNumber,b.invoiceDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.b2bTransaction.ctinName FROM B2BDetailEntity b JOIN b.items i  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)"),
		@NamedQuery(name = "B2bDetailEntity.findItcBulkCount", query = "SELECT count(i.id) FROM B2BDetailEntity b JOIN b.items i  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)"),

		@NamedQuery(name = "B2bDetailEntity.findBlankHsnBulk", query = "SELECT b.b2bTransaction.ctin, b.invoiceNumber,b.invoiceDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.b2bTransaction.ctinName,i.unit,i.quantity FROM B2BDetailEntity b JOIN b.items i  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)"),
		@NamedQuery(name = "B2bDetailEntity.countBlankHsnBulk", query = "SELECT count(i.id) FROM B2BDetailEntity b JOIN b.items i  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)"),

		@NamedQuery(name = "B2bDetailEntity.findMissingInvoices", query = "SELECT b from B2BDetailEntity  b   where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.dataSource=:dataSource and b.invoiceNumber not in (SELECT b1.invoiceNumber from B2BDetailEntity b1   where b1.b2bTransaction.taxpayerGstin=:taxPayerGstin and b1.b2bTransaction.returnType=:returnType and b1.b2bTransaction.monthYear=:monthYear and b1.dataSource=:dataSource1)"),
		@NamedQuery(name = "B2bDetailEntity.findErrorInvoices", query = "SELECT b from B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:gstn and b.b2bTransaction.monthYear in (:monthYear) and b.b2bTransaction.returnType=:returnType and b.invoiceNumber=:invno and b.b2bTransaction.ctin=:ctin"),
		@NamedQuery(name = "B2bDetailEntity.findErrorInvoicesByTransitId", query = "SELECT b from B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:gstn and b.b2bTransaction.monthYear in (:monthYear) and b.b2bTransaction.returnType=:returnType  and b.transitId=:transitId and b.isError=false"),
		@NamedQuery(name = "B2BDetailEntity.getFlagsCount", query = "select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from B2BDetailEntity b where  b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.taxpayerGstin=:gstin and b.dataSource= com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST group by b.isSynced,b.toBeSync,b.isTransit,b.isError "),
		@NamedQuery(name = "B2bDetailEntity.findItcSummary", query = "SELECT i.totalEligibleTax, sum(i.itcIgst)+sum(i.itcCgst)+sum(i.itcSgst)+sum(i.itcCess),sum(i.igst)+sum(i.cgst)+sum(i.sgst)+sum(i.cess) FROM B2BDetailEntity b JOIN b.items i where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null) group by i.totalEligibleTax"),
		@NamedQuery(name = "B2bDetailEntity.findErrorInvoicesGstr2", query = "SELECT b from B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:gstn and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.returnType=:returnType and b.invoiceNumber=:invno and b.transitId=:transitId and b.b2bTransaction.ctin=:ctin"),
		@NamedQuery(name = "B2bDetailEntity.findErrorDataByMonthYear", query = "SELECT b FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear  and b.isError=true and  b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <> 'DELETE' or b.flags is null ) "),
		@NamedQuery(name = "B2bDetailEntity.findMismatchedInvNoInvoice", query = "SELECT b from B2BDetailEntity  b   where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.dataSource=:dataSource and b.type=:type  "),

		@NamedQuery(name = "B2bDetailEntity.countNoSimilarData", query = "SELECT count(b.id) from B2BDetailEntity b WHERE b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and b.b2bTransaction.fillingStatus='Y' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) "),
		@NamedQuery(name = "B2bDetailEntity.countSimilarData", query = "SELECT count(b.id) from B2BDetailEntity b WHERE b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_SIMMILAR' and b.b2bTransaction.fillingStatus='Y' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) "),
		@NamedQuery(name = "B2bDetailEntity.countSimmNoSimSummary", query = "SELECT b.subType,COUNT(b.id)  FROM B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags<> 'DELETE' or b.flags is null)   and b.b2bTransaction.fillingStatus='Y' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) group by b.subType"),

		@NamedQuery(name = "B2bDetailEntity.findNoSimilarData", query = "SELECT b from B2BDetailEntity b WHERE b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR'  "),
		@NamedQuery(name = "B2bDetailEntity.findSimilarData", query = "SELECT b from B2BDetailEntity b WHERE b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_SIMMILAR'  "),

		@NamedQuery(name = "B2bDetailEntity.findInvoiceByCtin", query = "SELECT bt from B2BDetailEntity bt WHERE bt.b2bTransaction.taxpayerGstin=:taxPayerGstin and bt.b2bTransaction.returnType=:returnType and bt.b2bTransaction.monthYear=:monthYear and bt.b2bTransaction.ctin=:ctin and bt.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and bt.type='MISSING_OUTWARD' and (bt.flags <> 'DELETE' or bt.flags is null ) and abs(bt.taxAmount-:taxAmount)<=:diff "),
		@NamedQuery(name = "B2bDetailEntity.findInvoiceByCtinCount", query = "SELECT count(bt.id) from B2BDetailEntity bt WHERE bt.b2bTransaction.taxpayerGstin=:taxPayerGstin and bt.b2bTransaction.returnType=:returnType and bt.b2bTransaction.monthYear=:monthYear and bt.b2bTransaction.ctin=:ctin and bt.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and bt.type='MISSING_OUTWARD' and (bt.flags <> 'DELETE' or bt.flags is null ) and abs(bt.taxAmount-:taxAmount)<=:diff  "),

		// temp queriest to handle ctin filter to be made generic later
		// end temp queries to handle ctin filter to be made generic laterRcd
		@NamedQuery(name = "B2bDetailEntity.findAutoMatch", query = "select b.id from B2BDetailEntity b where b.b2bTransaction.taxpayerGstin=:taxpayerGstin and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.fillingStatus='Y' and b.type='MATCH'  and b.toBeSync=true"),

		@NamedQuery(name = "B2bDetailEntity.countNoSimilarDataWithFilter", query = "SELECT count(b.id) from B2BDetailEntity b WHERE b.b2bTransaction.ctin like :ctin and  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and  b.subType='MISSING_INWARD_NON_SIMMILAR'"),
		@NamedQuery(name = "B2bDetailEntity.countSimilarDataWithFilter", query = "SELECT count(b.id) from B2BDetailEntity b WHERE b.b2bTransaction.ctin like :ctin  and b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_SIMMILAR'   "),

		@NamedQuery(name = "B2bDetailEntity.findNoSimilarDataWithFilter", query = "SELECT b from B2BDetailEntity b WHERE b.b2bTransaction.ctin like :ctin and b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR'  "),
		@NamedQuery(name = "B2bDetailEntity.findSimilarDataWithFilter", query = "SELECT b from B2BDetailEntity b WHERE b.b2bTransaction.ctin like :ctin and b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and  b.subType='MISSING_INWARD_SIMMILAR' "),

		// end temp queries to handle ctin filter to be made generic later
		@NamedQuery(name = "B2bDetailEntity.findItcBulkWithFilter", query = "SELECT b.b2bTransaction.ctin, b.invoiceNumber,b.invoiceDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.b2bTransaction.ctinName FROM B2BDetailEntity b JOIN b.items i  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.ctin like :ctin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)"),
		@NamedQuery(name = "B2bDetailEntity.findItcBulkCountWithFilter", query = "SELECT count(i.id) FROM B2BDetailEntity b JOIN b.items i  where b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.ctin like :ctin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and (b.flags <>'DELETE' or b.flags is null)"),

		@NamedQuery(name = "B2BDetailEntity.updateErrorFlagByb2bTransaction", query = "update B2BDetailEntity b set b.isError=false,b.errMsg='',b.flags='' where  b.b2bTransaction=:b2bTransaction and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST and b.isTransit=false and b.isSynced=false"),

		@NamedQuery(name = "B2bDetailEntity.findIdsNoSimilarDataWithFilter", query = "SELECT b.id from B2BDetailEntity b WHERE b.b2bTransaction.ctin like :ctin and b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
		@NamedQuery(name = "B2bDetailEntity.findIdsNoSimilarDataWithOutFilter", query = "SELECT b.id from B2BDetailEntity b WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
		// reject all/pending all missing inward query
		@NamedQuery(name = "B2bDetailEntity.rejectAllMissingInwardWithoutFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
		@NamedQuery(name = "B2bDetailEntity.pendingAllMissingInwardWithoutFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
		@NamedQuery(name = "B2bDetailEntity.rejectAllMissingInwardWithFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin  and b.b2bTransaction.ctin=:ctin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
		@NamedQuery(name = "B2bDetailEntity.pendingAllMissingInwardWithFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.ctin=:ctin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type='MISSING_INWARD' and  (b.flags <> 'DELETE' or b.flags is null ) and b.subType='MISSING_INWARD_NON_SIMMILAR' and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  )  "),
		// reject all/pending all mismatch query
		@NamedQuery(name = "B2bDetailEntity.rejectAllMismatchWithoutFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST  "),
		@NamedQuery(name = "B2bDetailEntity.pendingAllMismatchWithoutFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST "),
		@NamedQuery(name = "B2bDetailEntity.rejectAllMismatchWithFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='REJECTED',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin  and b.b2bTransaction.ctin=:ctin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST "),
		@NamedQuery(name = "B2bDetailEntity.pendingAllMismatchWithFilter", query = "update B2BDetailEntity b set b.previousType=b.type,b.previousFlags=b.flags,b.flags='PENDING',b.source='PORTAL' WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.ctin=:ctin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null ) and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST "),
		// find mismatch invoices by mismatch type
		@NamedQuery(name = "B2bDetailEntity.findAllMismatchInvoiceByTypeWithoutFilter", query = "select b from  B2BDetailEntity b  WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.b2bTransaction.ctin=:ctin  and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST  "),
		@NamedQuery(name = "B2bDetailEntity.findAllMismatchInvoiceByTypeWithFilter", query = "select b from  B2BDetailEntity b  WHERE  b.b2bTransaction.taxpayerGstin=:taxPayerGstin and b.b2bTransaction.returnType=:returnType and b.b2bTransaction.monthYear=:monthYear and b.b2bTransaction.fillingStatus='Y' and b.b2bTransaction.ctin=:ctin   and b.type=:reconcileType and  (b.flags <> 'DELETE' or b.flags is null )  and (b.flags <> 'PENDING' and b.flags<>'REJECTED'  ) and b.dataSource=com.ginni.easemygst.portal.transaction.factory.Transaction$DataSource.EMGST  "),
		@NamedQuery(name="B2bdetailsEntity.findByInvAndCTIN",query="select b from B2BDetailEntity b where b.b2bTransaction.ctin=:ctin and b.b2bTransaction.taxpayerGstin.gstin=:gstin and b.invoiceNumber=:invoiceNumber and b.financialYear=:financialYear and b.b2bTransaction.returnType=:returnType and b.dataSource=:datasource")
})

@JsonSerialize(using = B2BDetailSerializer.class)
public class B2BDetailEntity extends CommonAttributesEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	// @Audited
	private String invoiceNumber;

	// @Audited
	private String flags;

	// @Audited
	private double taxableValue;

	// @Audited
	private double taxAmount;

	private Long aiID;

	// @Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "b2bTransactionId")
	private B2BTransactionEntity b2bTransaction;

	// @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "invoiceId", referencedColumnName = "id")
	private List<Item> items;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean toBeSync;

	private boolean isTransit;

	private boolean isValid;

	// @Audited
	private boolean reverseCharge;

	private String source;

	private String sourceId;

	@Column(name = "syncId")
	private String synchId;

	private String transitId;

	// @Audited
	private String type = "NEW";

	private String subType = "NEW";

	private String financialYear = "2017-2018";

	@Transient
	private String ctin;

	@Transient
	private String ctinName;
	
	@Transient
	private String gstin;
	
	@Transient
	private String taxpayerName;

	@Transient
	private String octin;

	@Transient
	private String octinName;

	// @Audited
	private String invoiceType;

	// @Audited
	private String etin;

	// @Audited
	@Temporal(TemporalType.DATE)
	private Date invoiceDate;

	@Transient
	private TransactionType transactionType;

	// @Audited
	private String pos;

	// @Audited
	@Enumerated(EnumType.STRING)
	private DataSource dataSource = DataSource.EMGST;

	private String cInvoiceNumber;

	private Date cInvoiceDate;

	private double cTaxAmount;

	private JsonNode previousInvoice;

	private transient String error_msg;

	private transient String error_cd;

	// @Audited
	private Boolean isError = false;

	// @Audited
	private String errMsg;

	private String checksum;

	private String previousFlags;

	private String previousType;

	private String originalInvoiceNumber;

	private Date originalInvoiceDate;

	public String getcInvoiceNumber() {
		return cInvoiceNumber;
	}

	public void setcInvoiceNumber(String cInvoiceNumber) {
		this.cInvoiceNumber = cInvoiceNumber;
	}

	public Date getcInvoiceDate() {
		return cInvoiceDate;
	}

	public void setcInvoiceDate(Date cInvoiceDate) {
		this.cInvoiceDate = cInvoiceDate;
	}

	public double getcTaxAmount() {
		return cTaxAmount;
	}

	public void setcTaxAmount(double cTaxAmount) {
		this.cTaxAmount = cTaxAmount;
	}

	public JsonNode getPreviousInvoice() {
		return previousInvoice;
	}

	public void setPreviousInvoice(JsonNode previousInvoice) {
		this.previousInvoice = previousInvoice;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public B2BDetailEntity() {
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public B2BDetailEntity(String id, boolean toBeSync, boolean isSynced, String flags) {

		this.setId(id);
		this.toBeSync = toBeSync;
		this.isSynced = isSynced;
		this.flags = flags;
	}

	public B2BDetailEntity(String id, Date invoiceDate, String invocieNumber, double taxAmount, double taxableValue) {
		this.setId(id);
		this.setInvoiceDate(invoiceDate);
		this.invoiceNumber = invocieNumber;
		this.taxAmount = taxAmount;
		this.taxableValue = taxableValue;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean getReverseCharge() {
		return this.reverseCharge;
	}

	public void setReverseCharge(boolean reverseCharge) {
		this.reverseCharge = reverseCharge;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource portal) {
		this.dataSource = portal;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public B2BTransactionEntity getB2bTransaction() {
		return b2bTransaction;
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public String getOriginalInvoiceNumber() {
		return originalInvoiceNumber;
	}

	public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
		this.originalInvoiceNumber = originalInvoiceNumber;
	}

	public Date getOriginalInvoiceDate() {
		return originalInvoiceDate;
	}

	public void setOriginalInvoiceDate(Date originalInvoiceDate) {
		this.originalInvoiceDate = originalInvoiceDate;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public double getTaxableValue() {
		return taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	public void setB2bTransaction(B2BTransactionEntity b2bTransaction) {
		this.b2bTransaction = b2bTransaction;
		/*
		 * if(!Objects.isNull(b2bTransaction)){ this.ctin=b2bTransaction.getCtin();
		 * this.ctinName=b2bTransaction.getCtinName(); }
		 */
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof B2BDetailEntity) {
			B2BDetailEntity b2bDetail = (B2BDetailEntity) obj;
			if (this == b2bDetail)
				return true;
			if (this.b2bTransaction.equals(b2bTransaction)
					&& this.getInvoiceNumber().equalsIgnoreCase(b2bDetail.getInvoiceNumber()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.b2bTransaction.hashCode() + invoiceNumber.hashCode();
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public String getCtin() {
		if (!Objects.isNull(b2bTransaction))
			ctin = b2bTransaction.getCtin();
		return ctin;
	}

	public void setCtin(String ctin) {
		this.ctin = ctin;
	}

	public String getCtinName() {
		if (!Objects.isNull(b2bTransaction))
			ctinName = b2bTransaction.getCtinName();
		return ctinName;
	}

	public void setCtinName(String ctinName) {
		this.ctinName = ctinName;
	}

	public String getOctin() {
		/*
		 * if(!Objects.isNull(b2bTransaction))
		 * octin=b2bTransaction.getTaxpayerGstin().getGstin();
		 */
		return octin;
	}

	public void setOctin(String octin) {

		this.octin = octin;
	}

	public String getOctinName() {
		/*
		 * if(!Objects.isNull(b2bTransaction))
		 * octinName=b2bTransaction.getTaxpayerGstin().getDisplayName();
		 */
		return octinName;
	}

	public void setOctinName(String octinName) {
		this.octinName = octinName;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	@JsonGetter("rchrg")
	public String getRevCharge() {
		if (this.reverseCharge)
			return "Y";
		return "N";
	}

	public boolean isToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getEtin() {
		return etin;
	}

	public void setEtin(String etin) {
		this.etin = etin;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	/*
	 * @JsonSetter("rchrg") public void setRevCharge(String input) { if
	 * (input.equals("Yes")) this.reverseCharge = true; else this.reverseCharge =
	 * false; }
	 */

	public B2BDetailEntity(String id, String invoiceNumber, B2BTransactionEntity b2bTransaction, boolean reverseCharge,
			Date invoiceDate, double taxableValue, double taxAmount, String flags, String type, String source,
			boolean isMarked, String etin, String invoiceType, boolean isSynced, boolean toBeSync, boolean isTransit,
			boolean isError, Date cInvoiceDate, String cInvoiceNumber, double cTaxAmount, String previousType,
			boolean isAmmendment, String originalInvoiceNumber, Date originalInvoiceDate) {

		this.setId(id);
		;
		this.invoiceNumber = invoiceNumber;
		this.b2bTransaction = b2bTransaction;
		this.reverseCharge = reverseCharge;
		this.invoiceDate = invoiceDate;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.source = source;
		this.isMarked = isMarked;
		this.etin = etin;
		this.invoiceType = invoiceType;
		this.isSynced = isSynced;
		this.toBeSync = toBeSync;
		this.isTransit = isTransit;
		this.isError = isError;
		this.cInvoiceDate = cInvoiceDate;
		this.cInvoiceNumber = cInvoiceNumber;
		this.cTaxAmount = cTaxAmount;
		this.previousType = previousType;
		this.isAmmendment = isAmmendment;
		this.originalInvoiceNumber = originalInvoiceNumber;
		this.originalInvoiceDate = originalInvoiceDate;

	}

	// constructor for b2b gstn data
	public B2BDetailEntity(Long aiID, String id, String invoiceNumber, B2BTransactionEntity b2bTransaction,
			boolean reverseCharge, Date invoiceDate, String flags, String type, String etin, String invoiceType,
			double taxableValue, double taxAmount, String pos, Item item,boolean isAmmendment) {

		this.setId(id);
		;
		this.invoiceNumber = invoiceNumber;
		this.b2bTransaction = b2bTransaction;
		this.reverseCharge = reverseCharge;
		this.invoiceDate = invoiceDate;
		this.flags = flags;
		this.type = type;
		this.etin = etin;
		this.invoiceType = invoiceType;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.pos = pos;
		this.aiID = aiID;
		this.isAmmendment=isAmmendment;
		// this.items=items;
		if (Objects.isNull(items)) {
			this.items = new ArrayList<>();
			this.getItems().add(item);

		} else {
			this.getItems().add(item);
		}

	}

	// constructor for b2ba gstn data
	public B2BDetailEntity(Long aiID, String id, String invoiceNumber, B2BTransactionEntity b2bTransaction,
			boolean reverseCharge, Date invoiceDate, String flags, String type, String etin, String invoiceType,
			double taxableValue, double taxAmount, String pos, Item item, boolean isAmmendment,
			String originalInvoiceNumber, Date originalInvoiceDate) {

		this.setId(id);
		;
		this.invoiceNumber = invoiceNumber;
		this.b2bTransaction = b2bTransaction;
		this.reverseCharge = reverseCharge;
		this.invoiceDate = invoiceDate;
		this.flags = flags;
		this.type = type;
		this.etin = etin;
		this.invoiceType = invoiceType;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.pos = pos;
		this.aiID = aiID;
		this.isAmmendment=isAmmendment;
		this.originalInvoiceDate=originalInvoiceDate;
		this.originalInvoiceNumber=originalInvoiceNumber;
		// this.items=items;
		if (Objects.isNull(items)) {
			this.items = new ArrayList<>();
			this.getItems().add(item);

		} else {
			this.getItems().add(item);
		}

	}

	/*
	 * public B2BDetailEntity(String id, String invoiceNumber ,Item i ) {
	 * 
	 * this.setId(id); this.invoiceNumber=invoiceNumber; if(Objects.isNull(items)){
	 * this.items=new ArrayList<>(); this.getItems().add(i);
	 * 
	 * }else{ this.getItems().add(i); }
	 * 
	 * }
	 */

	// copy constructor
	public B2BDetailEntity(B2BDetailEntity b2b) {
		super();
		this.invoiceNumber = b2b.getInvoiceNumber();
		this.flags = b2b.getFlags();
		this.taxableValue = b2b.getTaxableValue();
		this.taxAmount = b2b.getTaxAmount();
		this.b2bTransaction = b2b.getB2bTransaction();
		List<Item> items = new ArrayList<>();
		for (Item item : b2b.getItems()) {
			Item itm = new Item(item);
			// itm.setId(UUID.randomUUID().toString());
			items.add(itm);
		}
		this.items = items;

		this.isAmmendment = b2b.getIsAmmendment();
		this.isLocked = b2b.getIsLocked();
		this.isMarked = b2b.getIsMarked();
		this.isSynced = b2b.getIsSynced();
		this.toBeSync = b2b.toBeSync;
		this.isTransit = b2b.getIsTransit();
		this.isValid = b2b.getIsValid();
		this.reverseCharge = b2b.getReverseCharge();
		this.source = b2b.getSource();
		this.sourceId = b2b.getSourceId();
		this.synchId = b2b.getSynchId();
		this.transitId = b2b.getTransitId();
		this.type = b2b.getType();
		this.financialYear = b2b.getFinancialYear();
		this.ctin = b2b.getCtin();
		this.ctinName = b2b.getCtinName();
		this.invoiceType = b2b.getInvoiceType();
		this.etin = b2b.getEtin();
		this.invoiceDate = b2b.getInvoiceDate();
		this.pos = b2b.getPos();
		this.dataSource = b2b.getDataSource();
		this.checksum = b2b.getChecksum();
		this.subType = b2b.subType;

	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getPreviousFlags() {
		return previousFlags;
	}

	public void setPreviousFlags(String previousFlags) {
		this.previousFlags = previousFlags;
	}

	public String getPreviousType() {
		return previousType;
	}

	public void setPreviousType(String previousType) {
		this.previousType = previousType;
	}

	public TransactionType getTransactionType() {
		return TransactionType.B2B;
	}

	// @Audited
	public void setId(String id) {
		super.setId(id);
	}

	public Long getAiID() {
		return aiID;
	}

	public void setAiID(Long aiID) {
		this.aiID = aiID;
	}

	public String getGstin() {
		return  b2bTransaction.getTaxpayerGstin().getGstin();
	}

	public String getTaxpayerName() {
		return  b2bTransaction.getTaxpayerGstin().getUsername();
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public void setTaxpayerName(String taxpayerName) {
		this.taxpayerName = taxpayerName;
	}

}