package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

/**
 * The persistent class for the cdn_transaction database table.
 * 
 */
@Entity
@Table(name = "cdn_transaction")
@NamedQueries({ @NamedQuery(name = "CdnTransaction.findAll", query = "SELECT c FROM CDNTransactionEntity c"),
		@NamedQuery(name = "CDNTransactionEntity.removeByMonthYear", query = "delete from CDNTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.gstin=:gstin and b2b.returnType =:returnType"),
		@NamedQuery(name = "CdnTransaction.getByEmbeded", query = "select b from CDNTransactionEntity b where b.originalCtin=:originalCtin and b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
		@NamedQuery(name = "CdnTransaction.getByGstinMonthyearReturntype", query = "select b from CDNTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
		@NamedQuery(name = "CdnTransaction.getByGstinMonthyearReturntypeCtin", query = "select b from CDNTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.originalCtin=:ctin"),

})

public class CDNTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fillingStatus = "N";

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "gstin")
	private TaxpayerGstin gstin;

	@Transient
	private String gstinId;

	private String monthYear;

	private String returnType;

	private String originalCtin;

	private String originalCtinName;

	@OneToMany(mappedBy = "cdnTransaction", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<CDNDetailEntity> cdnDetails;

	private transient String error_msg;

	private transient String error_cd;

	public CDNTransactionEntity() {
	}

	public String getFillingStatus() {
		return this.fillingStatus;
	}

	public void setFillingStatus(String fillingStatus) {
		this.fillingStatus = fillingStatus;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public List<CDNDetailEntity> getCdnDetails() {
		return cdnDetails;
	}

	public String getOriginalCtin() {
		return originalCtin;
	}

	public void setOriginalCtin(String originalCtin) {
		this.originalCtin = originalCtin;
	}

	public String getOriginalCtinName() {
		return originalCtinName;
	}

	public void setOriginalCtinName(String originalCtinName) {
		this.originalCtinName = originalCtinName;
	}

	public void setCdnDetails(List<CDNDetailEntity> cdnDetails) {
		this.cdnDetails = cdnDetails;
	}

	public String getGstinId() {
		return gstinId = gstin.getGstin();
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((CDNTransactionEntity) obj).getGstin())
				.append(this.getReturnType(), ((CDNTransactionEntity) obj).getReturnType())
				.append(this.getMonthYear(), ((CDNTransactionEntity) obj).getMonthYear())
				.append(this.getOriginalCtin(), ((CDNTransactionEntity) obj).getOriginalCtin())

		;
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0
				: gstin.hashCode() + monthYear.hashCode() + returnType.hashCode() + originalCtin.hashCode());
		return result;
	}

}