package com.ginni.easemygst.portal.persistence.convertor;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter(autoApply=true)
public class JsonStringConvertor implements AttributeConverter<JsonNode,String> {
	
	private final static ObjectMapper _MAPPER= new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(JsonNode json) {
		
		try {
			return _MAPPER.writeValueAsString(json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public JsonNode convertToEntityAttribute(String jsonStr) {
		
		if(StringUtils.isEmpty(jsonStr))
			return _MAPPER.createObjectNode();
		try {
			return _MAPPER.readTree(jsonStr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
