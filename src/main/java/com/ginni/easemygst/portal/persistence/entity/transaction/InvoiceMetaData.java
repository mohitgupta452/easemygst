package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

public class InvoiceMetaData implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	@JoinColumn(name="taxpayerGstinId")
	private TaxpayerGstin taxpayerGstin;
	
	private String invoiceNumber;
	
	private String ctin;
	
	private TransactionType transactionType;
	
	private String finacialYear;
	
	private Timestamp updationTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCtin() {
		return ctin;
	}

	public void setCtin(String ctin) {
		this.ctin = ctin;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getFinacialYear() {
		return finacialYear;
	}

	public void setFinacialYear(String finacialYear) {
		this.finacialYear = finacialYear;
	}

	public Timestamp getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}
	
	

}
