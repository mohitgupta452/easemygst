package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.view.serializer.B2bUrDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the at_transaction database table.
 * 
 */
@Entity
@Table(name="b2bur_transaction")
@NamedQueries({
	@NamedQuery(name = "B2bUrTransaction.findAll", query = "SELECT b FROM B2bUrTransactionEntity b"),
	@NamedQuery(name = "B2bUrTransaction.getByGstinMonthyearReturntype", query = "select b from B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "B2bUrTransaction.findByPagination", query = "select b from B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "B2bUrTransaction.findCountByMonthYear", query = "select count(b.id) from B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and  (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "B2bUrTransaction.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	@NamedQuery(name="B2bUrTransactionEntity.removeById",query="delete from B2bUrTransactionEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false"),
	@NamedQuery(name="B2bUrTransactionEntity.deleteByMonthYear",query="delete from B2bUrTransactionEntity b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false and b.returnType =:returnType"),
	@NamedQuery(name="B2bUrTransactionEntity.updateFlagByMonthYear",query="update  B2bUrTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
	@NamedQuery(name="B2bUrTransactionEntity.updateFlagById",query="update B2bUrTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),

	
	@NamedQuery(name = "B2bUrTransaction.findSummaryByMonthYear", query = "select SUM(b.taxableValue),SUM(b.taxAmount) from B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and  (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "B2bUrTransactionEntity.findItcBulk", query = "SELECT b.invoiceNumber ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id FROM B2bUrTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "B2bUrTransactionEntity.findItcBulkCount", query = "SELECT count(i.id) FROM B2bUrTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),

	@NamedQuery(name = "B2bUrTransactionEntity.findBlankHsnBulk", query = "SELECT b.invoiceNumber ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.invoiceDate,b.taxAmount,i.unit,i.quantity FROM B2bUrTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "B2bUrTransactionEntity.countBlankHsnBulk", query = "SELECT count(i.id) FROM B2bUrTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)" ),

	
	@NamedQuery(name = "B2bUrTransactionEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
	@NamedQuery(name="B2bUrTransactionEntity.findNotSynced",query="select b from B2bUrTransactionEntity b where b.toBeSync=true and b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.isTransit=false"),
	@NamedQuery(name = "B2bUrTransaction.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "B2bUrTransactionEntity.findErrorInvoice",query = "Select b from B2bUrTransactionEntity  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear=:monthYear and b.invoiceNumber=:invno "),
	@NamedQuery(name= "B2bUrTransactionEntity.findErrorInvoicesByTransitId",query="SELECT b from B2bUrTransactionEntity b where b.gstin=:gstn and b.monthYear=:monthYear and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="B2bUrTransactionEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id))  from B2bUrTransactionEntity b where b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
	@NamedQuery(name = "B2bUrTransactionEntity.findItcSummary", query = "SELECT i.totalEligibleTax, sum(i.itcIgst)+sum(i.itcCgst)+sum(i.itcSgst)+sum(i.itcCess),sum(i.igst)+sum(i.cgst)+sum(i.sgst)+sum(i.cess) FROM B2bUrTransactionEntity  b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and  (b.flags <>'DELETE' or b.flags is null) group by i.totalEligibleTax"),
	@NamedQuery(name = "B2bUrTransactionEntity.findErrorDataByMonthYear", query = "select b from B2bUrTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),

})
@JsonSerialize(using=B2bUrDetailSerializer.class)
public class B2bUrTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;

	private String monthYear;

	private String returnType;

	private String flags;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;

	private String pos;

	private String stateName;

	private String invoiceNumber;

	private Date invoiceDate;;

	@Column(name="syncId")
	private String synchId;

	private String transitId;

	private String type;

	private Double taxableValue=0.0;
	
	private Double taxAmount=0.0;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	private List<Item> items;
	
	@Enumerated(EnumType.STRING)
	private SupplyType supplyType;
	
	private Boolean toBeSync;
	
	private transient String errorMsg;
	
	private transient String errorCd;
	
	private Boolean isError=false;
	
	private String errMsg;
	
	public B2bUrTransactionEntity() {
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}


	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}


	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}


	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}


	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public SupplyType getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(SupplyType supplyType) {
		this.supplyType = supplyType;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Double getTaxableValue() {
		return taxableValue;
	}

	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}


	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}



	

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}


	

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public TaxpayerGstin getGstin() {
		return gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorCd() {
		return errorCd;
	}

	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}
	
	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((B2bUrTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((B2bUrTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((B2bUrTransactionEntity) obj).getMonthYear())
			   .append(this.getInvoiceNumber(), ((B2bUrTransactionEntity) obj).getInvoiceNumber())
			   ;               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode()+invoiceNumber.hashCode());
		return result;
	}
	
	public B2bUrTransactionEntity( String invoiceNumber,Date invoiceDate, String pos, String stateName, double taxableValue,
			double taxAmount, String flags, String type, String source,String id,SupplyType supplyType,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError) {
		
		this.invoiceNumber=invoiceNumber;
		this.invoiceDate=invoiceDate;
		this.setId(id);
		this.pos=pos;
		this.stateName=stateName;
		this.taxableValue=taxableValue;;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.source = source;
		this.supplyType=supplyType;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;

	}

	public TransactionType getTransactionType() {
		return TransactionType.B2BUR;
	}

}