package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.ImpgDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.transaction.impl.IMPGTransaction.SEZ;


/**
 * The persistent class for the impg_transaction database table.
 * 
 */
@Entity
@Table(name="impg_transaction")
@NamedQueries({
	@NamedQuery(name = "IMPGTransactionEntity.findAll", query = "SELECT b FROM IMPGTransactionEntity b"),
	@NamedQuery(name = "IMPGTransactionEntity.getByGstinMonthyearReturntype", query = "select b from IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "IMPGTransactionEntity.findByPagination", query = "select b from IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "IMPGTransactionEntity.findCountByMonthYear", query = "select count(b.id) from IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and  (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "IMPGTransactionEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	
	@NamedQuery(name="IMPGTransactionEntity.removeById",query="delete from IMPGTransactionEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false"),
	@NamedQuery(name="IMPGTransactionEntity.deleteByMonthYear",query="delete from IMPGTransactionEntity b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false and b.returnType =:returnType"),
	@NamedQuery(name="IMPGTransactionEntity.updateFlagByMonthYear",query="update  IMPGTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
	@NamedQuery(name="IMPGTransactionEntity.updateFlagById",query="update IMPGTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),

	
	@NamedQuery(name = "IMPGTransactionEntity.findSummaryByMonthYear", query = "select SUM(b.taxableValue),SUM(b.taxAmount) from IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and  (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "IMPGTransactionEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
	@NamedQuery(name = "IMPGTransactionEntity.findItcBulk", query = "SELECT b.ctin ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.billOfEntryNumber FROM IMPGTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "IMPGTransactionEntity.findItcBulkCount", query = "SELECT count(i.id) FROM IMPGTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),

	@NamedQuery(name = "IMPGTransactionEntity.findBlankHsnBulk", query = "SELECT b.ctin ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.billOfEntryNumber,b.billOfEntryDate,i.unit,i.quantity FROM IMPGTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "IMPGTransactionEntity.countBlankHsnBulk", query = "SELECT count(i.id) FROM IMPGTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)" ),

	
	@NamedQuery(name="IMPGTransactionEntity.findNotSynced",query="select a from IMPGTransactionEntity a where a.gstin=:gstn and a.monthYear in (:monthYear) and a.toBeSync=true and a.returnType=:returnType and a.isTransit=false and a.isError=false"),
	@NamedQuery(name = "IMPGTransactionEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM IMPGTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync " ),
	@NamedQuery(name = "IMPGTransactionEntity.findErrorInvoice",query = "Select b from IMPGTransactionEntity  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear=:monthYear and b.billOfEntryNumber=:invno "),
	@NamedQuery(name= "IMPGTransactionEntity.findErrorInvoicesByTransitId",query="SELECT b from IMPGTransactionEntity b where b.gstin=:gstn and b.monthYear=:monthYear and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="IMPGTransactionEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO( b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from IMPGTransactionEntity b where b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
	@NamedQuery(name = "IMPGTransactionEntity.findItcSummary", query = "SELECT i.totalEligibleTax, sum(i.itcIgst)+sum(i.itcCgst)+sum(i.itcSgst)+sum(i.itcCess),sum(i.igst)+sum(i.cgst)+sum(i.sgst)+sum(i.cess) FROM IMPGTransactionEntity  b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <>'DELETE' or b.flags is null) group by i.totalEligibleTax"),
	//
	@NamedQuery(name = "IMPGTransactionEntity.findItcBulkWithFilter", query = "SELECT b.ctin ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.billOfEntryNumber FROM IMPGTransactionEntity b JOIN b.items i where b.ctin like :ctin and b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "IMPGTransactionEntity.findItcBulkCountWithFilter", query = "SELECT count(i.id) FROM IMPGTransactionEntity b JOIN b.items i where b.ctin like :ctin and b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),

})
@JsonSerialize(using=ImpgDetailSerializer.class)
public class IMPGTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String ctin;

	@Temporal(TemporalType.DATE)
	private Date billOfEntryDate;

	private String billOfEntryNumber;

	private double billOfEntryValue;

	private String flags;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;

	@JoinColumn(name="invoiceId", referencedColumnName="id")
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Item> items;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSubmit;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String monthYear;

	private String portCode;

	private String returnType;

	private String source;

	private String sourceId;

	private String synchId;

	private double taxableValue;

	private double taxAmount;

	private String transitId;

	private String type;
	
	private Boolean toBeSync;
	
	private transient String errorMsg;
	
	private transient String errorCd;
	
	private String errMsg;
	
	private Boolean isError=false;
	
	@Enumerated(EnumType.STRING)
	private SEZ sez;

	public IMPGTransactionEntity() {
	}
	public String getCtin() {
		return ctin;
	}

	public void setCtin(String ctin) {
		this.ctin = ctin;
	}
	
	public SEZ getSez() {
		return sez;
	}
	public void setSez(SEZ sez) {
		this.sez = sez;
	}
	public Date getBillOfEntryDate() {
		return this.billOfEntryDate;
	}

	public void setBillOfEntryDate(Date billOfEntryDate) {
		this.billOfEntryDate = billOfEntryDate;
	}

	public String getBillOfEntryNumber() {
		return this.billOfEntryNumber;
	}

	public void setBillOfEntryNumber(String billOfEntryNumber) {
		this.billOfEntryNumber = billOfEntryNumber;
	}

	public double getBillOfEntryValue() {
		return this.billOfEntryValue;
	}

	public void setBillOfEntryValue(double billOfEntryValue) {
		this.billOfEntryValue = billOfEntryValue;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getPortCode() {
		return this.portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public Boolean getToBeSync() {
		return toBeSync;
	}
	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getErrorCd() {
		return errorCd;
	}
	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}
	
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder()
			   .append(this.getGstin(), ((IMPGTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((IMPGTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((IMPGTransactionEntity) obj).getMonthYear())
			   .append(this.getBillOfEntryNumber(), ((IMPGTransactionEntity) obj).getBillOfEntryNumber())
			   ;               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode()+billOfEntryNumber.hashCode());
		return result;
	}
	
	
	public IMPGTransactionEntity(String billOfEntryNumber, Date billOfEntryDate,  double taxableValue,
			double taxAmount, String flags, String type, String source,String id,SEZ sez,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError) {
		this.billOfEntryNumber=billOfEntryNumber;
		this.billOfEntryDate=billOfEntryDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.source = source;
		this.setId(id);
		this.sez=sez;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;

	}
	public TransactionType getTransactionType() {
		return TransactionType.IMPG;
	}
	
	
}