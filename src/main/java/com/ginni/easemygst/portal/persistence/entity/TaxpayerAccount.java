package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the taxpayer_account database table.
 * 
 */
@Entity
@Table(name="taxpayer_account")
@NamedQuery(name="TaxpayerAccount.findAll", query="SELECT t FROM TaxpayerAccount t")
public class TaxpayerAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	private String credit;

	private String debit;

	@Lob
	private String description;

	private int referenceId;

	private String tag;

	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="taxpayerId")
	private Taxpayer taxpayer;

	private String transactionId;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private String updationIpAddress;

	@JsonIgnore
	private Timestamp updationTime;

	public TaxpayerAccount() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getCredit() {
		return this.credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getDebit() {
		return this.debit;
	}

	public void setDebit(String debit) {
		this.debit = debit;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Taxpayer getTaxpayer() {
		return this.taxpayer;
	}

	public void setTaxpayer(Taxpayer taxpayer) {
		this.taxpayer = taxpayer;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

}