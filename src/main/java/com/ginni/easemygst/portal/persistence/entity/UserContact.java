package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the user_contact database table.
 * 
 */
@Entity
@Table(name="user_contact")
@NamedQueries({ @NamedQuery(name="UserContact.findAll", query="SELECT u FROM UserContact u"),
	@NamedQuery(name="UserContact.findById", query="SELECT u FROM UserContact u WHERE u.user.id=:id")})
public class UserContact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Lob
	private String address;

	private String city;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;

	private String emailAddress;

	private Byte emailAuth;

	@Lob
	private String image;

	private Byte mobileAuth;

	private String mobileNumber;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;
	
	@JsonIgnore
	private String creationIpAddress;
	
	@JsonIgnore
	private String updationIpAddress;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="userId")
	private User user;

	//bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name="state")
	private State stateBean;

	public UserContact() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getEmailAddress() {
		if(!StringUtils.isEmpty(this.emailAddress)) return this.emailAddress.toLowerCase();
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		if(!StringUtils.isEmpty(emailAddress)) emailAddress = emailAddress.toLowerCase();
		this.emailAddress = emailAddress;
	}

	public Byte getEmailAuth() {
		return this.emailAuth;
	}

	public void setEmailAuth(Byte emailAuth) {
		this.emailAuth = emailAuth;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Byte getMobileAuth() {
		return this.mobileAuth;
	}

	public void setMobileAuth(Byte mobileAuth) {
		this.mobileAuth = mobileAuth;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public State getStateBean() {
		return this.stateBean;
	}

	public void setStateBean(State stateBean) {
		this.stateBean = stateBean;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

}