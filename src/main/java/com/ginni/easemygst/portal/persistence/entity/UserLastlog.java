package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


/**
 * The persistent class for the user_lastlogs database table.
 * 
 */
@Entity
@Table(name="user_lastlogs")
@NamedQueries({ @NamedQuery(name="UserLastlog.findAll", query="SELECT u FROM UserLastlog u"),
	@NamedQuery(name="UserLastlog.findById", query="SELECT u FROM UserLastlog u WHERE u.user.id=:id"),
})
public @Data class UserLastlog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String _CHECK_LAST_LOG="UserLastlog.checkLastLogin";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private LocalDateTime lastLogin;

	private String lastLoginIpAddress;

	private String lastNotificationIpAddress;

	private Timestamp lastNotificationTime;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="userId")
	private User user;
		
	public UserLastlog() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastLoginIpAddress() {
		return this.lastLoginIpAddress;
	}

	public void setLastLoginIpAddress(String lastLoginIpAddress) {
		this.lastLoginIpAddress = lastLoginIpAddress;
	}

	public String getLastNotificationIpAddress() {
		return this.lastNotificationIpAddress;
	}

	public void setLastNotificationIpAddress(String lastNotificationIpAddress) {
		this.lastNotificationIpAddress = lastNotificationIpAddress;
	}

	public Timestamp getLastNotificationTime() {
		return this.lastNotificationTime;
	}

	public void setLastNotificationTime(Timestamp lastNotificationTime) {
		this.lastNotificationTime = lastNotificationTime;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}