package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the data_upload_info database table.
 * 
 */
@Entity
@Table(name="data_upload_info")
@NamedQueries({ @NamedQuery(name="DataUploadInfo.findAll", query="SELECT d FROM DataUploadInfo d"),
	@NamedQuery(name="DataUploadInfo.findByStatus", query="SELECT d FROM DataUploadInfo d WHERE d.status=:status"),
	@NamedQuery(name="DataUploadInfo.findByMachineKey", query="SELECT d FROM DataUploadInfo d WHERE d.machineKey IN :key and d.serialNo IN :sno ORDER BY d.creationTime DESC")})
public class DataUploadInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private Byte matched;
	
	private Byte isPrimary;

	private String encKey;
	
	private String encIv;
	
	private String machineKey;
	
	private String serialNo;
	
	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	private Timestamp creationTime;
	
	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private String updationIpAddress;

	@JsonIgnore
	private Timestamp updationTime;

	private String fileFormat;
	
	private String fileName;

	private String gstin;

	private String monthYear;

	private String returnType;

	private String status;

	private String transaction;

	@Lob
	private String url;

	//bi-directional many-to-one association to User
	@JsonIgnore
	@JoinColumn(name="userId")
	@OneToOne
	private User user;

	@ManyToOne
	@JoinColumn(name="taxpayerGstinId")
	private TaxpayerGstin taxpayerGstin;
	
	public DataUploadInfo() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Byte getMatched() {
		return matched;
	}

	public void setMatched(Byte matched) {
		this.matched = matched;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getCreationTime() {
		return new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(this.creationTime);
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEncKey() {
		return encKey;
	}

	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}

	public String getGstin() {
		return this.gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransaction() {
		return this.transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getEncIv() {
		return encIv;
	}

	public void setEncIv(String encIv) {
		this.encIv = encIv;
	}

	public String getMachineKey() {
		return machineKey;
	}

	public void setMachineKey(String machineKey) {
		this.machineKey = machineKey;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public Byte getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Byte isPrimary) {
		this.isPrimary = isPrimary;
	}

}