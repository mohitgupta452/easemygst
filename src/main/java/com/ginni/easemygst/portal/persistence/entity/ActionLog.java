package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;

import lombok.Data;

import com.ginni.easemygst.portal.business.service.NotificationService.NotificationCategory;

/**
 * The persistent class for the action_log database table.
 * 
 */
@Entity
@Table(name="action_log")
@NamedQueries({
@NamedQuery(name="ActionLog.findAll", query="SELECT a FROM ActionLog a"),
@NamedQuery(name="ActionLog.findByOrganizationIdAndCategory", query="SELECT a FROM ActionLog a where a.organizationId=:orgId and a.category in :categories and a.isSeen in :isSeens and a.isActive=true order by a.id desc "),
@NamedQuery(name="ActionLog.findByOrganizationIdAndCategoryAll", query="SELECT a FROM ActionLog a where a.organizationId=:orgId and a.category in :categories  order by a.id desc "),
@NamedQuery(name="ActionLog.findByOrganizationIdAndCategoryAllCount", query="SELECT count(a.id) FROM ActionLog a where a.organizationId=:orgId and a.category in :categories  order by a.id desc "),
@NamedQuery(name="ActionLog.findUnseenCountByOrganizationIdAndCategory", query="SELECT count(a.id) FROM ActionLog a where a.organizationId=:orgId and a.category in :categories and a.isSeen in :isSeens")

})
public @Data class ActionLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id; 
	
	@Enumerated(EnumType.STRING)
	private UserAction action;

	@Temporal(TemporalType.TIMESTAMP)
	private Date actionTime;

	private String gstin;

	private String message;

	private int organizationId;

	private String userId;
	
	private boolean isSeen=false;
	
	private String category=NotificationCategory.EMGST_NOTIFICATION.toString();
	
	private String iconCategory;
	
	private boolean isActive=true;
	
	public ActionLog() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserAction getAction() {
		return this.action;
	}

	public void setAction(UserAction action) {
		this.action = action;
	}

	public Date getActionTime() {
		return this.actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public String getGstin() {
		return this.gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isSeen() {
		return isSeen;
	}

	public void setSeen(boolean isSeen) {
		this.isSeen = isSeen;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIconCategory() {
		return iconCategory;
	}

	public void setIconCategory(String iconCategory) {
		this.iconCategory = iconCategory;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	

}