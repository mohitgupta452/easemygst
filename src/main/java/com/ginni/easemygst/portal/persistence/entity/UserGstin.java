package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the user_gstin database table.
 * 
 */
@Entity
@Table(name="user_gstin")
@NamedQueries({ @NamedQuery(name="UserGstin.findAll", query="SELECT u FROM UserGstin u"),
	@NamedQuery(name="UserGstin.findById", query="SELECT u FROM UserGstin u WHERE u.id=:id"),
	@NamedQuery(name = "UserGstin.findIdentityByUsername", query = "SELECT u FROM UserGstin u WHERE u.user.username=:username and u.taxpayerGstin.gstin=:gstin and u.taxpayerGstin.active=1"),
	@NamedQuery(name = "UserGstin.findIdentityById", query = "SELECT u FROM UserGstin u WHERE u.user.id=:id and u.taxpayerGstin.active=1"),
	@NamedQuery(name = "UserGstin.findUserGstin", query = "SELECT u FROM UserGstin u WHERE u.user.id=:id and u.taxpayerGstin.id=:gid and u.gstReturnBean.id=:rid"),
	@NamedQuery(name = "UserGstin.findUsertype", query = "SELECT u FROM UserGstin u WHERE u.user.id=:uid and u.taxpayerGstin.gstin=:gstin and u.gstReturnBean.name=:rtype"),
	@NamedQuery(name = "UserGstin.findtypeprimary", query = "SELECT u FROM UserGstin u WHERE u.user.id=:uid and u.taxpayerGstin.gstin=:gstin and  u.gstReturnBean=null"),
	@NamedQuery(name = "UserGstin.findIdentityByUserPan", query = "select ug from UserGstin ug where ug.user.id=:id and ug.taxpayerGstin.taxpayer.panCard=:pan and ug.taxpayerGstin.active=1"),
	@NamedQuery(name = "UserGstin.findByTaxpayerId", query = "select ug from UserGstin ug where ug.taxpayerGstin.id=:id"),
	@NamedQuery(name = "UserGstin.findByUserOrg", query = "select ug from UserGstin ug where ug.user.id=:userId and ug.organisation.id=:organisationId order by ug.gstReturnBean.id asc"),
	@NamedQuery(name = "UserGstin.findByOrg", query = "select ug from UserGstin ug where ug.organisation.id=:organisationId"),
	@NamedQuery(name = "UserGstin.findDistinctByUser", query = "select DISTINCT ug.organisation from UserGstin ug where ug.user.id=:userId"),
	@NamedQuery(name = "UserGstin.findByUserTaxpayerId", query = "select ug from UserGstin ug where ug.taxpayerGstin.gstin=:gstin and ug.user.username=:username")})
public class UserGstin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;

	@JsonIgnore
	private String creationIpAddress;
	
	private String type;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;
	
	@JsonIgnore
	private String updationIpAddress;

	//bi-directional many-to-one association to TaxpayerGstin
	@ManyToOne
	@JoinColumn(name="gstinId")
	private TaxpayerGstin taxpayerGstin;

	//bi-directional many-to-one association to GstReturn
	@ManyToOne
	@JoinColumn(name="gstReturn")
	private GstReturn gstReturnBean;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="userId")
	private User user;
	
	//bi-directional many-to-one association to User
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="organisationId")
	private Organisation organisation;

	public UserGstin() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return this.taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public GstReturn getGstReturnBean() {
		return this.gstReturnBean;
	}

	public void setGstReturnBean(GstReturn gstReturnBean) {
		this.gstReturnBean = gstReturnBean;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

}