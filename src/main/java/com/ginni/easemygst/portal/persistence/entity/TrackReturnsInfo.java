package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import lombok.Data;


@Entity
@Table(name="track_returns_info")

@javax.persistence.NamedQueries({
	@NamedQuery(name="TrackReturnsInfo.findUnProcessedByGstin",query="select t from TrackReturnsInfo t where t.taxpayerGstin=:taxpayerGstin and t.isProcessed=false"),
	@NamedQuery(name="TrackReturnsInfo.findAllAuthRecords",query="select t from TrackReturnsInfo t where t.isProcessed=false and t.isAuthenticated=true")

})
public @Data class TrackReturnsInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="taxpayerGstinId")
    private TaxpayerGstin taxpayerGstin;
	
	private String monthYear;
	
	private String returnType; 
	
	private boolean isProcessed;
	
	private boolean isAuthenticated;
	
	public TrackReturnsInfo() {
		
	}
	
	public TrackReturnsInfo(int id,String gstin,String monthYear,String returnType) {
		
		this.id=id;
		this.taxpayerGstin=new TaxpayerGstin();
		this.monthYear=monthYear;
		this.returnType= returnType;
		this.taxpayerGstin.setGstin(gstin);
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("gstin",taxpayerGstin.getGstin())
				.append("month-year",monthYear).append("return-Type",returnType)
				.toString();
	}
}
