package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

/**
 * The persistent class for the b2b_transaction database table.
 * 
 */
@Entity
@Table(name = "b2b_transaction")
@NamedQueries({ @NamedQuery(name = "B2bTransaction.findAll", query = "SELECT b FROM B2BTransactionEntity b"),
		@NamedQuery(name = "B2BTransactionEntity.removeByMonthYear", query = "delete from B2BTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.taxpayerGstin=:gstin and b2b.returnType =:returnType"),
		@NamedQuery(name = "B2bTransaction.getByEmbeded", query = "select b from B2BTransactionEntity b where b.ctin=:ctin and b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
		@NamedQuery(name = "B2bTransaction.getByGstinMonthyearReturntype", query = "select b from B2BTransactionEntity b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
		@NamedQuery(name = "B2bTransaction.getByGstinMonthyearReturntypeCtin", query = "select b from B2BTransactionEntity b where b.taxpayerGstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.ctin=:ctin"),

})
public class B2BTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fillingStatus = "N";

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "taxpayerGstinId")
	private TaxpayerGstin taxpayerGstin;

	// @NotNull(message="Ctin must not be null")
	private String ctin;

	private String ctinName;

	private boolean isSubmit;

	private transient String error_msg;

	private transient String error_cd;

	// @NotNull(message="Month-Year must not be null")
	private String monthYear;

	// @NotNull(message="Return type must not be null")
	private String returnType;

	private Double taxableValue;

	private Double taxAmount;

	@JsonIgnore
	@OneToMany(mappedBy = "b2bTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<B2BDetailEntity> b2bDetails;

	private String financialYear;

	public B2BTransactionEntity() {
	}

	public String getFillingStatus() {
		return this.fillingStatus;
	}

	public void setFillingStatus(String fillingStatus) {
		this.fillingStatus = fillingStatus;
	}

	public String getCtinName() {
		return ctinName;
	}

	public void setCtinName(String ctinName) {
		this.ctinName = ctinName;
	}

	public Double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public Double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public List<B2BDetailEntity> getB2bDetails() {
		return b2bDetails;
	}

	public void setB2bDetails(List<B2BDetailEntity> b2bDetails) {
		this.b2bDetails = b2bDetails;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getCtin() {
		return ctin;
	}

	public void setCtin(String ctin) {
		this.ctin = ctin;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof B2BTransactionEntity) {
			B2BTransactionEntity b2bTransaction = (B2BTransactionEntity) object;
			if (this == b2bTransaction)
				return true;
			if (this.taxpayerGstin != null && this.ctin != null && this.returnType != null && this.monthYear != null
					&& this.taxpayerGstin.equals(b2bTransaction.getTaxpayerGstin())
					&& this.getMonthYear().equals(b2bTransaction.getMonthYear())
					&& this.getCtin().equals(b2bTransaction.getCtin())
					&& this.getReturnType().equals(b2bTransaction.getReturnType()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return java.util.Objects.hash(this.taxpayerGstin.hashCode(), this.ctin.hashCode(), this.returnType.hashCode(),
				this.monthYear.hashCode());
	}

	public boolean isSubmit() {
		return isSubmit;
	}

	public void setSubmit(boolean isSubmit) {
		this.isSubmit = isSubmit;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

}