package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gstn_api_log database table.
 * 
 */
@Entity
@Table(name="gstn_api_log")
@NamedQuery(name="GstnApiLog.findAll", query="SELECT g FROM GstnApiLog g")
public class GstnApiLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String function;

	private String module;

	private String monthYear;

	@Lob
	private String requestPayload;

	@Lob
	private String responsePayload;

	private String status;

	private String url;

	//bi-directional many-to-one association to TaxpayerGstin
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin taxpayerGstin;
		
	public GstnApiLog() {
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return this.taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getFunction() {
		return this.function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getRequestPayload() {
		return this.requestPayload;
	}

	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}

	public String getResponsePayload() {
		return this.responsePayload;
	}

	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}