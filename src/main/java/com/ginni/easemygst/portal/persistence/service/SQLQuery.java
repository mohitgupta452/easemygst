package com.ginni.easemygst.portal.persistence.service;

public interface SQLQuery {

	String SQL_VENDOR_FOLLOW_EMAIL = "SELECT v FROM Vendor v WHERE v.active=1 and v.email=1 and v.followEmail<>1 and "
			+ "v.requestId not in (SELECT u.requestId FROM User u WHERE u.requestId <> null)";

	String SQL_VENDOR_ADD = "INSERT INTO VENDOR (`taxpayerId`,`businessName`,`type`,`gstin`,`contactPerson`,"
			+ "`emailAddress`,`mobileNumber`,`website`,`organisation`,`panNumber`,`classification`,`gstType`,`status`,"
			+ "`emgst`,`active`,`address`,`state`,`city`,`pincode`,`email`,`emailAt`,`followEmail`,`followEmailAt`,"
			+ "`requestId`,`creationTime`,`creationIpAddress`,`createdBy`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

}
