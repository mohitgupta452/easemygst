package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the exp_transaction database table.
 * 
 */
@Entity
@Table(name="exp_transaction")
@NamedQueries({
	@NamedQuery(name="ExpTransaction.findAll", query="SELECT e FROM EXPTransactionEntity e"),
	@NamedQuery(name="EXPTransactionEntity.removeByMonthYear",query="delete from EXPTransactionEntity b2b where b2b.monthYear =:monthYear and b2b.gstin=:gstin and b2b.returnType =:returnType"),
	@NamedQuery(name = "ExpTransaction.getByGstinMonthyearReturntype", query = "select b from EXPTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
})
public class EXPTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fillingStatus;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="taxpayerGstinId")
	private TaxpayerGstin gstin;

	private String monthYear;

	private String returnType;
	
    
	@OneToMany(mappedBy="expTransaction",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<ExpDetail> expDetails;
	
	private Double taxableValue=0.0;

	private Double taxAmount=0.0;
	
	private transient String error_msg;
	
	private transient String error_cd;

	public EXPTransactionEntity() {
	}

	public String getFillingStatus() {
		return this.fillingStatus;
	}

	public void setFillingStatus(String fillingStatus) {
		this.fillingStatus = fillingStatus;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public List<ExpDetail> getExpDetails() {
		return expDetails;
	}

	public void setExpDetails(List<ExpDetail> expDetails) {
		this.expDetails = expDetails;
	}

	public Double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getError_msg() {
		return error_msg;
	}
	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_cd() {
		return error_cd;
	}

	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}

	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((EXPTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((EXPTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((EXPTransactionEntity) obj).getMonthYear())
			   ;               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode());
		return result;
	}
	
	

}