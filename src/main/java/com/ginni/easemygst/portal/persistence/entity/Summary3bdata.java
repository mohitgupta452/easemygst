package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the summary_3bdata database table.
 * 
 */
@Entity
@Table(name="summary_3bdata")
@NamedQueries({
@NamedQuery(name="Summary3bdata.findAll", query="SELECT s FROM Summary3bdata s"),
@NamedQuery(name="Summary3bdata.findBYtype", query="SELECT s.summarydata FROM Summary3bdata s where s.summaryType=:summaryType and s.monthYear=:monthYear and s.gstinId=:gstinId"),
@NamedQuery(name="Summary3bdata.deleteByGstinMonthYear",query="delete from Summary3bdata s where s.gstinId=:gstinId and s.monthYear=:monthYear"),
@NamedQuery(name="Summary3bdata.finddata",query="SELECT s FROM Summary3bdata s where s.monthYear=:monthYear and s.gstinId=:gstinId and s.summaryType=:summaryType "),

})
public class Summary3bdata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDateTime;

	private int gstinId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String monthYear;

	@Column(name="summary_type")
	private String summaryType;

	@Lob
	private String summarydata;

	public Summary3bdata() {
	}

	public Date getCreationDateTime() {
		return this.creationDateTime;
	}

	public void setCreationDateTime(Date creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public int getGstinId() {
		return this.gstinId;
	}

	public void setGstinId(int gstinId) {
		this.gstinId = gstinId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getSummaryType() {
		return this.summaryType;
	}

	public void setSummaryType(String summaryType) {
		this.summaryType = summaryType;
	}

	public String getSummarydata() {
		return this.summarydata;
	}

	public void setSummarydata(String summarydata) {
		this.summarydata = summarydata;
	}
	

	@PrePersist
	public void prepersist(){
		
		this.creationDateTime=new Timestamp(new Date().getTime());	
		
	}

}