package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.B2CLDetailSerializer;


/**
 * The persistent class for the b2cl_details database table.
 * 
 */
@Entity
@Table(name="b2cl_details")
@NamedQueries({
	@NamedQuery(name = "B2clDetailEntity.findByPagination", query = "SELECT b FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear"),
	@NamedQuery(name = "B2clDetailEntity.findCountByMonthYear", query = "SELECT count(b.id) FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "B2clDetailEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	
	@NamedQuery(name="B2CLDetailEntity.removeById",query="delete from B2CLDetailEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false"),
	@NamedQuery(name="B2CLDetailEntity.deleteByMonthYear",query="delete from B2CLDetailEntity b where b.b2clTransaction in (select B2CL from B2CLTransactionEntity B2CL where B2CL.monthYear =:monthYear and B2CL.gstin.gstin =:gstin and B2CL.returnType =:returnType) and b.isSynced=false and b.isTransit=false"),
	@NamedQuery(name="B2CLDetailEntity.updateFlagByMonthYear",query="update  B2CLDetailEntity b set b.flags='DELETE',b.toBeSync=true where b.b2clTransaction in (select B2CL from B2CLTransactionEntity B2CL where B2CL.monthYear =:monthYear and B2CL.gstin.gstin =:gstin and B2CL.returnType =:returnType) and b.isSynced=true and b.isTransit=false"),
	@NamedQuery(name="B2CLDetailEntity.updateFlagById",query="update B2CLDetailEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),

	@NamedQuery(name = "B2clDetailEntity.findSummaryByMonthYear", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) "),
	@NamedQuery(name = "B2clDetailEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear group by b.type"),

	@NamedQuery(name="B2CLDetailEntity.findNotSynced",query="select b from B2CLDetailEntity b where b.toBeSync=true and b.b2clTransaction.gstin=:gstn and b.b2clTransaction.monthYear in (:monthYear) and b.b2clTransaction.returnType=:returnType and b.isTransit=false and b.isError=false"),
	@NamedQuery(name = "B2clDetailEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "B2clDetailEntity.findErrorInvoice",query = "Select b from B2CLDetailEntity  b where b.b2clTransaction.gstin=:gstn and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear in (:monthYear) and b.invoiceNumber=:invno "),
	@NamedQuery(name= "B2clDetailEntity.findErrorInvoicesByTransitId",query="SELECT b from B2CLDetailEntity b where b.b2clTransaction.gstin=:gstn and b.b2clTransaction.monthYear in (:monthYear) and b.b2clTransaction.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="B2CLDetailEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from B2CLDetailEntity b where b.b2clTransaction.monthYear=:monthYear and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
	@NamedQuery(name = "B2clDetailEntity.findErrorDataByMonthYear", query = "SELECT b FROM B2CLDetailEntity b where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear  and b.isError=true and  b.flags <> 'DELETE' " ),
	@NamedQuery(name = "B2clDetailEntity.findBlankHsnBulk", query = "SELECT b.invoiceNumber, b.invoiceNumber,b.invoiceDate ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,i.unit,i.quantity FROM B2CLDetailEntity b JOIN b.items i  where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='')  and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "B2clDetailEntity.countBlankHsnBulk", query = "SELECT  count(i.id) FROM B2CLDetailEntity b JOIN b.items i where b.b2clTransaction.gstin=:taxPayerGstin and b.b2clTransaction.returnType=:returnType and b.b2clTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null) " ),

	
	
})
@JsonSerialize(using=B2CLDetailSerializer.class)
public class B2CLDetailEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String pos;
	
	private String invoiceNumber;
	
	private Date invoiceDate;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	private List<Item> items;

	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="b2clTransactionId")
	private B2CLTransactionEntity b2clTransaction;
	

	private String flags;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;
	
	private transient String error_msg;
	
	private transient String error_cd;

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	private String stateName;
	
	private double taxableValue;

	private double taxAmount;

	@Column(name="syncId")
	private String synchId;

	private String transitId;

	private String type;
	
	private String etin;
	
	private Boolean toBeSync;
	
	@Transient
	private TransactionType transactionType;
	
	private Boolean isError=false;
	
	private String errMsg;
	
       private String originalInvoiceNumber ;
	
	private Date originalInvoiceDate;
	
	
	public B2CLDetailEntity() {
	}
	public String getPos() {
		return pos;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getEtin() {
		return etin;
	}

	public void setEtin(String etin) {
		this.etin = etin;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}


	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public B2CLTransactionEntity getB2clTransaction() {
		return b2clTransaction;
	}

	public void setB2clTransaction(B2CLTransactionEntity b2clTransaction) {
		this.b2clTransaction = b2clTransaction;
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	
	public String getError_msg() {
		return error_msg;
	}
	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}
	public String getError_cd() {
		return error_cd;
	}
	public void setError_cd(String error_cd) {
		this.error_cd = error_cd;
	}
	
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	public String getOriginalInvoiceNumber() {
		return originalInvoiceNumber;
	}

	public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
		this.originalInvoiceNumber = originalInvoiceNumber;
	}

	public Date getOriginalInvoiceDate() {
		return originalInvoiceDate;
	}

	public void setOriginalInvoiceDate(Date originalInvoiceDate) {
		this.originalInvoiceDate = originalInvoiceDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.getInvoiceNumber(), ((B2CLDetailEntity) obj).getInvoiceNumber())
				.append(this.getB2clTransaction(), ((B2CLDetailEntity) obj).getB2clTransaction())
				;
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pos == null) ? 0 : pos.hashCode()+b2clTransaction.hashCode());
		return result;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public B2CLDetailEntity(String id, String invoiceNumber, B2CLTransactionEntity b2bTransaction, Date invoiceDate,
			double taxableValue, double taxAmount, String flags, String type, String source, String etin,String pos,
			String stateName,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError,
			boolean isAmmendment,String originalInvoiceNumber,Date originalInvoiceDate
			) {
		this.setId(id);
		this.invoiceNumber = invoiceNumber;
		this.b2clTransaction = b2bTransaction;
		this.invoiceDate = invoiceDate;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.source = source;
		this.etin = etin;
		this.pos=pos;
		this.stateName=stateName;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
		this.isAmmendment=isAmmendment;
		this.originalInvoiceNumber=originalInvoiceNumber;
		this.originalInvoiceDate=originalInvoiceDate;

	}
	public Boolean getToBeSync() {
		return toBeSync;
	}
	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}
	public TransactionType getTransactionType() {
		return TransactionType.B2CL;
	}
	
	public B2CLDetailEntity(String id, String invoiceNumber, B2CLTransactionEntity b2bTransaction, Date invoiceDate,
			double taxableValue, double taxAmount, String flags, String type,  String etin,String pos,String stateName,Item item,boolean isAmendment) {
		this.setId(id);
		this.invoiceNumber = invoiceNumber;
		this.b2clTransaction = b2bTransaction;
		this.invoiceDate = invoiceDate;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.etin = etin;
		this.pos=pos;
		this.stateName=stateName;
		this.isAmmendment=isAmendment;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}

	}
	
	public B2CLDetailEntity(String id, String invoiceNumber, B2CLTransactionEntity b2bTransaction, Date invoiceDate,
			double taxableValue, double taxAmount, String flags, String type,  String etin,String pos,String stateName,Item item,
			boolean isAmendment,String originalInvoiceNumber,Date originalInvoicedate) {
		this.setId(id);
		this.invoiceNumber = invoiceNumber;
		this.b2clTransaction = b2bTransaction;
		this.invoiceDate = invoiceDate;
		this.taxableValue = taxableValue;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.etin = etin;
		this.pos=pos;
		this.stateName=stateName;
		this.isAmmendment=isAmendment;
		this.originalInvoiceNumber=originalInvoiceNumber;
		this.originalInvoiceDate=originalInvoicedate;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}

	}
	
}