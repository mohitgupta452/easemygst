package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;


/**
 * The persistent class for the public notification database table.
 * 
 */
@Entity
@NamedQueries({
@NamedQuery(name="Publicnotification.findAll", query="SELECT p FROM Publicnotification p order by p.id desc"),
@NamedQuery(name="Publicnotification.findBycategory", query="SELECT p FROM Publicnotification p where p.category=:category order by p.id desc"),
@NamedQuery(name="Publicnotification.countAll", query="SELECT count(p.id) FROM Publicnotification p order by p.id desc"),
@NamedQuery(name=Publicnotification._FINDUnseenPublicNotification,query="select p.content from Publicnotification p where p.isActive=true and (:userLastLog between p.notificationStartDateTime and p.notificationEndDateTime) and  p.notificationStartDateTime > :userSecondLastLog"),
@NamedQuery(name=Publicnotification._FINDActiveMessage,query="select p.content from Publicnotification p where p.isActive=true and (:userLastLog between p.notificationStartDateTime and p.notificationEndDateTime)")

})
public @Data class Publicnotification implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String _FINDUnseenPublicNotification= "findUnseenPublicNotification";
	public static final String _FINDActiveMessage= "findActiveMessage";


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String circularNumber;

	private Date creationDateTime;

	@Temporal(TemporalType.DATE)
	private Date date;

	private String documentNumber;

	private String url;
	
	private String subject;
	
	private String content;
	
	private String category;
	
	private String iconCategory;
	
	private boolean isActive;
	
	private LocalDateTime notificationStartDateTime;
	
	private LocalDateTime notificationEndDateTime;

	public Publicnotification() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCircularNumber() {
		return this.circularNumber;
	}

	public void setCircularNumber(String circularNumber) {
		this.circularNumber = circularNumber;
	}

	public Date getCreationDateTime() {
		return this.creationDateTime;
	}

	public void setCreationDateTime(Timestamp creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getIconCategory() {
		return iconCategory;
	}

	public void setIconCategory(String iconCategory) {
		this.iconCategory = iconCategory;
	}


}