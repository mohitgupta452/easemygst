package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.view.serializer.CustomDateSerializer;

import java.sql.Timestamp;


/**
 * The persistent class for the erp_logs database table.
 * 
 */
@Entity
@Table(name="erp_logs")
@NamedQueries({ @NamedQuery(name="ErpLog.findAll", query="SELECT e FROM ErpLog e"),
	@NamedQuery(name="ErpLog.findByUserId", query="SELECT e FROM ErpLog e WHERE e.user.id=:userId ORDER BY e.creationTime DESC"),
	@NamedQuery(name="ErpLog.findByUserAndDesc", query="SELECT e FROM ErpLog e WHERE e.user.id=:userId and e.description like :description ORDER BY e.creationTime DESC")})
public class ErpLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@JsonIgnore
	private String createdBy;

	private Timestamp creationTime;

	@Lob
	private String description;

	@Lob
	private String errorFile;

	private String filename;

	private String gstin;

	private byte isError;

	private String monthYear;

	private String returnType;

	private String status;

	private String transactionType;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="userId")
	private User user;

	public ErpLog() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@JsonSerialize(using=CustomDateSerializer.class)
	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorFile() {
		return this.errorFile;
	}

	public void setErrorFile(String errorFile) {
		this.errorFile = errorFile;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getGstin() {
		return this.gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public byte getIsError() {
		return this.isError;
	}

	public void setIsError(byte isError) {
		this.isError = isError;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

}