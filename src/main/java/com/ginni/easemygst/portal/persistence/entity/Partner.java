package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the partner database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name="Partner.findAll", query="SELECT p FROM Partner p"),
	@NamedQuery(name = "Partner.findByData", query = "SELECT p FROM Partner p where p.active=1 and (p.emailAddress=:email or p.contactNo=:contactNo)"),
	@NamedQuery(name = "Partner.findByCode", query = "SELECT p FROM Partner p where p.active=1 and p.partnerCode=:code")})
public class Partner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@JsonIgnore
	private byte active;

	//bi-directional many-to-one association to Taxpayer
	@OneToOne
	@JoinColumn(name="billingAddressId")
	private BillingAddress billingAddress;

	private String city;

	private String clientBase;

	private String contactNo;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;

	private String emailAddress;

	private String partnerCode;
	
	@JsonIgnore
	private String partnerPercentage;

	private String partnerName;

	private String profession;

	private String state;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;

	// bi-directional many-to-one association to PartnerAccount
	@OneToMany(mappedBy = "partner")
	@JsonIgnore
	private List<PartnerAccount> partnerAccounts;

	// bi-directional many-to-one association to SkuPartner
	@OneToMany(mappedBy = "partner")
	@JsonIgnore
	private List<SkuPartner> skuPartners;
	
	public Partner() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClientBase() {
		return this.clientBase;
	}

	public void setClientBase(String clientBase) {
		this.clientBase = clientBase;
	}

	public String getContactNo() {
		return this.contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPartnerCode() {
		return this.partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerName() {
		return this.partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getProfession() {
		return this.profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public List<PartnerAccount> getPartnerAccounts() {
		return this.partnerAccounts;
	}

	public void setPartnerAccounts(List<PartnerAccount> partnerAccounts) {
		this.partnerAccounts = partnerAccounts;
	}

	public PartnerAccount addPartnerAccount(PartnerAccount partnerAccount) {
		getPartnerAccounts().add(partnerAccount);
		partnerAccount.setPartner(this);

		return partnerAccount;
	}

	public PartnerAccount removePartnerAccount(PartnerAccount partnerAccount) {
		getPartnerAccounts().remove(partnerAccount);
		partnerAccount.setPartner(null);

		return partnerAccount;
	}

	public List<SkuPartner> getSkuPartners() {
		return this.skuPartners;
	}

	public void setSkuPartners(List<SkuPartner> skuPartners) {
		this.skuPartners = skuPartners;
	}

	public SkuPartner addSkuPartner(SkuPartner skuPartner) {
		getSkuPartners().add(skuPartner);
		skuPartner.setPartner(this);

		return skuPartner;
	}

	public SkuPartner removeSkuPartner(SkuPartner skuPartner) {
		getSkuPartners().remove(skuPartner);
		skuPartner.setPartner(null);

		return skuPartner;
	}
	
	public String getPartnerPercentage() {
		return partnerPercentage;
	}

	public void setPartnerPercentage(String partnerPercentage) {
		this.partnerPercentage = partnerPercentage;
	}

}