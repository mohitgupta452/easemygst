package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import java.sql.Timestamp;


/**
 * The persistent class for the doc_issue database table.
 * 
 */
@Entity
@Table(name="doc_issue")
@NamedQueries({
@NamedQuery(name="DocIssue.findAll", query="SELECT d FROM DocIssue d"),
@NamedQuery(name = "DocIssue.getByGstinMonthyearReturntype", query = "select b from DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
@NamedQuery(name = "DocIssue.findCountByMonthYear", query = "select count(b.id) from DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
@NamedQuery(name = "DocIssue.findSummaryByMonthYear", query = "select SUM(b.totalNumber),SUM(b.canceled),SUM(b.netIssued) from DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
@NamedQuery(name = "DocIssue.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
@NamedQuery(name = "DocIssue.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
@NamedQuery(name = "DocIssue.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),


@NamedQuery(name="DocIssue.deleteByMonthYear", query="delete from DocIssue b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType "),
@NamedQuery(name="DocIssue.removeById",query="delete from DocIssue b where b.id in :ids "),
@NamedQuery(name="DocIssue.deleteByMonthYear1",query="delete from DocIssue b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType"),
@NamedQuery(name="DocIssue.updateFlagByMonthYear",query="update  DocIssue b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
@NamedQuery(name="DocIssue.updateFlagById",query="update DocIssue b set b.toBeSync=true, b.flags='DELETE' where b.id in :ids and b.isSynced=true and b.isTransit=false"),

@NamedQuery(name="DocIssue.findNotSynced",query="select b from DocIssue b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) and  b.isError=false"),
@NamedQuery(name = "DocIssue.findErrorInvoice",query = "Select b from DocIssue  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) "),
@NamedQuery(name= "DocIssue.findErrorInvoicesByTransitId",query="SELECT b from DocIssue b where b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
@NamedQuery(name="DocIssue.getFlagsCount",query="select new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from DocIssue b where b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError "),
@NamedQuery(name = "DocIssue.findErrorDataByMonthYear", query = "select b from DocIssue b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),



})
@JsonInclude(Include.NON_NULL)
public class DocIssue extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/*@Id
	private String id;*/

	private int canceled;

	private String category;

	/*private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;*/

	private String errMsg;
	
	private transient String errorCd;

	private String flags;

	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;


	private boolean isError=false;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSubmited;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String monthYear;

	private String returnType;

	private String snoFrom;

	private String snoTo;

	private String source;

	private String sourceId;

	private String syncId;

	@JsonProperty("toBeSynch")
	private boolean toBeSync=true;

	private int totalNumber;
	
	private int netIssued;

	private String transitId;

	private String type="NEW";
	
	private transient String error_msg;

	/*private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;*/

	public DocIssue() {
	}

	/*public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}*/

	public int getCanceled() {
		return this.canceled;
	}

	public void setCanceled(int canceled) {
		this.canceled = canceled;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

/*	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}*/

	public String getErrMsg() {
		return this.errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin ;
	}
	

	public boolean getIsError() {
		return this.isError;
	}

	public void setIsError(boolean isError) {
		this.isError = isError;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSubmited() {
		return this.isSubmited;
	}

	public void setIsSubmited(boolean isSubmited) {
		this.isSubmited = isSubmited;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getSnoFrom() {
		return this.snoFrom;
	}

	public void setSnoFrom(String snoFrom) {
		this.snoFrom = snoFrom;
	}

	public String getSnoTo() {
		return this.snoTo;
	}

	public void setSnoTo(String snoTo) {
		this.snoTo = snoTo;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSyncId() {
		return this.syncId;
	}

	public void setSyncId(String syncId) {
		this.syncId = syncId;
	}

	public boolean getToBeSync() {
		return this.toBeSync;
	}

	public void setToBeSync(boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public int getTotalNumber() {
		return this.totalNumber;
	}

	public void setTotalNumber(int totalNumber) {
		this.totalNumber = totalNumber;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getNetIssued() {
		return netIssued;
	}

	public void setNetIssued(int netIssued) {
		this.netIssued = netIssued;
	}

	public String getErrorCd() {
		return errorCd;
	}

	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	/*public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}*/
	
	public TransactionType getTransactionType() {
		return TransactionType.DOC_ISSUE;
	}

}