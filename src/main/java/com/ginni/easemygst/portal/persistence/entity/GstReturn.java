package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the gst_return database table.
 * 
 */
@Entity
@Table(name="gst_return")
@NamedQueries({ @NamedQuery(name="GstReturn.findAll", query="SELECT g FROM GstReturn g"),
	@NamedQuery(name="GstReturn.findById", query="SELECT g FROM GstReturn g where g.active=1 and g.id=:id")})
public class GstReturn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private byte active;
	
	private byte mandatory;

	private String code;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	@Lob
	private String description;

	@Temporal(TemporalType.DATE)
	private Date dueDate;
	
	@Temporal(TemporalType.DATE)
	private Date startDate;

	private String frequency;
	
	private String preProcess;

	private String name;

	private String status;
	
	private int dependent;
	
	private int lockedAfter;
	
	private int prevMonth;
	
	private Byte isParent;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	//bi-directional many-to-one association to BrtMapping
	@JsonIgnore
	@OneToMany(mappedBy="gstReturnBean")
	private List<BrtMapping> brtMappings;

	public GstReturn() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public byte getMandatory() {
		return mandatory;
	}

	public void setMandatory(byte mandatory) {
		this.mandatory = mandatory;
	}

	public int getLockedAfter() {
		return lockedAfter;
	}

	public void setLockedAfter(int lockedAfter) {
		this.lockedAfter = lockedAfter;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getPreProcess() {
		return preProcess;
	}

	public void setPreProcess(String preProcess) {
		this.preProcess = preProcess;
	}

	public Byte getIsParent() {
		return isParent;
	}

	public void setIsParent(Byte isParent) {
		this.isParent = isParent;
	}

	public String getFrequency() {
		return this.frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getDependent() {
		return dependent;
	}

	public void setDependent(int dependent) {
		this.dependent = dependent;
	}

	public int getPrevMonth() {
		return prevMonth;
	}

	public void setPrevMonth(int prevMonth) {
		this.prevMonth = prevMonth;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public List<BrtMapping> getBrtMappings() {
		return this.brtMappings;
	}

	public void setBrtMappings(List<BrtMapping> brtMappings) {
		this.brtMappings = brtMappings;
	}

	public BrtMapping addBrtMapping(BrtMapping brtMapping) {
		getBrtMappings().add(brtMapping);
		brtMapping.setGstReturnBean(this);

		return brtMapping;
	}

	public BrtMapping removeBrtMapping(BrtMapping brtMapping) {
		getBrtMappings().remove(brtMapping);
		brtMapping.setGstReturnBean(null);

		return brtMapping;
	}

}