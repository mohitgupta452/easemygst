package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.view.serializer.NILDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;


/**
 * The persistent class for the nil_transaction database table.
 * 
 */
@Entity
@Table(name="nil_transaction")
@NamedQueries({
	@NamedQuery(name="NilTransaction.findAll", query="SELECT n FROM NILTransactionEntity n"),
	@NamedQuery(name = "NilTransaction.getByGstinMonthyearReturntype", query = "select b from NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null) "),
	@NamedQuery(name = "NilTransaction.findByPagination", query = "select b from NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "NilTransaction.findCountByMonthYear", query = "select count(b.id) from NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "NilTransaction.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	
	@NamedQuery(name="NILTransactionEntity.removeById",query="delete from NILTransactionEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false"),
	@NamedQuery(name="NILTransactionEntity.deleteByMonthYear",query="delete from NILTransactionEntity b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false and b.returnType =:returnType"),
	@NamedQuery(name="NILTransactionEntity.updateFlagByMonthYear",query="update  NILTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
	@NamedQuery(name="NILTransactionEntity.updateFlagById",query="update NILTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),

	
	@NamedQuery(name = "NilTransaction.findSummaryByMonthYear", query = "select SUM(b.totExptAmt),SUM(b.totNilAmt),SUM(b.totNgsupAmt),SUM(b.totCompAmt) from NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "NilTransaction.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)  group by b.type"),
	@NamedQuery(name="NILTransactionEntity.findNotSynced",query="select a from NILTransactionEntity a where a.gstin=:gstn and a.monthYear in (:monthYear) and a.isError=false and a.returnType=:returnType and a.isTransit=false"),
	@NamedQuery(name = "NilTransaction.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "NILTransactionEntity.findErrorInvoice",query = "Select b from NILTransactionEntity  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) and b.supplyType=:suptyp "),
	@NamedQuery(name= "NILTransactionEntity.findErrorInvoicesByTransitId",query="SELECT b from NILTransactionEntity b where b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="NILTransactionEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from NILTransactionEntity b where b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
	@NamedQuery(name = "NILTransactionEntity.findErrorDataByMonthYear", query = "select b from NILTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "NILTransactionEntity.UnsyncByMonthYear", query = "update NILTransactionEntity b set b.toBeSync=true where b.gstin=:gstinid and b.monthYear=:monthYear and b.returnType=:returntype"),
})
//@JsonIgnoreProperties({"transit","locked","valid","amendment","gstnSynced","flags"})
@JsonSerialize(using=NILDetailSerializer.class)
public class NILTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;

	private String monthYear;

	private String returnType;
	
	@Enumerated(EnumType.STRING)
	private NilSupplyType supplyType;

	//@Transient
	private Double totNilAmt=0.0;

	//@Transient
	private Double totExptAmt=0.0;
	//@Transient
	private Double totNgsupAmt=0.0;
//	@Transient
	private Double totCompAmt=0.0;

	private String flags;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;

	private String synchId;

	private String transitId;

	private String type;
	
	private Boolean toBeSync;
	
	private transient String errorMsg;
	
	private transient String errorCd;
	
	private Boolean isError=false;
	
	private String errMsg;

	public NILTransactionEntity() {
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}


	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}


	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}


	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}


	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}


	public TaxpayerGstin getGstin() {
		return gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public Double getTotNilAmt() {
		return totNilAmt;
	}

	public void setTotNilAmt(Double totNilAmt) {
		this.totNilAmt = totNilAmt;
	}

	public Double getTotExptAmt() {
		return totExptAmt;
	}

	public void setTotExptAmt(Double totExptAmt) {
		this.totExptAmt = totExptAmt;
	}

	public Double getTotNgsupAmt() {
		return totNgsupAmt;
	}

	public void setTotNgsupAmt(Double totNgsupAmt) {
		this.totNgsupAmt = totNgsupAmt;
	}

	public Double getTotCompAmt() {
		return totCompAmt;
	}

	public void setTotCompAmt(Double totCompAmt) {
		this.totCompAmt = totCompAmt;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public NilSupplyType getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(NilSupplyType supplyType) {
		this.supplyType = supplyType;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorCd() {
		return errorCd;
	}

	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((NILTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((NILTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((NILTransactionEntity) obj).getMonthYear())
			   .append(this.getSupplyType(), ((NILTransactionEntity) obj).getSupplyType())


			   ;               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode()+supplyType.hashCode());
		return result;
	}
	
	public NILTransactionEntity(String id,Double totNilAmt, Double totExptAmt,Double totNgsupAmt, Double totCompAmt, String flags){
		this.setId(id);
		this.totNilAmt=totNilAmt;
		this.totExptAmt=totExptAmt;
		this.totNgsupAmt=totNgsupAmt;
		this.totCompAmt=totCompAmt;
		this.flags=flags;
				
	}
	
	public TransactionType getTransactionType() {
		return TransactionType.NIL;
	}
	
}