package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.business.dto.ReturnHistoryDTO;


/**
 * The persistent class for the gstin_action database table.
 * 
 */
@Entity
@Table(name="gstin_action")
@NamedQueries({ @NamedQuery(name="GstinAction.findAll", query="SELECT g FROM GstinAction g"),
	@NamedQuery(name = "GstinAction.findByStatus", query = "SELECT g FROM GstinAction g WHERE g.status=:status "
			+ "and g.particular=:particular and g.returnType=:returnType and g.taxpayerGstin.gstin=:gstin"),
	@NamedQuery(name = "GstinAction.findStatus", query = "SELECT g.status FROM GstinAction g WHERE  g.particular=:particular and g.returnType=:returnType and g.taxpayerGstin.gstin=:gstin"),
	
	@NamedQuery(name="GstinAction.findByTaxpayerGstinId", query="SELECT g FROM GstinAction g WHERE g.taxpayerGstin.id=:id"),
	@NamedQuery(name="GstinAction.findReturnHistory",query="select new com.ginni.easemygst.portal.business.dto.ReturnHistoryDTO(a.particular,a.returnType,a.status,a.actionDate) from GstinAction a where a.taxpayerGstin.gstin=:gstin and a.finanacialYear=:financialYear and a.returnType in ('gstr1','gstr3b')")})

@SqlResultSetMappings({
	@SqlResultSetMapping(name="GstinAction.ReturnHistoryDTOMapping",classes= {
			@ConstructorResult(targetClass=ReturnHistoryDTO.class,
					columns= {
							@ColumnResult(name="monthId"),
							@ColumnResult(name="returnType"),
							@ColumnResult(name="status"),
							@ColumnResult(name="startDate"),
							@ColumnResult(name="dueDate")
					})
	})
})

public class GstinAction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	private Date actionDate;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String particular;

	private String returnType;

	private String status;

	private String transaction;
	
	private String finanacialYear;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	//bi-directional many-to-one association to TaxpayerGstin
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="gstin")
	private TaxpayerGstin taxpayerGstin;
	
	@Column(name="refId")
	private String referenceId;
	
	private String checkStatusResponse;
	
	private String modeOfFilling;
	
	private String isvalid;
	
	public GstinAction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getActionDate() {
		return this.actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getParticular() {
		return this.particular;
	}

	public void setParticular(String particular) {
		this.particular = particular;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransaction() {
		return this.transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}
	
	public TaxpayerGstin getTaxpayerGstin() {
		return this.taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((particular == null) ? 0 : particular.hashCode());
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GstinAction other = (GstinAction) obj;
		if (particular == null) {
			if (other.particular != null)
				return false;
		} else if (!particular.equals(other.particular))
			return false;
		if (returnType == null) {
			if (other.returnType != null)
				return false;
		} else if (!returnType.equals(other.returnType))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	public String getCheckStatusResponse() {
		return checkStatusResponse;
	}

	public void setCheckStatusResponse(String checkStatusResponse) {
		this.checkStatusResponse = checkStatusResponse;
	}

	public String getModeOfFilling() {
		return modeOfFilling;
	}

	public void setModeOfFilling(String modeOfFilling) {
		this.modeOfFilling = modeOfFilling;
	}

	public String getIsvalid() {
		return isvalid;
	}

	public void setIsvalid(String isvalid) {
		this.isvalid = isvalid;
	}

	public String getFinanacialYear() {
		return finanacialYear;
	}

	public void setFinanacialYear(String finanacialYear) {
		this.finanacialYear = finanacialYear;
	}
}