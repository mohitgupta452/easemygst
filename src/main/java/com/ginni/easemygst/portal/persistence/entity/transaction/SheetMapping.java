package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;

import java.sql.Timestamp;


/**
 * The persistent class for the sheet_mappings database table.
 * 
 */
@Entity
@Table(name="sheet_mappings")
@NamedQueries({
@NamedQuery(name="SheetMapping.findAll", query="SELECT s FROM SheetMapping s"),
})@NamedQuery(name="SheetMapping.findBySource",query="select sm.mapping from SheetMapping sm where sm.returnType=:returnType and sm.sourceType=:sourceType")

public class SheetMapping extends CommonAttributesEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_SOURCE="SheetMapping.findBySource";

	private JsonNode mapping;

	private String templateName;

	@Enumerated(EnumType.STRING)
	private SourceType sourceType;

	private TaxpayerGstin taxpayerGstinId;

	private TransactionType transaction;

	private String version;
	
	@Enumerated(EnumType.STRING)
	private ReturnType returnType;

	public SheetMapping() {
	}

	public Object getMapping() {
		return this.mapping;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public TaxpayerGstin getTaxpayerGstinId() {
		return taxpayerGstinId;
	}

	public void setTaxpayerGstinId(TaxpayerGstin taxpayerGstinId) {
		this.taxpayerGstinId = taxpayerGstinId;
	}

	public TransactionType getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionType transaction) {
		this.transaction = transaction;
	}

	public void setMapping(JsonNode mapping) {
		this.mapping = mapping;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public ReturnType getReturnType() {
		return returnType;
	}

	public void setReturnType(ReturnType returnType) {
		this.returnType = returnType;
	}
	

}