package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.ImpsDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the imps_transaction database table.
 * 
 */
@Entity
@Table(name="imps_transaction")
@NamedQueries({
	@NamedQuery(name = "IMPSTransactionEntity.findAll", query = "SELECT b FROM IMPSTransactionEntity b"),
	@NamedQuery(name = "IMPSTransactionEntity.getByGstinMonthyearReturntype", query = "select b from IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "IMPSTransactionEntity.findByPagination", query = "select b from IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "IMPSTransactionEntity.findCountByMonthYear", query = "select count(b.id) from IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "IMPSTransactionEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	
	@NamedQuery(name="IMPSTransactionEntity.removeById",query="delete from IMPSTransactionEntity b where b.id in :ids b.isSynced=false and b.isTransit=false"),
	@NamedQuery(name="IMPSTransactionEntity.deleteByMonthYear",query="delete from IMPSTransactionEntity b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false  and b.returnType =:returnType"),
	@NamedQuery(name="IMPSTransactionEntity.updateFlagByMonthYear",query="update  IMPSTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true"),
	@NamedQuery(name="IMPSTransactionEntity.updateFlagById",query="update IMPSTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true"),

	
	@NamedQuery(name = "IMPSTransactionEntity.findSummaryByMonthYear", query = "select SUM(b.taxableValue),SUM(b.taxAmount) from IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null)"),
	@NamedQuery(name = "IMPSTransactionEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
	@NamedQuery(name = "IMPSTransactionEntity.findItcBulk", query = "SELECT b.invoiceNumber	 ,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id FROM IMPSTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "IMPSTransactionEntity.findItcBulkCount", query = "SELECT count(i.id) FROM IMPSTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null)" ),
	
	@NamedQuery(name = "IMPSTransactionEntity.findBlankHsnBulk", query = "SELECT b.invoiceNumber,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.invoiceDate,i.unit,i.quantity FROM IMPSTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)" ),
	@NamedQuery(name = "IMPSTransactionEntity.countBlankHsnBulk", query = "SELECT count(i.id) FROM IMPSTransactionEntity b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null)" ),

	
	@NamedQuery(name="IMPSTransactionEntity.findNotSynced",query="select a from IMPSTransactionEntity a where a.gstin=:gstn and a.monthYear in (:monthYear) and a.toBeSync=true and a.returnType=:returnType and a.isTransit=false and a.isError=false"),
	@NamedQuery(name = "IMPSTransactionEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "IMPSTransactionEntity.findErrorInvoice",query = "Select b from IMPSTransactionEntity  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear=:monthYear and b.invoiceNumber=:invno "),
	@NamedQuery(name= "IMPSTransactionEntity.findErrorInvoicesByTransitId",query="SELECT b from IMPSTransactionEntity b where b.gstin=:gstn and b.monthYear=:monthYear and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="IMPSTransactionEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO( b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from IMPSTransactionEntity b where  b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError"),
	@NamedQuery(name = "IMPSTransactionEntity.findItcSummary", query = "SELECT i.totalEligibleTax, sum(i.itcIgst)+sum(i.itcCgst)+sum(i.itcSgst)+sum(i.itcCess),sum(i.igst)+sum(i.cgst)+sum(i.sgst)+sum(i.cess) FROM IMPSTransactionEntity  b JOIN b.items i where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <>'DELETE' or b.flags is null) group by i.totalEligibleTax"),
	@NamedQuery(name = "IMPSTransactionEntity.findErrorDataByMonthYear", query = "select b from IMPSTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),


})
@JsonSerialize(using=ImpsDetailSerializer.class)

public class IMPSTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String flags;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;

	@Temporal(TemporalType.DATE)
	private Date invoiceDate;

	private String invoiceNumber;

	private double invoiceValue;
	
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Item> items;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	@Column(name="isSubmit")
	private boolean isSubmited;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String monthYear;
	
	private String financialYear;

	private String pos;

	private String stateName;
	
	private String returnType;

	private String source;

	private String sourceId;

	private String synchId;

	private double taxableValue;

	private double taxAmount;

	private String transitId;

	private String type;
	
	private Boolean toBeSync;
	
	private transient String errorMsg;
	
	private transient String errorCd;
	
	private String errMsg;
	
	private Boolean isError=false;
	

	public IMPSTransactionEntity() {
	}
	

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public double getInvoiceValue() {
		return this.invoiceValue;
	}

	public String getFinancialYear() {
		return financialYear;
	}


	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}


	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void setInvoiceValue(double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSubmited() {
		return this.isSubmited;
	}

	public void setIsSubmited(boolean isSubmited) {
		this.isSubmited = isSubmited;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getPos() {
		return this.pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getStateName() {
		return stateName;
	}


	public void setStateName(String stateName) {
		this.stateName = stateName;
	}


	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}


	public String getErrorCd() {
		return errorCd;
	}


	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}
	
	public String getErrMsg() {
		return errMsg;
	}


	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}


	public Boolean getIsError() {
		return isError;
	}


	public void setIsError(Boolean isError) {
		this.isError = isError;
	}


	@Override
	public boolean equals(Object obj){
		EqualsBuilder builder=new EqualsBuilder().
				append(this.getGstin(), ((IMPSTransactionEntity)obj).getGstin())
				.append(this.getReturnType(), ((IMPSTransactionEntity)obj).getReturnType())
				.append(this.getMonthYear(), ((IMPSTransactionEntity) obj).getMonthYear())
				.append(this.getInvoiceNumber(), ((IMPSTransactionEntity) obj).getInvoiceNumber());
		return builder.isEquals();
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode()+invoiceNumber.hashCode());
		return result;
	}
	
	public IMPSTransactionEntity(String invoiceNumber, Date invoiceDate,  double taxableValue,
			double taxAmount, String flags, String type, String source,String id,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError) {
		this.invoiceNumber=invoiceNumber;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.source = source;
		this.setId(id);
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
	}
	
	public TransactionType getTransactionType() {
		return TransactionType.IMPS;
	}

}