package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the tp_mapping database table.
 * 
 */
@Entity
@Table(name="tp_mapping")
@NamedQueries({ @NamedQuery(name="TpMapping.findAll", query="SELECT t FROM TpMapping t"),
	@NamedQuery(name="TpMapping.findBySecondaryId", query="SELECT t FROM TpMapping t WHERE t.staxpayer.id = :secondaryId"),
	@NamedQuery(name="TpMapping.findById", query="SELECT t FROM TpMapping t WHERE t.staxpayer.id = :secondaryId and t.ptaxpayer.id = :primaryId")})
public class TpMapping implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String createdBy;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer Id;

	private Timestamp creationTime;
	
	private String creationIpAddress;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="primaryTaxpayerId")
	private Taxpayer ptaxpayer;
	
	//bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="secondaryTaxpayerId")
	private Taxpayer staxpayer;
	
	public TpMapping() {
	
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Taxpayer getPtaxpayer() {
		return ptaxpayer;
	}

	public void setPtaxpayer(Taxpayer ptaxpayer) {
		this.ptaxpayer = ptaxpayer;
	}

	public Taxpayer getStaxpayer() {
		return staxpayer;
	}

	public void setStaxpayer(Taxpayer staxpayer) {
		this.staxpayer = staxpayer;
	}

}