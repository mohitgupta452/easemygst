package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the hsn_chapter database table.
 * 
 */
@Entity
@Table(name="hsn_chapter")
@NamedQueries({ @NamedQuery(name="HsnChapter.findAll", query="SELECT h FROM HsnChapter h"),
	@NamedQuery(name="HsnChapter.findByKey", query="SELECT h FROM HsnChapter h where h.code like :key or h.description like :key"),
	@NamedQuery(name="HsnChapter.findByCode", query="SELECT h FROM HsnChapter h where h.code=:code")})
public class HsnChapter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String code;

	private String cessRate;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	@Lob
	private String description;

	private String displayName;

	private String taxRate;

	//bi-directional many-to-one association to HsnSubChapter
	@OneToMany(mappedBy="hsnChapter")
	private List<HsnSubChapter> hsnSubChapters;

	public HsnChapter() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCessRate() {
		return this.cessRate;
	}

	public void setCessRate(String cessRate) {
		this.cessRate = cessRate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}

	public List<HsnSubChapter> getHsnSubChapters() {
		return this.hsnSubChapters;
	}

	public void setHsnSubChapters(List<HsnSubChapter> hsnSubChapters) {
		this.hsnSubChapters = hsnSubChapters;
	}

	public HsnSubChapter addHsnSubChapter(HsnSubChapter hsnSubChapter) {
		getHsnSubChapters().add(hsnSubChapter);
		hsnSubChapter.setHsnChapter(this);

		return hsnSubChapter;
	}

	public HsnSubChapter removeHsnSubChapter(HsnSubChapter hsnSubChapter) {
		getHsnSubChapters().remove(hsnSubChapter);
		hsnSubChapter.setHsnChapter(null);

		return hsnSubChapter;
	}

}