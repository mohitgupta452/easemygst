package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.view.serializer.CDNDetailSerializer;
import com.ginni.easemygst.portal.data.view.serializer.CDNURDetailSerializer;


/**
 * The persistent class for the cdn_details database table.
 * 
 */
@Entity
@Table(name="cdnur_details")
@NamedQueries({
@NamedQuery(name="CdnUrDetail.findAll", query="SELECT c FROM CDNURDetailEntity c"),
@NamedQuery(name = "CdnUrDetailEntity.findByPagination", query = "SELECT b FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear"),
@NamedQuery(name = "CdnUrDetailEntity.findCountByMonthYear", query = "SELECT count(b.id) FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
@NamedQuery(name = "CdnUrDetailEntity.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null)" ),


@NamedQuery(name="CDNURDetailEntity.removeById",query="delete from CDNURDetailEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false"),
@NamedQuery(name="CDNURDetailEntity.deleteByMonthYear",query="delete from CDNURDetailEntity b where b.cdnUrTransaction in (select cdn from CDNURTransactionEntity cdn where cdn.monthYear =:monthYear and cdn.gstin.gstin =:gstin and cdn.returnType =:returnType) and b.isSynced=false and b.isTransit=false"),
@NamedQuery(name="CDNURDetailEntity.updateFlagByMonthYear",query="update  CDNURDetailEntity b set b.flags='DELETE',b.toBeSync=true where b.cdnUrTransaction in (select cdn from CDNURTransactionEntity cdn where cdn.monthYear =:monthYear and cdn.gstin.gstin =:gstin and cdn.returnType =:returnType) and b.isSynced=true and b.isTransit=false"),
@NamedQuery(name="CDNURDetailEntity.updateFlagById",query="update  CDNURDetailEntity b set b.flags='DELETE',b.toBeSync=true where b.id in :ids and b.isSynced=true and b.isTransit=false"),


@NamedQuery(name = "CdnUrDetailEntity.findSummaryByMonthYear", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and  (b.flags <> 'DELETE' or b.flags is null ) and b.noteType='D'"),
@NamedQuery(name = "CdnUrDetailEntity.findSummaryByMonthYearCredRefund", query = "SELECT SUM(b.taxableValue),SUM(b.taxAmount) FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) and (b.noteType='C' or b.noteType='R')"),

@NamedQuery(name = "CdnUrDetailEntity.findItcBulk", query = "SELECT  b.revisedInvNo,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.cdnUrTransaction.originalCtinName FROM CDNURDetailEntity b JOIN b.items i where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null) " ),
@NamedQuery(name = "CdnUrDetailEntity.findItcBulkCount", query = "SELECT  count(i.id) FROM CDNURDetailEntity b JOIN b.items i where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.totalEligibleTax is NULL or i.totalEligibleTax='') and (b.flags <>'DELETE' or b.flags is null) " ),

@NamedQuery(name = "CdnUrDetailEntity.findBlankHsnBulk", query = "SELECT  b.revisedInvNo,i.igst,i.cgst,i.sgst,i.cess,i.taxRate,i.taxableValue,i.id,b.cdnUrTransaction.originalCtinName,i.unit,i.quantity,b.invoiceDate FROM CDNURDetailEntity b JOIN b.items i where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null) " ),
@NamedQuery(name = "CdnUrDetailEntity.countBlankHsnBulk", query = "SELECT  count(i.id) FROM CDNURDetailEntity b JOIN b.items i where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and b.id=i.invoiceId and (i.hsnCode is NULL or i.hsnCode='') and (b.flags <>'DELETE' or b.flags is null) " ),


@NamedQuery(name = "CdnUrDetailEntity.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear group by b.type"),
@NamedQuery(name="CDNURDetailEntity.findNotSynced",query="select b from CDNURDetailEntity b where b.toBeSync=true and b.cdnUrTransaction.gstin=:gstn and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear in (:monthYear) and b.isTransit=false and b.isError=false"),
@NamedQuery(name = "CdnUrDetailEntity.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
@NamedQuery(name= "CdnUrDetailEntity.findErrorInvoicesByTransitId",query="SELECT b from CDNURDetailEntity b where b.cdnUrTransaction.gstin=:gstn and b.cdnUrTransaction.monthYear in (:monthYear) and b.cdnUrTransaction.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
@NamedQuery(name = "CdnUrDetailEntity.findErrorInvoice",query = "Select b from CDNURDetailEntity  b where b.cdnUrTransaction.gstin=:gstn and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear in (:monthYear) and b.revisedInvNo=:revisedInvNo"),
@NamedQuery(name="CDNURDetailEntity.getFlagsCount",query="select  new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from CDNURDetailEntity b where b.cdnUrTransaction.monthYear=:monthYear and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError "),
@NamedQuery(name = "CdnUrDetailEntity.findItcSummary", query = "SELECT i.totalEligibleTax, sum(i.itcIgst)+sum(i.itcCgst)+sum(i.itcSgst)+sum(i.itcCess),sum(i.igst)+sum(i.cgst)+sum(i.sgst)+sum(i.cess) FROM CDNURDetailEntity b JOIN b.items i where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear  and (b.flags <>'DELETE' or b.flags is null) group by i.totalEligibleTax"),
@NamedQuery(name = "CdnUrDetailEntity.findErrorDataByMonthYear", query = "SELECT b FROM CDNURDetailEntity b where b.cdnUrTransaction.gstin=:taxPayerGstin and b.cdnUrTransaction.returnType=:returnType and b.cdnUrTransaction.monthYear=:monthYear  and b.isError=true and  (b.flags <> 'DELETE' or b.flags is null ) " ),




})
@JsonSerialize(using=CDNURDetailSerializer.class)
public class CDNURDetailEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="cdnUrTransactionId")
	private CDNURTransactionEntity cdnUrTransaction;

	private String invoiceNumber;

	private String flags;

	private boolean isAmmendment;

	private boolean isLocked;

	private  boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;

	@Column(name="syncId")
	private String synchId;

	private String transitId;

	private String type;

	@Transient
	private String originalCtin;
	
	@Transient
	private String originalCtinName;
	
	private double taxableValue;
	
	private double taxAmount;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	private List<Item> items;
	
	private Date invoiceDate;
	
	private String revisedInvNo;
	
	private String noteType;
	
	private Date revisedInvDate;
	
	private String reasonForNote;
	
	private String preGstRegime;
	
	private String invoiceType;
	
	private Boolean toBeSync;
	
    private String errMsg;
	
	private Boolean isError=false;
	
	private transient String error_msg;
	
	private String originalNoteNumber ;
	
	private Date originalNoteDate;
	
	
	public List<Item> getItems() {
		return items;
	}


	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getInvoiceType() {
		return invoiceType;
	}


	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}


	public String getRevisedInvNo() {
		return revisedInvNo;
	}

	public void setRevisedInvNo(String revisedInvNo) {
		this.revisedInvNo = revisedInvNo;
	}


	public Date getRevisedInvDate() {
		return revisedInvDate;
	}


	public void setRevisedInvDate(Date revisedInvDate) {
		this.revisedInvDate = revisedInvDate;
	}


	public Date getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public CDNURDetailEntity() {
	}


	public CDNURTransactionEntity getCdnUrTransaction() {
		return cdnUrTransaction;
	}

	public void setCdnUrTransaction(CDNURTransactionEntity cdnUrTransaction) {
		this.cdnUrTransaction = cdnUrTransaction;
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public String getOriginalCtin() {
		if(!Objects.isNull(cdnUrTransaction))
			originalCtin=cdnUrTransaction.getOriginalCtin();
		return originalCtin;
	}

	public void setOriginalCtin(String originalCtin) {
		this.originalCtin = originalCtin;
	}


	public String getOriginalCtinName() {
		if(!Objects.isNull(cdnUrTransaction))
			originalCtinName=cdnUrTransaction.getOriginalCtinName();
		return originalCtinName;
	}

	public void setOriginalCtinName(String originalCtinName) {
		this.originalCtinName = originalCtinName;
	}

	public double getTaxableValue() {
		return taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public double getTaxAmount() {
		return taxAmount;
	}




	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getNoteType() {
		return noteType;
	}


	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}

	public String getReasonForNote() {
		return reasonForNote;
	}


	public void setReasonForNote(String reasonForNote) {
		this.reasonForNote = reasonForNote;
	}


	public String getPreGstRegime() {
		return preGstRegime;
	}


	public void setPreGstRegime(String preGstRegime) {
		this.preGstRegime = preGstRegime;
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}
	
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getError_msg() {
		return error_msg;
	}


	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	
	public String getOriginalNoteNumber() {
		return originalNoteNumber;
	}

	public void setOriginalNoteNumber(String originalNoteNumber) {
		this.originalNoteNumber = originalNoteNumber;
	}

	public Date getOriginalNoteDate() {
		return originalNoteDate;
	}

	public void setOriginalNoteDate(Date originalNoteDate) {
		this.originalNoteDate = originalNoteDate;
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(this.cdnUrTransaction,this.getId(),this.getInvoiceNumber());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder()
				.append(this.getCdnUrTransaction(), ((CDNURDetailEntity) obj).getCdnUrTransaction())
				.append(this.getInvoiceNumber(), ((CDNURDetailEntity) obj).getInvoiceNumber());
		return builder.isEquals();
	}

	public CDNURDetailEntity(CDNURTransactionEntity cdnUrTransaction, String id,String invoiceNumber,
			Date invoiceDate,String revisedInvNo,
			Date revisedInvDate,String noteType, double taxableValue, double taxAmount, String flags, String type, String source,
			boolean isMarked,boolean isSynced,boolean toBeSync,boolean isTransit,boolean isError,
			boolean isAmmendment,String originalNoteNumber,Date originalNoteDate) {
	
		this.setId(id);;
		this.invoiceNumber=invoiceNumber;
		this.cdnUrTransaction=cdnUrTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.source=source;
		this.isMarked=isMarked;
		this.revisedInvNo=revisedInvNo;
		this.revisedInvDate=revisedInvDate;
		this.noteType=noteType;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
		this.isAmmendment=isAmmendment;
		this.originalNoteNumber=originalNoteNumber;
		this.originalNoteDate=originalNoteDate;
	}
	
	//constructor for save gstn
	public CDNURDetailEntity(CDNURTransactionEntity cdnUrTransaction, String id,String invoiceNumber,
			Date invoiceDate,String revisedInvNo,
			Date revisedInvDate,String noteType, double taxableValue, double taxAmount, String flags, String type,
			String reasonForNote,String preGstRegime,Item item,boolean isAmendment,String invoiceType) {
	
		this.setId(id);;
		this.invoiceNumber=invoiceNumber;
		this.cdnUrTransaction=cdnUrTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.revisedInvNo=revisedInvNo;
		this.revisedInvDate=revisedInvDate;
		this.noteType=noteType;
		this.reasonForNote=reasonForNote;
		this.preGstRegime=preGstRegime;
		this.isAmmendment=isAmendment;
		this.invoiceType=invoiceType;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}
	}
	
	//constructor for save gstn
	public CDNURDetailEntity(CDNURTransactionEntity cdnUrTransaction, String id,String invoiceNumber,
			Date invoiceDate,String revisedInvNo,
			Date revisedInvDate,String noteType, double taxableValue, double taxAmount, String flags, String type,
			String reasonForNote,String preGstRegime,Item item,boolean isAmendment,Date originalNoteDate,String originalNoteNumber,String invoiceType) {
	
		this.setId(id);;
		this.invoiceNumber=invoiceNumber;
		this.cdnUrTransaction=cdnUrTransaction;
		this.invoiceDate=invoiceDate;
		this.taxableValue=taxableValue;
		this.taxAmount=taxAmount;
		this.flags=flags;
		this.type=type;
		this.revisedInvNo=revisedInvNo;
		this.revisedInvDate=revisedInvDate;
		this.noteType=noteType;
		this.reasonForNote=reasonForNote;
		this.preGstRegime=preGstRegime;
		this.isAmmendment=isAmendment;
		this.originalNoteDate=originalNoteDate;
		this.originalNoteNumber=originalNoteNumber;
		this.invoiceType=invoiceType;

		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}
	}
	
	
	public TransactionType getTransactionType() {
		return TransactionType.CDNUR;
	}
	

}