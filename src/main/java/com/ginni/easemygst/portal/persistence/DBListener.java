package com.ginni.easemygst.portal.persistence;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.PostRemove;

import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.CommonAttributesEntity;

public class DBListener {

	@PostRemove
	public void deleteItem(CommonAttributesEntity attributesEntity){
		try {
			ReturnsService returnsService = (ReturnsService) InitialContext
					.doLookup("java:global/easemygst/ReturnsServiceImpl");
			returnsService.deleteItemsByInvoiceId(attributesEntity.getId());
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
}
