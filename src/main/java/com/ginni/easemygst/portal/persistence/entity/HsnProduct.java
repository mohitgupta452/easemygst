package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the hsn_product database table.
 * 
 */
@Entity
@Table(name = "hsn_product")
@NamedQueries({ @NamedQuery(name = "HsnProduct.findAll", query = "SELECT h FROM HsnProduct h"),
		@NamedQuery(name = "HsnProduct.findByCode", query = "SELECT h FROM HsnProduct h WHERE h.code=:code")

})
public class HsnProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String code;

	private String cessRate;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	@Lob
	private String description;

	private String displayName;

	private String taxRate;

	// bi-directional many-to-one association to HsnSubChapter
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "subChapterCode")
	private HsnSubChapter hsnSubChapter;

	public HsnProduct() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCessRate() {
		return this.cessRate;
	}

	public void setCessRate(String cessRate) {
		this.cessRate = cessRate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}

	public HsnSubChapter getHsnSubChapter() {
		return this.hsnSubChapter;
	}

	public void setHsnSubChapter(HsnSubChapter hsnSubChapter) {
		this.hsnSubChapter = hsnSubChapter;
	}

}