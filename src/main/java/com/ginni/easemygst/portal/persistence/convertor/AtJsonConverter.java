package com.ginni.easemygst.portal.persistence.convertor;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.data.transaction.AT;

@Converter(autoApply=true)
public class AtJsonConverter implements AttributeConverter<AT,String>{

	private final static ObjectMapper _Mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(AT attribute) {
		try {
			return _Mapper.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;	
	}

	@Override
	public AT convertToEntityAttribute(String dbData) {
		try {
			return _Mapper.readValue(dbData,AT.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
