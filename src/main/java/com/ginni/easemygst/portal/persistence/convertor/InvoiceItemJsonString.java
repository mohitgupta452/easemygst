package com.ginni.easemygst.portal.persistence.convertor;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.data.InvoiceItem;

@Converter(autoApply=true)
public class InvoiceItemJsonString implements AttributeConverter<List<InvoiceItem>,String>{
	
	private final static ObjectMapper _Mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(List<InvoiceItem> attribute) {
		
		try {
			return _Mapper.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InvoiceItem> convertToEntityAttribute(String dbData) {
		
		try {
			return _Mapper.readValue(dbData,List.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;	}
	
	

}
