package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the brt_mapping database table.
 * 
 */
@Entity
@Table(name="brt_mapping")
@NamedQueries({ @NamedQuery(name="BrtMapping.findAll", query="SELECT b FROM BrtMapping b"),
	@NamedQuery(name = "BrtMapping.findByBusinessTypes", query = "SELECT b FROM BrtMapping b WHERE b.businessTypeBean.id = 16 and b.active=1"),
	@NamedQuery(name = "BrtMapping.findByAllBusinessTypes", query = "SELECT b FROM BrtMapping b WHERE b.businessTypeBean.id = 16 and b.active=1")})
public class BrtMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private byte active;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	//bi-directional many-to-one association to BusinessType
	@ManyToOne
	@JoinColumn(name="businessType")
	private BusinessType businessTypeBean;

	//bi-directional many-to-one association to GstReturn
	@ManyToOne
	@JoinColumn(name="gstReturn")
	private GstReturn gstReturnBean;

	//bi-directional many-to-one association to ReturnTransaction
	@ManyToOne
	@JoinColumn(name="transactionType")
	private ReturnTransaction returnTransaction;

	public BrtMapping() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public BusinessType getBusinessTypeBean() {
		return this.businessTypeBean;
	}

	public void setBusinessTypeBean(BusinessType businessTypeBean) {
		this.businessTypeBean = businessTypeBean;
	}

	public GstReturn getGstReturnBean() {
		return this.gstReturnBean;
	}

	public void setGstReturnBean(GstReturn gstReturnBean) {
		this.gstReturnBean = gstReturnBean;
	}

	public ReturnTransaction getReturnTransaction() {
		return this.returnTransaction;
	}

	public void setReturnTransaction(ReturnTransaction returnTransaction) {
		this.returnTransaction = returnTransaction;
	}

}