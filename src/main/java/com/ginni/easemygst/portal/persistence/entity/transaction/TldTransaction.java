package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the tld_transaction database table.
 * 
 */
@Entity
@Table(name="tld_transaction")
@NamedQuery(name="TldTransaction.findAll", query="SELECT t FROM TldTransaction t")
public class TldTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private double advancePaid;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String flags;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;
	
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Item> items;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSubmited;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String monthYear;

	private String pos;

	private String returnType;

	private String source;

	private String sourceId;

	private String synchId;

	private double taxAmount;

	private String transitId;

	private String type;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	public TldTransaction() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getAdvancePaid() {
		return this.advancePaid;
	}

	public void setAdvancePaid(double advancePaid) {
		this.advancePaid = advancePaid;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public TaxpayerGstin getGstin() {
		return this.gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSubmited() {
		return this.isSubmited;
	}

	public void setIsSubmited(boolean isSubmited) {
		this.isSubmited = isSubmited;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getPos() {
		return this.pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

}