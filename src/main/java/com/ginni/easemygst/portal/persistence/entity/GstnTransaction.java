package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the gstin_transaction database table.
 * 
 */
@Entity
@Table(name="gstn_transaction")
@NamedQueries({ @NamedQuery(name="GstnTransaction.findAll", query="SELECT g FROM GstnTransaction g"),
	@NamedQuery(name="GstnTransaction.findTransaction", query="SELECT g FROM GstnTransaction g WHERE g.monthYear=:monthYear and "
			+ "g.returnType=:returnType and g.transactionType=:transactionType and g.taxpayerGstin.gstin=:gstin and g.actionRequired=:actionRequired")})
public class GstnTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	private String monthYear;

	@Lob
	private String remark;

	private String returnType;

	private String status;
	
	private byte isChanged;
	
	private byte actionRequired;

	@Lob
	private String transactionObject;

	private String transactionType;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;
	
	//bi-directional many-to-one association to TaxpayerGstin
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin taxpayerGstin;

	public GstnTransaction() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return this.taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public byte getIsChanged() {
		return isChanged;
	}

	public void setIsChanged(byte isChanged) {
		this.isChanged = isChanged;
	}

	public String getTransactionObject() {
		return this.transactionObject;
	}

	public void setTransactionObject(String transactionObject) {
		this.transactionObject = transactionObject;
	}

	public String getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public byte getActionRequired() {
		return actionRequired;
	}

	public void setActionRequired(byte actionRequired) {
		this.actionRequired = actionRequired;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

}