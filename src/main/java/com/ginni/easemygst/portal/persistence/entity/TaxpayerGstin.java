package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the taxpayer_gstin database table.
 * 
 */
@Entity
@Table(name = "taxpayer_gstin")
@NamedQueries({ @NamedQuery(name = "TaxpayerGstin.findAll", query = "SELECT t FROM TaxpayerGstin t"),
		@NamedQuery(name = "TaxpayerGstin.findByGstinAll", query = "SELECT t FROM TaxpayerGstin t where t.gstin=:gstin"),
		@NamedQuery(name = "TaxpayerGstin.findByPan", query = "SELECT t.gstin FROM TaxpayerGstin t where t.taxpayer.panCard=:pan"),
		@NamedQuery(name = "TaxpayerGstin.findLegalName", query = "SELECT t.taxpayer.legalName FROM TaxpayerGstin t where t.taxpayer.panCard=:pan"),
		@NamedQuery(name = "TaxpayerGstin.findByGstin", query = "SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin"),
		@NamedQuery(name = "TaxpayerGstin.findByGstinNoActive", query = "SELECT t FROM TaxpayerGstin t where t.gstin=:gstin"),
		@NamedQuery(name = "TaxpayerGstin.findByGstinDTO", query = "SELECT new com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO(t.id, t.gstin, t.displayName, t.monthYear) FROM TaxpayerGstin t where t.gstin=:gstin"),
		@NamedQuery(name = "TaxpayerGstin.findByIdentity", query = "SELECT t FROM TaxpayerGstin t where t.active=1 and t.id in (select ug.taxpayerGstin.id from UserGstin ug where ug.user.id=:id and ug.taxpayerGstin.taxpayer.panCard=:pan)") })
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxpayerGstin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Byte active=1;

	private Byte authenticated;

	private String authToken;

	private String authExpiry;

	private Timestamp authExpiryTime;

	private Timestamp maxAuthExpiryTime;

	private String appKey;

	private String otp;

	private String otpExpiry;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;

	@JsonIgnore
	private String creationIpAddress;

	private String gstin;

	private String status;

	private String monthYear;

	private String sek;

	private String username;

	private String type;

	private String displayName;

	private String address;

	private String pincode;

	private String mobileNumber;

	private String emailAddress;

	private Date startDate;

	private Date endDate;

	private String bankAccountDetails;

	private String termsConditions;

	/*private BigDecimal previousGt;

	private BigDecimal currentGt;*/

	private boolean flushData=true;

	/* private String returnPreference; */

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;

	@JsonIgnore
	private String updationIpAddress;

	// bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name = "state")
	private State stateBean;

	// bi-directional many-to-one association to Taxpayer
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "taxpayerId")
	private Taxpayer taxpayer;

	private String authServer;

	@JsonIgnore
	@OneToMany(mappedBy="taxpayerGstin",cascade = CascadeType.ALL, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<FilePrefernce> filePrefernce;

	public boolean isFlushData() {
		return flushData;
	}

	public void setFlushData(boolean flushData) {
		this.flushData = flushData;
	}

	@Column(name = "ewb_username")
	private String ewayBillUserName;

	@Column(name = "ewb_password")
	private String ewayBillPassword;
	
	/*@Column(name="ewb_isActive")
	private boolean isEWBActive;*/
		
	public TaxpayerGstin() {

	}

	public TaxpayerGstin(int id, String gstin, String authToken, String appKey, String sek, String username) {

		this.id = id;
		this.gstin = gstin;
		this.authToken = authToken;
		this.appKey = appKey;
		this.sek = sek;
		this.username = username;

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Byte getActive() {
		return this.active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Byte getAuthenticated() {
		return this.authenticated;
	}

	public void setAuthenticated(Byte authenticated) {
		this.authenticated = authenticated;
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getAuthExpiry() {
		return this.authExpiry;
	}

	public void setAuthExpiry(String authExpiry) {
		this.authExpiry = authExpiry;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOtpExpiry() {
		return otpExpiry;
	}

	public void setOtpExpiry(String otpExpiry) {
		this.otpExpiry = otpExpiry;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getSek() {
		return this.sek;
	}

	public void setSek(String sek) {
		this.sek = sek;
	}

	public String getOtp() {
		return this.otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getBankAccountDetails() {
		return bankAccountDetails;
	}

	public void setBankAccountDetails(String bankAccountDetails) {
		this.bankAccountDetails = bankAccountDetails;
	}

	public String getTermsConditions() {
		return termsConditions;
	}

	public void setTermsConditions(String termsConditions) {
		this.termsConditions = termsConditions;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getGstin() {
		if (!StringUtils.isEmpty(this.gstin))
			this.gstin = this.gstin.toUpperCase();
		return this.gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public State getStateBean() {
		return this.stateBean;
	}

	public void setStateBean(State stateBean) {
		this.stateBean = stateBean;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Taxpayer getTaxpayer() {
		return this.taxpayer;
	}

	public void setTaxpayer(Taxpayer taxpayer) {
		this.taxpayer = taxpayer;
	}

/*	public BigDecimal getPreviousGt() {
		return previousGt;
	}

	public void setPreviousGt(BigDecimal previousGt) {
		this.previousGt = previousGt;
	}

	public BigDecimal getCurrentGt() {
		return currentGt;
	}

	public void setCurrentGt(BigDecimal currentGt) {
		this.currentGt = currentGt;
	}*/

	public Timestamp getAuthExpiryTime() {
		return authExpiryTime;
	}

	public void setAuthExpiryTime(Timestamp authExpiryTime) {
		this.authExpiryTime = authExpiryTime;
	}

	public Timestamp getMaxAuthExpiryTime() {
		return maxAuthExpiryTime;
	}

	public void setMaxAuthExpiryTime(Timestamp maxAuthExpiryTime) {
		this.maxAuthExpiryTime = maxAuthExpiryTime;
	}

	public String getAuthServer() {
		return authServer;
	}

	public void setAuthServer(String authServer) {
		this.authServer = authServer;
	}

	public String getEwayBillUserName() {
		return ewayBillUserName;
	}

	public void setEwayBillUserName(String ewayBillUserName) {
		this.ewayBillUserName = ewayBillUserName;
	}

	public String getEwayBillPassword() {
		return ewayBillPassword;
	}

	public void setEwayBillPassword(String ewabillPassword) {
		this.ewayBillPassword = ewabillPassword;
	}
	
/*	public boolean isEWBActive() {
		return isEWBActive;
	}

	public void setEWBActive(boolean isEWBActive) {
		this.isEWBActive = isEWBActive;
	}*/

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TaxpayerGstin) {
			TaxpayerGstin gstin = (TaxpayerGstin) obj;
			if (this == gstin)
				return true;
			if (this.getGstin().equals(gstin.getGstin()))
				return true;
		}
		return false;
	}

	@Transient
	public String getFilePreferenceByFinancialYear(String financialYear) {

		return this.getFilePrefernce().stream()
				.filter(preference -> preference.getFinancialYear().equalsIgnoreCase(financialYear)).findFirst().get()
				.getPreference();

	}

	public List<FilePrefernce> getFilePrefernce() {
		return filePrefernce;
	}

	public void setFilePrefernce(List<FilePrefernce> filePrefernce) {
		this.filePrefernce = filePrefernce;
	}

}