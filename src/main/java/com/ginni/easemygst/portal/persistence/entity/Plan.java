package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the plans database table.
 * 
 */
@Entity
@Table(name="plans")
@NamedQueries({ @NamedQuery(name="Plan.findAll", query="SELECT p FROM Plan p WHERE p.status='active' order by p.recurringPrice asc"),
	@NamedQuery(name="Plan.findByCode", query="SELECT p FROM Plan p WHERE p.planCode=:code"),
	@NamedQuery(name="Plan.findByType", query="SELECT p FROM Plan p WHERE p.planCode like :type  order by p.planCode desc")})
@JsonIgnoreProperties(ignoreUnknown=true)
public class Plan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="plan_code")
	private String planCode;

	@Column(name="billing_cycles")
	private int billingCycles;

	@Column(name="created_time")
	private String createdTime;

	private String description;

	@Column(name="hsn_or_sac")
	private String hsnOrSac;

	@Column(name="plan_interval")
	private int planInterval;

	@Column(name="plan_interval_unit")
	private String planIntervalUnit;

	private String name;

	@Column(name="product_id")
	private String productId;

	@Column(name="product_type")
	private String productType;

	@Column(name="recurring_price")
	private int recurringPrice;

	@Column(name="setup_fee")
	private int setupFee;

	private String status;

	@Column(name="tax_id")
	private String taxId;

	@Column(name="trial_period")
	private int trialPeriod;

	@Column(name="updated_time")
	private String updatedTime;

	public Plan() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlanCode() {
		return this.planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public int getBillingCycles() {
		return this.billingCycles;
	}

	public void setBillingCycles(int billingCycles) {
		this.billingCycles = billingCycles;
	}

	public String getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHsnOrSac() {
		return this.hsnOrSac;
	}

	public void setHsnOrSac(String hsnOrSac) {
		this.hsnOrSac = hsnOrSac;
	}

	public int getPlanInterval() {
		return planInterval;
	}

	public void setPlanInterval(int planInterval) {
		this.planInterval = planInterval;
	}

	public String getPlanIntervalUnit() {
		return planIntervalUnit;
	}

	public void setPlanIntervalUnit(String planIntervalUnit) {
		this.planIntervalUnit = planIntervalUnit;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductId() {
		return this.productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public int getRecurringPrice() {
		return this.recurringPrice;
	}

	public void setRecurringPrice(int recurringPrice) {
		this.recurringPrice = recurringPrice;
	}

	public int getSetupFee() {
		return this.setupFee;
	}

	public void setSetupFee(int setupFee) {
		this.setupFee = setupFee;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTaxId() {
		return this.taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public int getTrialPeriod() {
		return this.trialPeriod;
	}

	public void setTrialPeriod(int trialPeriod) {
		this.trialPeriod = trialPeriod;
	}

	public String getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

}