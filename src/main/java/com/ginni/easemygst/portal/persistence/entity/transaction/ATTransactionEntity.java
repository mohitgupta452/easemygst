package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.view.serializer.ATDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;


/**
 * The persistent class for the at_transaction database table.
 * 
 */
@Entity
@Table(name="at_transaction")
@NamedQueries({
	@NamedQuery(name = "AtTransaction.findAll", query = "SELECT b FROM ATTransactionEntity b"),
	@NamedQuery(name = "AtTransaction.getByGstinMonthyearReturntype", query = "select b from ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "AtTransaction.findByPagination", query = "select b from ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear"),
	@NamedQuery(name = "AtTransaction.findCountByMonthYear", query = "select count(b.id) from ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "AtTransaction.findErrorCountByMonthYear", query = "SELECT count(b.id) FROM ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )" ),

	@NamedQuery(name="ATTransactionEntity.removeById",query="delete from ATTransactionEntity b where b.id in :ids and b.isSynced=false and b.isTransit=false "),
	@NamedQuery(name="ATTransactionEntity.deleteByMonthYear",query="delete from ATTransactionEntity b where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin)and b.isSynced=false and b.isTransit=false and b.returnType =:returnType"),
	@NamedQuery(name="ATTransactionEntity.updateFlagByMonthYear",query="update  ATTransactionEntity b set b.flags='DELETE',b.toBeSync=true where b.monthYear =:monthYear and b.gstin=(SELECT t FROM TaxpayerGstin t where t.active=1 and t.gstin=:gstin) and b.returnType =:returnType and b.isSynced=true and b.isTransit=false"),
	@NamedQuery(name="ATTransactionEntity.updateFlagById",query="update ATTransactionEntity b set b.toBeSync=true, b.flags='DELETE' where b.id in :ids and b.isSynced=true and b.isTransit=false"),

	
	@NamedQuery(name = "AtTransaction.findSummaryByMonthYear", query = "select SUM(b.advanceReceived),SUM(b.taxAmount) from ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name="ATTransactionEntity.findNotSynced",query="select a from ATTransactionEntity a where a.gstin=:gstn and a.monthYear in (:monthYear) and a.toBeSync=true and a.returnType=:returnType and a.isTransit=false and a.isError=false"),
	@NamedQuery(name = "AtTransaction.findTinDashboardSummary", query = "SELECT b.type,COUNT(b.id)  FROM ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear group by b.type"),
	@NamedQuery(name = "AtTransaction.findToBeSyncCountByMonthYear", query = "SELECT b.toBeSync,count(b.id)  FROM ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and (b.flags <> 'DELETE' or b.flags is null ) group by b.toBeSync" ),
	@NamedQuery(name = "ATTransactionEntity.findErrorInvoice",query = "Select b from ATTransactionEntity  b where b.gstin=:gstn and b.returnType=:returnType and b.monthYear in (:monthYear) and b.pos=:pos "),
	@NamedQuery(name= "ATTransactionEntity.findErrorInvoicesByTransitId",query="SELECT b from ATTransactionEntity b where b.gstin=:gstn and b.monthYear in (:monthYear) and b.returnType=:returnType and b.transitId=:transitId and b.isError=false"),
    @NamedQuery(name="ATTransactionEntity.getFlagsCount",query="select new com.ginni.easemygst.portal.business.dto.FlagsCounterDTO(b.isSynced,b.toBeSync,b.isTransit,b.isError ,count(b.id)) from ATTransactionEntity b where b.monthYear=:monthYear and b.returnType=:returnType and b.gstin=:gstin group by b.isSynced,b.toBeSync,b.isTransit,b.isError "),
	@NamedQuery(name = "AtTransaction.findErrorDataByMonthYear", query = "select b from ATTransactionEntity b where b.gstin=:taxPayerGstin and b.returnType=:returnType and b.monthYear=:monthYear and b.isError=true and (b.flags <> 'DELETE' or b.flags is null )"),
	@NamedQuery(name = "ATTransactionEntity.UnsyncByMonthYear", query = "update ATTransactionEntity b set b.toBeSync=true where b.gstin=:gstinid and b.monthYear=:monthYear and b.returnType=:returntype"),


})
@JsonSerialize(using=ATDetailSerializer.class)
public class ATTransactionEntity extends CommonAttributesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="gstin")
	private TaxpayerGstin gstin;

	private String monthYear;

	private String returnType;

	private String flags;

	private boolean isAmmendment;

	private boolean isLocked;

	private boolean isMarked;

	private boolean isSynced;

	private boolean isTransit;

	private boolean isValid;

	private String source;

	private String sourceId;

	private String pos;

	private String stateName;

	@Enumerated(EnumType.STRING)
	private SupplyType supplyType;

	@Column(name="syncId")
	private String synchId;

	private String transitId;

	private String type;

	private Double advanceReceived=0.0;
	
	private Double taxAmount=0.0;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="invoiceId", referencedColumnName="id")
	private List<Item> items;
	
	private String ammendmentMonthYear;
	
	private Boolean toBeSync;
	
	private transient String errorMsg;
	
	private transient String errorCd;
	
	private Boolean isError=false;
	
	private String errMsg;
	
	
	
	@Transient 
	private TransactionType transactionType;
	
	@Column(name="originalMonthYear")
	private String originalMonth ;
	
	/*private String originalPos;
	
	private String originalSupplyType;*/
	
	/*public String getOriginalPos() {
		return originalPos;
	}

	public void setOriginalPos(String originalPos) {
		this.originalPos = originalPos;
	}

	public String getOriginalSupplyType() {
		return originalSupplyType;
	}

	public void setOriginalSupplyType(String originalSupplyType) {
		this.originalSupplyType = originalSupplyType;
	}*/

	public String getOriginalMonth() {
		return originalMonth;
	}

	public void setOriginalMonth(String originalMonth) {
		this.originalMonth = originalMonth;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public ATTransactionEntity() {
	}

	public void setAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}


	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}


	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}


	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}


	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}


	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}



	public Double getAdvanceReceived() {
		return advanceReceived;
	}

	public void setAdvanceReceived(Double advanceReceived) {
		this.advanceReceived = advanceReceived;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getFlags() {
		return this.flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public boolean getIsAmmendment() {
		return this.isAmmendment;
	}

	public void setIsAmmendment(boolean isAmmendment) {
		this.isAmmendment = isAmmendment;
	}

	public boolean getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public boolean getIsMarked() {
		return this.isMarked;
	}

	public void setIsMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public boolean getIsSynced() {
		return this.isSynced;
	}

	public void setIsSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}

	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}


	public SupplyType getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(SupplyType supplyType) {
		this.supplyType = supplyType;
	}

	public String getSynchId() {
		return this.synchId;
	}

	public void setSynchId(String synchId) {
		this.synchId = synchId;
	}

	public String getTransitId() {
		return this.transitId;
	}

	public void setTransitId(String transitId) {
		this.transitId = transitId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public TaxpayerGstin getGstin() {
		return gstin;
	}

	public void setGstin(TaxpayerGstin gstin) {
		this.gstin = gstin;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	

	/*@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((ATTransactionEntity) obj).getGstin())
			   .append(this.getReturnType(), ((ATTransactionEntity) obj).getReturnType())
			   .append(this.getMonthYear(), ((ATTransactionEntity) obj).getMonthYear())
			   .append(this.getPos(), ((ATTransactionEntity) obj).getPos())
			   ;               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode()+monthYear.hashCode()+returnType.hashCode());
		return result;
	}*/
	
	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorCd() {
		return errorCd;
	}

	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}

	public ATTransactionEntity(SupplyType supplyType, String id, String pos, String stateName, double advanceReceived,
			double taxAmount, String flags, String type, String source,boolean isSynced,boolean toBeSync,
			boolean isTransit,boolean isError,
			boolean isAmmendment,String originalMonth/*,String originalPos,String originalSupplyType*/
			) {
		this.supplyType=supplyType;
		this.setId(id);
		this.pos=pos;
		this.stateName=stateName;
		this.advanceReceived=advanceReceived;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.source = source;
		this.isSynced=isSynced;
		this.toBeSync=toBeSync;
		this.isTransit=isTransit;
		this.isError=isError;
		this.isAmmendment=isAmmendment;
		this.originalMonth=originalMonth;
		/*this.originalPos=originalPos;
		this.originalSupplyType=originalSupplyType;*/

	}
	
	
	
	//constructor for save to gstn
	public ATTransactionEntity(String returnType,String monthYear,SupplyType supplyType, String id, String pos, String stateName, 
			double advanceReceived,
			double taxAmount, String flags, String type,Item item,boolean isAmmendment) {
		this.supplyType=supplyType;
		this.setId(id);
		this.pos=pos;
		this.stateName=stateName;
		this.advanceReceived=advanceReceived;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.returnType=returnType;
		this.monthYear=monthYear;
		this.isAmmendment=isAmmendment;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}

	}
	
	
	public ATTransactionEntity(String returnType,SupplyType supplyType, String id, String pos, String stateName, double advanceReceived,
			double taxAmount, String flags, String type,Item item,boolean isAmendment,String originalMonth) {
		this.supplyType=supplyType;
		this.setId(id);
		this.pos=pos;
		this.stateName=stateName;
		this.advanceReceived=advanceReceived;
		this.taxAmount = taxAmount;
		this.flags = flags;
		this.type = type;
		this.returnType=returnType;
		//this.monthYear=monthYear;
		this.isAmmendment=isAmendment;
		this.originalMonth=originalMonth;
		if(Objects.isNull(items)){
			this.items=new ArrayList<>();
			this.getItems().add(item);
			
		}else{
			this.getItems().add(item);
		}

	}

	public String getAmmendmentMonthYear() {
		return ammendmentMonthYear;
	}

	public void setAmmendmentMonthYear(String ammendmentMonthYear) {
		this.ammendmentMonthYear = ammendmentMonthYear;
	}

	public Boolean getToBeSync() {
		return toBeSync;
	}

	public void setToBeSync(Boolean toBeSync) {
		this.toBeSync = toBeSync;
	}
	
	public TransactionType getTransactionType() {
		return TransactionType.AT;
	}


}