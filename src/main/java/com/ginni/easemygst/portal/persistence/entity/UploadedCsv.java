package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;

/**
 * The persistent class for the uploaded_csv database table.
 * 
 */
@Entity
@Table(name = "uploaded_csv")
@NamedQueries({ @NamedQuery(name = "UploadedCsv.findAll", query = "SELECT u FROM UploadedCsv u"),
		@NamedQuery(name = "GSTR1_B2B.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE u.gstApplicability='OUTWARD SUPPLY' and u.cpGstinNo IS NOT NULL and u.cpGstinStateCode IS NOT NULL "),
		@NamedQuery(name = "GSTR1_B2CL.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE u.gstApplicability='OUTWARDSUPPLY' and u.cpGstinNo IS  NULL and (u.tradeType<>'EXPORT' and u.tradeType<>'IMPORT' ) AND u.netAmount>'250000' and u.cpGstinStateCode<>:stateCode and u.cpGstinStateCode IS NOT NULL "),
		@NamedQuery(name = "GSTR1_B2CS.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE u.gstApplicability='OUTWARDSUPPLY' and u.cpGstinNo IS  NULL and (u.tradeType<>'EXPORT' and u.tradeType<>'IMPORT' )  and u.cpGstinStateCode<>:stateCode "),
		@NamedQuery(name = "GSTR1_EXP.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE (u.gstApplicability='OUTWARDSUPPLY' and u.gstApplicability='NOT_APPLICABLE' ) and u.cpGstinNo IS  NULL and (u.tradeType='EXPORT' or u.tradeType='IMPORT' )  and u.cpGstinStateCode<>:stateCode and u.cpGstinStateCode IS NOT NULL "),
		@NamedQuery(name = "GSTR1_NIL.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE u.gstApplicability='NOT_APPLICABLE SALES' and u.cpGstinNo IS  NULL and u.hsnSacCode='NON-GST' "),
		@NamedQuery(name = "GSTR1_CDN.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE (u.gstApplicability='SALES CREDIT NOTE' or u.gstApplicability='SALES DEBIT NOTE' ) and u.cpGstinNo IS NOT NULL and u.cpGstinStateCode IS NOT NULL "),
		@NamedQuery(name = "GSTR2_B2B.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE u.gstApplicability='OUTWARD SUPPLY' and u.cpGstinNo IS NOT NULL and u.cpGstinStateCode IS NOT NULL "),
		@NamedQuery(name = "GSTR2_CDN.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE(u.gstApplicability='PURCHASE CREDIT NOTE' or u.gstApplicability='PURCHASE DEBIT NOTE' ) and u.cpGstinNo IS NOT NULL and u.cpGstinStateCode IS NOT NULL "),
		@NamedQuery(name = "GSTR2_NIL.findByDataUploadInfoId", query = "SELECT u FROM UploadedCsv u WHERE u.gstApplicability='NOT_APPLICABLE PURCHASE' and u.cpGstinNo IS  NULL and u.hsnSacCode='NON-GST' "),

})
public class UploadedCsv implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "CESS_AMOUNT")
	private String cessAmount;

	@Column(name = "CESS_RATE")
	private String cessRate;

	@Column(name = "CGST_AMOUNT")
	private String cgstAmount;

	@Column(name = "CGST_RATE")
	private String cgstRate;

	@Column(name = "CP_AGENT_CLASS_NAME")
	private String cpAgentClassName;

	@Column(name = "CP_CLASS_NAME")
	private String cpClassName;

	@Column(name = "CP_EMAIL")
	private String cpEmail;

	@Column(name = "CP_GSTIN_NO")
	private String cpGstinNo;

	@Column(name = "CP_GSTIN_REGISTRATION_DATE")
	private String cpGstinRegistrationDate;

	@Column(name = "CP_GSTIN_STATE_CODE")
	private String cpGstinStateCode;

	@Column(name = "CP_MOBILE_NO")
	private String cpMobileNo;

	@Column(name = "CP_NAME")
	private String cpName;

	@Column(name = "CP_PANCARD_NO")
	private String cpPancardNo;

	@Column(name = "CP_REGISTRATION_STATUS")
	private String cpRegistrationStatus;

	@Column(name = "DNCN_REASON")
	private String dncnReason;

	@Column(name = "ENTRY_DATE")
	private String entryDate;

	@Column(name = "ENTRY_NO")
	private String entryNo;

	@Column(name = "ENTRY_STATUS")
	private String entryStatus;

	@Column(name = "ENTRY_TYPE")
	private String entryType;

	private String etin;

	@Column(name = "GST_APPLICABILITY")
	private String gstApplicability;

	@Column(name = "HSN_SAC_CODE")
	private String hsnSacCode;

	@Column(name = "HSN_SAC_DESC")
	private String hsnSacDesc;

	@Column(name = "IGST_AMOUNT")
	private String igstAmount;

	@Column(name = "IGST_RATE")
	private String igstRate;

	@Column(name = "INPUT_CESS_AMOUNT")
	private String inputCessAmount;

	@Column(name = "INPUT_CGST_AMOUNT")
	private String inputCgstAmount;

	@Column(name = "INPUT_ELIGIBILITY")
	private String inputEligibility;

	@Column(name = "INPUT_IGST_AMOUNT")
	private String inputIgstAmount;

	@Column(name = "INPUT_SGST_AMOUNT")
	private String inputSgstAmount;

	@Column(name = "NET_AMOUNT")
	private String netAmount;

	@Column(name = "ORIGINAL_INV_ENT_DT")
	private String originalInvEntDt;

	@Column(name = "ORIGINAL_INV_ENT_NO")
	private String originalInvEntNo;

	@Column(name = "ORIGINAL_INV_ENT_REFDT")
	private String originalInvEntRefdt;

	@Column(name = "ORIGINAL_INV_ENT_REFNO")
	private String originalInvEntRefno;

	@Column(name = "REF_DOC_DATE")
	private String refDocDate;

	@Column(name = "REF_DOC_NO")
	private String refDocNo;

	@Column(name = "SGST_AMOUNT")
	private String sgstAmount;

	@Column(name = "SGST_RATE")
	private String sgstRate;

	@Column(name = "TAXABLE_AMOUNT")
	private String taxableAmount;

	@Column(name = "TOTAL_TAXRATE")
	private String totalTaxrate;

	private String tradeType;

	private String uom;

	private String quantity;

	private int uploadDataInfoId;

	private String traderTypeNil;

	private String ctinAvailability;

	private String hsnSacNil;

	private int lineNumber;

	private boolean isError;

	private String errorMsg;

	private String subType;

	private String invoiceType;

	public UploadedCsv() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCessAmount() {
		return this.cessAmount;
	}

	public void setCessAmount(String cessAmount) {
		this.cessAmount = cessAmount;
	}

	public String getCessRate() {
		return this.cessRate;
	}

	public void setCessRate(String cessRate) {
		this.cessRate = cessRate;
	}

	public String getCgstAmount() {
		return this.cgstAmount;
	}

	public void setCgstAmount(String cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public String getCgstRate() {
		return this.cgstRate;
	}

	public void setCgstRate(String cgstRate) {
		this.cgstRate = cgstRate;
	}

	public String getCpAgentClassName() {
		return this.cpAgentClassName;
	}

	public void setCpAgentClassName(String cpAgentClassName) {
		this.cpAgentClassName = cpAgentClassName;
	}

	public String getCpClassName() {
		return this.cpClassName;
	}

	public void setCpClassName(String cpClassName) {
		this.cpClassName = cpClassName;
	}

	public String getCpEmail() {
		return this.cpEmail;
	}

	public void setCpEmail(String cpEmail) {
		this.cpEmail = cpEmail;
	}

	public String getCpGstinNo() {
		return this.cpGstinNo;
	}

	public void setCpGstinNo(String cpGstinNo) {
		this.cpGstinNo = cpGstinNo;
	}

	public String getCpGstinRegistrationDate() {
		return this.cpGstinRegistrationDate;
	}

	public void setCpGstinRegistrationDate(String cpGstinRegistrationDate) {
		this.cpGstinRegistrationDate = cpGstinRegistrationDate;
	}

	public String getCpGstinStateCode() {
		return this.cpGstinStateCode;
	}

	public void setCpGstinStateCode(String cpGstinStateCode) {
		this.cpGstinStateCode = cpGstinStateCode;
	}

	public String getCpMobileNo() {
		return this.cpMobileNo;
	}

	public void setCpMobileNo(String cpMobileNo) {
		this.cpMobileNo = cpMobileNo;
	}

	public String getCpName() {
		return this.cpName;
	}

	public void setCpName(String cpName) {
		this.cpName = cpName;
	}

	public String getCpPancardNo() {
		return this.cpPancardNo;
	}

	public void setCpPancardNo(String cpPancardNo) {
		this.cpPancardNo = cpPancardNo;
	}

	public String getCpRegistrationStatus() {
		return this.cpRegistrationStatus;
	}

	public void setCpRegistrationStatus(String cpRegistrationStatus) {
		this.cpRegistrationStatus = cpRegistrationStatus;
	}

	public String getDncnReason() {
		return this.dncnReason;
	}

	public void setDncnReason(String dncnReason) {
		this.dncnReason = dncnReason;
	}

	public String getEntryDate() {

		return this.parseDate(this.entryDate);
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getEntryNo() {
		return this.entryNo;
	}

	public void setEntryNo(String entryNo) {
		this.entryNo = entryNo;
	}

	public String getEntryStatus() {
		return this.entryStatus;
	}

	public void setEntryStatus(String entryStatus) {
		this.entryStatus = entryStatus;
	}

	public String getEntryType() {
		return this.entryType;
	}

	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}

	public String getEtin() {
		return this.etin;
	}

	public void setEtin(String etin) {
		this.etin = etin;
	}

	public String getGstApplicability() {
		return this.gstApplicability;
	}

	public void setGstApplicability(String gstApplicability) {
		this.gstApplicability = gstApplicability;
	}

	public String getHsnSacCode() {
		return this.hsnSacCode;
	}

	public void setHsnSacCode(String hsnSacCode) {
		this.hsnSacCode = hsnSacCode;
	}

	public String getHsnSacDesc() {
		return this.hsnSacDesc;
	}

	public void setHsnSacDesc(String hsnSacDesc) {
		this.hsnSacDesc = hsnSacDesc;
	}

	public String getIgstAmount() {
		return this.igstAmount;
	}

	public void setIgstAmount(String igstAmount) {
		this.igstAmount = igstAmount;
	}

	public String getIgstRate() {
		return this.igstRate;
	}

	public void setIgstRate(String igstRate) {
		this.igstRate = igstRate;
	}

	public String getInputCessAmount() {
		return this.inputCessAmount;
	}

	public void setInputCessAmount(String inputCessAmount) {
		this.inputCessAmount = inputCessAmount;
	}

	public String getInputCgstAmount() {
		return this.inputCgstAmount;
	}

	public void setInputCgstAmount(String inputCgstAmount) {
		this.inputCgstAmount = inputCgstAmount;
	}

	public String getInputEligibility() {
		return this.inputEligibility;
	}

	public void setInputEligibility(String inputEligibility) {
		this.inputEligibility = inputEligibility;
	}

	public String getInputIgstAmount() {
		return this.inputIgstAmount;
	}

	public void setInputIgstAmount(String inputIgstAmount) {
		this.inputIgstAmount = inputIgstAmount;
	}

	public String getInputSgstAmount() {
		return this.inputSgstAmount;
	}

	public void setInputSgstAmount(String inputSgstAmount) {
		this.inputSgstAmount = inputSgstAmount;
	}

	public String getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getOriginalInvEntDt() {

		return this.parseDate(this.originalInvEntDt);
	}

	public void setOriginalInvEntDt(String originalInvEntDt) {
		this.originalInvEntDt = originalInvEntDt;
	}

	public String getOriginalInvEntNo() {
		return this.originalInvEntNo;
	}

	public void setOriginalInvEntNo(String originalInvEntNo) {
		this.originalInvEntNo = originalInvEntNo;
	}

	public String getOriginalInvEntRefdt() {
		return this.parseDate(this.originalInvEntRefdt);
	}

	public void setOriginalInvEntRefdt(String originalInvEntRefdt) {
		this.originalInvEntRefdt = originalInvEntRefdt;
	}

	public String getOriginalInvEntRefno() {
		return this.originalInvEntRefno;
	}

	public void setOriginalInvEntRefno(String originalInvEntRefno) {
		this.originalInvEntRefno = originalInvEntRefno;
	}

	public String getRefDocDate() {

		return this.parseDate(this.refDocDate);
	}

	public void setRefDocDate(String refDocDate) {
		this.refDocDate = refDocDate;
	}

	public String getRefDocNo() {
		return this.refDocNo;
	}

	public void setRefDocNo(String refDocNo) {
		this.refDocNo = refDocNo;
	}

	public String getSgstAmount() {
		return this.sgstAmount;
	}

	public void setSgstAmount(String sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public String getSgstRate() {
		return this.sgstRate;
	}

	public void setSgstRate(String sgstRate) {
		this.sgstRate = sgstRate;
	}

	public String getTaxableAmount() {
		return this.taxableAmount;
	}

	public void setTaxableAmount(String taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public String getTotalTaxrate() {
		return this.totalTaxrate;
	}

	public void setTotalTaxrate(String totalTaxrate) {
		this.totalTaxrate = totalTaxrate;
	}

	public String getTradeType() {
		return this.tradeType;
	}

	public void setTradeType(String tradetype) {
		this.tradeType = tradetype;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public int getUploadDataInfoId() {
		return uploadDataInfoId;
	}

	public void setUploadDataInfoId(int uploadDataInfoId) {
		this.uploadDataInfoId = uploadDataInfoId;
	}

	public String getTraderTypeNil() {
		return traderTypeNil;
	}

	public void setTraderTypeNil(String traderTypeNil) {
		this.traderTypeNil = traderTypeNil;
	}

	public String getCtinAvailability() {
		return ctinAvailability;
	}

	public void setCtinAvailability(String ctinAvailability) {
		this.ctinAvailability = ctinAvailability;
	}

	public String getHsnSacNil() {
		return hsnSacNil;
	}

	public void setHsnSacNil(String hsnSacNil) {
		this.hsnSacNil = hsnSacNil;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	private String parseDate(String date) {

		SimpleDateFormat sdfCsv = new SimpleDateFormat("dd-MMM-yy");
		SimpleDateFormat sdfPortal = new SimpleDateFormat("dd/MM/yyyy");

		Date td = null;
		if (StringUtils.isNoneEmpty(date)) {
			try {
				td = sdfCsv.parse(date);
			} catch (ParseException e) {
				return date;
			}
			return sdfPortal.format(td);
		}
		return date;

	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

}