package com.ginni.easemygst.portal.persistence.entity.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ginni.easemygst.portal.persistence.entity.EncodedData;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;

@Entity
@Table(name = "token_response")
@NamedQueries({ @NamedQuery(name = "TokenResponse.findAll", query = "SELECT i from TokenResponse i"),
		@NamedQuery(name = "TokenResponse.findByGstin", query = "SELECT r from TokenResponse r where r.status='PENDING' and r.taxpayerGstin=:gstn"
				+ " and r.monthYear=:monthYear and r.returnType=:returnType and r.transaction=:trans "),
		@NamedQuery(name = "TokenResponse.findById", query = "SELECT i from TokenResponse i where i.id=:id"),
		@NamedQuery(name = "TokenResponse.findByGstinMonthYear", query = "SELECT i from TokenResponse i where i.taxpayerGstin=:gstn and i.monthYear=:monthYear order by i.updationTime desc"),
		@NamedQuery(name = "TokenResponse.findByGstinMonthYearTransaction", query = "SELECT new com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse(i.status,i.updationTime) from TokenResponse i where i.taxpayerGstin=:taxpayerGstin and i.monthYear=:monthYear and i.transaction=:transactionType and i.returnType=:returnType order by i.updationTime desc"),
@NamedQuery(name = "TokenResponse.findLatestGstinMonthYear", query = "SELECT new com.ginni.easemygst.portal.persistence.entity.transaction.TokenResponse(i.status,i.updationTime) from TokenResponse i where i.taxpayerGstin=:taxpayerGstin and i.monthYear=:monthYear and i.returnType=:returnType order by i.updationTime desc")

})
public class TokenResponse implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "gstin")
	private TaxpayerGstin taxpayerGstin;

	@JsonIgnore
	@OneToMany(mappedBy = "referenceId", fetch = FetchType.LAZY)
	private List<EncodedData> encodedData;

	private String returnType;

	private String monthYear;

	private String status;

	private String validity;

	private String token;

	private Timestamp creationTime;

	private String transaction;

	private Timestamp updationTime;

	private Integer urlcount;
	
	public TokenResponse() {
	}
	
	public TokenResponse(String status,Date updationTime) {
		this.status=status;
		this.updationTime=Timestamp.from(updationTime.toInstant());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		this.taxpayerGstin = taxpayerGstin;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public Timestamp getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public Integer getUrlcount() {
		return urlcount;
	}

	public void setUrlcount(Integer urlcount) {
		this.urlcount = urlcount;
	}

}
