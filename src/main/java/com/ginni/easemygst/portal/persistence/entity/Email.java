package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the emails database table.
 * 
 */
@Entity
@Table(name="emails")
@NamedQuery(name="Email.findAll", query="SELECT e FROM Email e")
public class Email implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String attachments;

	private String emailevent;

	private String emailtype;

	private Timestamp generationTime;

	private String mailbcc;

	@Lob
	private String mailbody;

	private String mailcc;

	private String mailsubject;

	private String mailto;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedtime;

	private Timestamp processedTime;

	private String remarks;

	private int status;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="userId")
	private User user;

	public Email() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}

	public String getEmailevent() {
		return this.emailevent;
	}

	public void setEmailevent(String emailevent) {
		this.emailevent = emailevent;
	}

	public String getEmailtype() {
		return this.emailtype;
	}

	public void setEmailtype(String emailtype) {
		this.emailtype = emailtype;
	}

	public Timestamp getGenerationTime() {
		return this.generationTime;
	}

	public void setGenerationTime(Timestamp generationTime) {
		this.generationTime = generationTime;
	}

	public String getMailbcc() {
		return this.mailbcc;
	}

	public void setMailbcc(String mailbcc) {
		this.mailbcc = mailbcc;
	}

	public String getMailbody() {
		return this.mailbody;
	}

	public void setMailbody(String mailbody) {
		this.mailbody = mailbody;
	}

	public String getMailcc() {
		return this.mailcc;
	}

	public void setMailcc(String mailcc) {
		this.mailcc = mailcc;
	}

	public String getMailsubject() {
		return this.mailsubject;
	}

	public void setMailsubject(String mailsubject) {
		this.mailsubject = mailsubject;
	}

	public String getMailto() {
		return this.mailto;
	}

	public void setMailto(String mailto) {
		this.mailto = mailto;
	}

	public Date getModifiedtime() {
		return this.modifiedtime;
	}

	public void setModifiedtime(Date modifiedtime) {
		this.modifiedtime = modifiedtime;
	}

	public Timestamp getProcessedTime() {
		return this.processedTime;
	}

	public void setProcessedTime(Timestamp processedTime) {
		this.processedTime = processedTime;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}