package com.ginni.easemygst.portal.persistence.convertor;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.data.transaction.CDN.CdnData;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;

public class B2bDetailEntityJsonConverter implements AttributeConverter<B2BDetailEntity,String>{

	private final static ObjectMapper _Mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(B2BDetailEntity attribute) {
		try {
			return _Mapper.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;	
	}

	@Override
	public B2BDetailEntity convertToEntityAttribute(String dbData) {

		if(StringUtils.isEmpty(dbData))
			return null;
		try {
			return _Mapper.readValue(dbData,B2BDetailEntity.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
