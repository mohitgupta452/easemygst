package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the organisation database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name="Organisation.findAll", query="SELECT o FROM Organisation o"),
	@NamedQuery(name="Organisation.findBySubscriptionId", query="SELECT o FROM Organisation o WHERE o.subscriptionId=:id"),
	@NamedQuery(name="Organisation.findByCustomerId", query="SELECT o FROM Organisation o WHERE o.customerId=:id")})
public class Organisation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private Date activationDate;

	private String billingAddress;

	private String code;
	
	private Byte cardUpdate;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private String creationIpAddress;

	@JsonIgnore
	private Timestamp creationTime;

	private String customerId;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date lastBilling;
	
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	
	@Temporal(TemporalType.DATE)
	private Date expiresAt;
	
	private String intervalUnits;
	
	private String discountAmount;
	
	private String totalAmount;
	
	@Temporal(TemporalType.DATE)
	private Date currentTermEndsAt;	

	private String salesPerson;
	
	private String salesPersonId;
	
	private Boolean autoCollect;
	
	private String status;
	
	private String name;

	@Temporal(TemporalType.DATE)
	private Date nextBilling;

	private String shippingAddress;

	private String subscriptionId;

	private String subscriptionMode;

	private String subscriptionType;

	private String unitCost;
	
	private String gstin;
	
	private String planCode;
	
	private String planDetails;

	private Integer units;
	
	private Integer invoiceCount;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private String updationIpAddress;

	@JsonIgnore
	private Timestamp updationTime;

	public Organisation() {
	}

	public Date getActivationDate() {
		return this.activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public String getBillingAddress() {
		return this.billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getLastBilling() {
		return this.lastBilling;
	}

	public void setLastBilling(Date lastBilling) {
		this.lastBilling = lastBilling;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getNextBilling() {
		return this.nextBilling;
	}

	public void setNextBilling(Date nextBilling) {
		this.nextBilling = nextBilling;
	}

	public String getShippingAddress() {
		return this.shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getSubscriptionId() {
		return this.subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Byte getCardUpdate() {
		return cardUpdate;
	}

	public void setCardUpdate(Byte cardUpdate) {
		this.cardUpdate = cardUpdate;
	}

	public String getSubscriptionMode() {
		return this.subscriptionMode;
	}

	public void setSubscriptionMode(String subscriptionMode) {
		this.subscriptionMode = subscriptionMode;
	}

	public String getSubscriptionType() {
		return this.subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String getUnitCost() {
		return this.unitCost;
	}

	public void setUnitCost(String unitCost) {
		this.unitCost = unitCost;
	}

	public Integer getUnits() {
		return this.units;
	}

	public void setUnits(Integer units) {
		this.units = units;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getPlanDetails() {
		return planDetails;
	}

	public void setPlanDetails(String planDetails) {
		this.planDetails = planDetails;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	public String getIntervalUnits() {
		return intervalUnits;
	}

	public void setIntervalUnits(String intervalUnits) {
		this.intervalUnits = intervalUnits;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getCurrentTermEndsAt() {
		return currentTermEndsAt;
	}

	public void setCurrentTermEndsAt(Date currentTermEndsAt) {
		this.currentTermEndsAt = currentTermEndsAt;
	}

	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public String getSalesPersonId() {
		return salesPersonId;
	}

	public void setSalesPersonId(String salesPersonId) {
		this.salesPersonId = salesPersonId;
	}

	public Boolean getAutoCollect() {
		return autoCollect;
	}

	public void setAutoCollect(Boolean autoCollect) {
		this.autoCollect = autoCollect;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getInvoiceCount() {
		return invoiceCount;
	}

	public void setInvoiceCount(Integer invoiceCount) {
		this.invoiceCount = invoiceCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Organisation other = (Organisation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}