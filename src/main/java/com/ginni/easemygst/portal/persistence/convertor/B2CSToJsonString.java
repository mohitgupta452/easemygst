package com.ginni.easemygst.portal.persistence.convertor;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.data.transaction.B2CS;

@Converter(autoApply=true)
public class B2CSToJsonString implements AttributeConverter<B2CS,String> {

	private final static ObjectMapper _Mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(B2CS attribute) {
		try {
			return _Mapper.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public B2CS convertToEntityAttribute(String dbData) {
		try {
			return _Mapper.readValue(dbData,B2CS.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
