package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the return_transaction database table.
 * 
 */
@Entity
@Table(name="return_transaction")
@NamedQuery(name="ReturnTransaction.findAll", query="SELECT r FROM ReturnTransaction r")
public class ReturnTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private byte active;

	private String code;

	private String createdBy;

	private String creationIpAddress;

	private Timestamp creationTime;

	@Lob
	private String description;

	private String name;

	private String updatedBy;

	private String updationIpAddress;

	private Timestamp updationTime;

	//bi-directional many-to-one association to BrtMapping
	@JsonIgnore
	@OneToMany(mappedBy="returnTransaction")
	private List<BrtMapping> brtMappings;

	public ReturnTransaction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationIpAddress() {
		return this.creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIpAddress() {
		return this.updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public List<BrtMapping> getBrtMappings() {
		return this.brtMappings;
	}

	public void setBrtMappings(List<BrtMapping> brtMappings) {
		this.brtMappings = brtMappings;
	}

	public BrtMapping addBrtMapping(BrtMapping brtMapping) {
		getBrtMappings().add(brtMapping);
		brtMapping.setReturnTransaction(this);

		return brtMapping;
	}

	public BrtMapping removeBrtMapping(BrtMapping brtMapping) {
		getBrtMappings().remove(brtMapping);
		brtMapping.setReturnTransaction(null);

		return brtMapping;
	}

}