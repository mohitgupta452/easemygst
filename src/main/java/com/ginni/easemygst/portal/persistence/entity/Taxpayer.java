package com.ginni.easemygst.portal.persistence.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the taxpayer database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name="Taxpayer.findAll", query="SELECT t FROM Taxpayer t"),
	@NamedQuery(name = "Taxpayer.findByPan", query = "SELECT t FROM Taxpayer t where t.active=1 and t.panCard=:pan"),
	@NamedQuery(name = "Taxpayer.findByIdentity", query = "SELECT t FROM Taxpayer t where t.active=1 and t.panCard=:pan and t.id in (select ut.taxpayer.id from UserTaxpayer ut where ut.user.id=:id and ut.type=:type)"),
	@NamedQuery(name = "Taxpayer.findByIdentityType", query = "SELECT t FROM Taxpayer t where t.active=1 and t.id in (select ut.taxpayer.id from UserTaxpayer ut where ut.user.id=:id and ut.type=:type)")})
public class Taxpayer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Byte active;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp creationTime;
	
	@JsonIgnore
	private String creationIpAddress;
	
	private String displayLogo;

	private String emailAddress;

	private Byte emailAuth;

	private String legalName;

	private Byte mobileAuth;

	private String mobileNumber;
	
	private String displayName;

	private String panCard;

	private String status;

	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private Timestamp updationTime;
	
	@JsonIgnore
	private String updationIpAddress;
	
	private String requestId;

	//bi-directional many-to-one association to TaxpayerGstin
	@OneToMany(mappedBy="taxpayer", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<TaxpayerGstin> taxpayerGstins;
	
	public Taxpayer() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Byte getActive() {
		return this.active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public String getDisplayLogo() {
		return displayLogo;
	}

	public void setDisplayLogo(String displayLogo) {
		this.displayLogo = displayLogo;
	}

	public String getEmailAddress() {
		if(!StringUtils.isEmpty(this.emailAddress)) return this.emailAddress.toLowerCase();
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		if(!StringUtils.isEmpty(emailAddress)) emailAddress = emailAddress.toLowerCase();
		this.emailAddress = emailAddress;
	}

	public Object getEmailAuth() {
		return this.emailAuth;
	}

	public void setEmailAuth(Byte emailAuth) {
		this.emailAuth = emailAuth;
	}

	public String getLegalName() {
		return this.legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public Object getMobileAuth() {
		return this.mobileAuth;
	}

	public void setMobileAuth(Byte mobileAuth) {
		this.mobileAuth = mobileAuth;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPanCard() {
		if(!StringUtils.isEmpty(this.panCard)) this.panCard = this.panCard.toUpperCase();
		return this.panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getCreationIpAddress() {
		return creationIpAddress;
	}

	public void setCreationIpAddress(String creationIpAddress) {
		this.creationIpAddress = creationIpAddress;
	}

	public String getUpdationIpAddress() {
		return updationIpAddress;
	}

	public void setUpdationIpAddress(String updationIpAddress) {
		this.updationIpAddress = updationIpAddress;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<TaxpayerGstin> getTaxpayerGstins() {
		//if(!Objects.isNull(this.taxpayerGstins)) this.taxpayerGstins.removeIf(u->u.getActive()==0);
		return this.taxpayerGstins;
	}

	public void setTaxpayerGstins(List<TaxpayerGstin> taxpayerGstins) {
		this.taxpayerGstins = taxpayerGstins;
	}

	public TaxpayerGstin addTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		getTaxpayerGstins().add(taxpayerGstin);
		taxpayerGstin.setTaxpayer(this);

		return taxpayerGstin;
	}

	public TaxpayerGstin removeTaxpayerGstin(TaxpayerGstin taxpayerGstin) {
		getTaxpayerGstins().remove(taxpayerGstin);
		taxpayerGstin.setTaxpayer(null);

		return taxpayerGstin;
	}
	
}