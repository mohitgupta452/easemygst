package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class RevisionInvDetails {

	@JsonProperty("ty")
	private String ty;
	
	@JsonProperty("docty")
	private String docty;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("val")
	private Double val=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	private Double iamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
	private Double camt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("csamt")
	private Double csamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
	private Double samt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tc_i")
	private Double tc_i=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tc_c")
	private Double tc_c=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tc_s")
	private Double tc_s=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tc_cs")
	private Double tc_cs=0.0;

}
