package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerCashSummary {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonFormat(pattern = "DD-MM-YYY")
	@JsonProperty("fr_dt")
	private String fromDate;
	
	@JsonFormat(pattern = "DD-MM-YYY")
	@JsonProperty("to_dt")
	private String toDate;
	
	@JsonProperty("cr")
	private CreditITC credit;
	
	@JsonProperty("db")
	private DebitITC debit;
	
	@JsonProperty("op_bal")
	private LedgerLiabilityTransactionDetail openingBalance;
	
	@JsonProperty("cl_bal")
	private LedgerLiabilityTransactionDetail closingBalance;
	
	
}
