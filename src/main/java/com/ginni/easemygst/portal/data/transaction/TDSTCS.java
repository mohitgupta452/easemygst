package com.ginni.easemygst.portal.data.transaction;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.Audit;
import com.ginni.easemygst.portal.data.Comments;
import com.ginni.easemygst.portal.data.TaxAmount;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @lombok.Data class TDSTCS {

	@JsonProperty("tds_id")
	String id;

	@JsonProperty("ctin")
	private String deductorTin;

	@JsonProperty("etin")
	private String ecommerceTin;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tds_value")
	private Double tdsValue;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("gross_value")
	private Double grossValue;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("sales_return")
	private Double salesReturn;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")
	private Double taxAmount;

	@JsonProperty("tds_invoices")
	public List<TdsInvoice> tdsInvoices;

	@JsonInclude(Include.NON_NULL)
	public @Data static class TdsInvoice extends TaxAmount {

		@JsonProperty("flag")
		public TaxpayerAction taxPayerAction = TaxpayerAction.UPLOADED;;

		private ReconsileType type;

		@JsonProperty("invoice_id")
		String id;

		private boolean isAmendment;

		private boolean isValid = true;

		private Map<String, String> error;

		private boolean isGstnSynced = false;

		private boolean toBeSynced = false;

		private String syncId;

		private boolean isLocked = true;

		private String reconcileFlags;

		private String mismatchedInvoice;

		private List<Comments> comments;

		private List<Audit> audits;

		@JsonProperty("chksum")
		public String checkSum;

		private String flags = "";

		private String source;

		private String sourceId;

		private boolean isTransit = false;

		private String transitId = "";

		private boolean isMarked = false; // for marking pupose

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("net_val")
		public Double netVal;
		
			

	}
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getDeductorTin(), ((TDSTCS) obj).getDeductorTin());               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deductorTin== null) ? 0 : deductorTin.hashCode());
		return result;
	}

}
