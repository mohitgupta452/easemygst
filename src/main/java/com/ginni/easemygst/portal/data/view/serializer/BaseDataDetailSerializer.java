package com.ginni.easemygst.portal.data.view.serializer;


import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public abstract class BaseDataDetailSerializer<T> extends JsonSerializer<T> {

	public final static SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");
	
	
	

	public void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			json.writeStringField("itemId", item.getId());
			
			json.writeNumberField("num", item.getSerialNumber());

			
			json.writeStringField("hsn_sc", !StringUtils.isEmpty(item.getHsnCode())?item.getHsnCode():"");
			
			json.writeStringField("hsn_desc", !StringUtils.isEmpty(item.getHsnDescription())?item.getHsnDescription():"");
			
			json.writeStringField("unit", !StringUtils.isEmpty(item.getUnit())?item.getUnit():"");
			
			json.writeNumberField("quantity", item.getQuantity());

			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("txval", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			if (!Objects.isNull(item.getCgst()))
				json.writeNumberField("camt", item.getCgst());

			if (!Objects.isNull(item.getSgst()))
				json.writeNumberField("samt", item.getSgst());

			if (!Objects.isNull(item.getCess()))
				json.writeNumberField("csamt", item.getCess());

				//if (!StringUtils.isEmpty(item.getTotalEligibleTax())) {
					json.writeStringField("elg",
							item.getTotalEligibleTax() != null ? String.valueOf(item.getTotalEligibleTax()) : null);

					/*// writing itc detail
					json.writeFieldName("itc");// adding itc detail field
*/					this.writeItcDetail(json, item);
				//}

			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeItcDetail(JsonGenerator json, Item item) {
		try {
			// start itcdetail
			//json.writeStartObject();
				json.writeNumberField("tx_i",	item.getItcIgst());
				json.writeNumberField("tx_c",	item.getItcCgst());
				json.writeNumberField("tx_s",	item.getItcSgst());
				json.writeNumberField("tx_cs",	item.getItcCess());
			
		//	json.writeEndObject();
			// end itc detail
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



}
