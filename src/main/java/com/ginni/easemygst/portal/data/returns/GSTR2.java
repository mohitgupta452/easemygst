package com.ginni.easemygst.portal.data.returns;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.B2BDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNDeserializer;
import com.ginni.easemygst.portal.data.part.ITC;
import com.ginni.easemygst.portal.data.part.TLD;
import com.ginni.easemygst.portal.data.serialize.ATSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BURSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNURSerializer;
import com.ginni.easemygst.portal.data.serialize.HSNSUMSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPGSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPSBSerializer;
import com.ginni.easemygst.portal.data.serialize.NILSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.data.transaction.ISD;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.transaction.REVCH;
import com.ginni.easemygst.portal.data.transaction.TDSTCS;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public @lombok.Data class GSTR2 {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonFormat(pattern="MMyyyy")
	@JsonProperty("fp")
	private String monthYear;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("gt")
	private Double grossTurnover;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl")
	private String taxLiability;
	
	@JsonProperty("b2bss")
	@JsonDeserialize(using=B2BDeserializer.class)
	private List<B2BTransactionEntity> b2bs;
	
	@JsonProperty("b2ba")
	private List<B2B> b2bas;
	
	@JsonProperty("impg")
	private List<IMPG> impgas;
	
	@JsonSerialize(using =IMPGSerializer.class)
	@JsonProperty("imp_g")
	private List<IMPGTransactionEntity> impgtrans;
	
	@JsonProperty("imps")
	private List<REVCH> impss;
	
	@JsonProperty("impsa")
	private List<REVCH> impsas;
	
	@JsonSerialize(using=CDNSerializer.class)
	@JsonProperty("cdn")
	private List<CDNDetailEntity> cdn;

	@JsonProperty("cdna")
	private List<CDN> cdna;
	
	@JsonSerialize(using=NILSerializer.class)
	@JsonProperty("nil_supplies")
	private List<NILTransactionEntity> nil;
	
	@JsonProperty("isd")
	private List<ISD> isds;
	
	@JsonProperty("tds")
	private List<TDSTCS> tdss;
	
///*	@JsonProperty("tcs")
//	*/private List<TCS> tcss;
	
	@JsonProperty("itc")
	private List<ITC> itcs;
	
	@JsonSerialize(using=ATSerializer.class)
	@JsonProperty("txi")
	private List<ATTransactionEntity> at;
	
	@JsonProperty("ata")
	private List<AT> atas;
//	
//	@JsonProperty("txpd")
//	private List<TXPD> txpds;
	
//	@JsonProperty("itcrvsl")
//	private List<TXPD> itcrs;
	
	@JsonSerialize(using =HSNSUMSerializer.class)
	@JsonProperty("hsnsum")
	private List<Hsn> hsnsums;
	
	@JsonProperty("txia")
	private List<TLD> taxLib;
	
	//@JsonDeserialize(using=B2B )
	@JsonSerialize(using =B2BURSerializer.class)
	@JsonProperty("b2bur")
	private List<B2bUrTransactionEntity> b2bur;
	
	@JsonSerialize(using =TXPSerializer.class)
	@JsonProperty("txpd")
	private List<TxpdTransaction> txpd;
	
	@JsonSerialize(using =IMPSBSerializer.class)
	@JsonProperty("imp_s")
	private List<IMPSTransactionEntity> imps;
	
	@JsonSerialize(using =CDNURSerializer.class)
	@JsonProperty("cdnur")
	private List<CDNURDetailEntity> cdnur;
	
	@JsonSerialize(using =B2BSerializer.class)
	@JsonProperty("b2b")
	private List<B2BDetailEntity> b2b;
	
}
