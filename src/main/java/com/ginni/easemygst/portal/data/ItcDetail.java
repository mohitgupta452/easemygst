package com.ginni.easemygst.portal.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class ItcDetail {

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tx_i")
    private Double totalTaxAvalIgst;
    
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tx_c")
    private Double totalTaxAvalCgst;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tx_s")
    private Double totalTaxAvalSgst;
    
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tx_cs")
    private Double totalTaxAvalCess;
	
}
