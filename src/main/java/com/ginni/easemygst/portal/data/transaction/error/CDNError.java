package com.ginni.easemygst.portal.data.transaction.error;



import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class CDNError {

	@JsonProperty("ctin")
	private String gstin;
	
	@JsonProperty("typ")
	private String invoiceType;
	
	@JsonProperty("cname")
	private String recpName	;
	
	@JsonProperty("ty")
	private String noteType;
	
	@JsonProperty("nt_num")
	private String noteNo;
	
	@JsonProperty("nt_dt")
	private String noteDate;
	
	@JsonProperty("ont_num")
	private String origNoteNo;
	
	@JsonProperty("ont_dt")
	private String origNoteDate;
	
	@JsonProperty("error_cd")
	private String errorCode;
	
	@JsonProperty("error_msg")
	private String errorMsg;
	
	
	
	
	
	
	
}
