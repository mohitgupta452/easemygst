package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerTax {

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tx")
	private Double tax;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("intr")
	private Double interest;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("pen")
	private Double penalty;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("fee")
	private Double fee;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("oth")
	private Double other;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tot")
	private Double total;
}
