package com.ginni.easemygst.portal.data.part;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3Summary {
	
	@JsonProperty("turn_over_detail")
	private TOD tod;
	
	@JsonProperty("outward_supp")
	private Gstr3TransactionSummary outwardSupp;
	
	@JsonProperty("inward_supp")
	private Gstr3TransactionSummary inwardSupp;
	
	@JsonProperty("tot_tax_liability")
	private Gstr3TransactionSummary totTaxLiability;
	
	@JsonProperty("tcs_credit")
	private Gstr3TransactionSummary tcsCredit;
	
	@JsonProperty("tax_paid")
	private Gstr3TransactionSummary taxPaid;
	
	@JsonProperty("refund_claim")
	private Gstr3TransactionSummary refundClaim;
	
	public @Data static class Gstr3TransactionSummary {
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("igst")
	    public Double igstAmt = new Double(0.00);

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cgst")
	    public Double cgstAmt = new Double(0.00);
	    
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("sgst")
	    public Double sgstAmt = new Double(0.00);
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cess")
	    public Double cessAmt = new Double(0.00);
		
	}

}
