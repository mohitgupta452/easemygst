package com.ginni.easemygst.portal.data.serialize;

import static org.mockito.Matchers.intThat;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.utils.TaxCalcuator;

public class ItemSerializer {
	static RoundingMode _ROUNDING_MODE=RoundingMode.HALF_UP;

	BigDecimal writeLineItems(JsonGenerator json, List<Item> items, ReturnType returnType, TransactionType transType,
			String... flags) throws IOException {
		
		BigDecimal totalTaxableValue=BigDecimal.ZERO;

		String flag = null;
		if (flags != null && flags.length > 0)
			flag = flags[0];

		BigDecimal taxableValue = BigDecimal.ZERO;
		BigDecimal igst = BigDecimal.ZERO;
		BigDecimal cgst = BigDecimal.ZERO;
		BigDecimal sgst = BigDecimal.ZERO;
		BigDecimal cess = BigDecimal.ZERO;
		BigDecimal itcIgst = BigDecimal.ZERO;
		BigDecimal itcCgst = BigDecimal.ZERO;
		BigDecimal itcSgst = BigDecimal.ZERO;
		BigDecimal itcCess = BigDecimal.ZERO;
		String itcEligiblity = null;
		int sNo = 1;

		if (StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag) || "A".equalsIgnoreCase(flag)) {

			// Start Line Items
			json.writeFieldName("itms");
			json.writeStartArray();

			Map<Double, List<Item>> itemsMap = items.stream().collect(Collectors.groupingBy(Item::getTaxRate));

			for (Entry<Double, List<Item>> entry : itemsMap.entrySet()) {
				taxableValue = BigDecimal.ZERO;
				igst = BigDecimal.ZERO;
				cgst = BigDecimal.ZERO;
				sgst = BigDecimal.ZERO;
				cess = BigDecimal.ZERO;
				itcIgst = BigDecimal.ZERO;
				itcCgst = BigDecimal.ZERO;
				itcSgst = BigDecimal.ZERO;
				itcCess = BigDecimal.ZERO;
				itcEligiblity = null;

				for (Item item : entry.getValue()) {
					taxableValue = taxableValue
							.add(BigDecimal.valueOf(item.getTaxableValue()).setScale(2, _ROUNDING_MODE));
					igst = igst.add(BigDecimal.valueOf(item.getIgst()).setScale(2, _ROUNDING_MODE));
					cgst = cgst.add(BigDecimal.valueOf(item.getCgst()).setScale(2, _ROUNDING_MODE));
					sgst = sgst.add(BigDecimal.valueOf(item.getSgst()).setScale(2, _ROUNDING_MODE));
					cess = cess.add(BigDecimal.valueOf(item.getCess()).setScale(2, _ROUNDING_MODE));

					itcIgst = itcIgst.add(BigDecimal.valueOf(item.getItcIgst()).setScale(2, _ROUNDING_MODE));
					itcCgst = itcCgst.add(BigDecimal.valueOf(item.getItcCgst()).setScale(2, _ROUNDING_MODE));
					itcSgst = itcSgst.add(BigDecimal.valueOf(item.getItcSgst()).setScale(2, _ROUNDING_MODE));
					itcCess = itcCess.add(BigDecimal.valueOf(item.getItcCess()).setScale(2, _ROUNDING_MODE));
				
					itcEligiblity = item.getTotalEligibleTax();
					sNo="A".equalsIgnoreCase(flag)? item.getSerialNumber():sNo;
				}
				
				taxableValue=TaxCalcuator.getValidGstTaxableValue(sgst, igst, BigDecimal.valueOf(entry.getKey()), taxableValue);
				totalTaxableValue=totalTaxableValue.add(taxableValue);
				
					json.writeStartObject();// start item

					if (returnType == ReturnType.GSTR2 && transType == TransactionType.TXPD)
						json.writeNumberField("num", sNo++);
					if (returnType == ReturnType.GSTR2 && transType == TransactionType.AT)
						json.writeNumberField("num", sNo++);

					if (transType != TransactionType.AT && transType != TransactionType.TXPD
							&& transType != TransactionType.EXP && TransactionType.B2CSA != transType) {

						json.writeNumberField("num",sNo="A".equalsIgnoreCase(flag)?sNo:++sNo);
						if (StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag)) {
						json.writeFieldName("itm_det");
						json.writeStartObject();// start item detail
						}

					} // adding item detail field

					if (StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag)) {
					json.writeNumberField("rt", entry.getKey());

						if (returnType == ReturnType.GSTR2) {
							if (transType == TransactionType.TXPD || transType == TransactionType.AT)
								json.writeNumberField("adamt", taxableValue);
							else
								json.writeNumberField("txval", taxableValue);
						}
						if (returnType == ReturnType.GSTR1) {
							if (transType == TransactionType.TXPD || transType == TransactionType.AT)
								json.writeNumberField("ad_amt", taxableValue);
							else
								json.writeNumberField("txval", taxableValue);
						}

					if (igst != BigDecimal.ZERO)
						json.writeNumberField("iamt", igst);

					if (/*cgst != BigDecimal.ZERO*/!Objects.isNull(cgst) && cgst.compareTo(BigDecimal.ZERO)!=0)
						json.writeNumberField("camt", cgst);

					if (/*sgst != BigDecimal.ZERO*/!Objects.isNull(sgst) && sgst.compareTo(BigDecimal.ZERO)!=0)
						json.writeNumberField("samt", sgst);

					if (cess != BigDecimal.ZERO)
						json.writeNumberField("csamt", cess);
				}
				if (ReturnType.GSTR2 == returnType) {
					if (transType != TransactionType.TXPD && transType != TransactionType.AT) {
							if(StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag))
						json.writeEndObject();
						if (!StringUtils.isEmpty(itcEligiblity)) {

							// writing itc detail

							json.writeFieldName("itc");// adding itc detail field
							json.writeStartObject();
							json.writeStringField("elg", itcEligiblity.toLowerCase() != null ? String.valueOf(itcEligiblity).toLowerCase() : null);

							json.writeNumberField("tx_i", itcIgst);

							json.writeNumberField("tx_c", itcCgst);

							json.writeNumberField("tx_s", itcSgst);

							json.writeNumberField("tx_cs", itcCess);

							json.writeEndObject();
						}else if("A".equalsIgnoreCase(flag) || (StringUtils.isEmpty(flag) && ReturnType.GSTR2==returnType)) {
							throw new IOException(new AppException(ExceptionCode._INVALID_ITC));
						}

					}
				}

				if (StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag)) {
					if (returnType == ReturnType.GSTR2) {
						if (transType != TransactionType.TXPD && transType != TransactionType.B2BUR
								&& transType != TransactionType.CDNUR && transType != TransactionType.AT
								&& transType != TransactionType.B2B && transType != TransactionType.CDN)
							json.writeEndObject();
					}

					if (returnType == ReturnType.GSTR1) {
						if (transType != TransactionType.AT && transType != TransactionType.EXP
								&& transType != TransactionType.TXPD && transType != TransactionType.B2BUR && TransactionType.B2CSA != transType)
							json.writeEndObject();
					} // end item detail
				}
					
					json.writeEndObject();// end item
				

			}
			json.writeEndArray();
		}
		return totalTaxableValue;

	}

	private void addItc() {

	}
}
