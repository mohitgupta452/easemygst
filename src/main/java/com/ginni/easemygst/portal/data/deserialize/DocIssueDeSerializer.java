package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.record.DConRefRecord;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

public class DocIssueDeSerializer extends BaseDataDeserializer<List<DocIssue>> {
	
private ReturnType returnType = ReturnType.GSTR1;
	
	private TransactionType transType = TransactionType.AT;

	@Override
	public List<DocIssue> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<DocIssue> docs = new ArrayList<>();
		String errorMsg=null;
		String errorCd=null;
		if(node.has("error_cd"))
			errorCd=node.get("error_cd").asText();
	    if(node.has("error_msg"))
	    	errorMsg=node.get("error_msg").asText();	
		
		Iterator<JsonNode> docNodes = node.elements();
		while(docNodes.hasNext()){
			JsonNode docissNode = docNodes.next();
			DocIssue docIssue=new DocIssue();
			docIssue.setError_msg(errorMsg);
			docIssue.setErrorCd(errorCd);
//			if(docissNode.has("doc_num"))
//			 docIssue.setn	
			Iterator<JsonNode> dataNodes=docissNode.get("docs").elements();
			while(dataNodes.hasNext()){
				JsonNode dataNode=dataNodes.next();
				if(dataNode.has("from"))
					docIssue.setSnoFrom(dataNode.get("from").asText());	
				if(dataNode.has("to"))
					docIssue.setSnoTo(dataNode.get("to").asText());
				if(dataNode.has("totnum"))
					docIssue.setTotalNumber(dataNode.get("totnum").asInt());
				if(dataNode.has("cancel"))
					docIssue.setCanceled(dataNode.get("cancel").asInt());
				if(dataNode.has("net_issue"))
					docIssue.setNetIssued(dataNode.get("net_issue").asInt());
				docs.add(docIssue);
			}
		}

		return docs;
	}

	void writeLineDocs(JsonGenerator json, List<Item> items) throws IOException {
		json.writeFieldName("docs");
		json.writeStartArray();
		items.forEach(item -> writeLineItem(json, item));
		json.writeEndArray();
	}

	private void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

//			json.writeStringField("hsn_sc", item.getGoodsOrServiceCode());
			//if (item.getTaxableValue())
				json.writeNumberField("ad_amt",item.getTaxableValue());
			//if (item.getTaxRate() != null)
				json.writeNumberField("rt", item.getTaxRate());
			//if (item.getIgst() != null)
				json.writeNumberField("iamt", item.getIgst());
//			if (item.getCgstAmt() != null)
//				json.writeNumberField("camt", item.getCgstAmt());
//			if (item.getSgstAmt() != null)
//				json.writeNumberField("samt", item.getSgstAmt());
			//if (item.getCess() != null)
				json.writeNumberField("csamt", item.getCess());
			json.writeEndObject();// end item
		} catch (Exception e) {
			/// log
			e.printStackTrace();
		}

	}

}
