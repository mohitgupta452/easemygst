package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

public @lombok.Data class OUTS {

	@JsonProperty("inter")
	private IntrSupReg interStateSuppToRegisteredTaxPayers;
	
	@JsonProperty("intra")
	private ItraSupReg intraStateSuppToRegisteredTaxPayers;

	@JsonProperty("inter_c")
	private IntrSupCon interStateSuppToConsumers;
	
	@JsonProperty("intra_c")
	private ItraSupCon intraStateSuppToConsumers;
	
	@JsonProperty("exp")
	private Export export;

	@JsonProperty("revsup")
	private RevisionInvoiceOUTS revisionOfInvoices;
	
	@JsonProperty("ttxliab")
	private TaxLiabilityOUTS totalTaxLiabilityOnOutSupp;

	

	
}
