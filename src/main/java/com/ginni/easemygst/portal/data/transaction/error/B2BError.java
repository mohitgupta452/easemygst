package com.ginni.easemygst.portal.data.transaction.error;



import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class B2BError {

	@JsonProperty("ctin")
	private String gstin;
	
	@JsonProperty("inv_num")
	private String invoiceNo;
	
	@JsonProperty("date")
	private String date;
	
	@JsonProperty("onum")
	private String originalInvoiceNo;
	
	@JsonProperty("odt")
	private String originaldate;
	
	@JsonProperty("error_cd")
	private String errorCode;
	
	@JsonProperty("error_msg")
	private String errorMsg;
	
	
	
	
	
	
	
}
