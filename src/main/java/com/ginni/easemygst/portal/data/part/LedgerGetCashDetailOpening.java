package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerGetCashDetailOpening {

	@JsonProperty("desc")
	private String description;
		
	@JsonProperty("op_tot")
	private String openingTotal;
	
	@JsonProperty("igst")
	private LedgerTax igst;
	
	@JsonProperty("cgst")
	private LedgerTax cgst;
	
	@JsonProperty("sgst")
	private LedgerTax sgst;
	
	@JsonProperty("cess")
	private LedgerTax cess;
	
}
