package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ITCDebitTransaction {

	@JsonProperty("tr_name")
	private String transactionName;
	
	@JsonProperty("tr_dtl")
	private List<LedgerLiabilityTransactionDetail> transactionDetail;
}
