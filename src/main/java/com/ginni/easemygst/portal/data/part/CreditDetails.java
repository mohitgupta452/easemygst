package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class CreditDetails {

	@JsonProperty("gstin_ec")
	private String gstin_ec;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("irt")
	private Double irt;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("itx")
	private Double itx;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("crt")
	private Double crt;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("ctx")
	private Double ctx;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("srt")
	private Double srt;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("stx")
	private Double stx;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("csrt")
	private Double csrt;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cstx")
	private Double cstx;
	
	//not in api
	@JsonProperty("ctin_name")
	private String ctinName="";
	//not in api
	@JsonProperty("source")
	private String source="";

}
