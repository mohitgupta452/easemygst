package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TotalTaxLiabDetails {

	@JsonProperty("ty")
	private String ty;
	
	@JsonFormat(pattern="MM")
	@JsonProperty("mon")
	private String month;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("txval")
	private Double txval=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
	private Double cgstLiability=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
	private Double sgstLiability=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	private Double igstLiability=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("csamt")
	private Double csamt=0.0;

	
	
}
