package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;

public class B2bUrDetailDeserializer extends BaseDataDetailDeserializer <List<B2bUrTransactionEntity>>{

	@Override
	public List<B2bUrTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<B2bUrTransactionEntity> b2bUrTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> b2burNodes = node.elements();
		while (b2burNodes.hasNext()) {
			JsonNode b2burNode = b2burNodes.next();
			B2bUrTransactionEntity b2bUrTransactionEntity = new B2bUrTransactionEntity();
			if(b2burNode.has("inum"))
				b2bUrTransactionEntity.setInvoiceNumber(b2burNode.get("inum").asText());
			
			String invDate = b2burNode.has("idt") ? b2burNode.get("idt").asText() : null;
			if (!StringUtils.isEmpty(invDate)) {
				try {
					b2bUrTransactionEntity.setInvoiceDate(gstFmt.parse(invDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			if(b2burNode.has("pos")){
				b2bUrTransactionEntity.setPos(b2burNode.get("pos").asText());
				String stateName=Objects.nonNull(AppConfig.getStateNameByCode(b2bUrTransactionEntity.getPos()))?AppConfig.getStateNameByCode(b2bUrTransactionEntity.getPos()).getName():"";
				b2bUrTransactionEntity.setStateName(stateName);
			}
			/*if(b2burNode.has("state_name"))
			{String stateName=Objects.nonNull(AppConfig.getStateNameByCode(b2bUrTransactionEntity.getPos()))?AppConfig.getStateNameByCode(b2bUrTransactionEntity.getPos()).getName():"";
			b2bUrTransactionEntity.setStateName(stateName);
				//b2bUrTransactionEntity.setStateName(b2burNode.get("state_name").asText());
				
			}*/
			if(b2burNode.has("taxable_val"))
				b2bUrTransactionEntity.setTaxableValue(b2burNode.get("taxable_val").asDouble());
			if(b2burNode.has("tax_amt"))
				b2bUrTransactionEntity.setTaxAmount(b2burNode.get("tax_amt").asDouble());
			if(b2burNode.has("flags"))
				b2bUrTransactionEntity.setFlags(b2burNode.get("flags").asText());
			if(b2burNode.has("type"))
				b2bUrTransactionEntity.setType(b2burNode.get("type").asText());
			if(b2burNode.has("source"))
				b2bUrTransactionEntity.setSource(b2burNode.get("source").asText());
			if(b2burNode.has("invoice_id"))
				b2bUrTransactionEntity.setId(b2burNode.get("invoice_id").asText());
			if(b2burNode.has("sply_ty"))
				b2bUrTransactionEntity.setSupplyType(SupplyType.valueOf(b2burNode.get("sply_ty").asText()));
			b2bUrTransactionEntity.setItems(this.deserializeItem(b2burNode));
			b2bUrTransactionEntities.add(b2bUrTransactionEntity);
			
		}
		return b2bUrTransactionEntities;
	}

}
