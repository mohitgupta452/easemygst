package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;

public class EXPDetailDeserializer extends BaseDataDetailDeserializer <List<EXPTransactionEntity>> {

	@Override
	public List<EXPTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<EXPTransactionEntity> expTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> expNodes = node.elements();
		while (expNodes.hasNext()) {
			JsonNode expNode = expNodes.next();
			EXPTransactionEntity expTransactionEntity = new EXPTransactionEntity();
			List<ExpDetail> details = new ArrayList<>();
			
			details.add(this.deserializeInvoice(expNode,expTransactionEntity));
			expTransactionEntity.setExpDetails(details);
			expTransactionEntities.add(expTransactionEntity);
		}
		return expTransactionEntities;
	}
	
	public ExpDetail deserializeInvoice(JsonNode expNode,EXPTransactionEntity exp) {
		ExpDetail expDetail = new ExpDetail();
		expDetail.setExpTransaction(exp);
		if (expNode.has("exp_type"))
			expDetail.setExportType(expNode.get("exp_type").asText());
		if (expNode.has("inum"))
			expDetail.setInvoiceNumber(expNode.get("inum").asText());
		
		String invDate = expNode.has("idt") ? expNode.get("idt").asText() : null;
		if (!StringUtils.isEmpty(invDate)) {
			try {
				expDetail.setInvoiceDate(gstFmt.parse(invDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		if (expNode.has("sbpcode"))
			expDetail.setShippingBillPortCode(expNode.get("sbpcode").asText());
		
		if (expNode.has("sbnum"))
			expDetail.setShippingBillNo(expNode.get("sbnum").asText());
		
		String shippingDt = expNode.has("sbdt") ? expNode.get("sbdt").asText() : null;
		if (!StringUtils.isEmpty(shippingDt)) {
			try {
				expDetail.setShippingBillDate(gstFmt.parse(shippingDt));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if (expNode.has("taxable_val"))
			expDetail.setTaxableValue(expNode.get("taxable_val").asDouble());
		
		if (expNode.has("tax_amt"))
			expDetail.setTaxAmount(expNode.get("tax_amt").asDouble());
		
		if (expNode.has("flags"))
			expDetail.setFlags(expNode.get("flags").asText());
		
		if (expNode.has("type"))
			expDetail.setType(expNode.get("type").asText());
		
		if (expNode.has("source"))
			expDetail.setSource(expNode.get("source").asText());
		
		if (expNode.has("invoice_id"))
			expDetail.setId(expNode.get("invoice_id").asText());
		
		if (expNode.has("originalInvoiceNumber"))
			expDetail.setOriginalInvoiceNumber(expNode.get("originalInvoiceNumber").asText());
		String orgInvDate = expNode.has("originalInvoiceDate") ? expNode.get("originalInvoiceDate").asText() : null;
		if (!StringUtils.isEmpty(orgInvDate)) {
			try {
				expDetail.setOriginalInvoiceDate(gstFmt.parse(orgInvDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(StringUtils.isNoneEmpty(expDetail.getOriginalInvoiceNumber())||Objects.nonNull(expDetail.getOriginalInvoiceDate()))
			expDetail.setIsAmmendment(true);
		expDetail.setItems(this.deserializeItem(expNode));
		
		return expDetail;
	}

}
