package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2CLDetailSerializer extends BaseDataDetailSerializer<B2CLDetailEntity> {

	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");


	@Override
	public void serialize(B2CLDetailEntity b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			json.writeStartObject();
			json.writeStringField("pos", b2b.getPos());
			json.writeStringField("state_name",b2b.getStateName());
			//Invoice invoice=b2b.getInvoiceData();
			json.writeStringField("etin",b2b.getEtin() );
			//json.writeStringField("rchrg",String.valueOf(b2b.getReverseCharge()));
			json.writeStringField("inum", b2b.getInvoiceNumber());
			if(!Objects.isNull(b2b.getInvoiceDate()))
			json.writeStringField("idt", gstFmt.format(b2b.getInvoiceDate()));
			json.writeNumberField("taxable_val",b2b.getTaxableValue() );
			json.writeNumberField("tax_amt",b2b.getTaxAmount() );
			json.writeStringField("flags",b2b.getFlags() );
			json.writeStringField("type",b2b.getType() );
			json.writeStringField("source",b2b.getSource() );
		    json.writeStringField("invoice_id",b2b.getId() );
			
		    json.writeBooleanField("isSynced",b2b.getIsSynced());
		 	json.writeBooleanField("isTransit",b2b.getIsTransit() );
		 	String invType=StringUtils.isEmpty(b2b.getEtin())?"N":"Y";
		    json.writeStringField("inv_type",invType );

		 	if(!StringUtils.isEmpty(b2b.getToBeSync().toString()))
		    	json.writeBooleanField("toBeSynch",b2b.getToBeSync() );
		 	
		 	json.writeBooleanField("isError",b2b.getIsError() );
			json.writeStringField("errorMsg",b2b.getErrMsg() );
			json.writeNumberField("inv_val",b2b.getTaxAmount()+b2b.getTaxableValue() );

			

			/*if (!StringUtils.isEmpty(invoice.getPos()))
				json.writeStringField("pos", invoice.getPos());
			if(!Objects.isNull(invoice.getSupplierInvVal())) {
				json.writeNumberField("val", invoice.getSupplierInvVal());
			}
			*/
			
			if (Objects.nonNull(b2b.getB2clTransaction()) ) {
				if("ERROR".equalsIgnoreCase(b2b.getFlags())||b2b.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(b2b.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(b2b.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}
			
//ammendment
			
			json.writeBooleanField("isAmmendment",b2b.getIsAmmendment() );
			json.writeStringField("originalInvoiceNumber",b2b.getOriginalInvoiceNumber() );
			if(Objects.isNull(b2b.getOriginalInvoiceDate())){
				json.writeStringField("originalInvoiceDate", "");

			}else
			json.writeStringField("originalInvoiceDate", gstFmt.format(b2b.getOriginalInvoiceDate()));


			writeLineItems(json, b2b);
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	

	
	void writeLineItems( JsonGenerator json, B2CLDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	public void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			
			json.writeStringField("hsn_sc", !StringUtils.isEmpty(item.getHsnCode())?item.getHsnCode():"");
			
			json.writeStringField("hsn_desc", !StringUtils.isEmpty(item.getHsnDescription())?item.getHsnDescription():"");
			
			json.writeStringField("unit", !StringUtils.isEmpty(item.getUnit())?item.getUnit():"");
			
			json.writeNumberField("quantity", item.getQuantity());
			
			json.writeNumberField("num", item.getSerialNumber());
			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("txval", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			if (!Objects.isNull(item.getCess()))
				json.writeNumberField("csamt", item.getCess());

			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

}
