package com.ginni.easemygst.portal.data;

public enum TableFlags {

	ACCEPTED("Accepted"), DELETED("Deleted"), MODIFIED("Modified"), 
	REJECTED("Rejected"), PENDING("Pending"), UPLOADED("Uploaded"), 
	DRAFT("Draft");

	private String value;

	TableFlags(String val) {
		this.value = val;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}