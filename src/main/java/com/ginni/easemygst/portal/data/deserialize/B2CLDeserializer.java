package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2CLDeserializer extends BaseDataDeserializer<List<B2CLDetailEntity>> {

	@Override
	public List<B2CLDetailEntity> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<B2CLDetailEntity> b2cls = new ArrayList<>();
		Iterator<JsonNode> b2clNodes = node.elements();
		while (b2clNodes.hasNext()) {
			JsonNode b2clNode = b2clNodes.next();
			B2CLDetailEntity b2cl = new B2CLDetailEntity();
			if (b2clNode.has("pos"))
				b2cl.setPos(b2clNode.get("pos").asText());
			if(b2clNode.has("error_msg"))
				b2cl.setError_msg(b2clNode.get("error_msg").asText());
			if(b2clNode.has("error_cd"))
				b2cl.setError_cd(b2clNode.get("error_cd").asText());
			
			Iterator<JsonNode> invoices = b2clNode.get("inv").elements();
			while (invoices.hasNext()) {
				JsonNode invNode = invoices.next();

//				if (invNode.has("chksum"))
//					inv.setCheckSum(invNode.get("chksum").asText());
				if (invNode.has("inum"))
					b2cl.setInvoiceNumber(invNode.get("inum").asText());

				String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
				if (!StringUtils.isEmpty(invDate)) {
					try {
						b2cl.setInvoiceDate(gstFmt.parse(invDate));
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}

				if (invNode.has("val"))
					b2cl.setTaxableValue(invNode.get("val").asDouble());

				if (invNode.has("etin"))
					b2cl.setEtin(invNode.get("etin").asText());
				
                if(invNode.has("itms"))
				 b2cl.setItems(this.deserializeItem(invNode));

			
			//b2clentity.setB2clDetails(this.deserializeInvoice(b2clNode));
			b2cls.add(b2cl);
		}
		}
		return b2cls;

	}

	public List<B2CLDetailEntity> deserializeInvoice(JsonNode node) {

		Iterator<JsonNode> invoices = node.get("inv").elements();
		List<B2CLDetailEntity> invs = new ArrayList<>();
		while (invoices.hasNext()) {
			JsonNode invNode = invoices.next();
			B2CLDetailEntity inv = new B2CLDetailEntity();

//			if (invNode.has("chksum"))
//				inv.setCheckSum(invNode.get("chksum").asText());
			if (invNode.has("inum"))
				inv.setInvoiceNumber(invNode.get("inum").asText());

			String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
			if (!StringUtils.isEmpty(invDate)) {
				try {
					inv.setInvoiceDate(gstFmt.parse(invDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}

			if (invNode.has("val"))
				inv.setTaxableValue(invNode.get("val").asDouble());

			if (invNode.has("etin"))
				inv.setEtin(invNode.get("etin").asText());

			inv.setItems(this.deserializeItem(invNode));

			invs.add(inv);

		}
		return invs;
	}

	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();
			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			JsonNode itemDetNode = itemNode.get("itm_det");

			if (itemDetNode.has("txval"))
				item.setTaxableValue(itemDetNode.get("txval").asDouble());
			if (itemDetNode.has("rt"))
				item.setTaxRate(itemDetNode.get("rt").asDouble());
			if (itemDetNode.has("iamt"))
				item.setIgst(itemDetNode.get("iamt").asDouble());

			itms.add(item);
		}
		return itms;
	}

}
