package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3RefundCLMCL {

	@JsonFormat(pattern="#0.00")
	@JsonProperty("rf_cl_camt")
	private Double refundCgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("rf_cl_samt")
	private Double refundSgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("bnk_num")
	private Double refundIgstAmount;
}
