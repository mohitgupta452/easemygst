package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3ExportRC {

	@JsonProperty("ex_tp_rfd")
	private List<Gstr3ExporRfrcTP> exportRefund;
	
	@JsonProperty("ex_tp_adj")
	private List<Gstr3ExporRfrcAdj> exportAdjustment;
	
}
