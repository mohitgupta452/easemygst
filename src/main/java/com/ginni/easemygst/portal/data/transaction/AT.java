package com.ginni.easemygst.portal.data.transaction;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;
@JsonInclude(Include.NON_NULL)
//@JsonIgnoreProperties({"locked","valid","transit","amendment","gstnSynced"})
public @Data class AT {

	@JsonProperty("atId")
	String id;
	
	
	private boolean isValid=true;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("advance_rcvd")//overall advance received
	private Double advanceRcvd;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")//overall tax amount
	private Double taxAmount;
	
	private boolean isAmendment;

	private Map<String, String> error;
	
	@JsonIgnore
	private String flags = "";
	
	@JsonIgnore
	private String source;
	
	@JsonIgnore
	private String sourceId;
	
	private boolean isTransit = false;
	@JsonIgnore
	private String transitId = "";
	@JsonIgnore
	private boolean isLocked = true;

	
	
	@JsonProperty("state_name")
	private String stateName;
	
	@JsonProperty("status")
	private TaxpayerAction action;//incase of modify,delete,add 
	
	private boolean isGstnSynced = false;//
	
	@JsonProperty("flag")
	private TaxpayerAction taxPayerAction=TaxpayerAction.UPLOADED;;
	
	private ReconsileType type;

	@JsonProperty("chksum")
	private String checkSum;
	
	@JsonProperty("supst_cd")
	private Integer recipientStateCode;
	
	@JsonProperty("itms")
	private List<InvoiceItem> invoiceItems;
	
	@JsonProperty("sply_ty")
	private String supplyType;
	
	private String monthYear;
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getRecipientStateCode(), ((AT) obj).getRecipientStateCode());               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((recipientStateCode == null) ? 0 : recipientStateCode.hashCode());
		return result;
	}
	
	
}
