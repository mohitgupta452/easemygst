package com.ginni.easemygst.portal.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TaxAmount {
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_rate")
    public Double taxRate;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("iamt")
    public Double igstAmt;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("camt")
    public Double cgstAmt;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("samt")
    public Double sgstAmt;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("csamt")
    public Double cessAmt;//added after gov api 2.0 rlease
	
}
