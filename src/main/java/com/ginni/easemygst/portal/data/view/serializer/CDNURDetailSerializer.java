package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.transaction.CDN.CdnData;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class CDNURDetailSerializer extends BaseDataDetailSerializer<CDNURDetailEntity> {

	private String returnType = "gstr1";
	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");


	@Override
	public void serialize(CDNURDetailEntity cdn, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			if (cdn.getItems() != null && !cdn.getItems().isEmpty()
					&&cdn.getItems().get(0).getTotalEligibleTax() != null) {
				returnType = "gstr2";
			}
			json.writeStartObject();
			/*json.writeStringField("org_ctin", cdn.getOriginalCtin());
			json.writeStringField("org_ctin_name", cdn.getOriginalCtinName());*/
			json.writeStringField("ntty",cdn.getNoteType() );
			if(!Objects.isNull(cdn.getRevisedInvDate()))
			json.writeStringField("nt_dt", gstFmt.format(cdn.getRevisedInvDate()));
			json.writeStringField("inum", cdn.getInvoiceNumber());
			if(!Objects.isNull(cdn.getInvoiceDate()))
			json.writeStringField("idt", gstFmt.format(cdn.getInvoiceDate()));
			json.writeNumberField("taxable_val",cdn.getTaxableValue() );
			json.writeNumberField("tax_amt",cdn.getTaxAmount() );
			json.writeStringField("nt_num",cdn.getRevisedInvNo());
			json.writeStringField("flags",cdn.getFlags() );
			json.writeStringField("type",cdn.getType() );
			json.writeStringField("source",cdn.getSource() );
			json.writeStringField("invoice_id",cdn.getId() );
			json.writeStringField("pre_gst",!StringUtils.isEmpty(cdn.getPreGstRegime())?cdn.getPreGstRegime():"" );
			json.writeStringField("rsn",!StringUtils.isEmpty(cdn.getReasonForNote())?cdn.getReasonForNote():"" );

			json.writeBooleanField("isMarked",cdn.getIsMarked() );
			
			json.writeStringField("inv_type",!Objects.isNull(cdn.getInvoiceType())?cdn.getInvoiceType():"" );

			//if (!StringUtils.isEmpty(cdn.getPos()))
			//	json.writeStringField("pos", cdn.getPos());
			/*if(!Objects.isNull(invoice.getSupplierInvVal())) {
				json.writeNumberField("val", invoice.getSupplierInvVal());
			}*/
			
			if (Objects.nonNull(cdn.getCdnUrTransaction()) ) {
				if("ERROR".equalsIgnoreCase(cdn.getFlags())||cdn.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(cdn.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(cdn.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}
			json.writeBooleanField("isSynced",cdn.getIsSynced() );
			json.writeBooleanField("isTransit",cdn.getIsTransit() );
			json.writeBooleanField("toBeSynch",cdn.getToBeSync() );
			json.writeBooleanField("isError",cdn.getIsError() );
			json.writeStringField("errorMsg",cdn.getErrMsg() );
			json.writeNumberField("nt_val",cdn.getTaxAmount()+cdn.getTaxableValue() );
			
			//ammendment
			
			json.writeBooleanField("isAmmendment",cdn.getIsAmmendment() );
			json.writeStringField("originalNoteNumber",cdn.getOriginalNoteNumber() );
			if(Objects.isNull(cdn.getOriginalNoteDate())){
				json.writeStringField("originalNoteDate", "");

			}else
			json.writeStringField("originalNoteDate", gstFmt.format(cdn.getOriginalNoteDate()));	



			writeLineItems(json, cdn);
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	

	
	void writeLineItems( JsonGenerator json, CDNURDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	/*public void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			json.writeNumberField("num", item.getSerialNumber());

			
			if (!StringUtils.isEmpty(item.getHsnCode()))
				json.writeStringField("hsn_sc", item.getHsnCode());

			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("txval", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			if (!Objects.isNull(item.getCgst()))
				json.writeNumberField("camt", item.getCgst());

			if (!Objects.isNull(item.getSgst()))
				json.writeNumberField("samt", item.getSgst());

			if (!Objects.isNull(item.getCess()))
				json.writeNumberField("csamt", item.getCess());

			if ("gstr2".equalsIgnoreCase(returnType)) {
				if (!StringUtils.isEmpty(item.getTotalEligibleTax())) {
					json.writeStringField("elg",
							item.getTotalEligibleTax() != null ? String.valueOf(item.getTotalEligibleTax()) : null);

					// writing itc detail
					json.writeFieldName("itc");// adding itc detail field
					this.writeItcDetail(json, item);
				}
			}

			//json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/*private void writeItcDetail(JsonGenerator json, Item itc) {
		try {
			// start itcdetail
			json.writeStartObject();
			
			if (!Objects.isNull(itc.getTotalTaxAvalIgst()))
				json.writeStringField("tx_i",
					itc.getTotalTaxAvalIgst() != null ? String.valueOf(itc.getTotalTaxAvalIgst()) : null);
			
			if (!Objects.isNull(itc.getTotalTaxAvalCgst()))
				json.writeStringField("tx_c",
					itc.getTotalTaxAvalCgst() != null ? String.valueOf(itc.getTotalTaxAvalCgst()) : null);
			
			if (!Objects.isNull(itc.getTotalTaxAvalSgst()))
				json.writeStringField("tx_s",
					itc.getTotalTaxAvalSgst() != null ? String.valueOf(itc.getTotalTaxAvalSgst()) : null);
			
			if (!Objects.isNull(itc.getTotalTaxAvalCess()))
				json.writeStringField("tx_cs",
					itc.getTotalTaxAvalCess() != null ? String.valueOf(itc.getTotalTaxAvalCess()) : null);
			
			json.writeEndObject();
			// end itc detail
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

}
