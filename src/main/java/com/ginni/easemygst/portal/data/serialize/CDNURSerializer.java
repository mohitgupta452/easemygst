package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.HSNSUM.HsnSumData;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNTransaction;
import com.ginni.easemygst.portal.transaction.impl.TXPDTransaction;
import com.ginni.easemygst.portal.utils.Utility;

public class CDNURSerializer extends BaseDataSerializer<List<CDNURDetailEntity>> {

	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");

	private ReturnType returnType = ReturnType.GSTR1;

	private TransactionType transType = TransactionType.CDNUR;

	@Override
	public void serialize(List<CDNURDetailEntity> cdnurs, final JsonGenerator json,
			SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
		json.writeStartArray();
		// for (CDNURTransactionEntity cdn : cdnurs) {
		if (!cdnurs.isEmpty()) {
			returnType = ReturnType.valueOf(cdnurs.get(0).getCdnUrTransaction().getReturnType().toUpperCase());

		//	cdnurs=this.groupByDetailEntity(cdnurs);
			json.writeStartObject();
			cdnurs.forEach(urdetails -> {
				try {

					if (urdetails.getCdnUrTransaction().getReturnType().equalsIgnoreCase("GSTR1")) {

						json.writeStringField("typ", urdetails.getInvoiceType());

						if (!StringUtils.isEmpty(urdetails.getInvoiceNumber()))
							json.writeStringField("nt_num", urdetails.getRevisedInvNo());

						json.writeStringField("nt_dt", gstFmt.format(urdetails.getRevisedInvDate()));

						if (!Objects.isNull(urdetails.getFlags()) && !StringUtils.isEmpty(urdetails.getFlags()))
							json.writeStringField("flag",
									urdetails.getFlags() != null ? this.matchFlag(urdetails.getFlags()) : null);
						else {
							
							if(urdetails.getIsAmmendment()) {
								json.writeStringField("ont_num", urdetails.getOriginalNoteNumber());
								json.writeStringField("ont_dt", gstFmt.format(urdetails.getOriginalNoteDate()));
							}
							if (!StringUtils.isEmpty(urdetails.getNoteType()))
								json.writeStringField("ntty", urdetails.getNoteType());
							if (!StringUtils.isEmpty(urdetails.getPreGstRegime()))
								json.writeStringField("p_gst", StringUtils.upperCase(checkEmpty(urdetails.getPreGstRegime())));

							//json.writeStringField("rsn", urdetails.getReasonForNote());
							if (!StringUtils.isEmpty(urdetails.getInvoiceNumber()))
								json.writeStringField("inum", urdetails.getInvoiceNumber());
							if (urdetails.getInvoiceDate() != null)
								json.writeStringField("idt", gstFmt.format(urdetails.getInvoiceDate()));
							if (urdetails.getTaxableValue() != 0) {
								BigDecimal val = BigDecimal.ZERO;
								val = val.add(Utility.getBigDecimalScale(urdetails.getTaxableValue()))
										.add(Utility.getBigDecimalScale(urdetails.getTaxAmount()));
								json.writeNumberField("val", val.setScale(2, RoundingMode.FLOOR));
							}

							if (urdetails.getItems() != null) {
								if (!urdetails.getItems().isEmpty())
									new ItemSerializer().writeLineItems(json, urdetails.getItems(), returnType,
											transType);
							}
						}
					} else {
						if (!StringUtils.isEmpty(urdetails.getRevisedInvNo()))
							json.writeStringField("nt_num", urdetails.getRevisedInvNo());
						if (urdetails.getRevisedInvDate() != null)
							json.writeStringField("nt_dt", gstFmt.format(urdetails.getRevisedInvDate()));

						if (!Objects.isNull(urdetails.getFlags()) && !StringUtils.isEmpty(urdetails.getFlags()))
							json.writeStringField("flag",
									urdetails.getFlags() != null ? this.matchFlag(urdetails.getFlags()) : null);
						else {

							if (urdetails.getIsAmmendment()) {
								json.writeStringField("ont_num", urdetails.getOriginalNoteNumber());
								json.writeStringField("ont_dt", gstFmt.format(urdetails.getOriginalNoteDate()));
							}
							json.writeStringField("rtin", urdetails.getOriginalCtin());
							json.writeStringField("ntty", urdetails.getNoteType());
							//json.writeStringField("rsn", urdetails.getReasonForNote());
							json.writeStringField("p_gst", urdetails.getPreGstRegime());
							json.writeStringField("inum", urdetails.getInvoiceNumber());
							json.writeStringField("idt", gstFmt.format(urdetails.getInvoiceDate()));
							if (urdetails.getTaxableValue() + urdetails.getTaxAmount() != 0) {
								BigDecimal val = BigDecimal.ZERO;
								val = BigDecimal.valueOf(urdetails.getTaxableValue())
										.add(BigDecimal.valueOf(urdetails.getTaxAmount()));
								json.writeNumberField("val", val.setScale(2, RoundingMode.FLOOR));
							}
							if (urdetails.getItems() != null) {
								if (!urdetails.getItems().isEmpty())
									new ItemSerializer().writeLineItems(json, urdetails.getItems(), returnType,
											transType);
							}
						}
					}
					// writeCdnDatas(json, urdetails.getItems());
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			json.writeEndObject();
		}
		// }
		json.writeEndArray();
	}

	void writeCdnDatas(JsonGenerator json, List<Item> items) throws IOException {
		// Start HsnsumDatas
		json.writeFieldName("itms");
		json.writeStartArray();
		// End HsnsumDatas
		items.forEach(item -> {
			try {
				json.writeStartObject();
				json.writeNumberField("num", item.getSerialNumber());
				json.writeFieldName("itm_det");
				json.writeStartObject();
				json.writeNumberField("rt", item.getTaxRate());
				json.writeNumberField("txval", item.getTaxableValue());
				json.writeNumberField("iamt", item.getIgst());
				json.writeNumberField("csamt", item.getCgst());
				json.writeEndObject();
				json.writeEndObject();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		json.writeEndArray();
	}

	private void writeCDNData(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();
			json.writeNumberField("rt", item.getTaxRate());
			json.writeNumberField("ad_amt", item.getTaxableValue());
			json.writeNumberField("iamt", item.getIgst());
			json.writeNumberField("csamt", item.getCgst());
			json.writeEndObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String checkEmpty(String str) {
		if (!StringUtils.isEmpty(str))
			return str;
		else
			return "";
	}

	private List<CDNURDetailEntity> groupByDetailEntity(List<CDNURDetailEntity> datas){
		List<CDNURDetailEntity> result=new  ArrayList<>();
		Map<String, List<CDNURDetailEntity>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(CDNURDetailEntity::getId));
		for(Map.Entry<String, List<CDNURDetailEntity>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			CDNURDetailEntity b2bDetailEntity=null;
			for(CDNURDetailEntity b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

	
}
