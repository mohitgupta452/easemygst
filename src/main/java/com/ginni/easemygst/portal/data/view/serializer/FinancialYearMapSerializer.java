package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class FinancialYearMapSerializer extends JsonSerializer<Map<String,String>> {
  
	protected final Logger _Logger = LoggerFactory.getLogger(this.getClass());

     
    @Override
    public void serialize (Map<String,String>monthYears, JsonGenerator gen, SerializerProvider arg2)
      throws IOException, JsonProcessingException {
    	
    	gen.writeStartObject();
    	for(Map.Entry<String, String>ent:monthYears.entrySet()){
    	gen.writeStringField(ent.getKey(), ent.getValue());
    	_Logger.info("key {} value {}",ent.getKey(),ent.getValue());
    	}
    	
    	gen.writeEndObject();
    	_Logger.info("value of json {}",gen.getCurrentValue());
    }
}