package com.ginni.easemygst.portal.data.part;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.RegistrationType;

@JsonInclude(Include.NON_NULL)
public @lombok.Data class TLD {

	@JsonProperty("tldId")
	String id;

	private boolean isValid=true;// added after gov api 2.0 release

	private boolean isGstnSynced = false;// added after gov api 2.0 release

	private Map<String, String> error;

	private String flags = "";
	
	private boolean isAmendment;
	
	private String source;
	
	private String sourceId;
	
	private boolean isTransit = false;
	
	private String transitId = "";

/*
	@JsonProperty("ctin_name")
	private String gstinName;// added after gov api 2.0 release
*/	
	@JsonProperty("reg_type")
	private RegistrationType regType;// added after gov api 2.0 release
	
	@JsonProperty("chksum")
	private String checksum;// added after gov api 2.0 release
	
	@JsonProperty("flag")
	public TaxpayerAction taxPayerAction= TaxpayerAction.UPLOADED;;
	
	private ReconsileType type;
	

	@JsonFormat(pattern="#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tax_amount")//overall tax amount
	private Double taxAmount;
	
	@JsonProperty("cpty")
	private String ctin;
	
	
	@JsonProperty("st_cd")
	public Integer stateCode;
	
	@JsonProperty("state_name")
	private String stateName;

	@JsonProperty("dnum")
	public String supplierDocNumber;

	@JsonFormat(pattern = Transaction.jsonDateFormat)
	@JsonProperty("dt")
	public Date supplierDocDate;

	@JsonProperty("itms")
	List<InvoiceItem> items;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ctin == null) ? 0 : ctin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.getCtin(), ((TLD) obj).getCtin())
				.append(this.getStateCode(), ((TLD) obj).getStateCode());
		return builder.isEquals();
	}

}
