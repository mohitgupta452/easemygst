package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.HSNSUM.HsnSumData;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.transaction.impl.TXPDTransaction;

public class TXPSerializer extends BaseDataSerializer<List<TxpdTransaction>> {

	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	
    //private ReturnType returnType = ReturnType.GSTR1;
	
	private TransactionType transType = TransactionType.TXPD;


	@Override
	public void serialize(List<TxpdTransaction> txptransactions, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		
		ReturnType returnTyp=null;
		//txptransactions=this.groupByDetailEntity(txptransactions);
		 Map<String,List<TxpdTransaction>> expDetails=txptransactions.stream().collect(Collectors.groupingBy(TxpdTransaction::getPos));
		 
		 List<Item> items = null;
		 String flags=null;
			for(Entry<String, List<TxpdTransaction>> entry:expDetails.entrySet()){
				items = new ArrayList<>();
				json.writeStartObject();
				if(!StringUtils.isEmpty(entry.getKey()))
				   json.writeStringField("pos",(Integer.parseInt(entry.getKey())<10 && entry.getKey().length()==1)?"0"+entry.getKey():entry.getKey());
				
				boolean isFirst=false;
				for(TxpdTransaction tdp:entry.getValue()){
					if(!isFirst){
						if(!StringUtils.isEmpty(tdp.getError_msg()))
							json.writeStringField("error_msg", tdp.getError_msg());	
						if(!StringUtils.isEmpty(tdp.getError_cd()))
							json.writeStringField("error_cd", tdp.getError_cd());
						json.writeStringField("sply_ty", tdp.getSupplyType().getValue());
						for(ReturnType returnType:ReturnType.values()){
							if(returnType.toString().equalsIgnoreCase(tdp.getReturnType()))
								returnTyp=returnType;
						}
						isFirst=true;
					}
					flags=tdp.getFlags();
					items.addAll(tdp.getItems());
					//writeTXPDatas(json,tdp.getItems(),tdp.getSupplyType().getValue());
				}
				if (!Objects.isNull(flags) && !StringUtils.isEmpty(flags))
					json.writeStringField("flag",
							flags != null ? this.matchFlag(flags) : null);
				else{
					if(entry.getValue().get(0).getIsAmmendment())
						json.writeStringField("omon",entry.getValue().get(0).getOriginalMonth());
				new ItemSerializer().writeLineItems(json,items, returnTyp,transType);
				}
				
			//	json.writeEndArray();
				json.writeEndObject();
			}
		
//		txptransactions.forEach(txpTrans -> {
//		  try{	
//			json.writeStartObject();
//			//json.writeStringField("chksum", );
//			json.writeStringField("pos",checkEmpty(txpTrans.getPos()));
//			json.writeStringField("sply_ty", checkEmpty(txpTrans.getSupplyType().getValue()));
//			writeTXPDatas(json, txpTrans.getItems());
//			json.writeEndObject();
//		  }catch(Exception e){
//			  e.printStackTrace();
//		  }
//		});
		json.writeEndArray();
	}


	void writeTXPDatas(JsonGenerator json, List<Item> items,String supply) throws IOException {
		// Start HsnsumDatas
		
	    items.forEach(item -> writeTXPData(json,item,supply));
		// End HsnsumDatas

		
	}
	
	private void writeTXPData(JsonGenerator json,Item item,String supplyType){
		try{
			json.writeStartObject();
			json.writeNumberField("rt", item.getTaxRate());
			json.writeNumberField("ad_amt", BigDecimal.valueOf(item.getTaxableValue()));
			json.writeNumberField("iamt", BigDecimal.valueOf(item.getIgst()));
			json.writeNumberField("csamt", BigDecimal.valueOf(item.getCess()));
			if(supplyType.equalsIgnoreCase("INTRA")){
				json.writeNumberField("camt", BigDecimal.valueOf(item.getCgst()));
				json.writeNumberField("samt", BigDecimal.valueOf(item.getSgst()));
			}
			json.writeEndObject();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static String checkEmpty(String str){
		if(!StringUtils.isEmpty(str))
			return str;
		else
			return "";
	}
	
	private List<TxpdTransaction> groupByDetailEntity(List<TxpdTransaction> datas){
		List<TxpdTransaction> result=new  ArrayList<>();
		Map<String, List<TxpdTransaction>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(TxpdTransaction::getId));
		for(Map.Entry<String, List<TxpdTransaction>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			TxpdTransaction b2bDetailEntity=null;
			for(TxpdTransaction b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}


}
