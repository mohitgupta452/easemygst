package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

public class B2BDetailSerializer extends BaseDataDetailSerializer<B2BDetailEntity> {

	private String returnType = "gstr1";
	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");


	@Override
	public void serialize(B2BDetailEntity b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			if (b2b.getItems() != null && !b2b.getItems().isEmpty()
					&&b2b.getItems().get(0).getTotalEligibleTax() != null) {
				returnType = "gstr2";
			}
			json.writeStartObject();
			json.writeStringField("ctin", b2b.getCtin());
			json.writeStringField("ctin_name", StringUtils.isNotEmpty(b2b.getCtinName())?b2b.getCtinName():"NO NAME");
			//Invoice invoice=b2b.getInvoice();
			json.writeStringField("inv_type",b2b.getInvoiceType() );
			json.writeStringField("rchrg",String.valueOf(b2b.getRevCharge()));
			json.writeStringField("inum", b2b.getInvoiceNumber());
			if(Objects.isNull(b2b.getInvoiceDate())){
				json.writeStringField("idt", "");

			}else
			json.writeStringField("idt", gstFmt.format(b2b.getInvoiceDate()));
			json.writeStringField("etin", b2b.getEtin());
			json.writeNumberField("taxable_val",b2b.getTaxableValue() );
			json.writeNumberField("tax_amt",b2b.getTaxAmount() );
			json.writeStringField("flags",b2b.getFlags() );
			json.writeStringField("type",b2b.getType() );
			json.writeStringField("source",b2b.getSource() );
			json.writeStringField("invoice_id",b2b.getId() );
			json.writeBooleanField("isMarked",b2b.getIsMarked() );
			json.writeNumberField("inv_val",b2b.getTaxAmount()+b2b.getTaxableValue() );

			if (!StringUtils.isEmpty(b2b.getPos()))
				json.writeStringField("pos", b2b.getPos());
			else
				json.writeStringField("pos", "");

			/*if(!Objects.isNull(b2b.)) {
				json.writeNumberField("val", invoice.getSupplierInvVal());
			}*/
			
				json.writeNumberField("ctax_amount", !Objects.isNull(b2b.getcTaxAmount())?b2b.getcTaxAmount():0.0);
				json.writeStringField("cinv_num", !Objects.isNull(b2b.getcInvoiceNumber())?b2b.getcInvoiceNumber():"");
		
			if(Objects.isNull(b2b.getcInvoiceDate())){
				json.writeStringField("cidt", "");

			}else
			json.writeStringField("cidt", gstFmt.format(b2b.getcInvoiceDate()));
			json.writeBooleanField("isSynced",b2b.getIsSynced() );
			json.writeBooleanField("isTransit",b2b.getIsTransit() );
			json.writeBooleanField("toBeSynch",b2b.isToBeSync() );
			json.writeStringField("cfs",Objects.nonNull(b2b.getB2bTransaction())?b2b.getB2bTransaction().getFillingStatus():null);
		
			if(b2b.getType().contains("MISMATCH")||ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(b2b.getType())){
			if (Objects.nonNull(b2b.getB2bTransaction())
				&& ReturnType.GSTR2 == ReturnType
						.valueOf(StringUtils.upperCase(b2b.getB2bTransaction().getReturnType()))
				&& "N".equalsIgnoreCase(b2b.getB2bTransaction().getFillingStatus())) {
			json.writeStringField("flags", "DRAFT");
		}
			}
			
			if (Objects.nonNull(b2b.getB2bTransaction()) && ReturnType.GSTR1 == ReturnType
					.valueOf(StringUtils.upperCase(b2b.getB2bTransaction().getReturnType()))) {
				if("ERROR".equalsIgnoreCase(b2b.getFlags())||b2b.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(b2b.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(b2b.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}
			
			json.writeBooleanField("isError",b2b.getIsError() );
			json.writeStringField("errorMsg",b2b.getErrMsg() );
			json.writeStringField("octin", b2b.getOctin());
			json.writeStringField("octin_name", StringUtils.isNotEmpty(b2b.getOctinName())?b2b.getOctinName():"NO NAME");
			json.writeBooleanField("isRevertible",this.checkIsRevertible(b2b) );
			
			//ammendment
			
			json.writeBooleanField("isAmmendment",b2b.getIsAmmendment() );
			json.writeStringField("originalInvoiceNumber",b2b.getOriginalInvoiceNumber() );
			if(Objects.isNull(b2b.getOriginalInvoiceDate())){
				json.writeStringField("originalInvoiceDate", "");

			}else
			json.writeStringField("originalInvoiceDate", gstFmt.format(b2b.getOriginalInvoiceDate()));


			

			this.writeLineItems(json, b2b);
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	void writeLineItems( JsonGenerator json, B2BDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	
	boolean checkIsRevertible(B2BDetailEntity b2b){
		if(b2b.getIsSynced())
			return false;
		if (Objects.nonNull(b2b.getPreviousType()) && ("PENDING".equalsIgnoreCase(b2b.getFlags())
				|| "REJECTED".equalsIgnoreCase(b2b.getFlags()) || "ACCEPTED".equalsIgnoreCase(b2b.getFlags()))) {
			if(b2b.getPreviousType().contains("MISMATCH")|| b2b.getPreviousType()
					.contains("MISSING")){
				return true;
			}
		}
		
		return false;
	}
	
	
}
