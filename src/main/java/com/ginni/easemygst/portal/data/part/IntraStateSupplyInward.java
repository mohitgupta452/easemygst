package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class IntraStateSupplyInward {

	@JsonProperty("ty")
	private String ty;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("txrt")
	private Double txrt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("txval")
	private Double txval=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
	private Double camt=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
	private Double samt=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_c")
	private Double itc_c=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_s")
	private Double itc_s=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("cess")
	private Double cess=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_cs")
	private Double itc_cs=0.0;

}
