package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ItraSupReg {
	
	@JsonProperty("intra_det")
	private List<IntraStateSupplies> intra_det;
	
}
