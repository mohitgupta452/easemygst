package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.utils.Utility;

public class IMPSBSerializer extends BaseDataSerializer<List<IMPSTransactionEntity>>{
	
    DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
	
	@Override
	public void serialize(List<IMPSTransactionEntity> impstransactions, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		impstransactions.forEach(impstrans -> {
		  try{	
			json.writeStartObject();
			json.writeStringField("inum", impstrans.getInvoiceNumber());
			json.writeStringField("idt", dateFormat.format(impstrans.getInvoiceDate()));
			
			if (!Objects.isNull(impstrans.getFlags()) && !StringUtils.isEmpty(impstrans.getFlags()))
				json.writeStringField("flag",
						impstrans.getFlags() != null ? this.matchFlag(impstrans.getFlags()) : null);
			else{
			json.writeNumberField("ival", impstrans.getInvoiceValue());
			json.writeStringField("pos", impstrans.getPos());
			
			if(!StringUtils.isEmpty(impstrans.getErrorMsg()))
				json.writeStringField("error_msg", impstrans.getErrorMsg());
			if(!StringUtils.isEmpty(impstrans.getErrorCd()))
				json.writeStringField("error_cd", impstrans.getErrorCd());
			//json.writeStringField("chksum", impstrans.getC);
			if(impstrans.getItems()!=null){
			 if(!impstrans.getItems().isEmpty())	
			  writeIMPGDatas(json, impstrans.getItems());
			}
			}
			json.writeEndObject();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		});
		json.writeEndArray();
	}


	void writeIMPGDatas(JsonGenerator json, List<Item> items) throws IOException {
		// Start HsnsumDatas
		json.writeFieldName("itms");
		json.writeStartArray();
		int i=1;
	    items.forEach(item -> writeIMPGData(json,item,i));
		json.writeEndArray();
	}
	
	private void writeIMPGData(JsonGenerator json,Item item,int i){
		try{
			json.writeStartObject();
			json.writeNumberField("num", i++);
			json.writeNumberField("txval",Utility.getBigDecimalScale(item.getTaxableValue()));
			json.writeStringField("elg", item.getTotalEligibleTax().toLowerCase());
			json.writeNumberField("rt", item.getTaxRate());
			json.writeNumberField("iamt",Utility.getBigDecimalScale(item.getIgst()));
			json.writeNumberField("csamt",Utility.getBigDecimalScale(item.getCgst()));
			json.writeNumberField("tx_i", Utility.getBigDecimalScale(item.getItcIgst()));
			json.writeNumberField("tx_cs", Utility.getBigDecimalScale(item.getItcCgst()));
			json.writeEndObject();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
