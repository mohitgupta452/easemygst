package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerLiabilityDetail {

	@JsonProperty("db")
	private DebitITC debit;
	
	@JsonProperty("op_bal")
	private LedgerLiabilityTransactionDetail openingBalance;
	
	@JsonProperty("cl_bal")
	private LedgerLiabilityTransactionDetail closingBalance;
	
	
}
