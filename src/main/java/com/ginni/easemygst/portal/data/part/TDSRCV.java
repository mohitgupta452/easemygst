package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public @lombok.Data class TDSRCV {

	@JsonProperty("ty")
	private String gstin;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("irt")
	private Double igstRate;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itx")
	private Double igstTax;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("crt")
	private Double cgstRate;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ctx")
	private Double cgstTax;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("srt")
	private Double sgstRate;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("stx")
	private Double sgstTax;
	
}
