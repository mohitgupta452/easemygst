package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;

public class NILDetailSerializer extends BaseDataDetailSerializer<NILTransactionEntity> {

	private String returnType = "gstr1";
//	public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");


	@Override
	public void serialize(NILTransactionEntity nil, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
//		json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			json.writeStartObject();
			//NIL nil=b2csTran.getData();
			json.writeStringField("sply_ty", String.valueOf(nil.getSupplyType()));
			json.writeNumberField("tot_nil_amt",nil.getTotNilAmt() );
			json.writeNumberField("tot_exmptd_amt",nil.getTotExptAmt() );
			json.writeNumberField("tot_ngsp_amt",nil.getTotNgsupAmt() );
			if(!Objects.isNull(nil.getTotCompAmt()))
			json.writeNumberField("tot_comp_amt",nil.getTotCompAmt() );
			json.writeStringField("invoice_id",nil.getId() );
			json.writeStringField("source", !StringUtils.isEmpty(nil.getSource())?nil.getSource():"");
			
			json.writeBooleanField("isSynced",nil.getIsSynced() );
			json.writeBooleanField("isTransit",nil.getIsTransit() );
			json.writeBooleanField("toBeSynch",nil.getToBeSync() );
			json.writeBooleanField("isError",nil.getIsError() );
			json.writeStringField("errorMsg",nil.getErrMsg() );
			
			if (Objects.nonNull(nil) ) {
				if("ERROR".equalsIgnoreCase(nil.getFlags())||nil.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(nil.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(nil.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}

			json.writeEndObject();
			
		//}
//		json.writeEndArray();

	}

	

	
	

}
