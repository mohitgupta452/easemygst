package com.ginni.easemygst.portal.data.part;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerCashTransactionDetail {

	@JsonFormat(pattern = "DD-MM-YYYY")
	@JsonProperty("dt")
	private Date date;
	
	@JsonProperty("desc")
	private String description;
	
	@JsonProperty("ref_no")
	private String referenceNo;
	
		
	@JsonFormat(pattern="#0.00")
	@JsonProperty("bal")
	private Double balance;
	
	@JsonProperty("tr_typ")
	private String transactionType;
	
	@JsonProperty("igst")
	private LedgerTax igst;
	
	@JsonProperty("cgst")
	private LedgerTax cgst;
	
	@JsonProperty("sgst")
	private LedgerTax sgst;
	
	@JsonProperty("cess")
	private LedgerTax cess;
	
}
