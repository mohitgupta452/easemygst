package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class IntraStateSupplies {
	
	@JsonProperty("ty")
	private String ty;
	
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tx_r")
	private Double tx_r=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("txval")
	private Double txval=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("camt")
	private Double camt=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("samt")
	private Double samt=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cess")
	private Double cess=0.0;


}
