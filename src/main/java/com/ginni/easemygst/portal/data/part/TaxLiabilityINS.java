package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TaxLiabilityINS {

	@JsonProperty("txliab_det")
	private List<TotalTaxLiabDetails> txliab_det;

}
