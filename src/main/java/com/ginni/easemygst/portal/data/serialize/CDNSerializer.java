package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;


public class CDNSerializer extends BaseDataSerializer<List<CDNDetailEntity>> {

	//private String returnType = "gstr1";
	
	private ReturnType returnTypeEnum = ReturnType.GSTR1;
	private TransactionType transType = TransactionType.CDN;

	@Override
	public void serialize(List<CDNDetailEntity> cdns, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		
		if(!cdns.isEmpty()){
		if(cdns.get(0).getCdnTransaction()!=null)	
	     returnTypeEnum=ReturnType.valueOf(cdns.get(0).getCdnTransaction().getReturnType().toUpperCase());
		//cdns=this.groupByDetailEntity(cdns);
		Map<String,List<CDNDetailEntity>> cdnDetails=cdns.stream().collect(Collectors.groupingBy(CDNDetailEntity::getOriginalCtin));
//		for (CDNDetailEntity cdn : cdns) {
		
		for(Entry<String, List<CDNDetailEntity>> entry:cdnDetails.entrySet()){
			json.writeStartObject();
			if(!StringUtils.isEmpty(entry.getKey()))
			   json.writeStringField("ctin", entry.getKey().trim());
			writeCdnDatas(entry.getValue(), json);
			
			json.writeEndObject();
		}
//			if ("gstr1".equalsIgnoreCase(returnType)) {
//				if (!StringUtils.isEmpty(cdn.getOriginalCtin()))
//					json.writeStringField("ctin", cdn.getOriginalCtin());
//			} 
		//}
		}
		json.writeEndArray();
	}

	void writeCdnDatas(List<CDNDetailEntity> cdn, JsonGenerator json) throws IOException {
		// Start Invoice Details
//		if(!StringUtils.isEmpty(cdn.get(0).getError_msg()))
//			json.writeStringField("error_msg", cdn.get(0).getError_msg());	
//		if(!StringUtils.isEmpty(cdn.get(0).getError_cd()))
//			json.writeStringField("error_cd", cdn.get(0).getError_cd());
		json.writeFieldName("nt");
		json.writeStartArray();
		//cdn.forEach(cdnData -> writeCdnData(cdnData,json));
		for(CDNDetailEntity cdnData:cdn){
			writeCdnData(cdnData, json);
		}
		json.writeEndArray();
		// end Invoice Details
	}

	private void writeCdnData(CDNDetailEntity cdn, JsonGenerator json) {
		try {
			String flag=null;
			json.writeStartObject();
			//if ("gstr1".equalsIgnoreCase(returnType)) {
				
			    //json.writeStringField("chksum", "adsgshaklfsgdhdydudjdk");
				if (cdn.getRevisedInvNo() != null)
					json.writeStringField("nt_num", cdn.getRevisedInvNo());
				
				if (cdn.getRevisedInvDate() != null)
					json.writeStringField("nt_dt", gstFmt.format(cdn.getRevisedInvDate()));
				
				if (!Objects.isNull(cdn.getFlags()) && !StringUtils.isEmpty(cdn.getFlags())) {
					flag=this.matchFlag(cdn.getFlags());
					if(!StringUtils.isEmpty(flag)/*&& ReturnType.GSTR1!=returnTypeEnum*/)
					json.writeStringField("flag",flag);
					if(!"D".equalsIgnoreCase(flag) && !StringUtils.isEmpty(cdn.getChecksum()) && ReturnType.GSTR1!=returnTypeEnum)
					json.writeStringField("chksum", cdn.getChecksum());
				}
				
				
				if(!StringUtils.isEmpty(flag) && "D".equalsIgnoreCase(flag)){
					if (cdn.getInvoiceNumber() != null)
						json.writeStringField("inum", cdn.getInvoiceNumber());
					if (cdn.getInvoiceDate() != null)
						json.writeStringField("idt", gstFmt.format(cdn.getInvoiceDate()));
				}
				
				if(StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag)){
				
					if(cdn.getIsAmmendment()) {
						json.writeStringField("ont_num", cdn.getOriginalNoteNumber());
						json.writeStringField("ont_dt", gstFmt.format(cdn.getOriginalNoteDate()));
					}
					
					if (cdn.getNoteType() != null)    
						json.writeStringField("ntty", cdn.getNoteType().toUpperCase());
				// json.writeStringField("rsn",cdn.getReasonForNote()); //removed from version 1.0
				if (cdn.getPreGstRegime() != null)
					json.writeStringField("p_gst", StringUtils.upperCase(cdn.getPreGstRegime()));
				
				if (cdn.getInvoiceNumber() != null)
					json.writeStringField("inum", cdn.getInvoiceNumber());
				if (cdn.getInvoiceDate() != null)
					json.writeStringField("idt", gstFmt.format(cdn.getInvoiceDate()));
				}
				
				if(cdn.getItems()!=null){
				     
					BigDecimal taxableValue=new ItemSerializer().writeLineItems(json, cdn.getItems(), returnTypeEnum, transType,flag);
					
					if(StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag)){

					  BigDecimal val=BigDecimal.ZERO;
					  val=val.add(taxableValue).add(BigDecimal.valueOf(cdn.getTaxAmount()));
				    json.writeNumberField("val", val.setScale(2, RoundingMode.FLOOR));
					}
				}
				// }
			//}
			
			 
			//writeLineItems(cdn, json);
			json.writeEndObject();
		} catch (Exception e) {
			// log
			e.printStackTrace();
		}
	}
	
	
	void writeLineItems(CDNDetailEntity cdnDet, JsonGenerator json) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		cdnDet.getItems().forEach(item -> writeLineItem(cdnDet, json, item));
		// End Line Items

		json.writeEndArray();
	}
	
	
	private void writeLineItem(CDNDetailEntity b2B, JsonGenerator json, Item item) {
		try {
			
			json.writeStartObject();
			json.writeNumberField("num", item.getSerialNumber());

			json.writeFieldName("itm_det");// adding item detail field

			json.writeStartObject();// start item detail
		//	if ("gstr1".equalsIgnoreCase(returnType)) {

			if (item.getTaxableValue() != 0)
				json.writeNumberField("txval", item.getTaxableValue());
			if (item.getTaxRate() != 0)
				json.writeNumberField("rt", item.getTaxRate());
			if (item.getIgst() != 0)
				json.writeNumberField("iamt", item.getIgst());
			if (item.getCess() != 0)
				json.writeNumberField("csamt", item.getCess());
			
			//}
			
			json.writeEndObject();// end item detail
			json.writeEndObject();// end item
			
//			json.writeStartObject();// start item

		} catch (Exception e) {
			/// log
			e.printStackTrace();
		}
	}

	private void writeItcDetail(JsonGenerator json, ItcDetail itc) {
		try {

			json.writeStartObject();// start itcdetail
			if (itc.getTotalTaxAvalIgst() != null)
				json.writeNumberField("tx_i", itc.getTotalTaxAvalIgst());
			if (itc.getTotalTaxAvalCgst() != null)
				json.writeNumberField("tx_c", itc.getTotalTaxAvalCgst());
			if (itc.getTotalTaxAvalSgst() != null)
				json.writeNumberField("tx_s", itc.getTotalTaxAvalSgst());
		
			json.writeEndObject();// end itc detail
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private List<CDNDetailEntity> groupByDetailEntity(List<CDNDetailEntity> datas){
		List<CDNDetailEntity> result=new  ArrayList<>();
		Map<String, List<CDNDetailEntity>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(CDNDetailEntity::getId));
		for(Map.Entry<String, List<CDNDetailEntity>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			CDNDetailEntity b2bDetailEntity=null;
			for(CDNDetailEntity b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

}
