package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class RevisionInvoiceINS {

	@JsonProperty("state_cd")
	private String stateCode;
	
	@JsonProperty("dt")
	private List<RevisionInvDetails> dt;

}
