package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2BDeserializer extends  BaseDataDeserializer <List<B2BTransactionEntity>> {
	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	
	

	@Override
	public List<B2BTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<B2BTransactionEntity> b2bs = new ArrayList<>();
		Iterator<JsonNode> b2bNodes = node.elements();
		while (b2bNodes.hasNext()) {
			JsonNode b2bNode = b2bNodes.next();
			B2BTransactionEntity b2b = new B2BTransactionEntity();
			if (b2bNode.has("ctin"))
				b2b.setCtin(b2bNode.get("ctin").asText());
			if (b2bNode.has("cfs"))
				b2b.setFillingStatus(b2bNode.get("cfs").asText());
			if (b2bNode.has("ctin"))
				b2b.setCtin(b2bNode.get("ctin").asText());
			if (b2bNode.has("error_msg"))
				b2b.setError_msg(b2bNode.get("error_msg").asText());
			if (b2bNode.has("error_cd"))
				b2b.setError_cd(b2bNode.get("error_cd").asText());
			b2b.setB2bDetails(this.deserializeInvoice(b2bNode));
			b2bs.add(b2b);
		}
		return b2bs;
	}

	public List<B2BDetailEntity> deserializeInvoice(JsonNode node) {
       if(node.has("inv")){
		Iterator<JsonNode> invoices = node.get("inv").elements();
		List<B2BDetailEntity> invs = new ArrayList<>();
		while (invoices.hasNext()) {
			JsonNode invNode = invoices.next();
			B2BDetailEntity inv = new B2BDetailEntity();
			if (invNode.has("cflag")) {
				String flag = invNode.get("cflag").asText();
				inv.setFlags(flag);

//				for (TaxpayerAction tpa : TaxpayerAction.values()) {
//					if (tpa.getValue().equalsIgnoreCase(flag)) {
//						inv.setTa
//
//					}
//				}

			}
			if (invNode.has("updby"))
				inv.setUpdatedBy(invNode.get("updby").asText());
//			if (invNode.has("chksum"))
//				inv.setCheckSum(invNode.get("chksum").asText());(checksum not present
			if (invNode.has("inum"))
				inv.setInvoiceNumber(invNode.get("inum").asText());
			
			if(invNode.has("chksum"))
				inv.setChecksum(invNode.get("chksum").asText());

			String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
			if (!StringUtils.isEmpty(invDate)) {
				try {
					inv.setInvoiceDate(gstFmt.parse(invDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}

			if (invNode.has("val"))
				inv.setTaxableValue(invNode.get("val").asDouble());
			if (invNode.has("pos"))
				inv.setPos(invNode.get("pos").asText());
			if (invNode.has("rchrg"))
				if(invNode.get("rchrg").asText().equals("Y"))
					inv.setReverseCharge(true);
				else
					inv.setReverseCharge(false);
			if (invNode.has("etin"))
				inv.setEtin(invNode.get("etin").asText());
			if (invNode.has("inv_typ"))
				inv.setInvoiceType(invNode.get("inv_typ").asText());
			
			inv.setItems(this.deserializeItem(invNode));
			
			invs.add(inv);

		}
		return invs;
       }
       return null;
	}

	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();
			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			JsonNode itemDetNode = itemNode.get("itm_det");
			
			if (itemDetNode.has("txval"))
				item.setTaxableValue(itemDetNode.get("txval").asDouble());
			if (itemDetNode.has("rt"))
				item.setTaxRate(itemDetNode.get("rt").asDouble());
			if (itemDetNode.has("iamt"))
				item.setIgst(itemDetNode.get("iamt").asDouble());
			if (itemDetNode.has("camt"))
				item.setCgst(itemDetNode.get("camt").asDouble());
			if (itemDetNode.has("samt"))
				item.setSgst(itemDetNode.get("samt").asDouble());
			if (itemDetNode.has("csamt"))
				item.setCess(itemDetNode.get("csamt").asDouble());
			
//				item.setEligOfTotalTax(itemNode.get("itc").get("elg").asText());
//				item.setItcDetails(this.deserializeItcDetail(itemNode, item));
			itms.add(item);
			}
			
		return itms;
	}
	
	public ItcDetail deserializeItcDetail(JsonNode itemNode,InvoiceItem invItem){
		JsonNode itcNode=itemNode.has("itc")?itemNode.get("itc"):null;
		ItcDetail itcDetail=new ItcDetail();
		/*if (itemNode.has("elg"))
			invItem.setEligOfTotalTax(itcNode.get("elg").asText());*/
		if(!Objects.isNull(itcNode)){
			if (itemNode.has("tx_i"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_i").asDouble());
			if (itemNode.has("tx_c"))
				itcDetail.setTotalTaxAvalCgst(itcNode.get("tx_c").asDouble());
			if (itemNode.has("tx_s"))
				itcDetail.setTotalTaxAvalSgst(itcNode.get("tx_s").asDouble());
			if (itemNode.has("tx_cs"))
				itcDetail.setTotalTaxAvalCess(itcNode.get("tx_cs").asDouble());
		}
		return itcDetail;
	}

}
