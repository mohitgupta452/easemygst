package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2CSDetailSerializer extends BaseDataDetailSerializer<B2CSTransactionEntity> {

	private String returnType = "gstr1";
	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");


	@Override
	public void serialize(B2CSTransactionEntity b2cs, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			json.writeStartObject();
		//	B2CS b2cs=b2csTran.getB2csHelper();
			json.writeStringField("sply_ty", String.valueOf(b2cs.getSupplyType()));
			json.writeStringField("b2cs_typ", StringUtils.isEmpty(b2cs.getB2csType())?"":b2cs.getB2csType());
			json.writeStringField("invoice_no", StringUtils.isEmpty(b2cs.getInvoiceNumber())?"":b2cs.getInvoiceNumber());

			json.writeStringField("pos", b2cs.getPos());
			json.writeStringField("state_name",b2cs.getStateName());
			json.writeStringField("etin",b2cs.getEtin() );
			json.writeNumberField("taxable_val",b2cs.getTaxableValue() );
			if(!Objects.isNull(b2cs.getTaxAmount()))
			json.writeNumberField("tax_amt",b2cs.getTaxAmount() );
			else
			json.writeNumberField("tax_amt",0.0 );

			json.writeStringField("flags",b2cs.getFlags() );
			json.writeStringField("type",b2cs.getType() );
			json.writeStringField("source",b2cs.getSource() );
			json.writeStringField("invoice_id",b2cs.getId() );
			
			json.writeBooleanField("isSynced",b2cs.getIsSynced() );
			json.writeBooleanField("isTransit",b2cs.getIsTransit() );
			json.writeBooleanField("toBeSynch",b2cs.getToBeSync() );
			json.writeBooleanField("isError",b2cs.getIsError() );
			json.writeStringField("errorMsg",b2cs.getErrMsg() );
			
			if (Objects.nonNull(b2cs) ) {
				if("ERROR".equalsIgnoreCase(b2cs.getFlags())||b2cs.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(b2cs.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(b2cs.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}
			json.writeBooleanField("isAmmendment",b2cs.getIsAmmendment() );
			json.writeStringField("originalMonth",b2cs.getOriginalMonth() );
			json.writeStringField("originalPos",b2cs.getOriginalPos() );
			
			Item item=new Item();
			if(!Objects.isNull(b2cs.getItems())&&!b2cs.getItems().isEmpty())
				item=b2cs.getItems().get(0);
			json.writeStringField("hsn_sc", !StringUtils.isEmpty(item.getHsnCode())?item.getHsnCode():"");
			
			json.writeStringField("hsn_desc", !StringUtils.isEmpty(item.getHsnDescription())?item.getHsnDescription():"");
			
			json.writeStringField("unit", !StringUtils.isEmpty(item.getUnit())?item.getUnit():"");
			
			json.writeNumberField("quantity", !Objects.isNull(item.getQuantity())?item.getQuantity():0.0);
			
			json.writeNumberField("rt", !Objects.isNull(item.getTaxRate())?item.getTaxRate():0.0);
			
			json.writeNumberField("iamt", !Objects.isNull(item.getIgst())?item.getIgst():0.0);
			
			json.writeNumberField("camt", !Objects.isNull(item.getCgst())?item.getCgst():0.0);
			
			json.writeNumberField("samt", !Objects.isNull(item.getSgst())?item.getSgst():0.0);
			
			
			json.writeNumberField("csamt", !Objects.isNull(item.getCess())?item.getCess():0.0);

			



			
			

			

			json.writeEndObject();
		//}
	//	json.writeEndArray();
	 
	}

	

	
	

}
