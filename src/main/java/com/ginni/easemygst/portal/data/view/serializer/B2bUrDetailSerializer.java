package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;


public class B2bUrDetailSerializer extends BaseDataDetailSerializer<B2bUrTransactionEntity> {

	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");


	@Override
	public void serialize(B2bUrTransactionEntity b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			
			json.writeStartObject();
			
			//json.writeStringField("is_sez", b2b.gets);
			//json.writeStringField("ctin", !StringUtils.isEmpty(b2b.get)?b2b.getCtin():"");
			json.writeStringField("inum", !StringUtils.isEmpty(b2b.getInvoiceNumber())?b2b.getInvoiceNumber():"");
			json.writeStringField("idt", !Objects.isNull(b2b.getInvoiceDate())?gstFmt.format(b2b.getInvoiceDate()):"");
			json.writeStringField("pos", !StringUtils.isEmpty(b2b.getPos())?b2b.getPos():"");
			json.writeStringField("state_name", !StringUtils.isEmpty(b2b.getStateName())?b2b.getStateName():"");

			json.writeNumberField("taxable_val",b2b.getTaxableValue() );
			json.writeNumberField("tax_amt",b2b.getTaxAmount() );
			json.writeStringField("flags", !StringUtils.isEmpty(b2b.getFlags())?b2b.getFlags():"");
			json.writeStringField("type", !StringUtils.isEmpty(b2b.getType())?b2b.getType():"");
			json.writeStringField("source", !StringUtils.isEmpty(b2b.getSource())?b2b.getSource():"");
			json.writeStringField("invoice_id", !StringUtils.isEmpty(b2b.getId())?b2b.getId():"");
			json.writeStringField("sply_ty", !Objects.isNull(b2b.getSupplyType())?String.valueOf(b2b.getSupplyType()):"");
			
			if (Objects.nonNull(b2b) ) {
				if("ERROR".equalsIgnoreCase(b2b.getFlags())||b2b.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(b2b.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(b2b.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}

			json.writeBooleanField("isSynced",b2b.getIsSynced() );
			json.writeBooleanField("isTransit",b2b.getIsTransit() );
			json.writeBooleanField("toBeSynch",b2b.getToBeSync() );
			json.writeBooleanField("isError",b2b.getIsError() );
			json.writeStringField("errorMsg",b2b.getErrMsg() );
			json.writeNumberField("inv_val",b2b.getTaxAmount()+b2b.getTaxableValue() );




			writeLineItems(json, b2b);
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	

	
	void writeLineItems( JsonGenerator json, B2bUrTransactionEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	/*public void writeLineItem(JsonGenerator json,Item item) {
		try {
			json.writeStartObject();// start item

			json.writeNumberField("num", item.getSerialNumber());
			
			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("ad_amt", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			if (!Objects.isNull(item.getCgst()))
				json.writeNumberField("camt", item.getCgst());

			if (!Objects.isNull(item.getSgst()))
				json.writeNumberField("samt", item.getSgst());

			if (!Objects.isNull(item.getCess()))
				json.writeNumberField("csamt", item.getCess());
			//json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
	
}
