package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;

public class HsnSumDetailDeserializer extends BaseDataDetailDeserializer <List<Hsn>> {

	@Override
	public List<Hsn> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<Hsn> hsns = new ArrayList<>();
		Iterator<JsonNode> hsnNodes = node.elements();
		while (hsnNodes.hasNext()) {
			JsonNode hsnNode = hsnNodes.next();
			Hsn hsn = new Hsn();
			
			if (hsnNode.has("hsn_sc"))
				hsn.setCode(hsnNode.get("hsn_sc").asText());
			if (hsnNode.has("hsn_desc"))
				hsn.setDescription(hsnNode.get("hsn_desc").asText());
			if (hsnNode.has("iamt"))
				hsn.setIgst(hsnNode.get("iamt").asDouble());
			if (hsnNode.has("csamt"))
				hsn.setCess(hsnNode.get("csamt").asDouble());
			if (hsnNode.has("camt"))
				hsn.setCgst(hsnNode.get("camt").asDouble());
			if (hsnNode.has("quantity"))
				hsn.setQuantity(hsnNode.get("quantity").asDouble());
			if (hsnNode.has("samt"))
				hsn.setSgst(hsnNode.get("samt").asDouble());
			if (hsnNode.has("source"))
				hsn.setSource(hsnNode.get("source").asText());
			if (hsnNode.has("sourceId"))
				hsn.setSourceId(hsnNode.get("sourceId").asText());
			if (hsnNode.has("taxable_val"))
				hsn.setTaxableValue(hsnNode.get("taxable_val").asDouble());
			/*if (hsnNode.has("taxAmount"))
				hsn.setTaxAmount(hsnNode.get("taxAmount").asDouble());*/
			if (hsnNode.has("unit"))
				hsn.setUnit(hsnNode.get("unit").asText());
			if (hsnNode.has("value"))
				hsn.setValue(hsnNode.get("value").asDouble());
			
			if (hsnNode.has("toBeSync"))
				hsn.setToBeSync(hsnNode.get("toBeSync").asBoolean());
			if (hsnNode.has("isSynced"))
				hsn.setIsSynced(hsnNode.get("isSynced").asBoolean());
			if (hsnNode.has("isTransit"))
				hsn.setIsTransit(hsnNode.get("isTransit").asBoolean());
			if (hsnNode.has("flags"))
				hsn.setFlags(hsnNode.get("flags").asText());
			if (hsnNode.has("invoice_id"))
				hsn.setId(hsnNode.get("invoice_id").asText());
			double taxAmount=0.0;
			taxAmount=hsn.getIgst()+hsn.getCgst()+hsn.getSgst()+hsn.getCess();
			hsn.setTaxAmount(taxAmount);
			hsn.setValue(hsn.getTaxableValue()+hsn.getTaxAmount());
			/*if(hsnNode.has("taxpayerGstinId"))
				hsn.setTaxpayerGstin();*/
			hsns.add(hsn);
		}
		return hsns;
	}

}
