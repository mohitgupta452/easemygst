package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class JobDetails {

	@JsonProperty("desc")
	public String desc;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	public Double iamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("i_int")
	public Double i_int=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
	public Double camt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("c_int")
	public Double c_int=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
	public Double samt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("s_int")
	public Double s_int=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("csamt")
	public Double csamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("cs_int")
	public Double cs_int=0.0;

}
