package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RefundClaim {
	
	@JsonProperty("bankacc")
	private Long bankAccNo;
	
	@JsonProperty("igrfclm")
	private LedgerTax igRefClaim;
	
	@JsonProperty("cgrfclm")
	private LedgerTax cgRefClaim;
	
	@JsonProperty("sgrfclm")
	private LedgerTax sgRefClaim;
	
	@JsonProperty("csrfclm")
	private LedgerTax cessRefClaim;

}
