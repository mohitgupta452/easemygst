package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.HSNSUM.HsnSumData;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;

public class HSNSUMDeserializer extends BaseDataDeserializer<List<Hsn>> {

	@Override
	public List<Hsn> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<Hsn> hsns = new ArrayList<>();
		String errormsg=null;
		String errorcd=null;
		//node.get
//		Hsn hs=new Hsn();
		if(node.has("error_msg"))
			errormsg=node.get("error_msg").asText();
		if(node.has("error_cd"))
			errorcd=node.get("error_cd").asText();
	
		Iterator<JsonNode> hsnNodes = node.elements();
		while (hsnNodes.hasNext()) {
			JsonNode hsnNode = hsnNodes.next();
			Hsn hsn = new Hsn();
//			if (hsnNode.has("chksum"))
//				hsn.setCheckSum(hsnNode.get("chksum").asText());
			if(hsnNode.has("error_cd"))
				errorcd=hsnNode.get("error_cd").asText();
			if(hsnNode.has("error_msg"))
				errormsg=hsnNode.get("error_msg").asText();
			
			Iterator<JsonNode> hsnNodesdata = hsnNode.elements();
			while (hsnNodesdata.hasNext()) {
				JsonNode noddata = hsnNodesdata.next();
				Hsn hsna = new Hsn();
//				if (hsnNode.has("num"))
//					hsn.setSNo(hsnNode.get("num").asInt());
			  if(!StringUtils.isEmpty(errormsg))
				  hsna.setError_msg(errormsg);
			  if(!StringUtils.isEmpty(errorcd))
				  hsna.setError_cd(errorcd);
				
				if (noddata.has("hsn_sc"))
					hsna.setCode(noddata.get("hsn_sc").asText());

				if (noddata.has("desc"))
					hsna.setDescription(noddata.get("desc").asText());
				if (noddata.has("uqc"))
					hsna.setUnit(noddata.get("uqc").asText());
				if (noddata.has("qty"))
					hsna.setQuantity(noddata.get("qty").asDouble());
				if (noddata.has("txval"))
					hsna.setTaxableValue(noddata.get("txval").asDouble());
//				if (hsnNode.has("trt"))
//					hsn.setTaxRate(hsnNode.get("trt").asDouble());
				if (noddata.has("iamt"))
					hsna.setIgst(noddata.get("iamt").asDouble());
				if (noddata.has("camt"))
					hsna.setCgst(noddata.get("camt").asDouble());
				if (noddata.has("samt"))
					hsna.setSgst(noddata.get("samt").asDouble());
				if (noddata.has("csamt"))
					hsna.setCess(noddata.get("csamt").asDouble());
				hsns.add(hsna);

			}
			
			//hsns.add(hsn);
		}
		return hsns;

	}

	public List<HsnSumData> deserializeData(JsonNode dataNode) {

		List<HsnSumData> hsns = new ArrayList<>();
		Iterator<JsonNode> hsnNodes = dataNode.get("data").elements();
		while (hsnNodes.hasNext()) {
			JsonNode hsnNode = hsnNodes.next();
			HsnSumData hsn = new HsnSumData();
			if (hsnNode.has("num"))
				hsn.setSNo(hsnNode.get("num").asInt());
		
			if (hsnNode.has("hsn_sc"))
				hsn.setGoodsOrServiceCode(hsnNode.get("hsn_sc").asText());

			if (hsnNode.has("desc"))
				hsn.setHsnDesc(hsnNode.get("desc").asText());
			if (hsnNode.has("uqc"))
				hsn.setUnit(hsnNode.get("uqc").asText());
			if (hsnNode.has("qty"))
				hsn.setQuantity(hsnNode.get("qty").asDouble());
			if (hsnNode.has("txval"))
				hsn.setTaxableValue(hsnNode.get("txval").asDouble());
//			if (hsnNode.has("trt"))
//				hsn.setTaxRate(hsnNode.get("trt").asDouble());
			if (hsnNode.has("iamt"))
				hsn.setIgstAmt(hsnNode.get("iamt").asDouble());
			if (hsnNode.has("camt"))
				hsn.setCgstAmt(hsnNode.get("camt").asDouble());
			if (hsnNode.has("samt"))
				hsn.setSgstAmt(hsnNode.get("samt").asDouble());
			if (hsnNode.has("csamt"))
				hsn.setCessAmt(hsnNode.get("csamt").asDouble());

			hsns.add(hsn);
		}

		return hsns;

	}

}
