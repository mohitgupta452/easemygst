package com.ginni.easemygst.portal.data.part;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerLiabilityTransactionDetail {

	@JsonFormat(pattern = "DD-MM-YYYY")
	@JsonProperty("dt")
	private Date date;
	
	@JsonProperty("desc")
	private String description;
	
	@JsonProperty("trans_cd")
	private String tranctionCode;
	
	
	
	/*@JsonProperty("tr_tot")
	private String transactionTotal;
	
	@JsonProperty("igst")
	private LedgerTax igst;
	
	@JsonProperty("cgst")
	private LedgerTax cgst;
	
	@JsonProperty("sgst")
	private LedgerTax sgst;*/
	
}
