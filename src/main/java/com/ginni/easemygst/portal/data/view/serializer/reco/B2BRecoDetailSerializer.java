package com.ginni.easemygst.portal.data.view.serializer.reco;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.data.view.serializer.BaseDataDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2BRecoDetailSerializer extends JsonSerializer<B2BDetailEntity> {

	private String returnType = "gstr1";
	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	public final static SimpleDateFormat gstFmt = new SimpleDateFormat("dd/MM/yyyy");


	@Override
	public void serialize(B2BDetailEntity b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		
			json.writeStartObject();
			/*json.writeStringField("ctin", b2b.getCtin());
			json.writeStringField("ctin_name", StringUtils.isNotEmpty(b2b.getCtinName())?b2b.getCtinName():"NO NAME");*/
			//Invoice invoice=b2b.getInvoice();
			json.writeStringField("invoice_id",b2b.getId() );
			json.writeStringField("inum", b2b.getInvoiceNumber());
			json.writeStringField("financialYear",b2b.getFinancialYear() );
			json.writeNumberField("tax_amt",b2b.getTaxAmount() );
			json.writeNumberField("taxable_val",b2b.getTaxableValue() );
			json.writeObjectField("creationTime", b2b.getCreationTime());
			json.writeStringField("createdBy",b2b.getCreatedBy() );
			json.writeStringField("creationIpAddress",b2b.getCreationIPAddress() );
			json.writeStringField("updatedBy",b2b.getUpdatedBy() );
			json.writeStringField("updationIpAddress",b2b.getUpdationIPAddress() );
			json.writeObjectField("updationTime", b2b.getUpdationTime());
			//json.writeStringField("b2bTransactionId",b2b.getB2bTransaction().getId());
			json.writeBooleanField("isAmmendment",b2b.getIsAmmendment());
			json.writeBooleanField("isValid",b2b.getIsValid() );
			json.writeBooleanField("isTransit",b2b.getIsTransit() );
			json.writeBooleanField("isMarked",b2b.getIsMarked() );
			json.writeBooleanField("isSynced",b2b.getIsSynced() );
			json.writeBooleanField("isLocked",b2b.getIsLocked() );
			json.writeStringField("syncId",b2b.getSynchId());
			json.writeStringField("transitId",b2b.getTransitId());
			json.writeStringField("source",b2b.getSource() );
			json.writeStringField("sourceId",b2b.getSourceId());
			json.writeStringField("flags",b2b.getFlags() );
			json.writeStringField("type",b2b.getType() );
			json.writeBooleanField("rchrg",b2b.getReverseCharge());
			json.writeBooleanField("toBeSynch",b2b.isToBeSync() );
			json.writeStringField("inv_type",b2b.getInvoiceType() );
			json.writeStringField("etin", b2b.getEtin());
			if(Objects.isNull(b2b.getInvoiceDate())){
				json.writeStringField("idt", null);
			}else
			json.writeStringField("idt", gstFmt.format(b2b.getInvoiceDate()));						
			json.writeStringField("pos", b2b.getPos());
			json.writeStringField("dataSource", String.valueOf(b2b.getDataSource()));
	
			json.writeStringField("cinv_num", b2b.getcInvoiceNumber());
			if(Objects.isNull(b2b.getcInvoiceDate())){
				json.writeStringField("cidt", null);
			}else
			json.writeStringField("cidt", gstFmt.format(b2b.getcInvoiceDate()));
			json.writeNumberField("ctax_amount", b2b.getcTaxAmount());
			//previous invoice
			json.writeBooleanField("isError",b2b.getIsError() );
			json.writeStringField("errorMsg",b2b.getErrMsg() );
			json.writeStringField("checksum",b2b.getChecksum() );
			//json.writeBooleanField("isDelete",b2b.getis ); is delete
			json.writeStringField("previousFlags",b2b.getPreviousFlags() );
			json.writeStringField("previousType",b2b.getPreviousType() );


			this.writeLineItems(json, b2b);
			json.writeEndObject();
		

	}

	void writeLineItems( JsonGenerator json, B2BDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	public void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			json.writeStringField("id", item.getId());
			json.writeNumberField("num", item.getSerialNumber());
			json.writeStringField("hsn_sc", item.getHsnCode());

			json.writeStringField("hsn_desc", item.getHsnDescription());

			json.writeStringField("unit", item.getUnit());

			json.writeNumberField("quantity", item.getQuantity());

			json.writeNumberField("txval", item.getTaxableValue());

			json.writeNumberField("rt", item.getTaxRate());

			json.writeNumberField("iamt", item.getIgst());

			json.writeNumberField("camt", item.getCgst());

			json.writeNumberField("samt", item.getSgst());

			json.writeNumberField("csamt", item.getCess());

			json.writeStringField("elg", item.getTotalEligibleTax());

			json.writeNumberField("tx_i", item.getItcIgst());
			json.writeNumberField("tx_c", item.getItcCgst());
			json.writeNumberField("tx_s", item.getItcSgst());
			json.writeNumberField("tx_cs", item.getItcCess());
			json.writeStringField("invoiceId", item.getInvoiceId());

			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
