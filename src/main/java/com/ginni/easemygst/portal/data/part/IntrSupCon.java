package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class IntrSupCon {

	@JsonProperty("interc_det")
	private List<InterStateSupplies> interc_det;
	
}
