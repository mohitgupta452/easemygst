package com.ginni.easemygst.portal.data.transaction;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.transaction.factory.Transaction;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class CDN {

	@JsonProperty("cfs")
	private String filingStatus;//added after gov api 2.0 rlease
	
	@JsonProperty("cdn_id")
	String id;

	@JsonProperty("cgstin")
	private String gstin;// cparty tin for gstr2
	
	@JsonProperty("org_gstin")
	private String originalGstin;// for gstr1 new format

	@JsonProperty("ctin_name")
	private String gstinName;

	//@JsonFormat(pattern = "#0.00")
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")
	private Double taxAmount;

	@JsonProperty("cdn")
	private List<CdnData> cdnDatas;

	@JsonInclude(Include.NON_NULL)
	public @Data static class CdnData extends Invoice {

		
		@JsonProperty("cdn_data_id")
		String id;
		
		@JsonProperty("rsn")
		private String reason;
		
		@JsonProperty("p_gst")
		private Boolean isPreGst;

		@JsonProperty("ntty")
		private String creditDebitType;

		@JsonProperty("nt_num")//revised invoice number excel field
		private String creditDebitNum;

		@JsonFormat(pattern = Transaction.jsonDateFormat,timezone=Transaction.jsonDateTimeZone)
		@JsonProperty("nt_dt") // credit date for gstr2
		private Date creditDebitDate;//invoice date
		
		@JsonFormat(pattern = Transaction.jsonDateFormat)
		@JsonProperty("sbdt")
		private Date shippingBillDate;//// for gstr1 new format

		@JsonProperty("sbnum")
		private String shippingBillNum;// for gstr1 new format

		@JsonProperty("ont_num")
		public String origDebtCredtNoteNo;//invoice number 

		@JsonFormat(pattern = Transaction.jsonDateFormat,timezone=Transaction.jsonDateTimeZone)
		@JsonProperty("ont_dt")
		private Date origDebtCredtDate;//invoice date
		
		@JsonProperty("opd")
		private String orgMonthYear;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((creditDebitNum == null) ? 0 : creditDebitNum.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			EqualsBuilder builder = new EqualsBuilder().append(this.getCreditDebitNum(),
					((CdnData) obj).getCreditDebitNum());
			return builder.isEquals();
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((CDN) obj).getGstin());
		return builder.isEquals();
	}
}
