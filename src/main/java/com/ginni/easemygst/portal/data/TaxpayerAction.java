package com.ginni.easemygst.portal.data;

public enum TaxpayerAction {

	MODIFY("M"), DELETE("D"), ACCEPT("A"), REJECT("R"), PENDING("P"), UPLOADED("U"), DRAFT("DRAFT"), 
	MODIFYBYNO("MBYNO"), RECONCILE("RECON"), UPDATE_ITC("UPDATE_ITC"),UPDATE_ITC_BYNO("UPDATE_ITC_BYNO"), UPDATE_MARKER("UPDATE_MARKER"),
	RECO_DATE("RECO_DATE"), RECO_VAL_ROUND_OFF("RECO_VAL_ROUND_OFF"), RECO_TX_ROUND_OFF("RECO_TX_ROUND_OFF"),
	RECO_INVOICE_NO("RECO_INVOICE_NO"),NO_ACTION("N"),REVERT("REVERT"),MATCH_AND_OVERWRITE("MATCH_AND_OVERWRITE");

	private String value;

	TaxpayerAction(String val) {
		this.value = val;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}