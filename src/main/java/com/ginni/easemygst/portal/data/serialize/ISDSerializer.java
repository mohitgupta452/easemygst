package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.data.transaction.ISD;

public class ISDSerializer extends BaseDataSerializer<List<ISD>> {
	
	@Override
	public void serialize(List<ISD> isds, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		for (ISD isd : isds) {
			json.writeStartObject();
			json.writeStringField("chksum", !StringUtils.isEmpty(isd.getCheckSum()) ? isd.getCheckSum() : "");
			json.writeStringField("gstin_isd",
					!StringUtils.isEmpty(isd.getGstin()) ? isd.getGstin() : "");
			json.writeStringField("i_num",
					!StringUtils.isEmpty(isd.getInvoiceDetailNum()) ? isd.getInvoiceDetailNum() : "");
			json.writeStringField("id_dt",
					isd.getInvoiceDetailDate() != null ? gstFmt.format(isd.getInvoiceDetailDate()) : "");
			json.writeStringField("ig_cr", isd.getCreditIgst() != null ? String.valueOf(isd.getCreditIgst()) : "");
			json.writeStringField("cg_cr", isd.getCreditCgst() != null ? String.valueOf(isd.getCreditCgst()) : "");
			json.writeStringField("sg_cr", isd.getCreditSgst() != null ? String.valueOf(isd.getCreditSgst()) : "");
			json.writeEndObject();
		}
		json.writeEndArray();
		

	}

}
