package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2BSerializer extends BaseDataSerializer<List<B2BDetailEntity>> {

	private ReturnType returnType = ReturnType.GSTR1;
	Logger log=LoggerFactory.getLogger(this.getClass());

	private TransactionType transType = TransactionType.B2B;

	@Override
	public void serialize(List<B2BDetailEntity> b2bs, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();

		if (!b2bs.isEmpty()) {
			//b2bs=this.groupByB2bDetailEntity(b2bs);
			ObjectMapper om=new ObjectMapper();
			log.debug("{}",om.writeValueAsString(b2bs));
			Map<String, List<B2BDetailEntity>> b2bDetails = b2bs.stream()
					.collect(Collectors.groupingBy(B2BDetailEntity::getCtin));

			if (b2bs.get(0).getB2bTransaction() != null)
				returnType = ReturnType.valueOf(b2bs.get(0).getB2bTransaction().getReturnType().toUpperCase());

			for (Entry<String, List<B2BDetailEntity>> entry : b2bDetails.entrySet()) {
				json.writeStartObject();
				json.writeStringField("ctin", entry.getKey());
				try {
					writeInvoices(entry.getValue(), json);
				} catch (AppException e) {
					e.printStackTrace();
					new IOException(e);
				}
				json.writeEndObject();
			}
		}
		json.writeEndArray();

	}

	void writeInvoices(List<B2BDetailEntity> b2bs, JsonGenerator json) throws IOException, AppException {
		// Start Invoice Details
		if (!StringUtils.isEmpty(b2bs.get(0).getError_msg()))
			json.writeStringField("error_msg", b2bs.get(0).getError_msg());
		if (!StringUtils.isEmpty(b2bs.get(0).getError_cd()))
			json.writeStringField("error_cd", b2bs.get(0).getError_cd());
		json.writeFieldName("inv");

		json.writeStartArray();
		for (B2BDetailEntity inv : b2bs)

			writeInvoice(json, inv);
		json.writeEndArray();
		// end Invoice Details
	}

	private void writeInvoice(JsonGenerator json, B2BDetailEntity inv) throws AppException, IOException {
		try {

			String flag = null;
			json.writeStartObject();

			json.writeStringField("inum", inv.getInvoiceNumber());

			json.writeStringField("idt", gstFmt.format(inv.getInvoiceDate()));

			if (!StringUtils.isEmpty(this.matchFlag(inv.getFlags()))) {
				flag = this.matchFlag(inv.getFlags());
				if ("D".equalsIgnoreCase(flag))
					json.writeStringField("flag", flag);
				else {
					if ("Y".equalsIgnoreCase(inv.getB2bTransaction().getFillingStatus()))
						json.writeStringField("flag", flag);
					else
						flag = null;
				}
				if (!StringUtils.isEmpty(flag) && !"D".equalsIgnoreCase(flag))
					json.writeStringField("chksum", inv.getChecksum());
			}

			if (StringUtils.isEmpty(flag) || "M".equalsIgnoreCase(flag)) {

				if (inv.getIsAmmendment()) {

					json.writeStringField("oinum", inv.getOriginalInvoiceNumber());
					json.writeStringField("oidt", gstFmt.format(inv.getOriginalInvoiceDate()));
				}


				if (!StringUtils.isEmpty(inv.getPos()))
					json.writeStringField("pos",
							(Integer.parseInt(inv.getPos()) < 10 && inv.getPos().length() == 1) ? "0" + inv.getPos()
									: inv.getPos());

				// if (ReturnType.GSTR1==returnType) {

				if (!StringUtils.isEmpty(inv.getRevCharge()))
					json.writeStringField("rchrg", inv.getRevCharge());

				if (!StringUtils.isEmpty(inv.getEtin()))
					json.writeStringField("etin", inv.getEtin() != null ? String.valueOf(inv.getEtin()) : null);

				if (!StringUtils.isEmpty(inv.getInvoiceType()))
					json.writeStringField("inv_typ", inv.getInvoiceType());
				// }

			}
			if (inv.getItems() != null && !"D".equalsIgnoreCase(flag)) {
               BigDecimal taxableValue=	new ItemSerializer().writeLineItems(json, inv.getItems(), returnType, TransactionType.B2B, flag);
					
               BigDecimal val = BigDecimal.ZERO;
					val = val.add(BigDecimal.valueOf(inv.getTaxAmount()))
							.add(taxableValue);
					json.writeNumberField("val", val.setScale(2, RoundingMode.FLOOR));
			}
			json.writeEndObject();
		} catch (IOException e) {
			// log
			throw e;
		}
	}

	void writeLineItems(JsonGenerator json, B2BDetailEntity inv) throws Exception {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();

		for (Item item : inv.getItems())
			this.writeLineItem(json, item);

		json.writeEndArray();
	}

	private void writeLineItem(JsonGenerator json, Item item) throws Exception {
		try {
			json.writeStartObject();// start item

			json.writeNumberField("num", item.getSerialNumber());

			json.writeFieldName("itm_det");// adding item detail field

			json.writeStartObject();// start item detail

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("txval", item.getTaxableValue());

			if (item.getIgst() != 0.0)
				json.writeNumberField("iamt", item.getIgst());

			if (item.getCgst() != 0.0)
				json.writeNumberField("camt", item.getCgst());

			if (item.getSgst() != 0.0)
				json.writeNumberField("samt", item.getSgst());

			if (item.getCess() != 0.0)
				json.writeNumberField("csamt", item.getCess());

			if (ReturnType.GSTR2 == returnType) {
				if (!StringUtils.isEmpty(item.getTotalEligibleTax())) {
					json.writeStringField("elg",
							item.getTotalEligibleTax().toLowerCase() != null
									? String.valueOf(item.getTotalEligibleTax()).toLowerCase()
									: null);

					// writing itc detail
					json.writeFieldName("itc");// adding itc detail field
					this.writeItcDetail(json, item);
				}
			}

			json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {
			/// log
			throw e;
		}
	}

	private void writeItcDetail(JsonGenerator json, Item item) {
		try {
			// start itcdetail
			json.writeStartObject();

			json.writeNumberField("tx_i", item.getItcIgst());

			json.writeNumberField("tx_c", item.getItcCgst());

			json.writeNumberField("tx_s", item.getItcSgst());

			json.writeNumberField("tx_cs", item.getItcCess());

			json.writeEndObject();
			// end itc detail
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	private List<B2BDetailEntity> groupByB2bDetailEntity(List<B2BDetailEntity> datas){
		List<B2BDetailEntity> result=new  ArrayList<>();
		Map<String, List<B2BDetailEntity>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(B2BDetailEntity::getId));
		for(Map.Entry<String, List<B2BDetailEntity>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			B2BDetailEntity b2bDetailEntity=null;
			for(B2BDetailEntity b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

}
