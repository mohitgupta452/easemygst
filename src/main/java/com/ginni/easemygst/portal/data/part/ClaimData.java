package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ClaimData {

	@JsonFormat(pattern="#0.00")
	@JsonProperty("tax")
	private Double tax;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("pen")
	private Double penality;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("int")
	private Double interest;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("fee")
	private Double fees;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("oth")
	private Double others;
	
}
