package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;

public class CDNDetailDeserializer extends BaseDataDetailDeserializer<List<CDNTransactionEntity>> {

	@Override
	public List<CDNTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<CDNTransactionEntity> cdnTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> cdnNodes = node.elements();
		while (cdnNodes.hasNext()) {
			JsonNode cdnNode = cdnNodes.next();
			CDNTransactionEntity cdnTransactionEntity = new CDNTransactionEntity();
			if (cdnNode.has("org_ctin"))
				cdnTransactionEntity.setOriginalCtin(StringUtils.upperCase(cdnNode.get("org_ctin").asText()));
			if (cdnNode.has("org_ctin_name"))
				cdnTransactionEntity.setOriginalCtinName(cdnNode.get("org_ctin_name").asText());

			List<CDNDetailEntity> cdnDetailEntities = new ArrayList<>();
			cdnDetailEntities.add(this.deserializeInvoice(cdnNode,cdnTransactionEntity));
			cdnTransactionEntity.setCdnDetails(cdnDetailEntities);
			cdnTransactionEntities.add(cdnTransactionEntity);
		}
		return cdnTransactionEntities;
	}
	
	public CDNDetailEntity deserializeInvoice(JsonNode cdnNode,CDNTransactionEntity cdnTransactionEntity ) {
		
		CDNDetailEntity cdnDetailEntity = new CDNDetailEntity();
		cdnDetailEntity.setCdnTransaction(cdnTransactionEntity);
		if (cdnNode.has("ntty"))
			cdnDetailEntity.setNoteType(cdnNode.get("ntty").asText());
		
		String noteDate = cdnNode.has("nt_dt") ? cdnNode.get("nt_dt").asText() : null;
		if (!StringUtils.isEmpty(noteDate)) {
			try {
				cdnDetailEntity.setRevisedInvDate(gstFmt.parse(noteDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if (cdnNode.has("inum"))
			cdnDetailEntity.setInvoiceNumber(cdnNode.get("inum").asText());
		
		String invDate = cdnNode.has("idt") ? cdnNode.get("idt").asText() : null;
		if (!StringUtils.isEmpty(invDate)) {
			try {
				cdnDetailEntity.setInvoiceDate(gstFmt.parse(invDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		/*if (cdnNode.has("taxable_val"))
			cdnDetailEntity.setTaxableValue(cdnNode.get("taxable_val").asDouble());
			
		if (cdnNode.has("tax_amt"))
			cdnDetailEntity.setTaxableValue(cdnNode.get("tax_amt").asDouble());*/
		if (cdnNode.has("nt_num"))
			cdnDetailEntity.setRevisedInvNo(cdnNode.get("nt_num").asText());
		if (cdnNode.has("flags"))
			cdnDetailEntity.setFlags(cdnNode.get("flags").asText());
		if (cdnNode.has("type"))
			cdnDetailEntity.setType(cdnNode.get("type").asText());
		if (cdnNode.has("source"))
			cdnDetailEntity.setSource(cdnNode.get("source").asText());
		if (cdnNode.has("pre_gst"))
			cdnDetailEntity.setPreGstRegime(cdnNode.get("pre_gst").asText());
		if (cdnNode.has("rsn"))
			cdnDetailEntity.setReasonForNote(cdnNode.get("rsn").asText());
		if (cdnNode.has("invoice_id"))
			cdnDetailEntity.setId(cdnNode.get("invoice_id").asText());
		if (cdnNode.has("isMarked"))
			cdnDetailEntity.setIsMarked(cdnNode.get("isMarked").asBoolean());
		/*if (cdnNode.has("ctax_amount"))
			cdnDetailEntity.setcTaxAmount(cdnNode.get("ctax_amount").asDouble());
		if (cdnNode.has("cinv_num"))
			cdnDetailEntity.setcInvoiceNumber(cdnNode.get("cinv_num").asText());*/
		if (cdnNode.has("isSynced"))
			cdnDetailEntity.setIsSynced(cdnNode.get("isSynced").asBoolean());
		if (cdnNode.has("isTransit"))
			cdnDetailEntity.setIsTransit(cdnNode.get("isTransit").asBoolean());
		/*String cinvDate = cdnNode.has("cidt") ? cdnNode.get("cidt").asText() : null;
		if (!StringUtils.isEmpty(cinvDate)) {
			try {
				cdnDetailEntity.setInvoiceDate(gstFmt.parse(cinvDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		*/
		
		if (cdnNode.has("originalNoteNumber"))
			cdnDetailEntity.setOriginalNoteNumber(cdnNode.get("originalNoteNumber").asText());
		String orgNoteDate = cdnNode.has("originalNoteDate") ? cdnNode.get("originalNoteDate").asText() : null;
		if (!StringUtils.isEmpty(orgNoteDate)) {
			try {
				cdnDetailEntity.setOriginalNoteDate(gstFmt.parse(orgNoteDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(StringUtils.isNoneEmpty(cdnDetailEntity.getOriginalNoteNumber())||Objects.nonNull(cdnDetailEntity.getOriginalNoteDate()))
			cdnDetailEntity.setIsAmmendment(true);

		
		cdnDetailEntity.setItems(this.deserializeItem(cdnNode));
		
		return cdnDetailEntity;
	
	}

}
