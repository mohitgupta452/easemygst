package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TaxPaidB {
	
	@JsonProperty("py_det")
	private TaxPaidBPayble paybleDetail;
	
	@JsonProperty("pdcash")
	private List<TaxPaidBByCash>paidByCash;
	
	public @Data static class TaxPaidBPayble{
		
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_int")
		private Double igstIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_int")
		private Double cgstIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_int")
		private Double sgstIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_int")
		private Double cessIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_lfee")
		private Double igstLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_lfee")
		private Double cgstLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_lfee")
		private Double sgstLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_lfee")
		private Double cessLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_ors")
		private Double igstOthrsPayble;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_ors")
		private Double cgstOthrsPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_ors")
		private Double sgstOthrsPayble;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_ors")
		private Double cessOthrsPayble=0.0;
			
		
		
	}
	
public @Data static class TaxPaidBByCash{
		
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_int")
		private Double igstIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_int")
		private Double cgstIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_int")
		private Double sgstIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_int")
		private Double cessIntPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_lfee")
		private Double igstLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_lfee")
		private Double cgstLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_lfee")
		private Double sgstLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_lfee")
		private Double cessLtFeePayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_ors")
		private Double igstOthrsPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_ors")
		private Double cgstOthrsPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_ors")
		private Double sgstOthrsPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_ors")
		private Double cessOthrsPayble=0.0;
			
		
		
	}
	
	

}
