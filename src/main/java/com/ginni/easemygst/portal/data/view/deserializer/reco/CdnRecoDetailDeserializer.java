package com.ginni.easemygst.portal.data.view.deserializer.reco;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.data.view.deserializer.BaseDataDetailDeserializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;

public class CdnRecoDetailDeserializer extends  JsonDeserializer<CDNDetailEntity> {
	
	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd/MM/yyyy");


	@Override
	public CDNDetailEntity deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		return this.deserializeInvoice(node);
	}

	public CDNDetailEntity deserializeInvoice(JsonNode invNode) {
		CDNDetailEntity inv = new CDNDetailEntity();
		if (invNode.has("invoice_id"))
			inv.setId(invNode.get("invoice_id").asText());
		if (invNode.has("inum"))
			inv.setInvoiceNumber(invNode.get("inum").asText());
		if (invNode.has("tax_amt"))
			inv.setTaxAmount(invNode.get("tax_amt").asDouble());
		if (invNode.has("taxable_val"))
			inv.setTaxableValue(invNode.get("taxable_val").asDouble());
		if (invNode.has("createdBy"))
			inv.setCreatedBy(invNode.get("createdBy").asText());
		if (invNode.has("creationIpAddress"))
			inv.setCreationIPAddress(invNode.get("creationIpAddress").asText());
		if (invNode.has("updatedBy"))
			inv.setUpdatedBy(invNode.get("updatedBy").asText());
		if (invNode.has("updationIpAddress"))
			inv.setUpdationIPAddress(invNode.get("updationIpAddress").asText());
		/*if (invNode.has("b2bTransactionId"))
			inv.setb2bt(invNode.get("b2bTransactionId").asText());*/
		if (invNode.has("isAmmendment"))
			inv.setIsAmmendment(invNode.get("createdBy").asBoolean());
		if (invNode.has("isValid"))
			inv.setIsValid(invNode.get("isValid").asBoolean());
		if (invNode.has("isTransit"))
			inv.setIsTransit(invNode.get("isTransit").asBoolean());
		if (invNode.has("isMarked"))
			inv.setIsMarked(invNode.get("isMarked").asBoolean());
		if (invNode.has("isSynced"))
			inv.setIsSynced(invNode.get("isSynced").asBoolean());
		if (invNode.has("isLocked"))
				inv.setIsLocked(invNode.get("isLocked").asBoolean());
		if (invNode.has("syncId"))
			inv.setSynchId(invNode.get("syncId").asText());
		if (invNode.has("transitId"))
			inv.setTransitId(invNode.get("transitId").asText());
		if (invNode.has("source"))
			inv.setSource(invNode.get("source").asText());
		if (invNode.has("sourceId"))
			inv.setSourceId(invNode.get("sourceId").asText());
		if (invNode.has("flags"))
			inv.setFlags(invNode.get("flags").asText());
		if (invNode.has("type"))
			inv.setType(invNode.get("type").asText());
		
		String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
		if (!StringUtils.isEmpty(invDate)) {
			try {
				inv.setInvoiceDate(gstFmt.parse(invDate));
			} catch (ParseException e) {
				
				e.printStackTrace();
			}

		}
		
		if (invNode.has("revisedInvNo"))
			inv.setRevisedInvNo(invNode.get("revisedInvNo").asText());
		if (invNode.has("noteType"))
			inv.setNoteType(invNode.get("noteType").asText());
		
		
		
		String revInvDate = invNode.has("revisedInvDate") ? invNode.get("revisedInvDate").asText() : null;
		if (!StringUtils.isEmpty(revInvDate)) {
			try {
				inv.setRevisedInvDate(gstFmt.parse(revInvDate));
			} catch (ParseException e) {
				
				e.printStackTrace();
			}

		}
		if (invNode.has("preGstRegime"))
			inv.setPreGstRegime(invNode.get("preGstRegime").asText());
		
		if (invNode.has("rsn"))
			inv.setReasonForNote(invNode.get("rsn").asText());
		if (invNode.has("inv_type"))
			inv.setInvoiceType(invNode.get("inv_type").asText());	
		
		if (invNode.has("dataSource"))
			inv.setDataSource(DataSource.valueOf(invNode.get("dataSource").asText()));	
		
		if (invNode.has("toBeSynch"))
			inv.setToBeSync(invNode.get("toBeSynch").asBoolean());
		
		
		
		
		if (invNode.has("cinv_num"))
			inv.setcInvoiceNumber(invNode.get("cinv_num").asText());
		
		String cinvDate = invNode.has("cidt") ? invNode.get("cidt").asText() : null;
		if (!StringUtils.isEmpty(cinvDate)) {
			try {
				inv.setcInvoiceDate(gstFmt.parse(cinvDate));
			} catch (ParseException e) {
				
				e.printStackTrace();
			}

		}
		
		if (invNode.has("ctax_amount"))
			inv.setcTaxAmount(invNode.get("ctax_amount").asDouble());
		
		if (invNode.has("isError"))
			inv.setIsError(invNode.get("isError").asBoolean());
		if (invNode.has("errorMsg"))
			inv.setErrMsg(invNode.get("errorMsg").asText());
		if (invNode.has("checksum"))
			inv.setChecksum(invNode.get("checksum").asText());
		if (invNode.has("previousFlags"))
			inv.setPreviousFlags(invNode.get("previousFlags").asText());
		if (invNode.has("previousType"))
			inv.setPreviousType(invNode.get("previousType").asText());
		if (invNode.has("ctaxable_val"))
			inv.setcTaxAmount(invNode.get("ctaxable_val").asDouble());
		
		inv.setItems(this.deserializeItem(invNode));

		return inv;
	}

	
	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			double taxAmount=0.0;
			Item item = new Item();
			JsonNode itemNode = items.next();
			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			if (itemNode.has("hsn_sc"))
				item.setHsnCode(itemNode.get("hsn_sc").asText());
			if (itemNode.has("hsn_desc"))
				item.setHsnDescription(itemNode.get("hsn_desc").asText());
			if (itemNode.has("unit"))
				item.setUnit(itemNode.get("unit").asText());
			if (itemNode.has("quantity"))
				item.setQuantity(itemNode.get("quantity").asDouble());
			if (itemNode.has("ad_amt"))
				item.setTaxableValue(itemNode.get("ad_amt").asDouble());
			if (itemNode.has("txval"))
				item.setTaxableValue(itemNode.get("txval").asDouble());
			if (itemNode.has("rt"))
				item.setTaxRate(itemNode.get("rt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgst(itemNode.get("iamt").asDouble());
			if (itemNode.has("camt"))
				item.setCgst(itemNode.get("camt").asDouble());
			if (itemNode.has("samt"))
				item.setSgst(itemNode.get("samt").asDouble());
			if (itemNode.has("csamt"))
				item.setCess(itemNode.get("csamt").asDouble());
			if (itemNode.has("elg"))
			item.setTotalEligibleTax(itemNode.get("elg").asText());
			if (itemNode.has("tx_i")){
				item.setItcIgst(itemNode.get("tx_i").asDouble());
				
			}
			if (itemNode.has("tx_c")){
				item.setItcCgst(itemNode.get("tx_c").asDouble());
			}
			if (itemNode.has("tx_s")){
				item.setItcSgst(itemNode.get("tx_s").asDouble());
				
			}
			if (itemNode.has("tx_cs")){
				item.setItcCess(itemNode.get("tx_cs").asDouble());
			}
			
			/*if (itemNode.has("cs"))
				item.setId(itemNode.get("csamt").asText());*/
			if (itemNode.has("invoiceId"))
				item.setInvoiceId(itemNode.get("invoiceId").asText());
			
			
			taxAmount=item.getIgst()+item.getCgst()+item.getSgst()+item.getCess();
			item.setTaxAmount(taxAmount);
			itms.add(item);
			}
			
		return itms;
	}
	
}
