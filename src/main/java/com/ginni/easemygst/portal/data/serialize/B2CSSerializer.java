package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.ejb.TransactionAttributeType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.DoubleOrderedMap;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.GstMetadata.Returns;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.utils.TaxCalcuator;

public class B2CSSerializer extends
		BaseDataSerializer<List<com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity>> {

	static final RoundingMode _ROUNDING_MODE = RoundingMode.HALF_UP;

	@Override
	public void serialize(List<B2CSTransactionEntity> b2css, final JsonGenerator json,
			SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
		json.writeStartArray();
		Map<String, List<B2CSTransactionEntity>> b2clDetails = b2css.stream()
				.collect(Collectors.groupingBy(B2CSTransactionEntity::getPos));

		for (Entry<String, List<B2CSTransactionEntity>> entry : b2clDetails.entrySet()) {

			writeInvoices(entry.getValue(), json);

		}
		json.writeEndArray();

	}

	public void writeInvoices(List<B2CSTransactionEntity> b2csList, JsonGenerator json) throws IOException {
		
		try {

			boolean isAmendment = false;
			ArrayNode supplyNode = _MAPPER.createArrayNode();

			Map<Double, List<B2CSTransactionEntity>> b2clDetails = b2csList.stream()
					.collect(Collectors.groupingBy(B2CSTransactionEntity::getTaxRate));
			boolean isFirst = false;
			BigDecimal taxRate, taxvalue, iamt, camt, samt, csamt;
			String flags = null;

			for (Entry<Double, List<B2CSTransactionEntity>> entry : b2clDetails.entrySet()) {

				ObjectNode rateObjectNode = _MAPPER.createObjectNode();
				taxRate = taxvalue = iamt = camt = samt = csamt = BigDecimal.ZERO;
				int delCount = 0;
				json.writeStartObject();
				boolean isInterstate = false;

				for (B2CSTransactionEntity b2cs : entry.getValue()) {
					if (!isFirst) {
						isAmendment = b2cs.getIsAmmendment();
						json.writeStringField("pos",
								(Integer.parseInt(b2cs.getPos()) < 10 && b2cs.getPos().length() == 1)
										? "0" + b2cs.getPos()
										: b2cs.getPos());
						if (b2cs.getSupplyType() != null)
							json.writeStringField("sply_ty", String.valueOf(b2cs.getSupplyType()));
						json.writeStringField("typ", "OE");
						// if(!StringUtils.isEmpty(b2cs.getEtin()))
						// json.writeStringField("etin", b2cs.getEtin());
						if (!isAmendment)
							json.writeNumberField("rt", entry.getKey());
						else {

							json.writeStringField("omon", b2cs.getOriginalMonth());
							// json.writeStringField("opos", b2cs.getOriginalPos());
						}
						isFirst = true;
					}
					if (b2cs.getSupplyType() != null)
						isInterstate = String.valueOf(b2cs.getSupplyType()).equalsIgnoreCase("INTER") ? true : false;

					if (!"D".equalsIgnoreCase(this.matchFlag(b2cs.getFlags()))) {

						if (!isAmendment) {
							taxRate = BigDecimal.valueOf(entry.getKey());
							taxvalue = taxvalue
									.add(BigDecimal.valueOf(b2cs.getTaxableValue()).setScale(2, _ROUNDING_MODE));
							if (!CollectionUtils.isEmpty(b2cs.getItems())) {
								iamt = iamt.add(BigDecimal.valueOf(b2cs.getItems().get(0).getIgst()).setScale(2,
										_ROUNDING_MODE));
								camt = camt.add(BigDecimal.valueOf(b2cs.getItems().get(0).getCgst()).setScale(2,
										_ROUNDING_MODE));
								samt = samt.add(BigDecimal.valueOf(b2cs.getItems().get(0).getSgst()).setScale(2,
										_ROUNDING_MODE));
								csamt = csamt.add(BigDecimal.valueOf(b2cs.getItems().get(0).getCess()).setScale(2,
										_ROUNDING_MODE));
							}
						} else {

							new ItemSerializer().writeLineItems(json, b2cs.getItems(), ReturnType.GSTR1,
									TransactionType.B2CSA, flags);
						}
						flags = b2cs.getFlags();
					} else {
						delCount++;
					}
					if (entry.getValue().size() == delCount) {
						json.writeStringField("flag", "D");
					}
				}

				if (!isAmendment) {
					isFirst = false;

					if (entry.getValue().size() != delCount) {

						taxvalue = TaxCalcuator.getValidGstTaxableValue(samt, iamt, taxRate, taxvalue);

						if (isAmendment)
							json.writeNumberField("rt", entry.getKey());
						json.writeNumberField("txval", taxvalue);
						if (isInterstate /* iamt.compareTo(BigDecimal.ZERO)!=0 */)
							json.writeNumberField("iamt", iamt);
						if (!isInterstate /* camt.compareTo(BigDecimal.ZERO)!=0 */)
							json.writeNumberField("camt", camt);
						if (!isInterstate /* samt.compareTo(BigDecimal.ZERO)!=0 */)
							json.writeNumberField("samt", samt);
						if (BigDecimal.ZERO.compareTo(csamt) != 0)
							json.writeNumberField("csamt", csamt);
					}
				}
				json.writeEndObject();

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException(e);
		}
	}

}
