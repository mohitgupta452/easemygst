package com.ginni.easemygst.portal.data.returns;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.ATDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CLDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CSDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNDeserializer;
import com.ginni.easemygst.portal.data.deserialize.EXPDeserializer;
import com.ginni.easemygst.portal.data.deserialize.HSNSUMDeserializer;
import com.ginni.easemygst.portal.data.deserialize.NILDeserializer;
import com.ginni.easemygst.portal.data.part.ECOM;
import com.ginni.easemygst.portal.data.serialize.ATSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BURSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CLSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CSSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNURSerializer;
import com.ginni.easemygst.portal.data.serialize.DocIssueSerializer;
import com.ginni.easemygst.portal.data.serialize.EXPSerializer;
import com.ginni.easemygst.portal.data.serialize.HSNSUMSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPGSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPSBSerializer;
import com.ginni.easemygst.portal.data.serialize.NILSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.view.serializer.TXPDetailSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

import lombok.Data;


@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public @Data class GSTR1 implements Serializable {
	
	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("fp")
	private String monthYear;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("gt")
	private BigDecimal grossTurnover;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("cur_gt")
	private BigDecimal curgrossTurnover;
	
	@JsonDeserialize(using=B2BDeserializer.class)
	@JsonSerialize(using = B2BSerializer.class)
	@JsonProperty("b2b")
	private List<B2BDetailEntity> b2b;

	@JsonDeserialize(using=B2BDeserializer.class)
	@JsonSerialize(using = B2BSerializer.class)
	@JsonProperty("b2ba")
	private List<B2BDetailEntity> b2ba;

	@JsonDeserialize(using=B2CLDeserializer.class)
	@JsonSerialize(using = B2CLSerializer.class)
	@JsonProperty("b2cl")
	private List<B2CLDetailEntity> b2cl;
	
	@JsonDeserialize(using=B2CLDeserializer.class)
	@JsonSerialize(using = B2CLSerializer.class)
	@JsonProperty("b2cla")
	private List<B2CLDetailEntity> b2cla;
	
	@JsonDeserialize(using=B2CSDeserializer.class)
	@JsonSerialize(using = B2CSSerializer.class)
	@JsonProperty("b2cs")
	private List<B2CSTransactionEntity> b2cs;
	
	@JsonDeserialize(using=B2CSDeserializer.class)
	@JsonSerialize(using = B2CSSerializer.class)
	@JsonProperty("b2csa")
	private List<B2CSTransactionEntity> b2csa;
	
	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNSerializer.class)
	@JsonProperty("cdnr")
	private List<CDNDetailEntity> cdn;

	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNSerializer.class)
	@JsonProperty("cdnra")
	private List<CDNDetailEntity> cdna;
	
	@JsonDeserialize(using=NILDeserializer.class)
	@JsonSerialize(using=NILSerializer.class)
	@JsonProperty("nil")
	private List<NILTransactionEntity> nil;
	
	@JsonDeserialize(using=EXPDeserializer.class)
	@JsonSerialize(using=EXPSerializer.class)
	@JsonProperty("exp")
	private List<EXPTransactionEntity> exp;
	
	@JsonDeserialize(using=EXPDeserializer.class)
	@JsonSerialize(using=EXPSerializer.class)
	@JsonProperty("expa")
	private List<EXPTransactionEntity> expa;
	
	@JsonSerialize(using=ATSerializer.class)
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonProperty("at")
	private List<ATTransactionEntity> at;
	
	@JsonSerialize(using=ATSerializer.class)
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonProperty("ata")
	private List<ATTransactionEntity> ata;

	@JsonSerialize(using=TXPSerializer.class)
	@JsonProperty("txpd")
	private List<TxpdTransaction> txpd;
	
	@JsonSerialize(using=TXPSerializer.class)
	@JsonProperty("txpda")
	private List<TxpdTransaction> txpda;
	
	@JsonProperty("ecom")
	private List<ECOM> ecom;
	
	@JsonDeserialize(using=HSNSUMDeserializer.class)
	@JsonSerialize(using=HSNSUMSerializer.class)
	@JsonProperty("hsn")
	private List<Hsn> hsnsum;
	
	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNURSerializer.class)
	@JsonProperty("cdnur")
	private List<CDNURDetailEntity> cdnur;
	
	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonSerialize(using = CDNURSerializer.class)
	@JsonProperty("cdnura")
	private List<CDNURDetailEntity> cdnura;
	
	@JsonSerialize(using = IMPGSerializer.class)
	@JsonProperty("imp_g")
	private List<IMPGTransactionEntity> impg;
	
	@JsonSerialize(using = IMPSBSerializer.class)
	@JsonProperty("imp_s")
	private List<IMPSTransactionEntity> imps;
	
	@JsonSerialize(using = B2BURSerializer.class)
	@JsonProperty("b2bur")
	private List<B2bUrTransactionEntity> b2bur;
	
	@JsonSerialize(using=DocIssueSerializer.class)
	@JsonProperty("doc_issue")
	private List<DocIssue> docissue;
	
}
