package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.data.transaction.HSNSUM.HsnSumData;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.utils.Utility;

public class HSNSUMSerializer extends BaseDataSerializer<List<Hsn>> {

	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");

	//private String returnType = "gstr1";
	
	static final String[] uqcList = {"BAG","BAL","BDL", "BKL","BOU", "BOX", "BTL", "BUN", "CAN", "CBM", "CCM", "CMS", "CTN", "DOZ", 
			"DRM", "GGK", "GMS", "GRS", "GYD", "KGS", "KLR", "KME", "MLT", "MTR", "MTS", "NOS", "PAC", "PCS", "PRS", "QTL", "ROL", "SET",
			"SQF", "SQM", "SQY", "TBS", "TGM", "THD", "TON", "TUB", "UGS", "UNT", "YDS", "OTH"};
	

	@Override
	public void serialize(List<Hsn> hsnsums, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		
		
		 ReturnType returnType = ReturnType.GSTR1;
		 String returnTyp="GSTR1";
		
		
	//	json.writeStartArray();
		json.writeStartObject();
		if(!hsnsums.isEmpty()){
			if (!Objects.isNull(hsnsums.get(0).getFlags()) && !StringUtils.isEmpty(hsnsums.get(0).getFlags()))
				json.writeStringField("flag",
						hsnsums.get(0).getFlags() != null ? this.matchFlag(hsnsums.get(0).getFlags()) : null);
			
		else {
			
		 Map<String,List<Hsn>> hsnDetails=hsnsums.stream().collect(Collectors.groupingBy(Hsn::getCode));
		 if(!hsnsums.isEmpty())
		   returnTyp=hsnsums.get(0).getReturnType();
		
		 if(!StringUtils.isEmpty(returnTyp)){
		 if(returnTyp.equalsIgnoreCase("GSTR2"))
			 json.writeFieldName("det");
		 else
			 json.writeFieldName("data");
		 }
		//if(!StringUtils.isEmpty(returnTyp))
		json.writeStartArray();
		
		int i=1;

		for(Entry<String,List<Hsn>> entry:hsnDetails.entrySet()){
			if(StringUtils.isEmpty(entry.getKey()))
			{
				IOException io=new IOException(ApplicationMetadata.HSN_CODE_MISSING_IN_SOME_ITEMS);
				
				throw io;
				
			}
			json.writeStartObject();
			json.writeStringField("hsn_sc", entry.getKey());
			boolean isFirst=false;
			BigDecimal taxValue,iamt,camt,csamt,samt,val,quantity,valTot;
			taxValue=iamt=camt=csamt=samt=val=quantity=valTot=BigDecimal.ZERO;
			
			for(Hsn hsnsum:entry.getValue()){
				if(!isFirst){
						json.writeStringField("uqc",checkUqc(hsnsum.getUnit()).toUpperCase());
					 if(!StringUtils.isEmpty(hsnsum.getDescription()))
					    json.writeStringField("desc",!StringUtils.isEmpty(hsnsum.getDescription()) ? (hsnsum.getDescription().length()>30 ? hsnsum.getDescription().substring(0, 29) : hsnsum.getDescription()) : "");
					 isFirst=true;
				}
				
				if(hsnsum.getTaxableValue()!=0.0)
					taxValue=taxValue.add(Utility.getBigDecimalScale(hsnsum.getTaxableValue()));
				if(hsnsum.getIgst()!=0.0)
					iamt=iamt.add(Utility.getBigDecimalScale(hsnsum.getIgst()));
				if(hsnsum.getCgst()!=0.0)
					camt=camt.add(Utility.getBigDecimalScale(hsnsum.getCgst()));
				if(hsnsum.getSgst()!=0.0)
					samt=samt.add(Utility.getBigDecimalScale(hsnsum.getSgst()));
				if(hsnsum.getCess()!=0.0)
					csamt=csamt.add(Utility.getBigDecimalScale(hsnsum.getCess()));
				if(hsnsum.getQuantity()!=0)
					quantity=quantity.add(Utility.getBigDecimalScale(hsnsum.getQuantity()));
					
				valTot=valTot.add(Utility.getBigDecimalScale((hsnsum.getTaxableValue())).add(Utility.getBigDecimalScale(hsnsum.getIgst())).add(Utility.getBigDecimalScale(hsnsum.getCgst()))
							 .add(Utility.getBigDecimalScale(hsnsum.getCess())).add(Utility.getBigDecimalScale(hsnsum.getSgst())));
				
			}
			json.writeNumberField("num", i++);
			json.writeNumberField("iamt", iamt);//
		    json.writeNumberField("txval",taxValue);//
		    json.writeNumberField("camt",camt);//
		    json.writeNumberField("samt",samt);//
		    json.writeNumberField("csamt",csamt);//
		    json.writeNumberField("qty",quantity);//
		    json.writeNumberField("val", valTot);
			json.writeEndObject();
		}
		json.writeEndArray();
	 }
   }			
		json.writeEndObject();
	//	json.writeEndArray();
	}

	

	

	void writeHsnsumDatas(JsonGenerator json, Hsn hsnsum) throws IOException {
		// Start HsnsumDatas
//		   taxValue=taxValue+hsnsum.getTaxableValue();
//		   iamt=iamt+hsnsum.getIgst();
//		   samt=samt+hsnsum.getSgst();
//		   csamt=csamt+hsnsum.getCess();
//		   camt=camt+hsnsum.getCgst();
//		   quantity=quantity+hsnsum.getQuantity();
//		   val=val+(hsnsum.getTaxableValue()+hsnsum.getIgst()+hsnsum.getCgst()+hsnsum.getCess()+hsnsum.getSgst());
	}

	private void writeHsnsumData( JsonGenerator json, HsnSumData hData) {
		try {
			json.writeStartObject();// start item
			
		    json.writeNumberField("num", hData.getSNo());
			json.writeStringField("hsn_sc",checkEmpty( hData.getGoodsOrServiceCode()));//
			json.writeStringField("desc",checkEmpty( hData.getHsnDesc()));//
			json.writeStringField("uqc", checkUqc(hData.getUnit()));//
			json.writeNumberField("qty", hData.getQuantity());//
			//json.writeNumberField("val", hData.gete);
			json.writeNumberField("txval",hData.getTaxableValue());//
			//json.writeStringField("trt", hData.getTaxRate() != null ? String.valueOf(hData.getTaxRate()) : "");//
			json.writeNumberField("iamt", hData.getIgstAmt());//
			json.writeNumberField("camt", hData.getCgstAmt());//
			json.writeNumberField("samt", hData.getSgstAmt());//
			json.writeNumberField("csamt", hData.getCessAmt());//
			json.writeEndObject();// end item
		} catch (Exception e) {
			/// log
			e.printStackTrace();
		}
	}
	
	private static String checkUqc(String str) {
		if(StringUtils.isEmpty(str)) {
			return "OTH";
		}
		
		String str1 = "^[a-zA-Z]*$";
		final Pattern pattern1 = Pattern
				.compile(str1);
		if (!pattern1.matcher(str).matches()) {
			return "OTH";
		}
		if(Arrays.asList(uqcList).indexOf(str) == -1)
			return "OTH";
		
		return str;
	}
	
	private static String checkEmpty(String str){
		if(!StringUtils.isEmpty(str))
			return str.trim();
		else
			return "";
	}

	
	
	

}
