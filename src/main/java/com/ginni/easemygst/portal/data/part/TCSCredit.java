package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TCSCredit {

	@JsonProperty("tcs_det")
	private List<CreditDetails> tcs_det;
	
}
