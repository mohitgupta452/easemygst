package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3ExporRfrcAdj {

	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_cl_camt")
	private Double exportCgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_cl_samt")
	private Double exportSgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_cl_iamt")
	private Double exportIgstAmount;
}
