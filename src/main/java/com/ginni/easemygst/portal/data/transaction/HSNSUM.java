package com.ginni.easemygst.portal.data.transaction;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class HSNSUM {

	@JsonProperty("hsnsumId")
	String id;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;
	
	@JsonProperty("tax_amount")
	private Double taxAmount;
	
	@JsonProperty("flag")
	public TaxpayerAction taxPayerAction;

	@JsonProperty("chksum")
	public String checkSum;
	
	@JsonProperty("hsn")
	public List<HsnSumData> datas;
	
	@JsonInclude(Include.NON_NULL)
	public @Data static class HsnSumData extends InvoiceItem {
		
		@JsonProperty("hsnDataId")
		String id;
		
		private boolean isValid;
		
		private boolean isGstnSynced = false;
		
		private Map<String, String> error;

		private boolean isAmendment;
		
		private ReconsileType type;


		private String flags = "";
		
		private boolean isTransit = false;
		
		private String transitId = "";


		@JsonProperty("flag")
		private TaxpayerAction taxPayerAction;
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((datas== null) ? 0 : datas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.datas, ((HSNSUM) obj).getDatas());               
	   return builder.isEquals();
	}
}
