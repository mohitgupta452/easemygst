package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TTL {

	@JsonProperty("ttltx_det")
	private List<TotalTaxLiabilityDetails> ttltx_det;

}
