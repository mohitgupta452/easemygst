package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ImportDetails {

	@JsonProperty("ty")
	private String ty;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("aval")
	private Double aval=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	private Double iamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_i")
	private Double itc_i=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("csamt")
	private Double csamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_cs")
	private Double itc_cs=0.0;

}
