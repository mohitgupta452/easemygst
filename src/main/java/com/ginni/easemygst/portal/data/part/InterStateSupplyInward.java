package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class InterStateSupplyInward {

	@JsonProperty("ty")
	private String ty;
	
	@JsonProperty("state_cd")
	private String state_cd;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("txrt")
	private Double txrt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("txval")
	private Double txval=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	private Double iamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_i")
	private Double itc_i=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("cess")
	private Double cess=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_cs")
	private Double itc_cs=0.0;

}
