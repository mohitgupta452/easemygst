package com.ginni.easemygst.portal.data.part;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.TaxpayerAction;
@JsonInclude(Include.NON_NULL)
public @lombok.Data class ITCR {
/*No unique field find*/
	@JsonProperty("flag")
	public TaxpayerAction taxPayerAction;

	@JsonProperty("chksum")
	public String checkSum;
	
	@JsonProperty("rsn")
	private String reasonForReversal;
	
	@JsonProperty("des")
	private String detailItcTobeReversed;
	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
    public Double igstReversalAmt=0.0;
    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iint")
    public Double igstInterestApplicable=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
    public Double cgstReversalAmt=0.0;
    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("cint")
    public Double cgstInterestApplicable=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
    public Double sgstReversalAmt=0.0;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("sint")
    public Double sgstInterestApplicable=0.0;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((checkSum == null) ? 0 : checkSum.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getCheckSum(), ((ITCR) obj).getCheckSum());               
	   return builder.isEquals();
	}

    
	
	
}
