package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.transaction.impl.CDNURTransaction.CDNURInvType;

public class CDNURDetailDeserializer extends BaseDataDetailDeserializer<List<CDNURTransactionEntity>> {

	@Override
	public List<CDNURTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<CDNURTransactionEntity> cdnurTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> cdnurNodes = node.elements();
		while (cdnurNodes.hasNext()) {
			JsonNode cdnurNode = cdnurNodes.next();
			CDNURTransactionEntity cdnurTransactionEntity = new CDNURTransactionEntity();
			if (cdnurNode.has("org_ctin"))
				cdnurTransactionEntity.setOriginalCtin(cdnurNode.get("org_ctin").asText());
			if (cdnurNode.has("org_ctin_name"))
				cdnurTransactionEntity.setOriginalCtinName(cdnurNode.get("org_ctin_name").asText());
			List<CDNURDetailEntity> cdnurDetailEntity = new ArrayList<>();
			cdnurTransactionEntity.setCdnUrDetails(cdnurDetailEntity);
			cdnurDetailEntity.add(this.deserializeDetail(cdnurNode,cdnurTransactionEntity));
			cdnurTransactionEntities.add(cdnurTransactionEntity);
		}
		return cdnurTransactionEntities;
	}

	public CDNURDetailEntity deserializeDetail(JsonNode cdnurNode,CDNURTransactionEntity cdnurTransactionEntity)throws IOException {
		CDNURDetailEntity cdnurEntity = new CDNURDetailEntity();
		cdnurEntity.setCdnUrTransaction(cdnurTransactionEntity);
		if(cdnurNode.has("ntty"))
			cdnurEntity.setNoteType(cdnurNode.get("ntty").asText());
		
		String noteDate = cdnurNode.has("nt_dt") ? cdnurNode.get("nt_dt").asText() : null;
		if (!StringUtils.isEmpty(noteDate)) {
			try {
				cdnurEntity.setRevisedInvDate(gstFmt.parse(noteDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(cdnurNode.has("inum"))
			cdnurEntity.setInvoiceNumber(cdnurNode.get("inum").asText());
		
		String invDate = cdnurNode.has("idt") ? cdnurNode.get("idt").asText() : null;
		if (!StringUtils.isEmpty(invDate)) {
			try {
				cdnurEntity.setInvoiceDate(gstFmt.parse(invDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(cdnurNode.has("tax_amt"))
			cdnurEntity.setTaxAmount(cdnurNode.get("tax_amt").asDouble());
		if(cdnurNode.has("nt_num"))
			cdnurEntity.setRevisedInvNo(cdnurNode.get("nt_num").asText());
		if(cdnurNode.has("flags"))
			cdnurEntity.setFlags(cdnurNode.get("flags").asText());
		if(cdnurNode.has("type"))
			cdnurEntity.setType(cdnurNode.get("type").asText());
		if(cdnurNode.has("source"))
			cdnurEntity.setSource(cdnurNode.get("source").asText());
		if(cdnurNode.has("invoice_id"))
			cdnurEntity.setId(cdnurNode.get("invoice_id").asText());
		if(cdnurNode.has("pre_gst"))
			cdnurEntity.setPreGstRegime(cdnurNode.get("pre_gst").asText());
		if(cdnurNode.has("rsn"))
			cdnurEntity.setReasonForNote(cdnurNode.get("rsn").asText());
		if(cdnurNode.has("isMarked"))
			cdnurEntity.setIsMarked(cdnurNode.get("isMarked").asBoolean());
		
		
		if (cdnurNode.has("originalNoteNumber"))
			cdnurEntity.setOriginalNoteNumber(cdnurNode.get("originalNoteNumber").asText());
		String orgNoteDate = cdnurNode.has("originalNoteDate") ? cdnurNode.get("originalNoteDate").asText() : null;
		if (!StringUtils.isEmpty(orgNoteDate)) {
			try {
				cdnurEntity.setOriginalNoteDate(gstFmt.parse(orgNoteDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(StringUtils.isNoneEmpty(cdnurEntity.getOriginalNoteNumber())||Objects.nonNull(cdnurEntity.getOriginalNoteDate()))
			cdnurEntity.setIsAmmendment(true);
		
		cdnurEntity.setInvoiceType(CDNURInvType.valueOf(cdnurNode.get("inv_type").asText()).toString());
		if(StringUtils.isEmpty(cdnurEntity.getInvoiceType())){
			IOException ioe=new IOException("Invalid invoice type please enter the valid invoice type");
			throw ioe;
			
		}
			
		
		cdnurEntity.setItems(this.deserializeItem(cdnurNode));
		
		
		return cdnurEntity;
	}
}
