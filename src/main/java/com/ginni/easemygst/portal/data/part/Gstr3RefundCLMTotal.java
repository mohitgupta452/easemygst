package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3RefundCLMTotal {

	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_camt")
	private Double itcCsgtAmount;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_samt")
	private Double itcSsgtAmount;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_iamt")
	private Double itcIsgtAmount;
}
