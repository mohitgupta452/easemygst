package com.ginni.easemygst.portal.data.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

@JsonInclude(Include.NON_NULL)
public @lombok.Data class IMPG   {
		
	
	@JsonProperty("impgId")
	String id;
	
	@JsonProperty("ctin")
	private String gstin;
	
	private boolean isValid=true;

	@JsonFormat(pattern="#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tax_amount")//overall tax amount
	private Double taxAmount;
	
	private boolean isAmendment;

	private Map<String, String> error;
	
	private String flags = "";

	private String source;
	
	private String sourceId;
	
	private boolean isTransit = false;
	
	private String transitId = "";
	
	@JsonProperty("flag")
	private TaxpayerAction taxPayerAction= TaxpayerAction.UPLOADED;;
	
	
	private ReconsileType type;
	
	private String reconcileFlags;

	private boolean isGstnSynced = false;//

	@JsonProperty("chksum")
	private String checkSum;
		
	@JsonProperty("boe_num")
	public String billOfEntryNum;
	
	@JsonFormat(pattern=Transaction.jsonDateFormat)
	@JsonProperty("boe_dt") 
	public Date billOfEntryDate;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("boe_val")
	public Double billOfEntryVal;
	
	@JsonProperty("itms")
	private List<InvoiceItem> impgInvItms;
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getGstin(), ((IMPG) obj).getGstin());               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode());
		return result;
	}

}
