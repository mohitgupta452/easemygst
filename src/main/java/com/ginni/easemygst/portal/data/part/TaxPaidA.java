package com.ginni.easemygst.portal.data.part;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TaxPaidA {
	
	@JsonProperty("txpay")
	private TxPay taxPayble;
	
	@JsonProperty("pdcash")
	private List<TaxPaidAByCash>txPdByCash;
	
	@JsonProperty("pdcr")
	List<TaxPaidAByItcCredit>txPdByCredit;
	
	
	
	public @Data static class TxPay{
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("i_py")
		private Double igstPayble=0.0;
	
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("c_py")
		private Double cgstPayble=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("s_py")
		private Double sgstPayble=0.0;

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cs_py")
		private Double cessPayble=0.0;
		
	}
	
	
public @Data static class TaxPaidAByCash{
		
		@JsonProperty("debitno")
		private String debitNo;
	
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("ipd")
		private Double igstPaid=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cpd")
		private Double cgstPaid=0.0;
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("spd")
		private Double sgstPaid=0.0;

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cspd")
		private Double cessPaid=0.0;
		
	}


public @Data static class TaxPaidAByItcCredit{
	
	@JsonProperty("debitno")
	private String debitNo;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("i_pdi")
	private Double igstPaidUsngIgst=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("i_pdc")
	private Double igstPaidUsngCgst=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("i_pds")
	private Double igstPaidUsngSgst=0.0;
	
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("c_pdi")
	private Double cgstPaidUsngIgst=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("c_pdc")
	private Double cgstPaidUsngCgst=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("s_pdi")
	private Double sgstPaidUsngIgst=0.0;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("s_pds")
	private Double cgstPaidUsngSgst=0.0;
		
	@JsonProperty("cs_pdcs")
	private Double cessPaidUsngCess=0.0;
	
}
	

}
