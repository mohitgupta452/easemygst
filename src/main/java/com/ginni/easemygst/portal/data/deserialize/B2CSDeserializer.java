package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.impl.B2CLTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2CSTransaction;

public class B2CSDeserializer extends BaseDataDeserializer<List<B2CSTransactionEntity>> {

	@Override
	public List<B2CSTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<B2CSTransactionEntity> b2css = new ArrayList<>();
		Iterator<JsonNode> b2bNodes = node.elements();
		while (b2bNodes.hasNext()) {
			JsonNode b2csNode = b2bNodes.next();
			B2CSTransactionEntity b2cs = new B2CSTransactionEntity();
//			if (b2csNode.has("sply_ty"))
//				b2cs.setSupplyType(b2csNode.get("sply_ty").asText());
//			if (b2csNode.has("chksum"))
//				b2cs.setCheckSum(b2csNode.get("chksum").asText());
//			if (b2csNode.has("rt"))
//				b2cs.sett(b2csNode.get("rt").asDouble());
			if(b2csNode.has("typ"))
				b2cs.setB2csType(b2csNode.get("typ").asText());
			if(b2csNode.has("etin"))
				b2cs.setEtin(b2csNode.get("etin").asText());
			if (b2csNode.has("pos"))
				b2cs.setPos(b2csNode.get("pos").asText());
			if (b2csNode.has("txval"))
				b2cs.setTaxableValue(b2csNode.get("txval").asDouble());
			if(b2csNode.has("error_msg"))
				b2cs.setError_msg(b2csNode.get("error_msg").asText());
			if(b2csNode.has("error_cd"))
				b2cs.setError_cd(b2csNode.get("error_cd").asText());
//			if(b2csNode.has("iamt"))
//				b2cs.setIgst(b2csNode.get("iamt").asDouble());
//			if(b2csNode.has("samt"))
//				b2cs.setSgst(b2csNode.get("samt").asDouble());
			List<Item> listItem=new ArrayList<>();
			if(b2csNode.has("rt")){
				Item item=new Item();
				item.setTaxRate(b2csNode.get("rt").asDouble());
				listItem.add(item);
				b2cs.setItems(listItem);
			}
//			while(invoices.hasNext()){
//				JsonNode invNode = invoices.next();
//				
//				b2cs.setItems(invNode.get("itms"));
//			}
			

			b2css.add(b2cs);

		}
		return b2css;

	}

}
