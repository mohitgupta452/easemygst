package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerUtilizeITC {

	@JsonProperty("stscd")
	private String statusCode;
	
	@JsonProperty("db_no")
	private String dbno;
	
}
