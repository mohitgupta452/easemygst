package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.data.transaction.TDSTCS;
import com.ginni.easemygst.portal.data.transaction.TDSTCS.TdsInvoice;

public class TDSTCSSerializer extends BaseDataSerializer<List<TDSTCS>> {
	@Override
	public void serialize(List<TDSTCS> tdss, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		for (TDSTCS tds : tdss) {

			json.writeStartObject();
			json.writeStringField("ctin", !StringUtils.isEmpty(tds.getDeductorTin()) ? tds.getDeductorTin() : "");
			writeInvoices(tds, json);
			json.writeEndObject();
		}
		json.writeEndArray();		

	}

	void writeInvoices(TDSTCS tds, JsonGenerator json) throws IOException {
		// Start Invoice Details
		json.writeFieldName("tds_invoices");
		json.writeStartArray();
		tds.getTdsInvoices().forEach(inv -> writeInvoice(json, inv));
		json.writeEndArray();
		// end Invoice Details
	}
	
	private void writeInvoice(JsonGenerator json, TdsInvoice inv) {
		try {

			json.writeStartObject();
			json.writeStringField("chksum", !StringUtils.isEmpty(inv.getCheckSum())?inv.getCheckSum():"");
			json.writeStringField("irt", inv.getTaxRate() != null ? String.valueOf(inv.getTaxRate()) : "");
			json.writeStringField("iamt", inv.getIgstAmt() != null ? String.valueOf(inv.getIgstAmt()) : "");
			json.writeStringField("camt", inv.getCgstAmt() != null ? String.valueOf(inv.getCgstAmt()) : "");
			json.writeStringField("samt", inv.getSgstAmt() != null ? String.valueOf(inv.getSgstAmt()) : "");
			json.writeStringField("csamt", inv.getCessAmt() != null ? String.valueOf(inv.getCessAmt()) : "");

			json.writeEndObject();
		} catch (Exception e) {
			// log
			e.printStackTrace();
		}
	}

}
