package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3ExporRfrcTP {

	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_rf_camt")
	private Double exportRefundCgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_rf_samt")
	private Double exportRefundSgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_rf_iamt")
	private Double exportRefundIgstAmount;
}
