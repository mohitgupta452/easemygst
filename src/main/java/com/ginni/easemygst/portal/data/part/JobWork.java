package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class JobWork {

	@JsonProperty("job_det")
	public List<JobDetails> job_det;
	
}
