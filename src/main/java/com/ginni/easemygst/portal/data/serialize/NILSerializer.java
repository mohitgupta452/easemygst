package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;

public class NILSerializer extends BaseDataSerializer<List<NILTransactionEntity>> {

	@Override
	public void serialize(List<NILTransactionEntity> nils, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartObject();
	 if(!nils.isEmpty()){
		boolean isFirst=false;
		String returnType=null;
		if (!Objects.isNull(nils.get(0).getFlags()) && !StringUtils.isEmpty(nils.get(0).getFlags()))
			json.writeStringField("flag",
					nils.get(0).getFlags() != null ? this.matchFlag(nils.get(0).getFlags()) : null);
		else{
		
		if(!nils.isEmpty()){
			returnType=nils.get(0).getReturnType();
		if(!StringUtils.isEmpty(returnType)){
		if(returnType.equalsIgnoreCase("GSTR1")){
			json.writeFieldName("inv");
			json.writeStartArray();
		}}
		
		Map<NilSupplyType,List<NILTransactionEntity>> expDetails=nils.stream().collect(Collectors.groupingBy(NILTransactionEntity::getSupplyType));
		 BigDecimal expt_amt,nil_amt,ngsp_amt,comp_amt;
		
		for(Entry<NilSupplyType, List<NILTransactionEntity>> entry:expDetails.entrySet()){
			 expt_amt = nil_amt = ngsp_amt = comp_amt=BigDecimal.ZERO;
			if(returnType.equalsIgnoreCase("GSTR1"))
			 json.writeStartObject();
			 if(returnType.equalsIgnoreCase("GSTR1")){
			  for(NilSupplyType supplyType:NilSupplyType.values()){
				if(supplyType.equals(entry.getKey()))
					json.writeStringField("sply_ty",supplyType.getValue());
				}
			  }else{
				  json.writeFieldName(entry.getKey().getValue().toLowerCase());
			  }
			for(NILTransactionEntity nil:entry.getValue()){
				expt_amt=expt_amt.add(BigDecimal.valueOf(nil.getTotExptAmt()).setScale(2, RoundingMode.FLOOR));
				nil_amt=nil_amt.add(BigDecimal.valueOf(nil.getTotNilAmt()).setScale(2, RoundingMode.FLOOR));
				ngsp_amt=ngsp_amt.add(BigDecimal.valueOf(nil.getTotNgsupAmt()).setScale(2, RoundingMode.FLOOR));
				comp_amt=comp_amt.add(BigDecimal.valueOf(nil.getTotCompAmt()).setScale(2, RoundingMode.FLOOR));
			}
			if(returnType.equalsIgnoreCase("GSTR2")){
				json.writeStartObject();
				json.writeNumberField("cpddr",comp_amt);
				json.writeNumberField("exptdsply", expt_amt);
				json.writeNumberField("ngsply", ngsp_amt);
				json.writeNumberField("nilsply", nil_amt);
				json.writeEndObject();
			}
			else{
				json.writeNumberField("expt_amt",expt_amt);
				json.writeNumberField("nil_amt", nil_amt);
				json.writeNumberField("ngsup_amt",ngsp_amt);
				json.writeEndObject();
			}
			
		}
		}
		if(returnType.equalsIgnoreCase("GSTR1")){
		json.writeEndArray();
		}
	  }
	}
json.writeEndObject();
	}

	void writeLineItems(JsonGenerator json, NILTransactionEntity nil) throws IOException {
		// Start Line Items
		json.writeFieldName("inv");
		json.writeStartArray();
		//nil.forEach(item -> writeLineItem(json, item));
		// End Line Items
		json.writeEndArray();
		// End Line Items

		json.writeEndArray();
	}
	


}
