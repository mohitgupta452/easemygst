
package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class EXPSerializer extends BaseDataSerializer<List<ExpDetail>> {

	// final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");
	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	
	private ReturnType returnType = ReturnType.GSTR1;
	
	private TransactionType transType = TransactionType.EXP;

	@Override
	public void serialize(List<ExpDetail> exps, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		//exps=this.groupByDetailEntity(exps);
		 Map<String,List<ExpDetail>> expDetails=exps.stream().collect(Collectors.groupingBy(ExpDetail::getExportType));
			for(Entry<String, List<ExpDetail>> entry:expDetails.entrySet()){
				json.writeStartObject();
				if(!StringUtils.isEmpty(entry.getKey()))
				   json.writeStringField("exp_typ", entry.getKey());
				writeInvoices(entry.getValue(), json);
				json.writeEndObject();
			}
			
		json.writeEndArray();
	}

	void writeInvoices(List<ExpDetail> exp, JsonGenerator json) throws IOException {
		// Start Invoice Details
		if(!StringUtils.isEmpty(exp.get(0).getError_msg()))
			json.writeStringField("error_msg", exp.get(0).getError_msg());	
		if(!StringUtils.isEmpty(exp.get(0).getError_cd()))
			json.writeStringField("error_cd", exp.get(0).getError_cd());
		json.writeFieldName("inv");
		json.writeStartArray();
		exp.forEach(expl -> {
			try{
				json.writeStartObject();
				// json.writeStringField("chksum",expl.getC );
				json.writeStringField("inum", checkEmpty(expl.getInvoiceNumber()));
				//if(expl.getInvoiceDate()!=null)
				  json.writeStringField("idt", gstFmt.format(expl.getInvoiceDate()));
				  if (!Objects.isNull(expl.getFlags()) && !StringUtils.isEmpty(expl.getFlags()))
						json.writeStringField("flag",
								expl.getFlags() != null ? this.matchFlag(expl.getFlags()) : null);
				  else{
					  
			    if(expl.getIsAmmendment()) {
			    	json.writeStringField("oinum", expl.getOriginalInvoiceNumber());
			    	json.writeStringField("oidt", gstFmt.format(expl.getOriginalInvoiceDate()));
			    }
				if(expl.getTaxableValue()!=0)  
				 json.writeNumberField("val", BigDecimal.valueOf(expl.getTaxableValue()).setScale(2, RoundingMode.FLOOR));
//				if(!StringUtils.isEmpty(expl.getShippingBillPortCode()))
//				  json.writeStringField("sbpcode",expl.getShippingBillPortCode());
//				if(isInteger(expl.getShippingBillNo()))	
//				   json.writeNumberField("sbnum",Integer.parseInt(expl.getShippingBillNo()));
				if(expl.getShippingBillDate()!=null)
			      json.writeStringField("sbdt", gstFmt.format(expl.getShippingBillDate()));
			    
			    new ItemSerializer().writeLineItems(json, expl.getItems(), returnType,transType);
				  }
				//this.writeItems(json,expl.getItems());
			    json.writeEndObject();
			    			    
			}catch(Exception e){
				e.printStackTrace();
			}
		});
		json.writeEndArray();

		
		
		// end Invoice Details
	}
	
	
	private void writeLineItem(JsonGenerator json,Item item){
		try{
			json.writeStartObject();
			json.writeNumberField("txval", item.getTaxableValue());
			json.writeNumberField("rt", item.getTaxRate());
			json.writeNumberField("iamt", item.getIgst());
			json.writeEndObject();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	void writeLineItems(JsonGenerator json, ExpDetail inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		//inv.getInvoiceData().getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	
	private static String checkEmpty(String str){
		if(!StringUtils.isEmpty(str))
			return str;
		else
			return "";
	}
	
	public static boolean isInteger(String str) {
	    if (str == null) {
	        return false;
	    }
	    int length = str.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (str.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	        i = 1;
	    }
	    for (; i < length; i++) {
	        char c = str.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
	}
	
	
	private List<ExpDetail> groupByDetailEntity(List<ExpDetail> datas){
		List<ExpDetail> result=new  ArrayList<>();
		Map<String, List<ExpDetail>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(ExpDetail::getId));
		for(Map.Entry<String, List<ExpDetail>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			ExpDetail b2bDetailEntity=null;
			for(ExpDetail b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

}
