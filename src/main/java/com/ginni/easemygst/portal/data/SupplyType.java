package com.ginni.easemygst.portal.data;

public enum SupplyType {
	
	
	INTER("INTER"), INTRA("INTRA"),    
	INTERSTATE_REGISTERED("INTERSTATE_REGISTERED"),INTRASTATE_REGISTERED("INTRASTATE_REGISTERED"), 
	INTERSTATE_CONSUMER("INTERSTATE_CONSUMER"),INTRASTATE_CONSUMER("INTRASTATE_CONSUMER");
	    
		private String value;

		SupplyType(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }

	    public void setValue(String value) {
	        this.value = value;
	    }
	

}
