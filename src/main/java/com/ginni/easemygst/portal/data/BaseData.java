package com.ginni.easemygst.portal.data;

import java.util.List;

import lombok.Data;

public abstract @Data class BaseData {
	
	private List<Invoice> invoices;
	
}
