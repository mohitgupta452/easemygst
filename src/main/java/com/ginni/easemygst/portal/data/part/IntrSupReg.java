package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class IntrSupReg {

	@JsonProperty("in_det")
	private List<InterStateSupplies> in_det;

}
