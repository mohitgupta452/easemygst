package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.deserialize.BaseDataDeserializer;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.impl.B2CLTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2CSTransaction;

public class DocIssueDetailDeserializer extends BaseDataDeserializer<List<DocIssue>> {

	@Override
	public List<DocIssue> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<DocIssue> b2css = new ArrayList<>();
		Iterator<JsonNode> b2bNodes = node.elements();
		while (b2bNodes.hasNext()) {
			JsonNode b2csNode = b2bNodes.next();
			DocIssue b2cs = new DocIssue();
			if (b2csNode.has("category"))
				b2cs.setCategory(b2csNode.get("category").asText());
		
			if (b2csNode.has("snoFrom"))
				b2cs.setSnoFrom(b2csNode.get("snoFrom").asText());
			
			if (b2csNode.has("snoTo"))
				b2cs.setSnoTo(b2csNode.get("snoTo").asText());
			
			if (b2csNode.has("canceled"))
				b2cs.setCanceled(b2csNode.get("canceled").asInt());
			if (b2csNode.has("totalNumber"))
				b2cs.setTotalNumber(b2csNode.get("totalNumber").asInt());

			if (b2csNode.has("netIssued"))
				b2cs.setNetIssued(b2csNode.get("netIssued").asInt());
			
			if (b2csNode.has("source"))
				b2cs.setSource(b2csNode.get("source").asText());

			
			b2css.add(b2cs);


		}
		return b2css;

	}

}
