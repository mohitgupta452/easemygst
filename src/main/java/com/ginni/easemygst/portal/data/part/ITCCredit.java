package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ITCCredit {

	@JsonProperty("itc_det")
	private List<CreditDetails> itc_det;
	
}
