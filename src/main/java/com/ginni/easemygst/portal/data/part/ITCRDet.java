package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ITCRDet {

	@JsonProperty("itcr_rev")
	public List<ITCR> itcr_rev;
	
}
