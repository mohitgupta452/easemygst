package com.ginni.easemygst.portal.data.view.deserializer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;

public abstract class BaseDataDetailDeserializer <T> extends JsonDeserializer<T> {
	
	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd/MM/yyyy");
	
	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			double taxAmount=0.0;
			Item item = new Item();
			JsonNode itemNode = items.next();
			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			if (itemNode.has("hsn_sc"))
				item.setHsnCode(itemNode.get("hsn_sc").asText());
			if (itemNode.has("hsn_desc"))
				item.setHsnDescription(itemNode.get("hsn_desc").asText());
			if (itemNode.has("unit"))
				item.setUnit(itemNode.get("unit").asText());
			if (itemNode.has("quantity"))
				item.setQuantity(itemNode.get("quantity").asDouble());
			if (itemNode.has("ad_amt"))
				item.setTaxableValue(itemNode.get("ad_amt").asDouble());
			if (itemNode.has("txval"))
				item.setTaxableValue(itemNode.get("txval").asDouble());
			if (itemNode.has("rt"))
				item.setTaxRate(itemNode.get("rt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgst(itemNode.get("iamt").asDouble());
			if (itemNode.has("camt"))
				item.setCgst(itemNode.get("camt").asDouble());
			if (itemNode.has("samt"))
				item.setSgst(itemNode.get("samt").asDouble());
			if (itemNode.has("csamt"))
				item.setCess(itemNode.get("csamt").asDouble());
			if (itemNode.has("elg"))
			item.setTotalEligibleTax(itemNode.get("elg").asText());
			ExcelError ee=new ExcelError();
			if (itemNode.has("tx_i")){
				item.setItcIgst(itemNode.get("tx_i").asDouble());
				/*ee= new B2BTransaction().validateItcTaxAmt("transaction", 1, 1, String.valueOf(item.getIgst()), item.getTotalEligibleTax(),
						String.valueOf(item.getItcIgst()),  ee);*/
				
			}
			if (itemNode.has("tx_c")){
				item.setItcCgst(itemNode.get("tx_c").asDouble());
				/*ee= new B2BTransaction().validateItcTaxAmt("transaction", 1, 1, String.valueOf(item.getCgst()), item.getTotalEligibleTax(),
						String.valueOf(item.getItcCgst()),  ee);*/
			}
			if (itemNode.has("tx_s")){
				item.setItcSgst(itemNode.get("tx_s").asDouble());
				/*ee= new B2BTransaction().validateItcTaxAmt("transaction", 1, 1, String.valueOf(item.getSgst()), item.getTotalEligibleTax(),
						String.valueOf(item.getItcSgst()),  ee);*/
				
			}
			if (itemNode.has("tx_cs")){
				item.setItcCess(itemNode.get("tx_cs").asDouble());
				/*ee= new B2BTransaction().validateItcTaxAmt("transaction", 1, 1, String.valueOf(item.getCess()), item.getTotalEligibleTax(),
						String.valueOf(item.getItcCess()),  ee);*/
			}
			
			
			
			/*if(ee.isError()||ee.isFinalError()){
				AppException ae=new AppException();
				ae.setMessage(ee.getErrorDesc().toString());
				throw ae;
			}*/
			taxAmount=item.getIgst()+item.getCgst()+item.getSgst()+item.getCess();
			item.setTaxAmount(taxAmount);
			itms.add(item);
			}
			
		return itms;
	}
	
	/*public ItcDetail deserializeItcDetail(JsonNode itemNode,InvoiceItem invItem){
		JsonNode itcNode=itemNode.has("itc")?itemNode.get("itc"):null;
		ItcDetail itcDetail=new ItcDetail();
		if (itemNode.has("elg"))
			invItem.setEligOfTotalTax(itcNode.get("elg").asText());
		if(!Objects.isNull(itcNode)){
			if (itemNode.has("tx_i"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_i").asDouble());
			if (itemNode.has("tx_c"))
				itcDetail.setTotalTaxAvalCgst(itcNode.get("tx_c").asDouble());
			if (itemNode.has("tx_s"))
				itcDetail.setTotalTaxAvalSgst(itcNode.get("tx_s").asDouble());
			if (itemNode.has("tx_cs"))
				itcDetail.setTotalTaxAvalCess(itcNode.get("tx_cs").asDouble());
		}
		return itcDetail;
	}*/
	
	

}
