package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Imp {

	@JsonProperty("imp_det")
	private List<ImportDetails> imp_det;
	
}
