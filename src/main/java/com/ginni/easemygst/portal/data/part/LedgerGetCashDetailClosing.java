package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerGetCashDetailClosing {

	@JsonProperty("desc")
	private String description;
		
	@JsonProperty("cl_tot")
	private String closingTotal;
	
	@JsonProperty("igst")
	private LedgerTax igst;
	
	@JsonProperty("cgst")
	private LedgerTax cgst;
	
	@JsonProperty("sgst")
	private LedgerTax sgst;
	
	@JsonProperty("cess")
	private LedgerTax cess;
	
}
