package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TaxLiabilityOUTS {

	@JsonProperty("tottx_det")
	private List<TotalTaxLiabilityDetails> tottx_det;
	
}
