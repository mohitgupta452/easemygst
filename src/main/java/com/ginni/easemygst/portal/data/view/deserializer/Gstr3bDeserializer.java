package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.Gstr3BTransactionType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.InwardSupplyType;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class Gstr3bDeserializer {

	/*public  List<TblOutwardCombinedNew> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {*/
	public  List<TblOutwardCombinedNew> deserialize(JsonNode data)
			throws IOException, JsonProcessingException, AppException {
		/*ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);*/
		JsonNode node = data;
		List<TblOutwardCombinedNew> flatDatas = new ArrayList<>();

		/*
		 * JsonNode supplyDetail=null; JsonNode interSupply=null; JsonNode
		 * itcElg=null; JsonNode inwardSupply=null; JsonNode intrstLtfee=null;
		 */

		if (node.has("sup_details")) {
			flatDatas.addAll(this.deserializeSupplyDetail(node.get("sup_details")));
		}
		if (node.has("inter_sup")) {
			flatDatas.addAll(this.deserializeInterSupply(node.get("inter_sup")));
		}
		
		if (node.has("itc_elg")) {
			flatDatas.addAll(this.deserializeItcDetail(node.get("itc_elg")));
		}

		if (node.has("inward_sup")) {
			flatDatas.addAll(this.deserializeInwardSupply(node.get("inward_sup")));
		}
		if (node.has("intr_ltfee")) {
			flatDatas.addAll(this.deserializeIntrsLateFeeDetails(node.get("intr_ltfee")));
		}

		return flatDatas;
	}

	private List<TblOutwardCombinedNew> deserializeSupplyDetail(JsonNode node)throws AppException {
		List<TblOutwardCombinedNew> supplyDetails = new ArrayList<>();
		if (node.has("osup_det")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("osup_det"),Gstr3BTransactionType.PART1A));
		}
		if (node.has("osup_zero")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("osup_zero"),Gstr3BTransactionType.PART1B));
		}
		if (node.has("osup_nil_exmp")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("osup_nil_exmp"),Gstr3BTransactionType.PART1C));
		}
		if (node.has("isup_rev")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("isup_rev"),Gstr3BTransactionType.PART1D));
		}
		if (node.has("osup_nongst")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("osup_nongst"),Gstr3BTransactionType.PART1E));
		}
		return supplyDetails;
	}

	private List<TblOutwardCombinedNew> deserializeInterSupply(JsonNode node)throws AppException {
		List<TblOutwardCombinedNew> unRegDetails = new ArrayList<>();
		List<TblOutwardCombinedNew> compDetails = new ArrayList<>();
		List<TblOutwardCombinedNew> uinDetails = new ArrayList<>();
		if (node.has("unreg_details")) {
			Iterator<JsonNode> unDetailsNode = node.get("unreg_details").elements();
			while (unDetailsNode.hasNext()) {
				JsonNode detail = unDetailsNode.next();
				TblOutwardCombinedNew tempUnregItem=this.deserializeBaseAmounts(detail,Gstr3BTransactionType.PART2A);
				if(!Objects.isNull(tempUnregItem))
				unRegDetails.add(tempUnregItem);
			}
		}
		if (node.has("comp_details")) {
			Iterator<JsonNode> unDetailsNode = node.get("comp_details").elements();
			while (unDetailsNode.hasNext()) {
				JsonNode detail = unDetailsNode.next();
				TblOutwardCombinedNew tempUnregItem=this.deserializeBaseAmounts(detail,Gstr3BTransactionType.PART2B);
				if(!Objects.isNull(tempUnregItem))
				compDetails.add(tempUnregItem);
			}
		}

		if (node.has("uin_details")) {
			Iterator<JsonNode> unDetailsNode = node.get("uin_details").elements();
			while (unDetailsNode.hasNext()) {
				JsonNode detail = unDetailsNode.next();
				TblOutwardCombinedNew tempUnregItem=this.deserializeBaseAmounts(detail,Gstr3BTransactionType.PART2C);
				if(!Objects.isNull(tempUnregItem))
				uinDetails.add(tempUnregItem);
			}
		}
		unRegDetails.addAll(compDetails);
		unRegDetails.addAll(uinDetails);

		return unRegDetails;
	}

	private List<TblOutwardCombinedNew> deserializeInwardSupply(JsonNode node) {
		List<TblOutwardCombinedNew> inwards = new ArrayList<>();

		Iterator<JsonNode> unDetailsNode = node.get("isup_details").elements();
		while (unDetailsNode.hasNext()) {
			JsonNode detail = unDetailsNode.next();
			TblOutwardCombinedNew rowInter = new TblOutwardCombinedNew();
			TblOutwardCombinedNew rowIntra = new TblOutwardCombinedNew();
			rowInter.setSupply_Type(SupplyType.INTER.toString());
			rowIntra.setSupply_Type(SupplyType.INTRA.toString());


			if (InwardSupplyType.GST.toString().equalsIgnoreCase(detail.get("ty").asText())) {
					rowInter.setTransaction_Type(Gstr3BTransactionType.PART4A.toString());
					rowIntra.setTransaction_Type(Gstr3BTransactionType.PART4A.toString());

				if (detail.has("inter")){
					rowInter.setTaxable_Value(detail.get("inter").asDouble());
				}
				if (detail.has("intra")) {
					rowIntra.setTaxable_Value(detail.get("intra").asDouble());


				}

			} else if (InwardSupplyType.NONGST.toString().equalsIgnoreCase(detail.get("ty").asText())) {
				rowInter.setTransaction_Type(Gstr3BTransactionType.PART4B.toString());
				rowIntra.setTransaction_Type(Gstr3BTransactionType.PART4B.toString());


				if (detail.has("inter"))
					rowInter.setTaxable_Value(detail.get("inter").asDouble());
					//rowInter.setSupply_Type(SupplyType.INTER.toString());

				if (detail.has("intra")) {
					rowIntra.setTaxable_Value(detail.get("intra").asDouble());



				}
			}
			inwards.add(rowInter);
			inwards.add(rowIntra);

		}

		return inwards;
	}

	
	private List<TblOutwardCombinedNew> deserializeItcDetail(JsonNode node)throws AppException {
		List<TblOutwardCombinedNew> supplyDetails = new ArrayList<>();
		if (node.has("impg")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("impg"),Gstr3BTransactionType.PART3A));
		}
		if (node.has("imps")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("imps"),Gstr3BTransactionType.PART3B));
		}
		if (node.has("isrc")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("isrc"),Gstr3BTransactionType.PART3C));
		}
		if (node.has("isd")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("isd"),Gstr3BTransactionType.PART3D));
		}
		if (node.has("rul")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("rul"),Gstr3BTransactionType.PART3E));
		}
		if (node.has("other")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("other"),Gstr3BTransactionType.PART3F));
		}
		//itc rev,net,inelg
		/*if (node.has("itc_net")) {
			supplyDetails.add(this.deserializeBaseAmounts(node.get("itc_net"),Gstr3BTransactionType.PART3I));
		}*/
		if (node.has("itc_rev")) {
			JsonNode subNode=node.get("itc_rev");
			supplyDetails.add(this.deserializeBaseAmounts(subNode.get("rul"), Gstr3BTransactionType.PART3G));

			supplyDetails.add(this.deserializeBaseAmounts(subNode.get("other"), Gstr3BTransactionType.PART3H));

		}
		if (node.has("itc_inelg")) {
			JsonNode subNode=node.get("itc_inelg");

			supplyDetails.add(this.deserializeBaseAmounts(subNode.get("rul"), Gstr3BTransactionType.PART3J));

			supplyDetails.add(this.deserializeBaseAmounts(subNode.get("other"), Gstr3BTransactionType.PART3K));

		}
		//end itc rev net inelg
		return supplyDetails;
	}
	
	
	private List<TblOutwardCombinedNew> deserializeIntrsLateFeeDetails(JsonNode node)throws AppException {
		List<TblOutwardCombinedNew>intrLtFeeDetails=new ArrayList<>();
		if (node.has("intr_details")) {
			TblOutwardCombinedNew intrstLateFee = new TblOutwardCombinedNew();

			intrstLateFee = this.deserializeBaseAmounts(node.get("intr_details"),Gstr3BTransactionType.PART5A);
			intrLtFeeDetails.add(intrstLateFee);
		}
		
		if (node.has("ltFee_details")) {
			TblOutwardCombinedNew intrstLateFee = new TblOutwardCombinedNew();
			intrstLateFee = this.deserializeBaseAmounts(node.get("ltFee_details"),Gstr3BTransactionType.PART5B);
			intrLtFeeDetails.add(intrstLateFee);

		}
		

		return intrLtFeeDetails;
	}

	private TblOutwardCombinedNew deserializeBaseAmounts(JsonNode detail,Gstr3BTransactionType type) throws AppException {
		TblOutwardCombinedNew row = new TblOutwardCombinedNew();
		if(!Objects.isNull(type)){
			row.setTransaction_Type(type.toString());
		}
		if (detail.has("txval"))
			row.setTaxable_Value(detail.get("txval").asDouble());
		if (detail.has("iamt"))
			row.setIgst(detail.get("iamt").asDouble());
		if (detail.has("camt"))
			row.setCgst(detail.get("camt").asDouble());
		if (detail.has("samt"))
			row.setSgst(detail.get("samt").asDouble());
		if (detail.has("csamt"))
			row.setCess(detail.get("csamt").asDouble());
		if (detail.has("pos"))
			row.setTo_State(detail.get("pos").asText());
		
		if(!Objects.isNull(type)){
			if (row.getTransaction_Type().equalsIgnoreCase(Gstr3BTransactionType.PART2A.toString())
					|| row.getTransaction_Type().equalsIgnoreCase(Gstr3BTransactionType.PART2B.toString())
					|| row.getTransaction_Type().equalsIgnoreCase(Gstr3BTransactionType.PART2C.toString())) {
				if((row.getCess()+row.getCgst()+row.getIgst()+row.getSgst()+row.getTaxable_Value())>0 && StringUtils.isEmpty(row.getTo_State())){
				AppException ae=new AppException();
				ae.setMessage("State code/name is mandatory in case of inter-State supplies made to unregistered persons, composition taxable persons and UIN holders ");
				throw ae;
			}else if((row.getCess()+row.getCgst()+row.getIgst()+row.getSgst()+row.getTaxable_Value())==0 && StringUtils.isEmpty(row.getTo_State())){
				return null;
			}else if(!StringUtils.isEmpty(row.getTo_State())&&row.getTo_State().length()==1){
				row.setTo_State("0"+row.getTo_State());
			}
				
				
			}
		}
		return row;
	}

}
