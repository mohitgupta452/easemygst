package com.ginni.easemygst.portal.data.transaction.helper;

import java.util.Date;

import lombok.Data;

public class ExcelError {

	boolean isFinalError = false;
	
	boolean isError = false;

	StringBuilder errorDesc = new StringBuilder("");

	boolean isGstin;//

	Date date;

	boolean isIgst;

	int posValue;
	
	String pos;
	
	boolean isReverseCharge;
	
	String uniqueRowValue;
	
	boolean noValidationCheckForGinesys;
	
	boolean sewopOrWopay=false;
	
	String gstinPosImps="";
	String posImps="";
	
	String noteType;
	
	String preGstRegime;
	
	Date originalInvoiceDate;
	
	public Date getOriginalInvoiceDate() {
		return originalInvoiceDate;
	}

	public void setOriginalInvoiceDate(Date originalInvoiceDate) {
		this.originalInvoiceDate = originalInvoiceDate;
	}

	public boolean resetFieldsExceptErrorAndErrorDesc(){
		
		this.setDate(null);
		this.setGstin(false);
		this.setIgst(false);
		this.setPosValue(-1);
		this.setPos("");
		this.setReverseCharge(false);
		this.setSewopOrWopay(false);
		this.setGstinPosImps("");
		this.setPosImps("");
		this.setNoteType("");
		this.setPreGstRegime("");
		
		return true;
	}

	public boolean isFinalError() {
		return isFinalError;
	}

	public void setFinalError(boolean isFinalError) {
		this.isFinalError = isFinalError;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public StringBuilder getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(StringBuilder errorDesc) {
		this.errorDesc = errorDesc;
	}

	public boolean isGstin() {
		return isGstin;
	}

	public void setGstin(boolean isGstin) {
		this.isGstin = isGstin;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isIgst() {
		return isIgst;
	}

	public void setIgst(boolean isIgst) {
		this.isIgst = isIgst;
	}

	public int getPosValue() {
		return posValue;
	}

	public void setPosValue(int posValue) {
		this.posValue = posValue;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public boolean isReverseCharge() {
		return isReverseCharge;
	}

	public void setReverseCharge(boolean isReverseCharge) {
		this.isReverseCharge = isReverseCharge;
	}

	public String getUniqueRowValue() {
		return uniqueRowValue;
	}

	public void setUniqueRowValue(String uniqueRowValue) {
		this.uniqueRowValue = uniqueRowValue;
	}

	public boolean isNoValidationCheckForGinesys() {
		return noValidationCheckForGinesys;
	}

	public void setNoValidationCheckForGinesys(boolean noValidationCheckForGinesys) {
		this.noValidationCheckForGinesys = noValidationCheckForGinesys;
	}

	public boolean isSewopOrWopay() {
		return sewopOrWopay;
	}

	public void setSewopOrWopay(boolean sewopOrWopay) {
		this.sewopOrWopay = sewopOrWopay;
	}

	public String getGstinPosImps() {
		return gstinPosImps;
	}

	public void setGstinPosImps(String gstinPosImps) {
		this.gstinPosImps = gstinPosImps;
	}

	public String getPosImps() {
		return posImps;
	}

	public void setPosImps(String posImps) {
		this.posImps = posImps;
	}

	public String getNoteType() {
		return noteType;
	}

	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}

	public String getPreGstRegime() {
		return preGstRegime;
	}

	public void setPreGstRegime(String preGstRegime) {
		this.preGstRegime = preGstRegime;
	}
	
	
	

}
