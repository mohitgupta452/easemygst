package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Export {

	@JsonProperty("exp_det")
	private List<ExportDetails> exp_det;
	
}
