package com.ginni.easemygst.portal.data.transaction.error;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public@Data class B2CSError {
	
	@JsonProperty("state_cd")
	private Integer stateCode;
	
	
	@JsonProperty("error_cd")
	private String errorCode;
	
	@JsonProperty("error_msg")
	private String errorMsg;
}
