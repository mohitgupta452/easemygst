package com.ginni.easemygst.portal.data.deserialize;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonDeserializer;

public abstract class BaseDataDeserializer <T> extends JsonDeserializer<T> {
	
	protected  final Logger _LOGGER=LoggerFactory.getLogger(this.getClass()); 
	
	final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
}
