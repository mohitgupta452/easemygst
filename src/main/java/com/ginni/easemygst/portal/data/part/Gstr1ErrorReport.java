package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.transaction.error.B2BError;
import com.ginni.easemygst.portal.data.transaction.error.B2CLError;
import com.ginni.easemygst.portal.data.transaction.error.B2CSError;
import com.ginni.easemygst.portal.data.transaction.error.CDNError;

import lombok.Data;

public @Data class Gstr1ErrorReport {

	@JsonProperty("sec")
	private String section;
	
	@JsonProperty("b2b")
	private List<B2BError> b2b;
	
	@JsonProperty("b2ba")
	private List<B2BError> b2ba;
	
	@JsonProperty("b2cl")
	private List<B2CLError> b2cl;
	
	@JsonProperty("b2cla")
	private List<B2CLError> b2cla;
	
	@JsonProperty("b2cs")
	private List<B2CSError> b2cs;
	
	@JsonProperty("b2csa")
	private List<B2CSError> b2csa;
		
	@JsonProperty("cdn")
	private List<CDNError> cdn;
	
	@JsonProperty("cdna")
	private List<CDNError> cdna;
	
	@JsonProperty("NIL")
	private List<NIL> nil;
	
	@JsonProperty("exp")
	private List<EXP> exp;
	
	@JsonProperty("expa")
	private List<EXP> expa;
	
	@JsonProperty("at")
	private List<AT> at;
	
	@JsonProperty("ata")
	private List<AT> ata;
	
//	@JsonProperty("txp")
//	private List<TXP> txp;
	
	@JsonProperty("ecom")
	private List<ECOM> ecom;
	
	@JsonProperty("hsn")
	private List<HSNSUM> hsn;
	
	
}
