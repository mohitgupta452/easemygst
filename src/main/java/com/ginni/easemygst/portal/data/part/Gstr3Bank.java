package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Gstr3Bank {

	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
	private Double bankCgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
	private Double bankSgstAmount;	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	private Double bankIgstAmount;
	
}
