package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class ATDeserializer extends BaseDataDeserializer<List<ATTransactionEntity>> {

	@Override
	public List<ATTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<ATTransactionEntity> ats = new ArrayList<>();
		Iterator<JsonNode> atNodes = node.elements();
		while (atNodes.hasNext()) {
			JsonNode atNode = atNodes.next();
			ATTransactionEntity at = new ATTransactionEntity();
//			if (atNode.has("chksum"))
//				at.setCheckSum(atNode.get("chksum").asText());
			if (atNode.has("pos"))
				at.setPos(atNode.get("pos").asText());
			if (atNode.has("sply_ty")){
				String supplyType=atNode.get("sply_ty").asText();
				for(SupplyType st:SupplyType.values()){
					if(st.toString().equalsIgnoreCase(supplyType))
						at.setSupplyType(st);
				}
			}
			if(atNode.has("error_msg"))
				at.setErrorMsg(atNode.get("error_msg").asText());
			if(atNode.has("error_cd"))
				at.setErrorCd(atNode.get("error_cd").asText());
			
           if(atNode.has("itms"))
			at.setItems(this.deserializeItem(atNode));
			ats.add(at);
		}
		return ats;
	}

	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();

			if (itemNode.has("rt"))
				item.setTaxRate(itemNode.get("rt").asDouble());
//			if (itemNode.has("ad_amt"))
//				item.seta(itemNode.get("ad_amt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgst(itemNode.get("iamt").asDouble());
			if (itemNode.has("csamt"))
				item.setCess(itemNode.get("csamt").asDouble());
		
			itms.add(item);
		}
		return itms;
	}

}
