package com.ginni.easemygst.portal.data.transaction;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.BaseData;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ExportType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class EXP extends BaseData {
	
	
	@JsonProperty("expId")
	String id;
	
	@JsonProperty("ex_tp")
	private ExportType exportType;
	
	
	@JsonProperty("ctin")
	private String gstin;
	
	/*@JsonProperty("flag")
	public TaxpayerAction taxPayerAction;
*/
	@JsonProperty("chksum")
	public String checkSum;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;

	@JsonProperty("tax_amount")
	private Double taxAmount;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((exportType == null) ? 0 : exportType.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.exportType, ((EXP) obj).getExportType());               
	   return builder.isEquals();
	}

}
