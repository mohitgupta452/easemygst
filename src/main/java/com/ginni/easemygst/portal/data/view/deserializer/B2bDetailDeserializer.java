package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2bDetailDeserializer extends  BaseDataDetailDeserializer <List<B2BTransactionEntity>> {
	
	

	@Override
	public List<B2BTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<B2BTransactionEntity> b2bs = new ArrayList<>();
		Iterator<JsonNode> b2bNodes = node.elements();
		while (b2bNodes.hasNext()) {
			JsonNode b2bNode = b2bNodes.next();
			B2BTransactionEntity b2b = new B2BTransactionEntity();
			if (b2bNode.has("ctin"))
				b2b.setCtin(StringUtils.upperCase(b2bNode.get("ctin").asText()));
			if (b2bNode.has("ctin_name"))
				b2b.setCtinName(b2bNode.get("ctin_name").asText());
			List<B2BDetailEntity> b2bDetails=new ArrayList<>();
			b2bDetails.add(this.deserializeInvoice(b2bNode,b2b));
			b2b.setB2bDetails(b2bDetails);
			b2bs.add(b2b);
		}
		return b2bs;
	}

	public B2BDetailEntity deserializeInvoice(JsonNode invNode,B2BTransactionEntity b2bTran) {
		B2BDetailEntity inv = new B2BDetailEntity();
		inv.setB2bTransaction(b2bTran);
		if (invNode.has("inv_type"))
			inv.setInvoiceType(invNode.get("inv_type").asText());
		if (invNode.has("inum"))
			inv.setInvoiceNumber(invNode.get("inum").asText());

		String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
		if (!StringUtils.isEmpty(invDate)) {
			try {
				inv.setInvoiceDate(gstFmt.parse(invDate));
			} catch (ParseException e) {
				SimpleDateFormat sdf1=new SimpleDateFormat("dd MMM yyyy");
				try {
					inv.setInvoiceDate(sdf1.parse(invDate));
				} catch (ParseException e1) {
					e.printStackTrace();
				}
				e.printStackTrace();
			}

		}

		/*if (invNode.has("val"))
			inv.setTaxableValue(invNode.get("val").asDouble());*/
		if (invNode.has("pos"))
			inv.setPos(invNode.get("pos").asText());
		if (invNode.has("rchrg"))
			if (invNode.get("rchrg").asText().equals("Y"))
				inv.setReverseCharge(true);
			else
				inv.setReverseCharge(false);
		if (invNode.has("etin"))
			inv.setEtin(invNode.get("etin").asText());
		if (invNode.has("source"))
			inv.setSource(invNode.get("source").asText());

		if (invNode.has("invoice_id"))
			inv.setId(invNode.get("invoice_id").asText());
		/*
		 * invoice value, taxable value, taxamount
		 */

		if (invNode.has("originalInvoiceNumber"))
			inv.setOriginalInvoiceNumber(invNode.get("originalInvoiceNumber").asText());
		String orgInvDate = invNode.has("originalInvoiceDate") ? invNode.get("originalInvoiceDate").asText() : null;
		if (!StringUtils.isEmpty(orgInvDate)) {
			try {
				inv.setOriginalInvoiceDate(gstFmt.parse(orgInvDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(StringUtils.isNoneEmpty(inv.getOriginalInvoiceNumber())||Objects.nonNull(inv.getOriginalInvoiceDate()))
		inv.setIsAmmendment(true);
		inv.setItems(this.deserializeItem(invNode));

		return inv;
	}

	
}
