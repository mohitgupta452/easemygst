package com.ginni.easemygst.portal.data.transaction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.Audit;
import com.ginni.easemygst.portal.data.Comments;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;
//@JsonIgnoreProperties({"transit","locked","valid","amendment","gstnSynced","flags"})
public @Data class B2CS extends InvoiceItem {
	
	@JsonProperty("bId")
	String id;
	
	@JsonProperty("month_year")
	String monthYear;
	
	@JsonProperty("etin")
	private String ecomTin;//added after gov api 2.0 rlease
	
	@JsonProperty("flag")
	private TaxpayerAction taxpayerAction = TaxpayerAction.UPLOADED;
	
	private ReconsileType type;
	
	@JsonIgnore
	private boolean isTransit = false;
	
	@JsonIgnore
	private String transitId = "";

	@JsonProperty("chksum")
	private String checkSum;
    
	@JsonProperty("state_cd")
	private Integer stateCode;
	
	@JsonProperty("state_name")
	private String stateName;
	
	private boolean isAmendment = false;
	
	private boolean isValid = true;
	
	private Map<String, String> error = new HashMap<>();
	
	@JsonIgnore
	private boolean isGstnSynced = false;
	
	@JsonIgnore
	private boolean toBeSynced = false;
	
	@JsonIgnore
	private boolean isLocked = true;
	
	@JsonIgnore
	private String syncId;
	
	@JsonIgnore
	private String flags = "";
	
	@JsonIgnore
	private String source;
	
	@JsonIgnore
	private String sourceId;
	
	private List<Comments> comments;
	
	private List<Audit> audits;

	@JsonProperty("osupst_cd")
	private String originalStateCode;//no information regarding these fields
    
	@JsonFormat(pattern=Transaction.jsonDateFormat,timezone=Transaction.jsonDateTimeZone)
	@JsonProperty("omon")
	private Date originalMonth;
    
	@JsonProperty("ohsn_sc")
	private String originalGoodsOrServiceCode;
	
	@JsonProperty("sply_ty")
	public SupplyType supplyType;
	
	
	@JsonProperty("typ")
	private String typ;
	
	
	
}
