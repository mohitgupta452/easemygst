package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;

public class IMPSDetailDeserializer extends BaseDataDetailDeserializer<List<IMPSTransactionEntity>> {

	@Override
	public List<IMPSTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<IMPSTransactionEntity> impsTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> impsNodes = node.elements();
		while (impsNodes.hasNext()) {
			JsonNode impsNode = impsNodes.next();
			IMPSTransactionEntity impsTransactionEntity = new IMPSTransactionEntity();
			
			if (impsNode.has("inum"))
				impsTransactionEntity.setInvoiceNumber(impsNode.get("inum").asText());
			
			String invDate = impsNode.has("idt") ? impsNode.get("idt").asText() : null;
			if (!StringUtils.isEmpty(invDate)) {
				try {
					impsTransactionEntity.setInvoiceDate(gstFmt.parse(invDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if (impsNode.has("idt"))
				impsTransactionEntity.setPos(impsNode.get("pos").asText());
			if (impsNode.has("pos"))
				impsTransactionEntity.setPos(impsNode.get("pos").asText());
			
			String stateName=Objects.nonNull(AppConfig.getStateNameByCode(impsTransactionEntity.getPos()))?AppConfig.getStateNameByCode(impsTransactionEntity.getPos()).getName():"";
			impsTransactionEntity.setStateName(stateName);
			if (impsNode.has("taxable_val"))
				impsTransactionEntity.setTaxableValue(impsNode.get("taxable_val").asDouble());
			if (impsNode.has("tax_amt"))
				impsTransactionEntity.setTaxAmount(impsNode.get("tax_amt").asDouble());
			if (impsNode.has("flags"))
				impsTransactionEntity.setFlags(impsNode.get("flags").asText());
			if (impsNode.has("type"))
				impsTransactionEntity.setType(impsNode.get("type").asText());
			if (impsNode.has("source"))
				impsTransactionEntity.setSource(impsNode.get("source").asText());
			if (impsNode.has("invoice_id"))
				impsTransactionEntity.setId(impsNode.get("invoice_id").asText());
			
			if (impsNode.has("isSynced"))
				impsTransactionEntity.setIsSynced(impsNode.get("isSynced").asBoolean());
			if (impsNode.has("isTransit"))
				impsTransactionEntity.setIsTransit(impsNode.get("isTransit").asBoolean());
			if (impsNode.has("toBeSynch"))
				impsTransactionEntity.setToBeSync(impsNode.get("toBeSynch").asBoolean());
			if (impsNode.has("errorMsg"))
				impsTransactionEntity.setIsError(impsNode.get("isError").asBoolean());
			
			
			impsTransactionEntity.setItems(this.deserializeItem(impsNode));
			
			impsTransactionEntities.add(impsTransactionEntity);
		}
		return impsTransactionEntities;
	}

}
