package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.deserialize.BaseDataDeserializer;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.impl.B2CLTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2CSTransaction;

public class B2csDetailDeserializer extends BaseDataDeserializer<List<B2CSTransactionEntity>> {

	@Override
	public List<B2CSTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {
		double taxAmount=0.0;
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<B2CSTransactionEntity> b2css = new ArrayList<>();
		Iterator<JsonNode> b2bNodes = node.elements();
		while (b2bNodes.hasNext()) {
			JsonNode b2csNode = b2bNodes.next();
			B2CSTransactionEntity b2cs = new B2CSTransactionEntity();
			if (b2csNode.has("sply_ty"))
				b2cs.setSupplyType(SupplyType.valueOf(b2csNode.get("sply_ty").asText()));
			if (b2csNode.has("invoice_id"))
				b2cs.setId(b2csNode.get("invoice_id").asText());
			if (b2csNode.has("inum"))
				b2cs.setInvoiceNumber(b2csNode.get("inum").asText());
			

//			if (b2csNode.has("rt"))
//				b2cs.sett(b2csNode.get("rt").asDouble());
			if(b2csNode.has("b2cs_typ"))
				b2cs.setB2csType(b2csNode.get("b2cs_typ").asText());
			if(b2csNode.has("etin"))
				b2cs.setEtin(b2csNode.get("etin").asText());
			if (b2csNode.has("pos"))
				b2cs.setPos(b2csNode.get("pos").asText());
			//if(b2csNode.has("state_name")){
				String stateName=Objects.nonNull(AppConfig.getStateNameByCode(b2cs.getPos()))?AppConfig.getStateNameByCode(b2cs.getPos()).getName():"";
				b2cs.setStateName(stateName);
				//b2cs.setStateName(b2csNode.get("state_name").asText());
			//}
			if (b2csNode.has("source"))
				b2cs.setSource(b2csNode.get("source").asText());
			
			List<Item>items=new ArrayList<>();
				Item item=new Item();
				items.add(item);
				b2cs.setItems(items);
				
				if (b2csNode.has("num"))
					item.setSerialNumber(b2csNode.get("num").asInt());
				if (b2csNode.has("hsn_sc"))
					item.setHsnCode(b2csNode.get("hsn_sc").asText());
				if (b2csNode.has("hsn_desc"))
					item.setHsnDescription(b2csNode.get("hsn_desc").asText());
				if (b2csNode.has("unit"))
					item.setUnit(b2csNode.get("unit").asText());
				if (b2csNode.has("quantity"))
					item.setQuantity(b2csNode.get("quantity").asDouble());
				if (b2csNode.has("taxable_val"))
					item.setTaxableValue(b2csNode.get("taxable_val").asDouble());
				if (b2csNode.has("rt"))
					item.setTaxRate(b2csNode.get("rt").asDouble());
				if (b2csNode.has("iamt"))
					item.setIgst(b2csNode.get("iamt").asDouble());
				if (b2csNode.has("camt"))
					item.setCgst(b2csNode.get("camt").asDouble());
				if (b2csNode.has("samt"))
					item.setSgst(b2csNode.get("samt").asDouble());
				if (b2csNode.has("csamt"))
					item.setCess(b2csNode.get("csamt").asDouble());
				
				if (b2csNode.has("invoice_id"))
					item.setInvoiceId(b2csNode.get("invoice_id").asText());

				taxAmount=item.getIgst()+item.getCgst()+item.getSgst()+item.getCess();
				item.setTaxAmount(taxAmount);
//			while(invoices.hasNext()){
//				JsonNode invNode = invoices.next();
//				
//				b2cs.setItems(invNode.get("itms"));
//			}
				
				if (b2csNode.has("originalMonth"))
					b2cs.setOriginalMonth(b2csNode.get("originalMonth").asText());
				if (b2csNode.has("originalPos"))
					b2cs.setOriginalPos(b2csNode.get("originalPos").asText());
			b2cs.setTaxableValue(item.getTaxableValue());
			b2cs.setTaxAmount(taxAmount);
			
			if(StringUtils.isNoneEmpty(b2cs.getOriginalMonth())||StringUtils.isNoneEmpty(b2cs.getOriginalPos()))
				b2cs.setIsAmmendment(true);

			b2css.add(b2cs);

		}
		return b2css;

	}

}
