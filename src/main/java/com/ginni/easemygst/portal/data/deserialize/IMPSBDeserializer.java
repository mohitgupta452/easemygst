package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;
import com.ginni.easemygst.portal.transaction.impl.IMPGTransaction.SEZ;

public class IMPSBDeserializer extends BaseDataDeserializer<List<IMPSTransactionEntity>> {
	
	DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");

	@Override
	public List<IMPSTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<IMPSTransactionEntity> imps = new ArrayList<>();
		Iterator<JsonNode> impsNodes = node.elements();
		while (impsNodes.hasNext()) {
			JsonNode impgNode = impsNodes.next();
			IMPSTransactionEntity impg = new IMPSTransactionEntity();
			if(impgNode.has("error_msg"))
				impg.setErrorMsg(impgNode.get("error_msg").asText());
			if(impgNode.has("error_cd"))
				impg.setErrorCd(impgNode.get("error_cd").asText());
			
				
			
			if (impgNode.has("inum"))
				impg.setInvoiceNumber(impgNode.get("inum").asText());
			if (impgNode.has("idt"))
				try {
					impg.setInvoiceDate(dateFormat.parse(impgNode.get("idt").asText()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (impgNode.has("ival"))
				impg.setInvoiceValue(impgNode.get("ival").asDouble());
			if (impgNode.has("pos"))
				impg.setPos(impgNode.get("pos").asText());

//			if (impgNode.has("chksum"))
//				impg.setCheckSum(impgNode.get("chksum").asText());

            if(impgNode.has("itms"))
			 impg.setItems(this.deserializeItem(impgNode));
			imps.add(impg);
		}

		return imps;

	}

	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();

			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			if (itemNode.has("txval"))
				item.setTaxableValue(itemNode.get("txval").asDouble());
			if (itemNode.has("rt"))
				item.setTaxRate(itemNode.get("rt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgst(itemNode.get("iamt").asDouble());
			if (itemNode.has("csamt"))
				item.setCess(itemNode.get("csamt").asDouble());
			if (itemNode.has("elg")) {
				item.setTotalEligibleTax(itemNode.get("elg").asText());
			if(itemNode.has("tx_i"))	
				item.setItcIgst(itemNode.get("tx_i").asDouble());
			if(itemNode.has("tx_cs"))
				item.setItcCess(itemNode.get("tx_cs").asDouble());
				//item.setItcDetails(this.deserializeItcDetail(itemNode, item));
			}

			itms.add(item);
		}
		return itms;
	}

	public ItcDetail deserializeItcDetail(JsonNode itemNode, Item invItem) {
		JsonNode itcNode = itemNode;
		ItcDetail itcDetail = new ItcDetail();
		/*
		 * if (itemNode.has("elg"))
		 * invItem.setEligOfTotalTax(itcNode.get("elg").asText());
		 */
		if (!Objects.isNull(itcNode)) {
			if (itemNode.has("tx_i"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_i").asDouble());
			if (itemNode.has("tx_cs"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_cs").asDouble());
		}
		return itcDetail;
	}

}
