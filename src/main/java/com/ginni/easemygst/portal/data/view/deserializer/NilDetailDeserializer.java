package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;

public class NilDetailDeserializer extends BaseDataDetailDeserializer <List<NILTransactionEntity>>{

	@Override
	public List<NILTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<NILTransactionEntity> nilTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> nilTransactionNodes = node.elements();
		while (nilTransactionNodes.hasNext()) {
			JsonNode nilTransactionNode = nilTransactionNodes.next();
			NILTransactionEntity nilTransactionEntity = new NILTransactionEntity();
			
			if(nilTransactionNode.has("sply_ty"))
				nilTransactionEntity.setSupplyType(NilSupplyType.valueOf(nilTransactionNode.get("sply_ty").asText()));
			
			if(nilTransactionNode.has("tot_nil_amt"))
				nilTransactionEntity.setTotNilAmt(nilTransactionNode.get("tot_nil_amt").asDouble());
			
			if(nilTransactionNode.has("tot_exmptd_amt"))
				nilTransactionEntity.setTotExptAmt(nilTransactionNode.get("tot_exmptd_amt").asDouble());
			
			if(nilTransactionNode.has("tot_exmptd_amt"))
				nilTransactionEntity.setTotExptAmt(nilTransactionNode.get("tot_exmptd_amt").asDouble());
			
			if(nilTransactionNode.has("tot_ngsp_amt"))
				nilTransactionEntity.setTotNgsupAmt(nilTransactionNode.get("tot_ngsp_amt").asDouble());
			
			if(nilTransactionNode.has("tot_comp_amt"))
				nilTransactionEntity.setTotCompAmt(nilTransactionNode.get("tot_comp_amt").asDouble());
			
			/*if(nilTransactionNode.has("invoice_id"))
				nilTransactionEntity.setId(nilTransactionNode.get("invoice_id").asText());*/
			
			if(nilTransactionNode.has("source"))
				nilTransactionEntity.setSource(nilTransactionNode.get("source").asText());
			
			nilTransactionEntities.add(nilTransactionEntity);
		}
		return nilTransactionEntities;
	}
}
