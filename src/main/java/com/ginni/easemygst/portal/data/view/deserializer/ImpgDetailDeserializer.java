package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.transaction.impl.IMPGTransaction.SEZ;

public class ImpgDetailDeserializer extends BaseDataDetailDeserializer<List<IMPGTransactionEntity>> {

	@Override
	public List<IMPGTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<IMPGTransactionEntity> impgTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> impgNodes = node.elements();
		while (impgNodes.hasNext()) {
			JsonNode impgNode = impgNodes.next();
			IMPGTransactionEntity impgTransactionEntity = new IMPGTransactionEntity();
			
			if (impgNode.has("ctin"))
				impgTransactionEntity.setCtin(impgNode.get("ctin").asText());
			if (impgNode.has("boe_num"))
				impgTransactionEntity.setBillOfEntryNumber(impgNode.get("boe_num").asText());
			String billOfEntryDate = impgNode.has("boe_dt") ? impgNode.get("boe_dt").asText() : null;
			if (!StringUtils.isEmpty(billOfEntryDate)) {
				try {
					impgTransactionEntity.setBillOfEntryDate(gstFmt.parse(billOfEntryDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if (impgNode.has("port_code"))
				impgTransactionEntity.setPortCode(impgNode.get("port_code").asText());
			if (impgNode.has("taxable_val"))
				impgTransactionEntity.setTaxableValue(impgNode.get("taxable_val").asDouble());
			if (impgNode.has("tax_amt"))
				impgTransactionEntity.setTaxAmount(impgNode.get("tax_amt").asDouble());
			if (impgNode.has("flags"))
				impgTransactionEntity.setFlags(impgNode.get("flags").asText());
			if (impgNode.has("type"))
				impgTransactionEntity.setType(impgNode.get("type").asText());
			if (impgNode.has("source"))
				impgTransactionEntity.setSource(impgNode.get("source").asText());
			if (impgNode.has("invoice_id"))
				impgTransactionEntity.setId(impgNode.get("invoice_id").asText());
			/*if (impgNode.has("sez"))*/
				impgTransactionEntity.setSez(SEZ.valueOf(impgNode.get("sez").asText()));
			impgTransactionEntity.setItems(this.deserializeItem(impgNode));
			impgTransactionEntities.add(impgTransactionEntity);
		}
		return impgTransactionEntities;
	}

}
