package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class EXPDetailSerializer extends BaseDataDetailSerializer<ExpDetail> {

	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");

	@Override
	public void serialize(ExpDetail b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		// json.writeStartArray();
		// for (B2BDetailEntity b2b : b2bs) {

		json.writeStartObject();
		//Invoice invoice = b2b.getInvoiceData();
		json.writeStringField("exp_type", b2b.getExportType());
		json.writeStringField("inum", b2b.getInvoiceNumber());
		if (!Objects.isNull(b2b.getInvoiceDate()))
			json.writeStringField("idt", gstFmt.format(b2b.getInvoiceDate()));
		else
			json.writeStringField("idt", "");

		json.writeStringField("sbpcode", b2b.getShippingBillPortCode());// --
		json.writeStringField("sbnum", b2b.getShippingBillNo());
		if (!Objects.isNull(b2b.getShippingBillDate()))
			json.writeStringField("sbdt", gstFmt.format(b2b.getShippingBillDate()));
		if(!Objects.isNull(b2b.getTaxableValue())){
		json.writeNumberField("taxable_val", b2b.getTaxableValue());
		}
		else
			json.writeNumberField("taxable_val", 0.0);

		if(!Objects.isNull(b2b.getTaxAmount())){

		json.writeNumberField("tax_amt", b2b.getTaxAmount());
		}else
			json.writeNumberField("tax_amt", 0.0);

		json.writeStringField("flags", b2b.getFlags());
		json.writeStringField("type", b2b.getType());
		json.writeStringField("source", b2b.getSource());
		json.writeStringField("invoice_id",b2b.getId() );
		
		json.writeBooleanField("isSynced",b2b.getIsSynced() );
		json.writeBooleanField("isTransit",b2b.getIsTransit() );
		json.writeBooleanField("toBeSynch",b2b.getToBeSync() );
		json.writeBooleanField("isError",b2b.getIsError() );
		json.writeStringField("errorMsg",b2b.getErrMsg() );
		json.writeNumberField("inv_val",b2b.getTaxAmount()+b2b.getTaxableValue() );
		
		//ammendment
		
		json.writeBooleanField("isAmmendment",b2b.getIsAmmendment() );
		json.writeStringField("originalInvoiceNumber",b2b.getOriginalInvoiceNumber() );
		if(Objects.isNull(b2b.getOriginalInvoiceDate())){
			json.writeStringField("originalInvoiceDate", "");

		}else
		json.writeStringField("originalInvoiceDate", gstFmt.format(b2b.getOriginalInvoiceDate()));


		writeLineItems(json, b2b);
		json.writeEndObject();
		// }
		// json.writeEndArray();

	}

	void writeLineItems(JsonGenerator json, ExpDetail inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

/*	public void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			json.writeNumberField("num", item.getSerialNumber());

			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("txval", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			// json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

}
