package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

public class CDNDetailSerializer extends BaseDataDetailSerializer<CDNDetailEntity> {

	private String returnType = "gstr1";
	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");


	@Override
	public void serialize(CDNDetailEntity cdn, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			if (cdn.getItems() != null && !cdn.getItems().isEmpty()
					&&cdn.getItems().get(0).getTotalEligibleTax() != null) {
				returnType = "gstr2";
			}
			json.writeStartObject();
			json.writeStringField("org_ctin", cdn.getOriginalCtin());
			json.writeStringField("org_ctin_name", StringUtils.isNotEmpty(cdn.getOriginalCtinName())?cdn.getOriginalCtinName():"NO NAME");
			json.writeStringField("ntty",cdn.getNoteType() );
			if(!Objects.isNull(cdn.getRevisedInvDate()))
			json.writeStringField("nt_dt", gstFmt.format(cdn.getRevisedInvDate()));
			json.writeStringField("inum", cdn.getInvoiceNumber());
			if(!Objects.isNull(cdn.getInvoiceDate()))
			json.writeStringField("idt", gstFmt.format(cdn.getInvoiceDate()));
			json.writeNumberField("taxable_val",cdn.getTaxableValue() );
			json.writeNumberField("tax_amt",cdn.getTaxAmount() );
			json.writeStringField("nt_num",cdn.getRevisedInvNo());
			json.writeStringField("flags",cdn.getFlags());
			json.writeStringField("type",cdn.getType() );
			json.writeStringField("source",cdn.getSource() );
			json.writeStringField("pre_gst",!StringUtils.isEmpty(cdn.getPreGstRegime())?cdn.getPreGstRegime():"" );
			json.writeStringField("rsn",!StringUtils.isEmpty(cdn.getReasonForNote())?cdn.getReasonForNote():"" );
			json.writeStringField("invoice_id",cdn.getId() );
			json.writeBooleanField("isMarked",cdn.getIsMarked() );
			json.writeNumberField("ctax_amount", !Objects.isNull(cdn.getcTaxAmount())?cdn.getcTaxAmount():0.0);
			json.writeStringField("cinv_num", !Objects.isNull(cdn.getcInvoiceNumber())?cdn.getcInvoiceNumber():"");
			json.writeNumberField("inv_val",cdn.getTaxAmount()+cdn.getTaxableValue() );

			json.writeBooleanField("isSynced",cdn.getIsSynced() );
			json.writeBooleanField("isTransit",cdn.getIsTransit() );
			json.writeBooleanField("toBeSynch",cdn.getToBeSync() );
			json.writeStringField("cfs",cdn.getCdnTransaction().getFillingStatus() );
			if(cdn.getType().contains("MISMATCH")||ReconsileType.MISSING_INWARD.toString().equalsIgnoreCase(cdn.getType())){
			if(ReturnType.GSTR2 == ReturnType
					.valueOf(StringUtils.upperCase(cdn.getCdnTransaction().getReturnType()))
			&& "N".equalsIgnoreCase(cdn.getCdnTransaction().getFillingStatus())){
				json.writeStringField("flags","DRAFT");
			}
			}
			json.writeBooleanField("isError",cdn.getIsError() );
			json.writeStringField("errorMsg",cdn.getErrMsg() );
			json.writeNumberField("ctaxable_val",cdn.getcTaxableValue() );

			if (Objects.nonNull(cdn.getCdnTransaction()) && ReturnType.GSTR1 == ReturnType
					.valueOf(StringUtils.upperCase(cdn.getCdnTransaction().getReturnType()))) {
				if("ERROR".equalsIgnoreCase(cdn.getFlags())||cdn.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(cdn.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(cdn.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}
			
		if(Objects.isNull(cdn.getcInvoiceDate())){
			json.writeStringField("cidt", "");

		}else
		json.writeStringField("cidt", gstFmt.format(cdn.getcInvoiceDate()));
		json.writeBooleanField("isRevertible",this.checkIsRevertible(cdn) );

			
			//if (!StringUtils.isEmpty(cdn.getPos()))
			//	json.writeStringField("pos", cdn.getPos());
			/*if(!Objects.isNull(invoice.getSupplierInvVal())) {
				json.writeNumberField("val", invoice.getSupplierInvVal());
			}*/
			

		//ammendment
		
		json.writeBooleanField("isAmmendment",cdn.getIsAmmendment() );
		json.writeStringField("originalNoteNumber",cdn.getOriginalNoteNumber() );
		if(Objects.isNull(cdn.getOriginalNoteDate())){
			json.writeStringField("originalNoteDate", "");

		}else
		json.writeStringField("originalNoteDate", gstFmt.format(cdn.getOriginalNoteDate()));		
		
			writeLineItems(json, cdn);
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	

	
	void writeLineItems( JsonGenerator json, CDNDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}
	
	
	boolean checkIsRevertible(CDNDetailEntity b2b){
		if(b2b.getIsSynced())
			return false;
		if (Objects.nonNull(b2b.getPreviousType()) && ("PENDING".equalsIgnoreCase(b2b.getFlags())
				|| "REJECTED".equalsIgnoreCase(b2b.getFlags()) || "ACCEPTED".equalsIgnoreCase(b2b.getFlags()))) {
			if(b2b.getPreviousType().contains("MISMATCH")|| b2b.getPreviousType()
					.contains("MISSING")){
				return true;
			}
		}
		
		return false;
	}

	/*private void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			json.writeNumberField("num", item.getSerialNumber());

			
			if (!StringUtils.isEmpty(item.getHsnCode()))
				json.writeStringField("hsn_sc", item.getHsnCode());

			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("txval", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			if (!Objects.isNull(item.getCgst()))
				json.writeNumberField("camt", item.getCgst());

			if (!Objects.isNull(item.getSgst()))
				json.writeNumberField("samt", item.getSgst());

			if (!Objects.isNull(item.getCess()))
				json.writeNumberField("csamt", item.getCess());

			if ("gstr2".equalsIgnoreCase(returnType)) {
				if (!StringUtils.isEmpty(item.getTotalEligibleTax())) {
					json.writeStringField("elg",
							item.getTotalEligibleTax() != null ? String.valueOf(item.getTotalEligibleTax()) : null);

					// writing itc detail
					json.writeFieldName("itc");// adding itc detail field
					this.writeItcDetail(json, item);
				}
			}

			//json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeItcDetail(JsonGenerator json, Item itc) {
		try {
			// start itcdetail
			json.writeStartObject();
			
			if (!Objects.isNull(itc.getTotalTaxAvalIgst()))
				json.writeStringField("tx_i",
					itc.getTotalTaxAvalIgst() != null ? String.valueOf(itc.getTotalTaxAvalIgst()) : null);
			
			if (!Objects.isNull(itc.getTotalTaxAvalCgst()))
				json.writeStringField("tx_c",
					itc.getTotalTaxAvalCgst() != null ? String.valueOf(itc.getTotalTaxAvalCgst()) : null);
			
			if (!Objects.isNull(itc.getTotalTaxAvalSgst()))
				json.writeStringField("tx_s",
					itc.getTotalTaxAvalSgst() != null ? String.valueOf(itc.getTotalTaxAvalSgst()) : null);
			
			if (!Objects.isNull(itc.getTotalTaxAvalCess()))
				json.writeStringField("tx_cs",
					itc.getTotalTaxAvalCess() != null ? String.valueOf(itc.getTotalTaxAvalCess()) : null);
			
			json.writeEndObject();
			// end itc detail
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

}
