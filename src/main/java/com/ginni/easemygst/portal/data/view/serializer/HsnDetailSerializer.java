package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class HsnDetailSerializer extends BaseDataDetailSerializer<Hsn> {

	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");


	@Override
	public void serialize(Hsn b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			
			json.writeStartObject();
			
			json.writeStringField("hsn_sc", !StringUtils.isEmpty(b2b.getCode())?b2b.getCode():"");
			
			json.writeStringField("hsn_desc", !StringUtils.isEmpty(b2b.getDescription())?b2b.getDescription():"");
			
			json.writeStringField("unit", !StringUtils.isEmpty(b2b.getUnit())?b2b.getUnit():"");
			
			json.writeNumberField("quantity", !Objects.isNull(b2b.getQuantity())?b2b.getQuantity():0.0);
			
			json.writeNumberField("tot_value", !Objects.isNull(b2b.getValue())?b2b.getValue():0.0);
			
			json.writeNumberField("taxable_val", !Objects.isNull(b2b.getTaxableValue())?b2b.getTaxableValue():0.0);
			
			json.writeNumberField("tax_amt", !Objects.isNull(b2b.getTaxAmount())?b2b.getTaxAmount():0.0);
			
			json.writeNumberField("iamt", !Objects.isNull(b2b.getIgst())?b2b.getIgst():0.0);
			
			json.writeNumberField("camt", !Objects.isNull(b2b.getCgst())?b2b.getCgst():0.0);
			
			json.writeNumberField("samt", !Objects.isNull(b2b.getSgst())?b2b.getSgst():0.0);
			
			json.writeNumberField("csamt", !Objects.isNull(b2b.getCess())?b2b.getCess():0.0);

			/*if (Objects.nonNull(b2b) ) {
				if("ERROR".equalsIgnoreCase(b2b.getFlags())||b2b.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(b2b.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(b2b.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}*/
			

			json.writeStringField("source",b2b.getSource());
			
			json.writeStringField("invoice_id",b2b.getId() );

			json.writeBooleanField("isSynced",b2b.getIsSynced() );
			json.writeBooleanField("isTransit",b2b.getIsTransit() );
			json.writeBooleanField("toBeSynch",b2b.getToBeSync() );
			json.writeBooleanField("isError",!Objects.isNull(b2b.getIsError())?b2b.getIsError():false );
			json.writeStringField("errorMsg",b2b.getErrMsg() );


			
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	

	
	
	
}
