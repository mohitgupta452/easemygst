package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.gst.response.FileUrlResp;

public class URLDeserialize extends BaseDataDeserializer<List<FileUrlResp>> {

	@Override
	public List<FileUrlResp> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<FileUrlResp> urls = new ArrayList<>();
		Iterator<JsonNode> atNodes = node.elements();
		while (atNodes.hasNext()) {
			JsonNode atNode = atNodes.next();
			FileUrlResp at = new FileUrlResp();
			if (atNode.has("hash"))
				at.setHash(atNode.get("hash").asText());
			if (atNode.has("ic"))
				at.setIc(atNode.get("ic").asText());
			if(atNode.has("ul"))
				at.setUl(atNode.get("ul").asText());
			urls.add(at);
		}
		return urls;
	}

}
