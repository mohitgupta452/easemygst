package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class RevisionInvoiceOUTS {

	@JsonProperty("state_cd")
	private String stateCode;
	
	@JsonProperty("dt")
	private List<RevInvoiceOUTDetails> dt;
		
}
