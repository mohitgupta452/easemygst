package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.mapping.Array;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class B2CLSerializer extends BaseDataSerializer<List<B2CLDetailEntity>> {
	private ReturnType returnType = ReturnType.GSTR1;

	private TransactionType transType = TransactionType.B2CL;

	@Override
	public void serialize(List<B2CLDetailEntity> b2cls, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		//b2cls=this.groupByDetailEntity(b2cls);
		Map<String, List<B2CLDetailEntity>> b2clDetails = b2cls.stream()
				.collect(Collectors.groupingBy(B2CLDetailEntity::getPos));
		// for (B2CLDetailEntity cdn : cdns) {
		for (Entry<String, List<B2CLDetailEntity>> entry : b2clDetails.entrySet()) {
			json.writeStartObject();
			if (!StringUtils.isEmpty(entry.getKey())) {
				json.writeStringField("pos",
						(Integer.parseInt(entry.getKey()) < 10 && entry.getKey().length() == 1) ? "0" + entry.getKey()
								: entry.getKey());
			}

			writeInvoices(entry.getValue(), json);

			json.writeEndObject();
		}
		json.writeEndArray();

	}

	void writeInvoices(List<B2CLDetailEntity> b2cl, JsonGenerator json) throws IOException {
		// Start Invoice Details
		if (!StringUtils.isEmpty(b2cl.get(0).getError_msg()))
			json.writeStringField("error_msg", b2cl.get(0).getError_msg());
		if (!StringUtils.isEmpty(b2cl.get(0).getError_cd()))
			json.writeStringField("error_cd", b2cl.get(0).getError_cd());

		json.writeFieldName("inv");
		json.writeStartArray();
		b2cl.forEach(inv -> writeInvoice(json, inv));
		json.writeEndArray();
		// end Invoice Details
	}

	private void writeInvoice(JsonGenerator json, B2CLDetailEntity inv) {
		try {

			json.writeStartObject();
			// if (!StringUtils.isEmpty(inv.getInvoiceData().getCheckSum()))
			// json.writeStringField("chksum", inv.getInvoiceData().getCheckSum());

			if (inv.getInvoiceNumber() != null)
				json.writeStringField("inum", inv.getInvoiceNumber());
			if (inv.getInvoiceDate() != null)
				json.writeStringField("idt", gstFmt.format(inv.getInvoiceDate()));

			if (!Objects.isNull(inv.getFlags()) && !StringUtils.isEmpty(inv.getFlags()))
				json.writeStringField("flag", inv.getFlags() != null ? this.matchFlag(inv.getFlags()) : null);

			else {

				if (inv.getIsAmmendment()) {

					json.writeStringField("oinum", inv.getOriginalInvoiceNumber());
					json.writeStringField("oidt", gstFmt.format(inv.getOriginalInvoiceDate()));
				}

				if (!StringUtils.isEmpty(inv.getEtin()))
					json.writeStringField("etin", String.valueOf(inv.getEtin()));
				if (inv.getItems() != null) {
					
						BigDecimal taxableValue=new ItemSerializer().writeLineItems(json, inv.getItems(), returnType, transType);
					
						BigDecimal val = BigDecimal.ZERO;
						val = val
								.add(taxableValue.add(BigDecimal.valueOf(inv.getTaxAmount())));
						json.writeNumberField("val", val.setScale(2, RoundingMode.FLOOR));
				}
			}

			// writeLineItems(json, inv);

			json.writeEndObject();

		} catch (Exception e) {
			// log
			e.printStackTrace();
		}
	}

	void writeLineItems(JsonGenerator json, B2CLDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	private void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item
			if (item.getSerialNumber() != 0)
				json.writeNumberField("num", item.getSerialNumber());
			json.writeFieldName("itm_det");// adding item detail field
			json.writeStartObject();// start item detail

			if (item.getTaxableValue() != 0)
				json.writeNumberField("txval", item.getTaxableValue());
			if (item.getTaxRate() != 0)
				json.writeNumberField("rt", item.getTaxRate());
			if (item.getIgst() != 0)
				json.writeNumberField("iamt", item.getIgst());
			if (item.getCess() != 0)
				json.writeNumberField("csamt", item.getCess());
			json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	
	private List<B2CLDetailEntity> groupByDetailEntity(List<B2CLDetailEntity> datas){
		List<B2CLDetailEntity> result=new  ArrayList<>();
		Map<String, List<B2CLDetailEntity>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(B2CLDetailEntity::getId));
		for(Map.Entry<String, List<B2CLDetailEntity>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			B2CLDetailEntity b2bDetailEntity=null;
			for(B2CLDetailEntity b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

}
