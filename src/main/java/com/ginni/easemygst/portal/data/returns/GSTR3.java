package com.ginni.easemygst.portal.data.returns;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.dto.TaxPaidDto;
import com.ginni.easemygst.portal.data.part.INS;
import com.ginni.easemygst.portal.data.part.ITCRCV;
import com.ginni.easemygst.portal.data.part.OUTS;
import com.ginni.easemygst.portal.data.part.RFCLM;
import com.ginni.easemygst.portal.data.part.TDSRCV;
import com.ginni.easemygst.portal.data.part.TOD;
import com.ginni.easemygst.portal.data.part.TTL;

public @lombok.Data class GSTR3 {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("name")
	private String namd;
	
	@JsonProperty("add")
	private String address;
	
	@JsonFormat(pattern="MMyyyy")
	@JsonProperty("fp_fm")
	private Date monthYear;
	
	@JsonProperty("fp_fy")
	private Date financialYear;
	
	@JsonProperty("tod")
	private TOD turnover;
	
	@JsonProperty("out_s")
	private List<OUTS> outss;
	
	@JsonProperty("in_s")
	private List<INS> inss;
	
	@JsonProperty("ttl")
	private List<TTL> ttls;
	
	@JsonProperty("tds_rcd")
	private List<TDSRCV> tdsrcv;
	
	@JsonProperty("itc_rcd")
	private List<ITCRCV> itcrcv;
	
	@JsonProperty("pay_det")
	private List<TaxPaidDto> taxpds;
	
	@JsonProperty("rf_clm")
	private List<RFCLM> refundClaims;
	
}
