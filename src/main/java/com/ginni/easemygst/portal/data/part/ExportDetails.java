package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ExportDetails {
	
	@JsonProperty("ty")
	private String ty;
	
	@JsonProperty("txval")
	private Double txval=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("iamt")
	private Double iamt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("camt")
	private Double camt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("samt")
	private Double samt=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ex_tp")
	private Double ex_tp=0.0;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("cess")
	private Double cess=0.0;

	
}
