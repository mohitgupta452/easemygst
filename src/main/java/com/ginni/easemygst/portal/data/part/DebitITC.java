package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class DebitITC {

	@JsonProperty("tr")
	private List<ITCDebitTransaction> listOfDebitTransaction;
	
	@JsonProperty("tot_cr")
	private LedgerItcTotalCredit totalCredit;
	
	
}
