package com.ginni.easemygst.portal.data;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the items_hsn database table.
 * 
 */
@Entity
@Table(name="items_hsn")
@NamedQuery(name="ItemsHsn.findAll", query="SELECT i FROM ItemsHsn i")
public class ItemsHsn implements Serializable {
	private static final long serialVersionUID = 1L;

	private double cess;

	private double cgst;
	@Id
	private String hsnCode;

	private String hsnDescription;

	private double igst;

	private String monthYear;

	private double quantity;

	private String returnType;

	private double sgst;

	private double taxableValue;

	private String taxpayerGstinId;

	private double taxRate;

	private String unit;

	public ItemsHsn() {
	}

	public double getCess() {
		return this.cess;
	}

	public void setCess(double cess) {
		this.cess = cess;
	}

	public double getCgst() {
		return this.cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public String getHsnCode() {
		return this.hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getHsnDescription() {
		return this.hsnDescription;
	}

	public void setHsnDescription(String hsnDescription) {
		this.hsnDescription = hsnDescription;
	}

	public double getIgst() {
		return this.igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public String getMonthYear() {
		return this.monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public double getSgst() {
		return this.sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getTaxableValue() {
		return this.taxableValue;
	}

	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}

	public String getTaxpayerGstinId() {
		return this.taxpayerGstinId;
	}

	public void setTaxpayerGstinId(String taxpayerGstinId) {
		this.taxpayerGstinId = taxpayerGstinId;
	}

	public double getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}