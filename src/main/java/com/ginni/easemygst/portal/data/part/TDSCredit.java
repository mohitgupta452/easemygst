package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TDSCredit {

	@JsonProperty("tds_det")
	private List<CreditDetails> tds_det;
	
}
