package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;

public class ATDetailDeserializer extends BaseDataDetailDeserializer<List<ATTransactionEntity>> {

	@Override
	public List<ATTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<ATTransactionEntity> atTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> atNodes = node.elements();
		while (atNodes.hasNext()) {
			JsonNode atNode = atNodes.next();
			ATTransactionEntity atTransactionEntity = new ATTransactionEntity();
			if (atNode.has("sply_ty"))
				atTransactionEntity.setSupplyType(SupplyType.valueOf(atNode.get("sply_ty").asText()));
			if (atNode.has("pos"))
				atTransactionEntity.setPos(atNode.get("pos").asText());
			if (atNode.has("state_name"))
				atTransactionEntity.setStateName(atNode.get("state_name").asText());
			if (atNode.has("advance_recvd"))
				atTransactionEntity.setAdvanceReceived(atNode.get("advance_recvd").asDouble());
			if (atNode.has("tax_amt"))
				atTransactionEntity.setTaxAmount(atNode.get("tax_amt").asDouble());
			if (atNode.has("flags"))
				atTransactionEntity.setFlags(atNode.get("flags").asText());
			if (atNode.has("type"))
				atTransactionEntity.setType(atNode.get("type").asText());
			if (atNode.has("source"))
				atTransactionEntity.setSource(atNode.get("source").asText());
			if (atNode.has("invoice_id"))
				atTransactionEntity.setId(atNode.get("invoice_id").asText());
			
			if (atNode.has("originalMonth"))
				atTransactionEntity.setOriginalMonth(atNode.get("originalMonth").asText());
			/*if (atNode.has("originalPos"))
				atTransactionEntity.setOriginalPos(atNode.get("originalPos").asText());
			if (atNode.has("originalSupplyType"))
				atTransactionEntity.setOriginalSupplyType(atNode.get("originalSupplyType").asText());*/
			
			if (StringUtils.isNoneEmpty(atTransactionEntity.getOriginalMonth())
					/*|| StringUtils.isNoneEmpty(atTransactionEntity.getOriginalPos())
					|| StringUtils.isNoneEmpty(atTransactionEntity.getOriginalSupplyType())*/)
				atTransactionEntity.setIsAmmendment(true);
			
			atTransactionEntity.setItems(this.deserializeItem(atNode));
			
			atTransactionEntities.add(atTransactionEntity);
		}
		return atTransactionEntities;
	}

}
