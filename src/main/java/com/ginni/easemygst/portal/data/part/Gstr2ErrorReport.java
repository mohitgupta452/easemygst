package com.ginni.easemygst.portal.data.part;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.data.transaction.ISD;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.transaction.REVCH;
import com.ginni.easemygst.portal.data.transaction.TDSTCS;

import lombok.Data;

public @Data class Gstr2ErrorReport {

	@JsonProperty("B2B")
	private List<B2B> b2b;
	
	@JsonProperty("B2BA")
	private List<B2B> b2ba;
	
	@JsonProperty("IMPG")
	private List<IMPG> impg;
	
	@JsonProperty("IMPGA")
	private List<IMPG> impga;
	
	@JsonProperty("IMPS")
	private List<REVCH> imps;
	
	@JsonProperty("IMPSA")
	private List<REVCH> impsa;
	
	@JsonProperty("CDN")
	private List<CDN> cdn;
	
	@JsonProperty("CDNA")
	private List<CDN> cdna;
	
	@JsonProperty("NIL")
	private List<NIL> nil;
	
	@JsonProperty("ISD")
	private List<ISD> isd;
	
	@JsonProperty("TDS")
	private List<TDSTCS> tds;
	
//	@JsonProperty("TCS")
//	private List<TCS> tcs;
	
	@JsonProperty("ITC")
	private List<ITC> itc;
	
	@JsonProperty("AT")
	private List<AT> at;
	
	@JsonProperty("ATA")
	private List<AT> ata;
	
//	@JsonProperty("TXPD")
//	private List<TXPD> txpd;
	
//	@JsonProperty("ITCRVSL")
//	private List<ITCRVSL> itcrvsl;
	
	@JsonProperty("HSNSUM")
	private List<HSNSUM> hsnsum;
	
	
}
