package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class INS {

	@JsonProperty("inter")
	private IntrSupRec interStateSuppReceived;
	
	@JsonProperty("intra")
	private ItraSupRec intraStateSuppReceived;
	
	@JsonProperty("imp")
	private Imp imports;
	
	@JsonProperty("rev_inv")
	private RevisionInvoiceINS revision;
	
	@JsonProperty("tx_liab")
	private TaxLiabilityINS totalTaxLiability;
	
	@JsonProperty("itc_rev")
	private ITCRDet itcReversal;
	
	@JsonProperty("jb_wrk")
	private JobWork jb_wrk;

	
}
