package com.ginni.easemygst.portal.data.transaction.error;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public@Data class B2CLError {
	
	@JsonProperty("state_cd")
	private Integer stateCode;
	
	
	@JsonProperty("inv_num")
	private String invoiceNo;
	
	
	@JsonProperty("date")
	private Date date;
	
	@JsonProperty("error_cd")
	private String errorCode;
	
	@JsonProperty("error_msg")
	private String errorMsg;

}
