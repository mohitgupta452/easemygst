package com.ginni.easemygst.portal.data.part;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;

import lombok.Data;

public @Data class ECOM {



	@JsonProperty("ecomId")
	String id;
	
	@JsonProperty("chksum")
	public String checkSum;
	
	@JsonProperty("ecom_ty")
	public String ecomType;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;
	
	@JsonProperty("tax_amount")
	private Double taxAmount;
		
	@JsonProperty("einvoices")
	public List<EInvoice> eInvoices;

	
	public @Data static class EInvoice extends Invoice{
		
		@JsonProperty("flag")
		public TaxpayerAction taxPayerAction;//moved from Ecom to Einvoice 
		
		@JsonFormat(pattern="YYYY/MM" ,timezone=Transaction.jsonDateTimeZone)
		@JsonProperty("txprd")
		public Date taxPeriod;
		
		@JsonProperty("mid")
		public String merchantId;
		
		@JsonProperty("grsval")
		public Double grossValue;
		
		@JsonProperty("ctin")
		public String gstin;
		
		@JsonProperty("ctin_name")
		public String gstinName;
		
		@JsonFormat(pattern=Transaction.jsonDateFormat)
		@JsonProperty("orgi_txprd")
		public Date originalTaxPeriod;
		
		
		
		@JsonProperty("itms")
		public List<EInvoiceItem> invoiceItems;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((gstin == null) ? 0 : gstin.hashCode());
			return result;
		}

		
		@Override
		public boolean equals(Object obj) {     
		   EqualsBuilder builder = new EqualsBuilder().append(this.gstin, ((EInvoice) obj).getGstin());               
		   return builder.isEquals();
		}
		
	}
	
	public @Data static class EInvoiceItem extends InvoiceItem {
		
		
		
		@JsonProperty("sup_type")
		public String natureOfSupply;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((natureOfSupply == null) ? 0 : natureOfSupply.hashCode());
			return result;
		}

		
		@Override
		public boolean equals(Object obj) {     
		   EqualsBuilder builder = new EqualsBuilder().append(this.natureOfSupply, ((EInvoiceItem) obj).getNatureOfSupply());               
		   return builder.isEquals();
		}
		
		
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ecomType == null) ? 0 : ecomType.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.ecomType, ((ECOM) obj).getEcomType());               
	   return builder.isEquals();
	}
}
