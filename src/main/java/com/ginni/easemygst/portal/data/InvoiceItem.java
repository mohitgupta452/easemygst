package com.ginni.easemygst.portal.data;

import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class InvoiceItem extends TaxAmount {
	
	@GeneratedValue
	@JsonProperty("itemId")//will be used in case of  AT,
	String id;

	@JsonProperty("num")
	private Integer sNo;

	@JsonProperty("hsn_sc")
	private String goodsOrServiceCode;//should only be used either this or hsn
	
	//extra hsn fields start	b
	@JsonProperty("hsn_desc")
	private String hsnDesc;
	
	@JsonProperty("unit")
	public String unit;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("qty")
    public Double quantity;
	//extra hsn fields ends	

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("txval")
	private Double taxableValue;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")
	private Double taxAmount;

	// for gstr2
	@JsonProperty("elg")
	private String eligOfTotalTax;

	@ManyToOne
	@JsonProperty("itc_details")
	private ItcDetail itcDetails;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("adamt") 	 	//added for AT only after gov api-2.0 release
    public Double advanceAmount;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sNo == null) ? 0 : sNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder().append(this.sNo, ((InvoiceItem) obj).getSNo());
		return builder.isEquals();
	}

}
