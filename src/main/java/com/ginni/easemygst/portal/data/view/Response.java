package com.ginni.easemygst.portal.data.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ArrayNode;

import lombok.Data;

@JsonInclude(value=Include.NON_NULL)
public @Data class Response {
	
	@JsonProperty(value="resp_msg",required=true)
	private String responseMessage;
	
	@JsonProperty(value="resp_code",required=true)
	private String responseCode;
	
	private ArrayNode data;
	
	private Error error;
	
	public @Data static class Error{
		
		@JsonProperty(value="error_cd")
		private String errorCode="1001";
		
		@JsonProperty(value="message")
		private String errorMessage="Unable to process your request, please contact support team.";
		
	}
	
}
