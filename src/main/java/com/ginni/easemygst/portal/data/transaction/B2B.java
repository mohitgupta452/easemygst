package com.ginni.easemygst.portal.data.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.BaseData;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class B2B extends BaseData {

	@JsonProperty("bId")
	String id;
	
	@JsonProperty("ctin")
	private String gstin;
	
	@JsonProperty("ctin_name")
	private String gstinName;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")
	private Double taxAmount;
	
	@JsonProperty("cfs")
	private String filingStatus;//added after gov api 2.0 release
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((gstin == null) ? 0 : gstin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		/*if (!super.equals(obj))
			return false;*/
		if (getClass() != obj.getClass())
			return false;
		B2B other = (B2B) obj;
		if (gstin == null) {
			if (other.gstin != null)
				return false;
		} else if (!gstin.equals(other.gstin))
			return false;
		return true;
	}
	
}
