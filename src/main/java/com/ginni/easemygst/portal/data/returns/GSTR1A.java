package com.ginni.easemygst.portal.data.returns;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;

public @lombok.Data class GSTR1A {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonFormat(pattern="MMyyyy")
	@JsonProperty("fp")
	private Date monthYear;
	
	@JsonProperty("gt")
	private String grossTurnover;
	
	@JsonProperty("b2b")
	private List<B2B> b2b;

	@JsonProperty("b2ba")
	private List<B2B> b2ba;
	
	@JsonProperty("cdn")
	private List<CDN> cdn;

	@JsonProperty("cdna")
	private List<CDN> cdna;
	
}
