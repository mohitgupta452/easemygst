package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ExportType;

public class EXPDeserializer extends BaseDataDeserializer<List<ExpDetail>> {

	@Override
	public List<ExpDetail> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<ExpDetail> exps = new ArrayList<>();
		Iterator<JsonNode> expNodes = node.elements();
		while (expNodes.hasNext()) {
			JsonNode expNode = expNodes.next();
			ExpDetail exp=new ExpDetail();
		//	ExpDetail exp = new ExpDetail();
//			if (expNode.has("exp_typ")) {
//				exp.setExportType(expNode.get("exp_typ").asText());
			if(expNode.has("error_msg"))
				exp.setError_msg(expNode.get("error_msg").asText());
			if(expNode.has("error_cd"))
				exp.setError_cd(expNode.get("error_cd").asText());
			if(expNode.has("exp_typ"))
				exp.setExportType(expNode.get("exp_typ").asText());
			
			Iterator<JsonNode> invoices = expNode.get("inv").elements();
			while (invoices.hasNext()) {
				JsonNode invNode = invoices.next();

//				if (invNode.has("chksum"))
//					inv.setCheckSum(invNode.get("chksum").asText());
				if (invNode.has("inum"))
					exp.setInvoiceNumber(invNode.get("inum").asText());

				String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
				if (!StringUtils.isEmpty(invDate)) {
					try {
						exp.setInvoiceDate(gstFmt.parse(invDate));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				if (invNode.has("val"))
					exp.setTaxableValue(invNode.get("val").asDouble());
				if (invNode.has("sbpcode"))
					exp.setShippingBillPortCode(invNode.get("sbpcode").asText());
				if (invNode.has("sbnum"))
					exp.setShippingBillNo(invNode.get("sbnum").asText());

				String sbDate = invNode.has("sbdt") ? invNode.get("sbdt").asText() : null;
				if (!StringUtils.isEmpty(sbDate)) {
					try {
						exp.setShippingBillDate(gstFmt.parse(sbDate));
					} catch (ParseException e) {
						e.printStackTrace();
					}

				}


				exp.setItems(this.deserializeItem(invNode));
			
			
			//exp.setExpDetails(this.deserializeInvoice(expNode));
			exps.add(exp);
		}
		}

		return exps;

	}

	public List<ExpDetail> deserializeInvoice(JsonNode node) {

		Iterator<JsonNode> invoices = node.get("inv").elements();
		List<ExpDetail> invs = new ArrayList<>();
		while (invoices.hasNext()) {
			JsonNode invNode = invoices.next();
			ExpDetail inv = new ExpDetail();

//			if (invNode.has("chksum"))
//				inv.setCheckSum(invNode.get("chksum").asText());
			if (invNode.has("inum"))
				inv.setInvoiceNumber(invNode.get("inum").asText());

			String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
			if (!StringUtils.isEmpty(invDate)) {
				try {
					inv.setInvoiceDate(gstFmt.parse(invDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (invNode.has("val"))
				inv.setTaxableValue(invNode.get("val").asDouble());
			if (invNode.has("sbpcode"))
				inv.setShippingBillPortCode(invNode.get("sbpcode").asText());
			if (invNode.has("sbnum"))
				inv.setShippingBillNo(invNode.get("sbnum").asText());

			String sbDate = invNode.has("sbdt") ? invNode.get("sbdt").asText() : null;
			if (!StringUtils.isEmpty(sbDate)) {
				try {
					inv.setShippingBillDate(gstFmt.parse(sbDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}

			/*if (invNode.has("prs"))
				inv.setProvAssess(invNode.get("prs").asText());
			if (invNode.has("od_num"))
				inv.setOrderNum(invNode.get("od_num").asText());
			String orderDate = invNode.has("od_dt") ? invNode.get("od_dt").asText() : null;
			if (!StringUtils.isEmpty(orderDate)) {
				try {
					inv.setOrderDt(gstFmt.parse(orderDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}*/

			inv.setItems(this.deserializeItem(invNode));

			invs.add(inv);

		}
		return invs;
	}

	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();

			if (itemNode.has("txval")) 
				item.setTaxableValue(itemNode.get("txval").asDouble());
			if (itemNode.has("rt"))
				item.setTaxRate(itemNode.get("rt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgst(itemNode.get("iamt").asDouble());

			itms.add(item);
		}
		return itms;
	}

}
