package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;

import lombok.Data;

public class NILDeserializer extends BaseDataDeserializer<List<NILTransactionEntity>> {
	private String returnType = "gstr1";

	@Override
	public List<NILTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<NILTransactionEntity> nils = new ArrayList<>();
		Iterator<JsonNode> nilNodes = node.elements();
		while (nilNodes.hasNext()) {
			JsonNode nilNode = nilNodes.next();
			NILTransactionEntity nil = new NILTransactionEntity();

			if (nilNode.has("sply_ty")) {
				//this.setReturnType("gstr2");
				String splyTyp = nilNode.get("sply_ty").asText();
				for (NilSupplyType st : NilSupplyType.values()) {
					if (st.toString().equalsIgnoreCase(splyTyp)) {
						nil.setSupplyType(st);
					}
				}
			}
			if(nilNode.has("error_msg"))
				nil.setErrorMsg(nilNode.get("error_msg").asText());
			if(nilNode.has("error_cd"))
				nil.setErrorCd(nilNode.get("error_cd").asText());
			if(nilNode.has("expt_amt"))
				nil.setTotExptAmt(nilNode.get("expt_amt").asDouble());
			if(nilNode.has("nil_amt"))
				nil.setTotNilAmt(nilNode.get("nil_amt").asDouble());
			if(nilNode.has("ngsup_amt"))
				nil.setTotNgsupAmt(nilNode.get("ngsup_amt").asDouble());

			nils.add(nil);
		}

		return nils;

	}

}
