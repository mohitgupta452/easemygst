package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;


public class TXPDetailDeserializer extends BaseDataDetailDeserializer<List<TxpdTransaction>> {

	@Override
	public List<TxpdTransaction> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<TxpdTransaction> txpdTransactions = new ArrayList<>();
		Iterator<JsonNode> txpdNodes = node.elements();
		while (txpdNodes.hasNext()) {
			JsonNode txpdNode = txpdNodes.next();
			TxpdTransaction txpdTransaction = new TxpdTransaction();
			if (txpdNode.has("sply_ty"))
			txpdTransaction.setSupplyType(SupplyType.valueOf(txpdNode.get("sply_ty").asText()));
			if (txpdNode.has("pos"))
			txpdTransaction.setPos(txpdNode.get("pos").asText());
			String stateName=Objects.nonNull(AppConfig.getStateNameByCode(txpdTransaction.getPos()))?AppConfig.getStateNameByCode(txpdTransaction.getPos()).getName():"";
			txpdTransaction.setStateName(stateName);
			if (txpdNode.has("advance_adjusted"))
			txpdTransaction.setAdvanceAdjusted(txpdNode.get("advance_adjusted").asDouble());
			/*if (txpdNode.has("tax_amt"))
			txpdTransaction.setTaxAmount(txpdNode.get("tax_amt").asDouble());*/
			/*txpdTransaction.setFlags(txpdNode.get("flags").asText());
			txpdTransaction.setType(txpdNode.get("type").asText());*/
			if (txpdNode.has("source"))
			txpdTransaction.setSource(txpdNode.get("source").asText());
			if (txpdNode.has("invoice_id"))
			txpdTransaction.setId(txpdNode.get("invoice_id").asText());
			txpdTransaction.setItems(this.deserializeItem(txpdNode));
			
			if (txpdNode.has("originalMonth"))
				txpdTransaction.setOriginalMonth(txpdNode.get("originalMonth").asText());
			/*if (txpdNode.has("originalPos"))
				txpdTransaction.setOriginalPos(txpdNode.get("originalPos").asText());
			if (txpdNode.has("originalSupplyType"))
				txpdTransaction.setOriginalSupplyType(txpdNode.get("originalSupplyType").asText());*/
			
			if (StringUtils.isNoneEmpty(txpdTransaction.getOriginalMonth())
					/*|| StringUtils.isNoneEmpty(txpdTransaction.getOriginalPos())
					|| StringUtils.isNoneEmpty(txpdTransaction.getOriginalSupplyType())*/)
				txpdTransaction.setIsAmmendment(true);
			
			txpdTransactions.add(txpdTransaction);
		}
		return txpdTransactions;
	}
}
