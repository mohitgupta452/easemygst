package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class CDNReturnDeserializer extends BaseDataDeserializer<List<CDNDetailEntity>> {

	private Logger log = Logger.getLogger(getClass().getName());
	
	@Override
	public List<CDNDetailEntity> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<CDNDetailEntity> cdns = new ArrayList<>();
		Iterator<JsonNode> cdnNodes = node.elements();
		while (cdnNodes.hasNext()) {
			
			JsonNode cdnNode = cdnNodes.next();
			CDNDetailEntity cdn = new CDNDetailEntity();
			TaxpayerGstin taxpayerGstin=new TaxpayerGstin();
			if (cdnNode.has("ctin")){
				taxpayerGstin.setGstin(cdnNode.get("ctin").asText());
				cdn.setOriginalCtin(cdnNode.get("ctin").asText());
			}
			if(cdnNode.has("error_msg"))
                cdn.setError_msg(cdnNode.get("error_msg").asText());
			if(cdnNode.has("error_cd"))
			  cdn.setError_cd(cdnNode.get("error_cd").asText());
			if(cdnNode.has("nt")){
			Iterator<JsonNode> invoices = cdnNode.get("nt").elements();
			int j=0;
			while (invoices.hasNext()) {
				if(j>0){
				 cdn = new CDNDetailEntity();
				 taxpayerGstin=new TaxpayerGstin();
					if (cdnNode.has("ctin")){
						taxpayerGstin.setGstin(cdnNode.get("ctin").asText());
						cdn.setOriginalCtin(cdnNode.get("ctin").asText());
					}
					if(cdnNode.has("error_msg"))
		                cdn.setError_msg(cdnNode.get("error_msg").asText());
					if(cdnNode.has("error_cd"))
					  cdn.setError_cd(cdnNode.get("error_cd").asText());
				 
				}
			j++;
				JsonNode invNode = invoices.next();
				try{
				if(invNode.has("cflag"))
					cdn.setFlags(invNode.get("cflag").asText());
				if(invNode.has("updby"))
					cdn.setUpdatedBy(invNode.get("updby").asText());
				if(invNode.has("ntty"))
					cdn.setNoteType(invNode.get("ntty").asText());
				if(invNode.has("nt_num"))
					cdn.setRevisedInvNo(invNode.get("nt_num").asText());
				if(invNode.has("nt_dt"))
					cdn.setRevisedInvDate(gstFmt.parse(invNode.get("nt_dt").asText()));
				if(invNode.has("rsn"))
					cdn.setReasonForNote(invNode.get("rsn").asText());
				if(invNode.has("p_gst"))
					cdn.setPreGstRegime(invNode.get("p_gst").asText());
				if(invNode.has("inum"))
					cdn.setInvoiceNumber(invNode.get("inum").asText());
				if(invNode.has("idt"))
					cdn.setInvoiceDate(gstFmt.parse(invNode.get("idt").asText()));
				if(invNode.has("val"))
					cdn.setTaxableValue(invNode.get("val").asDouble());
				}catch(Exception e){
					e.printStackTrace();
				}
			    if(invNode.has("itms"))
				 cdn.setItems(this.deserializCDNItemDetail(invNode));

			cdns.add(cdn);
		}
			}else{
				if(cdnNode.has("inum"))
					cdn.setInvoiceNumber(cdnNode.get("inum").asText());
				if(cdnNode.has("nt_num"))
					cdn.setRevisedInvNo(cdnNode.get("nt_num").asText());
				cdns.add(cdn);
			}
	 }
		return cdns;

	}

	public List<CDNDetailEntity> deserializeCdnData(JsonNode node) {

		Iterator<JsonNode> invoices = node.get("nt").elements();
		List<CDNDetailEntity> invs = new ArrayList<>();
		try{
		while (invoices.hasNext()) {
			JsonNode invNode = invoices.next();
			CDNDetailEntity inv = new CDNDetailEntity();
//			if(invNode.has("chksum"))
//				inv.setCheckSum(invNode.get("chksum").asText());
			if(invNode.has("cflag"))
				inv.setFlags(invNode.get("cflag").asText());
			if(invNode.has("updby"))
				inv.setUpdatedBy(invNode.get("updby").asText());
			if(invNode.has("ntty"))
				inv.setNoteType(invNode.get("ntty").asText());
			if(invNode.has("nt_num"))
				inv.setRevisedInvNo(invNode.get("nt_num").asText());
			if(invNode.has("nt_dt"))
				inv.setRevisedInvDate(gstFmt.parse(invNode.get("nt_dt").asText()));
			if(invNode.has("rsn"))
				inv.setReasonForNote(invNode.get("rsn").asText());
			if(invNode.has("p_gst"))
				inv.setPreGstRegime(invNode.get("p_gst").asText());
			if(invNode.has("inum"))
				inv.setInvoiceNumber(invNode.get("inum").asText());
			if(invNode.has("idt"))
				inv.setInvoiceDate(gstFmt.parse(invNode.get("idt").asText()));
			if(invNode.has("val"))
				inv.setTaxableValue(invNode.get("val").asDouble());
			if(invNode.has("itms"))
			inv.setItems(this.deserializCDNItemDetail(invNode));

			invs.add(inv);

		}
	  } catch (Exception e) {
			e.printStackTrace();
	   }
		return invs;
	}

	public List<Item> deserializCDNItemDetail(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();
			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			JsonNode itemDetNode =null;
			if(itemNode.has("itm_det")){
			   itemDetNode = itemNode.get("itm_det");
			   if(itemDetNode!=null){
				   if (itemDetNode.has("txval"))
						item.setTaxableValue(itemDetNode.get("txval").asDouble());
					if (itemDetNode.has("rt"))
						item.setTaxRate(itemDetNode.get("rt").asDouble());
					if (itemDetNode.has("iamt"))
						item.setIgst(itemDetNode.get("iamt").asDouble());
					if (itemDetNode.has("camt"))
						item.setCgst(itemDetNode.get("camt").asDouble());
					if (itemDetNode.has("samt"))
						item.setSgst(itemDetNode.get("samt").asDouble());
					if (itemDetNode.has("csamt"))
						item.setCess(itemDetNode.get("csamt").asDouble());
			   }
			}
			else if(itemNode.has("itc")){
				itemDetNode=itemNode.get("itc");
				if(itemDetNode.has("elg"))
					item.setTotalEligibleTax(itemDetNode.get("elg").asText());
				if(itemDetNode.has("tx_cs"))
					item.setItcCess(itemDetNode.get("tx_cs").asDouble());
				if(itemDetNode.has("tx_i"))
					item.setIgst(itemDetNode.get("tx_i").asDouble());
				if(itemDetNode.has("tx_c"))
					item.setCgst(itemDetNode.get("tx_c").asDouble());
				if(itemDetNode.has("tx_s"))
					item.setItcSgst(itemDetNode.get("tx_s").asDouble());
			}
			
			itms.add(item);
			}
			
		return itms;
	}
}
