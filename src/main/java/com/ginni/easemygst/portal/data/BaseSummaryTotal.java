package com.ginni.easemygst.portal.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class BaseSummaryTotal {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_rec")
	private int totalInvoice;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_tax")
	private Double totalTax;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_sgst")
	private Double totalSgst;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_igst")
	private Double totalIGST;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_cgst")
	private Double totalCgst;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_cess")
	private Double totalCess;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_val")
	private Double totalValue;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("ttl_doc_issued")
	@JsonFormat(pattern="#0.00")
	private long totalDocIssue;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("ttl_doc_cancelled")
	private long totalDocCanceled;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("net_doc_issued")
	private long netDocIssued;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("ttl_nilsup_amt")
	private double totalNilSupAmt;
    
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("ttl_expt_amt")
	private double totalExptAmt;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("ttl_ngsup_amt")
	private double totalNgsupAmt;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("chksum")
	private String checkSum;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonFormat(pattern="#0.00")
	@JsonProperty("state_cd")
	private String status;
	
}
