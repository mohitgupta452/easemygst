package com.ginni.easemygst.portal.data.transaction;

import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class NIL {

	@JsonProperty("nil_id")
	String id;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tot_nil_amt")
	public Double totNilAmt;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tot_expt_amt")
	public Double totExptAmt;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tot_ngsup_amt")
	public Double totNgsupAmt;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tot_comp_amt")
	public Double totCompAmt;
	
	private String source;
	
	private String sourceId;
	
	private boolean isTransit = false;
	
	private String transitId = "";
	
	private boolean isAmendment;

	private boolean isValid=true;
	
	private ReconsileType type;

	private String flags = "";


	private Map<String, String> error;

	private boolean isGstnSynced = false;

	@JsonProperty("flag")
	public TaxpayerAction taxPayerAction= TaxpayerAction.UPLOADED;

	@JsonProperty("chksum")
	public String checkSum;

	@JsonProperty("sply_ty")
	public NilSupplyType supplyType;//gstr2
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getSupplyType(), ((NIL) obj).getSupplyType());               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((supplyType == null) ? 0 : supplyType.hashCode());
		return result;
	}
	

}
