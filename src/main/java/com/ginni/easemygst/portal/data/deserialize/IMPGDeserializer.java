package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;
import com.ginni.easemygst.portal.transaction.impl.IMPGTransaction.SEZ;

public class IMPGDeserializer extends BaseDataDeserializer<List<IMPGTransactionEntity>> {

	@Override
	public List<IMPGTransactionEntity> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<IMPGTransactionEntity> impgs = new ArrayList<>();
		Iterator<JsonNode> impgNodes = node.elements();
		while (impgNodes.hasNext()) {
			JsonNode impgNode = impgNodes.next();
			IMPGTransactionEntity impg = new IMPGTransactionEntity();
			if(impgNode.has("error_msg"))
				impg.setErrorMsg(impgNode.get("error_msg").asText());
			if(impgNode.has("error_cd"))
				impg.setErrorCd(impgNode.get("error_cd").asText());
			
			if(impgNode.has("is_sez")){
				String isSez=impgNode.get("is_sez").asText();
				for (SEZ st : SEZ.values()) {
					if (st.toString().equalsIgnoreCase(isSez)) {
						impg.setSez(st);
					}
				}
			}
			if(impgNode.has("stin")){
				TaxpayerGstin taxpayer = new TaxpayerGstin();
				taxpayer.setGstin(impgNode.get("stin").asText());
				impg.setGstin(taxpayer);
			}
				
			
			if (impgNode.has("boe_num"))
				impg.setBillOfEntryNumber(impgNode.get("boe_num").asText());
			if (impgNode.has("boe_val"))
				impg.setBillOfEntryValue(impgNode.get("boe_val").asDouble());

//			if (impgNode.has("chksum"))
//				impg.setCheckSum(impgNode.get("chksum").asText());

			String invDate = impgNode.has("boe_dt") ? impgNode.get("boe_dt").asText() : null;
			if (!StringUtils.isEmpty(invDate)) {
				try {
					impg.setBillOfEntryDate(gstFmt.parse(invDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(impgNode.has("port_code"))
			   impg.setPortCode(impgNode.get("port_code").asText());
            if(impgNode.has("itms"))   
			impg.setItems(this.deserializeItem(impgNode));
			impgs.add(impg);
		}

		return impgs;

	}

	public List<Item> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();

			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			if (itemNode.has("txval"))
				item.setTaxableValue(itemNode.get("txval").asDouble());
			if (itemNode.has("rt"))
				item.setTaxRate(itemNode.get("rt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgst(itemNode.get("iamt").asDouble());
			if (itemNode.has("csamt"))
				item.setCess(itemNode.get("csamt").asDouble());
			if (itemNode.has("elg")) {
				item.setTotalEligibleTax(itemNode.get("elg").asText());
			if(itemNode.has("tx_i"))	
				item.setItcIgst(itemNode.get("tx_i").asDouble());
			if(itemNode.has("tx_cs"))
				item.setItcCess(itemNode.get("tx_cs").asDouble());
				//item.setItcDetails(this.deserializeItcDetail(itemNode, item));
			}

			itms.add(item);
		}
		return itms;
	}

	public ItcDetail deserializeItcDetail(JsonNode itemNode, Item invItem) {
		JsonNode itcNode = itemNode;
		ItcDetail itcDetail = new ItcDetail();
		/*
		 * if (itemNode.has("elg"))
		 * invItem.setEligOfTotalTax(itcNode.get("elg").asText());
		 */
		if (!Objects.isNull(itcNode)) {
			if (itemNode.has("tx_i"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_i").asDouble());
			if (itemNode.has("tx_cs"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_cs").asDouble());
		}
		return itcDetail;
	}

}
