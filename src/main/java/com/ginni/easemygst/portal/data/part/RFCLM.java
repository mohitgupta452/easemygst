package com.ginni.easemygst.portal.data.part;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class RFCLM {
	
	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String ret_period;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("bankacc")
	private Long bankacc;
	
	@JsonProperty("debitno")
	private String debitno;
	
	@JsonProperty("igrfclm")
	private ClaimData igrfclm;
	
	@JsonProperty("cgrfclm")
	private ClaimData cgrfclm;
	
	@JsonProperty("sgrfclm")
	private ClaimData sgrfclm;
	
	@JsonProperty("csrfclm")
	private ClaimData csrfclm;

	
}
