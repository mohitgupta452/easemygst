package com.ginni.easemygst.portal.data.part;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class CashLedgerBalance {

		
	@JsonProperty("desc")
	private String description;
	
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("op_total")
	private Double openingTotal;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("cl_total")
	private Double closingTotal;
	
	
	@JsonProperty("igst")
	private LedgerTax igst;
	
	@JsonProperty("cgst")
	private LedgerTax cgst;
	
	@JsonProperty("sgst")
	private LedgerTax sgst;
	
	@JsonProperty("cess")
	private LedgerTax cess;
	
}
