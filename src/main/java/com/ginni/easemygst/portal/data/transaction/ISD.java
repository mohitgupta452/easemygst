package com.ginni.easemygst.portal.data.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.Audit;
import com.ginni.easemygst.portal.data.Comments;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class ISD extends ItcDetail  {

	@JsonProperty("isdId")
	String id;
	
	private boolean isAmendment;

	private boolean isValid=true;

	private Map<String, String> error;

	private boolean isGstnSynced = false;
	
	private boolean toBeSynced = false;

	private String syncId;

	private boolean isLocked = true;
	
	private String reconcileFlags;

	private String mismatchedInvoice;
	
	private List<Comments> comments;
	
	private List<Audit> audits;
	
	private String flags = "";
	
	private String source;

	private String sourceId;
	
	private boolean isTransit = false;
	
	private String transitId = "";
	
	private ReconsileType type;


	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")//not in document
	private Double taxAmount;
	
	@JsonProperty("flag")
	public TaxpayerAction taxPayerAction;

	@JsonProperty("chksum")
	public String checkSum;
	
	@JsonProperty("gstin_isd")
	public String gstin;

	@JsonProperty("i_num")
	public String invoiceDetailNum;

	@JsonFormat(pattern=Transaction.jsonDateFormat)
	@JsonProperty("i_dt")
	public Date invoiceDetailDate;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("ig_cr")
    public Double creditIgst;
    
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cg_cr")
    public Double creditCgst;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("sg_cr")
    public Double creditSgst;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("cs_cr")
    public Double creditCess;
	
	/*@JsonProperty("elg")
	private String eligOfTotalTax;

	@JsonProperty("itc_details")
	private ItcDetail itcDetails;*/
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((invoiceDetailNum == null) ? 0 : invoiceDetailNum.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.invoiceDetailNum, ((ISD) obj).getInvoiceDetailNum());               
	   return builder.isEquals();
	}
	
	
}
