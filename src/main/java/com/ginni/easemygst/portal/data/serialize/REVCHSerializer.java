package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.transaction.IMPG;

public class REVCHSerializer extends BaseDataSerializer<List<IMPG>> {

	// final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");

	@Override
	public void serialize(List<IMPG> impgs, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		for (IMPG impg : impgs) {
			json.writeStartObject();
			if (!StringUtils.isEmpty(impg.getCheckSum()))
				json.writeStringField("chksum", String.valueOf(impg.getCheckSum()));
			if (!StringUtils.isEmpty(impg.getBillOfEntryNum()))
				json.writeStringField("boe_num", String.valueOf(impg.getBillOfEntryNum()));
			if (impg.getBillOfEntryDate() != null)
				json.writeStringField("boe_dt", gstFmt.format(impg.getBillOfEntryDate()));
			if (impg.getBillOfEntryVal() != null)
				json.writeNumberField("boe_val", impg.getBillOfEntryVal());
			writeLineItems(json, impg);
			json.writeEndObject();
		}
		json.writeEndArray();
	}

	void writeLineItems(JsonGenerator json, IMPG impg) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		impg.getImpgInvItms().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	private void writeLineItem(JsonGenerator json, InvoiceItem item) {
		try {
			json.writeStartObject();// start item
			json.writeStringField("num", item.getSNo() != null ? String.valueOf(item.getSNo()) : "");
			json.writeStringField("hsn_sc", item.getGoodsOrServiceCode());
			if (item.getTaxableValue() != null)
				json.writeNumberField("txval", item.getTaxableValue());
			if (item.getTaxRate() != null)
				json.writeNumberField("irt", item.getTaxRate());
			if (item.getIgstAmt() != null)
				json.writeNumberField("iamt", item.getIgstAmt());
			json.writeStringField("elg",
					item.getEligOfTotalTax() != null ? String.valueOf(item.getEligOfTotalTax()) : "");
			 json.writeStringField("tx_i",
			 item.getItcDetails().getTotalTaxAvalIgst() != null ?
			 String.valueOf(item.getItcDetails().getTotalTaxAvalIgst()) : "");
//			 json.writeStringField("tc_i",
//			 item.get != null ?
//			 String.valueOf(item.getTotInpTaxCrdtAvalForClaimThisMonthIgst())
//			 : "");
			json.writeEndObject();// end item
		} catch (Exception e) {
			/// log
			e.printStackTrace();
		}
	}

}
