package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.utils.Utility;

public class B2BURSerializer extends BaseDataSerializer<List<B2bUrTransactionEntity>> {
	
	 final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	
     private ReturnType returnType = ReturnType.GSTR2;
	
	 private TransactionType transType = TransactionType.B2BUR;

	@Override
	public void serialize(List<B2bUrTransactionEntity> b2burs, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		b2burs=this.groupByDetailEntity(b2burs);
		for (B2bUrTransactionEntity b2bur : b2burs) {
			json.writeStartObject();
			if(!StringUtils.isEmpty(b2bur.getErrorMsg()))
				json.writeStringField("error_msg", b2bur.getErrorMsg());
			if(!StringUtils.isEmpty(b2bur.getErrorCd()))
				json.writeStringField("error_cd", b2bur.getErrorCd());
			json.writeFieldName("inv");
			json.writeStartArray();
			json.writeStartObject();
			//json.writeStringField("chksum",String.valueOf(i++));
			if(!StringUtils.isEmpty(b2bur.getInvoiceNumber()))
			  json.writeStringField("inum", b2bur.getInvoiceNumber());
			if(b2bur.getInvoiceDate()!=null)
			 json.writeStringField("idt", gstFmt.format(b2bur.getInvoiceDate()));
			if (!Objects.isNull(b2bur.getFlags()) && !StringUtils.isEmpty(this.matchFlag(b2bur.getFlags())))
				json.writeStringField("flag",
						b2bur.getFlags() != null ? this.matchFlag(b2bur.getFlags()) : null);
			else{
			if((b2bur.getTaxableValue()+b2bur.getTaxAmount())!=0){
				BigDecimal val=BigDecimal.ZERO;
				val=val.add(Utility.getBigDecimalScale(b2bur.getTaxableValue())).add(Utility.getBigDecimalScale(b2bur.getTaxAmount()));
			    json.writeNumberField("val",val.setScale(2,RoundingMode.FLOOR));
			    
			     
			}
			if(!StringUtils.isEmpty(b2bur.getPos()))
			  json.writeStringField("pos", (Integer.parseInt(b2bur.getPos())<10 && b2bur.getPos().length()==1)?"0"+b2bur.getPos():b2bur.getPos());
			if(b2bur.getSupplyType()!=null)
			 json.writeStringField("sply_ty",b2bur.getSupplyType().getValue());
			if(b2bur.getItems()!=null){
		      if(!b2bur.getItems().isEmpty())		 
			    new ItemSerializer().writeLineItems(json, b2bur.getItems(), returnType, transType);
			}
		 }
			//writeItems(b2bur.getItems(), json);
			json.writeEndObject();
			json.writeEndArray();
			json.writeEndObject();
			
		}
		json.writeEndArray();
	}
	
	private static String checkEmpty(String str){
		if(!StringUtils.isEmpty(str))
			return str;
		else
			return "";
	}
	
	private static void writeItems(List<Item> items,JsonGenerator json){
		try {
			json.writeFieldName("itms");
			json.writeStartArray();
			json.writeStartObject();
			items.forEach(item -> writeItemData(item,json));
			json.writeEndObject();
			json.writeEndArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
   }
	
	private static void writeItemData(Item item,JsonGenerator json){
		try{
			json.writeNumberField("num", item.getSerialNumber());
			json.writeFieldName("itm_det");
			json.writeStartObject();
			json.writeNumberField("rt", item.getTaxRate());
			json.writeNumberField("txval", item.getTaxableValue());
			json.writeNumberField("camt", item.getCess());
			json.writeNumberField("samt", item.getSgst());
			json.writeNumberField("iamt", item.getIgst());
			json.writeNumberField("csamt", item.getCgst());
			json.writeEndObject();
			json.writeFieldName("itc");
			json.writeStartObject();
			json.writeStringField("elg", checkEmpty(item.getTotalEligibleTax()));
			json.writeNumberField("tx_s", item.getItcSgst());
			json.writeNumberField("tx_c", item.getItcCgst());
			json.writeNumberField("tx_i", item.getItcIgst());
			json.writeNumberField("tx_cs", item.getItcCess());
			json.writeEndObject();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	private List<B2bUrTransactionEntity> groupByDetailEntity(List<B2bUrTransactionEntity> datas){
		List<B2bUrTransactionEntity> result=new  ArrayList<>();
		Map<String, List<B2bUrTransactionEntity>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(B2bUrTransactionEntity::getId));
		for(Map.Entry<String, List<B2bUrTransactionEntity>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			B2bUrTransactionEntity b2bDetailEntity=null;
			for(B2bUrTransactionEntity b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

}
