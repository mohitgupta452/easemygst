package com.ginni.easemygst.portal.data.view.deserializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;


public class B2CLDetailDeserializer extends BaseDataDetailDeserializer<List<B2CLTransactionEntity>>{

	@Override
	public List<B2CLTransactionEntity> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		List<B2CLTransactionEntity> b2clTransactionEntities = new ArrayList<>();
		Iterator<JsonNode> b2clNodes = node.elements();
		while (b2clNodes.hasNext()) {
			JsonNode b2clNode = b2clNodes.next();
			B2CLTransactionEntity b2clTransactionEntity = new B2CLTransactionEntity();
			List<B2CLDetailEntity> details = new ArrayList<>();
			
			details.add(this.deserializeInvoice(b2clNode,b2clTransactionEntity));
			b2clTransactionEntity.setB2clDetails(details);
			b2clTransactionEntities.add(b2clTransactionEntity);
		}
		return b2clTransactionEntities;
	}

	
	public B2CLDetailEntity deserializeInvoice(JsonNode b2clNode,B2CLTransactionEntity b2cl) {
		B2CLDetailEntity b2clDetail = new B2CLDetailEntity();
		b2clDetail.setB2clTransaction(b2cl);
		if (b2clNode.has("pos"))
			b2clDetail.setPos(b2clNode.get("pos").asText());
		if (b2clNode.has("inum"))
			b2clDetail.setInvoiceNumber(b2clNode.get("inum").asText());
		
		String invDate = b2clNode.has("idt") ? b2clNode.get("idt").asText() : null;
		if (!StringUtils.isEmpty(invDate)) {
			try {
				b2clDetail.setInvoiceDate(gstFmt.parse(invDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		/*if (b2clNode.has("state_name"))
			b2clDetail.setStateName(b2clNode.get("state_name").asText());*/
		String stateName=Objects.nonNull(AppConfig.getStateNameByCode(b2clDetail.getPos()))?AppConfig.getStateNameByCode(b2clDetail.getPos()).getName():"";
		b2clDetail.setStateName(stateName);
		
		if (b2clNode.has("etin"))
			b2clDetail.setEtin(b2clNode.get("etin").asText());
		
		if (b2clNode.has("taxable_val"))
			b2clDetail.setTaxableValue(b2clNode.get("taxable_val").asDouble());
		
		if (b2clNode.has("tax_amt"))
			b2clDetail.setTaxAmount(b2clNode.get("tax_amt").asDouble());
		
		if (b2clNode.has("flags"))
			b2clDetail.setFlags(b2clNode.get("flags").asText());
		
		if (b2clNode.has("type"))
			b2clDetail.setType(b2clNode.get("type").asText());
		
		if (b2clNode.has("source"))
			b2clDetail.setSource(b2clNode.get("source").asText());
		
		if (b2clNode.has("invoice_id"))
			b2clDetail.setId(b2clNode.get("invoice_id").asText());
		
		if (b2clNode.has("isSynced"))
		b2clDetail.setIsSynced(b2clNode.get("isSynced").asBoolean());
		
		if (b2clNode.has("isTransit"))
		b2clDetail.setIsTransit(b2clNode.get("isTransit").asBoolean());
		
		if (b2clNode.has("inv_type"))
			b2clDetail.setType(b2clNode.get("inv_type").asText());
		
		if (b2clNode.has("toBeSynch"))
			b2clDetail.setToBeSync(b2clNode.get("toBeSynch").asBoolean());
		
		if(b2clNode.has("isError"))
			b2clDetail.setIsError(b2clNode.get("isError").asBoolean());
		
		if(b2clNode.has("errorMsg"))
			b2clDetail.setIsError(b2clNode.get("errorMsg").asBoolean());
		
		if (b2clNode.has("originalInvoiceNumber"))
			b2clDetail.setOriginalInvoiceNumber(b2clNode.get("originalInvoiceNumber").asText());
		String orgInvDate = b2clNode.has("originalInvoiceDate") ? b2clNode.get("originalInvoiceDate").asText() : null;
		if (!StringUtils.isEmpty(orgInvDate)) {
			try {
				b2clDetail.setOriginalInvoiceDate(gstFmt.parse(orgInvDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		if(StringUtils.isNoneEmpty(b2clDetail.getOriginalInvoiceNumber())||Objects.nonNull(b2clDetail.getOriginalInvoiceDate()))
			b2clDetail.setIsAmmendment(true);
		
		b2clDetail.setItems(this.deserializeItem(b2clNode));
		
		return b2clDetail;
	}
}
