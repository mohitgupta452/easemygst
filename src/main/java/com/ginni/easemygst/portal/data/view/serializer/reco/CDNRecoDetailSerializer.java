package com.ginni.easemygst.portal.data.view.serializer.reco;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class CDNRecoDetailSerializer extends JsonSerializer<CDNDetailEntity> {

	private String returnType = "gstr1";
	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	public final static SimpleDateFormat gstFmt = new SimpleDateFormat("dd/MM/yyyy");


	@Override
	public void serialize(CDNDetailEntity cdn, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		
			json.writeStartObject();
			/*json.writeStringField("ctin", b2b.getCtin());
			json.writeStringField("ctin_name", StringUtils.isNotEmpty(b2b.getCtinName())?b2b.getCtinName():"NO NAME");*/
			//Invoice invoice=b2b.getInvoice();
			json.writeStringField("invoice_id",cdn.getId() );
			json.writeStringField("inum", cdn.getInvoiceNumber());
			//json.writeStringField("financialYear",cdn.getFinancialYear() );
			json.writeNumberField("tax_amt",cdn.getTaxAmount() );
			json.writeNumberField("taxable_val",cdn.getTaxableValue() );
			//creation time
			json.writeStringField("createdBy",cdn.getCreatedBy() );
			json.writeStringField("creationIpAddress",cdn.getCreationIPAddress() );
			json.writeStringField("updatedBy",cdn.getUpdatedBy() );
			json.writeStringField("updationIpAddress",cdn.getUpdationIPAddress() );
			//updation time
			//json.writeStringField("b2bTransactionId",b2b.getB2bTransaction().getId());
			json.writeBooleanField("isAmmendment",cdn.getIsAmmendment());
			json.writeBooleanField("isValid",cdn.getIsValid() );
			json.writeBooleanField("isTransit",cdn.getIsTransit() );
			json.writeBooleanField("isMarked",cdn.getIsMarked() );
			json.writeBooleanField("isSynced",cdn.getIsSynced() );
			json.writeBooleanField("isLocked",cdn.getIsLocked() );
			json.writeStringField("syncId",cdn.getSynchId());
			json.writeStringField("transitId",cdn.getTransitId());
			json.writeStringField("source",cdn.getSource() );
			json.writeStringField("sourceId",cdn.getSourceId());
			json.writeStringField("flags",cdn.getFlags() );
			json.writeStringField("type",cdn.getType() );
			if(Objects.isNull(cdn.getInvoiceDate())){
				json.writeStringField("idt", null);
			}else
			json.writeStringField("idt", gstFmt.format(cdn.getInvoiceDate()));	
			json.writeStringField("revisedInvNo", cdn.getRevisedInvNo());
			json.writeStringField("noteType", cdn.getNoteType());
			if(Objects.isNull(cdn.getRevisedInvDate())){
				json.writeStringField("revisedInvDate", null);
			}else
			json.writeStringField("revisedInvDate", gstFmt.format(cdn.getRevisedInvDate()));
			json.writeStringField("preGstRegime", cdn.getPreGstRegime());
			json.writeStringField("rsn", cdn.getReasonForNote());
			json.writeStringField("inv_type",cdn.getInvoiceType() );
			json.writeStringField("dataSource", String.valueOf(cdn.getDataSource()));
			json.writeBooleanField("toBeSynch",cdn.getToBeSync());
			json.writeStringField("cinv_num", cdn.getcInvoiceNumber());
			if(Objects.isNull(cdn.getcInvoiceDate())){
				json.writeStringField("cidt", null);
			}else
			json.writeStringField("cidt", gstFmt.format(cdn.getcInvoiceDate()));
			json.writeNumberField("ctax_amount", cdn.getcTaxAmount());
			//previous invoice
			json.writeBooleanField("isError",cdn.getIsError() );
			json.writeStringField("errorMsg",cdn.getErrMsg() );
			json.writeStringField("checksum",cdn.getChecksum() );
			//json.writeBooleanField("isDelete",b2b.getis ); is delete
			json.writeStringField("previousFlags",cdn.getPreviousFlags() );
			json.writeStringField("previousType",cdn.getPreviousType() );
			json.writeNumberField("ctaxable_val", cdn.getcTaxableValue());
			json.writeObjectField("creationTime", cdn.getCreationTime());
			json.writeObjectField("updationTime", cdn.getUpdationTime());


			this.writeLineItems(json, cdn);
			json.writeEndObject();
		

	}

	void writeLineItems( JsonGenerator json, CDNDetailEntity inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	public void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			json.writeStringField("id", item.getId());
			json.writeNumberField("num", item.getSerialNumber());
			json.writeStringField("hsn_sc", item.getHsnCode());

			json.writeStringField("hsn_desc", item.getHsnDescription());

			json.writeStringField("unit", item.getUnit());

			json.writeNumberField("quantity", item.getQuantity());

			json.writeNumberField("txval", item.getTaxableValue());

			json.writeNumberField("rt", item.getTaxRate());

			json.writeNumberField("iamt", item.getIgst());

			json.writeNumberField("camt", item.getCgst());

			json.writeNumberField("samt", item.getSgst());

			json.writeNumberField("csamt", item.getCess());

			json.writeStringField("elg", item.getTotalEligibleTax());

			json.writeNumberField("tx_i", item.getItcIgst());
			json.writeNumberField("tx_c", item.getItcCgst());
			json.writeNumberField("tx_s", item.getItcSgst());
			json.writeNumberField("tx_cs", item.getItcCess());
			json.writeStringField("invoiceId", item.getInvoiceId());

			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
