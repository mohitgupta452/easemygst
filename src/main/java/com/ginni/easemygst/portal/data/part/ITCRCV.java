package com.ginni.easemygst.portal.data.part;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class ITCRCV {
	
	@JsonProperty("itcrcv_id")
	String id;

	private boolean isAmendment;

	private boolean isValid;

	private Map<String, String> error;

	private boolean isGstnSynced = false;
	
	private String flags = "";
	
	private boolean isTransit = false;
	
	private String transitId = "";

	private ReconsileType type;

	
	@JsonProperty("flag")
	public TaxpayerAction taxPayerAction;

	@JsonProperty("chksum")
	public String checkSum;
	
	@JsonProperty("stin")
	public String gstin;//added after gov-api 2.0
	
		
	@JsonProperty("typ")
	public String documentType;//added after gov-api 2.0
	
		
	@JsonProperty("i_num")
	public String invoiceNumber;
	
	@JsonFormat(pattern=Transaction.jsonDateFormat)
	@JsonProperty("i_dt")
	public Date invoiceDate;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("o_ig")
    public Double earlierIgst;
    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("n_ig")
    public Double thisMonthIgst;

	    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("o_cg")
    public Double earlierCgst;
    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("n_cg")
    public Double thisMonthCgst;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("o_sg")
    public Double earlierSgst;
    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("n_sg")
    public Double thisMonthSgst;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("o_cs")
    public Double earlierCess;//added after gov-api 2.0
    
	@JsonFormat(pattern="#0.00")
	@JsonProperty("n_cs")
    public Double thisMonthCess;//added after gov-api 2.0
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((invoiceNumber== null) ? 0 : invoiceNumber.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getInvoiceNumber(), ((ITCRCV) obj).getInvoiceNumber());               
	   return builder.isEquals();
	}

}
