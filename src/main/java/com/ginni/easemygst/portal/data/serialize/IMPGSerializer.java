package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.utils.Utility;

public class IMPGSerializer extends BaseDataSerializer<List<IMPGTransactionEntity>>{
	
    DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
    
	
	@Override
	public void serialize(List<IMPGTransactionEntity> impgtransactions, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		impgtransactions.forEach(impgtrans -> {
		  try{	
			json.writeStartObject();
			
			json.writeStringField("boe_num", checkEmpty(impgtrans.getBillOfEntryNumber()));
			json.writeStringField("boe_dt", checkEmpty(dateFormat.format(impgtrans.getBillOfEntryDate())));
			if (!Objects.isNull(impgtrans.getFlags()) && !StringUtils.isEmpty(impgtrans.getFlags()))
				json.writeStringField("flag",
						impgtrans.getFlags() != null ? this.matchFlag(impgtrans.getFlags()) : null);
			else{
				json.writeStringField("is_sez",impgtrans.getSez().toString());
				if (!StringUtils.isEmpty(impgtrans.getCtin()))
				json.writeStringField("stin", checkEmpty(impgtrans.getCtin()));
			if((impgtrans.getTaxableValue()+impgtrans.getTaxAmount())!=0.0)	
			json.writeNumberField("boe_val", (impgtrans.getTaxableValue()+impgtrans.getTaxAmount()));
			//json.writeStringField("chksum", impgtrans.get\);
			json.writeStringField("port_code", checkEmpty(impgtrans.getPortCode()));
			
			if(!StringUtils.isEmpty(impgtrans.getErrorMsg()))
				json.writeStringField("error_msg", impgtrans.getErrorMsg());
			if(!StringUtils.isEmpty(impgtrans.getErrorCd()))
				json.writeStringField("error_cd", impgtrans.getErrorCd());
			if(impgtrans.getItems()!=null){
			  if(!impgtrans.getItems().isEmpty()){	
				 
			   writeIMPGDatas(json, impgtrans.getItems());
			  }
			}
			}
			json.writeEndObject();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		});
		json.writeEndArray();
	}


	void writeIMPGDatas(JsonGenerator json, List<Item> items) throws IOException {
		// Start HsnsumDatas
		json.writeFieldName("itms");
		json.writeStartArray();
		 int i=1;
	    items.forEach(item -> writeIMPGData(json,item,i));
		json.writeEndArray();
	}
	
	private void writeIMPGData(JsonGenerator json,Item item,int i){
		try{
			json.writeStartObject();
			json.writeNumberField("num",i++ );
			json.writeNumberField("rt", item.getTaxRate());
			json.writeNumberField("iamt",Utility.getBigDecimalScale(item.getIgst()));
			json.writeNumberField("csamt", Utility.getBigDecimalScale(item.getCgst()));
			json.writeNumberField("txval", Utility.getBigDecimalScale(item.getTaxableValue()));
			json.writeStringField("elg",item.getTotalEligibleTax().toLowerCase());
			json.writeNumberField("tx_i",Utility.getBigDecimalScale( item.getItcIgst()));
			json.writeNumberField("tx_cs", Utility.getBigDecimalScale(item.getItcCgst()));
			json.writeEndObject();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static String checkEmpty(String str){
		if(!StringUtils.isEmpty(str))
			return str;
		else
			return "";
	}

}
