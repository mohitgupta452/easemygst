package com.ginni.easemygst.portal.data.part;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LedgerITCDetails {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonFormat(pattern = "DD-MM-YYY")
	@JsonProperty("fr_dt")
	private Date fromDate;
	
	@JsonFormat(pattern = "DD-MM-YYY")
	@JsonProperty("to_dt")
	private Date toDate;
	
	@JsonProperty("tr")
	private List<LedgerCashTransactionDetail> transactionDetails;
	
	@JsonProperty("itctr")
	private List<LedgerITCTransactionDetail> itcTransactionDetails;
	
	@JsonProperty("op_bal")
	private CashLedgerBalance openingBalance;
	
	@JsonProperty("cl_bal")
	private CashLedgerBalance closingBalance;
}
