package com.ginni.easemygst.portal.data.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.ginni.easemygst.portal.business.dto.Gstr3Dto;
import com.ginni.easemygst.portal.business.dto.OutsInsSupplyDto;
import com.ginni.easemygst.portal.business.dto.TodDto;
import com.ginni.easemygst.portal.business.dto.TotalTaxLiabilityDto;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.GoodsService;
import com.ginni.easemygst.portal.data.part.CreditDetails;
import com.ginni.easemygst.portal.data.part.ExportDetails;
import com.ginni.easemygst.portal.data.part.INS;
import com.ginni.easemygst.portal.data.part.ITCR;
import com.ginni.easemygst.portal.data.part.ImportDetails;
import com.ginni.easemygst.portal.data.part.InterStateSupplies;
import com.ginni.easemygst.portal.data.part.InterStateSupplyInward;
import com.ginni.easemygst.portal.data.part.IntraStateSupplies;
import com.ginni.easemygst.portal.data.part.IntraStateSupplyInward;
import com.ginni.easemygst.portal.data.part.JobDetails;
import com.ginni.easemygst.portal.data.part.OUTS;
import com.ginni.easemygst.portal.data.part.RFCLM;
import com.ginni.easemygst.portal.data.part.RevInvoiceOUTDetails;
import com.ginni.easemygst.portal.data.part.RevisionInvDetails;
import com.ginni.easemygst.portal.data.part.TOD;
import com.ginni.easemygst.portal.data.part.TTL;
import com.ginni.easemygst.portal.data.part.TotalTaxLiabDetails;
import com.ginni.easemygst.portal.data.part.TotalTaxLiabilityDetails;
import com.ginni.easemygst.portal.gst.response.GetGstr3Resp;
import com.ginni.easemygst.portal.helper.AppException;

public class Gstr3Transactions {

	public List<OutsInsSupplyDto> getOutwardSupplyData(GetGstr3Resp gstr3) throws AppException{
		
		List<OutsInsSupplyDto> osDtos = new ArrayList<>();
		if (!Objects.isNull(gstr3) && !Objects.isNull(gstr3.getOutward_supply())) {
			/*List<InterStateSupplies> intercs=new ArrayList<>();
			List<IntraStateSupplies>intracs=new ArrayList<>();*/
			OUTS outSupp = gstr3.getOutward_supply();
			List<InterStateSupplies> inters = !Objects.isNull(outSupp.getInterStateSuppToRegisteredTaxPayers())
					? outSupp.getInterStateSuppToRegisteredTaxPayers().getIn_det() : new ArrayList<>();
			List<IntraStateSupplies> intras = !Objects.isNull(outSupp.getIntraStateSuppToRegisteredTaxPayers())
					? outSupp.getIntraStateSuppToRegisteredTaxPayers().getIntra_det() : new ArrayList<>();
			List<InterStateSupplies> intercs = !Objects.isNull(outSupp.getInterStateSuppToConsumers())&&!Objects.isNull(outSupp.getInterStateSuppToConsumers().getInterc_det())
					? outSupp.getInterStateSuppToConsumers().getInterc_det() :new ArrayList<>();
			List<IntraStateSupplies> intracs = !Objects.isNull(outSupp.getIntraStateSuppToRegisteredTaxPayers())&&!Objects.isNull(outSupp.getIntraStateSuppToConsumers().getIntrac_det())
					? outSupp.getIntraStateSuppToConsumers().getIntrac_det() : new ArrayList<>();
			List<ExportDetails> exports = !Objects.isNull(outSupp.getExport()) ? outSupp.getExport().getExp_det()
					: new ArrayList<>();
			List<RevInvoiceOUTDetails> revInvoices = !Objects.isNull(outSupp.getRevisionOfInvoices())
					? outSupp.getRevisionOfInvoices().getDt() : new ArrayList<>();
			List<TotalTaxLiabilityDetails> tTaxLibDtls = !Objects.isNull(outSupp.getTotalTaxLiabilityOnOutSupp())
					? outSupp.getTotalTaxLiabilityOnOutSupp().getTottx_det() : new ArrayList<>();

			Double taxableValue = 0.0;
			Double igst = 0.0;
			Double cgst = 0.0;
			Double sgst = 0.0;
			Double cess = 0.0;
			OutsInsSupplyDto osDto1 = new OutsInsSupplyDto();

			for (InterStateSupplies iss : inters) {
				igst += iss.getIamt();
				cess += iss.getCess();
				taxableValue += iss.getTxval();
			}
			osDto1.setCategory("Interstate to Registered Taxable Person");
			osDto1.setIgst(igst);
			osDto1.setSgst(sgst);
			osDto1.setCgst(cgst);
			osDto1.setCess(cess);
			osDto1.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto2 = new OutsInsSupplyDto();

			for (IntraStateSupplies iss : intras) {
				cgst += iss.getCamt();
				sgst += iss.getSamt();
				cess += iss.getCess();
				taxableValue += iss.getTxval();
			}
			osDto2.setCategory("Intra state to Registered Taxable Person");
			osDto2.setIgst(igst);
			osDto2.setSgst(sgst);
			osDto2.setCgst(cgst);
			osDto2.setCess(cess);
			osDto2.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto3 = new OutsInsSupplyDto();

			for (InterStateSupplies iss : intercs) {
				igst += iss.getIamt();
				cess += iss.getCess();
				taxableValue += iss.getTxval();
			}
			osDto3.setCategory("Interstate to Consumer");
			osDto3.setIgst(igst);
			osDto3.setSgst(sgst);
			osDto3.setCgst(cgst);
			osDto3.setCess(cess);
			osDto3.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto4 = new OutsInsSupplyDto();

			for (IntraStateSupplies iss : intracs) {
				cgst += iss.getCamt();
				sgst += iss.getSamt();
				cess += iss.getCess();
				taxableValue += iss.getTxval();
			}
			osDto4.setCategory("Intrastate to Consumer");
			osDto4.setIgst(igst);
			osDto4.setSgst(sgst);
			osDto4.setCgst(cgst);
			osDto4.setCess(cess);
			osDto4.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto5 = new OutsInsSupplyDto();

			for (ExportDetails ed : exports) {
				cgst += ed.getCamt();
				sgst += ed.getSamt();
				cess += ed.getCess();
				taxableValue += ed.getTxval();
			}
			osDto5.setCategory("Exports");
			osDto5.setIgst(igst);
			osDto5.setSgst(sgst);
			osDto5.setCgst(cgst);
			osDto5.setCess(cess);
			osDto5.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto6 = new OutsInsSupplyDto();

			for (RevInvoiceOUTDetails revInv : revInvoices) {
				cgst += revInv.getCamt();
				sgst += revInv.getSamt();
				cess += revInv.getCess();
				taxableValue += revInv.getVal();
			}
			osDto6.setCategory("Revision of Invoices");
			osDto6.setIgst(igst);
			osDto6.setSgst(sgst);
			osDto6.setCgst(cgst);
			osDto6.setCess(cess);
			osDto6.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto7 = new OutsInsSupplyDto();

			for (RevInvoiceOUTDetails revInv : revInvoices) {
				cgst += revInv.getCamt();
				sgst += revInv.getSamt();
				cess += revInv.getCess();
				taxableValue += revInv.getVal();
			}
			osDto7.setCategory("Total Tax Liability");
			osDto7.setIgst(igst);
			osDto7.setSgst(sgst);
			osDto7.setCgst(cgst);
			osDto7.setCess(cess);
			osDto7.setTaxableValue(taxableValue);

			osDtos.add(osDto1);
			osDtos.add(osDto2);
			osDtos.add(osDto3);
			osDtos.add(osDto4);
			osDtos.add(osDto5);
			osDtos.add(osDto6);
			osDtos.add(osDto7);
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}

		return osDtos;

	}
	
	
public List<OutsInsSupplyDto> getInwardSupplyData(GetGstr3Resp gstr3) throws AppException{
		
		List<OutsInsSupplyDto> osDtos = new ArrayList<>();
		if (!Objects.isNull(gstr3) && !Objects.isNull(gstr3.getInward_supply())) {
			INS inSupp = gstr3.getInward_supply();

			List<ImportDetails> imports = !Objects.isNull(inSupp.getImports()) ? inSupp.getImports().getImp_det()
					: new ArrayList<>();
			List<ITCR> itcRevs = !Objects.isNull(inSupp.getItcReversal()) ? inSupp.getItcReversal().getItcr_rev()
					: new ArrayList<>();
			List<IntraStateSupplyInward> intras = !Objects.isNull(inSupp.getIntraStateSuppReceived())
					? inSupp.getIntraStateSuppReceived().getIntra() : new ArrayList<>();
			List<InterStateSupplyInward> inters = !Objects.isNull(inSupp.getInterStateSuppReceived())
					? inSupp.getInterStateSuppReceived().getInter() : new ArrayList<>();
			List<RevisionInvDetails> revisions = !Objects.isNull(inSupp.getRevision()) ? inSupp.getRevision().getDt()
					: new ArrayList<>();
			List<JobDetails> jobs = !Objects.isNull(inSupp.getJb_wrk()) ? inSupp.getJb_wrk().getJob_det() : new ArrayList<>();
			List<TotalTaxLiabDetails> tTaxLibDtls = !Objects.isNull(inSupp.getTotalTaxLiability())
					? inSupp.getTotalTaxLiability().getTxliab_det() : new ArrayList<>();

			Double taxableValue = 0.0;
			Double igst = 0.0;
			Double cgst = 0.0;
			Double sgst = 0.0;
			Double cess = 0.0;
			OutsInsSupplyDto osDto1 = new OutsInsSupplyDto();

			for (ImportDetails imp : imports) {
				igst += imp.getIamt();
				cess += imp.getCsamt();

			}
			osDto1.setCategory("Interstate Reciepts");
			osDto1.setIgst(igst);
			osDto1.setSgst(sgst);
			osDto1.setCgst(cgst);
			osDto1.setCess(cess);
			osDto1.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto2 = new OutsInsSupplyDto();

			for (ITCR itcr : itcRevs) {
				igst += itcr.getIgstReversalAmt();
				cgst += itcr.getCgstReversalAmt();
				sgst += itcr.getSgstReversalAmt();
				cess += itcr.getCgstReversalAmt();
				// taxableValue += iss.getTxval();
			}
			osDto2.setCategory("ITC Reversal");
			osDto2.setIgst(igst);
			osDto2.setSgst(sgst);
			osDto2.setCgst(cgst);
			osDto2.setCess(cess);
			osDto2.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto3 = new OutsInsSupplyDto();

			for (IntraStateSupplyInward iss : intras) {
				cgst += iss.getCamt();
				cess += iss.getCess();
				sgst += iss.getSamt();
				taxableValue += iss.getTxval();
			}
			osDto3.setCategory("Intrastate Reciepts");
			osDto3.setIgst(igst);
			osDto3.setSgst(sgst);
			osDto3.setCgst(cgst);
			osDto3.setCess(cess);
			osDto3.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto4 = new OutsInsSupplyDto();

			for (InterStateSupplyInward iss : inters) {
				igst += iss.getIamt();
				cess += iss.getCess();
				taxableValue += iss.getTxval();
			}
			osDto4.setCategory("Interstate Reciepts");
			osDto4.setIgst(igst);
			osDto4.setSgst(sgst);
			osDto4.setCgst(cgst);
			osDto4.setCess(cess);
			osDto4.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto5 = new OutsInsSupplyDto();

			for (RevisionInvDetails rev : revisions) {
				cgst += rev.getCamt();
				sgst += rev.getSamt();
				igst += rev.getIamt();
				taxableValue += rev.getVal();
			}
			osDto5.setCategory("Revision of Invoices");
			osDto5.setIgst(igst);
			osDto5.setSgst(sgst);
			osDto5.setCgst(cgst);
			osDto5.setCess(cess);
			osDto5.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto6 = new OutsInsSupplyDto();

			for (JobDetails jobDet : jobs) {
				cgst += jobDet.getCamt();
				sgst += jobDet.getSamt();
				igst += jobDet.getIamt();
				cess += jobDet.getCsamt();
				// taxableValue += jobDet.get;
			}
			osDto6.setCategory("Job Details");// job details and taxliability
												// are missing from view--view
												// has mismatches,tax on rev
												// charge but they are not available in api
			osDto6.setIgst(igst);
			osDto6.setSgst(sgst);
			osDto6.setCgst(cgst);
			osDto6.setCess(cess);
			osDto6.setTaxableValue(taxableValue);

			taxableValue = 0.0;
			igst = 0.0;
			cgst = 0.0;
			sgst = 0.0;
			cess = 0.0;
			OutsInsSupplyDto osDto7 = new OutsInsSupplyDto();

			for (TotalTaxLiabDetails tld : tTaxLibDtls) {
				cgst += tld.getCgstLiability();
				sgst += tld.getSgstLiability();
				igst += tld.getIgstLiability();
				taxableValue += tld.getTxval();
			}
			osDto7.setCategory("Total Tax Liability");
			osDto7.setIgst(igst);
			osDto7.setSgst(sgst);
			osDto7.setCgst(cgst);
			osDto7.setCess(cess);
			osDto7.setTaxableValue(taxableValue);

			osDtos.add(osDto1);
			osDtos.add(osDto2);
			osDtos.add(osDto3);
			osDtos.add(osDto4);
			osDtos.add(osDto5);
			osDtos.add(osDto6);
			osDtos.add(osDto7);
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}

		return osDtos;

	}
	

	public List<TodDto> getTodData(GetGstr3Resp gstr3) throws AppException {
		List<TodDto>todDtos=new ArrayList<>();
		if (!Objects.isNull(gstr3)) {
			for(TOD tod:gstr3.getTurnover()){
				TodDto todDto=new TodDto();
				todDto.setExportTurnOver(tod.getExportTurnOver());
				todDto.setGrossTurnOver(tod.getGrossTurnOver());
				todDto.setNetTaxableTurnOver(tod.getNetTaxableTurnOver());
				todDto.setNilAndExemptedTurnOver(tod.getNilAndExemptedTurnOver());
				todDto.setNonGstTurnOver(tod.getNonGstTurnOver());
				todDtos.add(todDto);
			}
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
		return todDtos;
	}
	
	
	public List<TotalTaxLiabilityDto> getTtlData(GetGstr3Resp gstr3) throws AppException {
		List<TotalTaxLiabilityDto> ttlDtos=new ArrayList<>();
		if (!Objects.isNull(gstr3)) {
			TTL ttl=gstr3.getTotal_tax_liabilty();
			List<TotalTaxLiabilityDetails> ttls = !Objects.isNull(ttl) ? ttl.getTtltx_det()
					: new ArrayList<>();
			for(TotalTaxLiabilityDetails ttld:ttls){
				TotalTaxLiabilityDto ttlDto=new TotalTaxLiabilityDto();
				ttlDto.setCamt(ttld.getCamt());
				ttlDto.setSamt(ttld.getSamt());
				ttlDto.setIamt(ttld.getIamt());
				ttlDto.setCess(ttld.getCess());
				ttlDto.setTxval(ttld.getTxval());
				ttlDto.setTy(ttld.getTy());
				if(!StringUtils.isEmpty(ttld.getTy())){
				String tempGS = ttld.getTy().trim();
				if (tempGS.equalsIgnoreCase("G") || tempGS.equalsIgnoreCase("GOODS")
						|| tempGS.equalsIgnoreCase("GOOD"))
					ttlDto.setTy(GoodsService.GOODS.toString());
				else if (tempGS.equalsIgnoreCase("S") || tempGS.equalsIgnoreCase("SERVICES")
						|| tempGS.equalsIgnoreCase("SERVICE"))
					ttlDto.setTy(GoodsService.SERVICE.toString());
				}
				
				ttlDto.setMon(ttld.getMon());
				ttlDtos.add(ttlDto);
			}
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
		return ttlDtos;
	}

	public List<CreditDetails> getItcCreditData(GetGstr3Resp gstr3) throws AppException {
		
		
		if (!Objects.isNull(gstr3)) {
			List<CreditDetails>itcCredits=!Objects.isNull(gstr3.getItc_credit())&&!Objects.isNull(gstr3.getItc_credit().getItc_det())?gstr3.getItc_credit().getItc_det():null;
			return itcCredits;
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
	}
	
	public List<CreditDetails> getTcsCreditData(GetGstr3Resp gstr3) throws AppException {
		
		
		if (!Objects.isNull(gstr3)) {
			List<CreditDetails>tcsCredits=!Objects.isNull(gstr3.getTcs_credit())&&!Objects.isNull(gstr3.getTcs_credit().getTcs_det())?gstr3.getTcs_credit().getTcs_det():null;
			return tcsCredits;
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
	}
	
	
	public List<CreditDetails> getTdsCreditData(GetGstr3Resp gstr3) throws AppException {
		
		
		if (!Objects.isNull(gstr3)) {
			List<CreditDetails>tdsCredits=!Objects.isNull(gstr3.getTds_credit())&&!Objects.isNull(gstr3.getTds_credit().getTds_det())?gstr3.getTds_credit().getTds_det():null;
			return tdsCredits;
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
	}
	
	
	
	public List<CreditDetails> getTaxPaidData(GetGstr3Resp gstr3) throws AppException {//not working
		
		
		if (!Objects.isNull(gstr3)) {
			List<CreditDetails>tcsCredits=!Objects.isNull(gstr3.getTcs_credit())&&!Objects.isNull(gstr3.getTcs_credit().getTcs_det())?gstr3.getTcs_credit().getTcs_det():null;
			return tcsCredits;
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
	}
	
	
public RFCLM getRefundClaimData(GetGstr3Resp gstr3) throws AppException {//not working
		
		
		if (!Objects.isNull(gstr3)) {
			RFCLM rfclms=!Objects.isNull(gstr3.getRefund_claim())?new RFCLM():null;
			return rfclms;
			
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
	}
	
	
	
public Gstr3Dto getAllData(GetGstr3Resp gstr3) throws AppException {//not working
		
		Gstr3Dto gstr=new Gstr3Dto();
		if (!Objects.isNull(gstr3)) {
			gstr.setTurnover(this.getTodData(gstr3));
			gstr.setOutward_supply(this.getOutwardSupplyData(gstr3));
			gstr.setInward_supply(this.getInwardSupplyData(gstr3));
			gstr.setItc_credit(this.getItcCreditData(gstr3));
			gstr.setTcs_credit(this.getTcsCreditData(gstr3));
			gstr.setTds_credit(this.getTdsCreditData(gstr3));
			gstr.setTotal_tax_liabilty(this.getTtlData(gstr3));
			//rfcclm,taxpaid remaining
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);

		}
		return gstr;
	}
	
	
	
	
	
}
