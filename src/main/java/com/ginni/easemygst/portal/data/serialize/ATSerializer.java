package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

public class ATSerializer extends BaseDataSerializer<List<ATTransactionEntity>> {
	
private ReturnType returnType = null;
	
	private TransactionType transType = TransactionType.AT;

	@Override
	public void serialize(List<ATTransactionEntity> ats, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		json.writeStartArray();
		
		if(!ats.isEmpty()){
			//ats=this.groupByDetailEntity(ats);
		Map<String,List<ATTransactionEntity>> expDetails=ats.stream().collect(Collectors.groupingBy(ATTransactionEntity::getPos));
		 
		 List<Item> items = null;
		 String flags=null;
			for(Entry<String, List<ATTransactionEntity>> entry:expDetails.entrySet()){
				items = new ArrayList<>();
				json.writeStartObject();
				
				int delCount =0;
				boolean isFirst=false;
				for(ATTransactionEntity tdp:entry.getValue()){
					if("D".equalsIgnoreCase(this.matchFlag(tdp.getFlags()))){
						flags=tdp.getFlags();
						delCount++;
					}
					else{
					if(tdp.getItems()!=null){
					 if(!tdp.getItems().isEmpty())	
					 items.addAll(tdp.getItems());
					}
					if(tdp.getFlags()!=null  ){
						if(!tdp.getItems().isEmpty())
							flags=tdp.getFlags();
					}
					
					if(!StringUtils.isEmpty(tdp.getErrorMsg()))
						json.writeStringField("error_msg", tdp.getErrorMsg());
					if(!StringUtils.isEmpty(tdp.getErrorCd()))
						json.writeStringField("error_cd", tdp.getErrorCd());
					//writeTXPDatas(json,tdp.getItems(),tdp.getSupplyType().getValue());
				}}
				
					json.writeStringField("sply_ty",entry.getValue().get(0).getSupplyType().getValue());
					for(ReturnType returnTyp:ReturnType.values()){
						if(returnTyp.toString().equalsIgnoreCase(entry.getValue().get(0).getReturnType()))
							returnType=returnTyp;
					}
				if(!StringUtils.isEmpty(entry.getKey()))
					   json.writeStringField("pos",(Integer.parseInt(entry.getKey())<10 && entry.getKey().length()==1)?"0"+entry.getKey():entry.getKey());
				
				if (delCount != 0 && delCount==entry.getValue().size())
					json.writeStringField("flag",
							flags != null ? this.matchFlag(flags) : null);
				else{
					if(entry.getValue().get(0).getIsAmmendment())
					json.writeStringField("omon",entry.getValue().get(0).getOriginalMonth());
				    if(items!=null)	
			     	new ItemSerializer().writeLineItems(json,items, returnType,transType);
				}
				
			//	json.writeEndArray();
				json.writeEndObject();
			}
//		for (ATTransactionEntity at : ats) {
//			json.writeStartObject();
////			if (!StringUtils.isEmpty(at.getGstin().get))
////				json.writeStringField("chksum", String.valueOf(at.getCheckSum()));
//			if (!StringUtils.isEmpty(at.getPos()))
//				json.writeStringField("pos", String.valueOf(at.getPos()));
//			if (!StringUtils.isEmpty(at.getSupplyType().toString()))
//				json.writeStringField("sply_ty", String.valueOf(at.getSupplyType()));
//			this.writeLineItems(json, at.getItems());
//			json.writeEndObject();
//		}
		}
		json.writeEndArray();
	}

	void writeLineItems(JsonGenerator json, List<Item> itms) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		itms.forEach(item -> writeLineItem(json, item));
		// End Line Items
		json.writeEndArray();
	}

	private void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

//			json.writeStringField("hsn_sc", item.getGoodsOrServiceCode());
			//if (item.getTaxableValue())
				json.writeNumberField("ad_amt",BigDecimal.valueOf(item.getTaxableValue()));
			//if (item.getTaxRate() != null)
				json.writeNumberField("rt", BigDecimal.valueOf(item.getTaxRate()));
			//if (item.getIgst() != null)
				json.writeNumberField("iamt", BigDecimal.valueOf(item.getIgst()));
//			if (item.getCgstAmt() != null)
//				json.writeNumberField("camt", item.getCgstAmt());
//			if (item.getSgstAmt() != null)
//				json.writeNumberField("samt", item.getSgstAmt());
			//if (item.getCess() != null)
				json.writeNumberField("csamt", BigDecimal.valueOf(item.getCess()));
			json.writeEndObject();// end item
		} catch (Exception e) {
			/// log
			e.printStackTrace();
		}

	}
	
	private List<ATTransactionEntity> groupByDetailEntity(List<ATTransactionEntity> datas){
		List<ATTransactionEntity> result=new  ArrayList<>();
		Map<String, List<ATTransactionEntity>> b2bDetails = datas.stream()
				.collect(Collectors.groupingBy(ATTransactionEntity::getId));
		for(Map.Entry<String, List<ATTransactionEntity>> ent:b2bDetails.entrySet()){
			List<Item>items=new ArrayList<>();
			ATTransactionEntity b2bDetailEntity=null;
			for(ATTransactionEntity b2b:ent.getValue()){
				items.addAll(b2b.getItems());
				b2bDetailEntity=b2b;
			}
			b2bDetailEntity.setItems(items);
			result.add(b2bDetailEntity);
		}
		
		return result;
	}

}
