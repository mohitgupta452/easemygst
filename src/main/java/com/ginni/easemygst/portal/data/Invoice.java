package com.ginni.easemygst.portal.data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ReconsileType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
//@JsonIgnoreProperties({"transit","locked","valid","amendment","gstnSynced","flags"})


public @Data class Invoice {

	@JsonProperty("invoice_id")
	String id;

	@JsonProperty("etin")
	private String ecomTin;// added after gov api 2.0 rlease

	@JsonProperty("updby")
	private String uploadedBy;// added after gov api 2.0 rlease

	@JsonIgnore
	private boolean isAmendment = false;

	@JsonIgnore
	private boolean isValid = true;

	private Map<String, String> error = new HashMap<String, String>();

	@JsonIgnore
	private boolean isGstnSynced = false;

	@JsonIgnore
	private boolean toBeSynced = false;

	@JsonIgnore
	private String syncId;

	@JsonIgnore
	private boolean isLocked = true;

	@JsonIgnore
	private boolean isTransit = false;

	@JsonIgnore
	private String transitId = "";

	@JsonIgnore
	private ReconsileType type;

	private String reconcileFlags;

	private String mismatchedInvoice;

	private List<Comments> comments;

	private List<Audit> audits;
	
	@JsonIgnore
	private String flags = "";

	@JsonIgnore
	private String source;

	@JsonIgnore
	private String sourceId;

	@JsonIgnore
	private boolean isMarked = false; // for marking pupose

	@JsonProperty("flag")
	private TaxpayerAction taxPayerAction = TaxpayerAction.UPLOADED;
	
	@JsonProperty("cflag")
	private TaxpayerAction cTaxPayerAction = TaxpayerAction.UPLOADED;

	@JsonProperty("chksum")
	private String checkSum;

	@JsonProperty("inum")
	private String supplierInvNum;

	@JsonFormat(pattern = Transaction.jsonDateFormat, timezone = Transaction.jsonDateTimeZone)
	@JsonProperty("idt")
	private Date supplierInvDt;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("val")
	private Double supplierInvVal;

	@JsonProperty("pos")
	private String pos;

	@JsonIgnore
	private Boolean reverseCharge = false;

	@JsonFormat(pattern = Transaction.jsonDateFormat)
	@JsonProperty("sbdt")
	private Date shippingBillDate;//// for gstr1 EXP

	@JsonProperty("sbnum")
	private String shippingBillNum;// for gstr1 EXP
	
	@JsonProperty("sbpcode")
	private String shippingBillPortCode;// for gstr1 EXP

	@JsonProperty("itms")
	private List<InvoiceItem> items;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;

	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")
	private Double taxAmount;
	
	@JsonProperty("inv_type")
	private String invoiceType;
	
	@JsonProperty("opd")
	private String orgMonthYear;
	
	@JsonGetter("rchrg")
	public String getRevCharge() {
		if (this.reverseCharge != null && this.reverseCharge)
			return "Y";
		else
			return "N";
	}

	@JsonSetter("rchrg")
	public void setRevCharge(String input) {
		if (input.equals("Yes"))
			this.reverseCharge = true;
		else
			this.reverseCharge = false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((supplierInvNum == null) ? 0 : supplierInvNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invoice other = (Invoice) obj;
		if (supplierInvNum == null) {
			if (other.supplierInvNum != null)
				return false;
		} else if (!supplierInvNum.equals(other.supplierInvNum))
			return false;
		return true;
	}
	
	

}
