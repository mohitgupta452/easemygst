package com.ginni.easemygst.portal.data.view.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

public class TXPDetailSerializer extends BaseDataDetailSerializer<TxpdTransaction> {

	//public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd MMM yyyy");


	@Override
	public void serialize(TxpdTransaction b2b, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		//json.writeStartArray();
		//for (B2BDetailEntity b2b : b2bs) {
			
			json.writeStartObject();
			
			json.writeStringField("sply_ty", String.valueOf(b2b.getSupplyType()));
			String pos=b2b.getPos();
			if(!StringUtils.isEmpty(pos)){
				if(pos.length()<2)
					pos="0"+pos;
			}
			json.writeStringField("pos", pos);
			json.writeStringField("state_name",b2b.getStateName());
			if(!Objects.isNull(b2b.getAdvanceAdjusted())){
			json.writeNumberField("advance_adjusted",b2b.getAdvanceAdjusted() );
			}else{
				json.writeNumberField("advance_adjusted",0.0 );

			}
			if(!Objects.isNull(b2b.getTaxAmount())){
				json.writeNumberField("tax_amt",b2b.getTaxAmount() );
				}else{
					json.writeNumberField("tax_amt",0.0 );

				}
			json.writeStringField("flags",b2b.getFlags() );
			json.writeStringField("type",b2b.getType() );
			json.writeStringField("source",b2b.getSource());
			json.writeStringField("invoice_id",b2b.getId() );
			
			json.writeBooleanField("isSynced",b2b.getIsSynced() );
			json.writeBooleanField("isTransit",b2b.getIsTransit() );
			json.writeBooleanField("toBeSynch",b2b.getToBeSync() );
			json.writeBooleanField("isError",b2b.getIsError() );
			json.writeStringField("errorMsg",b2b.getErrMsg() );
			
			if (Objects.nonNull(b2b) ) {
				if("ERROR".equalsIgnoreCase(b2b.getFlags())||b2b.getIsError())
					json.writeStringField("flags", "ERROR");
				else if(b2b.getIsSynced())
					json.writeStringField("flags", "SYNCED");
				else if(StringUtils.isEmpty(b2b.getFlags()))
					json.writeStringField("flags", "NEW");
					
				}
//ammendment
			json.writeBooleanField("isAmmendment",b2b.getIsAmmendment() );
			json.writeStringField("originalMonth",b2b.getOriginalMonth() );
			/*json.writeStringField("originalPos",b2b.getOriginalPos() );
			json.writeStringField("originalSupplyType",b2b.getOriginalSupplyType() );*/

			writeLineItems(json, b2b);
			json.writeEndObject();
		//}
	//	json.writeEndArray();

	}

	

	
	void writeLineItems( JsonGenerator json, TxpdTransaction inv) throws IOException {
		// Start Line Items
		json.writeFieldName("itms");
		json.writeStartArray();
		if(!Objects.isNull(inv.getItems()))
		inv.getItems().forEach(item -> writeLineItem(json, item));
		// End Line Items

		json.writeEndArray();
	}

	public void writeLineItem(JsonGenerator json,Item item) {
		try {
			json.writeStartObject();// start item
			
			json.writeStringField("hsn_sc", !StringUtils.isEmpty(item.getHsnCode())?item.getHsnCode():"");
			
			json.writeStringField("hsn_desc", !StringUtils.isEmpty(item.getHsnDescription())?item.getHsnDescription():"");
			
			json.writeStringField("unit", !StringUtils.isEmpty(item.getUnit())?item.getUnit():"");
			
			json.writeNumberField("quantity", item.getQuantity());

			json.writeNumberField("num", item.getSerialNumber());
			
			if (!Objects.isNull(item.getTaxableValue()))
				json.writeNumberField("ad_amt", item.getTaxableValue());

			if (!Objects.isNull(item.getTaxRate()))
				json.writeNumberField("rt", item.getTaxRate());

			if (!Objects.isNull(item.getIgst()))
				json.writeNumberField("iamt", item.getIgst());

			if (!Objects.isNull(item.getCgst()))
				json.writeNumberField("camt", item.getCgst());

			if (!Objects.isNull(item.getSgst()))
				json.writeNumberField("samt", item.getSgst());

			if (!Objects.isNull(item.getCess()))
				json.writeNumberField("csamt", item.getCess());
			//json.writeEndObject();// end item detail
			json.writeEndObject();// end item
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
}
