package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.transaction.REVCH;

public class REVCHDeserializer extends BaseDataDeserializer<List<REVCH>> {

	@Override
	public List<REVCH> deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<REVCH> impss = new ArrayList<>();
		Iterator<JsonNode> impsNodes = node.elements();
		while (impsNodes.hasNext()) {
			JsonNode impsNode = impsNodes.next();
			REVCH imps = new REVCH();

			impss.add(imps);
		}

		return impss;

	}

	public List<InvoiceItem> deserializeItem(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<InvoiceItem> itms = new ArrayList<>();
		while (items.hasNext()) {
			InvoiceItem item = new InvoiceItem();
			JsonNode itemNode = items.next();

			if (itemNode.has("num"))
				item.setSNo(itemNode.get("num").asInt());

			if (itemNode.has("hsn_sc"))
				item.setGoodsOrServiceCode(itemNode.get("hsn_sc").asText());
			if (itemNode.has("txval"))
				item.setTaxableValue(itemNode.get("txval").asDouble());
			if (itemNode.has("irt"))
				item.setTaxRate(itemNode.get("trt").asDouble());
			if (itemNode.has("iamt"))
				item.setIgstAmt(itemNode.get("iamt").asDouble());
			if (itemNode.has("csamt"))
				item.setCessAmt(itemNode.get("csamt").asDouble());

			if (itemNode.has("elg")) {
				item.setEligOfTotalTax(itemNode.get("elg").asText());
				item.setItcDetails(this.deserializeItcDetail(itemNode, item));

			}

			itms.add(item);
		}
		return itms;
	}

	public ItcDetail deserializeItcDetail(JsonNode itemNode, InvoiceItem invItem) {
		JsonNode itcNode = itemNode;
		ItcDetail itcDetail = new ItcDetail();
		/*
		 * if (itemNode.has("elg"))
		 * invItem.setEligOfTotalTax(itcNode.get("elg").asText());
		 */
		if (!Objects.isNull(itcNode)) {
			if (itemNode.has("tx_i"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_i").asDouble());
			if (itemNode.has("tx_cs"))
				itcDetail.setTotalTaxAvalIgst(itcNode.get("tx_cs").asDouble());

		}
		return itcDetail;
	}

}
