package com.ginni.easemygst.portal.data.deserialize;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.CDN.CdnData;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.impl.CDNTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNURTransaction;

public class CDNURDeserializer extends BaseDataDeserializer<List<CDNURDetailEntity>> {

	@Override
	public List<CDNURDetailEntity> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		List<CDNURDetailEntity> cdns = new ArrayList<>();
		Iterator<JsonNode> cdnNodes = node.elements();
		try{
		while (cdnNodes.hasNext()) {
			JsonNode cdnNode = cdnNodes.next();
			CDNURDetailEntity inv = new CDNURDetailEntity();
			CDNURTransactionEntity cdnurTransactionEntity = new CDNURTransactionEntity();
			inv.setCdnUrTransaction(cdnurTransactionEntity);
			if (cdnNode.has("ctin"))
				cdnurTransactionEntity.setOriginalCtin(cdnNode.get("ctin").asText());
//			if (cdnNode.has("cfs"))
//				cdn.setFillingStatus(cdnNode.get("cfs").asText());
			if(cdnNode.has("error_msg"))
				inv.setError_msg(cdnNode.get("error_msg").asText());
				//cdn.setCdnDetails(this.deserializeCdnData(cdnNode));
			//cdn.setCdnDatas(this.deserializeCdnData(cdnNode));
			
			
//			Iterator<JsonNode> invoices = node.get("nt").elements();
//			try{
//			while (invoices.hasNext()) {
				JsonNode invNode =cdnNode;
//				if(invNode.has("chksum"))
//					inv.setCheckSum(invNode.get("chksum").asText());
				if(invNode.has("cflag"))
					inv.setFlags(invNode.get("cflag").asText());
				if(invNode.has("updby"))
					inv.setUpdatedBy(invNode.get("updby").asText());
				if(invNode.has("ntty"))
					inv.setNoteType(invNode.get("ntty").asText());
				if(invNode.has("nt_num"))
					inv.setRevisedInvNo(invNode.get("nt_num").asText());
				if(invNode.has("nt_dt"))
					inv.setRevisedInvDate(gstFmt.parse(invNode.get("nt_dt").asText()));
				if(invNode.has("rsn"))
					inv.setReasonForNote(invNode.get("rsn").asText());
				if(invNode.has("p_gst"))
					inv.setPreGstRegime(invNode.get("p_gst").asText());
				if(invNode.has("inum"))
					inv.setInvoiceNumber(invNode.get("inum").asText());
				if(invNode.has("idt"))
					inv.setInvoiceDate(gstFmt.parse(invNode.get("idt").asText()));
				if(invNode.has("val"))
					inv.setTaxableValue(invNode.get("val").asDouble());
				if(invNode.has("itms"))
				  inv.setItems(this.deserializCDNItemDetail(invNode));
				
				cdns.add(inv);
		}
		}catch(Exception exception){
			
		}
		return cdns;

	}

	public List<CDNDetailEntity> deserializeCdnData(JsonNode node) {

		Iterator<JsonNode> invoices = node.get("nt").elements();
		List<CDNDetailEntity> invs = new ArrayList<>();
		try{
		while (invoices.hasNext()) {
			JsonNode invNode = invoices.next();
			CDNDetailEntity inv = new CDNDetailEntity();
//			if(invNode.has("chksum"))
//				inv.setCheckSum(invNode.get("chksum").asText());
			if(invNode.has("cflag"))
				inv.setFlags(invNode.get("cflag").asText());
			if(invNode.has("updby"))
				inv.setUpdatedBy(invNode.get("updby").asText());
			if(invNode.has("ntty"))
				inv.setNoteType(invNode.get("ntty").asText());
			if(invNode.has("nt_num"))
				inv.setRevisedInvNo(invNode.get("nt_num").asText());
			if(invNode.has("nt_dt"))
				inv.setRevisedInvDate(gstFmt.parse(invNode.get("nt_dt").asText()));
			if(invNode.has("rsn"))
				inv.setReasonForNote(invNode.get("rsn").asText());
			if(invNode.has("p_gst"))
				inv.setPreGstRegime(invNode.get("p_gst").asText());
			if(invNode.has("inum"))
				inv.setInvoiceNumber(invNode.get("inum").asText());
			if(invNode.has("idt"))
				inv.setInvoiceDate(gstFmt.parse(invNode.get("idt").asText()));
			if(invNode.has("val"))
				inv.setTaxableValue(invNode.get("val").asDouble());
			
			inv.setItems(this.deserializCDNItemDetail(invNode));
			
			
//			if (invNode.has("flag")) {
//				String flag = invNode.get("flag").asText();
//				for (TaxpayerAction tpa : TaxpayerAction.values()) {
//					if (tpa.getValue().equalsIgnoreCase(flag)) {
//						inv.setTaxPayerAction(tpa);
//					}
//				}
//			}

//			if (invNode.has("cflag")) {
//				String flag = invNode.get("cflag").asText();
//
//				for (TaxpayerAction tpa : TaxpayerAction.values()) {
//					if (tpa.getValue().equalsIgnoreCase(flag)) {
//						//inv.setCPartyAction(tpa);
//
//					}
//				}
//
//			}
//			if (invNode.has("updby"))
//				inv.setUploadedBy(invNode.get("updby").asText());
//			if (invNode.has("chksum"))
//				inv.setCheckSum(invNode.get("chksum").asText());
//			if (invNode.has("ntty"))
//				inv.setCreditDebitType(invNode.get("ntty").asText());
//			if (invNode.has("nt_num"))
//				inv.setCreditDebitNum(invNode.get("nt_num").asText());
//
//			String ntDate = invNode.has("nt_dt") ? invNode.get("nt_dt").asText() : null;
//			if (!StringUtils.isEmpty(ntDate)) {
//				try {
//					inv.setCreditDebitDate(gstFmt.parse(ntDate));
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//
//			}
//
//			if (invNode.has("inum"))
//				inv.setOrigDebtCredtNoteNo(invNode.get("inum").asText());
//
//			String invDate = invNode.has("idt") ? invNode.get("idt").asText() : null;
//			if (!StringUtils.isEmpty(invDate)) {
//				try {
//					inv.setOrigDebtCredtDate(gstFmt.parse(invDate));
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//
//			}
//
//			if (invNode.has("pos"))
//				inv.setOrigDebtCredtNoteNo(invNode.get("pos").asText());

			/*if (invNode.has("rt"))
				inv.setTaxRate(invNode.get("rt").asDouble());
			if (invNode.has("iamt"))
				inv.setIgstAmt(invNode.get("iamt").asDouble());

			if (invNode.has("camt"))
				inv.setCgstAmt(invNode.get("camt").asDouble());

			if (invNode.has("samt"))
				inv.setSgstAmt(invNode.get("samt").asDouble());
			if (invNode.has("csamt"))
				inv.setCessAmt(invNode.get("csamt").asDouble());

			if (invNode.has("elg")) {
				inv.setEligOfTotalTax(invNode.get("itc").get("elg").asText());
				inv.setItcDetails(this.deserializeItcDetail(invNode));

			}*/

			invs.add(inv);

		}
	  } catch (Exception e) {
			e.printStackTrace();
	   }
		return invs;
	}

	public List<Item> deserializCDNItemDetail(JsonNode invNode) {
		Iterator<JsonNode> items = invNode.get("itms").elements();
		List<Item> itms = new ArrayList<>();
		
		while (items.hasNext()) {
			Item item = new Item();
			JsonNode itemNode = items.next();
			if (itemNode.has("num"))
				item.setSerialNumber(itemNode.get("num").asInt());
			JsonNode itemDetNode=null;
			if(itemNode.has("itm_det")){
			   itemDetNode = itemNode.get("itm_det");
			   if(itemDetNode!=null){
				   if (itemDetNode.has("txval"))
						item.setTaxableValue(itemDetNode.get("txval").asDouble());
					if (itemDetNode.has("rt"))
						item.setTaxRate(itemDetNode.get("rt").asDouble());
					if (itemDetNode.has("iamt"))
						item.setIgst(itemDetNode.get("iamt").asDouble());
					if (itemDetNode.has("camt"))
						item.setCgst(itemDetNode.get("camt").asDouble());
					if (itemDetNode.has("samt"))
						item.setSgst(itemDetNode.get("samt").asDouble());
					if (itemDetNode.has("csamt"))
						item.setCess(itemDetNode.get("csamt").asDouble());
			   }
			}
			else if(itemNode.has("itc")){
				itemDetNode=itemNode.get("itc");
				if(itemDetNode.has("elg"))
					item.setTotalEligibleTax(itemDetNode.get("elg").asText());
				if(itemDetNode.has("tx_cs"))
					item.setItcCess(itemDetNode.get("tx_cs").asDouble());
				if(itemDetNode.has("tx_i"))
					item.setIgst(itemDetNode.get("tx_i").asDouble());
				if(itemDetNode.has("tx_c"))
					item.setCgst(itemDetNode.get("tx_c").asDouble());
				if(itemDetNode.has("tx_s"))
					item.setItcSgst(itemDetNode.get("tx_s").asDouble());
				
			}
			
			itms.add(item);
			}
			
		return itms;
	}
}
