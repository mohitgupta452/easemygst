package com.ginni.easemygst.portal.data.transaction;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.BaseData;

import lombok.Data;

public @Data class B2CL extends BaseData {
	
	@JsonProperty("bId")
	String id;

	@JsonProperty("state_cd")
	private Integer stateCode;
	
	@JsonProperty("state_name")
	private String stateName;
	
	//@JsonFormat(pattern="#0.00")
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("taxable_value")
	private Double taxableValue;
	
	@JsonFormat(pattern = "#0.00")
	@JsonProperty("tax_amount")
	private Double taxAmount;
	
	@Override
	public boolean equals(Object obj) {     
	   EqualsBuilder builder = new EqualsBuilder().append(this.getStateCode(), ((B2CL) obj).getStateCode());               
	   return builder.isEquals();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stateCode == null) ? 0 : stateCode.hashCode());
		return result;
	}
	
}
