package com.ginni.easemygst.portal.data.serialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ginni.easemygst.portal.business.dto.DocIssueDTO;
import com.ginni.easemygst.portal.business.dto.DocIssueItemDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;

public class DocIssueSerializer extends BaseDataSerializer<List<DocIssue>> {

	private ReturnType returnType = ReturnType.GSTR1;

	private TransactionType transType = TransactionType.AT;

	static final String[] doclist = { "INVOICES FOR OUTWARD SUPPLY",
			"INVOICES FOR INWARD SUPPLY FROM UNREGISTERED PERSON", "REVISED INVOICE", "DEBIT NOTE", "CREDIT NOTE",
			"RECEIPT VOUCHER", "PAYMENT VOUCHER", "REFUND VOUCHER", "DELIVERY CHALLAN FOR JOB WORK",
			"DELIVERY CHALLAN FOR SUPPLY ON APPROVAL", "DELIVERY CHALLAN IN CASE OF LIQUID GAS",
			"DELIVERY CHALLAN IN CASES OTHER THAN BY WAY OF SUPPLY" };

	@Override
	public void serialize(List<DocIssue> docs, final JsonGenerator json, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {

		json.writeStartObject();

		if (docs != null) {
			if (!docs.isEmpty()) {
				returnType = ReturnType.valueOf(docs.get(0).getReturnType().toUpperCase());
				if (!Objects.isNull(docs.get(0).getFlags()) && !StringUtils.isEmpty(docs.get(0).getFlags()))
					json.writeStringField("flag",
							docs.get(0).getFlags() != null ? this.matchFlag(docs.get(0).getFlags()) : null);
				else {
					json.writeFieldName("doc_det");
					json.writeStartArray();
					
					int i = 1;
					List<DocIssueDTO> docIssueDTOs = new ArrayList<>();
					for (DocIssue doc : docs) {

						if (Arrays.asList(doclist).indexOf(StringUtils.upperCase(doc.getCategory())) > -1) {

							DocIssueDTO docIssueDTO = new DocIssueDTO();
							docIssueDTO.setInum(Arrays.asList(doclist).indexOf(StringUtils.upperCase(doc.getCategory())) + 1);
							List<DocIssueItemDTO> docIssueItemDTOs = new ArrayList<>();
							if (docIssueDTOs.contains(docIssueDTO)) {
								docIssueItemDTOs = docIssueDTOs.get(docIssueDTOs.indexOf(docIssueDTO)).getItems();
							} else {
								docIssueDTOs.add(docIssueDTO);
								docIssueDTO.setItems(docIssueItemDTOs);
							}

							DocIssueItemDTO docIssueItemDTO = new DocIssueItemDTO();
							docIssueItemDTO.setCanceled(doc.getCanceled());
							docIssueItemDTO.setNetIssued(doc.getNetIssued());
							docIssueItemDTO.setSnoFrom(doc.getSnoFrom());
							docIssueItemDTO.setSnoTo(doc.getSnoTo());
							docIssueItemDTO.setTotalNumber(doc.getTotalNumber());

							docIssueItemDTOs.add(docIssueItemDTO);

							/*
							 * json.writeNumberField("doc_num",
							 * Arrays.asList(doclist).indexOf(doc.getCategory())
							 * +1); json.writeFieldName("docs");
							 * json.writeStartArray(); json.writeStartObject();
							 * json.writeNumberField("num", i++);
							 * json.writeStringField("from", doc.getSnoFrom());
							 * json.writeStringField("to", doc.getSnoTo());
							 * json.writeNumberField("totnum",
							 * doc.getTotalNumber());
							 * json.writeNumberField("cancel",
							 * doc.getCanceled());
							 * json.writeNumberField("net_issue",
							 * doc.getNetIssued()); json.writeEndObject();
							 * json.writeEndArray();
							 */
						}
					}

					for (DocIssueDTO docIssueDTO : docIssueDTOs) {

						json.writeStartObject();
						json.writeNumberField("doc_num", docIssueDTO.getInum());
						json.writeFieldName("docs");
						json.writeStartArray();

						for (DocIssueItemDTO docIssueItemDTO : docIssueDTO.getItems()) {
							json.writeStartObject();
							json.writeNumberField("num", i++);
							json.writeStringField("from", docIssueItemDTO.getSnoFrom().trim());
							json.writeStringField("to", docIssueItemDTO.getSnoTo().trim());
							json.writeNumberField("totnum", docIssueItemDTO.getTotalNumber());
							json.writeNumberField("cancel", docIssueItemDTO.getCanceled());
							json.writeNumberField("net_issue", docIssueItemDTO.getNetIssued());
							json.writeEndObject();
						}

						json.writeEndArray();
						json.writeEndObject();
					}
					
					json.writeEndArray();
				}
			}

		}
	}

	void writeLineDocs(JsonGenerator json, List<Item> items) throws IOException {
		json.writeFieldName("docs");
		json.writeStartArray();
		items.forEach(item -> writeLineItem(json, item));
		json.writeEndArray();
	}

	private void writeLineItem(JsonGenerator json, Item item) {
		try {
			json.writeStartObject();// start item

			// json.writeStringField("hsn_sc", item.getGoodsOrServiceCode());
			// if (item.getTaxableValue())
			json.writeNumberField("ad_amt", item.getTaxableValue());
			// if (item.getTaxRate() != null)
			json.writeNumberField("rt", item.getTaxRate());
			// if (item.getIgst() != null)
			json.writeNumberField("iamt", item.getIgst());
			// if (item.getCgstAmt() != null)
			// json.writeNumberField("camt", item.getCgstAmt());
			// if (item.getSgstAmt() != null)
			// json.writeNumberField("samt", item.getSgstAmt());
			// if (item.getCess() != null)
			json.writeNumberField("csamt", item.getCess());
			json.writeEndObject();// end item
		} catch (Exception e) {
			/// log
			e.printStackTrace();
		}

	}

}
