package com.ginni.easemygst.portal.data.serialize;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class BaseDataSerializer<T> extends JsonSerializer<T> {

	public final SimpleDateFormat gstFmt = new SimpleDateFormat("dd-MM-yyyy");
	
	protected final static ObjectMapper _MAPPER =new ObjectMapper();

	protected String matchFlag(String flag){
		
		if("ACCEPTED".equalsIgnoreCase(flag))
			flag="A";
		else if("PENDING".equalsIgnoreCase(flag))
			flag="P";
		else if("REJECTED".equalsIgnoreCase(flag))
			flag="R";
		else if("DELETE".equalsIgnoreCase(flag))
			flag="D";
		else if("MODIFIED".equalsIgnoreCase(flag))
			flag="M";
		else flag=null;
	
	return flag;
}

}
