package com.ginni.easemygst.portal.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

public class RestClient {

	protected static Logger log = LoggerFactory.getLogger(RestClient.class);

	private static OutputStreamWriter out;
	
	private static final long _UNIREST_CONNECTION_TIMEOUT=600000;
	
	{
		Unirest.setTimeouts(_UNIREST_CONNECTION_TIMEOUT, _UNIREST_CONNECTION_TIMEOUT);
	}

	@SuppressWarnings("finally")
	public static String call(String endpoint, String input, Map<String, String> headers) {
		

		log.info("call to {} initiated with input string {}", endpoint, input);
		String output = "";

		try {
			URL url = new URL(endpoint);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);

			log.info("loading headers for the call");
			// load Headers for the call
			for (Map.Entry<String, String> entry : headers.entrySet()) {
				connection.setRequestProperty(entry.getKey(), entry.getValue());
			}

			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);

			// Load string object to get JSON object
			if (input != null) {
				// pass JSON Data to REST Service
				out = new OutputStreamWriter(connection.getOutputStream());
				out.write(input);
				out.close();
			}

			log.info("getting output from the call");
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				output += line + "\n";
			}
			in.close();

			log.info("call output received {}", output);
			return output;
		} catch (Exception e) {
			log.error("Error while rest client error message {}",endpoint, e);
		} finally {
			return output;
		}

	}
	
	@SuppressWarnings("finally")
	public static String call(String endpoint, JsonNode jsonNode, Map<String, String> headers) {

		log.info("call to {} initiated with input string {}", endpoint, jsonNode.getObject().toString());
		String output = "{\"status_cd\":\"0\"}";

		/*
		 * String certificatesTrustStorePath =
		 * "/Library/Java/JavaVirtualMachines/jdk1.8.0_25.jdk/Contents/Home/jre/lib/security/cacerts";
		 * System.setProperty("javax.net.ssl.trustStore",
		 * certificatesTrustStorePath);
		 * 
		 */
		try {
			if (jsonNode.getObject().has("action")) {
				String action = jsonNode.getObject().getString("action");
				String[] temp = action.split("_");
				if(!StringUtils.isEmpty(temp[0]))
				jsonNode.getObject().put("action", temp[0]);
				if ((AppConfig.getActiveProfile().contains("production")
						|| AppConfig.getActiveProfile().contains("quality")) && endpoint.contains("authenticate")) {
					endpoint = endpoint.replace("[VERSION]", "v1.0");
				} else {
					if(action.contains("RETTRACK"))
						endpoint=endpoint+"gstr/";
					endpoint = endpoint.replace("[VERSION]", temp[2]);
				}
				HttpResponse<String> strResponse=null;
				
				log.debug("request {}   headers {}   url {}",jsonNode,headers,endpoint);

				if (action.contains("POST")) {
					HttpRequestWithBody body = Unirest.post(endpoint);
					body.headers(headers);
					body.body(jsonNode.getObject());
					strResponse = body.asString();
					} else if (action.contains("GET")) {
					GetRequest body = Unirest.get(endpoint);
					//if(!action.contains("TP"))
					body.headers(headers);
					body.queryString(jsonNode.getObject().toMap());
					strResponse = body.asString();
				} 
				else if (action.contains("PUT")) {
					HttpRequestWithBody body = Unirest.put(endpoint);
					body.headers(headers);
					body.body(jsonNode);
					strResponse = body.asString();
				}else{
					log.debug("no action found");
				}
				
				log.debug("rsponse {}",strResponse);
				log.debug("Status Code {} ", strResponse.getStatus()+"");
				log.debug("Status Msg {} ", strResponse.getStatusText());
				log.debug("Response Msg {} ", strResponse.getBody());

				if (strResponse.getStatus() == 200)
					output = strResponse.getBody();
				else
					log.error("error while calling api {}", endpoint);

			}
		}
		catch (UnirestException e) {
			log.error("Unirest Exception while calling api {}", e);
			if(isRetry(e))
				output=call(endpoint,jsonNode,headers);
		} catch (Exception e) {
			log.error("Exception while calling api {}", e);
		} 
		
		finally {
			log.info("call output received {}", output);
			return output;
		}
	}

	@SuppressWarnings("finally")
	public static String callApi(String endpoint, String method, JsonNode jsonNode, Map<String, String> headers) {

		log.info("call to {} initiated with input string {}", endpoint, headers);
		String output = "{\"status_cd\":\"0\"}";
		String outputFlag = "{\"flag\":\"1\"}";

		try {
			HttpResponse<JsonNode> response = null;
			if (method.contains("POST_ESIGN")) {
				HttpRequestWithBody body = Unirest.post(endpoint);
				body.headers(headers);
				body.body(jsonNode.getObject());
				response = body.asJson();
			} if (method.contains("POST")) {
				HttpRequestWithBody body = Unirest.post(endpoint);
				body.headers(headers);
				body.body(jsonNode);
				response = body.asJson();
			} else if (method.contains("GET")) {
				GetRequest body = Unirest.get(endpoint);
				body.headers(headers);
				body.queryString(jsonNode.getObject().toMap());
				response = body.asJson();
			} else if (method.contains("PUT")) {
				HttpRequestWithBody body = Unirest.put(endpoint);
				body.headers(headers);
				body.body(jsonNode);
				response = body.asJson();
			} else if (method.contains("PATCH")) {
				HttpRequestWithBody body = Unirest.patch(endpoint);
				body.headers(headers);
				body.body(jsonNode);
				response = body.asJson();
			}

			if (response.getStatus() == 200)
				output = response.getBody().toString();
			else if (response.getStatus() == 201)
				output = response.getBody().toString();
			else if (response.getStatus() == 204)
				output = outputFlag;
			else
				log.error("error while calling api {}", endpoint);

			// Unirest.shutdown();
		} catch (UnirestException e) {
			log.error("Exception while calling api {}",e);
			if(isRetry(e))
				output=callApi(endpoint,method,jsonNode,headers);
		} finally {
			log.info("call output received {}", output);
			return output;
		}
	}

	@SuppressWarnings("finally")
	public static String callZohoApi(String endpoint, String method, JsonNode jsonNode, Map<String, String> headers) {

		log.info("call to {} initiated with input string {}", endpoint, headers);
		String output = "{\"status_cd\":\"0\"}";

		try {
			HttpResponse<String> response = null;
			if (method.contains("POSTP")) {
				HttpRequestWithBody body = Unirest.post(endpoint);
				body.headers(headers);
				body.body(jsonNode);
				response = body.asString();
			} else if (method.contains("POST")) {
				HttpRequestWithBody body = Unirest.post(endpoint);
				body.headers(headers);
				body.queryString(jsonNode.getObject().toMap());
				response = body.asString();
			} else if (method.contains("GET")) {
				GetRequest body = Unirest.get(endpoint);
				body.headers(headers);
				body.queryString(jsonNode.getObject().toMap());
				response = body.asString();
			} else if (method.contains("PUT")) {
				HttpRequestWithBody body = Unirest.put(endpoint);
				body.headers(headers);
				body.body(jsonNode);
				response = body.asString();
			}

			if (response.getStatus() == 200)
				output = response.getBody().toString();
			else
				log.error("error while calling api {}", endpoint);

		} catch (UnirestException e) {
			log.error("Exception while calling api {}", endpoint);
			e.printStackTrace();
		} catch (Exception e) {
			log.error("Exception while calling api {}", endpoint);
			e.printStackTrace();
		} finally {
			log.info("call output received {}", output);
			return output;
		}
	}
	
	
	@SuppressWarnings("finally")
	public static InputStream callFileDownload(String endpoint, JsonNode jsonNode, Map<String, String> headers) {

		log.info("call to {} initiated with input string {}", endpoint, jsonNode.getObject().toString());
		InputStream output =null;

		/*
		 * String certificatesTrustStorePath =
		 * "/Library/Java/JavaVirtualMachines/jdk1.8.0_25.jdk/Contents/Home/jre/lib/security/cacerts";
		 * System.setProperty("javax.net.ssl.trustStore",
		 * certificatesTrustStorePath);
		 */
		try {
					HttpClient client = new DefaultHttpClient();
					HttpGet request = new HttpGet(endpoint+jsonNode.getObject().toMap().get("endpoint"));
					for(Entry<String, String> en:headers.entrySet()){
						request.setHeader(en.getKey(), en.getValue());
					}
					org.apache.http.HttpResponse respons = client.execute(request);
				
				log.debug("Status Code {} ", respons.getStatusLine().getStatusCode()+"");

				if (respons.getStatusLine().getStatusCode()== 200)
					output = respons.getEntity().getContent();
				else
					log.error("error while calling api {}", endpoint);

		} catch (Exception e) {
			log.error("Exception while calling api {}", endpoint);
			e.printStackTrace();
		} finally {
			log.info("call output received {}", output);
			return output;
		}
	}
	
	public static String getCall(String url) throws AppException {
		
		GetRequest body = Unirest.get(url);
		try {
			String ip= body.asString().getBody();
			log.info("server ip address{}",ip);
			return ip;
		} catch (UnirestException e) {
			log.error("unirest exception ",e);
			throw new AppException(e);
		}
		
	}
	
	
	private static boolean isRetry(UnirestException unirestException) {
		
		if(unirestException.getCause().getCause() instanceof SocketTimeoutException) {
			
			return true;
			
		}
		return false;
		
	}

}
