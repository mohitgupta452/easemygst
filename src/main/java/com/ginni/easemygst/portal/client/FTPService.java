package com.ginni.easemygst.portal.client;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.utils.ManipulateFile;

public class FTPService {

	private final static Logger log = org.slf4j.LoggerFactory.getLogger(FTPService.class);
	
	public FTPClient connect(String hostname, String username, String password) {
		FTPClient client = new FTPClient();
		try {
			client.connect(hostname);
			
			   client.enterLocalPassiveMode();

			if(client.login(username,password))
				log.debug("ftp user logged in ....");
			
			log.debug("reply code " +client.getReplyCode()+" reply string "+client.getReplyString());
			
			if (client.getReplyCode() >= 200 && client.getReplyCode() <= 299) {
				return client;
			} else {
				return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public String readFile(String hostname, String username, String password, String fileName) {

		FTPClient client = new FTPClient();
		FileOutputStream fos = null;
		try {
			client = connect(hostname, username, password);
			
          
			String filePath=ManipulateFile.getParentDirBasePath("ftp")+fileName;
		
			// The remote filename to be downloaded   .
			fos = new FileOutputStream(filePath);
			
			
			log.debug("current working directory "+client.printWorkingDirectory());

			log.debug( "file list length  "+client.listFiles().length);
			
			// Download file from FTP  server
		
			if(client.retrieveFile(fileName, fos))
			{
				log.debug("download success");
			return filePath;
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				
				  if (fos != null) { fos.close(); }
				  
				  client.logout();
				client.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void main(String args[]) {

		FTPService ftpService = new FTPService();
		ftpService.readFile("", "", "","");
		/*
		 * CSVParser csvParser = new CSVParser(); String fileName =
		 * "M:/b2cl_template - b2cl.csv"; File file = new File(fileName); try {
		 * List<List<String>> csvValues = csvParser.readFile(file);
		 * System.out.println(csvValues.toString()); } catch
		 * (FileNotFoundException e) { e.printStackTrace(); }
		 */
	}

}