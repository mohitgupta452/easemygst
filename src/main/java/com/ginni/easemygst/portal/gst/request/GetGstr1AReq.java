package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetGstr1AReq {

	@JsonProperty("action")
	private String action;

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ctin")
	private String ctin;
	
	@JsonProperty("from_time")
	private String fromTime;
	
	@JsonProperty("ret_period")
	private String monthYear;

	@JsonProperty("state_cd")
	private String stateCode;
	
	@JsonIgnore
	private ReturnType returnType;
	
}
