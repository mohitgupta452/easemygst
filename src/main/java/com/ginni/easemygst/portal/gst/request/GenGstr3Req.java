package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class GenGstr3Req {
	
	@JsonProperty("gstin")
	private String gstin;

	@JsonProperty("ret_period")
	private String monthYear;
	
	@JsonProperty("action")
	private String action;

}
