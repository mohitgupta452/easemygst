package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SaveGstr1Req {

	@JsonIgnore
	private String gstin;
	
	@JsonIgnore
	private String monthYear;
	
	@JsonProperty("action")
	private String action;
	
	@JsonProperty("data")
	private String data;
	
}
