
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ty",
    "inter",
    "intra"
})
public class IsupDetail {

    @JsonProperty("ty")
    private String ty;
    @JsonProperty("inter")
    private Double inter;
    @JsonProperty("intra")
    private Double intra;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ty")
    public String getTy() {
        return ty;
    }

    @JsonProperty("ty")
    public void setTy(String ty) {
        this.ty = ty;
    }

    @JsonProperty("inter")
    public Double getInter() {
        return inter;
    }

    @JsonProperty("inter")
    public void setInter(Double inter) {
        this.inter = inter;
    }

    @JsonProperty("intra")
    public Double getIntra() {
        return intra;
    }

    @JsonProperty("intra")
    public void setIntra(Double intra) {
        this.intra = intra;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
