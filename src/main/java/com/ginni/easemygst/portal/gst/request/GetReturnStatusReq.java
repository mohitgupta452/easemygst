package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetReturnStatusReq {

	@JsonProperty("action")
	private String action;

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String monthYear;
	
	@JsonProperty("trans_id")
	private String transId;

	@JsonProperty("ref_id")
	private String ref_id;
	
}
