package com.ginni.easemygst.portal.gst.response;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.data.BaseSummaryTotal;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class SectionSummary extends BaseSummaryTotal {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("sec_nm")
	private String sectionName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("chksum")
	private String checksum;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private double taxAmount;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private double taxableValue;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("cpty_sum")
	private List<CPartySummary> cpsummaries;
}
