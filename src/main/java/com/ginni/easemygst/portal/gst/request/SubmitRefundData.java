package com.ginni.easemygst.portal.gst.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.data.part.Gstr3Summary;
import com.ginni.easemygst.portal.data.part.Gstr3Summary.Gstr3TransactionSummary;

import lombok.Data;

public @Data class SubmitRefundData {
	
	@JsonProperty("refclm")
	@JsonInclude(Include.NON_NULL)
	private RefundClaim refundClaim;
	
	@JsonProperty("gstin")
	@JsonInclude(Include.NON_NULL)
	private String gstin;
	
	@JsonProperty("ret_period")
	@JsonInclude(Include.NON_NULL)
	private String monthYear;
	
	@JsonProperty("action")
	@JsonInclude(Include.NON_NULL)
	private String action;
	
	@Data  public class RefundClaim{
		
		@JsonProperty("igrfclm")
		@JsonInclude(Include.NON_NULL)
		private Claim igrClaim;
		
		@JsonProperty("cgrfclm")
		@JsonInclude(Include.NON_NULL)
		private Claim cgrClaim;
		
		@JsonProperty("sgrfclm")
		@JsonInclude(Include.NON_NULL)
		private Claim sgrClaim;
		
		@JsonProperty("csrfclm")
		@JsonInclude(Include.NON_NULL)
		private Claim csrClaim;
		
		@JsonProperty("bankacc")
		private Integer accNo;
		
		
	}

	 @Data  public static class Claim {
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("tax")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal tax = new BigDecimal(0.00);

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("intr")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal intr = new BigDecimal(0.00);
	    
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("pen")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal pen = new BigDecimal(0.00);
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("fee")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal fee = new BigDecimal(0.00);
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("oth")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal oth = new BigDecimal(0.00);
		
	}
	
}



