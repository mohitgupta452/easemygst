package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import org.codehaus.jackson.map.deser.std.FromStringDeserializer.URLDeserializer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ginni.easemygst.portal.data.deserialize.URLDeserialize;

import lombok.Data;

public @Data class Get2AFileResp extends BaseResponse{
	
	@JsonDeserialize(using=URLDeserialize.class)
	@JsonProperty("urls")
	private List<FileUrlResp> urls;
	
	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonProperty("ek")
	private String ek;
	
	@JsonProperty("fc")
	private String fc;
	
//	@JsonProperty("error")
//	private String error;


}
