package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.Gstr1ErrorReport;

import lombok.Data;

public @Data class GetGstr1ReturnStatusResp {

	@JsonProperty("status_cd")
	private String statusCode;
	
	@JsonProperty("arn_no")
	private String acknowledgementNo;
	
	/*@JsonProperty("error_cd")
	private String errorCode;
	
	@JsonProperty("error_msg")
	private String errorMessage;*/
	
	@JsonProperty("error_report")
	private Gstr1ErrorReport errorReport;
	
}
