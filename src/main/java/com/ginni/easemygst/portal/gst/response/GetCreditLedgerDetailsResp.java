
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "itcLdgDtls",
    "provCrdBalList"
})
public class GetCreditLedgerDetailsResp {

    @JsonProperty("itcLdgDtls")
    private ItcLdgDtls itcLdgDtls;
    @JsonProperty("provCrdBalList")
    private ProvCrdBalList provCrdBalList;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("itcLdgDtls")
    public ItcLdgDtls getItcLdgDtls() {
        return itcLdgDtls;
    }

    @JsonProperty("itcLdgDtls")
    public void setItcLdgDtls(ItcLdgDtls itcLdgDtls) {
        this.itcLdgDtls = itcLdgDtls;
    }

    @JsonProperty("provCrdBalList")
    public ProvCrdBalList getProvCrdBalList() {
        return provCrdBalList;
    }

    @JsonProperty("provCrdBalList")
    public void setProvCrdBalList(ProvCrdBalList provCrdBalList) {
        this.provCrdBalList = provCrdBalList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
