
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dt",
    "desc",
    "ref_no",
    "ret_period",
    "sgstTaxAmt",
    "cgstTaxAmt",
    "igstTaxAmt",
    "cessTaxAmt",
    "igstTaxBal",
    "cgstTaxBal",
    "sgstTaxBal",
    "cessTaxBal",
    "tot_rng_bal",
    "tot_tr_amt",
    "tr_typ"
})
public class Tr {

    @JsonProperty("dt")
    private String dt;
    @JsonProperty("desc")
    private String desc;
    @JsonProperty("ref_no")
    private String refNo;
    @JsonProperty("ret_period")
    private String retPeriod;
    @JsonProperty("sgstTaxAmt")
    private Integer sgstTaxAmt;
    @JsonProperty("cgstTaxAmt")
    private Integer cgstTaxAmt;
    @JsonProperty("igstTaxAmt")
    private Integer igstTaxAmt;
    @JsonProperty("cessTaxAmt")
    private Integer cessTaxAmt;
    @JsonProperty("igstTaxBal")
    private Integer igstTaxBal;
    @JsonProperty("cgstTaxBal")
    private Integer cgstTaxBal;
    @JsonProperty("sgstTaxBal")
    private Integer sgstTaxBal;
    @JsonProperty("cessTaxBal")
    private Integer cessTaxBal;
    @JsonProperty("tot_rng_bal")
    private Integer totRngBal;
    @JsonProperty("tot_tr_amt")
    private Integer totTrAmt;
    @JsonProperty("tr_typ")
    private String trTyp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("dt")
    public String getDt() {
        return dt;
    }

    @JsonProperty("dt")
    public void setDt(String dt) {
        this.dt = dt;
    }

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @JsonProperty("ref_no")
    public String getRefNo() {
        return refNo;
    }

    @JsonProperty("ref_no")
    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @JsonProperty("ret_period")
    public String getRetPeriod() {
        return retPeriod;
    }

    @JsonProperty("ret_period")
    public void setRetPeriod(String retPeriod) {
        this.retPeriod = retPeriod;
    }

    @JsonProperty("sgstTaxAmt")
    public Integer getSgstTaxAmt() {
        return sgstTaxAmt;
    }

    @JsonProperty("sgstTaxAmt")
    public void setSgstTaxAmt(Integer sgstTaxAmt) {
        this.sgstTaxAmt = sgstTaxAmt;
    }

    @JsonProperty("cgstTaxAmt")
    public Integer getCgstTaxAmt() {
        return cgstTaxAmt;
    }

    @JsonProperty("cgstTaxAmt")
    public void setCgstTaxAmt(Integer cgstTaxAmt) {
        this.cgstTaxAmt = cgstTaxAmt;
    }

    @JsonProperty("igstTaxAmt")
    public Integer getIgstTaxAmt() {
        return igstTaxAmt;
    }

    @JsonProperty("igstTaxAmt")
    public void setIgstTaxAmt(Integer igstTaxAmt) {
        this.igstTaxAmt = igstTaxAmt;
    }

    @JsonProperty("cessTaxAmt")
    public Integer getCessTaxAmt() {
        return cessTaxAmt;
    }

    @JsonProperty("cessTaxAmt")
    public void setCessTaxAmt(Integer cessTaxAmt) {
        this.cessTaxAmt = cessTaxAmt;
    }

    @JsonProperty("igstTaxBal")
    public Integer getIgstTaxBal() {
        return igstTaxBal;
    }

    @JsonProperty("igstTaxBal")
    public void setIgstTaxBal(Integer igstTaxBal) {
        this.igstTaxBal = igstTaxBal;
    }

    @JsonProperty("cgstTaxBal")
    public Integer getCgstTaxBal() {
        return cgstTaxBal;
    }

    @JsonProperty("cgstTaxBal")
    public void setCgstTaxBal(Integer cgstTaxBal) {
        this.cgstTaxBal = cgstTaxBal;
    }

    @JsonProperty("sgstTaxBal")
    public Integer getSgstTaxBal() {
        return sgstTaxBal;
    }

    @JsonProperty("sgstTaxBal")
    public void setSgstTaxBal(Integer sgstTaxBal) {
        this.sgstTaxBal = sgstTaxBal;
    }

    @JsonProperty("cessTaxBal")
    public Integer getCessTaxBal() {
        return cessTaxBal;
    }

    @JsonProperty("cessTaxBal")
    public void setCessTaxBal(Integer cessTaxBal) {
        this.cessTaxBal = cessTaxBal;
    }

    @JsonProperty("tot_rng_bal")
    public Integer getTotRngBal() {
        return totRngBal;
    }

    @JsonProperty("tot_rng_bal")
    public void setTotRngBal(Integer totRngBal) {
        this.totRngBal = totRngBal;
    }

    @JsonProperty("tot_tr_amt")
    public Integer getTotTrAmt() {
        return totTrAmt;
    }

    @JsonProperty("tot_tr_amt")
    public void setTotTrAmt(Integer totTrAmt) {
        this.totTrAmt = totTrAmt;
    }

    @JsonProperty("tr_typ")
    public String getTrTyp() {
        return trTyp;
    }

    @JsonProperty("tr_typ")
    public void setTrTyp(String trTyp) {
        this.trTyp = trTyp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
