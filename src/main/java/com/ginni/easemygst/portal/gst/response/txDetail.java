
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
public @Data class txDetail {

    @JsonProperty("tx")
    private Double tx;
    @JsonProperty("intr")
    private Double intr;
    @JsonProperty("fee")
    private Double fee;
    @JsonProperty("oth")
    private Double oth;
    @JsonProperty("pen")
    private Double pen;  
    
    
    
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



}
