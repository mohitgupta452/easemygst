package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.ATDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BReturnDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BURDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CLDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CSDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNReturnDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNURDeserializer;
import com.ginni.easemygst.portal.data.deserialize.EXPDeserializer;
import com.ginni.easemygst.portal.data.deserialize.HSNSUMDeserializer;
import com.ginni.easemygst.portal.data.deserialize.IMPGDeserializer;
import com.ginni.easemygst.portal.data.deserialize.IMPSBDeserializer;
import com.ginni.easemygst.portal.data.deserialize.NILDeserializer;
import com.ginni.easemygst.portal.data.deserialize.TXPDeserializer;
import com.ginni.easemygst.portal.data.serialize.ATSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BURSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CLSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CSSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNURSerializer;
import com.ginni.easemygst.portal.data.serialize.EXPSerializer;
import com.ginni.easemygst.portal.data.serialize.HSNSUMSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPGSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPSBSerializer;
import com.ginni.easemygst.portal.data.serialize.NILSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetGstr2ReturnResp {

	public GetGstr2ReturnResp() {
		this.statusCd = "0";
	}

	@JsonProperty("status_cd")
	private String statusCd;

	@JsonDeserialize(using=B2BReturnDeserializer.class)
	@JsonSerialize(using=B2BSerializer.class)
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("b2b")
	private List<B2BDetailEntity> b2b;


	@JsonProperty("cdn")
	@JsonDeserialize(using=CDNReturnDeserializer.class)
	@JsonSerialize(using=CDNSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<CDNDetailEntity> cdn;
	
	@JsonProperty("cdnur")
	@JsonDeserialize(using=CDNURDeserializer.class)
	@JsonSerialize(using=CDNURSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<CDNURDetailEntity> cdnur;

	@JsonProperty("txpd")
	@JsonDeserialize(using=TXPDeserializer.class)
	@JsonSerialize(using=TXPSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<TxpdTransaction> txpd;
	
	@JsonSetter("txpdoc")
	@JsonDeserialize(using=TXPDeserializer.class)
	@JsonSerialize(using=TXPSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private void setTxpError(List<TxpdTransaction> txpError){
		this.txpd=txpError;
	}
	
	
	@JsonProperty("b2bur")
	@JsonDeserialize(using=B2BURDeserializer.class)
	@JsonSerialize(using=B2BURSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<B2bUrTransactionEntity> b2bur;
	
	@JsonProperty("imp_g")
	@JsonDeserialize(using=IMPGDeserializer.class)
	@JsonSerialize(using=IMPGSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<IMPGTransactionEntity> impg;
	
	@JsonProperty("imp_s")
	@JsonDeserialize(using=IMPSBDeserializer.class)
	@JsonSerialize(using=IMPSBSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<IMPSTransactionEntity> imps;
	
	@JsonSetter("imps")
	@JsonDeserialize(using=IMPSBDeserializer.class)
	@JsonSerialize(using=IMPSBSerializer.class)
	@JsonInclude(Include.NON_NULL)
//	private List<IMPSTransactionEntity> impsGstr2;
	public void setImpsError(List<IMPSTransactionEntity> impsGstr2){
		this.imps=impsGstr2;
	}
	
	@JsonProperty("txi")
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonSerialize(using=ATSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<ATTransactionEntity> at;
	
	@JsonProperty("nil_supplies")
	@JsonDeserialize(using=NILDeserializer.class)
	@JsonSerialize(using=NILSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<NILTransactionEntity> nil;
	
	@JsonSetter("nldt")
	@JsonDeserialize(using=NILDeserializer.class)
	@JsonSerialize(using=NILSerializer.class)
	@JsonInclude(Include.NON_NULL)
	public void setNilError(List<NILTransactionEntity> nilError){
		this.nil=nilError;
	}
	
	@JsonProperty("hsnsum")
	@JsonDeserialize(using=HSNSUMDeserializer.class)
	@JsonSerialize(using=HSNSUMSerializer.class)
	@JsonInclude(Include.NON_NULL)
	private List<Hsn> hsn;
	
	@JsonProperty("error")
	@JsonInclude(Include.NON_NULL)
	private Error error;
	
	@JsonProperty("error_cd")
	@JsonInclude(Include.NON_NULL)
	private String error_cd;
	
	@JsonProperty("error_msg")
	@JsonInclude(Include.NON_NULL)
	private String error_msg;
	
	
}
