package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class OCBal {

	@JsonProperty("igstbal")
	private Taxbal igstbal;
	@JsonProperty("sgstbal")
	private Taxbal sgstbal;
	@JsonProperty("cessbal")
	private Taxbal cessbal;
	@JsonProperty("tot_rng_bal")
	private Integer totRngBal;
	@JsonProperty("cgstbal")
	private Taxbal cgstbal;
	@JsonProperty("desc")
	private String desc;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
}
