
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sgst_tot_bal",
    "sgst",
    "igst_tot_bal",
    "cgst",
    "cess",
    "cess_tot_bal",
    "igst",
    "cgst_tot_bal"
})
public @Data class CashBal {

    @JsonProperty("sgst_tot_bal")
    private Integer sgstTotBal;
    @JsonProperty("sgst")
    private txDetail sgst;
    @JsonProperty("igst_tot_bal")
    private Integer igstTotBal;
    @JsonProperty("cgst")
    private txDetail cgst;
    @JsonProperty("cess")
    private txDetail cess;
    @JsonProperty("cess_tot_bal")
    private Integer cessTotBal;
    @JsonProperty("igst")
    private txDetail igst;
    @JsonProperty("cgst_tot_bal")
    private Integer cgstTotBal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

   

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
