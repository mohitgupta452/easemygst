
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "gstin",
    "fr_dt",
    "to_dt",
    "op_bal",
    "tr",
    "cl_bal"
})
public class ItcLdgDtls {

    @JsonProperty("gstin")
    private String gstin;
    @JsonProperty("fr_dt")
    private String frDt;
    @JsonProperty("to_dt")
    private String toDt;
    @JsonProperty("op_bal")
    private OpBal opBal;
    @JsonProperty("tr")
    private List<Tr> tr = null;
    @JsonProperty("cl_bal")
    private ClBal clBal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("gstin")
    public String getGstin() {
        return gstin;
    }

    @JsonProperty("gstin")
    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    @JsonProperty("fr_dt")
    public String getFrDt() {
        return frDt;
    }

    @JsonProperty("fr_dt")
    public void setFrDt(String frDt) {
        this.frDt = frDt;
    }

    @JsonProperty("to_dt")
    public String getToDt() {
        return toDt;
    }

    @JsonProperty("to_dt")
    public void setToDt(String toDt) {
        this.toDt = toDt;
    }

    @JsonProperty("op_bal")
    public OpBal getOpBal() {
        return opBal;
    }

    @JsonProperty("op_bal")
    public void setOpBal(OpBal opBal) {
        this.opBal = opBal;
    }

    @JsonProperty("tr")
    public List<Tr> getTr() {
        return tr;
    }

    @JsonProperty("tr")
    public void setTr(List<Tr> tr) {
        this.tr = tr;
    }

    @JsonProperty("cl_bal")
    public ClBal getClBal() {
        return clBal;
    }

    @JsonProperty("cl_bal")
    public void setClBal(ClBal clBal) {
        this.clBal = clBal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
