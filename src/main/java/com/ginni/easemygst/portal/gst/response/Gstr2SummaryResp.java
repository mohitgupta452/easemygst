package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class Gstr2SummaryResp {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String returnPeriod;
	
	@JsonProperty("checksum")
	private String checksum;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("ttl_value")
	private Double totalValue;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("tax_pd")
	private Double taxPaid;
	
	@JsonFormat(pattern="#0.00")
	@JsonProperty("itc_av")
	private Double itcAvailable;
	
	@JsonProperty("sec_sum")
	private List<Section2Summary> secSummary;
	
	public @Data class Section2Summary {
		
		@JsonProperty("sec_nm")
		private String sectionName;
		
		@JsonProperty("checksum")
		private String checksum;
		
		@JsonFormat(pattern="#0.00")
		@JsonProperty("ttl_value")
		private Double totalValue;
		
		@JsonFormat(pattern="#0.00")
		@JsonProperty("tax_pd")
		private Double taxPaid;
		
		@JsonFormat(pattern="#0.00")
		@JsonProperty("itc_av")
		private Double itcAvailable;

		@JsonProperty("cpty_sum")
		private List<CParty2Summary> cpsummaries;
		
	}
	
	public @Data class CParty2Summary {
		
		@JsonProperty("ctin")
		private String ctin;
		
		@JsonProperty("checksum")
		private String checksum;
		
		@JsonFormat(pattern="#0.00")
		@JsonProperty("ttl_value")
		private Double totalValue;
		
		@JsonFormat(pattern="#0.00")
		@JsonProperty("tax_pd")
		private Double taxPaid;
		
		@JsonFormat(pattern="#0.00")
		@JsonProperty("itc_av")
		private Double itcAvailable;
		
	}

}
