package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.data.BaseSummaryTotal;

import lombok.Data;


@JsonInclude(Include.NON_NULL)
public @Data class Gstr1SummaryResp extends BaseResponse{
	
	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String returnPeriod;
	
	@JsonProperty("chksum")
	private String checksum;
	
	@JsonProperty("summ_typ")
	private String summaryType;
	
	@JsonProperty("sec_sum")
	@JsonInclude(Include.NON_NULL)
	private List<SectionSummary> secSummary;
		
}