package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SaveGstr1Resp extends BaseResponse {

	@JsonProperty("reference_id")
	private String referenceId;
	
	@JsonProperty("trans_id")
	private String transactionId;
	
	@JsonProperty("status_cd")
	private String status;
	
	@JsonProperty("error")
	private Error error;
}
