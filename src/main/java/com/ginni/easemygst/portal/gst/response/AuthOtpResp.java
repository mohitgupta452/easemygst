package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class AuthOtpResp {

	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonProperty("error")
	private Error error;
	
}
