package com.ginni.easemygst.portal.gst.request;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

public @Data class ViewTrackReturnsReq {
	
	@JsonProperty("action")
	private String action;
	
	private String gstin;
	
	@JsonProperty("ret_period")
	private String returnPeriod;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("type")
	private String returnType;
	
	public void setReturnType(String returnType) {
		this.returnType=StringUtils.substringAfter(returnType, "GST");
	}

}
