package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.business.dto.TaxPaidDto;
import com.ginni.easemygst.portal.data.part.INS;
import com.ginni.easemygst.portal.data.part.ITCCredit;
import com.ginni.easemygst.portal.data.part.OUTS;
import com.ginni.easemygst.portal.data.part.RFCLM;
import com.ginni.easemygst.portal.data.part.TCSCredit;
import com.ginni.easemygst.portal.data.part.TDSCredit;
import com.ginni.easemygst.portal.data.part.TOD;
import com.ginni.easemygst.portal.data.part.TTL;

import lombok.Data;

public @Data class GetGstr3Resp extends BaseResponse{

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonFormat(pattern="MMYYYY")
	@JsonProperty("ret_period")
	private String ret_period;
	
	@JsonProperty("tod")
	private List<TOD> turnover;
	
	@JsonProperty("osup")

	private OUTS outward_supply;
	
	@JsonProperty("isup")
	private INS inward_supply;

	@JsonProperty("ttxl")
	private TTL total_tax_liabilty;

	@JsonProperty("tcs_cr")
	private TCSCredit tcs_credit;
	
	@JsonProperty("tds_cr")
	private TDSCredit tds_credit;
	
	@JsonProperty("itc_cr")
	private ITCCredit itc_credit;
	
	@JsonProperty("tpm")
	private TaxPaidDto tax_paid;
	
	@JsonProperty("rf_clm")
	private RFCLM refund_claim;
	
	
}
