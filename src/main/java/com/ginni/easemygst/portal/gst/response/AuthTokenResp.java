package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class AuthTokenResp {

	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonProperty("auth_token")
	private String authToken;
	
	@JsonProperty("expiry")
	private String expiry;
	
	@JsonProperty("sek")
	private String sek;
	
	@JsonProperty("error")
	private Error error;

}
