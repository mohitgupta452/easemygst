
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cgst_bal",
    "igst_bal",
    "sgst_bal",
    "cess_bal"
})
public class ItcBal {

    @JsonProperty("cgst_bal")
    private Integer cgstBal;
    @JsonProperty("igst_bal")
    private Integer igstBal;
    @JsonProperty("sgst_bal")
    private Integer sgstBal;
    @JsonProperty("cess_bal")
    private Integer cessBal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cgst_bal")
    public Integer getCgstBal() {
        return cgstBal;
    }

    @JsonProperty("cgst_bal")
    public void setCgstBal(Integer cgstBal) {
        this.cgstBal = cgstBal;
    }

    @JsonProperty("igst_bal")
    public Integer getIgstBal() {
        return igstBal;
    }

    @JsonProperty("igst_bal")
    public void setIgstBal(Integer igstBal) {
        this.igstBal = igstBal;
    }

    @JsonProperty("sgst_bal")
    public Integer getSgstBal() {
        return sgstBal;
    }

    @JsonProperty("sgst_bal")
    public void setSgstBal(Integer sgstBal) {
        this.sgstBal = sgstBal;
    }

    @JsonProperty("cess_bal")
    public Integer getCessBal() {
        return cessBal;
    }

    @JsonProperty("cess_bal")
    public void setCessBal(Integer cessBal) {
        this.cessBal = cessBal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
