package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.HSNSUM;
import com.ginni.easemygst.portal.data.transaction.INVSUM;
import com.ginni.easemygst.portal.data.transaction.NIL;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetGstr1Resp {
	
	public GetGstr1Resp() {
		this.statusCd = "0";
	}

	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonProperty("b2b")
	private List<B2B> b2b;

	@JsonProperty("b2cl")
	private List<B2CL> b2cl;
	
	@JsonProperty("b2cs")
	private List<B2CS> b2cs;
	
	@JsonProperty("b2csa")
	private List<B2CS> b2csa;
	
	@JsonProperty("cdna")
	private List<CDN> cdna;
	
	@JsonProperty("nil")
	private List<NIL> nil;
	
	@JsonProperty("exp")
	private List<EXP> exp;
	
	@JsonProperty("at")
	private List<AT> at;
	
	@JsonProperty("ata")
	private List<AT> ata;
	
//	@JsonProperty("txp")
//	private List<TXP> txp;
	
	@JsonProperty("hsnsum")
	private List<HSNSUM> hsnsum;
	
	@JsonProperty("invsum")
	private List<INVSUM> invsum;
	
	@JsonProperty("error")
	private Error error;
	
	private String gsp;
	
}
