package com.ginni.easemygst.portal.gst.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.ATDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BReturnDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CLDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CSDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNReturnDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNURDeserializer;
import com.ginni.easemygst.portal.data.deserialize.DocIssueDeSerializer;
import com.ginni.easemygst.portal.data.deserialize.EXPDeserializer;
import com.ginni.easemygst.portal.data.deserialize.HSNSUMDeserializer;
import com.ginni.easemygst.portal.data.deserialize.NILDeserializer;
import com.ginni.easemygst.portal.data.deserialize.TXPDeserializer;
import com.ginni.easemygst.portal.data.serialize.ATSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CLSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CSSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNURSerializer;
import com.ginni.easemygst.portal.data.serialize.DocIssueSerializer;
import com.ginni.easemygst.portal.data.serialize.EXPSerializer;
import com.ginni.easemygst.portal.data.serialize.HSNSUMSerializer;
import com.ginni.easemygst.portal.data.serialize.NILSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetReturnResp {

	public GetReturnResp() {
		this.statusCd = "0";
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("form_typ")
	private String formType;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("action")
	private String action;

	@JsonDeserialize(using=B2BReturnDeserializer.class)
	@JsonSerialize(using=B2BSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("b2b")
	private List<B2BDetailEntity> b2b;
	
	@JsonProperty("b2cl")
	@JsonDeserialize(using=B2CLDeserializer.class)
	@JsonSerialize(using=B2CLSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<B2CLDetailEntity> b2cl;
	
	@JsonProperty("cdnr")
	@JsonDeserialize(using=CDNReturnDeserializer.class)
	@JsonSerialize(using=CDNSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<CDNDetailEntity> cdnr;
	
	@JsonProperty("cdnur")
	@JsonDeserialize(using=CDNURDeserializer.class)
	@JsonSerialize(using=CDNURSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<CDNURDetailEntity> cdnur;
	
	
	@JsonProperty("exp")
	@JsonDeserialize(using=EXPDeserializer.class)
	@JsonSerialize(using=EXPSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<ExpDetail> exp;
	
	@JsonProperty("b2cs")
	@JsonDeserialize(using=B2CSDeserializer.class)
	@JsonSerialize(using=B2CSSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<B2CSTransactionEntity> b2cs;
	
	@JsonProperty("txp")
	@JsonDeserialize(using=TXPDeserializer.class)
	@JsonSerialize(using=TXPSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<TxpdTransaction> txpd;
	
	@JsonSetter("txpdoc")
	@JsonDeserialize(using=TXPDeserializer.class)
	@JsonSerialize(using=TXPSerializer.class)
	@JsonInclude(Include.NON_NULL)
	public void setTxpError(List<TxpdTransaction> txpError){
		this.txpd=txpError;
	}
	
	@JsonProperty("at")
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonSerialize(using=ATSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<ATTransactionEntity> at;
	
	@JsonProperty("nil")
	@JsonDeserialize(using=NILDeserializer.class)
	@JsonSerialize(using=NILSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<NILTransactionEntity> nil;
	
	@JsonProperty("hsnsum")
	@JsonDeserialize(using=HSNSUMDeserializer.class)
	@JsonSerialize(using=HSNSUMSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<Hsn> hsn;
	
	@JsonProperty("doc_issue")
	@JsonDeserialize(using=DocIssueDeSerializer.class)
	@JsonSerialize(using=DocIssueSerializer.class)	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<DocIssue> docissue;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error")
	private Error error;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error_cd")
	private String error_cd;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error_msg")
	private String error_msg;
	
	@JsonSetter("b2ba")
	@JsonDeserialize(using=B2BReturnDeserializer.class)
	@JsonSerialize(using=B2BSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setB2BA(List<B2BDetailEntity> b2bDetailEntities) {
		
		if(Objects.isNull(b2b) || b2b.isEmpty())
			b2b= new ArrayList<>();
		this.b2b.addAll(b2bDetailEntities);
	}
	
	@JsonSetter("b2cla")
	@JsonDeserialize(using=B2CLDeserializer.class)
	@JsonSerialize(using=B2CLSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
   public void setB2CLA(List<B2CLDetailEntity> b2clDetailEntities) {
		
		if(Objects.isNull(this.b2cl) || b2cl.isEmpty())
			this.b2cl= new ArrayList<>();
		this.b2cl.addAll(b2clDetailEntities);
	}
	
	@JsonSetter("cdnra")
	@JsonDeserialize(using=CDNReturnDeserializer.class)
	@JsonSerialize(using=CDNSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setCdnUra(List<CDNDetailEntity> cdnra) {
		if(Objects.isNull(this.cdnr) || cdnr.isEmpty())
			cdnr= new ArrayList<>();
		cdnr.addAll(cdnra);
	}
	
	@JsonSetter("cdnura")
	@JsonDeserialize(using=CDNURDeserializer.class)
	@JsonSerialize(using=CDNURSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setCdnura(List<CDNURDetailEntity> cdnura) {
		
		if(Objects.isNull(this.cdnur))
			this.cdnur=new ArrayList<>();
		this.cdnur.addAll(cdnura);
	}
	
	@JsonSetter("expa")
	@JsonDeserialize(using=EXPDeserializer.class)
	@JsonSerialize(using=EXPSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setExpa(List<ExpDetail> expa) {
		if(Objects.isNull(this.exp)) {
			this.exp=new ArrayList<>();
		}
		this.exp.addAll(expa);
	}
	
	@JsonProperty("b2csa")
	@JsonDeserialize(using=B2CSDeserializer.class)
	@JsonSerialize(using=B2CSSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setB2csa(List<B2CSTransactionEntity> b2csa) {
		if(Objects.isNull(this.b2cs)) {
			this.b2cs= new ArrayList<>();
		}
		this.b2cs.addAll(b2csa);
	}
	
	@JsonSetter("txpda")
	@JsonDeserialize(using=TXPDeserializer.class)
	@JsonSerialize(using=TXPSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setTxpda(List<TxpdTransaction> txpda) {
		if(Objects.isNull(this.txpd)) {
			this.txpd= new ArrayList<>();
		}
		txpd.addAll(txpda);
	}
	
	@JsonSetter("ata")
	@JsonDeserialize(using=ATDeserializer.class)
	@JsonSerialize(using=ATSerializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public void setAta(List<ATTransactionEntity> ata) {
		if(Objects.isNull(this.at)) {
			this.at= new ArrayList<>();
		}
		this.at.addAll(ata);
	}
	
	
}
