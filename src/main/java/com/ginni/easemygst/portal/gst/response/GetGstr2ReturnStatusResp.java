package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.Gstr2ErrorReport;

import lombok.Data;

public @Data class GetGstr2ReturnStatusResp {

	@JsonProperty("state_cd")
	private String statusCode;
	
	@JsonProperty("arn_no")
	private String acknowledgementNo;
	
	@JsonProperty("Error Report")
	private Gstr2ErrorReport errorReport;
	
}
