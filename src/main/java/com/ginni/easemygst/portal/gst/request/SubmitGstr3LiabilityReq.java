package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.Gstr3Summary;

import lombok.Data;

public @Data class SubmitGstr3LiabilityReq {
	
	@JsonProperty("intr_liab")
	private Gstr3TransactionSummary interesetLiability;

	@JsonProperty("oth_itc_rvl_usr")
	private Gstr3TransactionSummary otherItc;
	
	@JsonProperty("intr_liab_fwd_usr")
	private Gstr3TransactionSummary interLiabCarryForward;
	
	@JsonProperty("del_pymt_tx_usr")
	private Gstr3TransactionSummary interCarryForward;
	
	@JsonProperty("othr_intr")
	private Gstr3TransactionSummary otherInterest;
	
   public @Data static class Gstr3TransactionSummary {
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("iamt")
	    public Double igstAmt = new Double(0.00);

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("camt")
	    public Double cgstAmt = new Double(0.00);
	    
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("samt")
	    public Double sgstAmt = new Double(0.00);
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cess")
	    public Double cessAmt = new Double(0.00);
		
	}
	

}
