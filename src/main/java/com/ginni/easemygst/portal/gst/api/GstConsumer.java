package com.ginni.easemygst.portal.gst.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scripting.support.RefreshableScriptTargetSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.ApplicationMetadata;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata;
import com.ginni.easemygst.portal.constant.GstMetadata.Returns;
import com.ginni.easemygst.portal.constant.GstMetadata.ReturnsModule;
import com.ginni.easemygst.portal.gst.request.AuthRefreshTokenReq;
import com.ginni.easemygst.portal.gst.response.AuthRefreshTokenResp;
import com.ginni.easemygst.portal.gst.response.AuthTokenResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.persistence.entity.GstnApiLog;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.security.AESEncryption;
import com.ginni.easemygst.portal.security.EncryptDecrypt;
import com.ginni.easemygst.portal.security.GSTRSAEncryptDecryptService;
import com.ginni.easemygst.portal.utils.Utility;
import com.github.fge.jackson.jsonpointer.ReferenceToken;
import com.mashape.unirest.http.JsonNode;

@Named("gstConsumer")
@RequestScoped
public class GstConsumer implements Serializable {

	private final String KEY_DATA = "data";

	private final String KEY_ACTION = "action";

	private final String KEY_REK = "rek";

	private final String KEY_HMAC = "hmac";

	private static final long serialVersionUID = 1L;

	protected static Logger log = LoggerFactory.getLogger(GstConsumer.class);

	private Map<String, String> headers;

	private ObjectMapper objectMapper;

	private TaxpayerGstin taxpayerGstin;

	private String returnPeriod;

	private String gstUrl;

	private String token;

	private transient AESEncryption aesEncryption;

	private String appKey;

	private byte[] appKeyBytes;

	@Inject
	private CrudService crudService;

	@Inject
	private UserBean userBean;

	@Inject
	private GSTRSAEncryptDecryptService encryptDecrypt;
	
	private String gsp;
	@Inject
	private RedisService redisService;

	@PostConstruct
	public void init() {

	}
	
	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin,String gsp) throws AppException {

		this.objectMapper = new ObjectMapper();
		this.aesEncryption = new AESEncryption();
		this.taxpayerGstin = taxpayerGstin;

		this.headers = new HashMap<String, String>();
		
		//this.headers.put(GstMetadata.HEADER_KEY_CLIENT_ID, GstMetadata.HEADER_CLIENT_ID);
		//this.headers.put(GstMetadata.HEADER_KEY_CLIENT_SECRET, GstMetadata.HEADER_CLIENT_SECRET);

		GspFactory gspFactory = new GspFactory(gsp);
		this.headers.putAll(gspFactory.authenticate());
		this.gstUrl = gspFactory.getUrl();
		
		gspFactory.getClass();

		this.headers.put(GstMetadata.HEADER_KEY_CONTENT_TYPE, GstMetadata.HEADER_CONTENT_TYPE);
		this.headers.put(GstMetadata.HEADER_KEY_IP_USR,redisService.getValue(ApplicationMetadata.SERVER_IP_ADDRESS));
	}

	public void setTaxpayerGstin(TaxpayerGstin taxpayerGstin) throws AppException {

		this.objectMapper = new ObjectMapper();
		this.aesEncryption = new AESEncryption();
		this.taxpayerGstin = taxpayerGstin;

		this.headers = new HashMap<String, String>();
		
		//this.headers.put(GstMetadata.HEADER_KEY_CLIENT_ID, GstMetadata.HEADER_CLIENT_ID);
		//this.headers.put(GstMetadata.HEADER_KEY_CLIENT_SECRET, GstMetadata.HEADER_CLIENT_SECRET);

		GspFactory gspFactory = new GspFactory(null);
		this.headers.putAll(gspFactory.authenticate());
		this.gstUrl = gspFactory.getUrl();
		
		gspFactory.getClass();

		this.headers.put(GstMetadata.HEADER_KEY_CONTENT_TYPE, GstMetadata.HEADER_CONTENT_TYPE);
		this.headers.put(GstMetadata.HEADER_KEY_IP_USR,redisService.getValue(ApplicationMetadata.SERVER_IP_ADDRESS));
	}

	public void addHeaders(String key, String value) {
		this.headers.put(key, value);
	}

	public void addTaxpayerHeaders(boolean auth) throws AppException {

		this.headers.put(GstMetadata.HEADER_KEY_STATE_CD, StringUtils.substring(taxpayerGstin.getGstin(), 0, 2) + "");
		this.headers.put(GstMetadata.HEADER_KEY_TXN,
				Utility.randomString(6) + Calendar.getInstance().getTimeInMillis());
		this.headers.put(GstMetadata.HEADER_KEY_TXN,
				Utility.randomString(6) + Calendar.getInstance().getTimeInMillis());
		this.headers.put(GstMetadata.HEADER_KEY_USERNAME, taxpayerGstin.getUsername());

		if (auth) {
			if (taxpayerGstin.getAuthenticated() != null && taxpayerGstin.getAuthenticated() == 1) {
				appKeyBytes = aesEncryption.decodeBase64StringTOByte(this.taxpayerGstin.getAppKey());
				try {
					appKey = encryptDecrypt.encrypt(appKeyBytes);
				} catch (IOException e) {
					throw new AppException(e);
				}
				
				this.headers.put(GstMetadata.HEADER_KEY_APP_KEY, appKey);
				this.headers.put(GstMetadata.HEADER_KEY_GSTIN, taxpayerGstin.getGstin());
				this.headers.put(GstMetadata.HEADER_KEY_AUTH_TOKEN, taxpayerGstin.getAuthToken().toString());
				this.headers.put(GstMetadata.HEADER_KEY_APP_KEY, appKey);
				this.headers.put(GstMetadata.HEADER_KEY_GSTIN, taxpayerGstin.getGstin());
			} else {
				throw new AppException(ExceptionCode._GSTN_AUTH_FAILED);
			}
		}
	}

	private void logApiRequest(GstnApiLog gstnApiLog) {
		gstnApiLog.setCreationTime(Timestamp.from(Instant.now()));
		getClass();
		if (!Objects.isNull(userBean.getUserDto())) {
			gstnApiLog.setCreatedBy(userBean.getUserDto().getFirstName() + "_" + userBean.getUserDto().getLastName());
			gstnApiLog.setCreationIpAddress(userBean.getIpAddress());
		}
		crudService.create(gstnApiLog);
	}

	public String authenticate(Object object) {

		try {
			String input = this.objectMapper.writeValueAsString(object);
			String output = RestClient.call(this.gstUrl + GstMetadata.FUNCTION_AUTHENTICATION, new JsonNode(input),
					this.headers);

			// log gstn api request/response
			GstnApiLog gstnApiLog = new GstnApiLog();
			gstnApiLog.setFunction(GstMetadata.FUNCTION_AUTHENTICATION.toString());
			gstnApiLog.setRequestPayload(input);
			gstnApiLog.setResponsePayload(output);
			gstnApiLog.setStatus("COMPLETED");
			gstnApiLog.setTaxpayerGstin(taxpayerGstin);
			gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION_AUTHENTICATION);
			logApiRequest(gstnApiLog);

			return output;
		} catch (Exception e) {
			log.error("exception",e);
		}
		return null;
	}

	public String returns(Object object, ReturnsModule module) throws AppException {

		try {
			
			String input = this.objectMapper.writeValueAsString(object);
			// log gstn api request/response
						GstnApiLog gstnApiLog = new GstnApiLog();
						gstnApiLog.setModule(module != null ? module.toString() : "");
						gstnApiLog.setRequestPayload(input);
						/*if(!Objects.isNull(input)){
							ObjectNode jsonNode = new ObjectMapper().readValue(input, ObjectNode.class);

							String json = null;
							if (jsonNode.has(KEY_DATA))
								json = jsonNode.get(KEY_DATA).asText();
							if (jsonNode.has("tx_offset"))
								json = jsonNode.get("tx_offset").asText();
							if (jsonNode.has("refclm"))
								json = jsonNode.get("refclm").asText();
							if(Objects.isNull(json)&&Objects.nonNull(jsonNode)){//to handle gstr3b case
								json=jsonNode.toString();
							}
						gstnApiLog.setRequestPayload(json);
						}*/
						String output="";
			input = processInputPayload(input);
				if(!ReturnsModule.Ledgers.equals(module)){
		 output = RestClient.call(this.gstUrl + GstMetadata.FUNCTION_RETURNS+(module !=null?module:""),
							new JsonNode(input), this.headers);
			gstnApiLog.setFunction(GstMetadata.FUNCTION_RETURNS.toString());
		 gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION_RETURNS + module);
			}
			else{
				 output = RestClient.call(this.gstUrl + GstMetadata.FUNCTION_LEDGERS,
							new JsonNode(input), this.headers);
					gstnApiLog.setFunction(GstMetadata.FUNCTION_LEDGERS.toString());
				 gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION_LEDGERS);
			}
		
			output = processOutputPayload(output);
			gstnApiLog.setResponsePayload(output);
			gstnApiLog.setStatus("COMPLETED");
			gstnApiLog.setTaxpayerGstin(taxpayerGstin);
			
			logApiRequest(gstnApiLog);

			return output;
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			log.error("error in return method {}", e);
			throw new AppException();
		}
	}
	
	
	public String returns3bStatus(Object object, ReturnsModule module) throws AppException {

		try {
			String input = this.objectMapper.writeValueAsString(object);
			// log gstn api request/response
						GstnApiLog gstnApiLog = new GstnApiLog();
						gstnApiLog.setFunction(GstMetadata.FUNCTION_RETURNS.toString());
						gstnApiLog.setModule(module != null ? module.toString() : "");
						gstnApiLog.setRequestPayload(input);
						
			input = processInputPayload(input);
			String output =null;
			
			 output = RestClient.call(this.gstUrl + GstMetadata.FUNCTION_RETURNS_3B+(module !=null?"/"+module:""),
							new JsonNode(input), this.headers);
		
			output = processOutputPayload(output);

			
			gstnApiLog.setResponsePayload(output);
			gstnApiLog.setStatus("COMPLETED");
			gstnApiLog.setTaxpayerGstin(taxpayerGstin);
			gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION_RETURNS + module);
			logApiRequest(gstnApiLog);

			return output;
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			log.error("error in return method {}", e);
			throw new AppException();
		}
	}
	
	public InputStream returnsUrlData(Object object,ReturnsModule module) throws AppException {

		try{
			String input = this.objectMapper.writeValueAsString(object);
		   input = processInputPayload(input);
		 InputStream output = RestClient.callFileDownload(this.gstUrl + GstMetadata.FUNCTION__FILES+(module !=null?module:""),
					new JsonNode(input), this.headers);
		
		//log gstn api request/response
		GstnApiLog gstnApiLog = new GstnApiLog();
		gstnApiLog.setFunction(GstMetadata.FUNCTION_RETURNS.toString());
		gstnApiLog.setModule(module !=null?module.toString():"");
		gstnApiLog.setRequestPayload(input);
		//gstnApiLog.setResponsePayload(output);
		gstnApiLog.setStatus("COMPLETED");
		gstnApiLog.setTaxpayerGstin(taxpayerGstin);
		gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION__FILES + module);
		logApiRequest(gstnApiLog);
		
		return output;
	}catch(AppException e){
		throw e;
	} catch (Exception e) {
		log.error("error in return method {}",e);
	   throw new AppException();
	}
}

	public String ledgers(Object object) {

		try {
			String input = this.objectMapper.writeValueAsString(object);
			GstnApiLog gstnApiLog = new GstnApiLog();
			gstnApiLog.setFunction(GstMetadata.FUNCTION_RETURNS.toString());
			gstnApiLog.setRequestPayload(input);
			input = processInputPayload(input);
			String output = RestClient.call(this.gstUrl + GstMetadata.FUNCTION_LEDGERS, input, this.headers);
			output = processOutputPayload(output);

			//log gstn api request/response
			
			gstnApiLog.setResponsePayload(output);
			gstnApiLog.setStatus("COMPLETED");
			gstnApiLog.setTaxpayerGstin(taxpayerGstin);
			gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION_LEDGERS);
			logApiRequest(gstnApiLog);

			return output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public AESEncryption getAesEncryption() {
		return aesEncryption;
	}

	public void setAesEncryption(AESEncryption aesEncryption) {
		this.aesEncryption = aesEncryption;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public byte[] getAppKeyBytes() {
		return appKeyBytes;
	}

	public void setAppKeyBytes(byte[] appKeyBytes) {
		this.appKeyBytes = appKeyBytes;
	}

	public TaxpayerGstin getTaxpayerGstin() {
		return taxpayerGstin;
	}

	public String getReturnPeriod() {
		return returnPeriod;
	}

	public void setReturnPeriod(String returnPeriod) {
		this.returnPeriod = returnPeriod;
		this.headers.put(GstMetadata.HEADER_KEY_RET_PRD, returnPeriod);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	private String processInputPayload(String input) throws Exception {

		ObjectNode jsonNode = new ObjectMapper().readValue(input, ObjectNode.class);

		log.debug("input json node {}", input);

		String action = jsonNode.get("action").asText();

		log.debug("request body {}", jsonNode);

		if (action.contains("POST") || action.contains("PUT")) {
			String json = null;
			if (jsonNode.has(KEY_DATA))
				json = jsonNode.get(KEY_DATA).asText();
			if (jsonNode.has("tx_offset"))
				json = jsonNode.get("tx_offset").asText();
			if (jsonNode.has("refclm"))
				json = jsonNode.get("refclm").asText();
			if(Objects.isNull(json)&&Objects.nonNull(jsonNode)){//to handle gstr3b case
				json=jsonNode.toString();
			}
			log.debug("request body {}", json);

			byte[] authSEK = aesEncryption.decrypt(this.taxpayerGstin.getSek(),
					aesEncryption.decodeBase64StringTOByte(aesEncryption.encodeBase64String(this.appKeyBytes)));

			// convery payload to Base64
			byte[] jsonBase64 = Base64.getEncoder().encode(json.getBytes());

			// encrypt the Base64 data
			String encryptedPayload = aesEncryption.encrypt(new String(jsonBase64), authSEK);

			// create hmac
			HMac hmac = new HMac(new SHA256Digest());
			byte[] resBuf = new byte[hmac.getMacSize()];

			// init with the AuthSEK
			hmac.init(new KeyParameter(authSEK));
			// add the json(Base64)
			hmac.update(jsonBase64, 0, jsonBase64.length);
			hmac.doFinal(resBuf, 0);

			JSONObject object = new JSONObject();
			object.put(KEY_ACTION, action);
			object.put(KEY_DATA, encryptedPayload);
			object.put(KEY_HMAC, new String(Base64.getEncoder().encode(resBuf)));
			if (action.contains("RETFILE")) {
				log.debug("request body {}", jsonNode.toString());
				object.put("sign", jsonNode.get("sign").asText());
				object.put("st", jsonNode.get("st").asText());
				object.put("sid", jsonNode.get("sid").asText());
			}
			input = object.toString();
		}

		return input;
	}

	/**
	 * 
	 * @param output
	 * @return
	 * @throws Exception
	 */
	private String processFileData(String input) {

		return input;
	}

	private String processOutputPayload(String output) throws Exception {

		String jsonData = new String(output);
		JSONObject gstrRespObj = new JSONObject(output);

		if (gstrRespObj.has(KEY_DATA)) {

			String data = gstrRespObj.getString(KEY_DATA);
			String rek = gstrRespObj.getString(KEY_REK);

			byte[] authSEK = aesEncryption.decrypt(taxpayerGstin.getSek(),
					aesEncryption.decodeBase64StringTOByte(aesEncryption.encodeBase64String(this.appKeyBytes)));

			// recover apiEncryptionKey from Response using our AuthSEK
			byte[] apiEK = aesEncryption.decrypt(rek, authSEK);

			// using the apiEncryptionKey, recover the Json data (in base64 fmt)
			String respJsoninBase64 = new String(aesEncryption.decrypt(data, apiEK));

			// convert base64 to original json (in bytes)
			byte[] respJsonInBytes = aesEncryption.decodeBase64StringTOByte(respJsoninBase64);

			// convery original json in bytes to json string
			jsonData = new String(respJsonInBytes);
		}
		log.debug("response {}", jsonData);
		return jsonData;
	}

	public String returnsGet(Object object, ReturnsModule module) throws AppException {

		try {

			BeanUtils.setProperty(object, "gstin", null);
			BeanUtils.setProperty(object, "monthYear", null);
			BeanUtils.setProperty(object, "returnType", null);

			String input = this.objectMapper.writeValueAsString(object);
			// input = processInputPayload(input);
			String temp[] = Returns.GETCHUNKDATA.toString().split("_");
			String output = RestClient.callApi(this.gstUrl + GstMetadata.FUNCTION_RETURNS.replace("[VERSION]", temp[2])
					+ (module != null ? module : ""), temp[1], new JsonNode(input), this.headers);
			output = processOutputPayload(output);

			// log gstn api request/response
			GstnApiLog gstnApiLog = new GstnApiLog();
			gstnApiLog.setFunction(GstMetadata.FUNCTION_RETURNS.toString());
			gstnApiLog.setModule(module != null ? module.toString() : "");
			gstnApiLog.setRequestPayload(input);
			gstnApiLog.setResponsePayload(output);
			gstnApiLog.setStatus("COMPLETED");
			gstnApiLog.setTaxpayerGstin(taxpayerGstin);
			gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION_RETURNS + module);
			logApiRequest(gstnApiLog);

			return output;
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			log.error("error in return method {}", e);
			throw new AppException();
		}
	}
	
	public String returnsSearchTPData(Object object,ReturnsModule module) throws AppException {

		try{
			String input = this.objectMapper.writeValueAsString(object);
		   input = processInputPayload(input);
//		   headers.clear();
//		   headers.put(GstMetadata.HEADER_KEY_CLIENT_ID, GstMetadata.HEADER_CLIENT_ID);
//		   headers.put(GstMetadata.HEADER_KEY_CLIENT_SECRET, GstMetadata.HEADER_CLIENT_SECRET);
		 String output = RestClient.call(this.gstUrl + GstMetadata.SEARCH_TAXPAYER+(module !=null?module:""),
					new JsonNode(input), this.headers);
	
		
		//log gstn api request/response
		GstnApiLog gstnApiLog = new GstnApiLog();
		gstnApiLog.setFunction(GstMetadata.FUNCTION_RETURNS.toString());
		gstnApiLog.setModule(module !=null?module.toString():"");
		gstnApiLog.setRequestPayload(input);
		gstnApiLog.setResponsePayload(output);
		gstnApiLog.setStatus("COMPLETED");
		gstnApiLog.setTaxpayerGstin(taxpayerGstin);
		gstnApiLog.setUrl(this.gstUrl + GstMetadata.FUNCTION__FILES + module);
		logApiRequest(gstnApiLog);
		
		return output;
	}catch(AppException e){
		throw e;
	} catch (Exception e) {
		log.error("error in return method {}",e);
	   throw new AppException();
	}
}

	public String getGsp() {
		return gsp;
	}

	public void setGsp(String gsp) {
		this.gsp = gsp;
	}

}
