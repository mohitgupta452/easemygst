package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public @Data class Gstr3bSummaryResp extends BaseResponse	{

	@JsonProperty("gstin")
	@JsonInclude(Include.NON_NULL)
	private String gstin;
	@JsonProperty("ret_period")
	@JsonInclude(Include.NON_NULL)
	private String retPeriod;
	@JsonProperty("sup_details")
	@JsonInclude(Include.NON_NULL)
	private SupDetails supDetails;
	@JsonProperty("inter_sup")
	@JsonInclude(Include.NON_NULL)
	private InterSup interSup;
	@JsonProperty("itc_elg")
	@JsonInclude(Include.NON_NULL)
	private ItcElg itcElg;
	@JsonProperty("inward_sup")
	@JsonInclude(Include.NON_NULL)
	private InwardSup inwardSup;
	@JsonProperty("intr_ltfee")
	@JsonInclude(Include.NON_NULL)
	private IntrLtfee intrLtfee;
	@JsonProperty("tx_pmt")
	@JsonInclude(Include.NON_NULL)
	private TxPmt txPmt;
	
/*	@JsonProperty("pdcash")
	@JsonInclude(Include.NON_NULL)
	private Object pdcash;
	
	@JsonProperty("pditc")
	@JsonInclude(Include.NON_NULL)
	private Object pditc;*/
/*	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();*/

	@JsonProperty("gstin")
	public String getGstin() {
	return gstin;
	}

	@JsonProperty("gstin")
	public void setGstin(String gstin) {
	this.gstin = gstin;
	}

	@JsonProperty("ret_period")
	public String getRetPeriod() {
	return retPeriod;
	}

	@JsonProperty("ret_period")
	public void setRetPeriod(String retPeriod) {
	this.retPeriod = retPeriod;
	}

	@JsonProperty("sup_details")
	public SupDetails getSupDetails() {
	return supDetails;
	}

	@JsonProperty("sup_details")
	public void setSupDetails(SupDetails supDetails) {
	this.supDetails = supDetails;
	}

	@JsonProperty("inter_sup")
	public InterSup getInterSup() {
	return interSup;
	}

	@JsonProperty("inter_sup")
	public void setInterSup(InterSup interSup) {
	this.interSup = interSup;
	}

	@JsonProperty("itc_elg")
	public ItcElg getItcElg() {
	return itcElg;
	}

	@JsonProperty("itc_elg")
	public void setItcElg(ItcElg itcElg) {
	this.itcElg = itcElg;
	}

	@JsonProperty("inward_sup")
	public InwardSup getInwardSup() {
	return inwardSup;
	}

	@JsonProperty("inward_sup")
	public void setInwardSup(InwardSup inwardSup) {
	this.inwardSup = inwardSup;
	}

	@JsonProperty("intr_ltfee")
	public IntrLtfee getIntrLtfee() {
	return intrLtfee;
	}

	@JsonProperty("intr_ltfee")
	public void setIntrLtfee(IntrLtfee intrLtfee) {
	this.intrLtfee = intrLtfee;
	}

	@JsonProperty("tx_pmt")
	public TxPmt getTxPmt() {
	return txPmt;
	}

	@JsonProperty("tx_pmt")
	public void setTxPmt(TxPmt txPmt) {
	this.txPmt = txPmt;
	}

/*	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}*/
	
	
	
}
