package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.BaseSummaryTotal;

import lombok.Data;

public @Data class StateSummary extends BaseSummaryTotal {
	
	@JsonProperty("state_cd")
	private String stateCode;
	
	@JsonProperty("checksum")
	private String checksum;
	
}