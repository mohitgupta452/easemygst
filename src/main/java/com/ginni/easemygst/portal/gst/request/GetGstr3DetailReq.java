package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class GetGstr3DetailReq {
	
	@JsonProperty("gstin")
	private String gstin;

	@JsonProperty("ret_period")
	private String monthYear;
	
	@JsonProperty("action")
	private String action;
	
	@JsonProperty("final")
	private String finalvalue;

}
