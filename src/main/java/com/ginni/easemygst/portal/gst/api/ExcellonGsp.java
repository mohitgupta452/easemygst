package com.ginni.easemygst.portal.gst.api;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.spi.LoggerFactory;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.security.ExactEncrypt;
import com.mashape.unirest.http.JsonNode;

public class ExcellonGsp implements Gsp {
	
	private static final Logger _LOGGER=org.slf4j.LoggerFactory.getLogger(ExcellonGsp.class);

	public static final String EXACT_AUTH_VALIDITY = "exact_auth_validity";

	public static final String EXACT_AUTH_TOKEN = "exact_auth_token";

	@Override
	public Map<String, String> authenticate() throws AppException {

		Map<String, String> outputHeaders = new HashMap<>();
		outputHeaders.put(GstMetadata.HEADER_KEY_GSP_REQUEST_ID, UUID.randomUUID().toString());
		outputHeaders.put(GstMetadata.HEADER_KEY_GSP_ID, config.getProperty("main.gst.excellon.subscription.id"));
		
		boolean isApiCallRequired = true;
		if (!Objects.isNull(redisService.getValue(EXACT_AUTH_TOKEN))) {
			outputHeaders.put(GstMetadata.HEADER_KEY_GSP_AUTH_TOKEN, redisService.getValue(EXACT_AUTH_TOKEN));
			isApiCallRequired = false;
		}

		if (isApiCallRequired) {
			String clientId = config.getProperty("main.gst.excellon.client.id");
			String secureKey = config.getProperty("main.gst.excellon.client.secret");
			String authUrl = config.getProperty("main.gst.excellon.auth.url");

			String clientSecret = "";
			try {
				clientSecret = ExactEncrypt.encrypt(secureKey);
			} catch (Exception e) {
			_LOGGER.error("error while encrypting exact auth token",e);
				throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("ClientId", clientId);
			jsonObject.put("ClientSecret", clientSecret);

			Map<String, String> headers = new HashMap<>();
			headers.put("RequestId", UUID.randomUUID().toString());
			headers.put("Content-Type", "application/json");

			String output = RestClient.callApi(authUrl, "POST", new JsonNode(jsonObject.toString()), headers);
			JSONObject object = new JSONObject(output);

			if (!object.has("status_cd")) {
				if (object.has("IsAuthenticated") && object.getBoolean("IsAuthenticated")) {
					if (object.has("AuthenticationValidTillDateTime")) {
						String timing = object.getString("AuthenticationValidTillDateTime");
						/*Date dt = new Date(timing);
						Date dtNow = Calendar.getInstance().getTime();
						int timeDiff = (int) ((dt.getTime() - dtNow.getTime() - 60000) / 1000);*/
						if (object.has("AuthenticationToken")) {
							redisService.setValue(EXACT_AUTH_TOKEN, object.getString("AuthenticationToken"), true,
									20000);
							outputHeaders.put(GstMetadata.HEADER_KEY_GSP_AUTH_TOKEN, object.getString("AuthenticationToken"));
						}
					}
				}
			} else {
				// invalid response. please check logs for more information.
			}
		}

		return outputHeaders;
	}

	@Override
	public String getUrl() {
		return config.getProperty("main.gst.excellon.endpoint.url");
	}

	@Override
	public String getGspName() {
		return "EXCELLON";
	}

}
