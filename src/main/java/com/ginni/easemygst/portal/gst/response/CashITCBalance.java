
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cash_bal",
    "itc_bal",
    "gstin"
})
public class CashITCBalance {

    @JsonProperty("cash_bal")
    private CashBal cashBal;
    @JsonProperty("itc_bal")
    private ItcBal itcBal;
    @JsonProperty("gstin")
    private String gstin;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cash_bal")
    public CashBal getCashBal() {
        return cashBal;
    }

    @JsonProperty("cash_bal")
    public void setCashBal(CashBal cashBal) {
        this.cashBal = cashBal;
    }

    @JsonProperty("itc_bal")
    public ItcBal getItcBal() {
        return itcBal;
    }

    @JsonProperty("itc_bal")
    public void setItcBal(ItcBal itcBal) {
        this.itcBal = itcBal;
    }

    @JsonProperty("gstin")
    public String getGstin() {
        return gstin;
    }

    @JsonProperty("gstin")
    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
