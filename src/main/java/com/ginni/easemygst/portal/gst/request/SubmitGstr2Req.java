package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SubmitGstr2Req {
	

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String retPeriod;
	
	@JsonProperty("action")
	private String action;


}
