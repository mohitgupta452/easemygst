package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SaveGstr1AResp {

	@JsonProperty("ref_id")
	private String referenceId;
	
	@JsonProperty("txn_id")
	private String transactionId;
}
