package com.ginni.easemygst.portal.gst.api;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;

public class GspFactory implements Gsp {

	private Gsp gsp;

	public GspFactory(String gspname) {
		
		if (!"production".equalsIgnoreCase(AppConfig.getActiveProfile()) || StringUtils.isEmpty(gspname)) {
			gsp = new ExcellonGsp();
		} else if (gspname.equalsIgnoreCase("SPICE")) {
			gsp = new SpiceGsp();
		} else if (gspname.equalsIgnoreCase("EXCELLON")) {
			gsp = new ExcellonGsp();
		} else {
			gsp = new SpiceGsp();
		}

		try {
			config.load(GspFactory.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, String> authenticate() throws AppException {
		return gsp.authenticate();
	}

	@Override
	public String getUrl() {
		return gsp.getUrl();
	}

	@Override
	public String getGspName() {
		return gsp.getGspName();
	}

}
