package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TaxHeads {

	@JsonProperty("tx")
	private long tax;
	
	@JsonProperty("intr")
	private long interest;
	
	@JsonProperty("pen")
	private long penalty;
	
	@JsonProperty("oth")
	private long other;
	
	@JsonProperty("fee")
	private long fee;
	
	@JsonProperty("tot")
	private long total;

}
