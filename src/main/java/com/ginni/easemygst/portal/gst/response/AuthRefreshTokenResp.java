package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class AuthRefreshTokenResp {

	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonProperty("auth_token")
	private String authToken;
	
	@JsonProperty("sek")
	private String sek;
	
	@JsonProperty("expiry")
	private String expiry;
	
	@JsonProperty("error")
	private Error error;

}
