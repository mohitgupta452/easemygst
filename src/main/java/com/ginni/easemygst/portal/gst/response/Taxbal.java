package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Taxbal {

@JsonProperty("intr")
private Integer intr;
@JsonProperty("oth")
private Integer oth;
@JsonProperty("tx")
private Integer tx;
@JsonProperty("fee")
private Integer fee;
@JsonProperty("pen")
private Integer pen;
@JsonProperty("tot")
private Integer tot;
}
