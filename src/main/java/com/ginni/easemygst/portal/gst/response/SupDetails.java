package com.ginni.easemygst.portal.gst.response;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "osup_det",
    "osup_zero",
    "osup_nil_exmp",
    "isup_rev",
    "osup_nongst"
})
public class SupDetails {

    @JsonProperty("osup_det")
    private OsupDet osupDet;
    @JsonProperty("osup_zero")
    private OsupZero osupZero;
    @JsonProperty("osup_nil_exmp")
    private OsupNilExmp osupNilExmp;
    @JsonProperty("isup_rev")
    private IsupRev isupRev;
    @JsonProperty("osup_nongst")
    private OsupNongst osupNongst;


    @JsonProperty("osup_det")
    public OsupDet getOsupDet() {
        return osupDet;
    }

    @JsonProperty("osup_det")
    public void setOsupDet(OsupDet osupDet) {
        this.osupDet = osupDet;
    }

    @JsonProperty("osup_zero")
    public OsupZero getOsupZero() {
        return osupZero;
    }

    @JsonProperty("osup_zero")
    public void setOsupZero(OsupZero osupZero) {
        this.osupZero = osupZero;
    }

    @JsonProperty("osup_nil_exmp")
    public OsupNilExmp getOsupNilExmp() {
        return osupNilExmp;
    }

    @JsonProperty("osup_nil_exmp")
    public void setOsupNilExmp(OsupNilExmp osupNilExmp) {
        this.osupNilExmp = osupNilExmp;
    }

    @JsonProperty("isup_rev")
    public IsupRev getIsupRev() {
        return isupRev;
    }

    @JsonProperty("isup_rev")
    public void setIsupRev(IsupRev isupRev) {
        this.isupRev = isupRev;
    }

    @JsonProperty("osup_nongst")
    public OsupNongst getOsupNongst() {
        return osupNongst;
    }

    @JsonProperty("osup_nongst")
    public void setOsupNongst(OsupNongst osupNongst) {
        this.osupNongst = osupNongst;
    }


}

