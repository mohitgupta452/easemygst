package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.B2BDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BReturnDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CLDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CSDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNReturnDeserializer;
import com.ginni.easemygst.portal.data.deserialize.EXPDeserializer;
import com.ginni.easemygst.portal.data.deserialize.TXPDeserializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CLSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CSSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.EXPSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetGstr2Resp extends BaseResponse{

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("est")
	private String est;

	@JsonDeserialize(using=B2BDeserializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("b2b")
	private List<B2BTransactionEntity> b2b;

//	@JsonDeserialize(using=B2BDeserializer.class)
//	@JsonProperty("b2ba")
//	private List<B2BTransactionEntity> b2ba;

	@JsonProperty("cdn")
	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<CDNTransactionEntity> cdn;
	
	@JsonProperty("cdnr")
	@JsonDeserialize(using=CDNDeserializer.class)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<CDNTransactionEntity> cdnr;
	
//	@JsonProperty("b2cl")
//	@JsonDeserialize(using=B2CLDeserializer.class)
//	@JsonSerialize(using=B2CLSerializer.class)
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private List<B2CLDetailEntity> b2cl;
//
//	
//	@JsonProperty("cdnr")
//	@JsonDeserialize(using=CDNReturnDeserializer.class)
//	@JsonSerialize(using=CDNSerializer.class)
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private List<CDNDetailEntity> cdnr;
//	
//	@JsonProperty("exp")
//	@JsonDeserialize(using=EXPDeserializer.class)
//	@JsonSerialize(using=EXPSerializer.class)
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private List<EXPTransactionEntity> exp;
//	
//	@JsonProperty("b2cs")
//	@JsonDeserialize(using=B2CSDeserializer.class)
//	@JsonSerialize(using=B2CSSerializer.class)
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private List<B2CSTransactionEntity> b2cs;
//	
//	@JsonProperty("txpd")
//	@JsonDeserialize(using=TXPDeserializer.class)
//	@JsonSerialize(using=TXPSerializer.class)
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private List<TxpdTransaction> txpd;
	///{"status_cd":"0","error":{"message":"Not a valid GSP!","error_cd":"RET11402"}}
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error")
	private Error error;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error_cd")
	private String error_cd;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error_msg")
	private String error_msg;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("token")
	private String token;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("Num_files")
	private String numFiles;
	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Boolean isDelete;
	
}
