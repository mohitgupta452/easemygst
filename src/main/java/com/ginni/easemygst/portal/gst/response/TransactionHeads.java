package com.ginni.easemygst.portal.gst.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.LedgerTax;

import lombok.Data;

public @Data class TransactionHeads {

	@JsonProperty("dt")
	private Date date;
	
	@JsonProperty("desc")
	private String desc;
	
	@JsonProperty("trans_cd")
	private String transactionCode;
	
	@JsonProperty("igst")
	private LedgerTax igst;
	
	@JsonProperty("cgst")
	private LedgerTax cgst;
	
	@JsonProperty("sgst")
	private LedgerTax sgst;
	
	@JsonProperty("cess")
	private LedgerTax cess;
	
	@JsonProperty("tr_typ")
	private String transactionType;
	
	@JsonProperty("bal")
	private Double bal;
	
	@JsonProperty("cl_tot")
	private Double openingTotal;
	
	@JsonProperty("op_tot")
	private Double closingTotal;
	
	@JsonProperty("refNo")
	private String refNo;
	
	@JsonProperty("tr_name")
	private String trName;
	
	@JsonProperty("db_no")
	private String dbNo;
	
	
	
	
}
