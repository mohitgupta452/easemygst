package com.ginni.easemygst.portal.gst.response;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class GetITCLedgerDetailsResp {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("fr_dt")
	private Date fromDate;
	
	@JsonProperty("to_dt")
	private Date toDate;
	
	@JsonProperty("tr")
	private List<TransactionHeads> tr;
	
}
