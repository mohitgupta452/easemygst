package com.ginni.easemygst.portal.gst.api;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.GccMetadata;
import com.mashape.unirest.http.JsonNode;

@Named("gccConsumer")
@RequestScoped
public class GccConsumer implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static Logger log = LoggerFactory.getLogger(GccConsumer.class);

	private ObjectMapper objectMapper;

	private Map<String, String> headers;

	@PostConstruct
	public void init() {
		this.objectMapper = new ObjectMapper();
		this.headers = new HashMap<>();
	}

	public String getData(Object object) {

		try {
			String input = this.objectMapper.writeValueAsString(object);
			JsonNode jsonNode = new JsonNode(input);
			JSONObject json = jsonNode.getObject();
			String output = RestClient.callApi(GccMetadata.ENDPOINT_DOMAIN_DEV + GccMetadata.FUNCTION_DEMO + "/"
					+ json.get("ctin") + "/" + json.get("returnType") + "/" + json.get("transactionType"), "GET", 
					new JsonNode("{}"), this.headers);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
