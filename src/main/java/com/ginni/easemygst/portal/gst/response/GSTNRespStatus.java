package com.ginni.easemygst.portal.gst.response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface GSTNRespStatus {
	
	String SUCCESS="1";
	String FAILED="0";
	
	String CHECKSTATUS_PROCESSED="P";
	String CHECKSTATUS_IN_PROCESS="IP";
	String CHECKSTATUS_PROCESSED_ERROR="PE";
	String CHECKSTATUS_ERROR="ER";
	String CHECKSTATUS_REC_ERROR="REC";
	
	String CHECKSTATUS_PROCESS_MSG="Data processed and synced with GSTN Successfully.";
	String CHECKSTATUS_PROCESS_MSG_AFTER_SOME_TIME="Data processed and synced with GSTN Successfully, Please check data on gst portal after 5-6 minutes";
	String CONTACTSUPPORT_MSG=" Please contact support@easemygst.com for more information.";

	String CHECKSTATUS_IP_MSG="Please click 'Sync Status' after 10 mins for review of your data saved to GSTN ."+CONTACTSUPPORT_MSG;
	String CHECKSTATUS_PE_MSG="Data Processed Successfully with errors.please check the errors and try again.";
	String CHECKSTATUS_ER_MSG="Unable to Sync your data to GSTN. "+CONTACTSUPPORT_MSG;
	String CHECKSTATUS_REC_MSG="Unable to Sync your data to GSTN getting REC status "+CONTACTSUPPORT_MSG;

	String GSTN_AUTHMSG="Please authenticate to GSTN. ";
	String ERR_MSG="Unable to process your request. "+CONTACTSUPPORT_MSG;
	
	String DATA_PRESENT_MSG="Action already submitted.";
	String GET_CALL_IN_PROGRESS="Please Get Data After 45 minutes";
	String UNABLE_TO_GET_FILE="Unable To get file";
	
	String NO_REQUEST_FOUND="No Request Found";

	List<String> GSTN_AUTH_FAILED_CODES=Arrays.asList("RET11401","RET11402","GEN5002","RT-3BGV1002");
	
	
	String GSTN_Auth_Failed1="RET11401";
	String GSTN_Auth_Failed2="RET11402";
	String GSTN_Auth_Failed3="GEN5002";
	
	String GSTN_Auth_Failed_Select_Preferece="AUTH153";
	
	String GSTN_Auth_Failed_Select_Preferece_msg="select \"Monthly\" or \"Quarterly\" option for GSTR1 filing at GST portal with Yes / No";

	
	String  NO_DATA_TO_BE_SYNC="Data is not available to sync from GSTN";
	String TRY_LATTER="Error while calling GSTN please try after some time";
	
	
}
