package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class ViewTrackReturnsResp extends BaseResponse {
	
	@JsonProperty("EFiledlist")
	private java.util.List<EFileDetails> Efiledlist;
}
