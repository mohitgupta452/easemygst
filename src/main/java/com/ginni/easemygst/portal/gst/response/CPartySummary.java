package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.BaseSummaryTotal;

import lombok.Data;

public @Data class CPartySummary extends BaseSummaryTotal {
	
	@JsonProperty("ctin")
	private String ctin;
	
	@JsonProperty("checksum")
	private String checksum;
	
}
