
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tx_py"
})
public @Data class TxPmt {

    @JsonProperty("tx_py")
    private List<TxPy> txPy = null;
    
    @JsonProperty("pdcash")
	private Object pdcash;
	
	@JsonProperty("pditc")
	private Object pditc;


    @JsonProperty("tx_py")
    public List<TxPy> getTxPy() {
        return txPy;
    }

    @JsonProperty("tx_py")
    public void setTxPy(List<TxPy> txPy) {
        this.txPy = txPy;
    }
   
    
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
	
}
