package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class Error {

	@JsonProperty("error_cd")
	private String code;
	
	@JsonProperty("message")
	private String message;
	
}
