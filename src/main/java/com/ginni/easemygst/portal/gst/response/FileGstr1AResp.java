package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class FileGstr1AResp {

	@JsonProperty("status")
	private String status;
	
	@JsonProperty("ack_no")
	private String acknowledgementNo;
}
