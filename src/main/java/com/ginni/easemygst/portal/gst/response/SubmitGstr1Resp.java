package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SubmitGstr1Resp extends BaseResponse{

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("reference_id")
	private String referenceId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("txn_id")
	private String transactionId;
}
