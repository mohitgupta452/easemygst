package com.ginni.easemygst.portal.gst.api;

import java.util.HashMap;
import java.util.Map;

import com.ginni.easemygst.portal.constant.GstMetadata;
import com.ginni.easemygst.portal.helper.AppException;

public class SpiceGsp implements Gsp {

	@Override
	public Map<String, String> authenticate() throws AppException {
		Map<String, String> headers = new HashMap<>();
		headers.put(GstMetadata.HEADER_KEY_ASP_ID, config.getProperty("main.gst.spice.client.id"));
		headers.put(GstMetadata.HEADER_KEY_ASP_SECRET, config.getProperty("main.gst.spice.client.secret"));
		return headers;
	}

	@Override
	public String getUrl() {
		return config.getProperty("main.gst.spice.endpoint.url");
	}

	@Override
	public String getGspName() {
		return "SPICE";
	}

}
