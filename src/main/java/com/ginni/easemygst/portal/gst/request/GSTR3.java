package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.RefundClaim;

import lombok.Data;

public @Data class GSTR3 {

	@JsonProperty("gstin")
	private String gstin;

	@JsonProperty("ret_period")
	private String monthYear;
	
	private RefundClaim refundClaim;
	
	
}
