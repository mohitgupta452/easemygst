package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class Gstr2SummaryReq {

	@JsonProperty("action")
	private String action;
	
	@JsonProperty("gstin")
	public String gstin;
	
	@JsonProperty("ret_period")
	public String returnPeriod;
	
}
