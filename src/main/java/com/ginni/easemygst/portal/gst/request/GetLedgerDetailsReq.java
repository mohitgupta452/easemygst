package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class GetLedgerDetailsReq {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String returnPeriod;
	
	@JsonProperty("action")
	private String action;

	
}
