
package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "intr_details",
    "ltfee_details"
})
public class IntrLtfee {

    @JsonProperty("intr_details")
    private IntrDetails intrDetails;
    @JsonProperty("ltfee_details")
    private LtfeeDetails ltfeeDetails;

    @JsonProperty("intr_details")
    public IntrDetails getIntrDetails() {
        return intrDetails;
    }

    @JsonProperty("intr_details")
    public void setIntrDetails(IntrDetails intrDetails) {
        this.intrDetails = intrDetails;
    }

    @JsonProperty("ltfee_details")
    public LtfeeDetails getLtfeeDetails() {
        return ltfeeDetails;
    }

    @JsonProperty("ltfee_details")
    public void setLtfeeDetails(LtfeeDetails ltfeeDetails) {
        this.ltfeeDetails = ltfeeDetails;
    }


}
