
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "itc_avl",
    "itc_rev",
    "itc_net",
    "itc_inelg"
})
public @Data class ItcElg {

    @JsonProperty("itc_avl")
    private List<TypeDTO> itcAvl = null;
    @JsonProperty("itc_rev")
    private List<TypeDTO> itcRev = null;
    @JsonProperty("itc_net")
    private ItcNet itcNet;
    @JsonProperty("itc_inelg")
    private List<TypeDTO> itcInelg = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

   

}
