package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class GetGstr1AResp {

	public GetGstr1AResp() {
		this.statusCd = "0";
	}

	@JsonProperty("status_cd")
	private String statusCd;
	
	@JsonProperty("b2b")
	private List<B2B> b2b;

	@JsonProperty("b2ba")
	private List<B2B> b2ba;
	
	@JsonProperty("cdn")
	private List<CDN> cdn;

	@JsonProperty("cdna")
	private List<CDN> cdna;
	
	@JsonProperty("error")
	private Error error;
	
}
