
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ret_period",
    "cgstProCrBal",
    "igstProCrBal",
    "sgstProCrBal",
    "cessProCrBal",
    "totProCrBal"
})
public class ProvCrdBal {

    @JsonProperty("ret_period")
    private String retPeriod;
    @JsonProperty("cgstProCrBal")
    private Integer cgstProCrBal;
    @JsonProperty("igstProCrBal")
    private Integer igstProCrBal;
    @JsonProperty("sgstProCrBal")
    private Integer sgstProCrBal;
    @JsonProperty("cessProCrBal")
    private Integer cessProCrBal;
    @JsonProperty("totProCrBal")
    private Integer totProCrBal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ret_period")
    public String getRetPeriod() {
        return retPeriod;
    }

    @JsonProperty("ret_period")
    public void setRetPeriod(String retPeriod) {
        this.retPeriod = retPeriod;
    }

    @JsonProperty("cgstProCrBal")
    public Integer getCgstProCrBal() {
        return cgstProCrBal;
    }

    @JsonProperty("cgstProCrBal")
    public void setCgstProCrBal(Integer cgstProCrBal) {
        this.cgstProCrBal = cgstProCrBal;
    }

    @JsonProperty("igstProCrBal")
    public Integer getIgstProCrBal() {
        return igstProCrBal;
    }

    @JsonProperty("igstProCrBal")
    public void setIgstProCrBal(Integer igstProCrBal) {
        this.igstProCrBal = igstProCrBal;
    }

    @JsonProperty("sgstProCrBal")
    public Integer getSgstProCrBal() {
        return sgstProCrBal;
    }

    @JsonProperty("sgstProCrBal")
    public void setSgstProCrBal(Integer sgstProCrBal) {
        this.sgstProCrBal = sgstProCrBal;
    }

    @JsonProperty("cessProCrBal")
    public Integer getCessProCrBal() {
        return cessProCrBal;
    }

    @JsonProperty("cessProCrBal")
    public void setCessProCrBal(Integer cessProCrBal) {
        this.cessProCrBal = cessProCrBal;
    }

    @JsonProperty("totProCrBal")
    public Integer getTotProCrBal() {
        return totProCrBal;
    }

    @JsonProperty("totProCrBal")
    public void setTotProCrBal(Integer totProCrBal) {
        this.totProCrBal = totProCrBal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
