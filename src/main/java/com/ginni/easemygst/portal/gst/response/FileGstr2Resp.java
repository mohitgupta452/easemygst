package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class FileGstr2Resp {

	@JsonProperty("ack_num")
	private String acknowledgementNo;
	
}
