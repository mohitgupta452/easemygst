package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ginni.easemygst.portal.data.deserialize.ATDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2BDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CLDeserializer;
import com.ginni.easemygst.portal.data.deserialize.B2CSDeserializer;
import com.ginni.easemygst.portal.data.deserialize.CDNDeserializer;
import com.ginni.easemygst.portal.data.deserialize.EXPDeserializer;
import com.ginni.easemygst.portal.data.deserialize.HSNSUMDeserializer;
import com.ginni.easemygst.portal.data.deserialize.NILDeserializer;
import com.ginni.easemygst.portal.data.part.ECOM;
import com.ginni.easemygst.portal.data.serialize.ATSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BSerializer;
import com.ginni.easemygst.portal.data.serialize.B2BURSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CLSerializer;
import com.ginni.easemygst.portal.data.serialize.B2CSSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNSerializer;
import com.ginni.easemygst.portal.data.serialize.CDNURSerializer;
import com.ginni.easemygst.portal.data.serialize.EXPSerializer;
import com.ginni.easemygst.portal.data.serialize.HSNSUMSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPGSerializer;
import com.ginni.easemygst.portal.data.serialize.IMPSBSerializer;
import com.ginni.easemygst.portal.data.serialize.NILSerializer;
import com.ginni.easemygst.portal.data.serialize.TXPSerializer;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

import lombok.Data;

public @Data class GetCheckStatusGstr2Resp extends BaseResponse {
	

	@JsonProperty("status_cd")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String status;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error_report")
	private GetGstr2ReturnResp errorReport;
	
	@JsonProperty("form_typ")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String formType;
	
	@JsonProperty("action")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String action;
	
	
	
	
}
