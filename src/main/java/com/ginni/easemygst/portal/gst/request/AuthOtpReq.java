package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class AuthOtpReq {

	@JsonProperty("action")
	private String action;
	
	@JsonProperty("app_key")
	private String appKey;
	
	@JsonProperty("username")
	private String username;

}
