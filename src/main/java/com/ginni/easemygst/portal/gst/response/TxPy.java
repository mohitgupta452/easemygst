
package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tran_desc",
    "liab_ldg_id",
    "trans_typ",
    "igst",
    "cgst",
    "sgst",
    "cess"
})
public class TxPy {

    @JsonProperty("tran_desc")
    private String tranDesc;
    @JsonProperty("liab_ldg_id")
    private Integer liabLdgId;
    @JsonProperty("trans_typ")
    private Integer transTyp;
    @JsonProperty("igst")
    private Igst igst;
    @JsonProperty("cgst")
    private Cgst cgst;
    @JsonProperty("sgst")
    private Sgst sgst;
    @JsonProperty("cess")
    private Cess cess;


    @JsonProperty("tran_desc")
    public String getTranDesc() {
        return tranDesc;
    }

    @JsonProperty("tran_desc")
    public void setTranDesc(String tranDesc) {
        this.tranDesc = tranDesc;
    }

    @JsonProperty("liab_ldg_id")
    public Integer getLiabLdgId() {
        return liabLdgId;
    }

    @JsonProperty("liab_ldg_id")
    public void setLiabLdgId(Integer liabLdgId) {
        this.liabLdgId = liabLdgId;
    }

    @JsonProperty("trans_typ")
    public Integer getTransTyp() {
        return transTyp;
    }

    @JsonProperty("trans_typ")
    public void setTransTyp(Integer transTyp) {
        this.transTyp = transTyp;
    }

    @JsonProperty("igst")
    public Igst getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(Igst igst) {
        this.igst = igst;
    }

    @JsonProperty("cgst")
    public Cgst getCgst() {
        return cgst;
    }

    
    
    @JsonProperty("cgst")
    public void setCgst(Cgst cgst) {
        this.cgst = cgst;
    }

    @JsonProperty("sgst")
    public Sgst getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(Sgst sgst) {
        this.sgst = sgst;
    }

    @JsonProperty("cess")
    public Cess getCess() {
        return cess;
    }

    @JsonProperty("cess")
    public void setCess(Cess cess) {
        this.cess = cess;
    }

}
