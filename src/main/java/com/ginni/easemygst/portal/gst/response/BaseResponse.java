package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

abstract public @Data class BaseResponse {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("status_cd")
	protected String status;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error")
	protected Error error;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("gsp")
	protected String gsp;
}
