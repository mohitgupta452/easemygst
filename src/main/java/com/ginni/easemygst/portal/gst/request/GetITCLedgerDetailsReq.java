package com.ginni.easemygst.portal.gst.request;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class GetITCLedgerDetailsReq {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("fr_dt")
	private Date fromDate;
	
	@JsonProperty("to_dt")
	private Date toDate;
	
}
