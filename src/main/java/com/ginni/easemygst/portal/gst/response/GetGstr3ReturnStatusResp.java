package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class GetGstr3ReturnStatusResp {

	@JsonProperty("error_det")
	private String errorMessage;
	
	@JsonProperty("state_cd")
	private String statusCode;
	
	@JsonProperty("arn_no")
	private String acknowledgementNo;
	
	@JsonProperty("error_cd")
	private String errorCode;
	
}
