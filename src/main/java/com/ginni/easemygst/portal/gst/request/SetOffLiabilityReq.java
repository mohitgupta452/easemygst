package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SetOffLiabilityReq {

	@JsonProperty("action")
	private String action;
	
	@JsonProperty("data")
	private String data;
	
	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String monthYear;
	
	
	
}
