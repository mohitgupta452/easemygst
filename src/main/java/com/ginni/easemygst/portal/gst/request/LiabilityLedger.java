package com.ginni.easemygst.portal.gst.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.part.LedgerLiabilityTransactionDetail;

import lombok.Data;

public @Data class LiabilityLedger {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String monthYear;
	
	@JsonProperty("tr")
	private List<LedgerLiabilityTransactionDetail> transactions;
	
}
