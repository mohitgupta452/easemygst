package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SaveGstr3Resp {

	@JsonProperty("status")
	private String status;
	
	@JsonProperty("ack_num")
	private String acknowledgementNo;

	
}
