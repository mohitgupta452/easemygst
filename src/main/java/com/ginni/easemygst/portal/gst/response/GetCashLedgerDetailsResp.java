package com.ginni.easemygst.portal.gst.response;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ginni.easemygst.portal.utils.CustomJsonDateDeserializer;

import lombok.Data;

public  @Data class GetCashLedgerDetailsResp {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("fr_dt")
	@JsonDeserialize(using=CustomJsonDateDeserializer.class)
	private Date fromDate;

	@JsonProperty("to_dt")
	@JsonDeserialize(using=CustomJsonDateDeserializer.class)
	private Date toDate;
	
	@JsonProperty("tr")
	@JsonIgnore
	private List<TransactionHeads> tr;
	
	
	@JsonProperty("op_bal")
	private OCBal openingBalance;
	
	@JsonProperty("cl_bal")
	private OCBal closingBalance;

	
	@JsonIgnore
	public List<TransactionHeads> getTr() {
		return tr;
	}

	@JsonIgnore
	public void setTr(List<TransactionHeads> tr) {
		this.tr = tr;
	}
	
	

	
	
}
