package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class AuthRefreshTokenReq {

	@JsonProperty("action")
	private String action;
	
	@JsonProperty("app_key")
	private String appKey;
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("auth_token")
	private String authToken;
}
