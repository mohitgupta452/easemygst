package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class TypeDTO {
	@JsonProperty("ty")
	private String ty;
	@JsonProperty("iamt")
	private Double iamt;
	@JsonProperty("camt")
	private Double camt;
	@JsonProperty("samt")
	private Double samt;
	@JsonProperty("csamt")
	private Double csamt;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}
