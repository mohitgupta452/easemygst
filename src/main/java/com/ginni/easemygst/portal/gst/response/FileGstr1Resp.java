package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class FileGstr1Resp extends BaseResponse {
	
	@JsonProperty("ack_num")
	private String acknowledgementNo;
	
}
