package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class FileGstr3Resp {

	@JsonProperty("status")
	private String status;
	
	@JsonProperty("Ack_num")
	private String ackNum;
	
}
