
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tx",
    "intr",
    "fee"
})
public class Igst {

    @JsonProperty("tx")
    private Double tx;
    @JsonProperty("intr")
    private Double intr;
    @JsonProperty("fee")
    private Double fee;

    @JsonProperty("tx")
    public Double getTx() {
        return tx;
    }

    @JsonProperty("tx")
    public void setTx(Double tx) {
        this.tx = tx;
    }

    @JsonProperty("intr")
    public Double getIntr() {
        return intr;
    }

    @JsonProperty("intr")
    public void setIntr(Double intr) {
        this.intr = intr;
    }

    @JsonProperty("fee")
    public Double getFee() {
        return fee;
    }

    @JsonProperty("fee")
    public void setFee(Double fee) {
        this.fee = fee;
    }

}
