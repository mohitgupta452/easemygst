package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

public @Data class SubmitGstr1Req {

	private static final ObjectMapper _MAPPER=new ObjectMapper();

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String retPeriod;
	
	@JsonProperty("action")
	private String action;
	
	@JsonGetter("data")
	private String getData(){
		return _MAPPER.createObjectNode().put("gstin",this.gstin).put("ret_period",this.retPeriod).asText();
	}
}
