
package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "unreg_details",
    "comp_details",
    "uin_details"
})
public class InterSup {

    @JsonProperty("unreg_details")
    private List<Object> unregDetails = null;
    @JsonProperty("comp_details")
    private List<Object> compDetails = null;
    @JsonProperty("uin_details")
    private List<Object> uinDetails = null;

    @JsonProperty("unreg_details")
    public List<Object> getUnregDetails() {
        return unregDetails;
    }

    @JsonProperty("unreg_details")
    public void setUnregDetails(List<Object> unregDetails) {
        this.unregDetails = unregDetails;
    }

    @JsonProperty("comp_details")
    public List<Object> getCompDetails() {
        return compDetails;
    }

    @JsonProperty("comp_details")
    public void setCompDetails(List<Object> compDetails) {
        this.compDetails = compDetails;
    }

    @JsonProperty("uin_details")
    public List<Object> getUinDetails() {
        return uinDetails;
    }

    @JsonProperty("uin_details")
    public void setUinDetails(List<Object> uinDetails) {
        this.uinDetails = uinDetails;
    }


}
