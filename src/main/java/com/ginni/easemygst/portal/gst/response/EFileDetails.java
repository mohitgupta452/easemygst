package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class EFileDetails {
	
	@JsonProperty("arn")
	private String arnNumber;
	
	@JsonProperty("ret_prd")
	private  String returnPeriod;
	
	@JsonProperty("mof")
	private String modeOfFilling;
	
	@JsonProperty("dof")
	private String dateOfFilling;
	
	@JsonProperty("rtntype")
	private String returnType;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("valid")
	private String isValidReturn;
	

}
