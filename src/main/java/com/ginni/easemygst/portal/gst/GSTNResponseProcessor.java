package com.ginni.easemygst.portal.gst;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.gst.response.BaseResponse;
import com.ginni.easemygst.portal.gst.response.GSTNRespStatus;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.notify.LegacyEmail;

@RequestScoped
public class GSTNResponseProcessor {

	@Inject
	private TaxpayerService taxpayerService;

	public  void processGstnResponse(BaseResponse baseResponse,String gstin) throws AppException{
		
		String msg=null;
		String exceptionCode=ExceptionCode._FAILURE;
		if(baseResponse != null){
			String status=baseResponse.getStatus();
			if(!StringUtils.isEmpty(status) && GSTNRespStatus.FAILED.equalsIgnoreCase(status)){
				if(baseResponse.getError()!= null && !StringUtils.isEmpty(baseResponse.getError().getCode())){
					String errCode=baseResponse.getError().getCode();
					if(GSTNRespStatus.GSTN_AUTH_FAILED_CODES.contains(errCode)){
						msg=GSTNRespStatus.GSTN_AUTHMSG;
						markUnAuthToGSTNUser(gstin);		
						exceptionCode=ExceptionCode._UNAUTHORISED_GSTIN;
						}
					else if(GSTNRespStatus.GSTN_Auth_Failed_Select_Preferece.equalsIgnoreCase(errCode)){
						msg=GSTNRespStatus.GSTN_Auth_Failed_Select_Preferece_msg;
					}
					else
					msg=baseResponse.getError().getMessage();
				}
				throw new AppException(exceptionCode,
						StringUtils.isEmpty(msg) ? GSTNRespStatus.ERR_MSG : msg + GSTNRespStatus.CONTACTSUPPORT_MSG);
			}
		}

	}

	private void markUnAuthToGSTNUser(String gstin) {
		taxpayerService.markGstnUnAuth(gstin);
	}
}
