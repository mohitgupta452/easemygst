package com.ginni.easemygst.portal.gst.api;

import java.util.Map;
import java.util.Properties;

import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;

public interface Gsp {

	public RedisService redisService = new RedisService();
	
	public Properties config = new Properties();

	public Map<String, String> authenticate() throws AppException;
	
	public String getUrl();
	
	public String getGspName();

	
}
