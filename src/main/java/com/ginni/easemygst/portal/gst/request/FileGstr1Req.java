package com.ginni.easemygst.portal.gst.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class FileGstr1Req {
	
	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String retPeriod;
	
	@JsonProperty("action")
	private String action;
	
	@JsonProperty("data")
	private String b64Summary;
	
	@JsonProperty("sign")
	private String b64SignData;
	
	@JsonProperty("st")//DSC or ESIGN
	private String signType;
	
	@JsonProperty("sid")//pan no or uid
	private String signId;
	
}
