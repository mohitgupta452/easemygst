package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.BaseSummaryTotal;

import lombok.Data;

public @Data class Gstr1ASummaryResp extends BaseSummaryTotal {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("ret_period")
	private String returnPeriod;
	
	@JsonProperty("checksum")
	private String checksum;
	
	@JsonProperty("sec_sum")
	private List<SectionSummary> secSummary;
	
}
