package com.ginni.easemygst.portal.gst.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class LiabilityLedgerDetail {

	@JsonProperty("gstin")
	private String gstin;
	
	@JsonProperty("rt_period")
	private String monthYear;
	
	@JsonProperty("tr")
	private List<TransactionHeads> tr;
	
	@JsonProperty("amt_overdue")
	private TransactionHeads amountOverdue;
	
}
