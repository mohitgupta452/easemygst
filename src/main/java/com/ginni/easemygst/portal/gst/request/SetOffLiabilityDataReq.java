package com.ginni.easemygst.portal.gst.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.data.part.Gstr3Summary;
import com.ginni.easemygst.portal.data.part.Gstr3Summary.Gstr3TransactionSummary;

import lombok.Data;

public @Data class SetOffLiabilityDataReq {
	
	private String gstin;
	
	private String monthYear;
	
	@JsonProperty("action")
	@JsonInclude(Include.NON_NULL)
	private String action;
	
	@JsonProperty("tx_offset")
	@JsonInclude(Include.NON_NULL)
	private TaxOffset taxOffset;
	
	@JsonProperty("intr_offset")
	@JsonInclude(Include.NON_NULL)
	private IntrOffset intrOffset;
	
	@JsonProperty("lfee_offset")
	@JsonInclude(Include.NON_NULL)
	private LateFee latefeeOffset;
	
	
	@Data  public class TaxOffset{
		@JsonProperty("pdcash")
		@JsonInclude(Include.NON_NULL)
		private PaidCash paidCash;
		
		@JsonProperty("pditc")
		@JsonInclude(Include.NON_NULL)
		private PaidITC paidItc;
	}
	
	@Data  public class LateFee{
		@JsonProperty("c_pdlfee")
		@JsonFormat(pattern = "#0.00")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal latefeeCgst = new BigDecimal(0.00);
		
		@JsonProperty("s_pdlfee")
		@JsonFormat(pattern = "#0.00")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal latefeeSgst = new BigDecimal(0.00);
	}
	
	@Data  public class IntrOffset{
		@JsonProperty("i_pdint")
		@JsonFormat(pattern = "#0.00")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal igstpiadInterest = new BigDecimal(0.00);
		
		@JsonProperty("c_pdint")
		@JsonFormat(pattern = "#0.00")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal cgstpaidInterest = new BigDecimal(0.00);
		
		@JsonProperty("s_pdint")
		@JsonFormat(pattern = "#0.00")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal sgstpaidInterest = new BigDecimal(0.00);
		
		@JsonProperty("cs_pdint")
		@JsonFormat(pattern = "#0.00")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal cesspaidInterest = new BigDecimal(0.00);
	}

	 @Data  public static class PaidCash {
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("ipd")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal igstAmt = new BigDecimal(0.00);

		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cpd")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal cgstAmt = new BigDecimal(0.00);
	    
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("spd")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal sgstAmt = new BigDecimal(0.00);
		
		@JsonFormat(pattern = "#0.00")
		@JsonProperty("cspd")
		@JsonInclude(Include.NON_NULL)
	    public BigDecimal cessAmt = new BigDecimal(0.00);
		
	}
	 
	 @Data  public static class PaidITC {
			
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("i_pdi")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal igstpaidIgst = new BigDecimal(0.00);

			@JsonFormat(pattern = "#0.00")
			@JsonProperty("i_pdc")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal igstpaidCgst = new BigDecimal(0.00);
		    
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("i_pds")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal igstpaidSgst = new BigDecimal(0.00);
			
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("c_pdi")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal cgstpaidIgst = new BigDecimal(0.00);
			
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("c_pdc")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal cgstpaidCgst = new BigDecimal(0.00);
			
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("s_pdi")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal sgstpaidIgst = new BigDecimal(0.00);
			
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("s_pds")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal sgstpaidSgst = new BigDecimal(0.00);
			
			@JsonFormat(pattern = "#0.00")
			@JsonProperty("cs_pdcs")
			@JsonInclude(Include.NON_NULL)
		    public BigDecimal cesspaidCess = new BigDecimal(0.00);
		}
	
}



