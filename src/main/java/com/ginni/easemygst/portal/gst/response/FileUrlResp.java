package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data  class FileUrlResp {

 @JsonProperty("ul")	
 private String ul;
 
 @JsonProperty("ic")
 private String ic;
 
 @JsonProperty("hash")
 private String hash;
	

}
