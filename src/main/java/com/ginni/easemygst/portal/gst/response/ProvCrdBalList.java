
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "provCrdBal"
})
public class ProvCrdBalList {

    @JsonProperty("provCrdBal")
    private List<ProvCrdBal> provCrdBal = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("provCrdBal")
    public List<ProvCrdBal> getProvCrdBal() {
        return provCrdBal;
    }

    @JsonProperty("provCrdBal")
    public void setProvCrdBal(List<ProvCrdBal> provCrdBal) {
        this.provCrdBal = provCrdBal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
