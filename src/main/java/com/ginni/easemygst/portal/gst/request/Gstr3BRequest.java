package com.ginni.easemygst.portal.gst.request;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.InterestAndLateFeePayble.IntrLateFeeDetail;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.InwardSupply.InwardSupplyDetail;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.ItcEligibility.ItcDetail;
import com.ginni.easemygst.portal.gst.request.Gstr3BRequest.SupplyInter.DetailInter;
import com.ginni.easemygst.portal.persistence.entity.TblOutwardCombinedNew;

import lombok.Data;

@JsonInclude(value = Include.NON_NULL)
public class Gstr3BRequest {

	@JsonProperty("action")
	private String action;

	@JsonProperty("gstin")
	private String gstin;

	@JsonProperty("ret_period")
	private String returnPeriod;

	@JsonProperty("sup_details")
	private SupplyDetail supplyDetail;

	@JsonProperty("inter_sup")
	private SupplyInter supplyInter;

	@JsonProperty("itc_elg")
	private ItcEligibility itcDetail;

	@JsonProperty("inward_sup")
	private InwardSupply inwardSupply;

	@JsonProperty("intr_ltfee")
	private InterestAndLateFeePayble interLateFeePayble;

	public Gstr3BRequest() {
		// TODO Auto-generated constructor stub
	}

	public Gstr3BRequest(List<TblOutwardCombinedNew> part1AndPart5s,
			List<TblOutwardCombinedNew> AllExceptPart1AndPart5s) {
		this.supplyDetail = new SupplyDetail();
		this.supplyInter = new SupplyInter();
		this.itcDetail = new ItcEligibility();
		this.inwardSupply = new InwardSupply();
		this.interLateFeePayble = new InterestAndLateFeePayble();
		IntrLateFeeDetail interest = new IntrLateFeeDetail();
		IntrLateFeeDetail ltFee = new IntrLateFeeDetail();

		List<TblOutwardCombinedNew> datas = new ArrayList<>();
		datas.addAll(part1AndPart5s);
		datas.addAll(AllExceptPart1AndPart5s);

		for (TblOutwardCombinedNew part : part1AndPart5s) {
			Gstr3BTransactionType type = Gstr3BTransactionType.valueOf(part.getTransaction_Type());
			switch (type) {
			case PART1A:
				this.getSupplyDetail().setOutTaxableSup(new OutwardSupplyDetail(part));
				break;

			case PART1B:
				this.getSupplyDetail().setOutSupZeroRated(new ZeroDetail(part));
				break;
			case PART1C:
				this.getSupplyDetail().setOutSupNilRatedExempted(new Detail(part));
				break;

			case PART1D:
				this.getSupplyDetail().setInwardSupRevCharge(new OutwardSupplyDetail(part));
				break;
			case PART1E:
				this.getSupplyDetail().setOutSupNonGst(new Detail(part));
				break;

			case PART5A:
				interest = new IntrLateFeeDetail(part);
				// this.getInterLateFeePayble().setInterestLateFeeDetails(new
				// IntrLateFeeDetail(part));
				break;
			case PART5B:
				ltFee = new IntrLateFeeDetail(part);
				// this.getInterLateFeePayble().setInterestLateFeeDetails(new
				// IntrLateFeeDetail(part));
				break;

			default:
				System.out.println("invalid part type");

			}

		}

		IntrLateFeeDetail interestAndLateFee = new IntrLateFeeDetail();
		interestAndLateFee.setIgstAmount(interest.getIgstAmount().add(ltFee.getIgstAmount()));
		interestAndLateFee.setCgstAmount(interest.getCgstAmount().add(ltFee.getCgstAmount()));
		interestAndLateFee.setSgstAmount(interest.getSgstAmount().add(ltFee.getSgstAmount()));
		interestAndLateFee.setCessAmount(interest.getCessAmount().add(ltFee.getCessAmount()));
		this.getInterLateFeePayble().setInterestLateFeeDetails(interestAndLateFee);

		Map<String, List<TblOutwardCombinedNew>> groupByPartMap = AllExceptPart1AndPart5s.stream()
				.collect(Collectors.groupingBy(TblOutwardCombinedNew::getTransaction_Type));
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART2A.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART2A.toString())) {
				if (!Objects.isNull(part.getTo_State())) {
					if (Objects.isNull(this.supplyInter))
						this.supplyInter = new SupplyInter();
					this.supplyInter.getUnregPersonDetails().add(new DetailInter(part));
				}
			}
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART2B.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART2B.toString())) {
				if (!Objects.isNull(part.getTo_State())) {
					if (Objects.isNull(this.supplyInter))
						this.supplyInter = new SupplyInter();
					this.supplyInter.getCompTaxablePersonDetails().add(new DetailInter(part));
				}
			}
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART2C.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART2C.toString())) {
				if (!Objects.isNull(part.getTo_State())) {
					if (Objects.isNull(this.supplyInter))
						this.supplyInter = new SupplyInter();
					this.supplyInter.getUinDetails().add(new DetailInter(part));
				}
			}
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3A.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3A.toString())) {

				ItcDetail itcDetail = new ItcDetail(part);

				if (this.getItcDetail().getAvailableItc().contains(itcDetail)) {
					ItcDetail existingdetail = this.getItcDetail().getAvailableItc()
							.get(this.getItcDetail().getAvailableItc().indexOf(itcDetail));
					existingdetail.setCessAmount(itcDetail.getCessAmount());
					existingdetail.setCgstAmount(itcDetail.getCgstAmount());
					existingdetail.setIgstAmount(itcDetail.getIgstAmount());
					existingdetail.setSgstAmount(itcDetail.getSgstAmount());
				} else {
					this.getItcDetail().getAvailableItc().add(itcDetail);
				}
			}
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3B.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3B.toString())) {
				ItcDetail itcDetail = new ItcDetail(part);

				if (this.getItcDetail().getAvailableItc().contains(itcDetail)) {
					ItcDetail existingdetail = this.getItcDetail().getAvailableItc()
							.get(this.getItcDetail().getAvailableItc().indexOf(itcDetail));
					existingdetail.setCessAmount(itcDetail.getCessAmount());
					existingdetail.setCgstAmount(itcDetail.getCgstAmount());
					existingdetail.setIgstAmount(itcDetail.getIgstAmount());
					existingdetail.setSgstAmount(itcDetail.getSgstAmount());
				} else {
					this.getItcDetail().getAvailableItc().add(itcDetail);
				}
			}
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3C.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3C.toString())) {
				ItcDetail itcDetail = new ItcDetail(part);

				if (this.getItcDetail().getAvailableItc().contains(itcDetail)) {
					ItcDetail existingdetail = this.getItcDetail().getAvailableItc()
							.get(this.getItcDetail().getAvailableItc().indexOf(itcDetail));
					existingdetail.setCessAmount(itcDetail.getCessAmount());
					existingdetail.setCgstAmount(itcDetail.getCgstAmount());
					existingdetail.setIgstAmount(itcDetail.getIgstAmount());
					existingdetail.setSgstAmount(itcDetail.getSgstAmount());
				} else {
					this.getItcDetail().getAvailableItc().add(itcDetail);
				}
			}
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3D.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3D.toString())) {
				ItcDetail itcDetail = new ItcDetail(part);

				if (this.getItcDetail().getAvailableItc().contains(itcDetail)) {
					ItcDetail existingdetail = this.getItcDetail().getAvailableItc()
							.get(this.getItcDetail().getAvailableItc().indexOf(itcDetail));
					existingdetail.setCessAmount(itcDetail.getCessAmount());
					existingdetail.setCgstAmount(itcDetail.getCgstAmount());
					existingdetail.setIgstAmount(itcDetail.getIgstAmount());
					existingdetail.setSgstAmount(itcDetail.getSgstAmount());
				} else {
					this.getItcDetail().getAvailableItc().add(itcDetail);
				}
			}
		}

		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3F.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3F.toString())) {
				ItcDetail itcDetail = new ItcDetail(part);

				if (this.getItcDetail().getAvailableItc().contains(itcDetail)) {
					ItcDetail existingdetail = this.getItcDetail().getAvailableItc()
							.get(this.getItcDetail().getAvailableItc().indexOf(itcDetail));
					existingdetail.setCessAmount(itcDetail.getCessAmount());
					existingdetail.setCgstAmount(itcDetail.getCgstAmount());
					existingdetail.setIgstAmount(itcDetail.getIgstAmount());
					existingdetail.setSgstAmount(itcDetail.getSgstAmount());
				} else {
					this.getItcDetail().getAvailableItc().add(itcDetail);
				}
			}
		}

		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3G.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3G.toString())) {
				this.getItcDetail().getItcReversed().add(new ItcDetail(part));
			}
		}

		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3H.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3H.toString())) {
				this.getItcDetail().getItcReversed().add(new ItcDetail(part));
			}
		}

		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3J.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3J.toString())) {
				this.getItcDetail().getIneligibleItc().add(new ItcDetail(part));
			}
		}

		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART3K.toString())) {
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART3K.toString())) {
				this.getItcDetail().getIneligibleItc().add(new ItcDetail(part));
			}
		}

		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART4A.toString())) {
			InwardSupplyDetail consolidated = new InwardSupplyDetail();
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART4A.toString())) {
				InwardSupplyDetail temp = new InwardSupplyDetail(part);
				consolidated.setInter(consolidated.getInter().add(temp.getInter()));
				consolidated.setIntra(consolidated.getIntra().add(temp.getIntra()));
				consolidated.setInwardSupType(temp.getInwardSupType());
				/*
				 * this.getInwardSupply().getInwardSupDetail().add(new
				 * InwardSupplyDetailDto(part));
				 */ }
			this.getInwardSupply().getInwardSupDetail().add(consolidated);
		} else {
			TblOutwardCombinedNew tempPart = new TblOutwardCombinedNew();
			tempPart.setSupply_Type(SupplyType.INTER.toString());
			tempPart.setTransaction_Type(Gstr3BTransactionType.PART4A.toString());
			this.getInwardSupply().getInwardSupDetail().add(new InwardSupplyDetail(tempPart));
		}
		if (groupByPartMap.containsKey(Gstr3BTransactionType.PART4B.toString())) {
			InwardSupplyDetail consolidated = new InwardSupplyDetail();
			for (TblOutwardCombinedNew part : groupByPartMap.get(Gstr3BTransactionType.PART4B.toString())) {
				InwardSupplyDetail temp = new InwardSupplyDetail(part);
				consolidated.setInter(consolidated.getInter().add(temp.getInter()));
				consolidated.setIntra(consolidated.getIntra().add(temp.getIntra()));
				consolidated.setInwardSupType(temp.getInwardSupType());
				// this.getInwardSupply().getInwardSupDetail().add(new
				// InwardSupplyDetailDto(part));
			}
			this.getInwardSupply().getInwardSupDetail().add(consolidated);
		} else {
			TblOutwardCombinedNew tempPart = new TblOutwardCombinedNew();
			tempPart.setTransaction_Type(Gstr3BTransactionType.PART4B.toString());
			tempPart.setSupply_Type(SupplyType.INTER.toString());
			this.getInwardSupply().getInwardSupDetail().add(new InwardSupplyDetail(tempPart));
		}

		// setting net itc
		ItcDetail netItc = new ItcDetail();
		double igstAvailable = 0;
		double cgstAvailable = 0;
		double sgstAvailable = 0;
		double cessAvailable = 0;
		double igstReversed = 0;
		double cgstReversed = 0;
		double sgstReversed = 0;
		double cessReversed = 0;

		for (ItcDetail itc : this.getItcDetail().getAvailableItc()) {
			igstAvailable = igstAvailable + itc.getIgstAmount().doubleValue();
			cgstAvailable = cgstAvailable + itc.getCgstAmount().doubleValue();
			sgstAvailable = sgstAvailable + itc.getSgstAmount().doubleValue();
			cessAvailable = cessAvailable + itc.getCessAmount().doubleValue();
		}

		if (!Objects.isNull(this.getItcDetail().getItcReversed())) {
			for (ItcDetail itc : this.getItcDetail().getItcReversed()) {
				igstReversed = igstReversed + itc.getIgstAmount().doubleValue();
				cgstReversed = cgstReversed + itc.getCgstAmount().doubleValue();
				sgstReversed = sgstReversed + itc.getSgstAmount().doubleValue();
				cessReversed = cessReversed + itc.getCessAmount().doubleValue();
			}
		}
		netItc.setIgstAmount(new BigDecimal(igstAvailable - igstReversed).setScale(2, RoundingMode.HALF_UP));
		netItc.setCgstAmount(new BigDecimal(cgstAvailable - cgstReversed).setScale(2, RoundingMode.HALF_UP));
		netItc.setSgstAmount(new BigDecimal(sgstAvailable - sgstReversed).setScale(2, RoundingMode.HALF_UP));
		netItc.setCessAmount(new BigDecimal(cessAvailable - cessReversed).setScale(2, RoundingMode.HALF_UP));
		this.getItcDetail().setNetItcAvailable(netItc);
		// end

	}

	public enum Gstr3BTransactionType {
		PART1A("PART1A"), PART1B("PART1B"), PART1C("PART1C"), PART1D("PART1D"), PART1E("PART1E"), PART2A(
				"PART2A"), PART2B("PART2B"), PART2C("PART2C"), PART3A("PART3A"), PART3B("PART3B"), PART3C(
						"PART3C"), PART3D("PART3D"), PART3E("PART3E"), PART3F("PART3F"), PART3G("PART3G"), PART3H(
								"PART3H"), PART3I("PART3I"), PART3J("PART3J"), PART3K("PART3K"), PART4A(
										"PART4A"), PART4B("PART4B"), PART5A("PART5A"), PART5B("PART5B")

		;

		String type;

		Gstr3BTransactionType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum ItcDetailType {
		IMPG("IMPG"), IMPS("IMPS"), ISRC("ISRC"), ISD("ISD"), RUL("RUL"), OTH("OTH");

		String type;

		ItcDetailType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum InwardSupplyType {
		GST("GST"), NONGST("NONGST");

		String type;

		InwardSupplyType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type;
		};
	}

	public static @Data class BaseAmount {
		@JsonProperty("txval")
		private BigDecimal taxableValue = BigDecimal.ZERO;

		@JsonProperty("iamt")
		private BigDecimal igstAmount = BigDecimal.ZERO;

		@JsonProperty("camt")
		private BigDecimal cgstAmount = BigDecimal.ZERO;

		@JsonProperty("samt")
		private BigDecimal sgstAmount = BigDecimal.ZERO;

		@JsonProperty("csamt")
		private BigDecimal cessAmount = BigDecimal.ZERO;

		public BaseAmount(double igst, double cgst, double sgst, double cess, double taxableValue) {
			this.igstAmount = new BigDecimal(igst).setScale(2, RoundingMode.HALF_UP);
			this.cgstAmount = new BigDecimal(cgst).setScale(2, RoundingMode.HALF_UP);
			this.sgstAmount = new BigDecimal(sgst).setScale(2, RoundingMode.HALF_UP);
			this.cessAmount = new BigDecimal(cess).setScale(2, RoundingMode.HALF_UP);
			this.taxableValue = new BigDecimal(taxableValue).setScale(2, RoundingMode.HALF_UP);
		}

		public BaseAmount() {

		}

	}

	public static class SupplyDetail {

		@JsonProperty("osup_det")
		private OutwardSupplyDetail outTaxableSup;

		@JsonProperty("osup_zero")
		private ZeroDetail outSupZeroRated;

		@JsonProperty("osup_nil_exmp")
		private Detail outSupNilRatedExempted;

		@JsonProperty("isup_rev")
		private OutwardSupplyDetail inwardSupRevCharge;

		@JsonProperty("osup_nongst")
		private Detail outSupNonGst;

		public SupplyDetail() {
			this.outTaxableSup = new OutwardSupplyDetail();
			this.outSupZeroRated = new ZeroDetail();
			this.outSupNilRatedExempted = new Detail();
			this.inwardSupRevCharge = new OutwardSupplyDetail();
			this.outSupNonGst = new Detail();

		}

		public OutwardSupplyDetail getOutTaxableSup() {
			return outTaxableSup;
		}

		public void setOutTaxableSup(OutwardSupplyDetail outTaxableSup) {
			this.outTaxableSup = outTaxableSup;
		}

		public ZeroDetail getOutSupZeroRated() {
			return outSupZeroRated;
		}

		public void setOutSupZeroRated(ZeroDetail outSupZeroRated) {
			this.outSupZeroRated = outSupZeroRated;
		}

		public Detail getOutSupNilRatedExempted() {
			return outSupNilRatedExempted;
		}

		public void setOutSupNilRatedExempted(Detail outSupNilRatedExempted) {
			this.outSupNilRatedExempted = outSupNilRatedExempted;
		}

		public OutwardSupplyDetail getInwardSupRevCharge() {
			return inwardSupRevCharge;
		}

		public void setInwardSupRevCharge(OutwardSupplyDetail inwardSupRevCharge) {
			this.inwardSupRevCharge = inwardSupRevCharge;
		}

		public Detail getOutSupNonGst() {
			return outSupNonGst;
		}

		public void setOutSupNonGst(Detail outSupNonGst) {
			this.outSupNonGst = outSupNonGst;
		}
	}

	public static class OutwardSupplyDetail extends BaseAmount {

		public OutwardSupplyDetail() {
			super();
		}

		public OutwardSupplyDetail(double igst, double cgst, double sgst, double cess, double taxableValue) {
			super(igst, cgst, sgst, cess, taxableValue);
		}

		public OutwardSupplyDetail(TblOutwardCombinedNew part) {
			super(part.getIgst(), part.getCgst(), part.getSgst(), part.getCess(), part.getTaxable_Value());
		}

	}

	public @Data static class ZeroDetail {
		@JsonProperty("iamt")
		private BigDecimal igstAmount = BigDecimal.ZERO;

		@JsonProperty("csamt")
		private BigDecimal cessAmount = BigDecimal.ZERO;

		@JsonProperty("txval")
		private BigDecimal taxableValue = BigDecimal.ZERO;
		
		@JsonProperty("samt")
		private BigDecimal sgst = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		@JsonProperty("camt")
		private BigDecimal cgst = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		public ZeroDetail(TblOutwardCombinedNew part) {
			this.igstAmount = new BigDecimal(part.getIgst()).setScale(2, RoundingMode.HALF_UP);
			this.cessAmount = new BigDecimal(part.getCgst()).setScale(2, RoundingMode.HALF_UP);
			this.taxableValue = new BigDecimal(part.getTaxable_Value()).setScale(2, RoundingMode.HALF_UP);

		}

		public ZeroDetail() {

		}
	}

	public @Data static class Detail {

		@JsonProperty("txval")
		private BigDecimal taxableValue = BigDecimal.ZERO;

		@JsonProperty("iamt")
		private BigDecimal igst = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		@JsonProperty("samt")
		private BigDecimal sgst = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		@JsonProperty("camt")
		private BigDecimal cgst = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		@JsonProperty("csamt")
		private BigDecimal cess = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		public Detail(TblOutwardCombinedNew part) {
			this.taxableValue = new BigDecimal(part.getTaxable_Value()).setScale(2, RoundingMode.HALF_UP);
		}

		public Detail() {
		}

	}

	public static class SupplyInter {

		@JsonProperty("unreg_details")
		private List<DetailInter> unregPersonDetails = new ArrayList<>();

		@JsonProperty("comp_details")
		private List<DetailInter> compTaxablePersonDetails = new ArrayList<>();

		@JsonProperty("uin_details")
		private List<DetailInter> uinDetails = new ArrayList<>();

		public @Data static class DetailInter {
			@JsonProperty("pos")
			private String pos;

			@JsonProperty("txval")
			private BigDecimal taxableValue;

			@JsonProperty("iamt")
			private BigDecimal igstAmount;

			public DetailInter(TblOutwardCombinedNew part) {
				this.igstAmount = new BigDecimal(part.getIgst()).setScale(2, RoundingMode.HALF_UP);
				this.taxableValue = new BigDecimal(part.getTaxable_Value()).setScale(2, RoundingMode.HALF_UP);
				this.pos = part.getTo_State();
				if (!StringUtils.isEmpty(part.getTo_State())) {
					if (part.getTo_State().length() == 1)
						this.pos = "0" + part.getTo_State();
				}
			}

		}

		public List<DetailInter> getUnregPersonDetails() {
			return unregPersonDetails;
		}

		public void setUnregPersonDetails(List<DetailInter> unregPersonDetails) {
			this.unregPersonDetails = unregPersonDetails;
		}

		public List<DetailInter> getCompTaxablePersonDetails() {
			return compTaxablePersonDetails;
		}

		public void setCompTaxablePersonDetails(List<DetailInter> compTaxablePersonDetails) {
			this.compTaxablePersonDetails = compTaxablePersonDetails;
		}

		public List<DetailInter> getUinDetails() {
			return uinDetails;
		}

		public void setUinDetails(List<DetailInter> uinDetails) {
			this.uinDetails = uinDetails;
		}
	}

	public static class ItcEligibility {

		@JsonProperty("itc_avl")
		private List<ItcDetail> availableItc = new ArrayList<>();

		@JsonProperty("itc_rev")
		private List<ItcDetail> itcReversed = new ArrayList<>();

		@JsonProperty("itc_net")
		private ItcDetail netItcAvailable = new ItcDetail();

		@JsonProperty("itc_inelg")
		private List<ItcDetail> ineligibleItc = new ArrayList<>();

		@JsonInclude(value=Include.NON_NULL)
		public static class ItcDetail {
			@JsonProperty("ty")
			private ItcDetailType itcDetailType;

			@JsonProperty("iamt")
			private BigDecimal igstAmount = BigDecimal.ZERO;

			@JsonProperty("camt")
			private BigDecimal cgstAmount = BigDecimal.ZERO;

			@JsonProperty("samt")
			private BigDecimal sgstAmount = BigDecimal.ZERO;

			@JsonProperty("csamt")
			private BigDecimal cessAmount = BigDecimal.ZERO;

			public ItcDetail() {
				super();

			}

			public ItcDetail(TblOutwardCombinedNew part) {
				super();
				Gstr3BTransactionType type = Gstr3BTransactionType.valueOf(part.getTransaction_Type());
				ItcDetailType itcType = null;

				switch (type) {
				case PART3A:
					itcType = ItcDetailType.IMPG;
					break;

				case PART3B:
					itcType = ItcDetailType.IMPS;
					break;

				case PART3C:
					itcType = ItcDetailType.ISRC;
					break;
				case PART3D:
					itcType = ItcDetailType.ISD;
					break;
				case PART3E:
					// itcType=ItcDetailType.RUL;
					break;

				case PART3F:
					itcType = ItcDetailType.OTH;
					break;

				case PART3G:
					itcType = ItcDetailType.RUL;
					break;

				case PART3H:
					itcType = ItcDetailType.OTH;
					break;

				case PART3J:
					itcType = ItcDetailType.RUL;
					break;

				case PART3K:
					itcType = ItcDetailType.OTH;
					break;

				default:
					System.out.println("invalid part type");

				}
				this.itcDetailType = itcType;
				this.igstAmount = new BigDecimal(part.getIgst()).setScale(2, RoundingMode.HALF_UP);
				this.cgstAmount = new BigDecimal(part.getCgst()).setScale(2, RoundingMode.HALF_UP);
				this.sgstAmount = new BigDecimal(part.getSgst()).setScale(2, RoundingMode.HALF_UP);
				this.cessAmount = new BigDecimal(part.getCess()).setScale(2, RoundingMode.HALF_UP);
			}

			public ItcDetailType getItcDetailType() {
				return itcDetailType;
			}

			public void setItcDetailType(ItcDetailType itcDetailType) {
				this.itcDetailType = itcDetailType;
			}

			public BigDecimal getIgstAmount() {
				return igstAmount;
			}

			public void setIgstAmount(BigDecimal igstAmount) {
				this.igstAmount = this.igstAmount.add(igstAmount);
			}

			public BigDecimal getCgstAmount() {
				return cgstAmount;
			}

			public void setCgstAmount(BigDecimal cgstAmount) {
				this.cgstAmount = this.cgstAmount.add(cgstAmount);
			}

			public BigDecimal getSgstAmount() {
				return sgstAmount;
			}

			public void setSgstAmount(BigDecimal sgstAmount) {
				this.sgstAmount = this.sgstAmount.add(sgstAmount);
			}

			public BigDecimal getCessAmount() {
				return cessAmount;
			}

			public void setCessAmount(BigDecimal cessAmount) {
				this.cessAmount = this.cessAmount.add(cessAmount);
			}

			@Override
			public boolean equals(Object object) {

				if (object instanceof ItcDetail) {
					ItcDetail itcDetail = (ItcDetail) object;

					if (itcDetail.getItcDetailType() == this.itcDetailType)
						return true;
				}
				return false;
			}

			@Override
			public int hashCode() {
				return this.itcDetailType.hashCode();
			}
		}

		public List<ItcDetail> getAvailableItc() {
			return availableItc;
		}

		public void setAvailableItc(List<ItcDetail> availableItc) {
			this.availableItc = availableItc;
		}

		public List<ItcDetail> getItcReversed() {
			return itcReversed;
		}

		public void setItcReversed(List<ItcDetail> itcReversed) {
			this.itcReversed = itcReversed;
		}

		public ItcDetail getNetItcAvailable() {
			return netItcAvailable;
		}

		public void setNetItcAvailable(ItcDetail netItcAvailable) {
			this.netItcAvailable = netItcAvailable;
		}

		public List<ItcDetail> getIneligibleItc() {
			return ineligibleItc;
		}

		public void setIneligibleItc(List<ItcDetail> ineligibleItc) {
			this.ineligibleItc = ineligibleItc;
		}

	}

	public static class InwardSupply {

		@JsonProperty("isup_details")
		List<InwardSupplyDetail> inwardSupDetail = new ArrayList<>();

		public @Data static class InwardSupplyDetail {
			@JsonProperty("ty")
			private InwardSupplyType inwardSupType;

			@JsonProperty("inter")
			private BigDecimal inter = BigDecimal.ZERO;

			@JsonProperty("intra")
			private BigDecimal intra = BigDecimal.ZERO;

			/*
			 * public InwardSupplyDetail(InwardSupplyType supType,double inter,double
			 * intra){ this.inwardSupType=supType; this.setInter(inter);
			 * this.setIntra(intra); }
			 */

			public InwardSupplyDetail() {

			}

			public InwardSupplyDetail(TblOutwardCombinedNew part) {
				if (Gstr3BTransactionType.PART4A.toString().equalsIgnoreCase(part.getTransaction_Type())) {
					this.inwardSupType = InwardSupplyType.GST;
				} else if (Gstr3BTransactionType.PART4B.toString().equalsIgnoreCase(part.getTransaction_Type())) {
					this.inwardSupType = InwardSupplyType.NONGST;
				}

				if (SupplyType.INTER.toString().equalsIgnoreCase(part.getSupply_Type())) {
					this.setInter(new BigDecimal(part.getTaxable_Value()).setScale(2, RoundingMode.HALF_UP));
				} else if (SupplyType.INTRA.toString().equalsIgnoreCase(part.getSupply_Type())) {
					this.setIntra(new BigDecimal(part.getTaxable_Value()).setScale(2, RoundingMode.HALF_UP));
				}

			}
		}

		public List<InwardSupplyDetail> getInwardSupDetail() {
			return inwardSupDetail;
		}

		public void setInwardSupDetail(List<InwardSupplyDetail> inwardSupDetail) {
			this.inwardSupDetail = inwardSupDetail;
		}
	}

	public static class InterestAndLateFeePayble {
		@JsonProperty("intr_details")
		public IntrLateFeeDetail interestLateFeeDetails = new IntrLateFeeDetail();

		public @Data static class IntrLateFeeDetail {

			@JsonProperty("iamt")
			private BigDecimal igstAmount = BigDecimal.ZERO;

			@JsonProperty("camt")
			private BigDecimal cgstAmount = BigDecimal.ZERO;

			@JsonProperty("samt")
			private BigDecimal sgstAmount = BigDecimal.ZERO;

			@JsonProperty("csamt")
			private BigDecimal cessAmount = BigDecimal.ZERO;

			public IntrLateFeeDetail() {

			}

			public IntrLateFeeDetail(TblOutwardCombinedNew part) {
				super();
				this.igstAmount = new BigDecimal(part.getIgst()).setScale(2, RoundingMode.HALF_UP);
				this.cgstAmount = new BigDecimal(part.getCgst()).setScale(2, RoundingMode.HALF_UP);
				this.sgstAmount = new BigDecimal(part.getSgst()).setScale(2, RoundingMode.HALF_UP);
				this.cessAmount = new BigDecimal(part.getCess()).setScale(2, RoundingMode.HALF_UP);
			}

		}

		public IntrLateFeeDetail getInterestLateFeeDetails() {
			return interestLateFeeDetails;
		}

		public void setInterestLateFeeDetails(IntrLateFeeDetail interestLateFeeDetails) {
			this.interestLateFeeDetails = interestLateFeeDetails;
		}

	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getReturnPeriod() {
		return returnPeriod;
	}

	public void setReturnPeriod(String returnPeriod) {
		this.returnPeriod = returnPeriod;
	}

	public SupplyDetail getSupplyDetail() {
		return supplyDetail;
	}

	public void setSupplyDetail(SupplyDetail supplyDetail) {
		this.supplyDetail = supplyDetail;
	}

	public SupplyInter getSupplyInter() {
		return supplyInter;
	}

	public void setSupplyInter(SupplyInter supplyInter) {
		this.supplyInter = supplyInter;
	}

	public ItcEligibility getItcDetail() {

		return itcDetail;
	}

	public void setItcDetail(ItcEligibility itcDetail) {
		this.itcDetail = itcDetail;
	}

	public InwardSupply getInwardSupply() {
		return inwardSupply;
	}

	public void setInwardSupply(InwardSupply inwardSupply) {
		this.inwardSupply = inwardSupply;
	}

	public InterestAndLateFeePayble getInterLateFeePayble() {
		return interLateFeePayble;
	}

	public void setInterLateFeePayble(InterestAndLateFeePayble interLateFeePayble) {
		this.interLateFeePayble = interLateFeePayble;
	}

}
