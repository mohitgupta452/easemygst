package com.ginni.easemygst.portal.gst.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ginni.easemygst.portal.data.part.Gstr3Summary;
import com.ginni.easemygst.portal.data.part.Gstr3Summary.Gstr3TransactionSummary;

import lombok.Data;

public @Data class SubmitRefundReq {
	
	
	@JsonProperty("gstin")
	@JsonInclude(Include.NON_NULL)
	private String gstin;
	
	@JsonProperty("ret_period")
	@JsonInclude(Include.NON_NULL)
	private String monthYear;
	
	@JsonProperty("action")
	@JsonInclude(Include.NON_NULL)
	private String action;
	
	@JsonProperty("data")
	@JsonInclude(Include.NON_NULL)
	private String data;
	
	
	
}



