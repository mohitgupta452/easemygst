
package com.ginni.easemygst.portal.gst.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "desc",
    "igstTaxBal",
    "cgstTaxBal",
    "sgstTaxBal",
    "cessTaxBal",
    "tot_rng_bal"
})
public class OpBal {

    @JsonProperty("desc")
    private String desc;
    @JsonProperty("igstTaxBal")
    private Integer igstTaxBal;
    @JsonProperty("cgstTaxBal")
    private Integer cgstTaxBal;
    @JsonProperty("sgstTaxBal")
    private Integer sgstTaxBal;
    @JsonProperty("cessTaxBal")
    private Integer cessTaxBal;
    @JsonProperty("tot_rng_bal")
    private Integer totRngBal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @JsonProperty("igstTaxBal")
    public Integer getIgstTaxBal() {
        return igstTaxBal;
    }

    @JsonProperty("igstTaxBal")
    public void setIgstTaxBal(Integer igstTaxBal) {
        this.igstTaxBal = igstTaxBal;
    }

    @JsonProperty("cgstTaxBal")
    public Integer getCgstTaxBal() {
        return cgstTaxBal;
    }

    @JsonProperty("cgstTaxBal")
    public void setCgstTaxBal(Integer cgstTaxBal) {
        this.cgstTaxBal = cgstTaxBal;
    }

    @JsonProperty("sgstTaxBal")
    public Integer getSgstTaxBal() {
        return sgstTaxBal;
    }

    @JsonProperty("sgstTaxBal")
    public void setSgstTaxBal(Integer sgstTaxBal) {
        this.sgstTaxBal = sgstTaxBal;
    }

    @JsonProperty("cessTaxBal")
    public Integer getCessTaxBal() {
        return cessTaxBal;
    }

    @JsonProperty("cessTaxBal")
    public void setCessTaxBal(Integer cessTaxBal) {
        this.cessTaxBal = cessTaxBal;
    }

    @JsonProperty("tot_rng_bal")
    public Integer getTotRngBal() {
        return totRngBal;
    }

    @JsonProperty("tot_rng_bal")
    public void setTotRngBal(Integer totRngBal) {
        this.totRngBal = totRngBal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
