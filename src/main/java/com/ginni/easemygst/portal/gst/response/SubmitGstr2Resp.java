package com.ginni.easemygst.portal.gst.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class SubmitGstr2Resp extends BaseResponse{

	@JsonProperty("ref_id")
	private String referenceId;
	
	@JsonProperty("txn_id")
	private String transactionId;
}
