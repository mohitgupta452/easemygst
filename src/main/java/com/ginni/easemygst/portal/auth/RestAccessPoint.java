package com.ginni.easemygst.portal.auth;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.ginni.easemygst.portal.constant.ApplicationMetadata;

@Named("Access")
@RequestScoped
public class RestAccessPoint {
    private static Properties accessConfig = new Properties();
	private List<String> DatavViewAPI,DataSaveAPI,DataSubmitApi,DataModifyApi;
	@PostConstruct
	public void init() {
		try {
			accessConfig.load(RestAccessPoint.class.getClassLoader().getResourceAsStream("Accessconfig.properties"));
			DatavViewAPI=Arrays.asList(accessConfig.get("VIEW").toString().split(","));
			DataSaveAPI=Arrays.asList(accessConfig.get("SAVE").toString().split(","));
			DataSubmitApi=Arrays.asList(accessConfig.get("SUBMIT").toString().split(","));
			DataModifyApi=Arrays.asList(accessConfig.get("MODIFY").toString().split(","));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public String CheckApiType(String path){
		if(checkPath(DatavViewAPI,path))
		return ApplicationMetadata.AccessType.VIEW.toString();
		if(checkPath(DataSaveAPI,path))
		return ApplicationMetadata.AccessType.SAVE.toString();
		if(checkPath(DataSubmitApi,path))
			return ApplicationMetadata.AccessType.SUBMIT.toString();
		if(checkPath(DataModifyApi,path))
			return ApplicationMetadata.AccessType.MODIFY.toString();
		return "";
	}
	
	public boolean checkAcessbilty(String apitype,String acesstype){
		if(acesstype.equalsIgnoreCase(ApplicationMetadata.AccessType.FILE.toString())){
			return true;
		}
		if(acesstype.equalsIgnoreCase(ApplicationMetadata.AccessType.VIEW.toString())){
			if(apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.VIEW.toString())){
 				return true;
			}
		}
		if(acesstype.equalsIgnoreCase(ApplicationMetadata.AccessType.SAVE.toString())){
			if(apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.VIEW.toString()) ||
					apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.SAVE.toString())
					||	apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.MODIFY.toString()))
					{
				return true;
		}
		}
		
		if(acesstype.equalsIgnoreCase(ApplicationMetadata.AccessType.SUBMIT.toString())){
			if(apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.VIEW.toString()) || 
					apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.SAVE.toString())
					|| apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.SUBMIT.toString()) 	
					||	apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.MODIFY.toString())
					)
					{
				return true;
			}
		}
	
		if(acesstype.equalsIgnoreCase(ApplicationMetadata.AccessType.MODIFY.toString())){
			if(apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.VIEW.toString())
					|| apitype.equalsIgnoreCase(ApplicationMetadata.AccessType.MODIFY.toString())
					)
					{
				return true;
			}
		}
		
		
		return false;
	}
	
	
	
	
	
	public  String findgstin(String url){
		 Pattern p2 = Pattern.compile("[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}"); //
	     Matcher m = p2.matcher (url);
	     if (m.find()) {
	         return m.group();
	     }
		return "";
	}
	
	
	public String findreturnType(String url){
		  Pattern p2 = Pattern.compile("/(GSTR|gstr|Gstr)[1-3][a-cA-C]|/(GSTR|gstr|Gstr)[1-3]"); //
		     Matcher m = p2.matcher (url);
		     if (m.find()) {
		         return m.group().substring(1);
		     }
			return "";		
	}
	
	
	public static boolean checkPath(List<String> paths,String path){
		for (String data : paths) {
			if(path.startsWith(data)){
				return true;
			}	
		}
		return false;
	}
	
	
	
	
}
