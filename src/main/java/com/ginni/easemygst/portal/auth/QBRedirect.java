package com.ginni.easemygst.portal.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class QBRedirect
 */
@WebServlet("/QBRedirect")
public class QBRedirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QBRedirect() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		OAuthConsumer oauthconsumer;
		//oauthconsumer=new DefaultOAuthConsumer(consumerKey, consumerSecret)
		oauthconsumer = new DefaultOAuthConsumer(QBService.CONSUMER_KEY, QBService.CONSULMER_SECRET);
		String realmID = request.getParameter("realmId");
		
		HttpParameters additionalParams = new HttpParameters();
		additionalParams.put("oauth_callback", OAuth.OUT_OF_BAND);
		additionalParams.put("oauth_verifier", request.getParameter("oauth_verifier"));
		additionalParams.put("oauth_token", request.getParameter("oauth_token"));
		oauthconsumer.setAdditionalParameters(additionalParams);
		Map<String,String> map=new HashMap<>();
		
		
		// Sign the call to retrieve the access token request
		try {
			String signedURL = oauthconsumer.sign(QBService.ACCESS_TOKEN_URL);
			System.out.println("signed url"+signedURL);
			HttpResponse<String> responseString=Unirest.get(signedURL).asString();
			
			//String output=RestClient.callApi(signedURL, "GET", new JsonNode(""), map);
			System.out.println("output:;"+responseString.getBody());
			
			URL url = new URL(signedURL);

			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");

			String accesstokenresponse = "";
			String accessToken, accessTokenSecret = "";
			if (urlConnection != null) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),Charset.forName("UTF-8")));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = rd.readLine()) != null) {
					sb.append(line);
					System.out.println(sb.toString());
				}
				rd.close();
				accesstokenresponse = sb.toString();
			}
			if (accesstokenresponse != null) {
				String[] responseElements = accesstokenresponse.split("&");
				if (responseElements.length > 1) {
					accessToken = responseElements[1].split("=")[1];
					accessTokenSecret = responseElements[0].split("=")[1];
					System.out.println("OAuth accessToken: " + accessToken);
					System.out.println("OAuth accessTokenSecret: " + accessTokenSecret);
					Map<String, String> accesstokenmap = new HashMap<String, String>();
					accesstokenmap.put("accessToken", accessToken);
					accesstokenmap.put("accessTokenSecret", accessTokenSecret);
					//session.setAttribute("accessToken", accesstokenmap.get("accessToken"));
					//session.setAttribute("accessTokenSecret", accesstokenmap.get("accessTokenSecret"));
					//response.sendRedirect("/OauthSample/connected.jsp");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
//		
//		JSONObject jsonObject=new JSONObject();
//		jsonObject.put("oauth_callback", OAuth.OUT_OF_BAND);
//		jsonObject.put("oauth_verifier", request.getParameter("oauth_verifier"));
//		String output=RestClient.callApi(QBService.ACCESS_TOKEN_URL, "GET", new JsonNode(jsonObject.toString()),map);
//		JSONObject object=new JSONObject(output);
//		
//		if(!object.has("status_cd")) {
//				if(object.has("customer")) {
//					JSONObject innerJSON=new JSONObject(object.get("customer").toString());
//					System.out.println(innerJSON.get("customer_id"));
//			}
//		} else {
//			//invalid response. please check logs for more information.
//		}
		}*/

}
