package com.ginni.easemygst.portal.auth;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class GooglePojo {

	private String id;
    
	private String email;
    
	private boolean verified_email;
    
	private String name;
    
	private String given_name;
    
	private String family_name;
    
}
