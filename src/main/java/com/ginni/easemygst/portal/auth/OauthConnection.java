package com.ginni.easemygst.portal.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class OauthConnection {

	public static final String FB_APP_ID = "269794540050350";
	public static final String FB_APP_SECRET = "73fdc3c47c478ed13dfb9bb8a5351335";
	public static final String FB_REDIRECT_URI = "http://localhost:8180/easemygst/FacebookRedirect";
	public static final String FB_SCOPE = "email,public_profile,user_friends,user_birthday";
	
	public static final String GOOGLE_CLIENT_ID = "977049650370-ch1e52a8vsoqbnvt6ihq8dj5ehig9pb6.apps.googleusercontent.com";
	public static final String GOOGLE_CLIENT_SECRET = "66rZMVIHIJfSX06YrAX6Xkt0";
	public static final String GOOGLE_OAUTH_CALLBACK_URL = "http://localhost:8180/easemygst/GoogleRedirect";

	private String accessToken;
	
	public OauthConnection() { 
		this.accessToken = "";
	}
	
	public String getGoogleAuthUrl() {
		String goLoginUrl = "";
		try {
			goLoginUrl = "https://accounts.google.com/o/oauth2/auth?"
					+ "client_id="+GOOGLE_CLIENT_ID+"&access_type=offline"
					+ "&response_type=code&scope=email&redirect_uri="
					+ URLEncoder.encode(OauthConnection.GOOGLE_OAUTH_CALLBACK_URL, "UTF-8")
					+ "&approval_prompt=auto";
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return goLoginUrl;
	}
	
	public String getFBAuthUrl() {
		String fbLoginUrl = "";
		try {
			fbLoginUrl = "http://www.facebook.com/dialog/oauth?" + "client_id="
					+ OauthConnection.FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(OauthConnection.FB_REDIRECT_URI, "UTF-8")
					+ "&scope="+OauthConnection.FB_SCOPE;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fbLoginUrl;
	}

	public String getFBGraphUrl(String code) {
		String fbGraphUrl = "";
		try {
			fbGraphUrl = "https://graph.facebook.com/oauth/access_token?"
					+ "client_id=" + OauthConnection.FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(OauthConnection.FB_REDIRECT_URI, "UTF-8")
					+ "&client_secret=" + FB_APP_SECRET + "&code=" + code;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fbGraphUrl;
	}

	public String getAccessToken(String code) {
		if ("".equals(this.accessToken)) {
			URL fbGraphURL;
			try {
				fbGraphURL = new URL(getFBGraphUrl(code));
			} catch (MalformedURLException e) {
				e.printStackTrace();
				throw new RuntimeException("Invalid code received " + e);
			}
			URLConnection OauthConnection;
			StringBuffer b = null;
			try {
				OauthConnection = fbGraphURL.openConnection();
				BufferedReader in;
				in = new BufferedReader(new InputStreamReader(
						OauthConnection.getInputStream()));
				String inputLine;
				b = new StringBuffer();
				while ((inputLine = in.readLine()) != null)
					b.append(inputLine + "\n");
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to connect with Facebook "
						+ e);
			}

			this.accessToken = b.toString();
			if (this.accessToken.startsWith("{")) {
				throw new RuntimeException("ERROR: Access Token Invalid: "
						+ accessToken);
			}
		}
		return this.accessToken;
	}
	
	public String getFBGraph(String token) {
		String graph = null;
		try {

			String g = "https://graph.facebook.com/me?fields=name,first_name,last_name,birthday,email,gender&" + token;
			URL u = new URL(g);
			URLConnection c = u.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			String inputLine;
			StringBuffer b = new StringBuffer();
			while ((inputLine = in.readLine()) != null)
				b.append(inputLine + "\n");
			in.close();
			graph = b.toString();
			System.out.println(graph);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("ERROR in getting FB graph data. " + e);
		}
		return graph;
	}
	
	public String getFBFriends(String token) {
		String graph = null;
		try {

			String g = "https://graph.facebook.com/me/friends?" + token;
			URL u = new URL(g);
			URLConnection c = u.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			String inputLine;
			StringBuffer b = new StringBuffer();
			while ((inputLine = in.readLine()) != null)
				b.append(inputLine + "\n");
			in.close();
			graph = b.toString();
			System.out.println(graph);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("ERROR in getting FB graph data. " + e);
		}
		return graph;
	}

	public Map<String,String> getGraphData(String fbGraph) {
		System.out.println(fbGraph);
		Map<String,String> fbProfile = new HashMap<String,String>();
		try {
			JSONObject json = new JSONObject(fbGraph);
			fbProfile.put("id", json.getString("id"));
			fbProfile.put("first_name", json.getString("first_name"));
			if (json.has("email"))
				fbProfile.put("email", json.getString("email"));
			if (json.has("gender"))
				fbProfile.put("gender", json.getString("gender"));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException("ERROR in parsing FB graph data. " + e);
		}
		return fbProfile;
	}
	
}
