package com.ginni.easemygst.portal.auth;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.service.UserService;
import com.google.gson.Gson;

/**
 * Servlet implementation class FacebookServlet
 */
@WebServlet(description = "Redirect URL for Facebook Oauth", urlPatterns = { "/FacebookRedirect" })
public class FacebookRedirect extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private String code = "";

	protected Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Inject
	private UserService userService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FacebookRedirect() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		log.debug("entering facebook redirect doGet");

		code = request.getParameter("code");

		if (code == null || code.equals("")) {
			throw new RuntimeException("ERROR: Didn't get code parameter in callback.");
		}

		OauthConnection oauthConnection = new OauthConnection();

		log.debug("Sending Facebook Oauth Request");
		String token = oauthConnection.getAccessToken(code);

		log.debug("Getting Requested Data from Facebook GraphAPI");
		String graph = oauthConnection.getFBGraph(token);

		// Convert JSON response into Pojo class
		FacebookPojo data = new Gson().fromJson(graph, FacebookPojo.class);

		log.debug(data.toString());

		/*******************************************************
		 * UserDTO userDTO = new UserDTO();
		 * userDTO.setChannel(String.valueOf(RegisterUsing.FACEBOOK));
		 * userDTO.setChannelId(data.getEmail());
		 * userDTO.setEmail(data.getEmail());
		 * userDTO.setFirstName(data.getFirst_name());
		 * userDTO.setLastName(data.getLast_name());
		 * userDTO.setName(data.getName());
		 * userDTO.setRole(String.valueOf(UserRole.PATIENT));
		 * 
		 * if (idmService.isIdentityExists(userDTO.getChannelId())) { userDTO =
		 * idmService.getUserInfo(userDTO.getChannelId()); } else { userDTO =
		 * idmService.createUser(userDTO, new UserPassDTO(),
		 * RegisterUsing.FACEBOOK); }
		 * 
		 * // if userid is -1 if (userDTO.getId() < 0) { log.error("Unable to
		 * Register new Doc32 user"); response.sendRedirect("index.jsp");
		 * return; }
		 * 
		 * sessionHelper = new SessionHelper(request, response);
		 * sessionHelper.createSession(userDTO);
		 *******************************************************/

		log.debug("leaving facebook redirect doGet");
		response.sendRedirect("app");
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
