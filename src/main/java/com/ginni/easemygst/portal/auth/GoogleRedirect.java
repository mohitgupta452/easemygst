package com.ginni.easemygst.portal.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.business.service.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class GoogleRedirect
 */
@WebServlet(description = "Redirect URL for Google auth", urlPatterns = { "/GoogleRedirect" })
public class GoogleRedirect extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Inject
	private UserService userService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GoogleRedirect() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		log.debug("entering google redirect doGet");

		try {
			// get code
			String code = request.getParameter("code");
			// format parameters to post
			String urlParameters = String.format(
					"code=%s&client_id=" + OauthConnection.GOOGLE_CLIENT_ID + "&client_secret="
							+ OauthConnection.GOOGLE_CLIENT_SECRET + "&redirect_uri="
							+ OauthConnection.GOOGLE_OAUTH_CALLBACK_URL + "&grant_type=authorization_code",
					URLEncoder.encode(code, "UTF-8"));

			// post parameters
			log.debug("Sending Google Oauth Request");
			long startTime = System.currentTimeMillis();

			URL url = new URL("https://accounts.google.com/o/oauth2/token");
			URLConnection urlConn = url.openConnection();
			urlConn.setDoOutput(true);

			urlConn.setRequestProperty("Accept-Charset", "UTF-8");
			urlConn.setRequestProperty("USER-AGENT",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
			urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

			try (OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream())) {
				writer.write(urlParameters);
				writer.flush();
			}

			log.debug("Response Time is:{}", System.currentTimeMillis() - startTime);

			// get output in outputString
			String line, outputString = "";

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()))) {
				while ((line = reader.readLine()) != null) {
					outputString += line;
				}
			}

			log.debug(outputString);

			// get Access Token
			JsonObject json = (JsonObject) new JsonParser().parse(outputString);
			String access_token = json.get("access_token").getAsString();

			log.debug(access_token);

			// get User Info
			startTime = System.currentTimeMillis();
			log.debug("Sending Request for user Identity");
			url = new URL("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token);
			urlConn = url.openConnection();

			urlConn.setRequestProperty("Accept-Charset", "UTF-8");
			urlConn.setRequestProperty("USER-AGENT",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

			outputString = "";

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()))) {
				while ((line = reader.readLine()) != null) {
					outputString += line;
				}
			}

			log.debug("Response Time is {}", System.currentTimeMillis() - startTime);

			ServletOutputStream out = response.getOutputStream();
			log.debug(outputString);
			out.println(outputString);

			// Convert JSON response into Pojo class
			GooglePojo data = new Gson().fromJson(outputString, GooglePojo.class);

			log.debug(data.toString());

			/*******************************************************
			 * UserDTO userDTO = new UserDTO();
			 * userDTO.setChannel(String.valueOf(RegisterUsing.GOOGLE));
			 * userDTO.setChannelId(data.getEmail());
			 * userDTO.setEmail(data.getEmail());
			 * userDTO.setFirstName(data.getGiven_name());
			 * userDTO.setLastName(data.getFamily_name());
			 * userDTO.setName(data.getGiven_name()+" "+data.getFamily_name());
			 * userDTO.setRole(String.valueOf(UserRole.PATIENT));
			 * 
			 * if (idmService.isIdentityExists(userDTO.getChannelId())) {
			 * userDTO = idmService.getUserInfo(userDTO.getChannelId()); } else
			 * { userDTO = idmService.createUser(userDTO, new UserPassDTO(),
			 * RegisterUsing.GOOGLE); }
			 * 
			 * // if userid is -1 if (userDTO.getId() < 0) { log.error("Unable
			 * to Register new Doc32 user"); response.sendRedirect("index.jsp");
			 * return; }
			 * 
			 * sessionHelper = new SessionHelper(request, response);
			 * sessionHelper.createSession(userDTO);
			 *******************************************************/

			log.debug("leaving google redirect doGet");
			response.sendRedirect("app");
			return;

		} catch (ProtocolException | MalformedURLException e) {
			log.error(e.getMessage());
		}

		log.debug("leaving google redirect doGet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
