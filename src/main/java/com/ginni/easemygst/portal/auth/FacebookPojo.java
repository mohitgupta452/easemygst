package com.ginni.easemygst.portal.auth;

import lombok.Data;

@SuppressWarnings("restriction")
public @Data class FacebookPojo {

	private String id;
	
	private String email;
	
	private String name;
	
	private String first_name;
	
	private String last_name;
	
	private String birthday;
	
	private String gender;
	
}
