package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.B2bUrService;
import com.ginni.easemygst.portal.transaction.crud.IMPGService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class IMPGTransaction extends TransactionUtil implements Transaction {
	
	private ReturnsService returnService;
	private CrudService crudService;
	private FinderService finderService;
	
	public enum SEZ{
		Y("Y"),N("N");
		
		private String sez;
		
		SEZ(String sez){
			this.sez=sez;
		}
		public String toString(){
			return sez;
		}
	}

	int _GSTIN_INDEX = 0;
	int _BILL_NO_INDEX = 1;
	int _BILL_DATE_INDEX = 2;
	//int _BILL_VALUE_INDEX=3;
	int _PORT_CODE_INDEX = 3;
	int _SEZ_INDEX=16; 

	public IMPGTransaction() {
		 _SNO_INDEX = 4;
		 _HSN_CODE_INDEX = 5;
		 _DESCRIPTION_INDEX = 6;
		 _UNIT_INDEX = 7;
		 _QUANTITY_INDEX = 8;
		 _TAXABLE_VALUE_INDEX = 9;
		 _TAX_RATE_INDEX = 10;
		 _IGST_AMT_INDEX = 11;
		 _CESS_AMT_INDEX = 12;
		 _ELIGIBILITY_TAX_INDEX = 13;
		 _IGST_TAX_AVAIL_INDEX = 14;
		 _CESS_TAX_AVAIL_INDEX = 15;
	}

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	public static List<IMPGTransactionEntity> addIMPGObject(IMPGTransactionEntity impg, List<IMPGTransactionEntity> impgs) {
		Item item=impg.getItems().get(0);
		List<Item> items;
		if(impgs.contains(impg)){
			IMPGTransactionEntity impgOld=impgs.get(impgs.indexOf(impg));
			items=impgOld.getItems();
			if(items != null)
				items.add(item);
			impgOld.setTaxableValue(impg.getTaxableValue()+impgOld.getTaxableValue());
			impgOld.setTaxAmount(impg.getTaxAmount()+impgOld.getTaxAmount());
		}else{
			impgs.add(impg);
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGObject(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = impg.getImpgInvItms();

		String id = impg.getId();
		List<IMPG> searchedImpgs = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(a.getId())).collect(Collectors.toList());

		if (searchedImpgs.isEmpty()) {// to check if the gstin exist
			if (impgs.contains(impg)) {
				searchedImpgs.add(impgs.get(impgs.indexOf(impg)));
				impg.setId(searchedImpgs.get(0).getId());

			}
		}

		if (!searchedImpgs.isEmpty()) {// update existing impg

			IMPG existingImpg = searchedImpgs.get(0);
			items = existingImpg.getImpgInvItms();
			if (!items.isEmpty())
				items.clear();
			for (InvoiceItem tItem : tItems) {
				if (items.contains(tItem)) {// update item
					items.remove(tItem);
					items.add(tItem);
				} else
					items.add(tItem);// add
				if (impg.isGstnSynced())
					impg.setTaxPayerAction(TaxpayerAction.MODIFY);
				impg.setGstnSynced(false);

			}
			impg.setImpgInvItms(items);
			impgs.remove(existingImpg);
			impgs.add(impg);
		} else {// add new impg
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			impg.setGstnSynced(false);
			impgs.add(impg);
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGObjectByNo(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = impg.getImpgInvItms();
		String boeNum = impg.getBillOfEntryNum();
		List<IMPG> searchedImpgs = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(boeNum) && boeNum.equalsIgnoreCase(a.getBillOfEntryNum()))
				.collect(Collectors.toList());
		if (!searchedImpgs.isEmpty()) { // update existing impg

			IMPG existingImpg = searchedImpgs.get(0);
			items = existingImpg.getImpgInvItms();

			for (InvoiceItem tItem : tItems) {
				if (items.contains(tItem)) {// update item items.remove(tItem);
					items.add(tItem);
				} else {
					items.add(tItem);// add item
				}
				if (impg.isGstnSynced()) {
					impg.setTaxPayerAction(TaxpayerAction.MODIFY);
					impg.setGstnSynced(false);
				}
			}
			impg.setImpgInvItms(items);
			impgs.remove(existingImpg);
			impg.setId(existingImpg.getId());
			impgs.add(impg);
		} else {
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			if (impg.isGstnSynced()) {
				impg.setTaxPayerAction(TaxpayerAction.MODIFY);
				impg.setGstnSynced(false);
			}
			impgs.add(impg);
		}
		return impgs;
	}

	public static List<IMPG> deleteIMPGObject(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items;
		String impgId = impg.getId();
		List<IMPG> searchedImpg = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(impgId) && impgId.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedImpg.isEmpty()) {

			items = searchedImpg.get(0).getImpgInvItms();
			InvoiceItem item = impg.getImpgInvItms().get(0);
			if (item.getSNo() != null && items.contains(item)) {
				action = items.remove(item);
				searchedImpg.get(0).setTaxPayerAction(TaxpayerAction.DELETE);//
			}

			if (action == false) {
				if (searchedImpg.get(0).isGstnSynced()) {
					searchedImpg.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedImpg.get(0).setGstnSynced(false);
				} else {
					action = impgs.remove(searchedImpg.get(0));

				}
				action = true;

			}
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGItcData(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> invoiceItems;
		String bId = impg.getId();
		List<IMPG> searchedImpg = impgs.stream()
				.filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId())).collect(Collectors.toList());

		if (!searchedImpg.isEmpty()) {

			invoiceItems = searchedImpg.get(0).getImpgInvItms();
			List<InvoiceItem> invItems = impg.getImpgInvItms();

			for (InvoiceItem item : invItems) {
				if (item.getSNo() != null) {
					if (invoiceItems.indexOf(item) > -1) {
						InvoiceItem tempItem = invoiceItems.get(invoiceItems.indexOf(item));

						String elg = item.getEligOfTotalTax();
						if (!StringUtils.isEmpty(elg)) {
							if (elg.equals("no")) {
								tempItem.setEligOfTotalTax("no");
							} else if (elg.equalsIgnoreCase("ip") || elg.equalsIgnoreCase("cp")) {
								tempItem.setEligOfTotalTax(elg);
								Double igstTemp = tempItem.getIgstAmt();
								Double cgstTemp = tempItem.getCgstAmt();
								Double sgstTemp = tempItem.getSgstAmt();
								Double cessTemp = tempItem.getCessAmt();

								ItcDetail itcDetail = new ItcDetail();

								if (igstTemp != null && igstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalIgst(igstTemp);
								}

								if (cgstTemp != null && cgstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalCgst(cgstTemp);
								}

								if (sgstTemp != null && sgstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalSgst(sgstTemp);
								}

								if (cessTemp != null && cessTemp >= 0.0) {
									itcDetail.setTotalTaxAvalCess(cessTemp);
								}

								tempItem.setItcDetails(itcDetail);
							}
						}
					}
				}

			}
		}

		return impgs;

	}

	public static List<IMPGTransactionEntity> addIMPGObject(List<IMPGTransactionEntity> existingB2bs, List<IMPGTransactionEntity> transactionB2bs) {

		for (IMPGTransactionEntity impg : transactionB2bs) {
			existingB2bs = addIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGObject(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = updateIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> deleteIMPGObject(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = deleteIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGObjectByNo(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = updateIMPGObjectByNo(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGItcData(List<IMPG> existingImpgs, List<IMPG> transactionImpgs)
			throws AppException {

		for (IMPG impg : transactionImpgs) {
			existingImpgs = updateIMPGItcData(impg, existingImpgs);
		}

		return existingImpgs;
	}
	List<String> temp=new ArrayList<String>();
	private boolean isDate_index;
	private int _INVOICE_DATE_INDEX;
	private boolean isTaxable_index;
	private boolean isInvoice_type;
	private int _INVOICE_TYPE_INDEX;
	private boolean isSupply_index;
	private int _SUPPLY_VALUE_INDEX;
	private boolean isInvoice_number;
	private int _INVOICE_NUMBER_INDEX;
	private boolean isInvoice_value;
	private String cellRange;
	
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	@Override
	public int  processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException{

		List<IMPGTransactionEntity> impgs = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if(Objects.nonNull(values))
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
                       if(temp== null)
                    	   break;
					// convert list to impg object
					index++;
					convertListToTransactionData(ee, impgs, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin,userBean,taxpayerGstin,monthYear);

				}

			}

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}
			int uploadingCount=0;
			
            uploadingCount=  new IMPGService().insert(impgs,delete);
          //logging user action
			ApiLoggingService apiLoggingService;
			try {
				apiLoggingService = (ApiLoggingService) InitialContext.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
				MessageDto messageDto=new MessageDto();
				messageDto.setGstin(taxpayerGstin.getGstin());
				messageDto.setReturnType(gstReturn);
				messageDto.setMonthYear(monthYear);
				messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
				messageDto.setTransactionType(TransactionType.IMPG.toString());
				if(!StringUtils.isEmpty(delete)&& delete.equalsIgnoreCase("Yes"))
					apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,taxpayerGstin.getGstin());
					else
				apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto,taxpayerGstin.getGstin());
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//end logging user action
            return uploadingCount;

		} catch (SQLException e) {
			_Logger.error("sql exception ",e);
			throw new AppException(ExceptionCode._ERROR);
		}

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		List<IMPGTransactionEntity> impgs = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					// convert list to impg object
					index++;
					convertListToTransactionData(ee, impgs, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin,userBean,taxpayerGstin,monthYear);
				}

			}
			scanner.close();

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			JSONObject output = new JSONObject();
			output.put("data", JsonMapper.objectMapper.writeValueAsString(impgs));
			output.put("size", index);

			//return output;
            return   new IMPGService().insert(impgs,delete);

		} catch (JsonProcessingException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}
	
	
	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		List<IMPGTransactionEntity> impgs = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		  
		  String customHeaderIndex="";
			if(gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
				customHeaderIndex="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
		// set column mapping
		setColumnMapping(customHeaderIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int index = 0;

			
			for(UploadedCsv lineItem:lineItems) {
									// convert list to impg object
				index=lineItem.getLineNumber();
					convertListToTransactionData(ee, impgs, this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin,userBean,taxpayerGstin,monthYear);
				

			}

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			JSONObject output = new JSONObject();
			output.put("data", JsonMapper.objectMapper.writeValueAsString(impgs));
			output.put("size", index);

			//return output;
          return   new IMPGService().insert(impgs,delete);

		} catch (JsonProcessingException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	
	private List<String> convertLineItemIntoList(UploadedCsv lineItem,ReturnType returnType){
		List<String>fieldList=new ArrayList<>();
		
		fieldList.add(lineItem.getCpGstinNo());
		fieldList.add(lineItem.getEntryNo());
		fieldList.add(lineItem.getEntryDate());
		fieldList.add("");
		fieldList.add("1");//	 for serial no
		fieldList.add("NON GST".equalsIgnoreCase(lineItem.getHsnSacCode())?"":lineItem.getHsnSacCode());
		fieldList.add(lineItem.getHsnSacDesc());
		fieldList.add(lineItem.getUom());
		fieldList.add(lineItem.getQuantity());
		fieldList.add(StringUtils.isEmpty(lineItem.getTotalTaxrate())?lineItem.getNetAmount():lineItem.getTaxableAmount());
		fieldList.add(lineItem.getTotalTaxrate());
		fieldList.add(lineItem.getIgstAmount());
		fieldList.add(lineItem.getCessAmount());
		fieldList.add(lineItem.getInputEligibility());
		fieldList.add(lineItem.getInputIgstAmount());
		fieldList.add(lineItem.getInputCessAmount());
		fieldList.add("N");//sez index

		return fieldList;
	}
	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			_GSTIN_INDEX = Integer.valueOf(headers[0]);
			_BILL_NO_INDEX = Integer.valueOf(headers[1]);
			_BILL_DATE_INDEX = Integer.valueOf(headers[2]);
			//int _BILL_VALUE_INDEX=3;
			 _PORT_CODE_INDEX = Integer.valueOf(headers[3]);			
			_SNO_INDEX = Integer.valueOf(headers[4]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[5]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[6]);
			_UNIT_INDEX = Integer.valueOf(headers[7]);
			_QUANTITY_INDEX = Integer.valueOf(headers[8]);
			_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[9]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[10]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[11]);
			_CESS_AMT_INDEX = Integer.valueOf(headers[12]);
			_ELIGIBILITY_TAX_INDEX = Integer.valueOf(headers[13]);
			_IGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[14]);
			_CESS_TAX_AVAIL_INDEX = Integer.valueOf(headers[15]);
			_SEZ_INDEX= Integer.valueOf(headers[16]);

		}

	}

	private void convertListToTransactionData(ExcelError ee, List<IMPGTransactionEntity> impgs, List<String> temp, int index,
			String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos,
			String gstin,UserBean userBean,TaxpayerGstin taxPayerGstin,String monthYear) {

		ee.resetFieldsExceptErrorAndErrorDesc();
		
		ee.setUniqueRowValue("CTIN - ["+temp.get(_GSTIN_INDEX)+"] BillNo - ["+temp.get(_BILL_NO_INDEX)+"]");
		
		if(StringUtils.isEmpty(temp.get(_TAX_RATE_INDEX)))
			ee.setSewopOrWopay(true);		
		IMPGTransactionEntity impg=new IMPGTransactionEntity();
		impg.setSource(sourceName);
		impg.setSourceId(sourceId);
		Item item = new Item();
		List<Item> items = new ArrayList<>();
		items.add(item);
		impg.setItems(items);
		impg.setReturnType(gstReturn);
		impg.setMonthYear(monthYear);
		
		impg.setGstin(taxPayerGstin);

		InfoService.setInfo(impg, userBean);

		// setting gstin
		if(SourceType.TALLY.toString().equalsIgnoreCase(sourceName)){
			if (StringUtils.isNotEmpty(temp.get(_GSTIN_INDEX))
					)
				ee = this.validateGstin(sheetName, _GSTIN_INDEX, index, temp.get(_GSTIN_INDEX), ee);
		}else{
		if (StringUtils.isNotEmpty(temp.get(_GSTIN_INDEX))||(temp.size()>=_SEZ_INDEX && StringUtils.isNotEmpty(temp.get(_SEZ_INDEX)) && temp.get(_SEZ_INDEX).equalsIgnoreCase("y"))
				)
			ee = this.validateGstin(sheetName, _GSTIN_INDEX, index, temp.get(_GSTIN_INDEX), ee);
		}
		if (!ee.isError()) {
			impg.setCtin(temp.get(_GSTIN_INDEX));
		}
		ee.setIgst(true);	// bill no
		ee = this.validateBillNo(sheetName, _BILL_NO_INDEX, index, temp.get(_BILL_NO_INDEX), ee);
		if (!ee.isError()) {
			impg.setBillOfEntryNumber(temp.get(_BILL_NO_INDEX));
		}
		// bill date
		ee = this.validateDate(sheetName, _BILL_DATE_INDEX, index, temp.get(_BILL_DATE_INDEX), ee);
		if (!ee.isError()) {
			impg.setBillOfEntryDate(ee.getDate());
		}
		
		if(SourceType.TALLY.toString().equalsIgnoreCase(sourceName)){
			if (StringUtils.isNotEmpty(temp.get(_GSTIN_INDEX)))
			impg.setSez(SEZ.Y);
			else
				impg.setSez(SEZ.N);
		
		}else{
			this.validateSEZ(sheetName, _SEZ_INDEX, index, temp.get(_SEZ_INDEX), ee);
			if(!ee.isError())
			impg.setSez(SEZ.valueOf(temp.get(_SEZ_INDEX)));
			
		}
		// port code
				
			if(!StringUtils.isEmpty(temp.get(_PORT_CODE_INDEX)))	{
				impg.setPortCode(temp.get(_PORT_CODE_INDEX));
			}else{
				impg.setPortCode("");
			}
			double taxAmount=this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item,TransactionType.IMPG);
			
			impg.setTaxableValue(item.getTaxableValue());
			impg.setTaxAmount(taxAmount);
			
		if (!ee.isError())
			addIMPGObject(impg, impgs);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<IMPG>>() {
				});
				List<IMPG> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<IMPG>>() {
				});

				if (action == null) {
					//existingB2bs = addIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateIMPGObjectByNo(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.UPDATE_ITC) {
					existingB2bs = updateIMPGItcData(existingB2bs, transactionB2bs);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<IMPG> impgs = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<IMPG>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			Date startDate = new Date();

			double totalTaxAmount = 0.0;
			double amTaxAmount = 0.0;
			double amTaxValue = 0.0;
			double totalTaxableValue = 0.0;

			double igstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (IMPG impg : impgs) {

				double taxableValueInv = 0.0;
				double taxAmountInv = 0.0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;

				// shipping bill date
				Date billDate = impg.getBillOfEntryDate();
				String sbDateError = "";
				if (inputMonth.getMonth() < 9) {
					startDate.setDate(1);
					startDate.setMonth(3);
					startDate.setYear(inputMonth.getYear() - 1);
					sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
							+ sdf.format(inputMonth);
				} else {
					startDate.setDate(1);
					startDate.setMonth(3);
					startDate.setYear(inputMonth.getYear());
					sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
							+ sdf.format(inputMonth);
				}
				if (!Objects.isNull(billDate)
						&& !(billDate.compareTo(startDate) >= 0 && billDate.compareTo(inputMonth) <= 0)) {
					errors.put("BILL_DATE", sbDateError);
					errorFlag = false;
				}
				/*
				 * if (!StringUtils.isEmpty(impg.getOrgiBillOfEntryNum()) &&
				 * impg.getBillOfEntryDate() != null) { impg.setAmendment(true);
				 * } else { impg.setAmendment(false); }
				 */

				impg.setError(errors);
				impg.setValid(errorFlag);

				invoiceCount++;

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}

				/*
				 * List<ImpgInvItem> items = impg.getImpgInvItms(); if
				 * (!Objects.isNull(items)) { int itemCount = 0; for
				 * (InvoiceItem it : items) { itemCount++; it.setSNo(itemCount);
				 * 
				 * if (!Objects.isNull(it.getTaxableValue())) taxableValueInv +=
				 * it.getTaxableValue(); if (!Objects.isNull(it.getIgstAmt())) {
				 * taxAmountInv += it.getIgstAmt(); igstAmount +=
				 * it.getIgstAmt(); }
				 * 
				 * if (!Objects.isNull(it.getCessAmt())) { taxAmountInv +=
				 * it.getCessAmt(); cessAmount += it.getCessAmt(); }
				 * 
				 * } }
				 */
				impg.setTaxAmount(taxAmountInv);
				impg.setTaxableValue(taxableValueInv);
				if (impg.isAmendment()) {
					//
				}

				if (!impg.isValid()) {
					impg.setType(ReconsileType.ERROR);
					impg.setFlags("");

				}

				// impg.setFlags(flags.toString());
				setImpgFlag(impg);

				if (impg.isAmendment()) {
					amTaxAmount += taxAmountInv;
					amTaxValue += taxableValueInv;
				} else {
					totalTaxAmount += taxAmountInv;
					totalTaxableValue += taxableValueInv;
				}

			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", impgs);
			TransactionDataDTO impgSumm = new TransactionDataDTO();
			impgSumm.setTaxAmount(totalTaxAmount);
			impgSumm.setAmTaxAmount(amTaxAmount);
			impgSumm.setTaxableValue(totalTaxableValue);
			impgSumm.setAmTaxableValue(amTaxValue);
			impgSumm.setReconcileDTO(reconcileDTO);
			impgSumm.setError(summaryError);
			impgSumm.setCessAmount(cessAmount);
			impgSumm.setIgstAmount(igstAmount);
			gstrSummaryDTO.setImpg(impgSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setImpgFlag(IMPG impg) {
		if (!Objects.isNull(impg.getTaxPayerAction())) {
			if (impg.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				impg.setFlags(TableFlags.ACCEPTED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.REJECT)
				impg.setFlags(TableFlags.REJECTED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.PENDING)
				impg.setFlags(TableFlags.PENDING.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.MODIFY)
				impg.setFlags(TableFlags.MODIFIED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.DELETE)
				impg.setFlags(TableFlags.DELETED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				impg.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<IMPGTransactionEntity>b2csDatas=datas;
				for (IMPGTransactionEntity invoice : b2csDatas) {
					
					for(Item item:invoice.getItems()){
						List<String> columns = this.getFlatData(invoice,item, returnType);
						tabularData.add(columns);
					}
				}

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}	
	
private List<String> getFlatData(IMPGTransactionEntity invoice, Item item, String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		columns.add(this.getStringValue(invoice.getBillOfEntryNumber()));
		columns.add(this.getStringValue(invoice.getBillOfEntryDate()));
		columns.add(this.getStringValue(invoice.getPortCode()));

		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxableValue()));

		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCess()));
		columns.add(this.getStringValue(item.getTotalEligibleTax()));
		columns.add(this.getStringValue(item.getItcIgst()));
		columns.add(this.getStringValue(item.getItcCess()));
		columns.add(this.getStringValue(invoice.getSez()));

		
		
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));


		return columns;

		
		}
	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<IMPG> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<IMPG>>() {
				});

				data.removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;

	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getImpg();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<IMPG> datas = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				datas = objectMapper.readValue(input, new TypeReference<List<IMPG>>() {
				});

				if (subType.equals(ReconsileSubType.PENDING_ITC.toString())) {
					/*
					 * datas.removeIf(i -> i.getReconcileFlags() == null ||
					 * !i.getReconcileFlags().contains(ReconsileSubType.OWN.
					 * toString()));
					 */
					for (IMPG impgInvoice : datas) {
						impgInvoice.getImpgInvItms()
								.removeIf(it -> it.getEligOfTotalTax() != null && (it.getEligOfTotalTax().equals("no")
										|| it.getEligOfTotalTax().equals("ip") || it.getEligOfTotalTax().equals("cp")));
					}
					datas.removeIf(in -> in.getImpgInvItms().isEmpty());

				} else {

					/*
					 * datas.removeIf(i -> i.getReconcileFlags() == null ||
					 * !i.getReconcileFlags().contains(subType));
					 */
				}

				return JsonMapper.objectMapper.writeValueAsString(datas);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceImpgs = getData(source, returnType);

		Map<String, List<String>> changedImpgs = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionImpgs = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceImpgs) && !Objects.isNull(changedImpgs)) {

			Iterator it = sourceImpgs.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedImpgs.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedImpgs.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedImpgs.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceImpgs.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionImpgs.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionImpgs.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionImpgs.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionImpgs;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> impgs = objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
				});

				for (IMPG impg : impgs) {
					List<InvoiceItem> items = impg.getImpgInvItms();
					for (InvoiceItem item : items) {

						/*List<String> columns = getFlatData(impg, item, returnType);

						String temp = StringUtils.isEmpty(impg.getBillOfEntryNum()) ? ""
								: impg.getBillOfEntryNum() + ":" + item.getSNo() == null ? ""
										: String.valueOf(item.getSNo());
						tabularData.put(temp, columns);*/

					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		try {
			List<IMPG> impgs = JsonMapper.objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
			});

			List<IMPG> currentImpgs = new ArrayList<>();

			for (IMPG impg : impgs) {

				if (!impg.isAmendment() && !impg.isTransit()) {
					currentImpgs.add(impg);
					impg.setTransit(true);
					impg.setTransitId(transitId);
				}

			}

		//	gstr2.setImpgs(currentImpgs);

			if (currentImpgs.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(impgs);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<IMPG> impgs = JsonMapper.objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
			});
			for (IMPG impg : impgs) {
				if (impg.isTransit() && impg.getTransitId().equals(transitId)) {
					impg.setTransit(false);
					impg.setTransitId("");
					impg.setGstnSynced(true);
				}
			}
			return JsonMapper.objectMapper.writeValueAsString(impgs);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");
			finderService = (FinderService) InitialContext.doLookup("java:global/easemygst/FinderServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		gstinTransactionDTO
				.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "impgs"));
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		try {

			Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
					Gstr1Dto.class);
			List<IMPGTransactionEntity> impgs = gstr.getImpgs();
			for (IMPGTransactionEntity impg : impgs) {
				impg.setGstin(gstin);
				impg.setReturnType(gstinTransactionDTO.getReturnType());
				impg.setMonthYear(gstinTransactionDTO.getMonthYear());
				double taxableValue=0.0;
				double taxAmount=0.0;
				for(Item item:impg.getItems()){
					taxableValue+=item.getTaxableValue();
					taxAmount+=item.getTaxAmount();
					item.setInvoiceId(impg.getId());
				}
				impg.setTaxableValue(taxableValue);
				impg.setTaxAmount(taxAmount);
				impg.setSource(SourceType.PORTAL.toString());
				
				if (StringUtils.isEmpty(impg.getId())) {
					InfoService.setInfo(impg, gstinTransactionDTO.getUserBean());
					try {
						new IMPGService().insert(impgs,null);
					} catch (SQLException e) {
						e.printStackTrace();
					}

				} else {
					IMPGTransactionEntity existingImpg = crudService.find(IMPGTransactionEntity.class, impg.getId());
					this.checkIsEditible(existingImpg, impg);
					existingImpg.setBillOfEntryDate(impg.getBillOfEntryDate());
					existingImpg.setBillOfEntryNumber(impg.getBillOfEntryNumber());
					existingImpg.setBillOfEntryValue(impg.getBillOfEntryValue());
					existingImpg.setCtin(impg.getCtin());
					existingImpg.setGstin(impg.getGstin());
					existingImpg.setTaxableValue(impg.getTaxableValue());
					existingImpg.setMonthYear(impg.getMonthYear());
					existingImpg.setPortCode(impg.getPortCode());
					existingImpg.setTaxAmount(impg.getTaxAmount());
					existingImpg.setReturnType(impg.getReturnType());
					existingImpg.setType(impg.getType());
					existingImpg.setIsError(false);
					existingImpg.setErrMsg("");
					InfoService.setInfo(existingImpg, gstinTransactionDTO.getUserBean());
					crudService.update(existingImpg);
					returnService.deleteItemsByInvoiceId(existingImpg.getId());
					for(Item itm:impg.getItems()){
						itm.setInvoiceId(existingImpg.getId());
						crudService.create(itm);
					}

				}

			}

		}  catch (AppException ae) {

			throw ae;
		}
		catch (Exception e) {

			_Logger.error("exception in impg save method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return true;

	}
	
	private boolean checkIsEditible(IMPGTransactionEntity existingInvoice,IMPGTransactionEntity editedInvoice) throws AppException{
		if (existingInvoice.getIsTransit()) {
			AppException ae = new AppException();
			ae.setMessage("invoice is in transit state so can't be updated for the moment");
			_Logger.error("exception--invoice is in transit state so can't be updated for the moment ");
			throw ae;
		}
		if (existingInvoice.getIsSynced()
				&& (!existingInvoice.getBillOfEntryNumber().equalsIgnoreCase(editedInvoice.getBillOfEntryNumber())
				||existingInvoice.getBillOfEntryDate().compareTo(editedInvoice.getBillOfEntryDate())!=0)) {
			
			AppException ae = new AppException();
			ae.setMessage("invoice has been synced with GSTN so bill of entry number and bill of entry date can't be updated for the moment");
			_Logger.error("invoice has been synced with GSTN so bill of entry number and bill of entry date can't be updated for the moment ");
			throw ae;
		}
		
		return true;
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
//		gstExcel.loadSheet(3);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}

				if(temp!=null) {
					System.out.println("Inside 5.Overseas-SEZ and IMPG data...");
					System.out.println(temp);
					if(ReturnType.GSTR2.toString().equals(gstReturn)) {
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(3).equals("5.Overseas-SEZ and IMPG")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(3).equals("5.Overseas-SEZ and IMPG")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(3).equals("5.Overseas-SEZ and IMPG")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(3).equals("5.Overseas-SEZ and IMPG")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(3).equals("5.Overseas-SEZ and IMPG")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(3).equals("5.Overseas-SEZ and IMPG")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    						}
    					
						}
					}
				}
			}
		}
		return 0;
	}
}
