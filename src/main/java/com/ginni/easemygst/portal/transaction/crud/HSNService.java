package com.ginni.easemygst.portal.transaction.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import org.jfree.util.Log;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;

public class HSNService {
	
	final  Logger _Logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	private static String hsnInsert="INSERT INTO `hsn` (`taxpayerGstinId`, `monthYear`, `returnType`,`code`, `description`,"
			+ " `taxableValue`, `taxAmount`, `unit`, `quantity`, `value`, `igst`, `cgst`, `sgst`, `cess`, `source`,`createdBy`,`creationTime`,"
			+ "`creationIpAddress`,`sourceId`)"
			+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\n";
	
	private static String hsnDelete="DELETE from hsn  WHERE monthYear=? and taxpayerGstinId=? and returnType=?";
	
	public int insert(List<Hsn> hsns) throws SQLException
	{
		_Logger.debug("hsn insertion start");
		int updateCount=0;
		
		boolean isFirst=false;

		try(
		Connection connection= DBUtils.getConnection();
		PreparedStatement hsnDel=connection.prepareStatement(hsnDelete);
		PreparedStatement hsnPS=connection.prepareStatement(hsnInsert);
				)
		{
           connection.setAutoCommit(false);	

       for(Hsn hsn: hsns)
		{
			//hsnPS.setString(1,UUID.randomUUID().toString());
			hsnPS.setInt(1,hsn.getTaxpayerGstin().getId());
			hsnPS.setString(2,hsn.getMonthYear());
			hsnPS.setString(3,hsn.getReturnType());
			hsnPS.setString(4,hsn.getCode());
			hsnPS.setString(5,hsn.getDescription());
			hsnPS.setDouble(6,hsn.getTaxableValue());
			hsnPS.setDouble(7,hsn.getTaxAmount());
			hsnPS.setString(8,hsn.getUnit());
			hsnPS.setDouble(9,hsn.getQuantity());
			hsnPS.setDouble(10,hsn.getValue());
			hsnPS.setDouble(11,hsn.getIgst());
			hsnPS.setDouble(12,hsn.getCgst());
			hsnPS.setDouble(13,hsn.getSgst());
			hsnPS.setDouble(14,hsn.getCess());
			hsnPS.setString(15,hsn.getSource());
			hsnPS.setString(16,hsn.getCreatedBy());
			hsnPS.setTimestamp(17,Timestamp.from(Instant.now()));
			hsnPS.setString(18,hsn.getCreationIPAddress());
			hsnPS.setString(19,hsn.getSourceId());
			
			hsnPS.addBatch();
			
			if(!isFirst) {
				hsnDel.setString(1,hsn.getMonthYear());
				hsnDel.setInt(2, hsn.getTaxpayerGstin().getId());
				hsnDel.setString(3, hsn.getReturnType());
        	    isFirst = true;
        		hsnDel.executeUpdate();

        	}
			
		}
		
		
		updateCount=hsnPS.executeBatch().length;
		
		connection.commit();
		}
		catch(Exception e)
		{	
			_Logger.error("hsn insertion  error {}",e);

			updateCount=0;
			throw e;
		}
		_Logger.debug("hsn insertion end records {}",updateCount);

		return updateCount;
		
	}

}
