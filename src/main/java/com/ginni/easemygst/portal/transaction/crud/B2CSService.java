package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;

public class B2CSService extends ItemService {

	public int insertUpdate(List<B2CSTransactionEntity> b2csTransactions, String delete) throws SQLException {
		int count = 0;
		int updateCount = 0;
		if (b2csTransactions != null && !b2csTransactions.isEmpty()) {

			_Logger.info("Inserting b2cs records for gstin {}", b2csTransactions.get(0).getGstin().getGstin());

			String sql = "{call b2csInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

			String delB2CS = "delete from b2cs_transaction using b2cs_transaction inner join tempb2csIds on b2cs_transaction.aiId=tempb2csIds.id";

			String createTemp = "create TEMPORARY TABLE IF NOT EXISTS tempb2csIds (id int(11) NOT NULL, PRIMARY KEY(id)) ENGINE=MyISAM";

			String insertTemp = "insert into tempb2csIds (select aiId from b2cs_transaction where gstin=? and monthYear=?) ";

			try (Connection connection = DBUtils.getConnection();

					Connection delConnection = DBUtils.getConnection();

					CallableStatement itemPS = connection.prepareCall(itemInsertSql);
					PreparedStatement insertTempPS = delConnection.prepareStatement(insertTemp);

					Statement tempTableStatement = delConnection.createStatement();

					CallableStatement b2csTransactionCS = connection.prepareCall(sql);// connection.prepareStatement(insertQueryB2bDetails);
			) {
				connection.setAutoCommit(false);
				delConnection.setAutoCommit(false);
				connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
				delConnection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

				if ("YES".equalsIgnoreCase(delete)) {

					tempTableStatement.executeUpdate(createTemp);
					
					insertTempPS.setInt(1, b2csTransactions.get(0).getGstin().getId());
					insertTempPS.setString(2, b2csTransactions.get(0).getMonthYear());
					 insertTempPS.executeUpdate();
					
					updateCount=tempTableStatement.executeUpdate(delB2CS);
					
					
					

					_Logger.info("{} records has been delete from b2cs for tin {} , return {} , month year {}",
							updateCount, b2csTransactions.get(0).getGstin().getGstin(),
							b2csTransactions.get(0).getReturnType(), b2csTransactions.get(0).getMonthYear());

					delConnection.commit();
					DBUtils.closeQuietly(delConnection);
					updateCount = 0;

				}
				for (B2CSTransactionEntity b2csTransactionEntity : b2csTransactions) {
					String invoiceId = UUID.randomUUID().toString();

					// posList.add(b2csTransactionEntity.getPos());

					b2csTransactionCS.setString(1, invoiceId);
					b2csTransactionCS.setString(2, b2csTransactionEntity.getReturnType());
					b2csTransactionCS.setString(3, b2csTransactionEntity.getMonthYear());
					b2csTransactionCS.setInt(4, b2csTransactionEntity.getGstin().getId());
					b2csTransactionCS.setString(5, b2csTransactionEntity.getPos());
					b2csTransactionCS.setString(6, b2csTransactionEntity.getStateName());
					b2csTransactionCS.setString(7, b2csTransactionEntity.getSource());
					b2csTransactionCS.setString(8, b2csTransactionEntity.getSourceId());
					b2csTransactionCS.setString(9, b2csTransactionEntity.getSupplyType().toString());
					b2csTransactionCS.setString(10, b2csTransactionEntity.getEtin());
					b2csTransactionCS.setString(11, b2csTransactionEntity.getCreatedBy());
					b2csTransactionCS.setTimestamp(12, Timestamp.from(Instant.now()));
					b2csTransactionCS.setString(13, b2csTransactionEntity.getCreationIPAddress());
					b2csTransactionCS.setString(14, b2csTransactionEntity.getUpdatedBy());
					b2csTransactionCS.setTimestamp(15, Timestamp.from(Instant.now()));
					b2csTransactionCS.setString(16, b2csTransactionEntity.getUpdationIPAddress());
					b2csTransactionCS.setDouble(17, b2csTransactionEntity.getTaxAmount());
					b2csTransactionCS.setDouble(18, b2csTransactionEntity.getTaxableValue());
					b2csTransactionCS.setString(19, b2csTransactionEntity.getB2csType());
					b2csTransactionCS.setString(20, b2csTransactionEntity.getInvoiceNumber());
					//ammendment
					b2csTransactionCS.setBoolean(21, b2csTransactionEntity.getIsAmmendment());
					b2csTransactionCS.setString(22, b2csTransactionEntity.getOriginalMonth());
					b2csTransactionCS.setString(23, b2csTransactionEntity.getOriginalPos());
					
					

					b2csTransactionCS.addBatch();

					this.insertItems(b2csTransactionEntity.getItems(), itemPS, OPERATION.INSERT, invoiceId);

					if (++count % 50000 == 0) {
						updateCount += b2csTransactionCS.executeBatch().length;
						itemPS.executeBatch();
					}

				}
				updateCount += b2csTransactionCS.executeBatch().length;
				itemPS.executeBatch();
				connection.commit();

				_Logger.info("End Inserting b2cs records for gstin {} records {} ",
						b2csTransactions.get(0).getGstin().getGstin(), updateCount);

			} catch (SQLException exception) {
				_Logger.error("excepton while inserting b2cs records for gstin {}",
						b2csTransactions.get(0).getGstin().getGstin());
				updateCount = 0;
				_Logger.error("sql error", exception);
				throw exception;
			}
		}
		return updateCount;
	}
}
