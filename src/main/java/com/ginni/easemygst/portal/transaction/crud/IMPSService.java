package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.transaction.crud.ItemService.OPERATION;

public class IMPSService extends ItemService{

	public int insertUpdate(List<IMPSTransactionEntity> impsTransactionEntities,String delete) throws SQLException {

		 if(impsTransactionEntities.isEmpty())
	        	return 0;
		int updateCount=0;
	    String sql ="{call impsInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	    
	    String checkExist="select id,isSubmit from imps_transaction where gstin=? and monthYear=? and invoiceNumber=?"
				+ " and returnType=?";
	    
        String impsExistForDelete="select bd.isSynced,bd.id from imps_transaction bd where bd.gstin=? and bd.returnType=? and bd.monthYear=?  and bd.isDelete=0";
		
		String updateDelFlag="update imps_transaction set isDelete=1,flags='DELETE' where id=?";
		
		String deleteIfNotSynced="delete from imps_transaction where id=?";
		
		String deleteItems="DELETE a from items a,imps_transaction t JOIN(SELECT invoiceNumber,id from imps_transaction GROUP by invoiceNumber,id HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and t.isDelete=1 and t.isSynced=1 and t.gstin=? and t.returnType=? and t.monthYear=?";
		
		String impsDeleteDuplicate="DELETE a from imps_transaction a JOIN(SELECT invoiceNumber from imps_transaction GROUP by invoiceNumber HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.isDelete=1 and a.isSynced=1 and a.gstin=? and a.returnType=? and a.monthYear=?";  
	    
	    int count=0;
	    
	  try(  Connection connection=	DBUtils.getConnection();
		
				CallableStatement itemPS = connection.prepareCall(itemInsertSql);
			    PreparedStatement existPS= connection.prepareStatement(checkExist);
		     
			    PreparedStatement b2burDelExist=connection.prepareStatement(impsExistForDelete);
				
				PreparedStatement b2burDelDuplicate=connection.prepareStatement(impsDeleteDuplicate);
						
			    PreparedStatement b2burDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
						
				PreparedStatement b2burUpdateFlag=connection.prepareStatement(updateDelFlag);
			  
			    PreparedStatement itemdelete=connection.prepareStatement(deleteItems);
				Connection delItemconn = DBUtils.getConnection();

	       	CallableStatement atCS =connection.prepareCall(sql);//connection.prepareStatement(insertQueryB2bDetails);
		){
		  
		 connection.setAutoCommit(false); 
		 delItemconn.setAutoCommit(false);
			OPERATION operation=OPERATION.INSERT;
			
			if ("YES".equalsIgnoreCase(delete)) {
				// for marking delete flag
				b2burDelExist.setString(1, impsTransactionEntities.get(0).getGstin().getId().toString());
				b2burDelExist.setString(2, impsTransactionEntities.get(0).getReturnType());
				b2burDelExist.setString(3, impsTransactionEntities.get(0).getMonthYear());

				ResultSet deleteFlagResult = b2burDelExist.executeQuery();

				int updateFlagcount = 0;
				while (deleteFlagResult.next()) {
					if (deleteFlagResult.getBoolean(1)) {
						b2burUpdateFlag.setString(1, deleteFlagResult.getString(2));
						updateFlagcount += b2burUpdateFlag.executeUpdate();
					} else {
						b2burDelNotSynced.setString(1, deleteFlagResult.getString(2));
						updateFlagcount += b2burDelNotSynced.executeUpdate();
					}
				}
				
				DBUtils.closeQuietly(deleteFlagResult);
			}

		for (IMPSTransactionEntity atTransaction : impsTransactionEntities) {
			
			
			
			//this.gstin = atTransaction.getGstin().getGstin();
			
			String invoiceId=UUID.randomUUID().toString();
			if(!"YES".equalsIgnoreCase(delete)){
			existPS.setInt(1,atTransaction.getGstin().getId());
			existPS.setString(2,atTransaction.getMonthYear());
			existPS.setString(3,atTransaction.getInvoiceNumber());
			existPS.setString(4,atTransaction.getReturnType());
			
			ResultSet impgRS=existPS.executeQuery();

		   if(impgRS.next() && !impgRS.getBoolean(2)){
			   invoiceId=impgRS.getString(1);
			   operation=OPERATION.UPDATE;
		   }
		   
		   DBUtils.closeQuietly(impgRS);
		}
			atCS.setString(1,invoiceId);
			atCS.setInt(2, atTransaction.getGstin().getId());
			atCS.setString(3, atTransaction.getReturnType());
			atCS.setString(4, atTransaction.getMonthYear());
			atCS.setString(5, atTransaction.getFinancialYear());
			atCS.setString(6, atTransaction.getInvoiceNumber());
			atCS.setDate(7, new Date(atTransaction.getInvoiceDate().getTime()));
			atCS.setString(8, atTransaction.getPos());
			atCS.setString(9, atTransaction.getStateName());
			atCS.setString(10, atTransaction.getSource());
			atCS.setString(11, atTransaction.getSourceId());
			atCS.setString(12, atTransaction.getType());
			atCS.setDouble(13,atTransaction.getTaxableValue());
			atCS.setString(14, atTransaction.getCreatedBy());
			atCS.setTimestamp(15, Timestamp.from(Instant.now()));
			atCS.setString(16, atTransaction.getCreationIPAddress());
			atCS.setString(17, atTransaction.getUpdatedBy());
			atCS.setTimestamp(18, Timestamp.from(Instant.now()));
			atCS.setString(19, atTransaction.getUpdationIPAddress());
			atCS.setDouble(21,atTransaction.getTaxAmount());

			atCS.setString(20,operation.toString());

			atCS.addBatch();
			
			this.insertItems(atTransaction.getItems(), itemPS,operation, invoiceId);

			if(++count%20==0)
				updateCount+=atCS.executeBatch().length;
			
			operation=operation.INSERT;
		}
		updateCount+=atCS.executeBatch().length;
		itemPS.executeBatch();

		if("YES".equalsIgnoreCase(delete)){
			itemdelete.setString(1, impsTransactionEntities.get(0).getGstin().getId().toString());
			itemdelete.setString(2, impsTransactionEntities.get(0).getReturnType());
			itemdelete.setString(3, impsTransactionEntities.get(0).getMonthYear());
			 itemdelete.executeUpdate();
			b2burDelDuplicate.setString(1,impsTransactionEntities.get(0).getGstin().getId().toString());
			b2burDelDuplicate.setString(2, impsTransactionEntities.get(0).getReturnType());
			b2burDelDuplicate.setString(3, impsTransactionEntities.get(0).getMonthYear());
			b2burDelDuplicate.executeUpdate();
	    }
		connection.commit();
		
		//if(updateCount>0) this.generateForm3B(this.gstin);
	  }
	  return updateCount;
	}

}
