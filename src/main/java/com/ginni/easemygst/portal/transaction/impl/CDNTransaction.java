package com.ginni.easemygst.portal.transaction.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;
import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.MismatchDTO;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.CDN;
import com.ginni.easemygst.portal.data.transaction.CDN.CdnData;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;
import com.ginni.easemygst.portal.transaction.crud.CDNService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class CDNTransaction extends TransactionUtil implements Transaction {

	private ReturnsService returnService;

	private CrudService crudService;

	int GSTIN_INDEX = 0;
	int GSTIN_NAME = 1;
	int _INV_NO_INDEX = 2;
	int _INV_DATE_INDEX = 3;
	int _NOTE_NO_INDEX = 4;
	int _NOTE_DATE_INDEX = 5;
	int _NOTE_TYPE_INDEX = 6;
	int _REASON_FOR_NOTE_INDEX = 18;
	int _PRE_GST_REGIME = 19;

	int _ORIGINAL_NOTE_NUMBER = 20;
	int _ORIGINAL_NOTE_DATE = 21;

	int _ERR_INV_NO_INDEX = 100;
	int _ERR_INV_DATE_INDEX = 100;
	int _ERR_NOTE_NO_INDEX = 100;
	int _ERR_NOTE_DATE_INDEX = 100;

	public CDNTransaction() {

		_SNO_INDEX = 7;
		_HSN_CODE_INDEX = 8;
		_DESCRIPTION_INDEX = 9;
		_UNIT_INDEX = 10;
		_QUANTITY_INDEX = 11;
		_TAXABLE_VALUE_INDEX = 12;

		_TAX_RATE_INDEX = 13;
		_IGST_AMT_INDEX = 14;
		_CGST_AMT_INDEX = 15;
		_SGST_AMT_INDEX = 16;
		_CESS_AMT_INDEX = 17;

		_ELIGIBILITY_TAX_INDEX = 18;
		_IGST_TAX_AVAIL_INDEX = 19;
		_CGST_TAX_AVAIL_INDEX = 20;
		_SGST_TAX_AVAIL_INDEX = 21;
		_CESS_TAX_AVAIL_INDEX = 22;
		_TEMP_TALLY_POS = 7;

		_ORIGINAL_NOTE_NUMBER = 20;
		_ORIGINAL_NOTE_DATE = 21;

		_ERR_INV_NO_INDEX = _INV_NO_INDEX;
		_ERR_INV_DATE_INDEX = _INV_DATE_INDEX;
		_ERR_NOTE_NO_INDEX = _NOTE_NO_INDEX;
		_ERR_NOTE_DATE_INDEX = _NOTE_DATE_INDEX;
		
		System.out.println("Inside CDN Constructor...");

	}
	List<String> temp=new ArrayList<String>();

	private boolean isDate_index;

	private int _INVOICE_DATE_INDEX;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private int _SUPPLY_VALUE_INDEX;

	private boolean isInvoice_number;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;
	
	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;

	}

	public static List<CDN> addCDNObject(CDN cdn, List<CDN> cdns) {

		List<CdnData> tCdnDatas = cdn.getCdnDatas();

		List<CdnData> cdnDatas = new ArrayList<>();

		if (cdns.contains(cdn)) {
			cdn = cdns.get(cdns.indexOf(cdn));
			cdnDatas = cdn.getCdnDatas();
			for (CdnData tCdnData : tCdnDatas) {
				if (!cdnDatas.contains(tCdnData)) {
					if (tCdnData.isGstnSynced())
						tCdnData.setTaxPayerAction(TaxpayerAction.MODIFY);
					tCdnData.setGstnSynced(false);
					if (StringUtils.isEmpty(tCdnData.getId()))
						tCdnData.setId(getRandomUniqueId());
					cdnDatas.add(tCdnData);
				}
			}
		} else {
			for (CdnData cData : cdn.getCdnDatas()) {
				if (cData.isGstnSynced())
					cData.setTaxPayerAction(TaxpayerAction.MODIFY);
				cData.setGstnSynced(false);
				if (StringUtils.isEmpty(cData.getId()))
					cData.setId(getRandomUniqueId());
			}
			if (StringUtils.isEmpty(cdn.getId()))
				cdn.setId(getRandomUniqueId());
			cdns.add(cdn);

		}

		return cdns;
	}

	public static List<CDN> addCDNTransactionObject(CDNTransactionEntity cdn, List<CDNTransactionEntity> cdns) {
		/*
		 * CdnDetail cd=cdn.getCdnDetails().get(0); InvoiceItem
		 * item=cd.getInvoiceData().getItems().get(0); CdnTransaction tCdn=cdn; if
		 * (cdns.contains(cdn)) { cdn= cdns.get(cdns.indexOf(cdn));
		 * if(cdn.getCdnDetails() ==null) cdn.setCdnDetails(new ArrayList<CdnDetail>());
		 * else{ if(cdn.getCdnDetails().contains(cd)) {
		 * cdn.getCdnDetails().get(cdn.getCdnDetails().indexOf(cd)).getInvoiceData().
		 * getItems().add(item); } else{ cdn.getCdnDetails().add(cd);
		 * cd.getInvoiceData().getItems().add(item); cd.setInvoiceItems(invoiceItems);
		 * invoice.setItems(invoiceItems); }
		 * 
		 * 
		 * }
		 * 
		 * } else { cdns.add(cdn); }
		 * 
		 * 
		 * return cdns;
		 */
		return null;
	}

	public static List<CDN> updateCDNObject(CDN cdn, List<CDN> cdns) throws AppException {
		List<CdnData> cdnDatas = new ArrayList<>();
		List<CdnData> tCdnDatas = cdn.getCdnDatas();
		CDN tempCdn = cdn;
		String id = cdn.getId();
		List<CDN> searchedCdns = cdns.stream().filter(b -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());
		if (searchedCdns.isEmpty()) {// to check if the gstin exist according to
										// handlle client side case
			if (cdns.contains(cdn)) {
				searchedCdns.add(cdns.get(cdns.indexOf(cdn)));
				cdn.setId(searchedCdns.get(0).getId());

			}
		}

		if (!searchedCdns.isEmpty()) {// update existing cdn
			CDN existingCdn = searchedCdns.get(0);
			cdnDatas = existingCdn.getCdnDatas();
			for (CdnData tCdnData : tCdnDatas) {
				String invId = tCdnData.getId();
				List<CdnData> searchedCdnDatas = cdnDatas.stream()
						.filter(b -> b.getId() != null && b.getId().equalsIgnoreCase(invId))
						.collect(Collectors.toList());
				if (!searchedCdnDatas.isEmpty()) {// update existing
					CdnData existingCdnData = searchedCdnDatas.get(0);
					cdnDatas.remove(existingCdnData);
					if (tCdnData.isGstnSynced()) {
						tCdnData.setGstnSynced(false);
						tCdnData.setTaxPayerAction(TaxpayerAction.MODIFY);
					}
					cdnDatas.add(tCdnData);
				} else {// add new invoice
					if (cdnDatas.contains(tCdnData)) {
						AppException exp = new AppException();
						exp.setCode(ExceptionCode._ERROR);
						exp.setMessage(
								"Cdn Data  with same note number already exist, Please choose a different note number");
						throw exp;
					}
					if (StringUtils.isEmpty(tCdnData.getId()))
						tCdnData.setId(getRandomUniqueId());// genrating ids for
															// new
					// CdnData
					tCdnData.setGstnSynced(false);
					cdnDatas.add(tCdnData);
				}

			}
			cdn.setCdnDatas(cdnDatas);
			cdns.remove(existingCdn);
			cdns.add(cdn);
		} else {// add new cdn
			if (StringUtils.isEmpty(cdn.getId()))
				cdn.setId(getRandomUniqueId());
			for (CdnData cData : cdn.getCdnDatas()) {
				if (StringUtils.isEmpty(cData.getId()))
					cData.setId(getRandomUniqueId());// genrating ids for new
														// cdnData
				cData.setGstnSynced(false);
			}
			cdns.add(cdn);
		}

		return cdns;
	}

	public static List<CDN> updateCDNObjectByValue(CDN cdn, List<CDN> cdns, TaxpayerAction taxpayerAction)
			throws AppException {

		List<CdnData> cdnDatas = new ArrayList<>();

		List<CdnData> tCdnDatas = cdn.getCdnDatas();
		String id = cdn.getId();

		List<CDN> searchedCdns = cdns.stream().filter(b -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());

		if (!searchedCdns.isEmpty()) {// update existing cdn

			CDN existingCdn = searchedCdns.get(0);
			cdnDatas = existingCdn.getCdnDatas();

			for (CdnData tCdnData : tCdnDatas) {

				String invId = tCdnData.getId();
				List<CdnData> searchedCdnDatas = cdnDatas.stream()
						.filter(b -> b.getId() != null && b.getId().equalsIgnoreCase(invId))
						.collect(Collectors.toList());
				if (!searchedCdnDatas.isEmpty()) {// update existing

					CdnData existingCdnData = searchedCdnDatas.get(0);
					String mismatchCdnString = existingCdnData.getMismatchedInvoice();

					if (!StringUtils.isEmpty(mismatchCdnString)) {
						try {
							CdnData mismatchedCdnData = JsonMapper.objectMapper.readValue(mismatchCdnString,
									CdnData.class);

							if (!Objects.isNull(mismatchedCdnData)) {
								if (taxpayerAction == TaxpayerAction.RECO_DATE) {
									existingCdnData.setCreditDebitDate(mismatchedCdnData.getCreditDebitDate());
								} else if (taxpayerAction == TaxpayerAction.RECO_INVOICE_NO) {
									existingCdnData.setCreditDebitNum(mismatchedCdnData.getCreditDebitNum());
								} else if (taxpayerAction == TaxpayerAction.RECO_TX_ROUND_OFF) {

								} else if (taxpayerAction == TaxpayerAction.RECO_VAL_ROUND_OFF) {

								}

								if (existingCdnData.isGstnSynced()) {
									existingCdnData.setGstnSynced(false);
									existingCdnData.setTaxPayerAction(TaxpayerAction.MODIFY);
								}
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return cdns;
	}

	public static List<CDN> updateReconciledCdnObject(CDN cdn, List<CDN> cdns) throws AppException {

		if (cdns.contains(cdn)) {
			CDN existingCdn = cdns.get(cdns.indexOf(cdn));
			List<CdnData> existingCdndatas = existingCdn.getCdnDatas();
			for (CdnData cdnData : cdn.getCdnDatas()) {
				if (StringUtils.isEmpty(cdnData.getId()))
					cdnData.setId(getRandomUniqueId());
				existingCdndatas.add(cdnData);
			}

		} else {// add new b2b
			if (StringUtils.isEmpty(cdn.getId()))
				cdn.setId(getRandomUniqueId());
			for (CdnData cdnData : cdn.getCdnDatas()) {
				/*
				 * if (inv.isGstnSynced()) inv.setTaxPayerAction(TaxpayerAction.MODIFY);
				 */
				if (StringUtils.isEmpty(cdnData.getId()))
					cdnData.setId(getRandomUniqueId());
				cdnData.setGstnSynced(false);
			}
			cdns.add(cdn);
		}

		return cdns;

	}

	public static List<CDN> updateCDNObjectByNo(CDN cdn, List<CDN> cdns) throws AppException {
		List<CdnData> cdnDatas = new ArrayList<>();
		List<CdnData> tCdnDatas = cdn.getCdnDatas();
		String gstin = cdn.getGstin();
		List<CDN> searchedCdns = cdns.stream()
				.filter(b -> !StringUtils.isEmpty(gstin) && gstin.equalsIgnoreCase(b.getGstin()))
				.collect(Collectors.toList());

		if (!searchedCdns.isEmpty()) {// update existing cdn
			CDN existingCdn = searchedCdns.get(0);
			cdnDatas = existingCdn.getCdnDatas();
			for (CdnData tCdnData : tCdnDatas) {
				String creditDebitNo = tCdnData.getCreditDebitNum();
				List<CdnData> searchedCdnDatas = cdnDatas.stream().filter(
						b -> b.getCreditDebitNum() != null && b.getCreditDebitNum().equalsIgnoreCase(creditDebitNo))
						.collect(Collectors.toList());
				if (!searchedCdnDatas.isEmpty()) {// update existing
					CdnData existingCdnData = searchedCdnDatas.get(0);
					cdnDatas.remove(existingCdnData);
					if (tCdnData.isGstnSynced()) {
						tCdnData.setGstnSynced(false);
						tCdnData.setTaxPayerAction(TaxpayerAction.MODIFY);
					}
					if (StringUtils.isEmpty(tCdnData.getId()))// since we are
						tCdnData.setId(existingCdnData.getId());// removing
																// existing
																// cdnData and
																// adding new
																// from excel so
																// we need to
																// copy id from
																// existing cdn
																// data
					cdnDatas.add(tCdnData);
				} else {// add new cdn Data

					if (StringUtils.isEmpty(tCdnData.getId()))
						tCdnData.setId(getRandomUniqueId());// genrating ids for
															// new
					// CdnData
					tCdnData.setGstnSynced(false);
					cdnDatas.add(tCdnData);
				}

			}
			if (StringUtils.isEmpty(cdn.getId()))// since we are removing
				cdn.setId(existingCdn.getId()); // existing cdn and
												// adding new from excel so we
												// need to copy id from existing
												// cdn
			cdn.setCdnDatas(cdnDatas);
			cdns.remove(existingCdn);
			cdns.add(cdn);
		} else {// add new cdn
			if (StringUtils.isEmpty(cdn.getId()))
				cdn.setId(getRandomUniqueId());
			for (CdnData cData : cdn.getCdnDatas()) {
				if (StringUtils.isEmpty(cData.getId()))
					cData.setId(getRandomUniqueId());// genrating ids for new
														// cdnData
				cData.setGstnSynced(false);
			}
			cdns.add(cdn);
		}

		return cdns;
	}

	public static List<CDN> deleteCDNObject(CDN cdn, List<CDN> cdns) {

		List<CdnData> cdnDatas;
		String bId = cdn.getId();
		List<CDN> searchedCdn = cdns.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedCdn.isEmpty()) {
			// b2b = searchedB2b.get(0);
			cdnDatas = searchedCdn.get(0).getCdnDatas();
			List<CdnData> searchedCdnData = cdnDatas.stream().filter(
					b -> cdn.getCdnDatas() != null && b.getId().equalsIgnoreCase(cdn.getCdnDatas().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedCdnData.isEmpty()) {
				/*
				 * action = action == false ? cdnDatas.remove(searchedCdnData.get(0)) : true;
				 */
				if (action == false) {
					if (searchedCdnData.get(0).isGstnSynced()) {
						searchedCdnData.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
						searchedCdnData.get(0).setGstnSynced(false);
					} else {
						action = cdnDatas.remove(searchedCdnData.get(0));
						if (cdnDatas.isEmpty())// will delete b2b in case of no
												// invoices remaining
							cdns.remove(searchedCdn.get(0));
					}
					action = true;

				}
			}

			action = action == false ? cdns.remove(searchedCdn.get(0)) : true;
		}

		return cdns;

	}

	public static List<CDN> updateCDNItcData(CDN cdn, List<CDN> cdns) {

		List<CdnData> cdnDatas;
		String bId = cdn.getId();
		List<CDN> searchedCdn = cdns.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());

		if (!searchedCdn.isEmpty()) {

			cdnDatas = searchedCdn.get(0).getCdnDatas();
			List<CdnData> searchedCdnData = cdnDatas.stream().filter(
					b -> cdn.getCdnDatas() != null && b.getId().equalsIgnoreCase(cdn.getCdnDatas().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedCdnData.isEmpty()) {
				CdnData cdnData = searchedCdnData.get(0);

				// String elg = cdnData.getEligOfTotalTax();
				/*
				 * if (!StringUtils.isEmpty(elg)) { if (elg.equals("no")) { //
				 * cdnData.setEligOfTotalTax("no"); } else if (elg.equals("ip")) {
				 * //cdnData.setEligOfTotalTax("ip");
				 * 
				 * Double igstTemp = cdnData.getIgstAmt(); Double cgstTemp =
				 * cdnData.getCgstAmt(); Double sgstTemp = cdnData.getSgstAmt();
				 * 
				 * ItcDetail itcDetail = new ItcDetail();
				 * 
				 * if (igstTemp != null && igstTemp >= 0.0) {
				 * itcDetail.setTotalTaxAvalIgst(igstTemp); //
				 * itcDetail.setTotInpTaxCrdtAvalForClaimThisMonthIgst(igstTemp); }
				 * 
				 * if (cgstTemp != null && cgstTemp >= 0.0) {
				 * itcDetail.setTotalTaxAvalCgst(cgstTemp); //
				 * itcDetail.setTotInpTaxCrdtAvalForClaimThisMonthCgst(cgstTemp); }
				 * 
				 * if (sgstTemp != null && sgstTemp >= 0.0) {
				 * itcDetail.setTotalTaxAvalSgst(sgstTemp); //
				 * itcDetail.setTotInpTaxCrdtAvalForClaimThisMonthSgst(sgstTemp); }
				 * cdnData.setItcDetails(itcDetail);
				 * 
				 * } else if (elg.equals("cp")) { cdnData.setEligOfTotalTax("cp"); } }
				 */
			}

		}

		return cdns;

	}

	public static List<CDN> updateCDNItcDataByNo(CDN cdn, List<CDN> cdns) {

		List<CdnData> cdnDatas;
		String ctin = cdn.getGstin();
		List<CDN> searchedCdn = cdns.stream()
				.filter(b -> !StringUtils.isEmpty(ctin) && ctin.equalsIgnoreCase(b.getGstin()))
				.collect(Collectors.toList());

		if (!searchedCdn.isEmpty()) {

			cdnDatas = searchedCdn.get(0).getCdnDatas();
			List<CdnData> searchedCdnData = cdnDatas.stream()
					.filter(b -> cdn.getCdnDatas() != null
							&& b.getCreditDebitNum().equalsIgnoreCase(cdn.getCdnDatas().get(0).getCreditDebitNum()))
					.collect(Collectors.toList());
			/*
			 * if (!searchedCdnData.isEmpty()) { CdnData cdnData = searchedCdnData.get(0);
			 * 
			 * String elg = cdnData.getEligOfTotalTax(); if (!StringUtils.isEmpty(elg)) { if
			 * (elg.equals("no")) { // cdnData.setEligOfTotalTax("no"); } else if
			 * (elg.equals("ip")) { cdnData.setEligOfTotalTax("ip");
			 * 
			 * Double igstTemp = cdnData.getIgstAmt(); Double cgstTemp =
			 * cdnData.getCgstAmt(); Double sgstTemp = cdnData.getSgstAmt();
			 * 
			 * ItcDetail itcDetail = new ItcDetail();
			 * 
			 * if (igstTemp != null && igstTemp >= 0.0) {
			 * itcDetail.setTotalTaxAvalIgst(igstTemp); //
			 * itcDetail.setTotInpTaxCrdtAvalForClaimThisMonthIgst(igstTemp); }
			 * 
			 * if (cgstTemp != null && cgstTemp >= 0.0) {
			 * itcDetail.setTotalTaxAvalCgst(cgstTemp); //
			 * itcDetail.setTotInpTaxCrdtAvalForClaimThisMonthCgst(cgstTemp); }
			 * 
			 * if (sgstTemp != null && sgstTemp >= 0.0) {
			 * itcDetail.setTotalTaxAvalSgst(sgstTemp); //
			 * itcDetail.setTotInpTaxCrdtAvalForClaimThisMonthSgst(sgstTemp); }
			 * cdnData.setItcDetails(itcDetail);
			 * 
			 * } else if (elg.equals("cp")) { cdnData.setEligOfTotalTax("cp"); } } }
			 */

		}

		return cdns;

	}

	public static List<CDN> updateCdnMarkFlag(CDN cdn, List<CDN> cdns) throws AppException {

		List<CdnData> cdatas;
		String bId = cdn.getId();
		List<CDN> searchedCdn = cdns.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());

		if (!searchedCdn.isEmpty()) {
			cdatas = searchedCdn.get(0).getCdnDatas();
			List<CdnData> searchedCdata = cdatas.stream().filter(
					b -> cdn.getCdnDatas() != null && b.getId().equalsIgnoreCase(cdn.getCdnDatas().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedCdata.isEmpty()) {
				CdnData existingInv = searchedCdata.get(0);
				CdnData inProcessInv = cdn.getCdnDatas().get(0);
				existingInv.setMarked(inProcessInv.isMarked());
			} else {
				AppException ae = new AppException();
				ae.setMessage("Invalid cdn data Id ,No cdn data found with this id ");
				throw ae;
			}

		} else {
			AppException ae = new AppException();
			ae.setMessage("Invalid CDN Id,No cdn found with this id ");
			throw ae;
		}

		return cdns;
	}

	public static List<CDN> changeStatusCDNObject(CDN cdn, List<CDN> cdns, TaxpayerAction taxpayerAction) {

		List<CdnData> cdnDatas;
		String bId = cdn.getId();
		List<CDN> searchedCdn = cdns.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());

		if (!searchedCdn.isEmpty()) {
			// b2b = searchedB2b.get(0);
			cdnDatas = searchedCdn.get(0).getCdnDatas();
			List<CdnData> searchedCdnData = cdnDatas.stream().filter(
					b -> cdn.getCdnDatas() != null && b.getId().equalsIgnoreCase(cdn.getCdnDatas().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedCdnData.isEmpty()) {
				searchedCdnData.get(0).setTaxPayerAction(taxpayerAction);
				searchedCdnData.get(0).setGstnSynced(false);
			}
		}

		return cdns;

	}

	public static List<CDN> addCDNObject(List<CDN> existingCdns, List<CDN> transactionCdns) {

		for (CDN cdn : transactionCdns) {
			existingCdns = addCDNObject(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> updateCDNObject(List<CDN> existingCdns, List<CDN> transactionCdns) throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateCDNObject(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> updateCDNItcData(List<CDN> existingCdns, List<CDN> transactionCdns) throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateCDNItcData(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> updateCDNItcDataByNo(List<CDN> existingCdns, List<CDN> transactionCdns)
			throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateCDNItcDataByNo(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> updateCDNObjectByValue(List<CDN> existingCdns, List<CDN> transactionCdns,
			TaxpayerAction taxpayerAction) throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateCDNObjectByValue(cdn, existingCdns, taxpayerAction);
		}

		return existingCdns;
	}

	public static List<CDN> updateReconciledCdnObject(List<CDN> existingCdns, List<CDN> transactionCdns)
			throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateReconciledCdnObject(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> updateCDNObjectByNo(List<CDN> existingCdns, List<CDN> transactionCdns) throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateCDNObjectByNo(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> deleteCDNObject(List<CDN> existingCdns, List<CDN> transactionCdns) {

		for (CDN cdn : transactionCdns) {
			existingCdns = deleteCDNObject(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> updateCDNMarkFlag(List<CDN> existingCdns, List<CDN> transactionCdns) throws AppException {

		for (CDN cdn : transactionCdns) {
			existingCdns = updateCdnMarkFlag(cdn, existingCdns);
		}

		return existingCdns;
	}

	public static List<CDN> changeStatusCDNObject(List<CDN> existingCdns, List<CDN> transactionCdns,
			TaxpayerAction taxpayerAction) {

		for (CDN cdn : transactionCdns) {
			existingCdns = changeStatusCDNObject(cdn, existingCdns, taxpayerAction);
		}

		return existingCdns;
	}
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, SourceType source, String delete)
			throws AppException {

		String gstin = taxpayerGstin.getGstin();

		// List<CDN> cdns = new ArrayList<>();
		List<CDNTransactionEntity> cdnTrans = new ArrayList<>();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0 || this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if (Objects.nonNull(values))
						columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
					if (temp == null)
						break; // convert list to cdn object

					_Logger.trace(temp.toString());
					index++;
					convertListToTransactionData(ee, cdnTrans, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin, monthYear, userBean);

				}
				if (!ee.isError()) {
					Map<String, List<CDNTransactionEntity>> m = cdnTrans.stream()
							.collect(Collectors.groupingBy(x -> x.getOriginalCtin()));
					List<String> keys = new ArrayList(m.keySet());
					List<String> allrevisedNotelist = new ArrayList<String>();
					List<String> revisedNotelist = new ArrayList<String>();

					m.forEach((k, v) -> {
						int mapindex = keys.indexOf(k);

						for (CDNTransactionEntity cdnTransactionEntity : v) {
							if (mapindex > 0) {
								revisedNotelist.addAll(cdnTransactionEntity.getCdnDetails().stream().distinct()
										.map(data -> data.getRevisedInvNo()).collect(Collectors.toList()));
								Log.debug("revisedNotelist{}" + revisedNotelist.toString());
								List<String> dublicateRevisedInvNo = revisedNotelist.stream().filter(Object -> {
									return allrevisedNotelist.contains(Object);
								}).collect(Collectors.toList());
								Set<String> uniquedublicateRevisedInvNo = new HashSet<String>(dublicateRevisedInvNo);
								if (dublicateRevisedInvNo.size() > 0) {
									StringBuilder error = ee.getErrorDesc();
									error.append("duplicate note number");
									ee.setError(true);
								}
								revisedNotelist.clear();
							}

							allrevisedNotelist.addAll(cdnTransactionEntity.getCdnDetails().stream().distinct()
									.map(data -> data.getRevisedInvNo()).collect(Collectors.toList()));

							Log.debug("all revisednote{}" + allrevisedNotelist.toString());

						}

					});

				}
			}

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			/*
			 * JSONObject output = new JSONObject(); output.put("data",
			 * JsonMapper.objectMapper.writeValueAsString(cdns)); output.put("size", index);
			 */
			int uploadingCount = 0;

			uploadingCount = new CDNService().insertUpdateCDNTransaction(cdnTrans, delete);
			// logging user action
			ApiLoggingService apiLoggingService = (ApiLoggingService) InitialContext
					.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
			MessageDto messageDto = new MessageDto();
			messageDto.setGstin(taxpayerGstin.getGstin());
			messageDto.setReturnType(gstReturn);
			messageDto.setMonthYear(monthYear);
			messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
			messageDto.setTransactionType(TransactionType.CDN.toString());
			if (!StringUtils.isEmpty(delete) && delete.equalsIgnoreCase("Yes"))
				apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,
						taxpayerGstin.getGstin());
			else
				apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto, taxpayerGstin.getGstin());
			// end logging user action

			return uploadingCount;

		} catch (SQLException se) {
			_Logger.error("sql exception ", se);
			throw new AppException(ExceptionCode._ERROR);

		} catch (AppException appException) {
			_Logger.error("app exception ", appException);
			;
			throw appException;
		} catch (Exception e1) {
			_Logger.error("app exception ", e1);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public int saveTransaction(List<CDNTransactionEntity> cdnTransactionEntities) throws AppException {
		try {
			return new CDNService().insertUpdateCDNTransaction(cdnTransactionEntities, null);

		} catch (SQLException se) {
			_Logger.error("sql exception ", se);
			throw new AppException(ExceptionCode._ERROR);
		}
	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source)
			throws AppException {

		String gstin = taxpayerGstin.getGstin();

		// List<CDN> cdns = new ArrayList<>();
		List<CDNTransactionEntity> cdnTrans = new ArrayList<>();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					// convert list to cdn object
					index++;
					convertListToTransactionData(ee, cdnTrans, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin, monthYear, userBean);
				}
			}
			scanner.close();

			if (ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new CDNService().insertUpdateCDNTransaction(cdnTrans, delete);

		} catch (SQLException se) {
			_Logger.error("sql exception ", se);
			throw new AppException(ExceptionCode._ERROR);

		} catch (AppException appException) {
			_Logger.error("app exception ", appException);
			;
			throw appException;
		} catch (Exception e1) {
			_Logger.error("app exception ", e1);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException {

		String gstin = taxpayerGstin.getGstin();
		// List<CDN> cdns = new ArrayList<>();
		List<CDNTransactionEntity> cdnTrans = new ArrayList<>();
		String customHeaderIndex = "";
		if (gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
			customHeaderIndex = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21";
		else
			customHeaderIndex = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24";

		// set column mapping
		setColumnMapping(customHeaderIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		try {
			int index = 0;
			for (UploadedCsv lineItem : lineItems) {
				// convert list to b2b object
				index = lineItem.getLineNumber();
				convertListToTransactionData(ee, cdnTrans,
						this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName,
						sourceName, sourceId, gstReturn, gstinPos, taxpayerGstin, monthYear, userBean);
			}

			if (ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new CDNService().insertUpdateCDNTransaction(cdnTrans, delete);

		} catch (SQLException se) {
			_Logger.error("sql exception ", se);
			throw new AppException(ExceptionCode._ERROR);

		} catch (AppException appException) {
			_Logger.error("app exception ", appException);
			;
			throw appException;
		} catch (Exception e1) {
			_Logger.error("app exception ", e1);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private List<String> convertLineItemIntoList(UploadedCsv lineItem, ReturnType returnType) {
		List<String> fieldList = new ArrayList<>();
		if (returnType == ReturnType.GSTR1) {
			fieldList.add(lineItem.getCpGstinNo());
			fieldList.add(lineItem.getCpName());

			fieldList.add(lineItem.getOriginalInvEntNo());
			fieldList.add(lineItem.getOriginalInvEntDt());

			fieldList.add(lineItem.getEntryNo());
			fieldList.add(lineItem.getEntryDate());

			String noteType = "Invalid";
			if (!StringUtils.isEmpty(lineItem.getGstApplicability())) {
				if ("SALES CREDIT NOTE".equalsIgnoreCase(lineItem.getGstApplicability()))
					noteType = "C";
				else if ("SALES DEBIT NOTE".equalsIgnoreCase(lineItem.getGstApplicability()))
					noteType = "D";
			}

			fieldList.add(noteType);
			fieldList.add("1");// for serial no
			fieldList.add(lineItem.getHsnSacCode());
			fieldList.add(lineItem.getHsnSacDesc());
			fieldList.add(lineItem.getUom());
			fieldList.add(lineItem.getQuantity());
			fieldList.add(lineItem.getTaxableAmount());
			fieldList.add(lineItem.getTotalTaxrate());
			fieldList.add(lineItem.getIgstAmount());
			fieldList.add(lineItem.getCgstAmount());
			fieldList.add(lineItem.getSgstAmount());
			fieldList.add(lineItem.getCessAmount());
			fieldList.add(lineItem.getDncnReason());
			fieldList.add("N");

		} else if (returnType == ReturnType.GSTR2) {

			fieldList.add(lineItem.getCpGstinNo());
			fieldList.add(lineItem.getCpName());

			String invoiceNumber = "";
			String invoiceDate = "";

			String noteNumber = "";
			String noteDate = "";

			if (StringUtils.upperCase(lineItem.getGstApplicability()).contains("REVERSE")) {

				invoiceNumber = lineItem.getOriginalInvEntNo();
				invoiceDate = lineItem.getOriginalInvEntDt();

				noteNumber = lineItem.getEntryNo();
				noteDate = lineItem.getEntryDate();

				_ERR_INV_NO_INDEX = 2000;
				_ERR_INV_DATE_INDEX = 2001;
				_ERR_NOTE_NO_INDEX = 2002;
				_ERR_NOTE_DATE_INDEX = 2003;

			} else if (StringUtils.upperCase(lineItem.getGstApplicability()).equalsIgnoreCase("PURCHASE DEBIT NOTE")) {

				invoiceNumber = lineItem.getOriginalInvEntRefno();
				invoiceDate = lineItem.getOriginalInvEntRefdt();

				noteNumber = lineItem.getRefDocNo();
				noteDate = lineItem.getEntryDate();

				_ERR_INV_NO_INDEX = 1000;
				_ERR_INV_DATE_INDEX = 1001;
				_ERR_NOTE_NO_INDEX = 1002;
				_ERR_NOTE_DATE_INDEX = 2003;

			} else {

				invoiceNumber = lineItem.getOriginalInvEntRefno();
				invoiceDate = lineItem.getOriginalInvEntRefdt();

				noteNumber = lineItem.getRefDocNo();
				noteDate = lineItem.getRefDocDate();

				_ERR_INV_NO_INDEX = 1000;
				_ERR_INV_DATE_INDEX = 1001;
				_ERR_NOTE_NO_INDEX = 1002;
				_ERR_NOTE_DATE_INDEX = 1003;
			}

			fieldList.add(invoiceNumber);
			fieldList.add(invoiceDate);

			fieldList.add(noteNumber);
			fieldList.add(noteDate);

			String noteType = "Invalid";
			if (!StringUtils.isEmpty(lineItem.getGstApplicability())) {
				if ("REVERSE Debit Note".equalsIgnoreCase(lineItem.getGstApplicability())
						|| "Purchase CREDIT Note".equalsIgnoreCase(lineItem.getGstApplicability()))
					noteType = "C";
				else if ("Purchase Debit Note".equalsIgnoreCase(lineItem.getGstApplicability())
						|| "REVERSE CREDIT Note".equalsIgnoreCase(lineItem.getGstApplicability()))
					noteType = "D";
			}

			fieldList.add(noteType);
			fieldList.add("1");// for serial no
			fieldList.add(lineItem.getHsnSacCode());
			fieldList.add(lineItem.getHsnSacDesc());
			fieldList.add(lineItem.getUom());
			fieldList.add(lineItem.getQuantity());
			fieldList.add(lineItem.getTaxableAmount());
			fieldList.add(lineItem.getTotalTaxrate());
			fieldList.add(lineItem.getIgstAmount());
			fieldList.add(lineItem.getCgstAmount());
			fieldList.add(lineItem.getSgstAmount());
			fieldList.add(lineItem.getCessAmount());
			fieldList.add(lineItem.getInputEligibility());
			fieldList.add(lineItem.getInputIgstAmount());
			fieldList.add(lineItem.getInputCgstAmount());
			fieldList.add(lineItem.getInputSgstAmount());
			fieldList.add(lineItem.getInputCessAmount());
			fieldList.add("N");
			fieldList.add(lineItem.getDncnReason());
		}

		return fieldList;
	}

	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			if (gstReturn.equalsIgnoreCase("gstr1")) {

				GSTIN_INDEX = Integer.valueOf(headers[0]);
				GSTIN_NAME = Integer.valueOf(headers[1]);
				_INV_NO_INDEX = Integer.valueOf(headers[2]);
				_INV_DATE_INDEX = Integer.valueOf(headers[3]);
				_NOTE_NO_INDEX = Integer.valueOf(headers[4]);
				_NOTE_DATE_INDEX = Integer.valueOf(headers[5]);
				_NOTE_TYPE_INDEX = Integer.valueOf(headers[6]);
				_SNO_INDEX = Integer.valueOf(headers[7]);
				_HSN_CODE_INDEX = Integer.valueOf(headers[8]);
				_DESCRIPTION_INDEX = Integer.valueOf(headers[9]);
				_UNIT_INDEX = Integer.valueOf(headers[10]);
				_QUANTITY_INDEX = Integer.valueOf(headers[11]);
				_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[12]);
				_TAX_RATE_INDEX = Integer.valueOf(headers[13]);
				_IGST_AMT_INDEX = Integer.valueOf(headers[14]);
				_CGST_AMT_INDEX = Integer.valueOf(headers[15]);
				_SGST_AMT_INDEX = Integer.valueOf(headers[16]);
				_CESS_AMT_INDEX = Integer.valueOf(headers[17]);
				_REASON_FOR_NOTE_INDEX = Integer.valueOf(headers[18]);
				_PRE_GST_REGIME = Integer.valueOf(headers[19]);

			} else if (gstReturn.equalsIgnoreCase("gstr2")) {

				GSTIN_INDEX = Integer.valueOf(headers[0]);
				GSTIN_NAME = Integer.valueOf(headers[1]);
				_INV_NO_INDEX = Integer.valueOf(headers[2]);
				_INV_DATE_INDEX = Integer.valueOf(headers[3]);
				_NOTE_NO_INDEX = Integer.valueOf(headers[4]);
				_NOTE_DATE_INDEX = Integer.valueOf(headers[5]);
				_NOTE_TYPE_INDEX = Integer.valueOf(headers[6]);
				_SNO_INDEX = Integer.valueOf(headers[7]);
				_HSN_CODE_INDEX = Integer.valueOf(headers[8]);
				_DESCRIPTION_INDEX = Integer.valueOf(headers[9]);
				_UNIT_INDEX = Integer.valueOf(headers[10]);
				_QUANTITY_INDEX = Integer.valueOf(headers[11]);
				_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[12]);
				_TAX_RATE_INDEX = Integer.valueOf(headers[13]);
				_IGST_AMT_INDEX = Integer.valueOf(headers[14]);
				_CGST_AMT_INDEX = Integer.valueOf(headers[15]);
				_SGST_AMT_INDEX = Integer.valueOf(headers[16]);
				_CESS_AMT_INDEX = Integer.valueOf(headers[17]);
				_ELIGIBILITY_TAX_INDEX = Integer.valueOf(headers[18]);
				_IGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[19]);
				_CGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[20]);
				_SGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[21]);
				_CESS_TAX_AVAIL_INDEX = Integer.valueOf(headers[22]);
				_REASON_FOR_NOTE_INDEX = Integer.valueOf(headers[24]);
				_PRE_GST_REGIME = Integer.valueOf(headers[23]);

			}
		} else if (gstReturn.equalsIgnoreCase("gstr2")) {

			_REASON_FOR_NOTE_INDEX = 24;
			_PRE_GST_REGIME = 23;

		}

		_ERR_INV_NO_INDEX = _INV_NO_INDEX;
		_ERR_INV_DATE_INDEX = _INV_DATE_INDEX;
		_ERR_NOTE_NO_INDEX = _NOTE_NO_INDEX;
		_ERR_NOTE_DATE_INDEX = _NOTE_DATE_INDEX;

	}

	private void convertListToTransactionData(ExcelError ee, List<CDNTransactionEntity> cdns, List<String> temp,
			int index, String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {
		handleAmmendmentCase(temp, TransactionType.CDN);

		ee.resetFieldsExceptErrorAndErrorDesc();

		ee.setUniqueRowValue("CTIN - [" + temp.get(GSTIN_INDEX) + "] NoteNo - [" + temp.get(_NOTE_NO_INDEX) + "]");

		CDNDetailEntity cdnDetail = new CDNDetailEntity();
		String gstin = taxPayerGstin.getGstin();
		// CDN cdn = new CDN();
		CDNTransactionEntity cdn = new CDNTransactionEntity();

		cdn.setGstin(taxPayerGstin);
		cdn.setReturnType(gstReturn);
		cdn.setMonthYear(monthYear);

		Item item = new Item();

		cdnDetail.setSource(sourceName);
		cdnDetail.setSourceId(sourceId);

		InfoService.setInfo(cdn, userBean);
		InfoService.setInfo(cdnDetail, userBean);

		// setting original gstin
		ee = this.validateGstin(sheetName, GSTIN_INDEX, index, temp.get(GSTIN_INDEX), ee);
		cdn.setOriginalCtin(StringUtils.upperCase(temp.get(GSTIN_INDEX)));

		if (!ee.isError()) {
			this.validateCtinAgainstGSTIN(sheetName, GSTIN_INDEX, index, taxPayerGstin.getGstin(),
					StringUtils.upperCase(temp.get(GSTIN_INDEX)), ee);
		}

		if (cdns.contains(cdn)) {
			cdn = cdns.get(cdns.indexOf(cdn));
		} else {
			cdns.add(cdn);
		}
		cdnDetail.setCdnTransaction(cdn);

		if (SourceType.valueOf(sourceName) != SourceType.TALLY)
			cdn.setOriginalCtinName(temp.get(GSTIN_NAME));

		// setting is igst transaction
		this.setIsIgst(sheetName, gstinPos, temp.get(GSTIN_INDEX), null, ee);
		/*
		 * if ("gstr1".equalsIgnoreCase(gstReturn)) { this.setIsIgst(sheetName,
		 * gstinPos, temp.get(GSTIN_INDEX), null, ee); } else if
		 * ("gstr2".equalsIgnoreCase(gstReturn)) { if
		 * (!StringUtils.isEmpty(temp.get(GSTIN_INDEX)) &&
		 * temp.get(GSTIN_INDEX).length()>2) { gstinPos =
		 * temp.get(GSTIN_INDEX).substring(0, 2); this.setIsIgst(sheetName, gstinPos,
		 * temp.get(GSTIN_INDEX), ee.getPos(), ee); } }
		 */

		// setting original invoice number

		ee = this.validateInvoiceNo(sheetName, _ERR_INV_NO_INDEX, index, temp.get(_INV_NO_INDEX), ee, true);
		cdnDetail.setInvoiceNumber(temp.get(_INV_NO_INDEX));
		// cdnData.setOrigDebtCredtNoteNo(temp.get(_ORIGINAL_INV_NO_INDEX));

		if ("gstr1".equalsIgnoreCase(gstReturn) && SourceType.valueOf(sourceName) != SourceType.TALLY
				&& SourceType.valueOf(sourceName) != SourceType.SAP) {
			boolean isAmmendment = false;
			isAmmendment = this.isAmmendment(null, null, temp.get(_ORIGINAL_NOTE_NUMBER), temp.get(_ORIGINAL_NOTE_DATE),
					null, null, null);
			if (isAmmendment) {
				ee = this.validateNoteNo(sheetName, _ORIGINAL_NOTE_NUMBER, index, temp.get(_ORIGINAL_NOTE_NUMBER), ee,
						true);

				cdnDetail.setOriginalNoteNumber(temp.get(_ORIGINAL_NOTE_NUMBER));
				cdnDetail.setAmmendment(true);

				this.validateOriginalInvoiceDate(sheetName, _ORIGINAL_NOTE_DATE, index, temp.get(_ORIGINAL_NOTE_DATE),
						ee, cdn.getMonthYear(), TransactionType.CDN, true);
				cdnDetail.setAmmendment(true);

				if (!ee.isError()) {
					cdnDetail.setOriginalNoteDate(ee.getOriginalInvoiceDate());
				}
			}
		}

		// setting original invoice date
		ee = this.validateInvoiceDate(sheetName, _ERR_INV_DATE_INDEX, index, temp.get(_INV_DATE_INDEX), ee, monthYear,
				TransactionType.CDN);
		if (!ee.isError())
			cdnDetail.setInvoiceDate(ee.getDate());
		// setting note date
		ee = this.validateNoteDate(sheetName, _ERR_NOTE_DATE_INDEX, index, temp.get(_NOTE_DATE_INDEX),
				temp.get(_INV_DATE_INDEX), ee, monthYear);
		if (!ee.isError())
			cdnDetail.setRevisedInvDate(ee.getDate());

		// note date

		// setting note number
		ee = this.validateNoteNo(sheetName, _ERR_NOTE_NO_INDEX, index, temp.get(_NOTE_NO_INDEX), ee, true);
		cdnDetail.setRevisedInvNo(temp.get(_NOTE_NO_INDEX));

		// setting note type
		ee = this.validateNoteType(sheetName, _NOTE_TYPE_INDEX, index, temp.get(_NOTE_TYPE_INDEX), ee);
		cdnDetail.setNoteType(temp.get(_NOTE_TYPE_INDEX));

		ee = this.validatePreGstRegime(sheetName, _PRE_GST_REGIME, temp.get(_PRE_GST_REGIME), index, ee);
		if (!ee.isError())
			cdnDetail.setPreGstRegime(temp.get(_PRE_GST_REGIME));

		// for itc purpose if(preGstRegime==NO and notetype=c/d) then itc applicable
		// else no
		ee.setPreGstRegime(temp.get(_PRE_GST_REGIME));
		ee.setNoteType(temp.get(_NOTE_TYPE_INDEX));
		// end

		// String rsn=this.validateCdnRsn(sheetName, _REASON_FOR_NOTE_INDEX, index,
		// temp.get(_REASON_FOR_NOTE_INDEX), ee);
		// cdnDetail.setReasonForNote(rsn);
		double taxAmount = 0;
		if (SourceType.valueOf(sourceName) == SourceType.TALLY)
			taxAmount = this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, gstin);
		else
			taxAmount = this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item, TransactionType.CDN);

		cdnDetail.setCdnTransaction(cdn);
		if (cdn.getCdnDetails() == null) {

			if (!ee.isError()) {
				cdn.setCdnDetails(new ArrayList<CDNDetailEntity>());
				cdn.getCdnDetails().add(cdnDetail);
				cdnDetail.setTaxableValue(item.getTaxableValue());
				cdnDetail.setTaxAmount(taxAmount);
				List<Item> items = new ArrayList<>();
				cdnDetail.setItems(items);
				items.add(item);
			}
		}

		else {
			if (cdn.getCdnDetails().contains(cdnDetail)) {

				cdnDetail = cdn.getCdnDetails().get(cdn.getCdnDetails().indexOf(cdnDetail));
				if (cdnDetail.getNoteType().equalsIgnoreCase(temp.get(_NOTE_TYPE_INDEX))) {

					if (!ee.isError()) {
						cdnDetail.getItems().add(item);
						cdnDetail.setTaxableValue(BigDecimal.valueOf(cdnDetail.getTaxableValue())
								.add(BigDecimal.valueOf(item.getTaxableValue())).doubleValue());
						cdnDetail.setTaxAmount(cdnDetail.getTaxAmount() + taxAmount);
					}
				} else {
					ee.setErrorDesc(ee.getErrorDesc().append(sheetName + "," + (_NOTE_TYPE_INDEX + 1) + "," + index
							+ "," + ee.getUniqueRowValue() + " Value - [" + temp.get(_NOTE_TYPE_INDEX)
							+ "] - Invalid note type, Same note number should have same note type either C or D |"));
					ee.setError(true);
				}
			} else {

				if (!ee.isError()) {
					cdn.getCdnDetails().add(cdnDetail);
					List<Item> items = new ArrayList<>();
					items.add(item);
					cdnDetail.setItems(items);
					cdnDetail.setTaxableValue(item.getTaxableValue());
					cdnDetail.setTaxAmount(taxAmount);
				}
			}

		}
		/*
		 * if (!ee.isError()) addCDNObject(cdn, cdns);
		 */
	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<CDN> existingCdns = objectMapper.readValue(existingData, new TypeReference<List<CDN>>() {
				});
				List<CDN> transactionCdns = objectMapper.readValue(transactions, new TypeReference<List<CDN>>() {
				});

				if (action == null) {
					existingCdns = addCDNObject(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.MODIFY) {
					existingCdns = updateCDNObject(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.DELETE) {
					existingCdns = deleteCDNObject(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingCdns = updateCDNObjectByNo(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.RECONCILE) {
					existingCdns = updateReconciledCdnObject(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.UPDATE_ITC) {
					existingCdns = updateCDNItcData(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.UPDATE_ITC) {
					existingCdns = updateCDNItcData(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.UPDATE_ITC_BYNO) {
					existingCdns = updateCDNItcDataByNo(existingCdns, transactionCdns);
				} else if (action == TaxpayerAction.UPDATE_MARKER) {
					existingCdns = updateCDNMarkFlag(existingCdns, transactionCdns);
				} else if (action != null && action.toString().contains("RECO_")) {
					existingCdns = updateCDNObjectByValue(existingCdns, transactionCdns, action);
				} else if (action != null) {
					existingCdns = changeStatusCDNObject(existingCdns, transactionCdns, action);
				}

				return objectMapper.writeValueAsString(existingCdns);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {

		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return false;
			} else {
				gstinTransactionDTO
						.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "cdns"));

				Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
						Gstr1Dto.class);
				;

				List<CDNTransactionEntity> transactionCdns = gstr.getCdns();
				for (CDNTransactionEntity b2bTran : transactionCdns) {
					double taxableValue = 0.0;
					double taxAmount = 0.0;
					b2bTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					b2bTran.setReturnType(gstinTransactionDTO.getReturnType());

					b2bTran.setGstin((TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
							TaxpayerGstin.class));
					CDNDetailEntity inv = b2bTran.getCdnDetails().get(0);
					inv.setCdnTransaction(b2bTran);
					for (Item item : inv.getItems()) {
						taxableValue += item.getTaxableValue();
						taxAmount += item.getTaxAmount();
						item.setInvoiceId(inv.getId());
					}
					inv.setTaxableValue(taxableValue);
					inv.setTaxAmount(taxAmount);
					if (StringUtils.isEmpty(inv.getId())) {// create invoice
						List<CDNTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
								"CdnTransaction.getByGstinMonthyearReturntypeCtin",
								QueryParameter.with("taxPayerGstin", b2bTran.getGstin())
										.and("monthYear", gstinTransactionDTO.getMonthYear())
										.and("returnType", b2bTran.getReturnType())
										.and("ctin", b2bTran.getOriginalCtin()).parameters());
						if (b2bSearch.isEmpty()) {
							/*
							 * inv.setB2bTransaction(b2bTran); crudService.create(b2bTran);
							 */// use kaustub method here
							InfoService.setInfo(b2bTran, gstinTransactionDTO.getUserBean());
							InfoService.setInfo(inv, gstinTransactionDTO.getUserBean());
							try {

								new CDNService().insertUpdateCDNTransaction(transactionCdns, null);
							} catch (SQLException e) {
								Log.error("exception in saving CDN transaction from portal");
								e.printStackTrace();
							}
						} else {
							CDNTransactionEntity existingB2b = b2bSearch
									.get(0);/* incase ctin name is different in new invoice */
							existingB2b.setOriginalCtinName(b2bTran.getOriginalCtinName());
							inv.setCdnTransaction(existingB2b);
							// crudService.create(inv);
							crudService.update(existingB2b);
							InfoService.setInfo(existingB2b, gstinTransactionDTO.getUserBean());
							InfoService.setInfo(inv, gstinTransactionDTO.getUserBean());
							try {

								new CDNService().insertUpdateCDNTransaction(transactionCdns, null);
							} catch (SQLException e) {
								Log.error("exception in saving cdn transaction from portal");
								e.printStackTrace();
							}

							// crudService.create(inv);
						}
						// add new invoice
					} else {
						CDNDetailEntity existingInv = crudService.find(CDNDetailEntity.class, inv.getId());

						List<CDNTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
								"CdnTransaction.getByGstinMonthyearReturntypeCtin",
								QueryParameter.with("taxPayerGstin", b2bTran.getGstin())
										.and("monthYear", gstinTransactionDTO.getMonthYear())
										.and("returnType", b2bTran.getReturnType())
										.and("ctin", existingInv.getCdnTransaction().getOriginalCtin()).parameters());
						InfoService.setInfo(b2bTran, gstinTransactionDTO.getUserBean());

						this.checkIsEditible(existingInv, inv);
						InfoService.setInfo(existingInv, gstinTransactionDTO.getUserBean());
						existingInv.setInvoiceNumber(inv.getInvoiceNumber());
						existingInv.setInvoiceDate(inv.getInvoiceDate());
						existingInv.setTaxableValue(inv.getTaxableValue());
						existingInv.setInvoiceType(inv.getInvoiceType());
						existingInv.setTaxAmount(inv.getTaxAmount());
						existingInv.setOriginalCtinName(b2bTran.getOriginalCtinName());
						existingInv.setPreGstRegime(inv.getPreGstRegime());
						existingInv.setReasonForNote(inv.getReasonForNote());
						existingInv.setRevisedInvDate(inv.getRevisedInvDate());
						existingInv.setRevisedInvNo(inv.getRevisedInvNo());
						existingInv.setIsError(false);
						existingInv.setErrMsg("");
						existingInv.setFlags("");
						existingInv.setIsAmmendment(inv.getIsAmmendment());
						existingInv.setOriginalNoteNumber(inv.getOriginalNoteNumber());
						;
						existingInv.setOriginalNoteDate(inv.getOriginalNoteDate());
						if (b2bSearch.isEmpty()) {
							inv.setCdnTransaction(b2bTran);
							existingInv.setCdnTransaction(b2bTran);
							b2bTran.setCdnDetails(new ArrayList<>());
							crudService.update(b2bTran);
							returnService.deleteItemsByInvoiceId(existingInv.getId());
							for (Item itm : inv.getItems()) {
								itm.setInvoiceId(existingInv.getId());
								crudService.create(itm);
							}

						} else {
							CDNTransactionEntity existingB2b = b2bSearch.get(0);
							InfoService.setInfo(existingB2b, gstinTransactionDTO.getUserBean());
							String ctinName = b2bTran.getOriginalCtinName();
							existingB2b.setOriginalCtinName(ctinName);
							inv.setCdnTransaction(existingB2b);
							// existingInv.setCdnTransaction(existingB2b);

							crudService.update(existingInv);
							// crudService.update(existingB2b);
							returnService.deleteItemsByInvoiceId(existingInv.getId());

							for (Item itm : inv.getItems()) {
								itm.setInvoiceId(existingInv.getId());
								crudService.create(itm);
							}

						}

					}

				}

				return true;
			}
		} catch (AppException ae) {

			throw ae;
		} catch (Exception e) {

			_Logger.error("exception in cdn method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private boolean checkIsEditible(CDNDetailEntity existingInvoice, CDNDetailEntity editedInvoice)
			throws AppException {
		if (existingInvoice.getIsTransit()) {
			AppException ae = new AppException();
			ae.setMessage("invoice is in transit state so can't be updated for the moment");
			_Logger.error("exception--invoice is in transit state so can't be updated for the moment ");
			throw ae;
		}
		if (existingInvoice.getIsSynced()
				&& (!existingInvoice.getInvoiceNumber().equalsIgnoreCase(editedInvoice.getInvoiceNumber())
						|| !existingInvoice.getCdnTransaction().getOriginalCtin()
								.equalsIgnoreCase(editedInvoice.getCdnTransaction().getOriginalCtin())
						|| existingInvoice.getInvoiceDate().compareTo(editedInvoice.getInvoiceDate()) != 0)) {

			AppException ae = new AppException();
			ae.setMessage(
					"invoice has been synced with GSTN so ctin,invoice number and invoice date can't be updated for the moment");
			_Logger.error(
					"invoice has been synced with GSTN so ctin,invoice number and invoice date can't be updated for the moment ");
			throw ae;
		}

		return true;
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			List<CDN> transactionCdns = objectMapper.readValue(object, new TypeReference<List<CDN>>() {
			});
			if (!Objects.isNull(transactionCdns))
				transactionCdns.removeIf(u -> u.getGstin().indexOf(key) == -1);
			return objectMapper.writeValueAsString(transactionCdns);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {

		try {
			if (StringUtils.isEmpty(object)) {
				return finalObj;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<CDN> existingCdns = objectMapper.readValue(object, new TypeReference<List<CDN>>() {
				});
				for (CDN cdn : existingCdns) {
					cdn.setGstin(key);
					if (isFiled)
						cdn.setFilingStatus("Y");
					else
						cdn.setFilingStatus("N");
				}

				if (!StringUtils.isEmpty(finalObj)) {
					List<CDN> transactionCdns = objectMapper.readValue(finalObj, new TypeReference<List<CDN>>() {
					});
					existingCdns = addCDNObject(existingCdns, transactionCdns);
				}

				return objectMapper.writeValueAsString(existingCdns);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void setCdnDataFlag(CdnData cdnData) {
		if (!Objects.isNull(cdnData.getTaxPayerAction())) {
			if (cdnData.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				cdnData.setFlags(TableFlags.ACCEPTED.toString());
			else if (cdnData.getTaxPayerAction() == TaxpayerAction.REJECT)
				cdnData.setFlags(TableFlags.REJECTED.toString());
			else if (cdnData.getTaxPayerAction() == TaxpayerAction.PENDING)
				cdnData.setFlags(TableFlags.PENDING.toString());
			else if (cdnData.getTaxPayerAction() == TaxpayerAction.MODIFY)
				cdnData.setFlags(TableFlags.MODIFIED.toString());
			else if (cdnData.getTaxPayerAction() == TaxpayerAction.DELETE)
				cdnData.setFlags(TableFlags.DELETED.toString());
			else if (cdnData.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				cdnData.setFlags(TableFlags.UPLOADED.toString());
			else if (cdnData.getTaxPayerAction() == TaxpayerAction.DRAFT)
				cdnData.setFlags(TableFlags.DRAFT.toString());
		}
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<CDN> reconsile = new ArrayList<>();
			List<CDN> buyer = new ArrayList<>();
			List<CDN> supplier = new ArrayList<>();

			int missingCount = 0;

			if (!StringUtils.isEmpty(object1)) {
				buyer = objectMapper.readValue(object1, new TypeReference<List<CDN>>() {
				});
			} else {
				buyer = new ArrayList<>();
			}

			if (!StringUtils.isEmpty(object2)) {
				supplier = objectMapper.readValue(object2, new TypeReference<List<CDN>>() {
				});
			} else {
				supplier = new ArrayList<>();
			}

			for (CDN cdn : supplier) {
				if (buyer.contains(cdn)) {
					CDN suppCdn = buyer.get(buyer.indexOf(cdn));
					List<CdnData> cdnDatas = cdn.getCdnDatas();
					List<CdnData> newCdnDatas = new ArrayList<>();
					CDN newCdn = new CDN();
					if (viewType.equals(ViewType1.BUYER)) {
						newCdn.setId(suppCdn.getId());
					} else {
						newCdn.setId(cdn.getId());
					}
					newCdn.setTaxableValue(cdn.getTaxableValue());
					newCdn.setTaxAmount(cdn.getTaxAmount());
					newCdn.setGstin(cdn.getGstin());
					newCdn.setGstinName(cdn.getGstinName());
					newCdn.setFilingStatus(cdn.getFilingStatus());
					// newCdn.setType(cdn.getType());
					newCdn.setCdnDatas(newCdnDatas);
					List<CdnData> suppInvoices = suppCdn.getCdnDatas();
					for (CdnData cdnData : cdnDatas) {
						if (cdnData.isValid()) {
							int i = suppInvoices.indexOf(cdnData);
							CdnData suppCdn1 = null;
							boolean tempFlagSupp = false;
							if (i > -1) {
								suppCdn1 = suppInvoices.get(i);
								tempFlagSupp = !Objects.isNull(suppCdn1.getTaxPayerAction())
										&& (suppCdn1.getTaxPayerAction().equals(TaxpayerAction.REJECT)
												|| suppCdn1.getTaxPayerAction().equals(TaxpayerAction.PENDING));
							}
							if (!Objects.isNull(suppCdn1) && !tempFlagSupp) {
								if (viewType.equals(ViewType1.SUPPLIER)) {
									boolean mismatch = false;
									String rType = "";

									if (!suppCdn1.getCreditDebitDate().equals(cdnData.getCreditDebitDate())) {
										rType += ReconsileSubType.DATE.toString() + ",";
										mismatch = true;
									}

									if (!suppCdn1.getTaxableValue().equals(cdnData.getTaxableValue())) {
										double inv = suppCdn1.getTaxableValue() - cdnData.getTaxableValue();
										if (inv <= 1.00 && inv >= -1.00) {
											rType += ReconsileSubType.VALUE_ROUND_OFF.toString() + ",";
										} else {
											rType += ReconsileSubType.VALUE.toString() + ",";
										}
										mismatch = true;
									}

									if (!suppCdn1.getTaxAmount().equals(cdnData.getTaxAmount())) {
										double inv = suppCdn1.getTaxAmount() - cdnData.getTaxAmount();
										if (inv <= 1.00 && inv >= -1.00) {
											rType += ReconsileSubType.TAX_ROUND_OFF.toString() + ",";
										} else {
											rType += ReconsileSubType.TAX.toString() + ",";
										}
										mismatch = true;
									}

									if (mismatch) {
										if (!(rType.contains(ReconsileSubType.DATE.toString())
												|| rType.contains(ReconsileSubType.VALUE.toString())
												|| rType.contains(ReconsileSubType.TAX.toString()))) {
											rType = ReconsileSubType.ROUNDING_OFF.toString();
										} else if (!rType.contains(ReconsileSubType.DATE.toString())) {
											rType = ReconsileSubType.MAJOR.toString();
										}
										cdnData.setType(ReconsileType.MISMATCH);
										cdnData.setReconcileFlags(rType);
										cdnData.setTaxPayerAction(TaxpayerAction.MODIFY);

										// copying cdnData id from counter party
										suppCdn1.setId(cdnData.getId());
										cdnData.setMismatchedInvoice(
												JsonMapper.objectMapper.writeValueAsString(suppCdn1));
										if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
											if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
												cdnData.setLocked(false);
												cdnData.setFlags(TableFlags.DRAFT.toString());
												cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
											}
										}
									} else {
										boolean processFlag = !Objects.isNull(cdnData.getTaxPayerAction())
												&& (cdnData.getTaxPayerAction().equals(TaxpayerAction.REJECT)
														|| cdnData.getTaxPayerAction().equals(TaxpayerAction.PENDING));

										boolean processFlagSupp = !Objects.isNull(suppCdn1.getTaxPayerAction())
												&& (suppCdn1.getTaxPayerAction().equals(TaxpayerAction.REJECT)
														|| suppCdn1.getTaxPayerAction().equals(TaxpayerAction.PENDING));

										boolean pFlag = processFlag || processFlagSupp;

										if (!pFlag) {
											cdnData.setType(ReconsileType.MATCH);
											cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
											cdnData.setTaxPayerAction(TaxpayerAction.ACCEPT);
											cdnData.setCheckSum(suppCdn1.getCheckSum());
											if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
												if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
													cdnData.setLocked(false);
													cdnData.setFlags(TableFlags.DRAFT.toString());
													cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
												}
											}
										} else {
											if (processFlag) {
												cdnData.setType(ReconsileType.MISSING_OUTWARD);
												cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
											}
										}
									}

									newCdnDatas.add(cdnData);
								}
							} else {
								cdnData.setTaxPayerAction(TaxpayerAction.UPLOADED);
								cdnData.setType(ReconsileType.NEW);
								if (viewType.equals(ViewType1.BUYER)) {
									cdnData.setType(ReconsileType.MISSING_INWARD);
									cdnData.setReconcileFlags(ReconsileSubType.MISSING.toString());
									missingCount++;
									if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
										if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
											cdnData.setLocked(false);
											cdnData.setFlags(TableFlags.DRAFT.toString());
											cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
										}
									}
								} else {
									cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
								}
								newCdnDatas.add(cdnData);
							}
							setCdnDataFlag(cdnData);
						} else {
							cdnData.setType(ReconsileType.ERROR);
							cdnData.setReconcileFlags("");
							if (viewType.equals(ViewType1.SUPPLIER))
								newCdnDatas.add(cdnData);
						}
					}
					if (newCdnDatas.size() > 0)
						reconsile = addCDNObject(newCdn, reconsile);
				} else {
					List<CdnData> cdnDatas = cdn.getCdnDatas();
					List<CdnData> newCdnDatas = new ArrayList<>();
					CDN newCdn = new CDN();
					newCdn.setId(cdn.getId());
					newCdn.setTaxableValue(cdn.getTaxableValue());
					newCdn.setTaxAmount(cdn.getTaxAmount());
					newCdn.setGstin(cdn.getGstin());
					newCdn.setGstinName(cdn.getGstinName());
					// newCdn.setType(cdn.getType());
					for (CdnData cdnData : cdnDatas) {
						if (cdnData.isValid()) {
							cdnData.setTaxPayerAction(TaxpayerAction.UPLOADED);
							cdnData.setType(ReconsileType.NEW);
							if (viewType.equals(ViewType1.BUYER)) {
								cdnData.setType(ReconsileType.MISSING_INWARD);
								cdnData.setReconcileFlags(ReconsileSubType.MISSING.toString());
								missingCount++;
								if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
									if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
										cdnData.setLocked(false);
										cdnData.setFlags(TableFlags.DRAFT.toString());
										cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
									}
								}
							} else {
								cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
							}
							setCdnDataFlag(cdnData);
							newCdnDatas.add(cdnData);
						} else {
							cdnData.setType(ReconsileType.ERROR);
							cdnData.setReconcileFlags("");
							if (viewType.equals(ViewType1.SUPPLIER))
								newCdnDatas.add(cdnData);
						}
					}
					newCdn.setCdnDatas(newCdnDatas);
					reconsile = addCDNObject(newCdn, reconsile);
				}
			}

			for (CDN cdn : buyer) {
				if (supplier.contains(cdn)) {
					CDN recvCdn = supplier.get(supplier.indexOf(cdn));
					List<CdnData> cdnDatas = cdn.getCdnDatas();
					List<CdnData> newCdnDatas = new ArrayList<>();
					CDN newCdn = new CDN();
					if (viewType.equals(ViewType1.SUPPLIER)) {
						newCdn.setId(recvCdn.getId());
					} else {
						newCdn.setId(cdn.getId());
					}
					newCdn.setGstin(cdn.getGstin());
					newCdn.setTaxableValue(cdn.getTaxableValue());
					newCdn.setTaxAmount(cdn.getTaxAmount());
					newCdn.setGstinName(cdn.getGstinName());
					newCdn.setFilingStatus(cdn.getFilingStatus());
					// newCdn.setType(cdn.getType());
					newCdn.setCdnDatas(newCdnDatas);
					List<CdnData> recvCdndatas = recvCdn.getCdnDatas();
					for (CdnData cdnData : cdnDatas) {
						if (cdnData.isValid()) {
							int i = recvCdndatas.indexOf(cdnData);
							CdnData recvCdn1 = null;
							boolean tempFlagSupp = false;
							if (i > -1) {
								recvCdn1 = recvCdndatas.get(i);
								tempFlagSupp = !Objects.isNull(recvCdn1.getTaxPayerAction())
										&& (recvCdn1.getTaxPayerAction().equals(TaxpayerAction.REJECT)
												|| recvCdn1.getTaxPayerAction().equals(TaxpayerAction.PENDING));
							}
							if (!Objects.isNull(recvCdn1) && !tempFlagSupp) {
								if (viewType.equals(ViewType1.BUYER)) {
									boolean mismatch = false;
									String rType = "";

									if (!recvCdn1.getCreditDebitDate().equals(cdnData.getCreditDebitDate())) {
										rType += ReconsileSubType.DATE.toString() + ",";
										mismatch = true;
									}

									if (!recvCdn1.getTaxableValue().equals(cdnData.getTaxableValue())) {
										double inv = recvCdn1.getTaxableValue() - cdnData.getTaxableValue();
										if (inv <= 1.00 && inv >= -1.00) {
											rType += ReconsileSubType.VALUE_ROUND_OFF.toString() + ",";
										} else {
											rType += ReconsileSubType.VALUE.toString() + ",";
										}
										mismatch = true;
									}

									if (!recvCdn1.getTaxAmount().equals(cdnData.getTaxAmount())) {
										double inv = recvCdn1.getTaxAmount() - cdnData.getTaxAmount();
										if (inv <= 1.00 && inv >= -1.00) {
											rType += ReconsileSubType.TAX_ROUND_OFF.toString() + ",";
										} else {
											rType += ReconsileSubType.TAX.toString() + ",";
										}
										mismatch = true;
									}

									if (mismatch) {
										if (!(rType.contains(ReconsileSubType.DATE.toString())
												|| rType.contains(ReconsileSubType.VALUE.toString())
												|| rType.contains(ReconsileSubType.TAX.toString()))) {
											rType = ReconsileSubType.ROUNDING_OFF.toString();
										} else if (!rType.contains(ReconsileSubType.DATE.toString())) {
											rType = ReconsileSubType.MAJOR.toString();
										}
										cdnData.setType(ReconsileType.MISMATCH);
										cdnData.setReconcileFlags(rType);
										cdnData.setTaxPayerAction(TaxpayerAction.MODIFY);

										// copying cdnData id from counter party
										recvCdn1.setId(cdnData.getId());
										cdnData.setMismatchedInvoice(
												JsonMapper.objectMapper.writeValueAsString(recvCdn1));
										if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
											if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
												cdnData.setLocked(false);
												cdnData.setFlags(TableFlags.DRAFT.toString());
												cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
											}
										}
									} else {
										boolean processFlag = !Objects.isNull(cdnData.getTaxPayerAction())
												&& (cdnData.getTaxPayerAction().equals(TaxpayerAction.REJECT)
														|| cdnData.getTaxPayerAction().equals(TaxpayerAction.PENDING));

										boolean processFlagRecv = !Objects.isNull(recvCdn1.getTaxPayerAction())
												&& (recvCdn1.getTaxPayerAction().equals(TaxpayerAction.REJECT)
														|| recvCdn1.getTaxPayerAction().equals(TaxpayerAction.PENDING));

										boolean pFlag = processFlag || processFlagRecv;

										if (!pFlag) {
											cdnData.setType(ReconsileType.MATCH);
											cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
											cdnData.setTaxPayerAction(TaxpayerAction.ACCEPT);
											cdnData.setCheckSum(recvCdn1.getCheckSum());
											if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
												if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
													cdnData.setLocked(false);
													cdnData.setFlags(TableFlags.DRAFT.toString());
													cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
												}
											}
										} else {
											if (processFlag) {
												cdnData.setType(ReconsileType.MISSING_INWARD);
												cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
											}
										}
									}

									newCdnDatas.add(cdnData);
								}
							} else {
								cdnData.setTaxPayerAction(TaxpayerAction.UPLOADED);
								cdnData.setType(ReconsileType.MISSING_OUTWARD);
								if (viewType.equals(ViewType1.BUYER)) {
									cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
								} else {
									cdnData.setReconcileFlags(ReconsileSubType.MISSING.toString());
									missingCount++;
									if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
										if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
											cdnData.setLocked(false);
											cdnData.setFlags(TableFlags.DRAFT.toString());
											cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
										}
									}
								}
								newCdnDatas.add(cdnData);
							}
							setCdnDataFlag(cdnData);
						} else {
							cdnData.setType(ReconsileType.ERROR);
							cdnData.setReconcileFlags("");
							if (viewType.equals(ViewType1.BUYER))
								newCdnDatas.add(cdnData);
						}
					}
					if (newCdnDatas.size() > 0)
						reconsile = addCDNObject(newCdn, reconsile);
				} else {
					List<CdnData> cdnDatas = cdn.getCdnDatas();
					List<CdnData> newCdnDatas = new ArrayList<>();
					CDN newCdn = new CDN();
					newCdn.setId(cdn.getId());
					newCdn.setGstin(cdn.getGstin());
					newCdn.setTaxableValue(cdn.getTaxableValue());
					newCdn.setTaxAmount(cdn.getTaxAmount());
					newCdn.setGstinName(cdn.getGstinName());
					// newCdn.setType(cdn.getType());
					for (CdnData cdnData : cdnDatas) {
						if (cdnData.isValid()) {
							cdnData.setTaxPayerAction(TaxpayerAction.UPLOADED);
							cdnData.setType(ReconsileType.MISSING_OUTWARD);
							if (viewType.equals(ViewType1.BUYER)) {
								cdnData.setReconcileFlags(ReconsileSubType.OWN.toString());
							} else {
								cdnData.setReconcileFlags(ReconsileSubType.MISSING.toString());
								missingCount++;
								if (!StringUtils.isEmpty(cdn.getFilingStatus())) {
									if (cdn.getFilingStatus().equalsIgnoreCase("N")) {
										cdnData.setLocked(false);
										cdnData.setFlags(TableFlags.DRAFT.toString());
										cdnData.setTaxPayerAction(TaxpayerAction.DRAFT);
									}
								}
							}
							setCdnDataFlag(cdnData);
							newCdnDatas.add(cdnData);
						} else {
							cdnData.setType(ReconsileType.ERROR);
							cdnData.setReconcileFlags("");
							if (viewType.equals(ViewType1.BUYER))
								newCdnDatas.add(cdnData);
						}
					}
					newCdn.setCdnDatas(newCdnDatas);
					reconsile = addCDNObject(newCdn, reconsile);
				}
			}

			if (viewType.equals(ViewType1.SUPPLIER)) {

				for (CDN cdn : reconsile) {
					List<CdnData> cdnDatas = cdn.getCdnDatas();
					cdnDatas.forEach((temp) -> {
						if (temp.getType().equals(ReconsileType.MISSING_INWARD)) {
							cdnDatas.forEach((temp1) -> {
								if (temp1.getType().equals(ReconsileType.MISSING_OUTWARD)) {
									if (temp.getCreditDebitDate().equals(temp1.getCreditDebitDate())
											&& temp.getTaxableValue().equals(temp1.getTaxableValue())
											&& temp.getTaxAmount().equals(temp1.getTaxAmount())) {
										temp.setReconcileFlags(ReconsileSubType.INVOICE_NO.toString());
										try {
											temp.setMismatchedInvoice(
													JsonMapper.objectMapper.writeValueAsString(temp1));
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
							});
						}
					});
				}

			} else if (viewType.equals(ViewType1.BUYER)) {

				for (CDN cdn : reconsile) {
					List<CdnData> cdnDatas = cdn.getCdnDatas();
					cdnDatas.forEach((temp) -> {
						if (temp.getType().equals(ReconsileType.MISSING_OUTWARD)) {
							cdnDatas.forEach((temp1) -> {
								if (temp1.getType().equals(ReconsileType.MISSING_INWARD)) {
									if (temp.getCreditDebitDate().equals(temp1.getCreditDebitDate())
											&& temp.getTaxableValue().equals(temp1.getTaxableValue())
											&& temp.getTaxAmount().equals(temp1.getTaxAmount())) {
										temp.setReconcileFlags(ReconsileSubType.INVOICE_NO.toString());
										try {
											temp.setMismatchedInvoice(
													JsonMapper.objectMapper.writeValueAsString(temp1));
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
							});
						}
					});
				}

			}

			Map<String, Object> temp = new HashMap<>();
			temp.put("reconcile", reconsile);
			temp.put("missing", missingCount);
			if (viewType.equals(ViewType1.SUPPLIER)) {
				temp.put("data", supplier);
			} else if (viewType.equals(ViewType1.BUYER)) {
				temp.put("data", buyer);
			}

			return JsonMapper.objectMapper.writeValueAsString(temp);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public Object validateData(Object object, String monthYear) throws AppException {

		try {
			// SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";
			String returnType = jsonObject.get("return") != null ? jsonObject.get("return").toString()
					: ReturnType.GSTR1.toString();

			int missingCount = jsonObject.has("missing") && jsonObject.get("missing") != null
					? jsonObject.getInt("missing")
					: 0;

			List<CDN> cdns = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<CDN>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			/*
			 * Calendar cal = Calendar.getInstance(); Date currentDate = cal.getTime(); int
			 * currYear = currentDate.getYear(); if (currentDate.before(inputMonth)) {
			 * inputMonth = currentDate; }
			 */

			Date startDate = new Date();

			double totalTaxableValue = 0.0;
			double totalTaxAmount = 0;
			double amTaxableValue = 0.0;
			double amTaxAmount = 0;

			boolean summaryError = false;

			double matchedTaxableValue = 0.0;
			double matchedTaxAmount = 0;
			double mismatchedTaxableValue = 0.0;
			double mismatchedTaxAmount = 0;

			double igstAmount = 0.0;
			double cgstAmount = 0.0;
			double sgstAmount = 0.0;
			double cessAmount = 0.0;

			double itcClaimedAmount = 0.0;
			double itcNotClaimedAmount = 0.0;
			double itcPendingAmount = 0.0;

			int matchedCount = 0;
			int mismatchedCount = 0;
			int errorCount = 0;
			int pendingCount = 0;
			int acceptCount = 0;
			int rejectCount = 0;

			double matchPercentage = 0.00;

			int invoiceCount = 0;

			int dateCount = 0;
			int roundOffCount = 0;
			int majorCount = 0;
			int invoiceNoCount = 0;

			List<MismatchDTO> mismatchDTOs = new ArrayList<>();

			for (CDN cdn : cdns) {
				double taxableValue = 0.0;
				double taxAmount = 0;
				List<CdnData> cdnDatas = cdn.getCdnDatas();
				for (CdnData cdnData : cdnDatas) {

					boolean amountFlag = false;
					boolean isCredit = false;

					boolean processFlag = !Objects.isNull(cdnData.getTaxPayerAction())
							&& (cdnData.getTaxPayerAction().equals(TaxpayerAction.REJECT)
									|| cdnData.getTaxPayerAction().equals(TaxpayerAction.PENDING)
									|| cdnData.getTaxPayerAction().equals(TaxpayerAction.ACCEPT));

					double taxAmountInv = 0;

					Map<String, String> errors = new HashMap<>();
					String dateError = "";
					boolean errorFlag = true;
					Date invDate = cdnData.getOrigDebtCredtDate();
					Date noteDate = cdnData.getCreditDebitDate();

					if (!StringUtils.isEmpty(cdnData.getCreditDebitType())) {
						if (cdnData.getCreditDebitType().equalsIgnoreCase("credit")
								|| cdnData.getCreditDebitType().equalsIgnoreCase("cr")
								|| cdnData.getCreditDebitType().equalsIgnoreCase("c")) {
							isCredit = Boolean.TRUE;
						}
					}

					if (!processFlag) {
						if (!(noteDate.compareTo(invDate) >= 0)) {
							dateError = "Invalid note or invoice date, note date cannot be before invoice date";
							errors.put("INVOICE_DATE", dateError);
							errorFlag = false;
						}
						if (!(noteDate.compareTo(new Date()) < 0)) {
							dateError = "Invalid note date, it should be <= " + sdf.format(new Date());
							errors.put("NOTE_DATE", dateError);
							errorFlag = false;
						}

						String dateErrorMsg = "";
						if (inputMonth.getMonth() < 9) {
							startDate.setDate(1);
							startDate.setMonth(3);
							startDate.setYear(inputMonth.getYear() - 1);
							dateErrorMsg = "Invalid note date, it should be between " + sdf.format(startDate) + " and "
									+ sdf.format(inputMonth);
						} else {
							startDate.setDate(1);
							startDate.setMonth(3);
							startDate.setYear(inputMonth.getYear());
							dateErrorMsg = "Invalid note date, it should be between " + sdf.format(startDate) + " and "
									+ sdf.format(inputMonth);
						}
						if (!(noteDate.compareTo(startDate) >= 0 && noteDate.compareTo(inputMonth) <= 0)) {
							errors.put("NOTE_DATE", dateErrorMsg);
							errorFlag = false;
						}

						if (!Objects.isNull(cdnData.getOrigDebtCredtDate())
								&& !Objects.isNull(cdnData.getOrigDebtCredtNoteNo())) {
							cdnData.setAmendment(true);
						} else {
							cdnData.setAmendment(false);
						}

						Double invoiceValue = cdnData.getTaxableValue();
						if (invoiceValue < 0.0) {
							errors.put("INVOICE_VALUE", "Invalid differential value, it must be greater than zero");
							errorFlag = false;
						}

						cdnData.setError(errors);
						cdnData.setValid(errorFlag);

						if (!errorFlag) {
							summaryError = true;
							errorCount += 1;
						}

						if (!cdnData.isValid()) {
							cdnData.setType(ReconsileType.ERROR);
						}
					} else {
						cdnData.setError(new HashMap<>());
						cdnData.setValid(Boolean.TRUE);
					}

					/*
					 * if (!Objects.isNull(cdnData.getCgstAmt())) { taxAmountInv +=
					 * cdnData.getCgstAmt(); } if (!Objects.isNull(cdnData.getSgstAmt())) {
					 * taxAmountInv += cdnData.getSgstAmt(); } if
					 * (!Objects.isNull(cdnData.getIgstAmt())) { taxAmountInv +=
					 * cdnData.getIgstAmt(); } if (!Objects.isNull(cdnData.getCessAmt())) {
					 * taxAmountInv += cdnData.getCessAmt(); }
					 */
					cdnData.setTaxAmount(taxAmountInv);

					if (processFlag) {
						if (cdnData.getTaxPayerAction().equals(TaxpayerAction.ACCEPT)) {
							acceptCount += 1;
							matchedCount += 1;
							// matchedTaxableValue += cdnData.getValue();
							matchedTaxAmount += taxAmountInv;

							invoiceCount++;

						} else if (cdnData.getTaxPayerAction().equals(TaxpayerAction.REJECT)) {
							rejectCount += 1;
							amountFlag = true;
						} else if (cdnData.getTaxPayerAction().equals(TaxpayerAction.PENDING)) {
							pendingCount += 1;
							amountFlag = true;
						}
					} else {

						invoiceCount++;

						if (!Objects.isNull(cdnData.getType())) {
							if (cdnData.getType().equals(ReconsileType.MISMATCH)) {
								mismatchedCount += 1;
								mismatchedTaxableValue += cdnData.getTaxableValue();
								mismatchedTaxAmount += taxAmountInv;

								double tempTaxableValue = 0.0;
								double tempTaxAmount = 0;
								MismatchDTO mismatchDTO = new MismatchDTO();
								mismatchDTO.setGstin(cdn.getGstin());
								mismatchDTO.setGstinName(cdn.getGstinName());
								if (mismatchDTOs.contains(mismatchDTO)) {
									mismatchDTO = mismatchDTOs.get(mismatchDTOs.indexOf(mismatchDTO));
									tempTaxableValue = mismatchDTO.getTaxableValue();
									tempTaxAmount = mismatchDTO.getTaxAmount();
								} else {
									mismatchDTOs.add(mismatchDTO);
								}
								tempTaxableValue += cdnData.getTaxableValue();
								tempTaxAmount += taxAmountInv;
								mismatchDTO.setTaxableValue(tempTaxableValue);
								mismatchDTO.setTaxAmount(tempTaxAmount);

							} /*
								 * else if (cdnData.getType().equals(ReconsileType. MISSING_SUPPLIER) &&
								 * returnType.equalsIgnoreCase(ReturnType. GSTR2.toString())) { missingCount +=
								 * 1; } else if (cdnData.getType().equals(ReconsileType. MISSING_BUYER) &&
								 * returnType.equalsIgnoreCase(ReturnType. GSTR1.toString())) { missingCount +=
								 * 1; }
								 */

							String rType = cdnData.getReconcileFlags();
							if (!StringUtils.isEmpty(rType)) {
								if (rType.contains(ReconsileSubType.DATE.toString())) {
									dateCount += 1;
								}
								if (rType.equals(ReconsileSubType.ROUNDING_OFF.toString())) {
									roundOffCount += 1;
								}
								if (rType.equals(ReconsileSubType.MAJOR.toString())) {
									majorCount += 1;
								}
								if (rType.contains(ReconsileSubType.INVOICE_NO.toString())) {
									invoiceNoCount += 1;
								}
							}
						}
					}

					if (!amountFlag) {

						if (returnType.equalsIgnoreCase(ReturnType.GSTR1.toString())) {
							/*
							 * if (!Objects.isNull(cdnData.getCgstAmt())) { if (isCredit) cgstAmount -=
							 * cdnData.getCgstAmt(); else cgstAmount += cdnData.getCgstAmt(); } if
							 * (!Objects.isNull(cdnData.getSgstAmt())) { if (isCredit) sgstAmount -=
							 * cdnData.getSgstAmt(); else sgstAmount += cdnData.getSgstAmt(); } if
							 * (!Objects.isNull(cdnData.getIgstAmt())) { if (isCredit) igstAmount -=
							 * cdnData.getIgstAmt(); else igstAmount += cdnData.getIgstAmt(); } if
							 * (!Objects.isNull(cdnData.getCessAmt())) { if (isCredit) cessAmount -=
							 * cdnData.getCessAmt(); else cessAmount += cdnData.getCessAmt(); }
							 */
						} else if (returnType.equalsIgnoreCase(ReturnType.GSTR2.toString())) {
							/*
							 * ItcDetail itcDetail = cdnData.getItcDetails(); String elg =
							 * cdnData.getEligOfTotalTax();
							 */
							/*
							 * if (!Objects.isNull(itcDetail. getTotInpTaxCrdtAvalForClaimThisMonthCgst()))
							 * { if (isCredit) { cgstAmount -= itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthCgst(); itcPendingAmount -=
							 * StringUtils.isEmpty(elg) ? cdnData.getCgstAmt() : 0.0; itcNotClaimedAmount -=
							 * elg != null && elg.equals("no") ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthCgst() : 0.0; itcClaimedAmount -= elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthCgst() : 0.0; } else { cgstAmount +=
							 * itcDetail. getTotInpTaxCrdtAvalForClaimThisMonthCgst(); itcPendingAmount +=
							 * StringUtils.isEmpty(elg) ? cdnData.getCgstAmt() : 0.0; itcNotClaimedAmount +=
							 * elg != null && elg.equals("no") ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthCgst() : 0.0; itcClaimedAmount += elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthCgst() : 0.0; } }
							 */
							/*
							 * if (!Objects.isNull(itcDetail. getTotInpTaxCrdtAvalForClaimThisMonthSgst()))
							 * { if (isCredit) { sgstAmount -= itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthSgst(); itcPendingAmount -=
							 * StringUtils.isEmpty(elg) ? cdnData.getSgstAmt() : 0.0; itcNotClaimedAmount -=
							 * elg != null && elg.equals("no") ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthSgst() : 0.0; itcClaimedAmount -= elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthSgst() : 0.0; } else { sgstAmount +=
							 * itcDetail. getTotInpTaxCrdtAvalForClaimThisMonthSgst(); itcPendingAmount +=
							 * StringUtils.isEmpty(elg) ? cdnData.getSgstAmt() : 0.0; itcNotClaimedAmount +=
							 * elg != null && elg.equals("no") ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthSgst() : 0.0; itcClaimedAmount += elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthSgst() : 0.0; } }
							 */
							/*
							 * if (!Objects.isNull(itcDetail. getTotInpTaxCrdtAvalForClaimThisMonthIgst()))
							 * { if (isCredit) { igstAmount -= itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthIgst(); itcPendingAmount -=
							 * StringUtils.isEmpty(elg) ? cdnData.getIgstAmt() : 0.0; itcNotClaimedAmount -=
							 * elg != null && elg.equals("no") ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthIgst() : 0.0; itcClaimedAmount -= elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthIgst() : 0.0; } else { igstAmount +=
							 * itcDetail. getTotInpTaxCrdtAvalForClaimThisMonthIgst(); itcPendingAmount +=
							 * StringUtils.isEmpty(elg) ? cdnData.getIgstAmt() : 0.0; itcNotClaimedAmount +=
							 * elg != null && elg.equals("no") ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthIgst() : 0.0; itcClaimedAmount += elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ? itcDetail.
							 * getTotInpTaxCrdtAvalForClaimThisMonthIgst() : 0.0; } }
							 */
							/*
							 * if (!Objects.isNull(itcDetail. getTotCessAvailableItcThisMonth())) { if
							 * (isCredit) { cessAmount -= itcDetail.getTotCessAvailableItcThisMonth();
							 * itcPendingAmount -= StringUtils.isEmpty(elg) ? cdnData.getCessAmt() : 0.0;
							 * itcNotClaimedAmount -= elg != null && elg.equals("no") ?
							 * itcDetail.getTotCessAvailableItcThisMonth() : 0.0; itcClaimedAmount -= elg !=
							 * null && (elg.equals("ip") || elg.equals("cp")) ?
							 * itcDetail.getTotCessAvailableItcThisMonth() : 0.0; } else { cessAmount +=
							 * itcDetail.getTotCessAvailableItcThisMonth(); itcPendingAmount +=
							 * StringUtils.isEmpty(elg) ? cdnData.getCessAmt() : 0.0; itcNotClaimedAmount +=
							 * elg != null && elg.equals("no") ? itcDetail.getTotCessAvailableItcThisMonth()
							 * : 0.0; itcClaimedAmount += elg != null && (elg.equals("ip") ||
							 * elg.equals("cp")) ? itcDetail.getTotCessAvailableItcThisMonth() : 0.0; } }
							 */
						}

						if (cdnData.isAmendment()) {
							// amend flag
						}

						if (cdnData.isAmendment()) {
							amTaxableValue += cdnData.getTaxableValue();
							amTaxAmount += taxAmountInv;
						} else {
							totalTaxableValue += cdnData.getTaxableValue();
							totalTaxAmount += taxAmountInv;
						}

						taxableValue += cdnData.getTaxableValue();
						taxAmount += taxAmountInv;
					}

					cdn.setTaxableValue(taxableValue);
					cdn.setTaxAmount(taxAmount);
				}
			}

			Collections.sort(mismatchDTOs);

			if (invoiceCount > 0) {
				if (acceptCount > 0)
					matchPercentage = ((double) acceptCount / invoiceCount) * 100;
			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setAcceptCount(acceptCount);
			reconcileDTO.setRejectCount(rejectCount);
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setPendingCount(pendingCount);
			reconcileDTO.setMatchCount(matchedCount);
			reconcileDTO.setMatchPercentage(matchPercentage);
			reconcileDTO.setMismatchCount(mismatchedCount);
			reconcileDTO.setMissingCount(missingCount);
			reconcileDTO.setMatchedTaxableValue(matchedTaxableValue);
			reconcileDTO.setMatchedTaxAmount(matchedTaxAmount);
			reconcileDTO.setMismatchedTaxableValue(mismatchedTaxableValue);
			reconcileDTO.setMismatchedTaxAmount(mismatchedTaxAmount);
			reconcileDTO.setDateCount(dateCount);
			reconcileDTO.setRoundOffCount(roundOffCount);
			reconcileDTO.setMajorCount(majorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);
			reconcileDTO.setInvoiceNoCount(invoiceNoCount);
			reconcileDTO.setItcClaimedAmount(itcClaimedAmount);
			reconcileDTO.setItcNotClaimedAmount(itcNotClaimedAmount);
			reconcileDTO.setItcPendingAmount(itcPendingAmount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", cdns);
			TransactionDataDTO cdnSumm = new TransactionDataDTO();
			cdnSumm.setTaxableValue(totalTaxableValue);
			cdnSumm.setTaxAmount(totalTaxAmount);
			cdnSumm.setAmTaxableValue(amTaxableValue);
			cdnSumm.setAmTaxAmount(amTaxAmount);
			cdnSumm.setError(summaryError);
			cdnSumm.setReconcileDTO(reconcileDTO);
			cdnSumm.setMismatchDTO(mismatchDTOs);
			cdnSumm.setCessAmount(cessAmount);
			cdnSumm.setIgstAmount(igstAmount);
			cdnSumm.setCgstAmount(cgstAmount);
			cdnSumm.setSgstAmount(sgstAmount);
			gstrSummaryDTO.setCdn(cdnSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.ALL, returnType);
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file
		List<List<String>> tabularData = new ArrayList<>();
		try {

			List<CDNDetailEntity> b2bDatas = datas;
			for (CDNDetailEntity invoice : b2bDatas) {

				for (Item item : invoice.getItems()) {
					List<String> columns = this.getFlatData(invoice, item, returnType);
					tabularData.add(columns);
				}
			}

		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;

	}

	private List<String> getFlatData(CDNDetailEntity invoice, Item item, String returnType) {

		List<String> columns = new ArrayList<>();
		columns.add(this.getStringValue(invoice.getCdnTransaction().getOriginalCtin()));
		columns.add(this.getStringValue(invoice.getCdnTransaction().getOriginalCtinName()));
		columns.add(this.getStringValue(invoice.getInvoiceNumber()));
		columns.add(this.getStringValue(invoice.getInvoiceDate()));
		columns.add(this.getStringValue(invoice.getRevisedInvNo()));
		columns.add(this.getStringValue(invoice.getRevisedInvDate()));
		columns.add(this.getStringValue(invoice.getNoteType()));

		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxableValue()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCgst()));
		columns.add(this.getStringValue(item.getSgst()));
		columns.add(this.getStringValue(item.getCess()));

		if (ReturnType.GSTR1.toString().equalsIgnoreCase(returnType)) {
			columns.add(this.getStringValue(invoice.getReasonForNote()));
			columns.add(this.getStringValue(invoice.getPreGstRegime()));
		}

		if (ReturnType.GSTR2.toString().equalsIgnoreCase(returnType)) {
			columns.add(this.getStringValue(item.getTotalEligibleTax()));
			columns.add(this.getStringValue(item.getItcIgst()));
			columns.add(this.getStringValue(item.getItcCgst()));
			columns.add(this.getStringValue(item.getItcSgst()));
			columns.add(this.getStringValue(item.getItcCess()));
		}
		if (ReturnType.GSTR2.toString().equalsIgnoreCase(returnType)) {
			columns.add(this.getStringValue(invoice.getPreGstRegime()));
			columns.add(this.getStringValue(invoice.getReasonForNote()));
		}
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));

		return columns;

	}

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<CDN> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<CDN>>() {
				});

				for (CDN cdn : data) {
					cdn.getCdnDatas().removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);
				}

				data.removeIf(i -> i.getCdnDatas().isEmpty());

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<CDN> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<CDN>>() {
				});

				if (subType.equals(ReconsileSubType.PENDING_ITC.toString())) {
					/*
					 * for (CDN cdn : data) { cdn.getCdnDatas().removeIf(i -> i.getReconcileFlags()
					 * == null || !i.getReconcileFlags().contains(ReconsileSubType.OWN.toString()));
					 * cdn.getCdnDatas() .removeIf(it -> it.getEligOfTotalTax() != null &&
					 * (it.getEligOfTotalTax().equals("no") || it.getEligOfTotalTax().equals("ip")
					 * || it.getEligOfTotalTax().equals("cp"))); }
					 */
				} else {
					for (CDN cdn : data) {
						cdn.getCdnDatas().removeIf(
								i -> i.getReconcileFlags() == null || !i.getReconcileFlags().contains(subType));
					}
				}

				data.removeIf(i -> i.getCdnDatas().isEmpty());

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {

		List<CDN> cdns = new ArrayList<>();

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.GINESYS.toString();

		try {
			JsonNode objects = JsonMapper.objectMapper.readTree(data);

			for (JsonNode jsonNode : objects) {

				CDN cdn = new CDN();

				if (jsonNode.has("ctin"))
					cdn.setGstin(jsonNode.get("ctin").asText());

				if (jsonNode.has("ctin_name")) {
					cdn.setGstinName(jsonNode.get("ctin_name").asText());
				}

				/*
				 * if (jsonNode.has("noteType")) cdn.setType(jsonNode.get("noteType").asText());
				 */
				List<CdnData> cdnDatas = new ArrayList<>();
				CdnData cdnData = new CdnData();
				cdnDatas.add(cdnData);
				cdn.setCdnDatas(cdnDatas);

				cdnData.setSource(sourceName);
				cdnData.setSourceId(sourceId);

				// setting whether this is credit or debit
				if (jsonNode.has("noteType")) {
					if (jsonNode.get("noteType").asText().equalsIgnoreCase("C")
							|| jsonNode.get("noteType").asText().equalsIgnoreCase("CREDIT")
							|| jsonNode.get("noteType").asText().equalsIgnoreCase("CR"))
						cdnData.setCreditDebitType("Credit");
					else if (jsonNode.get("noteType").asText().equalsIgnoreCase("D")
							|| jsonNode.get("noteType").asText().equalsIgnoreCase("DEBIT")
							|| jsonNode.get("noteType").asText().equalsIgnoreCase("DR"))
						cdnData.setCreditDebitType("Debit");
				}

				// setting cd number
				if (jsonNode.has("noteNumber"))
					cdnData.setCreditDebitNum(jsonNode.get("noteNumber").asText());

				if (jsonNode.has("cdDate")) {
					try {
						cdnData.setCreditDebitDate(sdfGcc.parse(jsonNode.get("cdDate").asText()));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				/*
				 * if (jsonNode.has("reason"))
				 * cdnData.setReason(jsonNode.get("reason").asText());
				 * 
				 * if (jsonNode.has("invoiceNumber"))
				 * cdnData.setOriginalNum(jsonNode.get("invoiceNumber").asText() );
				 */
				if (jsonNode.has("invoiceDate")) {
					/*
					 * try { cdnData.setInvoiceDate(sdfGcc.parse(jsonNode.get(
					 * "invoiceDate").asText())); } catch (ParseException e) { e.printStackTrace();
					 * }
					 */
				}

				/*
				 * if (jsonNode.has("value"))
				 * cdnData.setValue(Double.parseDouble(jsonNode.get("value"). asText())); if
				 * (jsonNode.has("igstr"))
				 * cdnData.setIgstRate(Double.parseDouble(jsonNode.get("igstr"). asText())); if
				 * (jsonNode.has("igsta"))
				 * cdnData.setIgstAmt(Double.parseDouble(jsonNode.get("igsta"). asText())); if
				 * (jsonNode.has("cgstr"))
				 * cdnData.setCgstRate(Double.parseDouble(jsonNode.get("cgstr"). asText())); if
				 * (jsonNode.has("cgsta"))
				 * cdnData.setCgstAmt(Double.parseDouble(jsonNode.get("cgsta"). asText())); if
				 * (jsonNode.has("sgstr"))
				 * cdnData.setSgstRate(Double.parseDouble(jsonNode.get("sgstr"). asText())); if
				 * (jsonNode.has("sgsta"))
				 * cdnData.setSgstAmt(Double.parseDouble(jsonNode.get("sgsta"). asText()));
				 */
				// setting reverse charge
				/*
				 * if (jsonNode.has("reverse") & jsonNode.has("reversePer")) { if
				 * (jsonNode.get("reverse").asText().equalsIgnoreCase("Y") ||
				 * jsonNode.get("reverse").asText().equalsIgnoreCase("YES")) {
				 * cdnData.setReverseCharge(true); int i =
				 * Double.valueOf(jsonNode.get("reversePer").asText()).intValue( );
				 * cdnData.setReverseChargePercentage(Double.valueOf(i).intValue ()); } else if
				 * (jsonNode.get("reverse").asText().equalsIgnoreCase("N") ||
				 * jsonNode.get("reverse").asText().equalsIgnoreCase("NO")) {
				 * cdnData.setReverseCharge(false); cdnData.setReverseChargePercentage(0); } }
				 */

				ItcDetail itcDetails = new ItcDetail();
				// cdnData.setItcDetails(itcDetails);

				if (jsonNode.has("eitc"))
					// cdnData.setEligOfTotalTax(jsonNode.get("eitc").asText());
					if (jsonNode.has("ttaigst"))
						itcDetails.setTotalTaxAvalIgst(Double.parseDouble(jsonNode.get("ttaigst").asText()));
				if (jsonNode.has("ttacgst"))
					itcDetails.setTotalTaxAvalCgst(Double.parseDouble(jsonNode.get("ttacgst").asText()));
				if (jsonNode.has("ttasgst"))
					itcDetails.setTotalTaxAvalSgst(Double.parseDouble(jsonNode.get("ttasgst").asText()));
				/*
				 * if (jsonNode.has("itcigst"))
				 * itcDetails.setTotInpTaxCrdtAvalForClaimThisMonthIgst(
				 * Double.parseDouble(jsonNode.get("itcigst").asText())); if
				 * (jsonNode.has("itccgst"))
				 * itcDetails.setTotInpTaxCrdtAvalForClaimThisMonthCgst(
				 * Double.parseDouble(jsonNode.get("itccgst").asText())); if
				 * (jsonNode.has("itcsgst"))
				 * itcDetails.setTotInpTaxCrdtAvalForClaimThisMonthSgst(
				 * Double.parseDouble(jsonNode.get("itcsgst").asText()));
				 */

				addCDNObject(cdn, cdns);
			}

			return JsonMapper.objectMapper.writeValueAsString(cdns);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public EmailDTO getEmailContent(EmailDTO emailDTO, Object primaryInvoice, Object secondaryInvocie)
			throws AppException {

		CDNDetailEntity cdnPrimary = (CDNDetailEntity) primaryInvoice;
		CDNDetailEntity cdnSecondary = (CDNDetailEntity) secondaryInvocie;
		try {

			String body = emailDTO.getMailbody();

			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");

			body = body.replace("[PRIMARY_COMPANY_NAME]",
					cdnPrimary.getCdnTransaction().getGstin().getTaxpayer().getLegalName());
			body = body.replace("[PRIMARY_GSTIN]", cdnPrimary.getCdnTransaction().getGstin().getGstin());
			// body = body.replace("[PRIMARY_TYPE]", cdnPrimary.getNoteType());
			body = body.replace("[PRIMARY_NOTE_NO]", cdnPrimary.getInvoiceNumber());
			body = body.replace("[PRIMARY_NOTE_DATE]", sdf.format(cdnPrimary.getInvoiceDate()));
			// body = body.replace("[PRIMARY_INVOICE_NO]", cdnPrimary.getRevisedInvNo());
			// body = body.replace("[PRIMARY_INVOICE_DATE]",
			// sdf.format(cdnPrimary.getRevisedInvDate()));
			body = body.replace("[PRIMARY_NOTE_VALUE]", cdnPrimary.getTaxableValue() + cdnPrimary.getTaxAmount() + "");
			body = body.replace("[PRIMARY_DIFFERENTIAL_VALUE]", cdnPrimary.getTaxableValue() + "");
			body = body.replace("[PRIMARY_DIFFERENTIAL_TAX]", cdnPrimary.getTaxAmount() + "");

			String primaryNoteDate = sdf.format(cdnPrimary.getRevisedInvDate());
			String primaryInvoiceNo = cdnPrimary.getRevisedInvNo();
			String primaryInvoiceDate = sdf.format(cdnPrimary.getInvoiceDate());
			String primaryDifferentialValue = cdnPrimary.getTaxableValue() + "";
			String primaryDifferentialTax = cdnPrimary.getTaxAmount() + "";

			ReconsileType reconsileType = ReconsileType.valueOf(StringUtils.upperCase(cdnPrimary.getType()));

			if (ReconsileType.MISMATCH.equals(reconsileType) || ReconsileType.MISMATCH_INVOICENO == reconsileType
					|| ReconsileType.MISMATCH_DATE == reconsileType
					|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
					|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
					|| ReconsileType.MISMATCH_MAJOR == reconsileType) {

				body = body.replace("[SECONDARY_COMPANY_NAME]", cdnSecondary.getCdnTransaction().getOriginalCtinName());
				body = body.replace("[SECONDARY_GSTIN]", cdnSecondary.getCdnTransaction().getOriginalCtin());
				// body = body.replace("[SECONDARY_TYPE]", cdnPrimary.getNoteType());
				body = body.replace("[SECONDARY_NOTE_NO]", cdnSecondary.getInvoiceNumber());
				body = body.replace("[SECONDARY_NOTE_DATE]", sdf.format(cdnSecondary.getInvoiceDate()));
				// body = body.replace("[SECONDARY_INVOICE_NO]", cdnPrimary.getRevisedInvNo());
				// body = body.replace("[SECONDARY_INVOICE_DATE]",
				// sdf.format(cdnPrimary.getRevisedInvDate()));
				body = body.replace("[SECONDARY_NOTE_VALUE]",
						cdnSecondary.getTaxableValue() + cdnSecondary.getTaxAmount() + "");
				body = body.replace("[SECONDARY_DIFFERENTIAL_VALUE]", cdnSecondary.getTaxableValue() + "");
				body = body.replace("[SECONDARY_DIFFERENTIAL_TAX]", cdnSecondary.getTaxAmount() + "");

				String secondaryNoteDate = sdf.format(cdnSecondary.getRevisedInvDate());
				String secondaryInvoiceNo = cdnSecondary.getRevisedInvNo();
				String secondaryInvoiceDate = sdf.format(cdnSecondary.getInvoiceNumber());
				String secondaryDifferentialValue = cdnSecondary.getTaxableValue() + "";
				String secondaryDifferentialTax = cdnSecondary.getTaxAmount() + "";

				if (primaryNoteDate.equalsIgnoreCase(secondaryNoteDate)) {
					body = body.replace("[colorNoteDate]", "black");
				} else {
					body = body.replace("[colorNoteDate]", "red");
				}

				if (primaryInvoiceNo.equalsIgnoreCase(secondaryInvoiceNo)) {
					body = body.replace("[colorInvoiceNo]", "black");
				} else {
					body = body.replace("[colorInvoiceNo]", "red");
				}

				if (primaryInvoiceDate.equalsIgnoreCase(secondaryInvoiceDate)) {
					body = body.replace("[colorInvoiceDate]", "black");
				} else {
					body = body.replace("[colorInvoiceDate]", "red");
				}

				if (primaryDifferentialValue.equalsIgnoreCase(secondaryDifferentialValue)) {
					body = body.replace("[colorDifferentialValue]", "black");
				} else {
					body = body.replace("[colorDifferentialValue]", "red");
				}

				if (primaryDifferentialTax.equalsIgnoreCase(secondaryDifferentialTax)) {
					body = body.replace("[colorDifferentialTax]", "black");
				} else {
					body = body.replace("[colorDifferentialTax]", "red");
				}

			}
			emailDTO.setRemarks("");
			emailDTO.setMailbody(body);

			return emailDTO;

		} catch (Exception e) {
			_Logger.error("excepiton in getemailcontent cdn ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {

		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getCdn();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceCdns = getData(source, returnType);

		Map<String, List<String>> changedCdns = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionB2bs = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceCdns) && !Objects.isNull(changedCdns)) {

			Iterator it = sourceCdns.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedCdns.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedCdns.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedCdns.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceCdns.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionB2bs.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionB2bs.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionB2bs.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionB2bs;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<CDN> cdns = objectMapper.readValue(object, new TypeReference<List<B2B>>() {
				});

				for (CDN cdn : cdns) {
					List<CdnData> dataInvoices = cdn.getCdnDatas();
					for (CdnData data : dataInvoices) {
						for (InvoiceItem item : data.getItems()) {
							// List<String> columns = getFlatData(cdn, data,item, returnType);

							String temp = StringUtils.isEmpty(cdn.getGstin()) ? ""
									: cdn.getGstin() + ":" + data.getCreditDebitNum() + ":" + item.getSNo();
							// tabularData.put(temp, columns);
						}

					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<CDN> cdns = JsonMapper.objectMapper.readValue(object, new TypeReference<List<CDN>>() {
			});

			List<CDN> currentCdns = new ArrayList<>();

			for (CDN cdn : cdns) {
				CDN cdnNew = new CDN();
				List<CdnData> cdnDatasNew = new ArrayList<>();
				cdnNew.setGstin(cdn.getGstin());
				List<CdnData> cdnDatas = cdn.getCdnDatas();
				for (CdnData cdnData : cdnDatas) {
					if (!cdnData.isAmendment() && !cdnData.isTransit()) {
						cdnDatasNew.add(cdnData);
						cdnData.setTransit(true);
						cdnData.setTransitId(transitId);
					}
				}
				if (!cdnNew.getCdnDatas().isEmpty()) {
					currentCdns.add(cdnNew);
				}
			}

			// gstr1.setCdn(currentCdns);

			if (currentCdns.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(cdns);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<CDN> cdns = JsonMapper.objectMapper.readValue(object, new TypeReference<List<CDN>>() {
			});

			for (CDN cdn : cdns) {
				List<CdnData> cdnDatas = cdn.getCdnDatas();
				for (CdnData cdnData : cdnDatas) {
					if (cdnData.isTransit() && cdnData.getTransitId().equals(transitId)) {
						cdnData.setTransit(false);
						cdnData.setTransitId("");

						cdnData.setGstnSynced(true);
					}
				}
			}

			return JsonMapper.objectMapper.writeValueAsString(cdns);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
		gstExcel.loadSheet(6);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				
				if(temp!=null) {
//					System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
			}
		}
		return 0;
	}
}
