package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;

public class B2CLService extends ItemService{
	
	public int inserUpdate(List<B2CLTransactionEntity> b2clTransactionEntities,String delete) throws SQLException, JsonProcessingException, InterruptedException
	{
		int updateCount=0;
		if(b2clTransactionEntities != null && !b2clTransactionEntities.isEmpty()){

			_Logger.info("Inserting B2CL records for gstin {}",b2clTransactionEntities.get(0).getGstin().getGstin());
			String b2clExistForDelete="select bd.isSynced,bd.id from b2cl_transaction bt,b2cl_details bd where bt.taxpayerGstinId=? and bt.returnType=? and bt.monthYear=? and bt.id=bd.b2clTransactionId and bd.isDelete=0";
			
			String updateDelFlag="update b2cl_details set isDelete=1,flags='DELETE' where id=?";
			
			String deleteIfNotSynced="delete from b2cl_details where id=?";
			
			String b2clDeleteDuplicate="DELETE a from b2cl_transaction bt,b2cl_details a JOIN(SELECT invoiceNumber from b2cl_details GROUP by invoiceNumber HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.isDelete=1 and a.isSynced=1 and bt.id=a.b2clTransactionId and bt.taxpayerGstinId=? and bt.returnType=? and bt.monthYear=?";
			
		String inserQueryB2CLTransaction="INSERT INTO `b2cl_transaction` (`id`, `monthYear`, `returnType`, `taxAmount`, `taxableValue`, `creationTime`, `creationIpAddress`, "
				+ "`createdBy`, `updationTime`,"
				+ " `updationIpAddress`, `updatedBy`, `taxpayerGstinId`) "
				+ "VALUES (?,?,?,NULL,NULL,?,?,?,?,?,?,?)\n" + 
				"\n" + 
				""; 
		
		String b2bTransactionExist="select id from b2cl_transaction where taxpayerGstinId=? and monthYear=? and returnType=? ";

      String sql ="{call b2clInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
   int count=0;
   boolean update=false;

		
try(Connection connection=	DBUtils.getConnection();
	
	PreparedStatement b2clTransctionPS =connection.prepareStatement(inserQueryB2CLTransaction);
		
		PreparedStatement b2clDelDuplicate=connection.prepareStatement(b2clDeleteDuplicate);
		
		PreparedStatement b2clDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
		
	CallableStatement  b2clDetailCS =connection.prepareCall(sql);//connection.prepareStatement(insertQueryB2bDetails);
	
	PreparedStatement b2clExistsPS=connection.prepareStatement(b2bTransactionExist);
		
		CallableStatement itemPS = connection.prepareCall(itemInsertSql);
		){
	connection.setAutoCommit(false);
	
	if ("YES".equalsIgnoreCase(delete)) {
		
		try(Connection delConn=DBUtils.getConnection();
			PreparedStatement b2clDelExist=delConn.prepareStatement(b2clExistForDelete);
			PreparedStatement b2clUpdateFlag=delConn.prepareStatement(updateDelFlag);
				){
			delConn.setAutoCommit(false);
		// for marking delete flag
		b2clDelExist.setString(1, b2clTransactionEntities.get(0).getGstin().getId().toString());
		b2clDelExist.setString(2, b2clTransactionEntities.get(0).getReturnType());
		b2clDelExist.setString(3, b2clTransactionEntities.get(0).getMonthYear());

		ResultSet deleteFlagResult = b2clDelExist.executeQuery();

		while (deleteFlagResult.next()) {
			if (deleteFlagResult.getBoolean(1)) {
				b2clUpdateFlag.setString(1, deleteFlagResult.getString(2));
				b2clUpdateFlag.addBatch();
			} else {
				b2clDelNotSynced.setString(1, deleteFlagResult.getString(2));
				b2clDelNotSynced.addBatch();
			}
		}
		b2clUpdateFlag.executeBatch();
		b2clDelNotSynced.executeBatch();
		delConn.commit();
	DBUtils.closeQuietly(delConn);
	}
	}
	
	for(B2CLTransactionEntity b2clTransactionEntity : b2clTransactionEntities)
	{
		b2clExistsPS.setString(1,b2clTransactionEntity.getGstin().getId().toString());
		b2clExistsPS.setString(2,b2clTransactionEntity.getMonthYear());
		b2clExistsPS.setString(3,b2clTransactionEntity.getReturnType());

		ResultSet resultSet=b2clExistsPS.executeQuery();
		
		String id=null;
		if(resultSet.next())
		{
		 id= resultSet.getString(1);
		 if(!"YES".equalsIgnoreCase(delete))
		  update=true;
		}
		DBUtils.closeQuietly(resultSet);
		 if(StringUtils.isEmpty(id))
		 {
		    id=UUID.randomUUID().toString();
		    
		    b2clTransctionPS.setString(1, id);
		    b2clTransctionPS.setString(2,b2clTransactionEntity.getMonthYear());
		    b2clTransctionPS.setString(3,b2clTransactionEntity.getReturnType());
		    
		    
		    b2clTransctionPS.setTimestamp(4,Timestamp.from(Instant.now()));
		    b2clTransctionPS.setString(5,b2clTransactionEntity.getCreationIPAddress());
		    b2clTransctionPS.setString(6,b2clTransactionEntity.getCreatedBy());
		    b2clTransctionPS.setTimestamp(7,Timestamp.from(Instant.now()));
		    b2clTransctionPS.setString(8,b2clTransactionEntity.getUpdationIPAddress());
		    b2clTransctionPS.setString(9,b2clTransactionEntity.getUpdatedBy());

		    b2clTransctionPS.setInt(10,b2clTransactionEntity.getGstin().getId());
		    
		    b2clTransctionPS.execute();
	}
		 
		 for(B2CLDetailEntity b2clDetails : b2clTransactionEntity.getB2clDetails())
		 {
			 OPERATION operation = OPERATION.INSERT;
				String invoiceId = UUID.randomUUID().toString();
				if (update) {

					String checkDetailSql = "select id,isLocked from b2cl_details where b2clTransactionId=? and invoiceNumber=?";

					PreparedStatement checkDetail = connection.prepareStatement(checkDetailSql);

					checkDetail.setString(1, id);
					checkDetail.setString(2, b2clDetails.getInvoiceNumber());

					ResultSet set = checkDetail.executeQuery();

					if (set.next()) {
						if (Objects.nonNull(set.getBoolean(2)) && set.getBoolean(2) && set.getString(1) != null)
							continue;
						invoiceId = set.getString(1);
						operation = OPERATION.UPDATE;
					} else
						invoiceId = UUID.randomUUID().toString();
					DBUtils.closeQuietly(set);
				}
			 
			 b2clDetailCS.setString(1,invoiceId);
			 b2clDetailCS.setString(2,id);
			 b2clDetailCS.setString(3,b2clDetails.getSource());
			 b2clDetailCS.setString(4,b2clDetails.getSourceId());
			 b2clDetailCS.setString(5,b2clDetails.getType());
			 b2clDetailCS.setString(6,b2clDetails.getPos());
			 b2clDetailCS.setString(7,b2clDetails.getStateName());
			 b2clDetailCS.setString(8,b2clDetails.getInvoiceNumber());
			 b2clDetailCS.setDouble(10,b2clDetails.getTaxAmount());
			 b2clDetailCS.setDouble(9,b2clDetails.getTaxableValue());
			 b2clDetailCS.setString(11,b2clDetails.getEtin());
			 b2clDetailCS.setString(12,b2clDetails.getCreatedBy());
			 b2clDetailCS.setTimestamp(13,Timestamp.from(Instant.now()));
			 b2clDetailCS.setString(14,b2clDetails.getCreationIPAddress());
			 b2clDetailCS.setString(15,b2clDetails.getUpdatedBy());
			 b2clDetailCS.setTimestamp(16,Timestamp.from(Instant.now()));
			 b2clDetailCS.setString(17,b2clDetails.getUpdationIPAddress());
			 b2clDetailCS.setDate(18,new Date(b2clDetails.getInvoiceDate().getTime()));
			 b2clDetailCS.setString(19,operation.toString());
			 //ammendment
			 b2clDetailCS.setBoolean(20,b2clDetails.getIsAmmendment());
			 b2clDetailCS.setString(21,b2clDetails.getOriginalInvoiceNumber());
			 if(!Objects.isNull(b2clDetails.getOriginalInvoiceDate()))
				 b2clDetailCS.setDate(22, new Date(b2clDetails.getOriginalInvoiceDate().getTime()));
					else
						b2clDetailCS.setDate(22, null);
			 
			 b2clDetailCS.addBatch();
			 
			 this.insertItems(b2clDetails.getItems(), itemPS,operation, invoiceId);
				itemPS.executeBatch();


			 if(++count % 50==0)
				updateCount+= b2clDetailCS.executeBatch().length;
		 }
		 
	}
	
	updateCount+=b2clDetailCS.executeBatch().length;
	if("YES".equalsIgnoreCase(delete)){
		b2clDelDuplicate.setString(1,b2clTransactionEntities.get(0).getGstin().getId().toString());
		b2clDelDuplicate.setString(2, b2clTransactionEntities.get(0).getReturnType());
		b2clDelDuplicate.setString(3,b2clTransactionEntities.get(0).getMonthYear());
		b2clDelDuplicate.executeUpdate();
    }
	connection.commit();
		_Logger.info("End Inserting B2cl records for gstin {} records {} ",b2clTransactionEntities.get(0).getGstin().getGstin(),updateCount);

	//if(updateCount>0) this.generateForm3B(this.gstin);
	
	}
catch(SQLException exception)
{	updateCount=0;

		_Logger.error("excepton while inserting B2cl records for gstin {}",b2clTransactionEntities.get(0).getGstin().getGstin(),exception);
	throw exception;
}
		}
return updateCount;
}


}
