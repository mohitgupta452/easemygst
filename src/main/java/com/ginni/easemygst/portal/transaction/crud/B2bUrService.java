package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.transaction.crud.ItemService.OPERATION;

public class B2bUrService extends ItemService{
	
	private static final String checkExistSQL="select id,isSubmit from b2bur_transaction where gstin=? and monthYear=? and invoiceNumber=?"
			+ " and returnType=?";

	public int insertUpdate(List<B2bUrTransactionEntity> b2bUrTransactionEntities,String delete) throws SQLException, InterruptedException {
	    int updateCount=0;

		if(b2bUrTransactionEntities != null && !b2bUrTransactionEntities.isEmpty()){
		_Logger.info("Inserting B2bur records for gstin {}",b2bUrTransactionEntities.get(0).getGstin().getGstin());
		
		String sql ="{call b2burInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
		String b2burExistForDelete="select bd.isSynced,bd.id from b2bur_transaction bd where bd.gstin=? and bd.returnType=? and bd.monthYear=?  and bd.isDelete=0";
		
		String updateDelFlag="update b2bur_transaction set isDelete=1,flags='DELETE' where id=?";
		
		String deleteIfNotSynced="delete from b2bur_transaction where id=?";
		
		String deleteItems="DELETE a from items a,b2bur_transaction t JOIN(SELECT invoiceNumber,id from b2bur_transaction GROUP by invoiceNumber,id HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and t.isDelete=1 and t.isSynced=1 and t.gstin=? and t.returnType=? and t.monthYear=?";
		
		String b2burDeleteDuplicate="DELETE a from b2bur_transaction a JOIN(SELECT invoiceNumber from b2bur_transaction GROUP by invoiceNumber HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.isDelete=1 and a.isSynced=1 and a.gstin=? and a.returnType=? and a.monthYear=?";
	    
		int count=0;
	    
	    OPERATION operation =OPERATION.INSERT;
	    
	  try(  Connection connection=	DBUtils.getConnection();
				CallableStatement itemCS = connection.prepareCall(itemInsertSql);
	           	CallableStatement b2burCS =connection.prepareCall(sql);//connection.prepareStatement(insertQueryB2bDetails);
				PreparedStatement existPS=connection.prepareStatement(checkExistSQL);
				PreparedStatement b2burDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
				PreparedStatement b2burUpdateFlag=connection.prepareStatement(updateDelFlag);
			  
		){
		 connection.setAutoCommit(false); 
		 
		 if ("YES".equalsIgnoreCase(delete)) {
			 try(Connection connectionB2burDel=DBUtils.getConnection();
			PreparedStatement b2burDelExist=connectionB2burDel.prepareStatement(b2burExistForDelete);){
				 connectionB2burDel.setAutoCommit(false);
				// for marking delete flag
				b2burDelExist.setString(1, b2bUrTransactionEntities.get(0).getGstin().getId().toString());
				b2burDelExist.setString(2, b2bUrTransactionEntities.get(0).getReturnType());
				b2burDelExist.setString(3, b2bUrTransactionEntities.get(0).getMonthYear());

				ResultSet deleteFlagResult = b2burDelExist.executeQuery();

				while (deleteFlagResult.next()) {
					if (deleteFlagResult.getBoolean(1)) {
						b2burUpdateFlag.setString(1, deleteFlagResult.getString(2));
						 b2burUpdateFlag.executeUpdate();
					} else {
						b2burDelNotSynced.setString(1, deleteFlagResult.getString(2));
						b2burDelNotSynced.executeUpdate();
					}
				}
				
				connectionB2burDel.commit();
				DBUtils.closeQuietly(deleteFlagResult);
				DBUtils.closeQuietly(connectionB2burDel);
			 }
			}

		for(B2bUrTransactionEntity b2bur:b2bUrTransactionEntities)
		{
			
			String invoiceId=UUID.randomUUID().toString();
		if(!"YES".equalsIgnoreCase(delete)){
			existPS.setInt(1,b2bur.getGstin().getId());
			existPS.setString(2,b2bur.getMonthYear());
			existPS.setString(3,b2bur.getInvoiceNumber());
			existPS.setString(4,b2bur.getReturnType());
			
			ResultSet impgRS=existPS.executeQuery();
			
		 	
		   if(impgRS.next() && !impgRS.getBoolean(2)){
			   invoiceId=impgRS.getString(1);
			   operation=OPERATION.UPDATE;
		   }
			DBUtils.closeQuietly(impgRS);
		 }
			
		b2burCS.setString(1,invoiceId);
		b2burCS.setInt(2,b2bur.getGstin().getId());
		b2burCS.setString(3,b2bur.getReturnType());
		b2burCS.setString(4,b2bur.getPos());
		b2burCS.setString(5,b2bur.getStateName());
		b2burCS.setString(6,b2bur.getInvoiceNumber());
		b2burCS.setDate(7,new Date(b2bur.getInvoiceDate().getTime()));
		b2burCS.setDouble(8,b2bur.getTaxableValue());
		b2burCS.setDouble(9,b2bur.getTaxAmount());
		b2burCS.setString(10,b2bur.getSource());
		b2burCS.setString(11,b2bur.getSourceId());
		b2burCS.setString(12,b2bur.getCreatedBy());
		b2burCS.setTimestamp(13,b2bur.getCreationTime());
		b2burCS.setString(14,b2bur.getCreationIPAddress());
		b2burCS.setTimestamp(15,b2bur.getUpdationTime());
		b2burCS.setString(16,b2bur.getUpdatedBy());
		b2burCS.setString(17,b2bur.getUpdationIPAddress());
		b2burCS.setString(18,operation.toString());
		b2burCS.setString(19,b2bur.getSupplyType().toString());
		b2burCS.setString(20,b2bur.getMonthYear());
		
		b2burCS.addBatch();
			
			this.insertItems(b2bur.getItems(), itemCS,operation, invoiceId);

			if(++count%100==0) {
				updateCount+=b2burCS.executeBatch().length;
			itemCS.executeBatch();
			}

			   operation=OPERATION.INSERT;

		}
		updateCount+=b2burCS.executeBatch().length;
		itemCS.executeBatch();
		 connection.commit();
         DBUtils.closeQuietly(connection);
         
		if("YES".equalsIgnoreCase(delete)){
			try(Connection delConn=DBUtils.getConnection();
				PreparedStatement b2burDelDuplicate=delConn.prepareStatement(b2burDeleteDuplicate);
					){
				delConn.setAutoCommit(false);
			
			b2burDelDuplicate.setString(1,b2bUrTransactionEntities.get(0).getGstin().getId().toString());
			b2burDelDuplicate.setString(2, b2bUrTransactionEntities.get(0).getReturnType());
			b2burDelDuplicate.setString(3, b2bUrTransactionEntities.get(0).getMonthYear());
			b2burDelDuplicate.executeUpdate();
			
			delConn.commit();
			DBUtils.closeQuietly(delConn);
	    }
         
  		_Logger.info("End Inserting B2BUR records for gstin {} records {} ",b2bUrTransactionEntities.get(0).getGstin().getGstin(),updateCount);
	  }
	  }
	  catch(SQLException exception)
	  {		  updateCount=0;

	  		_Logger.error("excepton while inserting B2BUR records for gstin {}",b2bUrTransactionEntities.get(0).getGstin().getGstin(),exception);
		  throw exception;
	  }
		}
	  return updateCount;
	}

}
