package com.ginni.easemygst.portal.transaction.factory;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.ReturnsService.DeleteType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.ExportType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction.B2BInvType;
import com.ginni.easemygst.portal.transaction.impl.B2CLTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2CSTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNURTransaction.CDNURInvType;
import com.ginni.easemygst.portal.transaction.impl.EXPTransaction;
import com.ginni.easemygst.portal.transaction.impl.IMPGTransaction.SEZ;

public abstract class TransactionUtil {
	protected final NumberFormat formatter = new DecimalFormat("#0.00");

	protected final int TALLY_EXCEL_START_ROW_NO = 4;
	
	protected final int B2B_XL_COL_COUNT = 21;
	protected final int CDN_XL_COL_COUNT = 22;
	protected final int B2CL_XL_COL_COUNT = 15;
	protected final int B2CS_XL_COL_COUNT = 17;
	protected final int AT_XL_COL_COUNT = 16;
	protected final int TXPD_XL_COL_COUNT = 16;
	protected final int CDNUR_XL_COL_COUNT = 19;
	protected final int EXP_XL_COL_COUNT = 16;

	
	protected final Logger _Logger = LoggerFactory.getLogger(this.getClass());

	public Map<Integer,String>docIssueMap=new HashMap<>();
	
	Date LOWEST_GST_DATE=null;
	
	public String getStringValue(Object object) {
		
		if (Objects.isNull(object))
			return "";
		else if (object.getClass() == Date.class || object.getClass() == java.sql.Date.class||object.getClass()==Timestamp.class)
			return Transaction.sdf.format(object);
		else if(object.getClass()==Double.class)
			return formatter.format(object);
		else
			return String.valueOf(object);

	}
	
	public void handleAmmendmentCase(List<String> temp,TransactionType trans){
		int diff=0;
		switch(trans){
		case B2B:
			if(temp.size()<B2B_XL_COL_COUNT){
				diff=B2B_XL_COL_COUNT-temp.size();
			}
			break;
			
		case CDN:
			if(temp.size()<CDN_XL_COL_COUNT){
				diff=CDN_XL_COL_COUNT-temp.size();
			}
			break;
			
		case B2CL:
			if(temp.size()<B2CL_XL_COL_COUNT){
				diff=B2CL_XL_COL_COUNT-temp.size();
			}
			break;
		case B2CS:
			if(temp.size()<B2CS_XL_COL_COUNT){
				diff=B2CS_XL_COL_COUNT-temp.size();
			}
			break;
		case EXP:
			if(temp.size()<EXP_XL_COL_COUNT){
				diff=EXP_XL_COL_COUNT-temp.size();
			}
			break;
			
		case TXPD:
			if(temp.size()<TXPD_XL_COL_COUNT){
				diff=TXPD_XL_COL_COUNT-temp.size();
			}
			break;
			
		case AT:
			if(temp.size()<AT_XL_COL_COUNT){
				diff=AT_XL_COL_COUNT-temp.size();
			}
			break;
			
		case CDNUR:
			if(temp.size()<CDNUR_XL_COL_COUNT){
				diff=CDNUR_XL_COL_COUNT-temp.size();
			}
			break;
			
		}
		
		
		for(int i=0;i<diff;i++){
			temp.add("");
		}
	}
	public boolean getTallyColumCountCondition(SourceType source,int rowNum){
		return source ==SourceType.TALLY && rowNum<TALLY_EXCEL_START_ROW_NO;
	}
	
	public TransactionUtil() {

		docIssueMap.put(1, "Invoices for Outward Supply".toUpperCase());
		docIssueMap.put(2, "Invoices for Inward Supply from Unregistered Person".toUpperCase());
		docIssueMap.put(3, "Revised Invoice".toUpperCase());
		docIssueMap.put(4, "Debit Note".toUpperCase());
		docIssueMap.put(5, "Credit Note".toUpperCase());
		docIssueMap.put(6, "Receipt Voucher".toUpperCase());
		docIssueMap.put(7, "Payment Voucher".toUpperCase());
		docIssueMap.put(8, "Refund Voucher".toUpperCase());
		docIssueMap.put(9, "Delivery Challan for job work".toUpperCase());
		docIssueMap.put(10, "Delivery Challan for supply on approval".toUpperCase());
		docIssueMap.put(11, "Delivery Challan in case of liquid gas".toUpperCase());
		docIssueMap.put(12, "Delivery Challan in cases other than by way of supply".toUpperCase());
		
		try {
			String date = "01/07/2017";
//			if ("local".equalsIgnoreCase(AppConfig.getActiveProfile()))
//				date = "01/01/2015";
			LOWEST_GST_DATE = new SimpleDateFormat("dd/MM/yyyy").parse("01/07/2017");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

	public EmailDTO getEmailContent(EmailDTO emailDTO, Object primaryInvoice, Object secondaryInvocie)
			throws AppException {
		return null;
	}

	public String processErrorMsg(String errorMsg ){
		String formattedMsg="";
		if(StringUtils.isEmpty(errorMsg))
			return "";
		else
			return formattedMsg=errorMsg.replace("|", "\n");
	}
	public void deleteTransaction(String gstn, String monthYear, TransactionType transaction, ReturnType returnType,
			String delete, DataSource dataSource) throws Exception {

		if (StringUtils.isNotEmpty(delete) && delete.equalsIgnoreCase("yes"))
			try {
				ReturnsService returnService = (ReturnsService) InitialContext
						.doLookup("java:global/easemygst/ReturnsServiceImpl");
				returnService.deleteTransactionData(gstn, monthYear, transaction, returnType, null, DeleteType.ALL,
						dataSource);
			} catch (NamingException e) {
				_Logger.error("error", e);
				throw new AppException(ExceptionCode._ERROR, "Error while deleting data.");
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public Double getToleranceValue(Double taxableValue) {
		
		Double defaultValue = 50.0;

		Double toleranceTaxableValue = taxableValue * 0.00005;
		
		toleranceTaxableValue = 0.0;

		return toleranceTaxableValue > defaultValue ? toleranceTaxableValue : defaultValue;
	}

	public void checkIgstTaxRates(String value) throws NumberFormatException {

		try {
			Double temp = Double.parseDouble(value);
			if (!(temp == 0.0 || temp == 5.0 || temp == 12.0 || temp == 18.0 || temp == 28.0 || temp == 3.0
					|| temp == 0.25||temp==0.1))
				throw new NumberFormatException();
		} catch (NumberFormatException e) {
			throw new NumberFormatException();
		}

	}

	public void checkCSgstTaxRates(String value) throws NumberFormatException {

		try {
			Double temp = Double.parseDouble(value);
			if (!(temp == 0.0 || temp == 2.5 || temp == 6.0 || temp == 9.0 || temp == 14.0))
				throw new NumberFormatException();
		} catch (NumberFormatException e) {
			throw new NumberFormatException();
		}

	}

	public void checkCessTaxRates(String value) throws NumberFormatException {
		/*
		 * try { Double temp = Double.parseDouble(value); if(!(temp==0.0 ||
		 * temp==5.0 || temp==12.0 || temp==18.0 || temp==28.0)) throw new
		 * NumberFormatException(); } catch(NumberFormatException e) { throw new
		 * NumberFormatException(); }
		 */
	}

	public ExcelError validateGstinOrName(TransactionType transaction, int _GSTIN_INDEX, int index, String gstinOrName,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (!StringUtils.isEmpty(gstinOrName)) {
			boolean isGstin = false;
			gstinOrName = gstinOrName.trim();
			if (Character.isDigit(gstinOrName.charAt(0)))
				isGstin = true;
			if (isGstin) {
				try {

					if (!StringUtils.isEmpty(gstinOrName) && gstinOrName.length() == 15) {
						String ctin = gstinOrName;
						String st = ctin.substring(0, 2);
						int scode = Integer.parseInt(st);
						if (!(scode > 0 && scode <= 37))
							throw new NumberFormatException();

						// posValue=st;
					} else {
						isError = true;
						error.append(transaction.toString() + "," + (_GSTIN_INDEX + 1) + "," + index + ","
								+ ee.getUniqueRowValue() + " Value - [" + gstinOrName
								+ "] - Invalid GSTIN value it should be exactly 15 character and should contain valid state code|");
					}
				} catch (NumberFormatException e) {
					isError = true;

					error.append(transaction.toString() + "," + (_GSTIN_INDEX + 1) + "," + index + ","
							+ ee.getUniqueRowValue() + " Value - [" + gstinOrName
							+ "] - Invalid GSTIN value it should be exactly 15 character and should contain valid state code|");
				}
			} else {
				if (gstinOrName.length() > 50) {
					isError = true;

					error.append(transaction.toString() + "," + (_GSTIN_INDEX + 1) + "," + index + ","
							+ ee.getUniqueRowValue() + " Value - [" + gstinOrName
							+ "] - Invalid counter party name length it should be less than 50 characters|");
				}

			}

		} else {
			isError = true;
			error.append(transaction.toString() + "," + (_GSTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + gstinOrName
					+ "] - Invalid Counter party GSTIN or name value it please enter either Couner party gstin or name|");

		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateInvoiceType(TransactionType transaction, int _INVOICE_TYPE_INDEX, int index,
			String gstinOrName, String invoiceType, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		boolean isGstin = false;
		if (!StringUtils.isEmpty(gstinOrName)) {

			gstinOrName = gstinOrName.trim();
			if (Character.isDigit(gstinOrName.charAt(0)))
				isGstin = true;

		}

		if (isGstin && "B2C".equalsIgnoreCase(invoiceType) || (!isGstin && "B2B".equalsIgnoreCase(invoiceType))) {
			isError = true;
			error.append(transaction.toString() + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + ","
					+ ee.getUniqueRowValue() + " Value - [" + invoiceType
					+ "] - Invalid invoice type value it should be B2B If TIN is provided Or B2C if TIN name is provided|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateDate(String sheetName, int _INVOICE_DATE_INDEX, int index, String invDate,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			ee.setDate(Transaction.sdf.parse(invDate));

		} catch (ParseException e1) {
			try {
				ee.setDate(Transaction.sdfGcc.parse(invDate));
			} catch (ParseException e) {
				error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + invDate + "] - Unable to parse date use format dd/mm/yyyy|");
				isError = true;
			}
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;
	}

	protected void validateSEZ(String sheetName, int SEZ_INDEX, int index, String sezValue, ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			SEZ.valueOf(sezValue);
		} catch (Exception e) {
			isError = true;
			error.append(sheetName + "," + (SEZ_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ sezValue + "] - Invalid sez value it should be Y or N " + "|");

		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
	}

	public ExcelError validateOriginalInvoiceDate(String sheetName, int _INVOICE_DATE_INDEX, int index, String invDate,
			ExcelError ee, String monthYear, TransactionType transactionType,boolean isMandatory) {
		SimpleDateFormat invSdf=new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date invoiceDate = null;
		try {
			if(isMandatory)
			invoiceDate = invSdf.parse(invDate);

		} catch (ParseException exception) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invDate + "] - Unable to parse date use format dd/mm/yyyy|");
			isError = true;
			

			return ee;
		}
		ee.setOriginalInvoiceDate(invoiceDate);;
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	/*
	 public ExcelError validateOriginalInvoiceDate(String sheetName, int _INVOICE_DATE_INDEX, int index, String invDate,
			ExcelError ee, String monthYear, TransactionType transactionType,String orgInvoiceDate,String originalMonthYear) {
		SimpleDateFormat invSdf=new SimpleDateFormat("dd/MM/yyyy");
		DateTimeFormatter yearMonthFormatter=DateTimeFormatter.ofPattern("MMyyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date invoiceDate = null;
		boolean isOrginalMonthYear=StringUtils.isEmpty(orgInvoiceDate)?true:false;
		try {
			if(!isOrginalMonthYear){
				Date invoiceDateD=invSdf.parse(invDate);
				Date originalInvoiceDateD=invSdf.parse(orgInvoiceDate);
				if(StringUtils.isNoneEmpty(orgInvoiceDate)){
					if(invoiceDate.compareTo(originalInvoiceDateD)<=1){
						//original invoice date must be less than from  invoice date
						
					}
					
				}
			}else{
				YearMonth currentMonthYear=YearMonth.parse(monthYear,yearMonthFormatter);
				YearMonth orginalYearMonth=YearMonth.parse(originalMonthYear,yearMonthFormatter);
				if(currentMonthYear.compareTo(orginalYearMonth)<=1){
					
				}
			}
			if(!StringUtils.isEmpty(invDate))
			invoiceDate = invSdf.parse(invDate);

		} catch (ParseException exception) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invDate + "] - Unable to parse date use format dd/mm/yyyy|");
			isError = true;
			ee.setDate(invoiceDate);
			ee.setFinalError(ee.isFinalError() || isError);
			ee.setError(isError);
			ee.setErrorDesc(error);

			return ee;
		}
		
		return ee;
	}
	 
	 */
	
	
	public ExcelError validateInvoiceDate(String sheetName, int _INVOICE_DATE_INDEX, int index, String invDate,
			ExcelError ee, String monthYear, TransactionType transactionType) {
		
		SimpleDateFormat invSdf=new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar cal = null;
		if (transactionType == TransactionType.CDN || transactionType == TransactionType.CDNUR) {
			// LOWEST_GST_DATE=new SimpleDateFormat("dd/mm/yyyy").parse()
			cal = Calendar.getInstance();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				cal.set(Calendar.MONTH,inpMonth);
				cal.set(Calendar.YEAR,inpYear);
				cal.set(Calendar.DAY_OF_MONTH,1);
			}
			
			cal.add(Calendar.YEAR, -4);
			LOWEST_GST_DATE = cal.getTime();
		}

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date invoiceDate = null;
		try {
			invoiceDate = invSdf.parse(invDate);

		} catch (ParseException exception) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invDate + "] - Unable to parse invoice date use format dd/mm/yyyy|");
			isError = true;
			ee.setDate(invoiceDate);
			ee.setFinalError(ee.isFinalError() || isError);
			ee.setError(isError);
			ee.setErrorDesc(error);

			return ee;
		}

		Date currentDate = new Date();
		int inpMonth = 0;
		int inpYear = 0;
		if (LOWEST_GST_DATE.compareTo(invoiceDate) > 0) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invDate + "] - Invalid invoice date it can not be less than "
					+ invSdf.format(LOWEST_GST_DATE) + " use format  dd/mm/yyyy|");
			isError = true;
		} else if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
			inpMonth = Integer.parseInt(monthYear.substring(0, 2));
			inpYear = Integer.parseInt(monthYear.substring(2, 6));
			Calendar cal1 = Calendar.getInstance();
			/*if (inpMonth == 12)
				inpMonth = 1;*/
			cal1.set(Calendar.MONTH, inpMonth);
			cal1.set(Calendar.YEAR, inpYear);
			cal1.set(Calendar.DATE, 1);
			cal1.add(Calendar.DATE, -1);
			currentDate = cal1.getTime();
			if (currentDate.compareTo(cal1.getTime()) > 0)
				currentDate = cal1.getTime();

			if (transactionType == TransactionType.CDN || transactionType == TransactionType.CDNUR) {
				// LOWEST_GST_DATE=new SimpleDateFormat("dd/mm/yyyy").parse()
				/*cal = cal1;
				cal.add(Calendar.MONTH, -18);
				LOWEST_GST_DATE = cal.getTime();*/
			}
			
			if (currentDate.compareTo(invoiceDate) < 0) {
				_Logger.info("current date {}  invoice date {}",invSdf.format(currentDate),invSdf.format(invoiceDate));

				error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + invDate
						+ "] - Invalid invoice date it can not be future date use format  dd/mm/yyyy|");
				isError = true;
			}
			else if (LOWEST_GST_DATE.compareTo(invoiceDate) > 0) {
				
				error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + invDate + "] - Invalid invoice date it can not be less than "
						+ Transaction.sdf.format(LOWEST_GST_DATE) + " use format  dd/mm/yyyy|");
				isError = true;
			} 
		}
		ee.setDate(invoiceDate);

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;
	}

	public ExcelError validateNoteDate(String sheetName, int _NOTE_DATE_INDEX, int index, String ntDate, String invDate,
			ExcelError ee, String monthYear) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date invoiceDate;
		Date noteDate;

		SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy");
		
		SimpleDateFormat sdfGcc = new SimpleDateFormat("MM/dd/yy");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");



		Date currentDate = new Date();
		int inpMonth = 0;
		int inpYear = 0;
		if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
			inpMonth = Integer.parseInt(monthYear.substring(0, 2));
			inpYear = Integer.parseInt(monthYear.substring(2, 6));
			Calendar cal1 = Calendar.getInstance();
			/*if (inpMonth == 12)
				inpMonth = 0;*/
			cal1.set(Calendar.MONTH, inpMonth);
			cal1.set(Calendar.YEAR, inpYear);
			cal1.set(Calendar.DATE, 1);
			cal1.add(Calendar.DATE, -1);
			if (currentDate.compareTo(cal1.getTime()) > 0)
				currentDate = cal1.getTime();
		}
		
	
		try {
			invoiceDate = sdf.parse(invDate);
			noteDate = sdf.parse(ntDate);
			

			if (noteDate.compareTo(invoiceDate) < 0 || noteDate.compareTo(currentDate) > 0) {
				error.append(sheetName + "," + (_NOTE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + ntDate
						+ "] - Invalid D/DEBIT or C/CREDIT or R/REFUND date it cannot be prior to invoice date ["+format.format(invoiceDate)+"]  and must be on or before "
						+ format.format(currentDate) + "|");
				isError = true;
			}
			ee.setDate(sdf.parse(ntDate));

		} catch (ParseException e1) {
			try {

				invoiceDate = sdfGcc.parse(invDate);
				noteDate = sdfGcc.parse(ntDate);
				if (noteDate.compareTo(invoiceDate) < 0 || noteDate.compareTo(currentDate) > 0) {
					error.append(sheetName + "," + (_NOTE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + ntDate
							+ "] - Invalid D/DEBIT or C/CREDIT or R/REFUND date it cannot be prior to invoice date and must be on or before "
							+ format.format(currentDate) + "|");
					isError = true;
				}
				ee.setDate(sdfGcc.parse(ntDate));
			} catch (ParseException e) {
				error.append(sheetName + "," + (_NOTE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + ntDate + "] - Unable to parse note date use format dd/mm/yyyy|");
				isError = true;
			}
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;
	}

	public ExcelError validateGstin(String sheetName, int _GSTIN_INDEX, int index, String gstin, ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if (!StringUtils.isEmpty(gstin) && gstin.length() == 15) {
				String ctin = gstin;

					String st = ctin.substring(0, 2);
					int scode = Integer.parseInt(st);
					if (!((scode > 0 && scode <= 37 && scode!=28) || scode==97))
						throw new NumberFormatException();

					final Pattern pattern = Pattern
							.compile("[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Zz1-9A-Ja-j]{1}[0-9a-zA-Z]{1}");
					Pattern pattern2=Pattern.compile("[0-9]{4}[A-Z]{3}[0-9]{5}[UO]{1}[N][A-Z0-9]{1}");
					
					if (!(pattern.matcher(ctin).matches()||pattern2.matcher(ctin).matches() )) {
						isError = true;
						error.append(sheetName + "," + (_GSTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
								+ " Value - [" + gstin
								+ "] - Invalid GSTIN value it should be like first 2 characters should be numeric "
								+ "after that next 5 characters should be alphabets than next 4 "
								+ "characters should be numeric than one alphabet than any alpahnumeric character "
								+ " 14th character should be Z respectively|");
					}
				
			} else {
				isError = true;
				error.append(sheetName + "," + (_GSTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + gstin
						+ "] - Invalid GSTIN/UIN value it should be exactly 15 character and should contain valid state code|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_GSTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + gstin
					+ "] - Invalid GSTIN/UIN value it should be exactly 15 character and should contain valid state code|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateGstinExp(String sheetName, int _GSTIN_INDEX, int index, String gstin, String expType,
			ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		// if (ExportType.EXP.toString().equalsIgnoreCase(expType) &&
		// StringUtils.isEmpty(gstin)) {
		// isError = true;
		// error.append(sheetName + "," + (_GSTIN_INDEX + 1) + "," + index
		// + ",Remove GSTIN as this is not applicable in case of export
		// type-EXP|");
		// } else {
		try {
			if (!StringUtils.isEmpty(gstin) && gstin.length() == 15) {
				String ctin = gstin;
				String st = ctin.substring(0, 2);
				int scode = Integer.parseInt(st);
				if (!(scode > 0 && scode <= 37))
					throw new NumberFormatException();

			} else {
				isError = true;
				error.append(sheetName + "," + (_GSTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + gstin
						+ "] - Invalid GSTIN value it should be exactly 15 character and should contain valid state code|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_GSTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + gstin
					+ "] - Invalid GSTIN value it should be exactly 15 character and should contain valid state code|");
		}
		// }
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateReverseCharge(String sheetName, int _IS_REVERSE_INDEX, int index, String reverseCharge,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		ee.setError(isError);
		ee.setErrorDesc(error);

		if (reverseCharge != null && reverseCharge.equalsIgnoreCase("Y") || reverseCharge.equalsIgnoreCase("YES")) {
			isError = false;
			ee.setReverseCharge(true);

		} else if (reverseCharge != null && reverseCharge.equalsIgnoreCase("N")
				|| reverseCharge.equalsIgnoreCase("NO")) {
			isError = false;
			ee.setReverseCharge(false);
		} else {
			isError = true;
			error.append(sheetName + "," + (_IS_REVERSE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + reverseCharge + "] - Invalid reverse charge use either Y/YES or N/NO|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	protected ExcelError validateStateCode(String sheetName, int _PLACE_OF_SUPPLY_INDEX, int index,
			String placeOfSupply, ExcelError ee) {

		String posValue = "";

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		StateDTO stateDTO = AppConfig.getStateNameByStateCode(placeOfSupply);
		if (stateDTO != null) {
			int pos = Integer.parseInt(stateDTO.getScode());
			if (pos < 10)
				posValue = "0" + pos;
			else
				posValue = String.valueOf(pos);
			ee.setPosValue(pos);
			ee.setPos(posValue);

		} else {
			isError = true;
			error.append(sheetName + "," + (_PLACE_OF_SUPPLY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + placeOfSupply + "] - Invalid place of supply it must be valid state code|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	protected ExcelError validateNatureOfDocument(String sheetName, int _NATURE_OF_DOC_INDEX, int index,
			String natureOfDoc, ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		List<Integer>docTypeCodes=new ArrayList<>(docIssueMap.keySet());
		List<String>docTypeValues=new ArrayList<>(docIssueMap.values());
		boolean isValid=false;
		
		
		try{
			if(docIssueMap.containsValue(StringUtils.upperCase(natureOfDoc))){
				isValid=true;
			}
			if(!isValid){
			int code=Integer.parseInt(natureOfDoc);
			if(!docIssueMap.containsKey(code)){
				isError = true;
				error.append(sheetName + "," + (_NATURE_OF_DOC_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + natureOfDoc + "] - Invalid Nature of document value use either "+StringUtils.join(docTypeCodes, "/")+" or "+StringUtils.join(docTypeValues, "/")+"  |");
		
				/*error.append(sheetName + "," + (_NATURE_OF_DOC_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + natureOfDoc + "] - Invalid Nature of document value use either "+StringUtils.join(docTypeCodes, "/")+"  |");
*/	
			}
			}
		}
		catch(NumberFormatException e){
			
			isError = true;
			error.append(sheetName + "," + (_NATURE_OF_DOC_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + natureOfDoc + "] - Invalid Nature of document value use either "+StringUtils.join(docTypeCodes, "/")+" or "+StringUtils.join(docTypeValues, "/")+"  |");
			
		/*	error.append(sheetName + "," + (_NATURE_OF_DOC_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + natureOfDoc + "] - Invalid Nature of document value use either "+StringUtils.join(docTypeCodes, "/")+"  |");
*/	
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	
	public ExcelError validatePos(String sheetName, int _PLACE_OF_SUPPLY_INDEX, int index, String placeOfSupply,
			String gstinPos, ExcelError ee) {
		String posValue = "";

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		boolean isIgst = false;
		try {
			int pos = Double.valueOf(placeOfSupply).intValue();
			if ((pos < 1 || pos > 37 || pos == 28 )&& pos!=97)
				throw new NumberFormatException();
			if (pos < 10)
				posValue = "0" + pos;
			else
				posValue = pos + "";

			ee.setPosValue(pos);
			ee.setPos(posValue);

			if (!posValue.equalsIgnoreCase(gstinPos))
				isIgst = true;
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_PLACE_OF_SUPPLY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + placeOfSupply
					+ "] - Invalid place of supply it must be integer allowed range 1-37|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		ee.setIgst(isIgst);

		return ee;

	}

	public ExcelError validateStateCode(String sheetName, int _PLACE_OF_SUPPLY_INDEX, int index, String placeOfSupply,
			String gstinPos, ExcelError ee) {
		String posValue = "";

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		boolean isIgst = false;
		try {
			int pos = Double.valueOf(placeOfSupply).intValue();
			if (pos < 1 || pos > 37 || pos==28)
				throw new NumberFormatException();
			if (pos < 10)
				posValue = "0" + pos;
			else
				posValue = pos + "";

			ee.setPosValue(pos);
			ee.setPos(posValue);

			if (posValue.equalsIgnoreCase(gstinPos)) {
				isError = true;
				error.append(sheetName + "," + (_PLACE_OF_SUPPLY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + placeOfSupply
						+ "] - Invalid place of supply it must be different from gstin state code allowed range 1-37|");

			} else {
				isIgst = true;
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_PLACE_OF_SUPPLY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + placeOfSupply
					+ "] - Invalid place of supply it must be integer allowed range 1-37|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		ee.setIgst(isIgst);

		return ee;

	}

	public ExcelError validateInvoiceValue(String sheetName, int _INVOICE_VALUE_INDEX, int index, String invValue,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			Double invoiceValue = new Double(0.0);

			invoiceValue = Double.parseDouble(invValue);
			if (!(invoiceValue > 0.0)) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_INVOICE_VALUE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invValue
					+ "] - Invalid invoice value it should be numeric value and greater than zero only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	public ExcelError validateIgstAmt(String sheetName, int _IGST_AMT_INDEX, int index, String igstAmt, Double taxRate,
			Double taxableValue, ExcelError ee) {
		if (taxRate == null) {
			taxRate = 100.0;
		}
		if (taxableValue == null) {
			taxableValue = 0.0;
		}

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (true) {
			if(!ee.isSewopOrWopay()){
			try {
				if (!StringUtils.isEmpty(igstAmt)) {
					if (!isIgst && Double.parseDouble(igstAmt) != 0.0) {
						isError = true;
						error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + ","
								+ ee.getUniqueRowValue() + " Value - [" + igstAmt
								+ "] - remove IGST amount as this is Intrastate transaction|");
					} else {
						if (Double.parseDouble(igstAmt) < 0.0)
							throw new NumberFormatException();
						if (Double.parseDouble(igstAmt) != 0.0) {
							double igstAmnt = Double.parseDouble(igstAmt);
							double tempAmt = (taxableValue * taxRate) / 100;

							Double tolerance = getToleranceValue(taxableValue);
							if ((Math.abs(tempAmt - igstAmnt) > tolerance)
									&& taxRate != 100.0) {
								isError = true;
								error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + ","
										+ ee.getUniqueRowValue() + " Value - [" + igstAmnt
										+ "] - Incorrect IGST amount - as per mentioned tax rate it should be "
										+ Transaction.decimalFormat.format(tempAmt) + "|");
							}

						}
					}
				} else if (isIgst) {
					isError = true;
					error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + igstAmt
							+ "] - IGST amount is mandatory as this is Interstate transaction|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + igstAmt + "] - Invalid IGST amount use numeric value only|");
			}
		}
		} 

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	public ExcelError validateNoTaxAmt(String sheetName, String expType, int index, int _IGST_AMT_INDEX, String igstAmt,
			int _TAX_RATE_INDEX, String taxRate, ExcelError ee) {

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		double igstAmount = 0.0;
		double taxRateVal = 0.0;

		if (ExportType.WOPAY.toString().equalsIgnoreCase(expType)) {
			try {
				if (!StringUtils.isEmpty(igstAmt) && Double.parseDouble(igstAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + igstAmt
							+ "] - Remove IGST amount as this is not required if export type is WOPAY|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + igstAmt
						+ "] - Input 0 or leave the field blank as this is not required if export type is WOPAY|");
			}
			try {
				if (!StringUtils.isEmpty(taxRate) && Double.parseDouble(taxRate) != 0.0) {

					isError = true;
					error.append(sheetName + "," + (_TAX_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + _TAX_RATE_INDEX
							+ "] - Remove tax rate as this is not required if export type is WOPAY|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_TAX_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + taxRate
						+ "] - Input 0 or leave the field blank as this is not required if export type is WOPAY|");

			}

		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	public ExcelError validateNoTaxAmtB2b(String sheetName, String invType, int index, int _IGST_AMT_INDEX,
			String igstAmt, int _CGST_AMT_INDEX, String cgstAmt, int _SGST_AMT_INDEX, String sgstAmt,
			int _CESS_AMT_INDEX, String cessAmt, int _TAX_RATE_INDEX, String taxRate, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		

		if (B2BInvType.SEWOP.toString().equalsIgnoreCase(invType)) {
			try {
				if (!StringUtils.isEmpty(igstAmt) && Double.parseDouble(igstAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + igstAmt
							+ "] - Remove IGST amount as this is not required if invoice type is SEWOP|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + igstAmt
						+ "] - Input 0 or leave the field blank as this is not required if invoice type is SEWOP|");
			}
			try {
				if (!StringUtils.isEmpty(cgstAmt) && Double.parseDouble(cgstAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + cgstAmt
							+ "] - Remove CGST amount as this is not required if invoice type is SEWOP|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + cgstAmt
						+ "] - Input 0 or leave the field blank as this is not required if invoice type is SEWOP|");
			}
			
			try {
				if (!StringUtils.isEmpty(sgstAmt) && Double.parseDouble(sgstAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + sgstAmt
							+ "] - Remove SGST amount as this is not required if invoice type is SEWOP|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + sgstAmt
						+ "] - Input 0 or leave the field blank as this is not required if invoice type is SEWOP|");
			}
			

			try {
				if (!StringUtils.isEmpty(cessAmt) && Double.parseDouble(cessAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_CESS_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + cessAmt
							+ "] - Remove Cess amount as this is not required if invoice type is SEWOP|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_CESS_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + cessAmt
						+ "] - Input 0 or leave the field blank as this is not required if invoice type is SEWOP|");
			}
			try {
				if (!StringUtils.isEmpty(taxRate) && Double.parseDouble(taxRate) != 0.0) {

					isError = true;
					error.append(sheetName + "," + (_TAX_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + _TAX_RATE_INDEX
							+ "] - Remove tax rate as this is not required if export type is WOPAY|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_TAX_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + taxRate
						+ "] - Input 0 or leave the field blank as this is not required if export type is WOPAY|");

			}
			
			

		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}
	
	/*
	 * public ExcelError validateNoTaxAmt1(String sheetName, String invoiceType,
	 * int index, int _IGST_AMT_INDEX,String igstAmt, int _CGST_AMT_INDEX,String
	 * cgstAmt, int _SGST_AMT_INDEX,String sgstAmt, int _CESS_AMT_INDEX,String
	 * cessAmt, String taxRate, Double taxableValue, ExcelError ee) {
	 * 
	 * 
	 * boolean isIgst = ee.isIgst(); boolean isError = ee.isError();
	 * StringBuilder error = ee.getErrorDesc(); try{
	 * 
	 * }catch(Exception e){
	 * 
	 * }
	 * 
	 * 
	 * 
	 * ee.setFinalError(ee.isFinalError() || isError); ee.setError(isError);
	 * ee.setErrorDesc(error);
	 * 
	 * return ee;
	 * 
	 * }
	 */

	public ExcelError validateIgstAmtB2cs(String sheetName, int _IGST_AMT_INDEX, int index, String igstAmt,
			Double taxRate, Double taxableValue, ExcelError ee) {
		if (taxRate == null) {
			taxRate = 100.0;
		}
		if (taxableValue == null) {
			taxableValue = 0.0;
		}

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			if (!StringUtils.isEmpty(igstAmt)) {
				if (!isIgst && Double.parseDouble(igstAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + igstAmt + "] - remove IGST amount as this is Intrastate transaction|");
				} else {
					if (Double.parseDouble(igstAmt) != 0.0) {
						double igstAmnt = Double.parseDouble(igstAmt);
						double tempAmt = (taxableValue * taxRate) / 100;

						Double tolerance = getToleranceValue(taxableValue);
						if ((Math.abs(tempAmt - igstAmnt) > tolerance)
								&& taxRate != 100.0) {
							isError = true;
							error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + ","
									+ ee.getUniqueRowValue() + " Value - [" + igstAmnt
									+ "] - Incorrect IGST amount - as per mentioned tax rate it should be "
									+ Transaction.decimalFormat.format(tempAmt) + "|");
						}

					}
				}
			} else if (isIgst) {
				isError = true;
				error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + igstAmt + "] - IGST amount is mandatory as this is Interstate transaction|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_IGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + igstAmt + "] - Invalid IGST amount use numeric value only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	public ExcelError validateCgstAmt(String sheetName, int _CGST_AMT_INDEX, int index, String cgstAmt, Double taxRate,
			Double taxableValue, ExcelError ee) {
		if (taxRate == null) {
			taxRate = 100.0;
		}
		if (taxableValue == null) {
			taxableValue = 0.0;
		}

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		if (true) {
			if(!ee.isSewopOrWopay()){
			try {
				if (!StringUtils.isEmpty(cgstAmt)) {
					if (isIgst && Double.parseDouble(cgstAmt) != 0.0) {
						isError = true;
						error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + ","
								+ ee.getUniqueRowValue() + " Value - [" + cgstAmt
								+ "] - remove CGST amount as this is Interstate transaction|");
					} else {
						/*
						 * if (Double.parseDouble(cgstAmt) < 0.0) throw new
						 * NumberFormatException();
						 */
						if (Double.parseDouble(cgstAmt) != 0.0) {
							double tempAmt = (taxableValue * taxRate) / (100 * 2);
							double cgstAmnt = Double.parseDouble(cgstAmt);

							Double tolerance = getToleranceValue(taxableValue);
							if ((Math.abs(tempAmt - cgstAmnt) > tolerance)
									&& taxRate != 100.0) {
								isError = true;
								error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + ","
										+ ee.getUniqueRowValue() + " Value - [" + cgstAmnt
										+ "] - Incorrect CGST amount - as per mentioned tax rate it should be "
										+ Transaction.decimalFormat.format(tempAmt) + "|");
							}
						}
					}
				} else if (!isIgst) {
					isError = true;
					error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + cgstAmt
							+ "] - CGST amount is mandatory as this is Intrastate transaction|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + cgstAmt
						+ "] - Invalid CGST amount use numeric value and greater than 0.0 only|");
			}
		}
		} 

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateCgstAmtB2cs(String sheetName, int _CGST_AMT_INDEX, int index, String cgstAmt,
			Double taxRate, Double taxableValue, ExcelError ee) {
		if (taxRate == null) {
			taxRate = 100.0;
		}
		if (taxableValue == null) {
			taxableValue = 0.0;
		}

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			if (!StringUtils.isEmpty(cgstAmt)) {
				if (isIgst && Double.parseDouble(cgstAmt) != 0.0) {
					isError = true;
					error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + cgstAmt + "] - remove CGST amount as this is Interstate transaction|");
				} else {

					if (Double.parseDouble(cgstAmt) != 0.0) {
						double tempAmt = (taxableValue * taxRate) / (100 * 2);
						double cgstAmnt = Double.parseDouble(cgstAmt);

						Double tolerance = getToleranceValue(taxableValue);
						if ((Math.abs(tempAmt - cgstAmnt) > tolerance)
								&& taxRate != 100.0) {
							isError = true;
							error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + ","
									+ ee.getUniqueRowValue() + " Value - [" + cgstAmnt
									+ "] - Incorrect CGST amount - as per mentioned tax rate it should be "
									+ Transaction.decimalFormat.format(tempAmt) + "|");
						}
					}
				}
			} else if (!isIgst) {
				isError = true;
				error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + cgstAmt + "] - CGST amount is mandatory as this is Intrastate transaction|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_CGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + cgstAmt + "] - Invalid CGST amount use numeric value and greater than 0.0 only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateSgstAmt(String sheetName, int _SGST_AMT_INDEX, int index, String sgstAmt, Double taxRate,
			Double taxableValue, ExcelError ee) {
		if (taxRate == null) {
			taxRate = 100.0;
		}
		if (taxableValue == null) {
			taxableValue = 0.0;
		}

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (true) {
			if(!ee.isSewopOrWopay()){

			try {
				if (!StringUtils.isEmpty(sgstAmt)) {
					if (isIgst && Double.parseDouble(sgstAmt) != 0.0) {
						isError = true;
						error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + ","
								+ ee.getUniqueRowValue() + " Value - [" + sgstAmt
								+ "] - remove SGST amount as this is Interstate transaction|");
					} else {
						/*
						 * if (Double.parseDouble(sgstAmt) < 0.0) throw new
						 * NumberFormatException();
						 */
						if (Double.parseDouble(sgstAmt) != 0.0) {
							double tempAmt = (taxableValue * taxRate) / (100 * 2);
							double sgstAmnt = Double.parseDouble(sgstAmt);

							Double tolerance = getToleranceValue(taxableValue);
							if ((Math.abs(tempAmt - sgstAmnt )>tolerance)
									&& taxRate != 100.0) {
								isError = true;
								error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + ","
										+ ee.getUniqueRowValue() + " Value - [" + sgstAmnt
										+ "] - Incorrect SGST amount - as per mentioned tax rate it should be "
										+ Transaction.decimalFormat.format(tempAmt) + "|");
							}
						}
					}
				} else if (!isIgst) {
					isError = true;
					error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + sgstAmt
							+ "] - SGST amount is mandatory as this is Intrastate transaction|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + sgstAmt
						+ "] - Invalid SGST amount use numeric value and greater than 0.0 only|");
			}
			}
		} 

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateSgstAmtB2cs(String sheetName, int _SGST_AMT_INDEX, int index, String sgstAmt,
			Double taxRate, Double taxableValue, ExcelError ee) {
		if (taxRate == null) {
			taxRate = 100.0;
		}
		if (taxableValue == null) {
			taxableValue = 0.0;
		}

		boolean isIgst = ee.isIgst();
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (ee.isNoValidationCheckForGinesys()) {
			try {
				if (!StringUtils.isEmpty(sgstAmt)) {
					if (isIgst && Double.parseDouble(sgstAmt) != 0.0) {
						isError = true;
						error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + ","
								+ ee.getUniqueRowValue() + " Value - [" + sgstAmt
								+ "] - remove SGST amount as this is Interstate transaction|");
					} else {

						if (Double.parseDouble(sgstAmt) != 0.0) {
							double tempAmt = (taxableValue * taxRate) / (100 * 2);
							double sgstAmnt = Double.parseDouble(sgstAmt);

							Double tolerance = getToleranceValue(taxableValue);
							if ((Math.abs(tempAmt - sgstAmnt) > tolerance)
									&& taxRate != 100.0) {
								isError = true;
								error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + ","
										+ ee.getUniqueRowValue() + " Value - [" + sgstAmnt
										+ "] - Incorrect SGST amount - as per mentioned tax rate it should be "
										+ Transaction.decimalFormat.format(tempAmt) + "|");
							}
						}
					}
				} else if (!isIgst) {
					isError = true;
					error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + sgstAmt
							+ "] - SGST amount is mandatory as this is Intrastate transaction|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + sgstAmt
						+ "] - Invalid SGST amount use numeric value and greater than 0.0 only|");
			}
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateEtin(String sheetName, int _ETIN_INDEX, int index, String etin, ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if (!StringUtils.isEmpty(etin)) {
				if (etin.length() != 15)
					throw new NumberFormatException();
				/*if(!(etin.charAt(13)=='C'||etin.charAt(13)=='c'))
					throw new NumberFormatException();*/
				String ctin = etin;
				String st = ctin.substring(0, 2);
				int scode = Integer.parseInt(st);
				if (!(scode > 0 && scode <= 33))
					throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_ETIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ etin
					+ "] - Invalid ETIN value it should be exactly 15 character and should contain valid state code and second last character must be 'C'|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateCessRate(String sheetName, int _CEES_RATE_INDEX, int index, String cessRate,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if (!StringUtils.isEmpty(cessRate)) {
				checkCessTaxRates(cessRate);
				Double.parseDouble(cessRate);
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_CEES_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + cessRate
					+ "] - Invalid CESS rate use numeric value only allowed values 0.0/2.5/6.0/9.0/14.0|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateCessAmount(String sheetName, int _CEES_AMT_INDEX, int index, String cessAmount,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (true) {
			if(!ee.isSewopOrWopay()){

			try {
				if (!StringUtils.isEmpty(cessAmount)) {
					if (Double.parseDouble(cessAmount) < 0.0)
						throw new NumberFormatException();
					Double.parseDouble(cessAmount);
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_CEES_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + cessAmount
						+ "] - Invalid cess amount use numeric value and greater than 0.0 only|");
			}
			}
		} 

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateCessAmountB2cs(String sheetName, int _CEES_AMT_INDEX, int index, String cessAmount,
			ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if (!StringUtils.isEmpty(cessAmount)) {
				Double.parseDouble(cessAmount);
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(
					sheetName + "," + (_CEES_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
							+ cessAmount + "] - Invalid cess amount use numeric value and greater than 0.0 only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	private static final List<String> rsns = Arrays.asList(new String[] { "01-Sales Return", "02-Post Sale Discount",
			"03-Deficiency in services", "04-Correction in Invoice", "05-Change in POS",
			"06-Finalization of Provisional assessment", "07-Others"

	});

	public String validateCdnRsn(String sheetName, int _REASON_FOR_NOTE_INDEX, int index, String rsn,
			ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		
		if(StringUtils.isNotEmpty(rsn))
		for(String reason: rsns){
			if(reason.toUpperCase().contains(rsn.toUpperCase()))
				return reason;
			
		}
		isError = true;
		error.append(sheetName + "," + (_REASON_FOR_NOTE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + rsn + "] - Invalid Reason value use either " + StringUtils.join(rsns, "/") + "|");
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}

	public ExcelError validateItcEligibility(String sheetName, int _ELIGIBILITY_TAX_INDEX, int index,
			String itcEligibility, ExcelError ee, TransactionType tranType) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		itcEligibility = StringUtils.lowerCase(itcEligibility);
		List<String> valids;
		List<String> fullNames;
		if (TransactionType.IMPG == tranType) {
			valids = Arrays.asList("no", "ip", "cp");
			fullNames = Arrays.asList("none", "input", "capital");

		} else if (TransactionType.IMPS == tranType) {
			valids = Arrays.asList("no", "is","ip");
			fullNames = Arrays.asList("none", "input-services");
		} else {
			valids = Arrays.asList("no", "ip", "cp", "is");
			fullNames = Arrays.asList("none", "input", "capital", "input-services");
		}

		if (TransactionType.IMPS == tranType || TransactionType.B2B == tranType || TransactionType.B2BUR == tranType) {
			try {
				if (!valids.contains(StringUtils.lowerCase(itcEligibility)))
					throw new NumberFormatException();
				int posCode = Integer.parseInt(ee.getGstinPosImps());
				int gstinPosCode = Integer.parseInt(ee.getPosImps());
				if (posCode == gstinPosCode && (StringUtils.isEmpty(itcEligibility))) {

					isError = true;
					error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + ","
							+ ee.getUniqueRowValue() + " Value - [" + itcEligibility
							+ "] - Invalid ITC eligbility value use either " + StringUtils.join(valids, "/") + " for "
							+ StringUtils.join(fullNames, "/") + "  as itc is applicable in this transaction |");

				} else if (posCode != gstinPosCode && ("is".equalsIgnoreCase(itcEligibility)
						|| "ip".equalsIgnoreCase(itcEligibility) || "cp".equalsIgnoreCase(itcEligibility))) {
					isError = true;
					error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + ","
							+ ee.getUniqueRowValue() + " Value - [" + itcEligibility
							+ "] - Invalid ITC eligbility value set eligibility as no for none as itc is not applicable |");

				}

			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + itcEligibility + "] - Invalid ITC eligbility value use either "
						+ StringUtils.join(valids, "/") + " for " + StringUtils.join(fullNames, "/") + " |");

			}

			ee.setFinalError(ee.isFinalError() || isError);
			ee.setError(isError);
			ee.setErrorDesc(error);
			return ee;
		} else if (TransactionType.CDN == tranType || TransactionType.CDNUR == tranType) {
			try {
				if (!valids.contains(StringUtils.lowerCase(itcEligibility)))
					throw new NumberFormatException();
				String noteType = ee.getNoteType();
				//String preGstRegim = ee.getPreGstRegime();

				if (!(("C".equalsIgnoreCase(noteType) || "D".equalsIgnoreCase(noteType)))
						&& ("is".equalsIgnoreCase(itcEligibility) || "ip".equalsIgnoreCase(itcEligibility)
								|| "cp".equalsIgnoreCase(itcEligibility))) {
					isError = true;
					error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + ","
							+ ee.getUniqueRowValue() + " Value - [" + itcEligibility
							+ "] - Invalid ITC eligbility value set eligibility as no for none as itc is not applicable |");

				}

			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + itcEligibility + "] - Invalid ITC eligbility value use either "
						+ StringUtils.join(valids, "/") + " for " + StringUtils.join(fullNames, "/") + " |");

			}

			ee.setFinalError(ee.isFinalError() || isError);
			ee.setError(isError);
			ee.setErrorDesc(error);
			return ee;
		}

		else {

			try {
				if (!StringUtils.isEmpty(itcEligibility)) {

					if (valids.contains(StringUtils.lowerCase(itcEligibility))) {

					} else {
						throw new NumberFormatException();

					}
				} else {
					isError = true;
					error.append(
							sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
									+ " Value - [" + itcEligibility + "] - Invalid ITC eligbility value use either "
									+ StringUtils.join(valids, "/") + " for " + StringUtils.join(fullNames, "/") + "|");
				}
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + itcEligibility + "] - Invalid ITC eligbility value use either "
						+ StringUtils.join(valids, "/") + " for " + StringUtils.join(fullNames, "/") + " |");
			}

			ee.setFinalError(ee.isFinalError() || isError);
			ee.setError(isError);
			ee.setErrorDesc(error);
			return ee;
		}
	}

	/*public ExcelError validateItcEligibilityImps(String sheetName, int _ELIGIBILITY_TAX_INDEX, int index,
			String itcEligibility, ExcelError ee,String gstinPos ,String pos) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		
		List<String> valids ;
		List<String> fullNames;
		
			valids = Arrays.asList("no","is");
			fullNames = Arrays.asList("none","input-services");
		try{
			if(!("no".equalsIgnoreCase(itcEligibility)||"is".equalsIgnoreCase(itcEligibility)))
				throw new NumberFormatException();	
			int posCode=Integer.parseInt(pos);
			int gstinPosCode=Integer.parseInt(gstinPos);
			if(posCode==gstinPosCode && ("no".equalsIgnoreCase(itcEligibility)||StringUtils.isEmpty(itcEligibility))){
				isError = true;
				error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + itcEligibility
						+ "] - Invalid ITC eligbility value use eligibility as is for input-services as itc is applicable in this transaction |");
				
			}
			else if(posCode!=gstinPosCode && ("is".equalsIgnoreCase(itcEligibility))){
				isError = true;
				error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + itcEligibility
						+ "] - Invalid ITC eligbility value set eligibility as no for none as itc is not applicable |");
				
			}
			
		}
		catch(NumberFormatException e){
			isError = true;
			error.append(sheetName + "," + (_ELIGIBILITY_TAX_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + itcEligibility
					+ "] - Invalid ITC eligbility value use either "+StringUtils.join(valids, "/")+" for "+StringUtils.join(fullNames,"/")+" |");

		}
		
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}*/
	
	
		
	public ExcelError validateExportType(String sheetName, int _EXP_TYPE_INDEX, int index, String expType,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			ExportType.valueOf(expType);
		} catch (Exception e) {
			isError = true;
			error.append(sheetName + "," + (_EXP_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + expType + "] - Invalid export type use either of these-WOPAY/ WPAY|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateSupplyType(String sheetName, int _SUPPLY_TYPE_INDEX, int index, String supplyType,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if(!supplyType.equalsIgnoreCase("FROMPOS"))
				SupplyType.valueOf(StringUtils.upperCase(supplyType));
		} catch (Exception e) {
			isError = true;
			error.append(sheetName + "," + (_SUPPLY_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + supplyType + "] - Invalid supply type value  use either of these-INTER/INTRA |");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	public ExcelError validateHsnDescription(String sheetName, int _HSN_DESC_INDEX, int index, String hsnDescription,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		
		if(!StringUtils.isEmpty(hsnDescription)){
			if(hsnDescription.length()>200){
				isError = true;
				error.append(
						sheetName + "," + (_HSN_DESC_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
								+ hsnDescription + "] - Invalid hsn Description. Description length should not be more  than  200 characters|");
			}
			
		}

		

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	
	
	
	public ExcelError validateMonthYear(String sheetName, int _ORG_MONTH_YEAR_INDEX, int index, String monthYearr,
			ExcelError ee,String currentMonthYear ,boolean isMandatory) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMyyyy");
		SimpleDateFormat sdf=new SimpleDateFormat("MMyyyy");
		try {
			if(!StringUtils.isEmpty(monthYearr))
				sdf.parse(monthYearr);
		} catch (Exception e) {
			isError = true;
			error.append(sheetName + "," + (_ORG_MONTH_YEAR_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + monthYearr + "] - Invalid original month year value  use format MMyyyy |");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	public boolean isAmmendment(String orgnlInvoiceNo, String orgnlInvoiceDate, String orgnlNoteNo, String orgnlNoteDate,
			String orgnlMonthYear, String orgnlPos, String orgnlSupplyType) {
		if (StringUtils.isNoneEmpty(orgnlInvoiceNo) || StringUtils.isNoneEmpty(orgnlInvoiceDate)
				|| StringUtils.isNoneEmpty(orgnlNoteNo) || StringUtils.isNoneEmpty(orgnlNoteDate)
				|| StringUtils.isNoneEmpty(orgnlMonthYear) || StringUtils.isNoneEmpty(orgnlPos)
				|| StringUtils.isNoneEmpty(orgnlSupplyType) )
		{
			return true;
		}
			return false;
	}
	public ExcelError validateOriginalPos(String sheetName, int _ORIGINAL_PLACE_OF_SUPPLY_INDEX, int index, String placeOfSupply,
			 ExcelError ee,boolean isMandatory) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			if(isMandatory||StringUtils.isNoneEmpty(placeOfSupply)){
			int pos = Double.valueOf(placeOfSupply).intValue();
			if (pos < 1 || pos > 37 || pos == 28)
				throw new NumberFormatException();
			}
			
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_ORIGINAL_PLACE_OF_SUPPLY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + placeOfSupply
					+ "] - Invalid original place of supply it must be integer allowed range 1-37|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

		return ee;

	}

	
	public ExcelError validateInvoiceNo(String sheetName, int _EXP_TYPE_INDEX, int index, String invoiceNo,
			ExcelError ee, boolean isMandatory) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int size;
		size = StringUtils.isEmpty(invoiceNo) ? 0 : invoiceNo.length();
		boolean isValidLength = size <= 16 && size >0 ? true : false;
       
		boolean isValid=false;
		if(isValidLength) {
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9-/]+$");
		Matcher matcher = pattern.matcher(invoiceNo);
		isValid = matcher.matches();
		}
		
		if ((StringUtils.isEmpty(invoiceNo) && isMandatory) || !isValidLength || !isValid) {
			isError = true;
			error.append(sheetName + "," + (_EXP_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invoiceNo
					+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");

		}

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	public ExcelError validateCtinAgainstGSTIN(String sheetName, int _CTIN_INDEX, int index, String gstin,String ctin,
			ExcelError ee) {
		
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		
		if(gstin.equalsIgnoreCase(ctin)) {
			
			isError=true;
			error.append(sheetName + "," + (_CTIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + ctin
					+ "] - CGTIN should not be same as user GSTIN |");
		}
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateNoteNo(String sheetName, int _NOTE_NO_INDEX, int index, String noteNo, ExcelError ee,
			boolean isMandatory) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int size;
		size = StringUtils.isEmpty(noteNo) ? 0 : noteNo.length();
		boolean isValidLength = size <= 16 && size > 0 ? true : false;
		
		boolean isValid=false;
		if(isValidLength) {
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9-/]+$");
		Matcher matcher = pattern.matcher(noteNo);
		isValid = matcher.matches();
		}
		if ((StringUtils.isEmpty(noteNo) && isMandatory) || !isValidLength || !isValid) {
			isError = true;
			error.append(sheetName + "," + (_NOTE_NO_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + noteNo
					+ "] - Invalid Credit/Debit/Refund Voucher number  please input relevant data. it should not exceed 16 characters and should not contain any blank space/comma/dot(.)/underscore(_)|");
			
			}

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	
	public boolean validateDocIssueSn(String serialNo) {
		boolean isValid;
		boolean isMandatory=true;
		int size;
		size = StringUtils.isEmpty(serialNo) ? 0 : serialNo.length();
		boolean isValidLength = size <= 16 ? true : false;
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9-/]+$");
		Matcher matcher = pattern.matcher(serialNo);
		isValid = matcher.matches();
		if ((StringUtils.isEmpty(serialNo) && isMandatory) || !isValidLength || !isValid) {
			isValid=false;
			
			}
		else
			isValid=true;
			return isValid;
	}
	
	
	public ExcelError validateDocIssueSnoExcel(String sheetName, int _SNO_INDEX, int index, String sno, ExcelError ee,
			boolean isMandatory) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int size;
		size = StringUtils.isEmpty(sno) ? 0 : sno.length();
		boolean isValidLength = size <= 16 ? true : false;
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9-/]+$");
		Matcher matcher = pattern.matcher(sno);
		boolean isValid = matcher.matches();
		if ((StringUtils.isEmpty(sno) && isMandatory) || !isValidLength || !isValid) {
			isError = true;
			error.append(sheetName + "," + (_SNO_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + sno
					+ "] - Invalid Serial No value please input relevant data. it should not exceed 16 characters and should not contain any blank space/comma/dot(.)/underscore(_)|");
			
			}

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	
	public ExcelError appendDuplicateInvoiceNoMsg(String transactionName,  String invNo, String msg,ExcelError ee) {
		String message=StringUtils.isEmpty(msg)?"Duplicate entry for invoice number "+invNo:msg;
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		error.append(transactionName + ",  , ," + message+"|");

		return ee;
	}

	public ExcelError validateShippingBillNo(String sheetName, int _BILL_NO_INDEX, int index, String billNo,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		/*
		 * if (StringUtils.isEmpty(billNo)) { isError = true;
		 * error.append(sheetName + "," + (_BILL_NO_INDEX + 1) + "," + index +
		 * "," + ee.getUniqueRowValue() + " Value - [" + billNo +
		 * "] - Invalid shipping bill number please input relevant data|"); }
		 */

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateHsnOrDesc(String sheetName, int _HSN_INDEX, int index, String hsn, String desc,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (StringUtils.isEmpty(hsn) && StringUtils.isEmpty(desc)) {
			isError = true;
			error.append(sheetName + "," + (_HSN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ hsn + "] - Please Input atleast one field from Hsn and Description|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateRevAndEtin(String sheetName, int _ETIN_INDEX, int index, boolean isReverse, String etin,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (isReverse && !StringUtils.isEmpty(etin)) {
			isError = true;
			error.append(sheetName + "," + (_ETIN_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ etin + "] - Remove etin as it is not applicable in case of reverse charge -YES/Y|");
		} /*
			 * else if (!isReverse && StringUtils.isEmpty(etin) ) { isError =
			 * true; error.append(sheetName + "," + (_ETIN_INDEX + 1) + "," +
			 * index +
			 * ", Please Provide etin as  it is applicable in case of reverse charge-NO/N|"
			 * ); }
			 */

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateHsnCode(String sheetName, int _HSN_CODE_INDEX, int index, String hsnCode, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int size = StringUtils.isEmpty(hsnCode) ? 0 : hsnCode.length();
		try{
		if(!StringUtils.isEmpty(hsnCode)){
		if (size > 8) {
			throw new NumberFormatException();
		}
		Integer.parseInt(hsnCode);
		}
		}catch(NumberFormatException e){			isError = true;
			error.append(sheetName + "," + (_HSN_CODE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + hsnCode
					+ "] - Invalid HSN it should be numeric and length can not be exceeded more than 8 digits please input relevant data|");
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateType(String sheetName, int _TYPE_INDEX, int index, String type, ExcelError ee,String etin) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (!("E".equalsIgnoreCase(type) || "OE".equalsIgnoreCase(type))) {
			isError = true;
			error.append(sheetName + "," + (_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ type
					+ "] - Invalid  Type value please input either E/OE for ecom/other than ecom |");
			ee.setError(isError);
			ee.setErrorDesc(error);
		}
	/*	else if("E".equalsIgnoreCase(type) && StringUtils.isEmpty(etin)) {
			
			isError = true;
			error.append(sheetName + "," + (_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ type
					+ "] - Invalid  Type value please input OE for other than ecom |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			
		}*/
		return ee;
	}

	public ExcelError validateQuantity(String sheetName, int _QUANTITY_INDEX, int index, String quantity,
			ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			if (!StringUtils.isEmpty(quantity)) {
				double qty = Double.parseDouble(quantity);
				if (qty < 0.0)
					throw new NumberFormatException();

			}

		} catch (NumberFormatException exp) {
			isError = true;
			error.append(
					sheetName + "," + (_QUANTITY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
							+ quantity + "] - Invalid Quantity value use numeric value and greater than 0.0 only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateQuantityB2CS(String sheetName, int _QUANTITY_INDEX, int index, String quantity,
			ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			if (!StringUtils.isEmpty(quantity)) {
				Double.parseDouble(quantity);
			}

		} catch (NumberFormatException exp) {
			isError = true;
			error.append(
					sheetName + "," + (_QUANTITY_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
							+ quantity + "] - Invalid Quantity value use numeric value only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateTaxableValue(String sheetName, int _TAXABLE_VALUE, int index, String taxVal,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (true) {
			try {
				if (Double.parseDouble(taxVal) < 0.0)
					throw new NumberFormatException();
				Double.parseDouble(taxVal);
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_TAXABLE_VALUE + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + taxVal
						+ "] - Invalid taxable value use numeric value and greater than 0.0 only|");
			}
		} else {
			try {
				Double.parseDouble(taxVal);
			} catch (NumberFormatException e) {
				isError = true;
				error.append(sheetName + "," + (_TAXABLE_VALUE + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + taxVal + "] - Invalid taxable value use numeric value only|");
			}
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateAdvanceAdjustedValue(String sheetName, int _TAXABLE_VALUE, int index, String taxVal,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if (Double.parseDouble(taxVal) < 0.0)
				throw new NumberFormatException();
			Double.parseDouble(taxVal);
		} catch (NumberFormatException e) {
			isError = true;
			error.append(
					sheetName + "," + (_TAXABLE_VALUE + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
							+ taxVal + "] - Invalid advance amount use numeric value and greater than 0.0 only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateTaxableValueB2cs(String sheetName, int _TAXABLE_VALUE, int index, String taxVal,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {

			Double.parseDouble(taxVal);
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_TAXABLE_VALUE + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + taxVal + "] - Invalid taxable value use numeric value  only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateTaxRate(String sheetName, int _TAX_RATE_INDEX, int index, String taxRt, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {// mandatory
				// if (!StringUtils.isEmpty(taxRt)) {
			if(!ee.isSewopOrWopay()){

			checkIgstTaxRates(taxRt);
			double rt=Double.parseDouble(taxRt);
			_Logger.debug("validate tax rt {}",rt);

			}
			// }
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_TAX_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + taxRt
					+ "] - Invalid Tax rate use numeric value only allowed values 0.0/0.25/3.0/5.0/12.0/18.0/28.0|");
		}
		
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	/*
	 * public ExcelError validateTaxAmt(String sheetName, int _TAX_AMT_INDEX,
	 * int index, String taxAmt, ExcelError ee) {
	 * 
	 * boolean isError = ee.isError(); StringBuilder error = ee.getErrorDesc();
	 * 
	 * try { if (!StringUtils.isEmpty(taxAmt)) { if (Double.parseDouble(taxAmt)
	 * < 0.0) throw new NumberFormatException(); if (Double.parseDouble(taxAmt)
	 * != 0.0) { Double.parseDouble(taxAmt); } } } catch (NumberFormatException
	 * e) { isError = true; error.append(sheetName + "," + (_TAX_AMT_INDEX + 1)
	 * + "," + index + "," + ee.getUniqueRowValue() + " Value - [" + taxAmt +
	 * "] - Invalid tax amount use numeric value only|"); }
	 * 
	 * ee.setFinalError(ee.isFinalError() || isError); ee.setError(isError);
	 * ee.setErrorDesc(error); return ee;
	 * 
	 * }
	 */

	/*
	 * public ExcelError validateItcTaxAmt(String sheetName, int _TAX_AMT_INDEX,
	 * int index, String taxAmt, String elg, double cTaxAmount, ExcelError ee) {
	 * 
	 * boolean isError = ee.isError(); StringBuilder error = ee.getErrorDesc();
	 * 
	 * try { if (!StringUtils.isEmpty(taxAmt)) { double d =
	 * Double.parseDouble(taxAmt); if (Double.parseDouble(taxAmt) < 0.0) throw
	 * new NumberFormatException(); if (Double.parseDouble(taxAmt) != 0.0) {
	 * Double.parseDouble(taxAmt); } } } catch (NumberFormatException e) {
	 * isError = true; error.append(sheetName + "," + (_TAX_AMT_INDEX + 1) + ","
	 * + index + "," + ee.getUniqueRowValue() + " Value - [" + taxAmt +
	 * "] - Invalid tax amount use numeric value only|"); }
	 * 
	 * ee.setFinalError(ee.isFinalError() || isError); ee.setError(isError);
	 * ee.setErrorDesc(error); return ee;
	 * 
	 * }
	 */

	public ExcelError validateItcTaxAmt(String sheetName, int _TAX_AMT_INDEX, int index, String taxAmt, String elg,
			String cTaxAmount, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		double cTaxAmt = 0.0;

		try {
			if (!StringUtils.isEmpty(taxAmt)) {
				cTaxAmt = Double.parseDouble(cTaxAmount);

				if (Double.parseDouble(taxAmt) < 0.0)
					throw new NumberFormatException();
				if (("no".equalsIgnoreCase(elg) || StringUtils.isEmpty(elg)) && Double.parseDouble(taxAmt) > 0) {
					isError = true;
					error.append(sheetName + "," + (_TAX_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
							+ " Value - [" + taxAmt
							+ "] - Remove tax amount as this is not applicable in itc elibility no/none/empty case|");
				} else if ("is".equalsIgnoreCase(elg) || "ip".equalsIgnoreCase(elg) || "cp".equalsIgnoreCase(elg)) {
					double taxAmount = Double.parseDouble(taxAmt);
					if(cTaxAmt!=0 && taxAmount==0){
						isError = true;
						error.append(sheetName + "," + (_TAX_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
								+ " Value - [" + taxAmt + "] - Invalid tax amount it should not be 0 if itc eligibility is any of is/ip/cp "
								+ cTaxAmount + "|");
					}
					else
					if (!(taxAmount <= cTaxAmt)) {
						isError = true;
						error.append(sheetName + "," + (_TAX_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
								+ " Value - [" + taxAmt + "] - Invalid tax amount it should be less than or equal "
								+ cTaxAmount + "|");
					}

				} /*else if ("is".equalsIgnoreCase(elg) || "ip".equalsIgnoreCase(elg) || "cp".equalsIgnoreCase(elg)) {
					double taxAmount = Double.parseDouble(taxAmt);
					if (!(taxAmount <= cTaxAmt)) {
						isError = true;
						error.append(sheetName + "," + (_TAX_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
								+ " Value - [" + taxAmt + "] - Invalid tax amount it should be less than or equal "
								+ cTaxAmount + "|");
					}

				}*/

			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_TAX_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + taxAmt + "] - Invalid tax amount use numeric value only|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateNumericValue(String sheetName, int _NUMERIC_VALUE_INDEX, int index, String value,
			String msg, ExcelError ee) {
		String dispMsg = "Invalid Input Value ";
		if (!StringUtils.isEmpty(msg))
			dispMsg = msg;
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if(!StringUtils.isEmpty(value)) {
		try {
				Double.parseDouble(value);
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_NUMERIC_VALUE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + value + "] - " + dispMsg + " it should be numeric value only|");
		}
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}
	
	public ExcelError validateIntegerNumericValue(String sheetName, int _NUMERIC_VALUE_INDEX, int index, String value,
			String msg, ExcelError ee) {
		String dispMsg = "Invalid Input Value ";
		if (!StringUtils.isEmpty(msg))
			dispMsg = msg;
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			
			int i=0;
				i=Integer.parseInt(value);
				if(i<0)
					throw new NumberFormatException();
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_NUMERIC_VALUE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + value + "] - " + dispMsg + " it should be positive Integer only|");
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}
	
	public ExcelError validateTotalNumber(String sheetName, int _NUMERIC_VALUE_INDEX, int index, int canceled,int totalNo,
			 ExcelError ee) {
		
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		
			if(canceled>totalNo){
				isError = true;
				error.append(sheetName + "," + (_NUMERIC_VALUE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + totalNo + "] - invalid total number value it should be greater than or equal to canceled number|");
			}
				
		
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}
	

	public ExcelError validateBillNo(String sheetName, int _BILL_NO_INDEX, int index, String billNo, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (StringUtils.isEmpty(billNo)) {
			isError = true;
			error.append(sheetName + "," + (_BILL_NO_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + billNo + "] - Invalid bill number please input relevant data|");
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError validateUnit(String sheetName, int _UNIT_INDEX, int index, String unit, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (!StringUtils.isEmpty(unit)) {
			String regex = "(.)*(\\d)(.)*"; // to check if string contains a no.
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(unit);
			boolean isMatched = matcher.matches();
			if (isMatched) {
				isError = true;
				error.append(sheetName + "," + (_UNIT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + unit
						+ "] - Invalid unit value it should not contains any number please input relevant data|");
			}
		}

		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	public ExcelError setIsIgst(String sheetName, String gstinPos, String cgstin, String pos, ExcelError ee) {
		String posValue = "";
		ee.setIgst(false);
		try {
			if (StringUtils.isEmpty(pos)) {
				if (!StringUtils.isEmpty(cgstin) && cgstin.length() == 15) {
					String ctin = cgstin;
					String st = ctin.substring(0, 2);
					int scode = Integer.parseInt(st);
					if ((scode < 1 || scode > 37) && scode !=97 )
						throw new NumberFormatException();
					posValue = st;
				}
			} else {
				int ps = Double.valueOf(pos).intValue();
				if ((ps < 1 || ps > 37) && ps !=97 )
					throw new NumberFormatException();
				if (ps < 10)
					posValue = "0" + ps;
				else
					posValue = ps + "";
			}
			if (!gstinPos.equalsIgnoreCase(posValue))
				ee.setIgst(true);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return ee;
	}

	public ExcelError validateNoteType(String sheetName, int _NOTE_TYPE_INDEX, int index, String noteType,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (!("C".equalsIgnoreCase(noteType)  || "D".equalsIgnoreCase(noteType)
				 || "R".equalsIgnoreCase(noteType)
				)) {
			isError = true;
			error.append(sheetName + "," + (_NOTE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + noteType
					+ "] - Invalid note type value please input either C/D/R for CREDIT/DEBIT/REFUND relevant data|");
		}

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	protected void validateInvoiceType(String sheetName, int _INVOICE_TYPE_INDEX, int index, String invoiceType,
			ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			B2BInvType.valueOf(invoiceType.toUpperCase());
		} catch (Exception e) {
			isError = true;
			error.append(sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invoiceType + "] - Invalid invoice type value it should be R/SEWP/SEWOP/DE |");
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);

	}
	
	protected ExcelError validateInvoiceTypeCdnUr(String sheetName, int _INVOICE_TYPE_INDEX, int index, String invoiceType,
			ExcelError ee,String returnType) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
				CDNURInvType.valueOf(invoiceType.toUpperCase());
				if("GSTR2".equalsIgnoreCase(returnType) && !StringUtils.isEmpty(invoiceType)){
					if(!("B2BUR".equalsIgnoreCase(invoiceType)||"IMPS".equalsIgnoreCase(invoiceType))){
						throw new AppException();
					}
				}
					
		} catch (Exception e) {
			isError = true;
			if("GSTR1".equalsIgnoreCase(returnType))
			error.append(sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + invoiceType + "] - Invalid invoice type value it should be EXPWP/EXPWOP/B2CL |");
			else
			error.append(sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceType + "] - Invalid invoice type value it should be B2BUR/IMPS |");
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	protected ExcelError validatePreGstRegime(String sheetName, int _PRE_GST_REGIME_INDEX, String preGstRegime,
			int index, ExcelError ee) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (!preGstRegime.equalsIgnoreCase("Y") && !preGstRegime.equalsIgnoreCase("N")) {
			isError = true;
			error.append(sheetName + "," + (_PRE_GST_REGIME_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + preGstRegime
					+ "] - Invalid Pre GST Regime value please input Y or N relevant data|");
		}

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	public ExcelError validateSno(String sheetName, int _SNO_INDEX, int index, String sNo, ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		try {
			Integer.parseInt(sNo);
			if (StringUtils.isEmpty(sNo)) {

				isError = true;
				error.append(sheetName + "," + (_SNO_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + sNo + "] - Invalid Serial No please input relevant data|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_SNO_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
					+ sNo + "] - Invalid Serial No It should be numeric value only|");
		}

		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;

	}

	protected int _SNO_INDEX = 100;
	protected int _HSN_CODE_INDEX = 100;
	protected int _DESCRIPTION_INDEX = 100;
	protected int _UNIT_INDEX = 100;
	protected int _QUANTITY_INDEX = 100;
	protected int _TAXABLE_VALUE_INDEX = 100;
	protected int _TAX_RATE_INDEX = 100;
	protected int _IGST_AMT_INDEX = 100;
	protected int _CGST_AMT_INDEX = 100;
	protected int _SGST_AMT_INDEX = 100;
	protected int _CESS_AMT_INDEX = 100;
	protected int _ELIGIBILITY_TAX_INDEX = 100;
	protected int _IGST_TAX_AVAIL_INDEX = 100;
	protected int _CGST_TAX_AVAIL_INDEX = 100;
	protected int _SGST_TAX_AVAIL_INDEX = 100;
	protected int _CESS_TAX_AVAIL_INDEX = 100;
	protected int _ADVANCE_AMOUNT_INDEX = 100;
	protected int _INVOICE_VALUE_INDEX = 100;


	protected double setInvoiceItems(ExcelError ee, String sheetName, String gstReturn, int index, List<String> temp,
			Item item, TransactionType transactionType) {
		ee.setNoValidationCheckForGinesys(false);
		double taxAmount = 0.0;
		// setting serial no
		if (_SNO_INDEX != 100) {
			ee = this.validateSno(sheetName, _SNO_INDEX, index, temp.get(_SNO_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SNO_INDEX)))
				item.setSerialNumber(Integer.parseInt(temp.get(_SNO_INDEX)));
		}

		// setting goods and services code
		if (_HSN_CODE_INDEX != 100) {
			ee = this.validateHsnCode(sheetName, _HSN_CODE_INDEX, index, temp.get(_HSN_CODE_INDEX), ee);
			if (!ee.isError())
				item.setHsnCode(temp.get(_HSN_CODE_INDEX));
		}

		// setting description
		if (_DESCRIPTION_INDEX != 100){
			ee = this.validateHsnDescription(sheetName, _DESCRIPTION_INDEX, index, temp.get(_DESCRIPTION_INDEX), ee);

			item.setHsnDescription(temp.get(_DESCRIPTION_INDEX));
			
		}

		// ee = this.validateHsnOrDesc(sheetName, _HSN_CODE_INDEX, index,
		// temp.get(_HSN_CODE_INDEX),
		// temp.get(_DESCRIPTION_INDEX), ee);

		// setting unit
		if (_UNIT_INDEX != 100)
			item.setUnit(temp.get(_UNIT_INDEX));

		// setting quantity
		if (_QUANTITY_INDEX != 100) {
			ee = this.validateQuantity(sheetName, _QUANTITY_INDEX, index, temp.get(_QUANTITY_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_QUANTITY_INDEX)))
				item.setQuantity(Double.parseDouble(temp.get(_QUANTITY_INDEX)));
		}
		double taxableValue = 0.0;

		if (_TAXABLE_VALUE_INDEX != 100 || _ADVANCE_AMOUNT_INDEX != 100) {
			if (TransactionType.AT == transactionType) {
				_TAXABLE_VALUE_INDEX = _ADVANCE_AMOUNT_INDEX;
				ee = this.validateAdvanceAdjustedValue(sheetName, _ADVANCE_AMOUNT_INDEX, index,
						temp.get(_ADVANCE_AMOUNT_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_ADVANCE_AMOUNT_INDEX))) {
					item.setTaxableValue(Double.parseDouble(temp.get(_ADVANCE_AMOUNT_INDEX)));
				}
			} else {
				// setting taxable value
				ee = this.validateTaxableValue(sheetName, _TAXABLE_VALUE_INDEX, index, temp.get(_TAXABLE_VALUE_INDEX),
						ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAXABLE_VALUE_INDEX)))
					item.setTaxableValue(Double.parseDouble(temp.get(_TAXABLE_VALUE_INDEX)));// advance
			}
		}
		double taxRate = 100;
		// SETTING tax rate
		if (_TAX_RATE_INDEX != 100 && !ee.isSewopOrWopay()) {
			ee = this.validateTaxRate(sheetName, _TAX_RATE_INDEX, index, temp.get(_TAX_RATE_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAX_RATE_INDEX)))
				item.setTaxRate(Double.parseDouble(temp.get(_TAX_RATE_INDEX)));
			try {
				taxRate = Double.parseDouble(temp.get(_TAX_RATE_INDEX));
				taxableValue = Double.parseDouble(temp.get(_TAXABLE_VALUE_INDEX));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if (taxRate > 28.0)
			taxRate = 28.0;

		// setting igst amt
		if (_IGST_AMT_INDEX != 100) {
			ee = this.validateIgstAmt(sheetName, _IGST_AMT_INDEX, index, temp.get(_IGST_AMT_INDEX), taxRate,
					taxableValue, ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_IGST_AMT_INDEX))) {
				double igst = Double.parseDouble(temp.get(_IGST_AMT_INDEX));
				item.setIgst(igst);
				taxAmount += igst;
			}
		}
		// setting cgst amt

		if (TransactionType.EXP != transactionType && TransactionType.CDNUR != transactionType
				&& TransactionType.B2CL != transactionType) {
			if (_CGST_AMT_INDEX != 100) {
				ee = this.validateCgstAmt(sheetName, _CGST_AMT_INDEX, index, temp.get(_CGST_AMT_INDEX), taxRate,
						taxableValue, ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CGST_AMT_INDEX))) {
					double cgst = Double.parseDouble(temp.get(_CGST_AMT_INDEX));
					item.setCgst(cgst);
					taxAmount += cgst;
				}
			}

			// setting sgst amt
			if (_SGST_AMT_INDEX != 100) {
				ee = this.validateSgstAmt(sheetName, _SGST_AMT_INDEX, index, temp.get(_SGST_AMT_INDEX), taxRate,
						taxableValue, ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SGST_AMT_INDEX))) {
					double sgst = Double.parseDouble(temp.get(_SGST_AMT_INDEX));
					item.setSgst(sgst);
					taxAmount += sgst;
				}
			}
		}

		// setting Cess amt
		if (_CESS_AMT_INDEX != 100) {
			ee = this.validateCessAmount(sheetName, _CESS_AMT_INDEX, index, temp.get(_CESS_AMT_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CESS_AMT_INDEX))) {
				double cessAmt = Double.parseDouble(temp.get(_CESS_AMT_INDEX));
				item.setCess(cessAmt);
				taxAmount += cessAmt;
			}
		}

		item.setTaxAmount(taxAmount);

		if (gstReturn.equalsIgnoreCase("gstr2")) {
			
			double totalItc=0;

			// setting itc details

			// following serial no can be changed
			if (_ELIGIBILITY_TAX_INDEX != 100) {
				ee = this.validateItcEligibility(sheetName, _ELIGIBILITY_TAX_INDEX, index,
						temp.get(_ELIGIBILITY_TAX_INDEX), ee,transactionType);
				// if (!ee.isError())
				item.setTotalEligibleTax(temp.get(_ELIGIBILITY_TAX_INDEX));
			}

			// following serial no can be changed
			if (_IGST_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _IGST_TAX_AVAIL_INDEX, index, temp.get(_IGST_TAX_AVAIL_INDEX),
						item.getTotalEligibleTax(), temp.get(_IGST_TAX_AVAIL_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_IGST_TAX_AVAIL_INDEX))) {
					
					
					item.setItcIgst(Double.parseDouble(temp.get(_IGST_TAX_AVAIL_INDEX)));
				
				totalItc+=item.getItcIgst();
				}
			}

			// following serial no can be changed
			if (_CGST_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _CGST_TAX_AVAIL_INDEX, index, temp.get(_CGST_TAX_AVAIL_INDEX),
						item.getTotalEligibleTax(), temp.get(_CGST_TAX_AVAIL_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CGST_TAX_AVAIL_INDEX))) {
					item.setItcCgst(Double.parseDouble(temp.get(_CGST_TAX_AVAIL_INDEX)));
					totalItc+=item.getItcCgst();
	
				}
			}

			// following serial no can be changed
			if (_SGST_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _SGST_TAX_AVAIL_INDEX, index, temp.get(_SGST_TAX_AVAIL_INDEX),
						item.getTotalEligibleTax(), temp.get(_SGST_TAX_AVAIL_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SGST_TAX_AVAIL_INDEX))) {
					item.setItcSgst(Double.parseDouble(temp.get(_SGST_TAX_AVAIL_INDEX)));
					totalItc+=item.getItcSgst();
	
				}
			}

			// following serial no can be changed
			if (_CESS_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _CESS_TAX_AVAIL_INDEX, index, temp.get(_CESS_TAX_AVAIL_INDEX),
						item.getTotalEligibleTax(), temp.get(_CESS_TAX_AVAIL_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CESS_TAX_AVAIL_INDEX))) {
					item.setItcCess(Double.parseDouble(temp.get(_CESS_TAX_AVAIL_INDEX)));
					totalItc+=item.getItcCess();
				}
			}
			
			this.validateItcAmtOverTaxamt(sheetName, index, taxAmount, totalItc, ee);
						
		}

		return taxAmount;
	}
	
	private boolean validateItcAmtOverTaxamt(String sheetName,int index,double totalTaxAmt,double totalItcAmt,ExcelError ee) {
		
		if(totalTaxAmt < totalItcAmt) {
			ee.setError(true);
			
			ee.getErrorDesc().append(sheetName + ",-," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + totalItcAmt
					+ "] - Total Itc amount should not be greater than total tax amount ["+totalTaxAmt+"] |");
			ee.setFinalError(true);
			return false;
		}
		return true;
		
	}
	protected double setInvoiceItemsCsv(ExcelError ee, String sheetName, String gstReturn, int index, UploadedCsv lineItem,
			Item item, TransactionType transactionType) {
		ee.setNoValidationCheckForGinesys(false);
		double taxAmount = 0.0;
		// setting serial no
		/*if (_SNO_INDEX != 100) {
			ee = this.validateSno(sheetName, _SNO_INDEX, index, temp.get(_SNO_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SNO_INDEX)))
				item.setSerialNumber(Integer.parseInt(temp.get(_SNO_INDEX)));
		}*/

		// setting goods and services code
		if (_HSN_CODE_INDEX != 100) {
			ee = this.validateHsnCode(sheetName, _HSN_CODE_INDEX, index, lineItem.getHsnSacCode(), ee);
			if (!ee.isError())
				item.setHsnCode(lineItem.getHsnSacCode());
		}

		// setting description
		if (_DESCRIPTION_INDEX != 100){
			ee = this.validateHsnDescription(sheetName, _DESCRIPTION_INDEX, index, lineItem.getHsnSacDesc(), ee);
			item.setHsnDescription(lineItem.getHsnSacDesc());
			
		}

		// ee = this.validateHsnOrDesc(sheetName, _HSN_CODE_INDEX, index,
		// temp.get(_HSN_CODE_INDEX),
		// temp.get(_DESCRIPTION_INDEX), ee);

		// setting unit
		if (_UNIT_INDEX != 100)
			item.setUnit(lineItem.getUom());

		// setting quantity
		if (_QUANTITY_INDEX != 100) {
			ee = this.validateQuantity(sheetName, _QUANTITY_INDEX, index, lineItem.getQuantity(), ee);
			if (!ee.isError() && !StringUtils.isEmpty(lineItem.getQuantity()))
				item.setQuantity(Double.parseDouble(lineItem.getQuantity()));
		}
		double taxableValue = 0.0;

		if (_TAXABLE_VALUE_INDEX != 100 || _ADVANCE_AMOUNT_INDEX != 100) {
			if (TransactionType.AT == transactionType) {
				_TAXABLE_VALUE_INDEX = _ADVANCE_AMOUNT_INDEX;
				ee = this.validateAdvanceAdjustedValue(sheetName, _ADVANCE_AMOUNT_INDEX, index,
						lineItem.getTaxableAmount(), ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getTaxableAmount())) {
					item.setTaxableValue(Double.parseDouble(lineItem.getTaxableAmount()));
				}
			} else {
				// setting taxable value
				ee = this.validateTaxableValue(sheetName, _TAXABLE_VALUE_INDEX, index, lineItem.getTaxableAmount(),
						ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getTaxableAmount()))
					item.setTaxableValue(Double.parseDouble(lineItem.getTaxableAmount()));// advance
			}
		}
		double taxRate = 100;
		// SETTING tax rate
		if (_TAX_RATE_INDEX != 100) {
			ee = this.validateTaxRate(sheetName, _TAX_RATE_INDEX, index, lineItem.getTotalTaxrate(), ee);
			if (!ee.isError() && !StringUtils.isEmpty(lineItem.getTotalTaxrate()))
				item.setTaxRate(Double.parseDouble(lineItem.getTotalTaxrate()));
			try {
				taxRate = Double.parseDouble(lineItem.getTotalTaxrate());
				taxableValue = Double.parseDouble(lineItem.getTotalTaxrate());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if (taxRate > 28.0)
			taxRate = 28.0;

		// setting igst amt
		if (_IGST_AMT_INDEX != 100) {
			ee = this.validateIgstAmt(sheetName, _IGST_AMT_INDEX, index, lineItem.getIgstAmount(), taxRate,
					taxableValue, ee);
			if (!ee.isError() && !StringUtils.isEmpty(lineItem.getIgstAmount())) {
				double igst = Double.parseDouble(lineItem.getTotalTaxrate());
				item.setIgst(igst);
				taxAmount += igst;
			}
		}
		// setting cgst amt

		if (TransactionType.EXP != transactionType && TransactionType.CDNUR != transactionType
				&& TransactionType.B2CL != transactionType) {
			if (_CGST_AMT_INDEX != 100) {
				ee = this.validateCgstAmt(sheetName, _CGST_AMT_INDEX, index, lineItem.getCgstAmount(), taxRate,
						taxableValue, ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getCgstAmount())) {
					double cgst = Double.parseDouble(lineItem.getCgstAmount());
					item.setCgst(cgst);
					taxAmount += cgst;
				}
			}

			// setting sgst amt
			if (_SGST_AMT_INDEX != 100) {
				ee = this.validateSgstAmt(sheetName, _SGST_AMT_INDEX, index,  lineItem.getSgstAmount(), taxRate,
						taxableValue, ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getSgstAmount())) {
					double sgst = Double.parseDouble(lineItem.getSgstAmount());
					item.setSgst(sgst);
					taxAmount += sgst;
				}
			}
		}

		// setting Cess amt
		if (_CESS_AMT_INDEX != 100) {
			ee = this.validateCessAmount(sheetName, _CESS_AMT_INDEX, index, lineItem.getCessAmount(), ee);
			if (!ee.isError() && !StringUtils.isEmpty( lineItem.getCessAmount())) {
				double cessAmt = Double.parseDouble( lineItem.getCessAmount());
				item.setCess(cessAmt);
				taxAmount += cessAmt;
			}
		}

		item.setTaxAmount(taxAmount);

		if (gstReturn.equalsIgnoreCase("gstr2")) {

			// setting itc details

			// following serial no can be changed
			if (_ELIGIBILITY_TAX_INDEX != 100) {
				ee = this.validateItcEligibility(sheetName, _ELIGIBILITY_TAX_INDEX, index,
						lineItem.getInputEligibility(), ee,transactionType);
				// if (!ee.isError())
				item.setTotalEligibleTax(lineItem.getInputEligibility());
			}

			// following serial no can be changed
			if (_IGST_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _IGST_TAX_AVAIL_INDEX, index, lineItem.getInputIgstAmount(),
						item.getTotalEligibleTax(), lineItem.getInputIgstAmount(), ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getInputIgstAmount()))
					item.setItcIgst(Double.parseDouble(lineItem.getInputIgstAmount()));
			}

			// following serial no can be changed
			if (_CGST_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _CGST_TAX_AVAIL_INDEX, index, lineItem.getInputCgstAmount(),
						item.getTotalEligibleTax(), lineItem.getInputCgstAmount(), ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getInputCgstAmount()))
					item.setItcCgst(Double.parseDouble(lineItem.getInputCgstAmount()));
			}

			// following serial no can be changed
			if (_SGST_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _SGST_TAX_AVAIL_INDEX, index, lineItem.getInputSgstAmount(),
						item.getTotalEligibleTax(), lineItem.getInputSgstAmount(), ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getInputSgstAmount()))
					item.setItcSgst(Double.parseDouble(lineItem.getInputSgstAmount()));
			}

			// following serial no can be changed
			if (_CESS_TAX_AVAIL_INDEX != 100) {
				ee = this.validateItcTaxAmt(sheetName, _CESS_TAX_AVAIL_INDEX, index,lineItem.getCessAmount(),
						item.getTotalEligibleTax(), lineItem.getCessAmount(), ee);
				if (!ee.isError() && !StringUtils.isEmpty(lineItem.getCessAmount()))
					item.setItcCess(Double.parseDouble(lineItem.getCessAmount()));
			}
		}

		return taxAmount;
	}


	protected double setInvoiceItemsB2CS(ExcelError ee, String sheetName, String gstReturn, int index,
			List<String> temp, Item item, TransactionType transactionType) {

		double taxAmount = 0.0;
		// setting serial no
		if (_SNO_INDEX != 100) {
			ee = this.validateSno(sheetName, _SNO_INDEX, index, temp.get(_SNO_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SNO_INDEX)))
				item.setSerialNumber(Integer.parseInt(temp.get(_SNO_INDEX)));
		}

		// setting goods and services code
		if (_HSN_CODE_INDEX != 100) {
			ee = this.validateHsnCode(sheetName, _HSN_CODE_INDEX, index, temp.get(_HSN_CODE_INDEX), ee);
			if (!ee.isError())
				item.setHsnCode(temp.get(_HSN_CODE_INDEX));
		}

		// setting description
		if (_DESCRIPTION_INDEX != 100)
			item.setHsnDescription(temp.get(_DESCRIPTION_INDEX));

		// ee = this.validateHsnOrDesc(sheetName, _HSN_CODE_INDEX, index,
		// temp.get(_HSN_CODE_INDEX),
		// temp.get(_DESCRIPTION_INDEX), ee);

		// setting unit
		if (_UNIT_INDEX != 100)
			item.setUnit(temp.get(_UNIT_INDEX));

		// setting quantity
		if (_QUANTITY_INDEX != 100) {
			ee = this.validateQuantityB2CS(sheetName, _QUANTITY_INDEX, index, temp.get(_QUANTITY_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_QUANTITY_INDEX)))
				item.setQuantity(item.getQuantity()+Math.abs(Double.parseDouble(temp.get(_QUANTITY_INDEX))));
		}
		double taxableValue = 0.0;

		if (_TAXABLE_VALUE_INDEX != 100 || _ADVANCE_AMOUNT_INDEX != 100) {
			if (TransactionType.AT == transactionType) {
				_TAXABLE_VALUE_INDEX = _ADVANCE_AMOUNT_INDEX;
				ee = this.validateAdvanceAdjustedValue(sheetName, _ADVANCE_AMOUNT_INDEX, index,
						temp.get(_ADVANCE_AMOUNT_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_ADVANCE_AMOUNT_INDEX))) {
					item.setTaxableValue(Double.parseDouble(temp.get(_ADVANCE_AMOUNT_INDEX)));
				}
			} else {
				// setting taxable value
				ee = this.validateTaxableValueB2cs(sheetName, _TAXABLE_VALUE_INDEX, index,
						temp.get(_TAXABLE_VALUE_INDEX), ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAXABLE_VALUE_INDEX)))
					item.setTaxableValue(item.getTaxableValue()+Double.parseDouble(temp.get(_TAXABLE_VALUE_INDEX)));// advance
			}
		}
		double taxRate = 100;
		// SETTING tax rate
		if (_TAX_RATE_INDEX != 100) {
			ee = this.validateTaxRate(sheetName, _TAX_RATE_INDEX, index, temp.get(_TAX_RATE_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAX_RATE_INDEX)))
				item.setTaxRate(Double.parseDouble(temp.get(_TAX_RATE_INDEX)));
			try {
				taxRate = Double.parseDouble(temp.get(_TAX_RATE_INDEX));
				taxableValue = Double.parseDouble(temp.get(_TAXABLE_VALUE_INDEX));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if (taxRate > 28.0)
			taxRate = 28.0;

		// setting igst amt
		if (_IGST_AMT_INDEX != 100) {
			ee = this.validateIgstAmtB2cs(sheetName, _IGST_AMT_INDEX, index, temp.get(_IGST_AMT_INDEX), taxRate,
					taxableValue, ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_IGST_AMT_INDEX))) {
				double igst = Double.parseDouble(temp.get(_IGST_AMT_INDEX));
				  item.setIgst(item.getIgst()+igst);
				taxAmount += igst;
			}
		}
		// setting cgst amt

		if (TransactionType.EXP != transactionType && TransactionType.CDNUR != transactionType
				&& TransactionType.B2CL != transactionType) {
			if (_CGST_AMT_INDEX != 100) {
				ee = this.validateCgstAmtB2cs(sheetName, _CGST_AMT_INDEX, index, temp.get(_CGST_AMT_INDEX), taxRate,
						taxableValue, ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CGST_AMT_INDEX))) {
					double cgst = Double.parseDouble(temp.get(_CGST_AMT_INDEX));
					item.setCgst(item.getCgst()+cgst);
					taxAmount += cgst;
				}
			}

			// setting sgst amt
			if (_SGST_AMT_INDEX != 100) {
				ee = this.validateSgstAmtB2cs(sheetName, _SGST_AMT_INDEX, index, temp.get(_SGST_AMT_INDEX), taxRate,
						taxableValue, ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SGST_AMT_INDEX))) {
					double sgst = Double.parseDouble(temp.get(_SGST_AMT_INDEX));
					item.setSgst(item.getSgst()+sgst);
					taxAmount += sgst;
				}
			}
		}

		// setting Cess amt
		if (_CESS_AMT_INDEX != 100) {
			ee = this.validateCessAmountB2cs(sheetName, _CESS_AMT_INDEX, index, temp.get(_CESS_AMT_INDEX), ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CESS_AMT_INDEX))) {
				double cessAmt = Double.parseDouble(temp.get(_CESS_AMT_INDEX));
				item.setCess(item.getCess()+cessAmt);
				taxAmount += cessAmt;
			}
		}

		item.setTaxAmount(item.getTaxAmount()+taxAmount);

		return taxAmount;
	}
	
	protected double setTallyInvoiceItem(ExcelError ee, String sheetName, String gstReturn, int index,
			List<String> tallyData, Item item,String gstin){
		
		BigDecimal cgst=BigDecimal.ZERO;
		BigDecimal sgst=BigDecimal.ZERO;
		BigDecimal igst=BigDecimal.ZERO;
		BigDecimal txableValue= new BigDecimal(tallyData.get(_TAXABLE_VALUE_INDEX));
		BigDecimal cess=BigDecimal.ZERO;
		
		if(_CESS_AMT_INDEX!=100)
		if(!StringUtils.isEmpty(tallyData.get(_CESS_AMT_INDEX)))
		 cess= new BigDecimal(tallyData.get(_CESS_AMT_INDEX));

		BigDecimal taxRate=BigDecimal.ZERO;
   item.setTaxableValue(txableValue.doubleValue());
		if(!StringUtils.isEmpty(tallyData.get(_TAX_RATE_INDEX)))
		 taxRate= new BigDecimal(tallyData.get(_TAX_RATE_INDEX));
		item.setTaxRate(taxRate.doubleValue());
		BigDecimal gstTaxes=txableValue.multiply(taxRate).divide(new BigDecimal(100));
		
	if(!ee.isIgst()){
		cgst=sgst=gstTaxes.divide(new BigDecimal(2));
	}
	else
		igst=gstTaxes;
	
	item.setCess(item.getCess()+cess.doubleValue());
	item.setCgst(item.getCgst()+cgst.doubleValue());
	item.setSgst(item.getSgst()+sgst.doubleValue());
	item.setIgst(item.getIgst()+igst.doubleValue());
	item.setTaxAmount(gstTaxes.add(cess).doubleValue());
	
	
	return gstTaxes.add(cess).doubleValue();
	}

	public int _TEMP_TALLY_POS=100;
	protected List<String> processTallyExcel(List<String> tallyData,String gstin,String sheetName,ExcelError ee){
		
		if(!StringUtils.isEmpty(tallyData.get(_TEMP_TALLY_POS)))
			tallyData.add(tallyData.get(_TEMP_TALLY_POS).split("-")[0]);
	return tallyData;
	}
	protected JsonNode saveCompositTransaction(List<B2BTransactionEntity> b2bTransactionEntities,
			List<EXPTransactionEntity> expTransactionEntities, List<B2CSTransactionEntity> b2csTransactionEntity,
			List<B2CLTransactionEntity> b2clTransactionEntities, ExcelError ee, B2BTransaction b2bTransaction,
			B2CLTransaction b2clTransaction, EXPTransaction expTransaction, B2CSTransaction b2csTransaction,
			UserBean userBean, String gstn, String monthYear, ReturnType returnType, String delete) throws Exception {

		ObjectNode responseNode = new ObjectMapper().createObjectNode();

		if (ee.isFinalError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		} else {

			int updateCount = 0;
			int totalCount = 0;
			if (!b2bTransactionEntities.isEmpty()) {
				this.deleteTransaction(gstn, monthYear, TransactionType.B2B, returnType, delete, DataSource.EMGST);
				updateCount = b2bTransaction.saveTransaction(b2bTransactionEntities,null);
				totalCount += updateCount;
				responseNode.put("b2bMsg", updateCount + " B2B updated successfully");
			}

			if (!b2clTransactionEntities.isEmpty()) {
				this.converB2clToB2cs(b2clTransactionEntities, b2csTransactionEntity, userBean);
				if (!b2clTransactionEntities.isEmpty()) {
					this.deleteTransaction(gstn, monthYear, TransactionType.B2CL, returnType, delete, DataSource.EMGST);
					updateCount = b2clTransaction.saveTransaction(b2clTransactionEntities);
					responseNode.put("b2clMsg", updateCount + " B2CL updated successfully");
					totalCount += updateCount;
				}

			}

			if (!b2csTransactionEntity.isEmpty()) {
				this.deleteTransaction(gstn, monthYear, TransactionType.B2CS, returnType, delete, DataSource.EMGST);
				updateCount = b2csTransaction.saveTransaction(b2csTransactionEntity,null);
				responseNode.put("b2csMsg", updateCount + " B2CS updated successfully");
				totalCount += updateCount;

			}

			if (!expTransactionEntities.isEmpty()) {
				this.deleteTransaction(gstn, monthYear, TransactionType.EXP, returnType, delete, DataSource.EMGST);
				updateCount = expTransaction.saveExpTransaction(expTransactionEntities);
				responseNode.put("expMsg", updateCount + " EXP updated successfully");
				totalCount += updateCount;

			}

			responseNode.put("totalCount", totalCount);

		}

		return responseNode;
	}

	private void converB2clToB2cs(List<B2CLTransactionEntity> b2cls,
			List<B2CSTransactionEntity> b2csTransactionEntities, UserBean userBean) {

		List<B2CLTransactionEntity> removalB2CLTrans = new ArrayList<>();
		for (B2CLTransactionEntity b2clTransactionEntity : b2cls) {
			List<B2CLDetailEntity> b2clDetailEntities = b2clTransactionEntity.getB2clDetails();
			List<B2CLDetailEntity> removalB2CL = new ArrayList<>();
			for (B2CLDetailEntity b2cl : b2clDetailEntities) {
				if (b2cl.getTaxableValue() + b2cl.getTaxAmount() < 250000) {

					B2CSTransactionEntity b2csTransactionEntity2 = new B2CSTransactionEntity();

					b2csTransactionEntity2.setMonthYear(b2clTransactionEntity.getMonthYear());

					b2csTransactionEntity2.setSource(b2cl.getSource());
					b2csTransactionEntity2.setSourceId(b2cl.getSourceId());

					b2csTransactionEntity2.setGstin(b2clTransactionEntity.getGstin());

					b2csTransactionEntity2.setReturnType(b2clTransactionEntity.getReturnType());

					InfoService.setInfo(b2csTransactionEntity2, userBean);
					b2csTransactionEntity2.setPos(b2cl.getPos());
					b2csTransactionEntity2.setStateName(b2cl.getStateName());

					b2csTransactionEntity2.setSupplyType(SupplyType.INTER);

					b2csTransactionEntity2.setEtin(b2cl.getEtin());

					b2csTransactionEntity2.setItems(b2cl.getItems());

					b2csTransactionEntity2.setTaxableValue(b2cl.getTaxableValue());
					b2csTransactionEntity2.setTaxAmount(b2cl.getTaxAmount());

					if (StringUtils.isNotEmpty(b2csTransactionEntity2.getEtin()))
						b2csTransactionEntity2.setB2csType("E");
					else
						b2csTransactionEntity2.setB2csType("OE");
					b2csTransactionEntities.add(b2csTransactionEntity2);
					removalB2CL.add(b2cl);
				}
			}
			b2clDetailEntities.removeAll(removalB2CL);
			if (b2clDetailEntities.isEmpty() || b2clDetailEntities.size() < 1)
				removalB2CLTrans.add(b2clTransactionEntity);
		}
		b2cls.removeAll(removalB2CLTrans);
	}
	
	
	

}
