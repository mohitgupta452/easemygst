package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNURTransactionEntity;

public class CDNURService extends ItemService{
	
	public int insertUpdateCDNTransaction(List<CDNURTransactionEntity> cdnTransactions,ReturnType returnType,String delete) throws SQLException
	{	      int updateCount=0;
		if(cdnTransactions != null && !cdnTransactions.isEmpty()){

			_Logger.info("Inserting cdnur records for gstin {}",cdnTransactions.get(0).getGstin().getGstin());
		
		
		String cdnTransactionSelect="select id from cdnur_transaction where gstin=? and monthYear=? and returnType=?";
		
		String cdnurExistForDelete="select bd.isSynced,bd.id from cdnur_transaction bt,cdnur_details bd where bt.gstin=? and bt.returnType=? and bt.monthYear=? and bt.id=bd.cdnUrTransactionId and bd.isDelete=0";
		
		String updateDelFlag="update cdnur_details set isDelete=1,flags='DELETE' where id=?";
		
		String deleteIfNotSynced="delete from cdnur_details where id=?";
		
		String deleteItems="DELETE a from items a,cdnur_details c,cdnur_transaction t JOIN(SELECT invoiceNumber,id from cdnur_details GROUP by invoiceNumber,id HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and c.isDelete=1 and c.cdnUrTransactionId=t.id and c.isSynced=1 and t.gstin=? and t.returnType=? and t.monthYear=?";
		
		String cdnurDeleteDuplicate="DELETE a from cdnur_transaction bt,cdnur_details a JOIN(SELECT invoiceNumber from cdnur_details GROUP by invoiceNumber HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.isDelete=1 and a.isSynced=1 and bt.gstin=? and bt.returnType=? and bt.monthYear=?";
		
		if(returnType==ReturnType.GSTR2)
			cdnTransactionSelect="select id from cdnur_transaction where gstin=? and monthYear=? and returnType=? and originalCtin=?";
		
		String cdnTransactionInsertionQry="INSERT INTO `cdnur_transaction` (`id`, `gstin`, `returnType`,`monthYear`, `creationTime`,"
				+ " `createdBy`,`creationIpAddress`, `updationTime`, `updatedBy`, `updationIpAddress`, `originalCtin`, `originalCtinName`) "
				+ "VALUES (?,?,?,?, CURRENT_TIMESTAMP,?,?, NULL, NULL, NULL, ?,?)\n";
		
	      String sql ="{call cdnUrInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	      
	      int count =0;
	      
		try(	Connection connection=	DBUtils.getConnection();
				CallableStatement itemCS = connection.prepareCall(itemInsertSql);
			PreparedStatement cdnTransactionPS =connection.prepareStatement(cdnTransactionInsertionQry);
				
			PreparedStatement cdnurDelExist=connection.prepareStatement(cdnurExistForDelete);
				
			PreparedStatement cdnurDelDuplicate=connection.prepareStatement(cdnurDeleteDuplicate);
				
			PreparedStatement cdnurDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
				
			PreparedStatement cdnurUpdateFlag=connection.prepareStatement(updateDelFlag);
				
			PreparedStatement itemdelete=connection.prepareStatement(deleteItems);	


		CallableStatement  cdnDetailsCS =connection.prepareCall(sql);//connection.prepareStatement(insertQueryB2bDetails);
		
		PreparedStatement cdnExist=connection.prepareStatement(cdnTransactionSelect);
				Connection delItemconn = DBUtils.getConnection();

		){
			
			connection.setAutoCommit(false);
			delItemconn.setAutoCommit(false);
      boolean update =false;
      
      if ("YES".equalsIgnoreCase(delete)) {
			// for marking delete flag
			cdnurDelExist.setInt(1, cdnTransactions.get(0).getGstin().getId());
			cdnurDelExist.setString(2, cdnTransactions.get(0).getReturnType());
			cdnurDelExist.setString(3, cdnTransactions.get(0).getMonthYear());

			ResultSet deleteFlagResult = cdnurDelExist.executeQuery();

			int updateFlagcount = 0;
			while (deleteFlagResult.next()) {
				if (deleteFlagResult.getBoolean(1)) {
					cdnurUpdateFlag.setString(1, deleteFlagResult.getString(2));
					updateFlagcount += cdnurUpdateFlag.executeUpdate();
				} else {
					cdnurDelNotSynced.setString(1, deleteFlagResult.getString(2));
					updateFlagcount += cdnurDelNotSynced.executeUpdate();
				}
			}
			DBUtils.closeQuietly(deleteFlagResult);
		}
		for(CDNURTransactionEntity cdnTransaction : cdnTransactions){
			
			cdnExist.setInt(1,cdnTransaction.getGstin().getId());
			cdnExist.setString(2,cdnTransaction.getMonthYear());
			cdnExist.setString(3,cdnTransaction.getReturnType());
			if(returnType==ReturnType.GSTR2)
			cdnExist.setString(4,cdnTransaction.getOriginalCtin());
			ResultSet resultSet=cdnExist.executeQuery();
			
			String id=null;
			if(resultSet.next())
			{
			 id= resultSet.getString(1);
			 if(!"YES".equalsIgnoreCase(delete))
			  update=true;
			}
			else
			{
				id=UUID.randomUUID().toString();
				
				cdnTransactionPS.setString(1,id);
				cdnTransactionPS.setInt(2,cdnTransaction.getGstin().getId());
				cdnTransactionPS.setString(3,cdnTransaction.getReturnType());
				cdnTransactionPS.setString(4,cdnTransaction.getMonthYear());
				cdnTransactionPS.setString(5,cdnTransaction.getCreatedBy());
				cdnTransactionPS.setString(6,cdnTransaction.getCreationIPAddress());
				cdnTransactionPS.setString(7,cdnTransaction.getOriginalCtin());
				cdnTransactionPS.setString(8,cdnTransaction.getOriginalCtinName());
				cdnTransactionPS.execute();
			}
			DBUtils.closeQuietly(resultSet);
			
			for(CDNURDetailEntity cdnDetail : cdnTransaction.getCdnUrDetails())
			{
				OPERATION operation =OPERATION.INSERT;
				String invoiceId = UUID.randomUUID().toString();
				if (update) {

					String checkDetailSql = "select id,isLocked from cdnur_details where cdnUrTransactionId=? and invoiceNumber=?";

					PreparedStatement checkDetail = connection.prepareStatement(checkDetailSql);

					checkDetail.setString(1, id);
					checkDetail.setString(2, cdnDetail.getInvoiceNumber());

					ResultSet set = checkDetail.executeQuery();

					if (set.next()) {
						if (Objects.nonNull(set.getBoolean(2)) && set.getBoolean(2) && set.getString(1) != null)
							continue;
						invoiceId = set.getString(1);
						operation = OPERATION.UPDATE;
					} 
					DBUtils.closeQuietly(set);
				}
				cdnDetailsCS.setString(1,invoiceId);
				cdnDetailsCS.setString(2,cdnDetail.getInvoiceNumber());
				cdnDetailsCS.setString(3,cdnDetail.getNoteType());
				
				cdnDetailsCS.setTimestamp(4,Timestamp.from(Instant.now()));
				cdnDetailsCS.setString(5,cdnDetail.getCreatedBy());
				cdnDetailsCS.setString(6,cdnDetail.getCreationIPAddress());
				cdnDetailsCS.setString(7,cdnDetail.getUpdatedBy());
				cdnDetailsCS.setString(8,cdnDetail.getUpdationIPAddress());
				cdnDetailsCS.setTimestamp(9,Timestamp.from(Instant.now()));
				cdnDetailsCS.setString(10,id);
				cdnDetailsCS.setString(11,cdnDetail.getSource());
				cdnDetailsCS.setDouble(12,cdnDetail.getTaxAmount());
				cdnDetailsCS.setDouble(13,cdnDetail.getTaxableValue());
				
				cdnDetailsCS.setString(14,cdnDetail.getPreGstRegime());
				
				cdnDetailsCS.setDate(19,new Date(cdnDetail.getRevisedInvDate().getTime()));
				cdnDetailsCS.setString(20,cdnDetail.getInvoiceType());
				cdnDetailsCS.setString(21,operation.toString());
				cdnDetailsCS.setString(15,cdnDetail.getReasonForNote());
				
				cdnDetailsCS.setString(18,cdnDetail.getSourceId());
				
				//cdnDetailsCS.setString(14,cdnDetail.getRevisedCtin());
				//cdnDetailsCS.setString(15,cdnDetail.getRevisedCtinName());
				cdnDetailsCS.setString(16,cdnDetail.getRevisedInvNo());
				cdnDetailsCS.setDate(17,new Date(cdnDetail.getInvoiceDate().getTime()));
				
				//ammendment
				cdnDetailsCS.setBoolean(22,cdnDetail.getIsAmmendment());
				cdnDetailsCS.setString(23,cdnDetail.getOriginalNoteNumber());
				if(!Objects.isNull(cdnDetail.getOriginalNoteDate()))
				cdnDetailsCS.setDate(24,new Date(cdnDetail.getOriginalNoteDate().getTime()));
				else
					cdnDetailsCS.setDate(24, null);
				
				
				cdnDetailsCS.addBatch();
				
				this.insertItems(cdnDetail.getItems(), itemCS,operation,invoiceId);

				if(++count % 30==0 ) {
					updateCount+=cdnDetailsCS.executeBatch().length;
					itemCS.executeBatch();

				}
			}
			
			updateCount+=cdnDetailsCS.executeBatch().length;
			itemCS.executeBatch();
			
			update=Boolean.FALSE;
		}
		
		if("YES".equalsIgnoreCase(delete)){
			itemdelete.setString(1, cdnTransactions.get(0).getGstin().getId().toString());
			itemdelete.setString(2, cdnTransactions.get(0).getReturnType());
			itemdelete.setString(3, cdnTransactions.get(0).getMonthYear());
			itemdelete.executeUpdate();
			cdnurDelDuplicate.setString(1,cdnTransactions.get(0).getGstin().getId().toString());
			cdnurDelDuplicate.setString(2, cdnTransactions.get(0).getReturnType());
			cdnurDelDuplicate.setString(3, cdnTransactions.get(0).getMonthYear());
			cdnurDelDuplicate.executeUpdate();
	    }
		
		connection.commit();
		
	        }
		catch(Exception exception)
		{
			updateCount=0;
			throw exception;
		}
		_Logger.info("cdnur insertion end for gstin {} , records {} updated",cdnTransactions.get(0).getGstin().getGstin(),updateCount);

		}

		return updateCount;
	}

}
