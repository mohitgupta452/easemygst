package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;

public class ATService extends ItemService {

	public int insertUpdate(List<ATTransactionEntity> atTransactionEntities, String delete) throws SQLException {
		int updateCount = 0;

		if (atTransactionEntities != null && !atTransactionEntities.isEmpty()) {
			_Logger.info("AT insertion start for gstin {}", atTransactionEntities.get(0).getGstin().getGstin());

			String sql = "{call atInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

			String atExistForDelete = "select bd.isSynced,bd.id from at_transaction bd where bd.gstin=? and bd.returnType=? and bd.monthYear=?  and bd.isDelete=0";

			String updateDelFlag = "update at_transaction set isDelete=1,flags='DELETE' where id=?";

			String deleteIfNotSynced = "delete from at_transaction where id=?";

			String atDeleteDuplicate = "DELETE a from at_transaction a JOIN(SELECT pos from at_transaction where isDelete=1 GROUP by pos HAVING COUNT(*)>1) b WHERE  a.isSynced=1 and a.gstin=? and a.returnType=? and a.monthYear=? and a.pos=?";
			int count = 0;

			try (Connection connection = DBUtils.getConnection();
					CallableStatement itemCS = connection.prepareCall(itemInsertSql);

					PreparedStatement atDelNotSynced = connection.prepareStatement(deleteIfNotSynced);

					PreparedStatement atUpdateFlag = connection.prepareStatement(updateDelFlag);

					CallableStatement atCS = connection.prepareCall(sql);// connection.prepareStatement(insertQueryB2bDetails);
			) {
				connection.setAutoCommit(false);

				if ("YES".equalsIgnoreCase(delete)) {
					try(Connection delConn=DBUtils.getConnection();
					PreparedStatement atDelExist = delConn.prepareStatement(atExistForDelete);
							){
						delConn.setAutoCommit(false);
					atDelExist.setString(1, atTransactionEntities.get(0).getGstin().getId().toString());
					atDelExist.setString(2, atTransactionEntities.get(0).getReturnType());
					atDelExist.setString(3, atTransactionEntities.get(0).getMonthYear());

					ResultSet deleteFlagResult = atDelExist.executeQuery();

					int updateFlagcount = 0;
					while (deleteFlagResult.next()) {
						if (deleteFlagResult.getBoolean(1)) {
							atUpdateFlag.setString(1, deleteFlagResult.getString(2));
							updateFlagcount += atUpdateFlag.executeUpdate();
						} else {
							atDelNotSynced.setString(1, deleteFlagResult.getString(2));
							updateFlagcount += atDelNotSynced.executeUpdate();
						}
					}
                     delConn.commit();
					DBUtils.closeQuietly(deleteFlagResult);
					DBUtils.closeQuietly(delConn);
				}
			}
				Set<String> posList = new HashSet<>();
				for (ATTransactionEntity atTransaction : atTransactionEntities) {

					String invoiceId = UUID.randomUUID().toString();
					posList.add(atTransaction.getPos());

					atCS.setString(1, invoiceId);
					atCS.setInt(2, atTransaction.getGstin().getId());
					atCS.setString(3, atTransaction.getReturnType());
					atCS.setString(4, atTransaction.getMonthYear());
					atCS.setString(5, atTransaction.getAmmendmentMonthYear());
					atCS.setString(6, atTransaction.getPos());
					atCS.setString(7, atTransaction.getStateName());
					atCS.setString(8, atTransaction.getSupplyType().toString());
					atCS.setString(9, atTransaction.getSource());
					atCS.setString(10, atTransaction.getSourceId());
					atCS.setString(11, atTransaction.getType());
					atCS.setDouble(12, atTransaction.getAdvanceReceived());
					atCS.setString(13, atTransaction.getCreatedBy());
					atCS.setTimestamp(14, Timestamp.from(Instant.now()));
					atCS.setString(15, atTransaction.getCreationIPAddress());
					atCS.setString(16, atTransaction.getUpdatedBy());
					atCS.setTimestamp(17, Timestamp.from(Instant.now()));
					atCS.setString(18, atTransaction.getUpdationIPAddress());
					atCS.setDouble(19, atTransaction.getTaxAmount());
					// ammendment
					atCS.setBoolean(20, atTransaction.getIsAmmendment());
					atCS.setString(21, atTransaction.getOriginalMonth());
					/*
					 * atCS.setString(22, atTransaction.getOriginalPos());
					 * atCS.setString(23,atTransaction.getOriginalSupplyType());
					 */

					atCS.addBatch();

					this.insertItems(atTransaction.getItems(), itemCS, OPERATION.INSERT, invoiceId);

					if (++count % 100 == 0) {
						updateCount += atCS.executeBatch().length;
						itemCS.executeBatch();
					}
				}
				updateCount += atCS.executeBatch().length;
				itemCS.executeBatch();
				connection.commit();


				if ("YES".equalsIgnoreCase(delete)) {
					try(Connection delConn=DBUtils.getConnection();
					PreparedStatement atDelDuplicate = delConn.prepareStatement(atDeleteDuplicate);
							){
					delConn.setAutoCommit(false);
					for (String pos : posList) {
						atDelDuplicate.setString(1, atTransactionEntities.get(0).getGstin().getId().toString());
						atDelDuplicate.setString(2, atTransactionEntities.get(0).getReturnType());
						atDelDuplicate.setString(3, atTransactionEntities.get(0).getMonthYear());
						atDelDuplicate.setString(4, pos);
						atDelDuplicate.addBatch();
					}
					atDelDuplicate.executeBatch();
					delConn.commit();
					DBUtils.closeQuietly(atDelDuplicate);
					DBUtils.closeQuietly(delConn);
					}
				}
				//
				_Logger.info("AT insertion end for gstin {}, records {} updated",
						atTransactionEntities.get(0).getGstin().getGstin(), updateCount);

			} catch (Exception e) {
				_Logger.error("exception while inserting AT records for gstin {}",
						atTransactionEntities.get(0).getGstin().getGstin(), e);
				updateCount = 0;
				throw e;
			}
		}
		return updateCount;
	}

}
