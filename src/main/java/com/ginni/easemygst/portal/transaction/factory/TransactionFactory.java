package com.ginni.easemygst.portal.transaction.factory;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.transaction.impl.ALLTransaction;
import com.ginni.easemygst.portal.transaction.impl.ATTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2BURTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2CLTransaction;
import com.ginni.easemygst.portal.transaction.impl.B2CSTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNURTransaction;
import com.ginni.easemygst.portal.transaction.impl.DocIssueTransaction;
import com.ginni.easemygst.portal.transaction.impl.EXPTransaction;
import com.ginni.easemygst.portal.transaction.impl.HSNTransaction;
import com.ginni.easemygst.portal.transaction.impl.IMPGTransaction;
import com.ginni.easemygst.portal.transaction.impl.IMPSTransaction;
import com.ginni.easemygst.portal.transaction.impl.NILTransaction;
import com.ginni.easemygst.portal.transaction.impl.TXPDTransaction;
import com.ginni.easemygst.portal.utils.GstExcel;

public class TransactionFactory implements Transaction {
	

	private Transaction transaction;

	@Inject
	private Logger log;

	public TransactionFactory(String type) {
		if (type.equalsIgnoreCase(TransactionType.AT.toString())) {
			this.transaction = new ATTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.B2B.toString())) {
			System.out.println("Inside Factory of B2B");
			this.transaction = new B2BTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.B2CL.toString())) {
			this.transaction = new B2CLTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.B2CS.toString())) {
			System.out.println("Inside Factory of B2CS");
			this.transaction = new B2CSTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.CDN.toString()) ||
				type.equalsIgnoreCase("CDNR")){
			this.transaction = new CDNTransaction();
	} else if (type.equalsIgnoreCase(TransactionType.EXP.toString())) {
			this.transaction = new EXPTransaction();
		} 
	else if (type.equalsIgnoreCase(TransactionType.CDNUR.toString())) {
		this.transaction = new CDNURTransaction();
	   }
		else if (type.equalsIgnoreCase(TransactionType.HSNSUM.toString())) {
			this.transaction = new HSNTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.TXPD.toString())) {
			this.transaction = new TXPDTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.NIL.toString())) {
			this.transaction = new NILTransaction();
		} else if (type.equalsIgnoreCase(TransactionType.IMPG.toString())) {
			this.transaction = new IMPGTransaction();
		}else if (type.equalsIgnoreCase(TransactionType.IMPS.toString())) {
			this.transaction = new IMPSTransaction();
		}
		else if (type.equalsIgnoreCase(TransactionType.B2BUR.toString())) {
			this.transaction = new B2BURTransaction();
			
		}else if (type.equalsIgnoreCase(TransactionType.DOC_ISSUE.toString()) || 
				type.equalsIgnoreCase("DOCISSUED")  ) {
			this.transaction = new DocIssueTransaction();
		}
		
		else if (type.equalsIgnoreCase("ALL")) {
			this.transaction = new ALLTransaction();
		}
		
	}

	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException {
		return this.transaction.processExcelList(gstExcel, gstReturn, headerIndex, taxpayerGstin, sheetName,
				monthYear,userBean,source,delete);
	}
	
	@Override
	public int processCsvData(Scanner csvFile, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,String delete,SourceType source) throws AppException {
		return this.transaction.processCsvData(csvFile, gstReturn, headerIndex, taxpayerGstin, sheetName, monthYear, userBean,delete,source);
	}
	
	@Override
	public void deleteTransaction(String gstn,String monthYear, TransactionType transaction,
			ReturnType returnType,String delete,DataSource dataSource) throws AppException,Exception{
		 this.transaction.deleteTransaction( gstn,monthYear,transaction,
				returnType, delete,dataSource);
	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction taxpayerAction)
			throws AppException {
		return this.transaction.processTransactionData(transactions, existingData, taxpayerAction);
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		return this.transaction.fetchTransactionData(object, key);
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		return this.transaction.processFetchedData(finalObj, object, key, isFiled);
	}

	@Override
	public String compareTransactions(String supplier, String receiver, ViewType1 viewType) throws AppException {
		return this.transaction.compareTransactions(supplier, receiver, viewType);
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		return this.transaction.validateData(object, monthYear);
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.transaction.getErrorData(datas, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return this.transaction.getCompleteData(datas, returnType);
	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		return this.transaction.getTransactionsData(input);
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		return this.transaction.getReconciledTransactionsData(input, subType);
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		return this.transaction.readDataFromGcc(data);
	}

	@Override
	public EmailDTO getEmailContent(EmailDTO emailDTO,Object primaryInvoice,Object secondaryInvoice) throws AppException {
		return this.transaction.getEmailContent(emailDTO,primaryInvoice,secondaryInvoice);
	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		return this.transaction.getTransactionSummary(gstrSummaryDTO);
	}

	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {
		return this.transaction.getExceptionData(source, changed, returnType);
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		return this.transaction.saveGstr1TransactionData(object, gstr1, transitId);
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		return this.transaction.saveGstr2TransactionData(object, gstr2, transitId);
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		return this.transaction.saveGstr1aTransactionData(object, gstr1a, transitId);
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		return this.transaction.processTransitData(object, transitId, gstr1ReturnStatusResp);
	}
	
	
	
	public Object saveTransactionData(GstinTransactionDTO gstinTransactionDTO,  TaxpayerAction action) throws AppException{
		return this.transaction.saveTransactionData(gstinTransactionDTO, action);

	}
	
	@Override
	public int processCsvDataConsolidated( String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,String delete,SourceType source,List<UploadedCsv>lineItems) throws AppException {
		return this.transaction.processCsvDataConsolidated(gstReturn, headerIndex, taxpayerGstin, sheetName, monthYear, userBean,delete,source,lineItems);
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin, String sheetName,
			String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		return this.transaction.processList(gstExcel,transaction,gstReturn,taxpayerGstin, sheetName,
				monthYear,source);
	}


}
