package com.ginni.easemygst.portal.transaction.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;

import org.apache.commons.lang3.StringUtils;

import com.ginni.easemygst.portal.business.dto.*;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.VendorDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.State;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.Vendor;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.transaction.crud.ATService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class ALLTransaction implements Transaction {

	int _BUSINESS_NAME_INDEX = 0;
	int _GSTIN_INDEX = 1;
	int _CONTACT_PERSON_INDEX = 2;
	int _EMAIL_ADDRESS_INDEX = 3;
	int _MOBILE_NUMBER_INDEX = 4;
	int _WEBSITE_INDEX = 5;
	int _ORGANISATION_INDEX = 6;
	int _PAN_NUMBER_INDEX = 7;
	int _ADDRESS_INDEX = 8;
	int _PINCODE_INDEX = 9;
	int _STORE_TYPE_INDEX = 10;
	int _GST_TYPE_INDEX = 11;

	final Pattern pattern1 = Pattern
			.compile("[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}");

	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, SourceType source,String delete) throws AppException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean,String delete,SourceType source) throws AppException {

		String gstin = taxpayerGstin.getGstin();

		int rowNum = 0;

		CSVParser csvParser = new CSVParser();

		boolean isErrorFlag = false;
		List<Vendor> vendors = new ArrayList<>();
		StringBuilder sb = new StringBuilder();

		int valuesCount = 0;

		boolean skipHeader = false;
		try {
			while (scanner.hasNext()) {
				List<String> values = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					if (values == null)
						break;
					// convert list to at object
					rowNum++;

					if (!Objects.isNull(values)) {
						boolean isError = false;

						Vendor vendor = new Vendor();
						vendor.setTaxpayer(taxpayerGstin.getTaxpayer());
						vendor.setBusinessName(values.get(_BUSINESS_NAME_INDEX));
						vendor.setType("supplier");
						vendor.setGstin(values.get(_GSTIN_INDEX));
						vendor.setContactPerson(values.get(_CONTACT_PERSON_INDEX));
						vendor.setEmailAddress(values.get(_EMAIL_ADDRESS_INDEX));
						vendor.setMobileNumber(values.get(_MOBILE_NUMBER_INDEX));
						vendor.setWebsite(values.get(_WEBSITE_INDEX));
						vendor.setOrganisation(values.get(_ORGANISATION_INDEX));
						vendor.setPanNumber(values.get(_PAN_NUMBER_INDEX));
						vendor.setAddress(values.get(_ADDRESS_INDEX));
						vendor.setPincode(values.get(_PINCODE_INDEX));
						vendor.setClassification(values.get(_STORE_TYPE_INDEX));
						vendor.setGstType(values.get(_GST_TYPE_INDEX));
						vendor.setActive((byte) 1);
						vendor.setEmail((byte) 0);
						
						String uniqueValue = "NAME - "+values.get(_BUSINESS_NAME_INDEX);

						boolean isGstin = false;
						if (!StringUtils.isEmpty(vendor.getGstin())) {
							if (!pattern1.matcher(vendor.getGstin()).matches()) {
								isError = true;
								sb.append("data," + (_GSTIN_INDEX + 1) + "," + rowNum
										+ ","+uniqueValue+" - Invalid GSTIN Number, it should be exactly 15 characters|");
							} else {
								try {
									isGstin = true;
								} catch (NumberFormatException e) {
									isError = true;
									sb.append("data," + (_GSTIN_INDEX + 1) + "," + rowNum
											+ ","+uniqueValue+" - Invalid GSTIN Number, incorrect format found|");
								}
							}
						}

						if (!StringUtils.isEmpty(vendor.getPanNumber())) {
							String panno = "";
							if (isGstin)
								panno = vendor.getGstin().substring(2, 12);
							if (vendor.getPanNumber().length() != 10) {
								isError = true;
								sb.append("data," + (_PAN_NUMBER_INDEX + 1) + "," + rowNum
										+ ","+uniqueValue+" - Invalid PAN Number, it should be exactly 10 characters|");
							} else if (isGstin && !vendor.getPanNumber().equalsIgnoreCase(panno)) {
								isError = true;
								sb.append("data," + (_PAN_NUMBER_INDEX + 1) + "," + rowNum
										+ ","+uniqueValue+" - Invalid PAN Number, not matching with the given GSTIN|");
							}
						}

						/*if (!StringUtils.isEmpty(vendor.getPincode())) {
							if (vendor.getPincode().length() != 6) {
								isError = true;
								sb.append("data," + (_PINCODE_INDEX + 1) + "," + rowNum
										+ ","+uniqueValue+" - Invalid Pin Code, it should be exactly 6 digits|");
							}
						}*/

						if (!StringUtils.isEmpty(vendor.getEmailAddress())) {
							if (!Utility.isValidEmailAddress(vendor.getEmailAddress())) {
								isError = true;
								sb.append("data," + (_EMAIL_ADDRESS_INDEX + 1) + "," + rowNum
										+ ","+uniqueValue+" - Invalid Email Address found, please enter valida email address|");
							}
						}

						/*
						 * boolean dflag = validateVendor(vendor); if (!dflag) {
						 * isError = true; sb.append("data,-," + rowNum +
						 * ",Either of Business Name / Contact Person / Email Address is missing|"
						 * ); }
						 */

						if (!isError) {
							vendor.setCreationTime(new Timestamp(Instant.now().toEpochMilli()));
							vendors.add(vendor);
							valuesCount++;
						} else {
							isErrorFlag = isErrorFlag || isError;
						}
					}
				}
			}
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		scanner.close();

		if (isErrorFlag) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(sb.toString());
			throw exp;
		}

		String selectVendor = "select id from vendor where taxpayerId = ? and gstin = ?";

		String inserQueryVendors = " INSERT INTO `vendor` (`taxpayerId`, `businessName`, `type`, `gstin`, `contactPerson`,"
				+ " `emailAddress`, `mobileNumber`, `website`, `organisation`, `panNumber`, `classification`, `gstType`, `active`,`creationTime`) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";

		try (

			Connection connection = DBUtils.getConnection();
				PreparedStatement dataStatement = connection.prepareStatement(selectVendor);
				PreparedStatement insertStatement = connection.prepareStatement(inserQueryVendors);
				){

			for(Vendor vendor :vendors){
				try {
					dataStatement.setInt(1, vendor.getTaxpayer().getId());
					dataStatement.setString(2, vendor.getGstin());
					ResultSet rs = dataStatement.executeQuery();
					if (rs.next()) {

					} else {
						insertStatement.setInt(1, vendor.getTaxpayer().getId());
						insertStatement.setString(2, vendor.getBusinessName());
						insertStatement.setString(3, !StringUtils.isEmpty(vendor.getGstType()) && vendor.getGstType().equalsIgnoreCase("Vendor") ? "supplier" : "buyer");
						insertStatement.setString(4, vendor.getGstin());
						insertStatement.setString(5, vendor.getContactPerson());
						insertStatement.setString(6, vendor.getEmailAddress());
						insertStatement.setString(7, vendor.getMobileNumber());
						insertStatement.setString(8, vendor.getWebsite());
						insertStatement.setString(9, vendor.getOrganisation());
						insertStatement.setString(10, vendor.getPanNumber());
						insertStatement.setString(11, vendor.getClassification());
						insertStatement.setString(12, vendor.getGstType());
						insertStatement.setByte(13, (byte) 1);
						insertStatement.executeUpdate();
					}
					DBUtils.closeQuietly(rs);
				} catch (Exception e) {

				} finally {

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return valuesCount;

	}
	
	
	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		return 0;
	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction taxpayerAction)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String supplier, String receiver, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}
	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteTransaction(String gstn, String monthYear, TransactionType transaction, ReturnType returnType,
			String delete,DataSource data) throws AppException {
		// TODO Auto-generated method stub

	}

	private boolean validateVendor(Vendor vendor) {

		if (!StringUtils.isEmpty(vendor.getBusinessName()) && !StringUtils.isEmpty(vendor.getType())
				&& !StringUtils.isEmpty(vendor.getContactPerson()) && !StringUtils.isEmpty(vendor.getEmailAddress())) {
			return true;
		}

		return false;
	}

	@Override
	public EmailDTO getEmailContent(EmailDTO emailDTO, Object primaryInvoice, Object secondaryInvoice)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		return 0;
	}

}
