package com.ginni.easemygst.portal.transaction.factory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.GstExcel;

public interface Transaction {
	
	public static final List<String> ignorableFields=Arrays.asList(new String[]{"transit","locked","valid","amendment","gstnSynced","flags"});

	static final int _DEADLOCK_ERR_CODE=1213;
	static final int _MAX_RETRY=3;
	static final int _MAX_RETRY_WAIT_TIME=5000;
	static final int _LOCK_WAIT_TIMEOUT_ERR_CODE=1205;
	
	public enum FilterType {
		EMPTY("EMPTY"), NON_EMPTY("NON_EMPTY"), CONTAINS("CONTAINS"), DOES_NOT_CONTAINS(
				"DOES_NOT_CONTAINS"), START_WITH("START_WITH"), ENDS_WITH(
						"ENDS_WITH"), NULL("NULL"), NOT_NULL("NOT_NULL"), EQUAL("EQUAL"), NOT_EQUAL("NOT_EQUAL"),GREATER_THAN("GREATER_THAN"),LESS_THAN("LESS_THAN");

		private FilterType(String type) {
			this.type = type;
		}

		String type;

		@Override
		public String toString() {
			return this.type;
		}

	}
	
	public enum SourceType {

		EXCEL("EXCEL"), PORTAL("PORTAL"), GSTN("GSTN"), ERP("ERP"), GINESYS("GINESYS"), NAVISION("NAVISION"), SAP("SAP"), TALLY("TALLY");

		SourceType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum Flags {

		AMEND("AMEND"), ERROR("ERROR"), MODIFY("MODIFY"), ACCEPT("ACCEPT"), REJECT("REJECT"), PENDING("PENDING"), DRAFT(
				"DRAFT"), DELETE("DELETE");

		Flags(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum ReconsileType {
		MISSING("MISSING"),
		MISSING_OUTWARD("MISSING_OUTWARD"), // MISSIN_SUPPLIER -
		MISSING_INWARD("MISSING_INWARD"), // MISSIN_BUYER - 
		NEW("NEW"), // 
		MISMATCH("MISMATCH"), MATCH("MATCH"), DRAFT("DRAFT"), ERROR("ERROR"),
		MISMATCH_DATE("MISMATCH_DATE"),MISMATCH_ROUNDOFF_TAXAMOUNT("MISMATCH_ROUNDOFF_TAXAMOUNT"),
		MISMATCH_ROUNDOFF_TAXABLEVALUE("MISMATCH_ROUNDOFF_TAXVALUE"),
		MISMATCH_INVOICENO("MISMATCH_INVOICENO"),
		MISMATCH_MAJOR("MISMATCH_MAJOR"),
		MISSING_INWARD_MISMATCH_INVOICE_NO("MISSING_INWARD_MISMATCH_INVOICE_NO"),
		COPY_DATA("COPY_DATA"),
		COMPARE_TRANSACTION("COMPARE_TRANSACTION"),
		COMPARE_TRANSACTION_CTIN("COMPARE_TRANSACTION_CTIN"),

		MISSING_INWARD_NON_SIMMILAR("MISSING_INWARD_NON_SIMMILAR"),
		MISSING_INWARD_SIMMILAR("MISSING_INWARD_SIMMILAR")
		
		;

		ReconsileType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum ExceptionSheet {

		CHANGES("CHANGES"), ADDED("ADDED"), DELETED("DELETED");

		ExceptionSheet(String sheet) {
			this.sheet = sheet;
		}

		private String sheet;

		@Override
		public String toString() {
			return this.sheet;
		};
	}

	public enum ReconsileSubType {

		OWN("OWN"), MISSING("MISSING"), DATE("DATE"), MAJOR("MAJOR"), ROUNDING_OFF("ROUNDING_OFF"), VALUE_ROUND_OFF(
				"VAL_ROUND_OFF"), TAX_ROUND_OFF("TX_ROUND_OFF"), VALUE("VALUE"), TAX("TAX"), INVOICE_NO(
						"INVOICE_NO"), PENDING_ITC("PENDING_ITC");

		ReconsileSubType(String subType) {
			this.subType = subType;
		}

		private String subType;

		@Override
		public String toString() {
			return this.subType;
		};
	}

	public enum DataFetchType {

		VALID("VALID"), INVALID("INVALID"), ALL("ALL"), EXCEPTION("EXCEPTION");

		DataFetchType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum EcomType {

		INTER("INTER"), INTERA("INTERA");

		EcomType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	public enum ExportType {

		WPAY("WPAY"), WOPAY("WOPAY");

		ExportType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};

		public String getValue() {
			return this.type;
		};
	}

	public enum RegistrationType {

		REGD("REGD"), UNREGD("UNREGD");
		RegistrationType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};

		public String getValue() {
			return this.type;
		};
	}

	public enum NilSupplyType {
		INTR_REGISTERED("INTR_REGISTERED","INTRB2B"),INTRA_REGISTERED("INTRA_REGISTERED","INTRAB2B"),
		INTR_UNREGISTERED("INTR_UNREGISTERED","INTRB2C"),INTRA_UNREGISTERED("INTRA_UNREGISTERED","INTRAB2C"),
		INTER("INTER","INTER"), INTRA("INTRA","INTRA");
		
		private final String key;
	    private final String value;
		
		NilSupplyType(String key, String value) {
	        this.key = key;
	        this.value = value;
	    }

	    public String getKey() {
	        return key;
	    }
	    public String getValue() {
	        return value;
	    }

		@Override
		public String toString() {
			return this.key;
		};
	}
	
	public enum InvoiceType {
		R("R","Regular"),SEWP("SEWP","SEZ supplies with payment"),SEWOP("SEWOP","SEZ supplies without payment"),DE("DE","Deemed Exp");
		InvoiceType(String key,String value) {
			this.key = key;
	        this.value = value;
		}

		private final String key;
	    private final String value;
		@Override
		public String toString() {
			return this.key;
		};

		public String getValue() {
			return this.value;
		};
	}
	
	public enum DataSource {

		EMGST("EMGST"),GSTN("GSTN");
		DataSource(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};

		public String getValue() {
			return this.type;
		};
	}
	
	public enum ItcEligibilityType {

		NO("NO"), IP("IP"),CP("CP") ,IS("IS");

		ItcEligibilityType(String type) {
			this.type = type;
		}

		private String type;

		@Override
		public String toString() {
			return this.type;
		};
	}

	DecimalFormat decimalFormat = new DecimalFormat("#0.00");

	String jsonDateFormat = "dd MMM yyyy";

	String jsonDateTimeZone = "IST";

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	SimpleDateFormat sdfLastSync = new SimpleDateFormat("dd MMM yyyy hh:mm a");

	SimpleDateFormat sdfGcc = new SimpleDateFormat("MM/dd/yy");

	int  processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException;
	
	int processList(GstExcel gstExcel,String transaction,String gstReturn,TaxpayerGstin taxpayerGstin,String sheetName,String monthYear,SourceType source) throws AppException;
	
	int processCsvData(Scanner csvFile, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,String delete,SourceType source) throws AppException;
	
	String processTransactionData(String transactions, String existingData, TaxpayerAction taxpayerAction)
			throws AppException;

	String fetchTransactionData(String object, String key) throws AppException;

	String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException;

	String compareTransactions(String supplier, String receiver, ViewType1 viewType) throws AppException;

	String getTransactionsData(String input) throws AppException;

	String getReconciledTransactionsData(String input, String subType) throws AppException;

	Object readDataFromGcc(String data) throws AppException;

	EmailDTO getEmailContent(EmailDTO emailDTO,Object primaryInvoice,Object secondaryInvoice) throws AppException;

	Object validateData(Object object, String monthYear) throws AppException;

	Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException;

	List<List<String>> getErrorData(List datas, String returnType) throws AppException;

	List<List<String>> getCompleteData(List datas, String returnType) throws AppException;

	Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException;

	String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException;

	String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException;

	String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException;

	String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException;
	
	public <T> T saveTransactionData(GstinTransactionDTO gstinTransactionDTO,  TaxpayerAction action) throws AppException;

	public void deleteTransaction(String gstn,String monthYear, TransactionType transaction,
			ReturnType returnType,String delete,DataSource dataSource) throws AppException,Exception;

	public int processCsvDataConsolidated( String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,String delete,SourceType source,List<UploadedCsv>lineItems) throws AppException; 

}
