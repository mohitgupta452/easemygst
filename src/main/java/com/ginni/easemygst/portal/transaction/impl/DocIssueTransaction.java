package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.jfree.util.Log;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.crud.ATService;
import com.ginni.easemygst.portal.transaction.crud.DocIssueService;
import com.ginni.easemygst.portal.transaction.crud.HSNService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class DocIssueTransaction extends TransactionUtil implements Transaction {

	private FinderService finderService;

	int _NATURE_OF_DOCUMENT=0;
	int _SERIAL_NO_FROM=1;
	int _SERIAL_NO_TO=2;
	int _TOTAL_NO=3;
	int _CANCELLED=4;
	
	
	public DocIssueTransaction() {
		super();
		 	 _NATURE_OF_DOCUMENT=0;
		 	 _SERIAL_NO_FROM=1;
			 _SERIAL_NO_TO=2;
			 _TOTAL_NO=3;
			 _CANCELLED=4;

	}

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	/* No unique field in case of adding/updating item */
	public static List<AT> addATObject(AT at, List<AT> ats) {

		List<InvoiceItem> invoiceItems = new ArrayList<>();
		List<InvoiceItem> tInvoiceItems = at.getInvoiceItems();
		if (ats.contains(at)) {
			at = ats.get(ats.indexOf(at));
			invoiceItems = at.getInvoiceItems();
			for (InvoiceItem tInvoiceItem : tInvoiceItems) {
				if (!invoiceItems.contains(tInvoiceItem)) {
					if (StringUtils.isEmpty(tInvoiceItem.getId()))
						tInvoiceItem.setId(getRandomUniqueId());
					invoiceItems.add(tInvoiceItem);
				}
			}

		} else {
			if (StringUtils.isEmpty(at.getId()))
				at.setId(getRandomUniqueId());
			for (InvoiceItem tInvoiceItem : tInvoiceItems) {
				if (StringUtils.isEmpty(tInvoiceItem.getId()))
					tInvoiceItem.setId(getRandomUniqueId());
			}
			ats.add(at);
		}

		return ats;
	}

	
	public static List<AT> updateATObject(AT at, List<AT> ats) {
		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = at.getInvoiceItems();
		AT tempAt = at;
		String id = at.getId();
		List<AT> searchedAts = ats.stream().filter(a -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());
		if (searchedAts.isEmpty()) {// to check if the gstin exist according to
									// handlle client side case
			if (ats.contains(at)) {
				searchedAts.add(ats.get(ats.indexOf(at)));
				at.setId(searchedAts.get(0).getId());

			}
		}
		if (!searchedAts.isEmpty()) {// update existing at

			AT existingAt = searchedAts.get(0);
			items = existingAt.getInvoiceItems();
			for (InvoiceItem tItem : tItems) {
				List<InvoiceItem> searchedItem = items.stream().filter(b -> b.getId().equalsIgnoreCase(tItem.getId()))
						.collect(Collectors.toList());

				if (!searchedItem.isEmpty()) {// update
					items.remove(searchedItem.get(0));
				}
				if (StringUtils.isEmpty(tItem.getId()))
					tItem.setId(getRandomUniqueId());
				items.add(tItem);// add
				if (at.isGstnSynced())
					at.setTaxPayerAction(TaxpayerAction.MODIFY);
				at.setGstnSynced(false);

			}
			at.setInvoiceItems(items);
			ats.remove(existingAt);
			ats.add(at);
		} else {// add new at
			if (StringUtils.isEmpty(at.getId()))
				at.setId(getRandomUniqueId());
			at.setGstnSynced(false);
			for (InvoiceItem itm : at.getInvoiceItems()) {// genrating ids for
															// items
				if (StringUtils.isEmpty(itm.getId()))
					itm.setId(getRandomUniqueId());

			}
			ats.add(at);
		}

		return ats;
	}
	/* No unique field in case of adding/updating item */

	public static List<AT> updateATObjectByNo(AT at, List<AT> ats) {
		/*
		 * List<InvoiceItem> items = new ArrayList<>(); List<InvoiceItem> tItems
		 * = at.getInvoiceItems(); //String docNum = at.getDocumentNumber();
		 * List<AT> searchedAts = ats.stream() .filter(a ->
		 * !StringUtils.isEmpty(docNum) &&
		 * docNum.equalsIgnoreCase(a.getDocumentNumber()))
		 * .collect(Collectors.toList());
		 * 
		 * if (!searchedAts.isEmpty()) {// update existing at
		 * 
		 * AT existingAt = searchedAts.get(0); //items =
		 * existingAt.getInvoiceItems(); for (InvoiceItem tItem : tItems) {
		 * List<InvoiceItem> searchedItem = items.stream().filter(b ->
		 * b.getId().equalsIgnoreCase(tItem.getId()))
		 * .collect(Collectors.toList());// not decided on which // number item
		 * to be // searched
		 * 
		 * if (!searchedItem.isEmpty()) {// update
		 * items.remove(searchedItem.get(0));
		 * tItem.setId(searchedItem.get(0).getId()); items.add(tItem);
		 * 
		 * } else { if (StringUtils.isEmpty(tItem.getId()))
		 * tItem.setId(getRandomUniqueId()); items.add(tItem); } if
		 * (at.isGstnSynced()) at.setTaxPayerAction(TaxpayerAction.MODIFY);
		 * at.setGstnSynced(false);
		 * 
		 * } at.setInvoiceItems(items); ats.remove(existingAt);
		 * at.setId(existingAt.getId()); ats.add(at); } else {// add new at if
		 * (StringUtils.isEmpty(at.getId())) at.setId(getRandomUniqueId()); if
		 * (at.isGstnSynced()) at.setTaxPayerAction(TaxpayerAction.MODIFY);
		 * at.setGstnSynced(false); for (InvoiceItem itm : at.getInvoiceItems())
		 * {// genrating ids for // items if (StringUtils.isEmpty(itm.getId()))
		 * itm.setId(getRandomUniqueId());
		 * 
		 * } ats.add(at); }
		 */

		return ats;
	}

	public static List<AT> deleteATObject(AT at, List<AT> ats) {

		List<InvoiceItem> items;
		String aId = at.getId();
		List<AT> searchedAt = ats.stream().filter(a -> !StringUtils.isEmpty(aId) && aId.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedAt.isEmpty()) {

			if (action == false) {
				if (searchedAt.get(0).isGstnSynced()) {
					searchedAt.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedAt.get(0).setGstnSynced(false);
				} else {
					action = ats.remove(searchedAt.get(0));

				}
				action = true;

			}
		}

		return ats;
	}

	public static List<AT> changeStatusB2CSObject(AT at, List<AT> ats, TaxpayerAction taxpayerAction) {
		String bId = at.getId();
		List<AT> temp = ats.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equals(b.getId()))
				.collect(Collectors.toList());
		if (!temp.isEmpty()) {
			temp.get(0).setTaxPayerAction(taxpayerAction);
			temp.get(0).setGstnSynced(false);
		}

		return ats;
	}

	public static List<AT> addATObject(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = addATObject(at, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<AT> updateATObject(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = updateATObject(at, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<AT> deleteATObject(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = deleteATObject(at, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<AT> changeStatusATObject(List<AT> existingAts, List<AT> transactionAts,
			TaxpayerAction taxpayerAction) {

		for (AT at : transactionAts) {
			existingAts = changeStatusB2CSObject(at, existingAts, taxpayerAction);
		}

		return existingAts;
	}

	public static List<AT> updateATObjectByNo(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = updateATObjectByNo(at, existingB2bs);
		}

		return existingB2bs;
	}

	@Override
	public int processExcelList(GstExcel gstExcel,String returnType,String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException {

        String gstin=taxpayerGstin.getGstin();
        
        List<DocIssue> docIssues= new ArrayList<>();
		
		setColumnMapping(headerIndex);

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.EXCEL.toString();

		ExcelError ee = new ExcelError();

			int columnCount = 0;
			Integer index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if(Objects.nonNull(values))
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
					if(temp == null)
						break;
					Log.debug(temp);
					// convert list to b2b object
					index++;
					convertListToTransactionData(ee,docIssues,temp, index, sheetName, sourceName, sourceId,
							 taxpayerGstin,userBean,returnType,monthYear);

				}
			
			}
			
			if (ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}
			
			try {
				return new DocIssueService().insert(docIssues,delete);
			} catch (SQLException e) {
                  _Logger.error("sql exception ",e);
				throw new AppException(ExceptionCode._ERROR);
			}
	}
	
	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		return 0;
	}
	
	@Override
	public int processCsvData(Scanner scanner, String gstReturn, 
			String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex);

		ExcelError ee = new ExcelError();
        List<DocIssue> docIssues= new ArrayList<>();


		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					if(temp==null)
						break;
					// convert list to at object
					index++;
					convertListToTransactionData(ee,docIssues,temp, index, sheetName, sourceName, sourceId,
							 taxpayerGstin,userBean,gstReturn,monthYear);				}
			}
			scanner.close();

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new DocIssueService().insert(docIssues,delete);
		} catch (AppException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setColumnMapping(String headerIndex) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			_NATURE_OF_DOCUMENT = Integer.valueOf(headers[0]);
			_SERIAL_NO_FROM = Integer.valueOf(headers[1]);
			_SERIAL_NO_TO = Integer.valueOf(headers[2]);
			_TOTAL_NO = Integer.valueOf(headers[3]);
			_CANCELLED = Integer.valueOf(headers[4]);
			
			

		}

	}

	private void convertListToTransactionData(ExcelError ee,List<DocIssue> docIssues, List<String> temp, Integer index,
			String sheetName, String sourceName, String sourceId, TaxpayerGstin gstin,UserBean userBean,String returnType,String monthYear) {

		ee.setUniqueRowValue("Nature of document - ["+temp.get(_NATURE_OF_DOCUMENT)+"]");
		
		DocIssue docIssue = new DocIssue();
	     
	     docIssue.setSource(sourceName);
	     docIssue.setSourceId(sourceId);
	     
	     InfoService.setInfo(docIssue, userBean);
	  docIssue.setGstin(gstin);
	  docIssue.setMonthYear(monthYear);
	  docIssue.setReturnType(returnType);
	
	   String nod=temp.get(_NATURE_OF_DOCUMENT);
	   this.validateNatureOfDocument(sheetName, _NATURE_OF_DOCUMENT, index, nod, ee);
		if(!ee.isError()){
			Integer nodCode;
			try{
				nodCode=Integer.parseInt(nod);
				docIssue.setCategory(this.docIssueMap.get(nodCode));
			}catch(NumberFormatException e){
				docIssue.setCategory(nod);
			}
			   
		}
	   
	   String snoFrom=	temp.get( _SERIAL_NO_FROM);
	   this.validateDocIssueSnoExcel(sheetName, _SERIAL_NO_FROM, index, snoFrom, ee, true);
		if(!ee.isError()){
			   docIssue.setSnoFrom(snoFrom);

		}
	   
	   String snoTo=temp.get( _SERIAL_NO_TO);
	   this.validateDocIssueSnoExcel(sheetName, _SERIAL_NO_TO, index, snoTo, ee, true);
		if(!ee.isError()){
			   docIssue.setSnoTo(snoTo);
		}
	   
	   String totalNo=temp.get( _TOTAL_NO);
 		this.validateIntegerNumericValue(sheetName, _TOTAL_NO, index, totalNo, "Invalid total number value ", ee);
 		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_TOTAL_NO))){
 		   docIssue.setTotalNumber(Integer.parseInt(totalNo));
 		}
	   
	   String cancelled=temp.get( _CANCELLED);
	   this.validateIntegerNumericValue(sheetName, _CANCELLED, index, totalNo, "Invalid cancelled value ", ee);
		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_CANCELLED))){
		   docIssue.setCanceled(Integer.parseInt(cancelled));
		}
		this.validateTotalNumber(sheetName, _TOTAL_NO, index,docIssue.getCanceled() ,docIssue.getTotalNumber(),  ee);
	       docIssues.add(docIssue);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<AT> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<AT>>() {
				});
				List<AT> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<AT>>() {
				});

				if (action == null) {
					existingB2bs = addATObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateATObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteATObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateATObjectByNo(existingB2bs, transactionB2bs);
				} else if (action != null) {
					existingB2bs = changeStatusATObject(existingB2bs, transactionB2bs, action);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<AT> ats = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<AT>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			List<StateDTO> states = new ArrayList<>();

			try {
				finderService = (FinderService) InitialContext.doLookup("java:comp/env/finder");
				states = finderService.getAllState();
			} catch (NamingException e) {
				e.printStackTrace();
			}

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}
			/*
			 * Calendar cal = Calendar.getInstance(); Date currentDate =
			 * cal.getTime(); int currYear = currentDate.getYear(); int
			 * currMonth = currentDate.getMonth();
			 * 
			 * if (currMonth - 1 == inputMonth.getMonth()) {
			 * cal.set(Calendar.DATE, 1); cal.add(Calendar.DATE, -1);
			 * currentDate = cal.getTime(); }
			 */

			Date startDate = new Date();

			double totalTaxAmount = 0.0;
			double totalAdvanceRcvd = 0.0;
			double amTaxAmount = 0.0;
			double amAdvanceRcvd = 0.0;

			double igstAmount = 0.0;
			double cgstAmount = 0.0;
			double sgstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (AT at : ats) {

				StateDTO temp = new StateDTO();
				temp.setId(Double.valueOf(at.getRecipientStateCode()).intValue());
				int i = states.indexOf(temp);
				if (i > -1) {
					StateDTO stateDTO = states.get(states.indexOf(temp));
					at.setStateName(stateDTO.getName());
				} else {
					at.setStateName("No Name");
				}

				double advncRcvdInv = 0.0;
				double taxAmountInv = 0.0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;

				/*
				 * Date doucmentDate = at.getDocumentDate(); String dateError =
				 * ""; if (inputMonth.getMonth() < 9) { startDate.setDate(1);
				 * startDate.setMonth(3); startDate.setYear(inputMonth.getYear()
				 * - 1); dateError =
				 * "Invalid document date, it should be between " +
				 * sdf.format(startDate) + " and " + sdf.format(inputMonth); }
				 * else { startDate.setDate(1); startDate.setMonth(3);
				 * startDate.setYear(inputMonth.getYear()); dateError =
				 * "Invalid document date, it should be between " +
				 * sdf.format(startDate) + " and " + sdf.format(inputMonth); }
				 * if (!(doucmentDate.compareTo(startDate) >= 0 &&
				 * doucmentDate.compareTo(inputMonth) <= 0)) {
				 * errors.put("DOCUMENT_DATE", dateError); errorFlag = false; }
				 */

				/*
				 * if (!StringUtils.isEmpty(at.getOriginalDocumentNumber()) &&
				 * !StringUtils.isEmpty(at.getOriginalGstin())) {
				 * at.setAmendment(true); } else { at.setAmendment(false); }
				 */

				at.setError(errors);
				at.setValid(errorFlag);
				List<InvoiceItem> items = at.getInvoiceItems();
				if (!Objects.isNull(items)) {
					for (InvoiceItem it : items) {

						if (!Objects.isNull(it.getCgstAmt())) {
							taxAmountInv += it.getCgstAmt();
							cgstAmount = +it.getCgstAmt();
						}
						if (!Objects.isNull(it.getSgstAmt())) {
							taxAmountInv += it.getSgstAmt();
							sgstAmount = +it.getSgstAmt();

						}
						if (!Objects.isNull(it.getIgstAmt())) {
							taxAmountInv += it.getIgstAmt();
							igstAmount = +it.getIgstAmt();

						}
						if (!Objects.isNull(it.getCessAmt())) {
							taxAmountInv += it.getCessAmt();
							cessAmount = +it.getCessAmt();

						}
						if (!Objects.isNull(it.getAdvanceAmount())) {
							advncRcvdInv += it.getAdvanceAmount();
						}
					}
				}
				invoiceCount++;

				setAtFlag(at);

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}
				at.setTaxAmount(taxAmountInv);
				at.setAdvanceRcvd(advncRcvdInv);
				if (at.isAmendment()) {

				}

				if (!at.isValid()) {
					at.setType(ReconsileType.ERROR);
					at.setFlags("");

				}

				// at.setFlags(flags.toString());

				if (at.isAmendment()) {
					amTaxAmount += taxAmountInv;
					amAdvanceRcvd += advncRcvdInv;
				} else {
					totalTaxAmount += taxAmountInv;
					totalAdvanceRcvd += advncRcvdInv;
				}

			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", ats);
			TransactionDataDTO atSumm = new TransactionDataDTO();
			atSumm.setTaxAmount(totalTaxAmount);
			atSumm.setAmTaxAmount(amTaxAmount);
			atSumm.setTaxableValue(totalAdvanceRcvd);// storing advance amount
			atSumm.setAmTaxableValue(amAdvanceRcvd);// storing advance amount
			atSumm.setError(summaryError);
			atSumm.setCessAmount(cessAmount);
			atSumm.setIgstAmount(igstAmount);
			atSumm.setCgstAmount(cgstAmount);
			atSumm.setSgstAmount(sgstAmount);
			atSumm.setReconcileDTO(reconcileDTO);
			gstrSummaryDTO.setAta(atSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setAtFlag(AT at) {
		if (!Objects.isNull(at.getTaxPayerAction())) {
			if (at.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				at.setFlags(TableFlags.ACCEPTED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.REJECT)
				at.setFlags(TableFlags.REJECTED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.PENDING)
				at.setFlags(TableFlags.PENDING.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.MODIFY)
				at.setFlags(TableFlags.MODIFIED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.DELETE)
				at.setFlags(TableFlags.DELETED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				at.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<DocIssue>b2csDatas=datas;
				for (DocIssue invoice : b2csDatas) {
						List<String> columns = this.getFlatData(invoice, returnType);
						tabularData.add(columns);
				}

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}
private List<String> getFlatData(DocIssue invoice,String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		columns.add(this.getStringValue(invoice.getCategory()));
		columns.add(this.getStringValue(invoice.getSnoFrom()));
		columns.add(this.getStringValue(invoice.getSnoTo()));
		columns.add(this.getStringValue(invoice.getTotalNumber()));
		columns.add(this.getStringValue(invoice.getCanceled()));
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));


		return columns;

		
		}

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<AT> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<AT>>() {
				});

				data.removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getAta();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceAts = getData(source, returnType);

		Map<String, List<String>> changedAts = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionAts = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceAts) && !Objects.isNull(changedAts)) {

			Iterator it = sourceAts.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedAts.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedAts.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedAts.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceAts.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionAts.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionAts.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionAts.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionAts;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<AT> ats = objectMapper.readValue(object, new TypeReference<List<EXP>>() {
				});

				for (AT at : ats) {
					List<InvoiceItem> items = at.getInvoiceItems();

					for (InvoiceItem item : items) {
						//List<String> columns = getFlatData(at, item, returnType);

						/*
						 * String temp =
						 * StringUtils.isEmpty(at.getDocumentNumber())? "" :
						 * at.getDocumentNumber() + ":" +
						 * item.getGoodsOrServiceCode() == null ? "" :
						 * String.valueOf(item.getGoodsOrServiceCode()) + ":" +
						 * item.getTaxableValue() == null ? "" :
						 * String.valueOf(item.getAdvanceAmount());
						 */
						// tabularData.put(temp, columns);
					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<AT> ats = JsonMapper.objectMapper.readValue(object, new TypeReference<List<AT>>() {
			});

			List<AT> currentAts = new ArrayList<>();

			for (AT at : ats) {

				if (!at.isAmendment() && !at.isTransit()) {
					currentAts.add(at);
					at.setTransit(true);
					at.setTransitId(transitId);
				}

			}

			//gstr1.setAt(currentAts);

			if (currentAts.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(ats);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<AT> ats = JsonMapper.objectMapper.readValue(object, new TypeReference<List<AT>>() {
			});
			for (AT at : ats) {
				if (at.isTransit() && at.getTransitId().equals(transitId)) {
					at.setTransit(false);
					at.setTransitId("");
					at.setGstnSynced(true);
				}
			}
			return JsonMapper.objectMapper.writeValueAsString(ats);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public ATTransactionEntity saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		String transactions = gstinTransactionDTO.getTransactionObject();
		try {
			if (StringUtils.isEmpty(transactions)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();

				List<AT> transactionAts = objectMapper.readValue(transactions, new TypeReference<List<AT>>() {
				});

				List<StateDTO> states = new ArrayList<>();

				try {
					finderService = (FinderService) InitialContext.doLookup("java:comp/env/finder");
					states = finderService.getAllState();
				} catch (NamingException e) {
					e.printStackTrace();
				}
				ATTransactionEntity atTran = new ATTransactionEntity();
				for (AT at : transactionAts) {

					atTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					atTran.setReturnType(gstinTransactionDTO.getReturnType());
					atTran.setGstin((TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
							TaxpayerGstin.class));
					/*
					 * ObjectNode data=objectMapper.convertValue(at,
					 * ObjectNode.class); data.remove(ignorableFields);
					 */

					StateDTO temp = new StateDTO();
					temp.setId(at.getRecipientStateCode());
					int i = states.indexOf(temp);
					if (i > -1) {
						StateDTO stateDTO = states.get(states.indexOf(temp));
						atTran.setStateName(stateDTO.getName());
						atTran.setPos(String.valueOf(at.getRecipientStateCode()));
					} else {
						atTran.setStateName("No Name");
					}
					//atTran.setSupplyType(at.getSupplyType());

					atTran.setIsValid(at.isValid());
					atTran.setIsLocked(at.isLocked());
					atTran.setSource(SourceType.PORTAL.toString());
					atTran.setFlags(at.getFlags());
					atTran.setType(String.valueOf(at.getType()));

				}

				return atTran;
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		return 0;
	}

}
