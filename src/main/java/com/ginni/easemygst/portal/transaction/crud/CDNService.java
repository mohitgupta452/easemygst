package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;

public class CDNService extends ItemService{
	
	public int insertUpdateCDNTransaction(List<CDNTransactionEntity> cdnTransactions,String delete) throws SQLException
	{	      int updateCount=0;

		if(cdnTransactions != null && !cdnTransactions.isEmpty()){

			_Logger.info("Inserting cdn records for gstin {}",cdnTransactions.get(0).getGstin().getGstin());
			
			GstinTransactionDTO gstinTransactionDto=new GstinTransactionDTO();
			gstinTransactionDto.setTaxpayerGstin((TaxpayerGstinDTO)EntityHelper.convert(cdnTransactions.get(0).getGstin(), TaxpayerGstinDTO.class));
			gstinTransactionDto.setReturnType(cdnTransactions.get(0).getReturnType());
			gstinTransactionDto.setMonthYear(cdnTransactions.get(0).getMonthYear());
			gstinTransactionDto.setTransactionType(TransactionType.CDN.toString());
			
		String cdnExistForDelete="select bd.isSynced,bd.id from cdn_transaction bt,cdn_details bd where gstin=? and returnType=? and monthYear=? and bt.id=bd.cdnTransactionId and bd.isDelete=0 and bd.dataSource='EMGST'";
		
        String updateDelFlag="update cdn_details set isDelete=1,flags='DELETE' where id=? and dataSource='EMGST'";
		
		String deleteIfNotSynced="delete from cdn_details where id=? and dataSource='EMGST'";
		
		String deleteItems="DELETE a from items a,cdn_details cd,cdn_transaction t JOIN(SELECT invoiceNumber,id,revisedInvNo from cdn_details GROUP by invoiceNumber,id,revisedInvNo HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and cd.cdnTransactionId=t.id and cd.isDelete=1 and cd.isSynced=1 and t.gstin=? and t.returnType=? and t.monthYear=?";
		
		String cdnDeleteDuplicate="DELETE a from cdn_transaction bt,cdn_details a JOIN(SELECT invoiceNumber,revisedInvNo from cdn_details GROUP by invoiceNumber,revisedInvNo HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.revisedInvNo=b.revisedInvNo and a.isDelete=1 and a.isSynced=1 and bt.gstin=? and bt.returnType=? and bt.monthYear=?";
	
		String cdnTransactionSelect="select id,fillingStatus,originalCtinName from cdn_transaction where gstin=? and monthYear=? and returnType=? and originalCtin=?";
		
		String cdnTransactionInsertionQry="INSERT INTO `cdn_transaction` (`id`, `gstin`, `returnType`,`monthYear`, `creationTime`,"
				+ " `creationIpAddress`, `createdBy`, `updationTime`, `updatedBy`, `updationIpAddress`, `originalCtin`, `originalCtinName`,`fillingStatus`) "
				+ "VALUES (?,?,?,?, CURRENT_TIMESTAMP,?,?, NULL, NULL, NULL, ?,?,?)\n";
		
	      String sql ="{call cdnInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	      
			String cdnTransactionUpdate="update cdn_transaction set fillingStatus=?,originalCtinName=? where id=?";
	      
	      int count =0;
	      
		try(	Connection connection=	DBUtils.getConnection();
				CallableStatement itemCS = connection.prepareCall(itemInsertSql);
			PreparedStatement cdnTransactionPS =connection.prepareStatement(cdnTransactionInsertionQry);
				
              PreparedStatement cdnDelExist=connection.prepareStatement(cdnExistForDelete);
				
				PreparedStatement cdnDelDuplicate=connection.prepareStatement(cdnDeleteDuplicate);
				
				PreparedStatement cdnDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
				
				PreparedStatement cdnUpdateFlag=connection.prepareStatement(updateDelFlag);
				PreparedStatement itemDelete=connection.prepareStatement(deleteItems);
				Connection delItemconn = DBUtils.getConnection();


		CallableStatement  cdnDetailsCS =connection.prepareCall(sql);//connection.prepareStatement(insertQueryB2bDetails);
		
		PreparedStatement cdnExist=connection.prepareStatement(cdnTransactionSelect);
				PreparedStatement updateCdnTrasnaction=connection.prepareStatement(cdnTransactionUpdate);

		){
			
			connection.setAutoCommit(false);
			connection.setAutoCommit(false);
      boolean update =false;
      int updateFlagcount = 0;
      if ("YES".equalsIgnoreCase(delete)) {
			// for marking delete flag
			cdnDelExist.setString(1, cdnTransactions.get(0).getGstin().getId().toString());
			cdnDelExist.setString(2, cdnTransactions.get(0).getReturnType());
			cdnDelExist.setString(3, cdnTransactions.get(0).getMonthYear());

			ResultSet deleteFlagResult = cdnDelExist.executeQuery();

			while (deleteFlagResult.next()) {
				if (deleteFlagResult.getBoolean(1)) {
					cdnUpdateFlag.setString(1, deleteFlagResult.getString(2));
					updateFlagcount += cdnUpdateFlag.executeUpdate();
				} else {
					cdnDelNotSynced.setString(1, deleteFlagResult.getString(2));
					updateFlagcount += cdnDelNotSynced.executeUpdate();
				}
			}
			
			DBUtils.closeQuietly(deleteFlagResult);
		}
		for(CDNTransactionEntity cdnTransaction : cdnTransactions)
		{
			
					
			
			cdnExist.setInt(1,cdnTransaction.getGstin().getId());
			cdnExist.setString(2,cdnTransaction.getMonthYear());
			cdnExist.setString(3,cdnTransaction.getReturnType());
			cdnExist.setString(4,cdnTransaction.getOriginalCtin());
			ResultSet resultSet=cdnExist.executeQuery();
			
			String id=null;
			if(resultSet.next())
			{
			 id= resultSet.getString(1);
			 if(!"YES".equalsIgnoreCase(delete))
			   update=true;
			if((StringUtils.isNotEmpty(cdnTransaction.getFillingStatus()) && 
					!cdnTransaction.getFillingStatus().equalsIgnoreCase("N") &&
					!cdnTransaction.getFillingStatus().equalsIgnoreCase(resultSet.getString(2))) 
					|| StringUtils.isNoneEmpty(cdnTransaction.getOriginalCtinName())){
				
			updateCdnTrasnaction.setString(1,cdnTransaction.getFillingStatus());
			updateCdnTrasnaction.setString(2,cdnTransaction.getOriginalCtinName());
			updateCdnTrasnaction.setString(3,id);
			updateCdnTrasnaction.executeUpdate();}
			
			}
			else
			{
				id=UUID.randomUUID().toString();
				
				cdnTransactionPS.setString(1,id);
				cdnTransactionPS.setInt(2,cdnTransaction.getGstin().getId());
				cdnTransactionPS.setString(3,cdnTransaction.getReturnType());
				cdnTransactionPS.setString(4,cdnTransaction.getMonthYear());
				cdnTransactionPS.setString(5,cdnTransaction.getCreatedBy());
				cdnTransactionPS.setString(6,cdnTransaction.getCreationIPAddress());
				cdnTransactionPS.setString(7,cdnTransaction.getOriginalCtin());
				cdnTransactionPS.setString(8,cdnTransaction.getOriginalCtinName());
				cdnTransactionPS.setString(9,cdnTransaction.getFillingStatus());
				cdnTransactionPS.execute();
			}
			
			DBUtils.closeQuietly(resultSet);
			for(CDNDetailEntity cdnDetail : cdnTransaction.getCdnDetails())
			{
				OPERATION operation =OPERATION.INSERT;
				String invoiceId = UUID.randomUUID().toString();
				if (update) {

					String checkDetailSql = "select id,isLocked from cdn_details where cdnTransactionId=? and revisedInvNo=? and dataSource=? and invoiceNumber=?";

					PreparedStatement checkDetail = connection.prepareStatement(checkDetailSql);

					checkDetail.setString(1, id);
					checkDetail.setString(2, cdnDetail.getRevisedInvNo());
					checkDetail.setString(3,cdnDetail.getDataSource().toString());
					checkDetail.setString(4,cdnDetail.getInvoiceNumber());


					ResultSet set = checkDetail.executeQuery();

					if (set.next()) {
						if (Objects.nonNull(set.getBoolean(2)) && set.getBoolean(2) && set.getString(1) != null)
							continue;
						invoiceId = set.getString(1);
						operation = OPERATION.UPDATE;
					} 
					DBUtils.closeQuietly(set);
				}
				cdnDetailsCS.setString(1,invoiceId);
				cdnDetailsCS.setString(2,cdnDetail.getInvoiceNumber());
				cdnDetailsCS.setString(3,cdnDetail.getNoteType());
				
				cdnDetailsCS.setTimestamp(4,Timestamp.from(Instant.now()));
				cdnDetailsCS.setString(5,cdnDetail.getCreatedBy());
				cdnDetailsCS.setString(6,cdnDetail.getCreationIPAddress());
				cdnDetailsCS.setString(7,cdnDetail.getUpdatedBy());
				cdnDetailsCS.setString(8,cdnDetail.getUpdationIPAddress());
				cdnDetailsCS.setTimestamp(9,Timestamp.from(Instant.now()));
				cdnDetailsCS.setString(10,id);
				cdnDetailsCS.setString(11,cdnDetail.getSource());
				cdnDetailsCS.setDouble(12,cdnDetail.getTaxAmount());
				cdnDetailsCS.setDouble(13,cdnDetail.getTaxableValue());
				
				cdnDetailsCS.setString(14,cdnDetail.getPreGstRegime());
				
				cdnDetailsCS.setDate(19,new Date(cdnDetail.getInvoiceDate().getTime()));
				cdnDetailsCS.setString(20,cdnDetail.getInvoiceType());
				cdnDetailsCS.setString(21,operation.toString());
				cdnDetailsCS.setString(22,cdnDetail.getDataSource().toString());
				cdnDetailsCS.setString(23,cdnDetail.getChecksum());

				cdnDetailsCS.setString(15,cdnDetail.getReasonForNote());
				
				cdnDetailsCS.setString(18,cdnDetail.getSourceId());
				
				//cdnDetailsCS.setString(14,cdnDetail.getRevisedCtin());
				//cdnDetailsCS.setString(15,cdnDetail.getRevisedCtinName());
				cdnDetailsCS.setString(16,cdnDetail.getRevisedInvNo());
				cdnDetailsCS.setDate(17,new Date(cdnDetail.getRevisedInvDate().getTime()));
				//ammendment
				cdnDetailsCS.setBoolean(24,cdnDetail.getIsAmmendment());
				cdnDetailsCS.setString(25,cdnDetail.getOriginalNoteNumber());
				if(!Objects.isNull(cdnDetail.getOriginalNoteDate()))
				cdnDetailsCS.setDate(26,new Date(cdnDetail.getOriginalNoteDate().getTime()));
				else
					cdnDetailsCS.setDate(26, null);
				
				
				cdnDetailsCS.addBatch();
				
				this.insertItems(cdnDetail.getItems(), itemCS,operation,invoiceId);
				
				if(++count % 100==0 )
					updateCount+=cdnDetailsCS.executeBatch().length;
				itemCS.executeBatch();
			}
			
			updateCount+=cdnDetailsCS.executeBatch().length;
			itemCS.executeBatch();
			
			update=Boolean.FALSE;
		}
		
		if("YES".equalsIgnoreCase(delete)){
			itemDelete.setString(1, cdnTransactions.get(0).getGstin().getId().toString());
		    itemDelete.setString(2, cdnTransactions.get(0).getReturnType());
		    itemDelete.setString(3, cdnTransactions.get(0).getMonthYear());
			  itemDelete.executeUpdate();
			cdnDelDuplicate.setString(1,cdnTransactions.get(0).getGstin().getId().toString());
			cdnDelDuplicate.setString(2, cdnTransactions.get(0).getReturnType());
			cdnDelDuplicate.setString(3, cdnTransactions.get(0).getMonthYear());
			cdnDelDuplicate.executeUpdate();
        }
		
		connection.commit();
		
	        }
		catch(Exception exception)
		{
			_Logger.error("exception while inserting cdn records for gstin {}",cdnTransactions.get(0).getGstin().getGstin(),exception);

			updateCount=0;
			throw exception;
		}
		_Logger.info("cdn insertion end for gstin {} , records {} updated",cdnTransactions.get(0).getGstin().getGstin(),updateCount);
		this.compareTransaction(gstinTransactionDto);
		}
		
		return updateCount;
	}
	
	private boolean checkDuplicateInvoice(GstinTransactionDTO gstinTransactionDto, Set<String> invoiceNumbers,
			Set<String> ctinInvNos,String deleteAll) throws SQLException, AppException {

		if (Objects.isNull(gstinTransactionDto) || Objects.isNull(invoiceNumbers) || Objects.isNull(ctinInvNos)
				|| !ReturnType.GSTR1.toString().equalsIgnoreCase(gstinTransactionDto.getReturnType())||"YES".equalsIgnoreCase(deleteAll))
			return false;
		_Logger.info("check checkDuplicateInvoice starts  ");
		try (Connection conn = DBUtils.getConnection();) {
			StringBuilder invNumBuilder = new StringBuilder();
			StringBuilder ctinInvNumBuilder = new StringBuilder();
			for (String inum : invoiceNumbers) {
				invNumBuilder.append("?,");
			}
			for (String inum : ctinInvNos) {
				ctinInvNumBuilder.append("?,");
			}
			if (invNumBuilder.length() > 1) {
				invNumBuilder.deleteCharAt(invNumBuilder.length() - 1);
				ctinInvNumBuilder.deleteCharAt(ctinInvNumBuilder.length() - 1);
			}
			String checkDuplicateInvoiceQuery = "SELECT b.invoiceNumber from cdn_details b\n"
					+ "INNER JOIN cdn_transaction  t on\n" + "b.cdnTransactionId=t.id\n" + "WHERE\n"
					+ "t.gstin=?\n" + "AND t.monthYear=?\n" + "AND t.returnType=?\n"
					+ "AND b.flags<>'DELETE' \n" + "AND b.invoiceNumber in(" + invNumBuilder.toString() + ")\n"
					+ "AND concat(t.ctin,b.invoiceNumber)  NOT in (" + ctinInvNumBuilder.toString() + ")"
					+ " AND b.dataSource='EMGST'";
			PreparedStatement checkDuplicateInvoicePreStmt = conn.prepareStatement(checkDuplicateInvoiceQuery);
			checkDuplicateInvoicePreStmt.setInt(1, gstinTransactionDto.getTaxpayerGstin().getId());
			checkDuplicateInvoicePreStmt.setString(2, gstinTransactionDto.getMonthYear());
			checkDuplicateInvoicePreStmt.setString(3, gstinTransactionDto.getReturnType());
			int count = 4;
			for (String inum : invoiceNumbers) {
				checkDuplicateInvoicePreStmt.setString(count, inum);
				count++;
			}

			for (String ctinInum : ctinInvNos) {
				checkDuplicateInvoicePreStmt.setString(count, ctinInum);
				count++;
			}
			ResultSet rs = checkDuplicateInvoicePreStmt.executeQuery();
			ExcelError ee = new ExcelError();
			boolean isDuplicateFound = false;
			while (rs.next()) {
				isDuplicateFound = true;
				this.appendDuplicateInvoiceNoMsg(TransactionType.CDN, rs.getString(1), null, ee);
				_Logger.debug("duplicate invoice numbers are {} ", rs.getString(1));
			}
			DBUtils.closeQuietly(rs);
			if (isDuplicateFound) {
				AppException ae = new AppException();
				ae.setMessage(ee.getErrorDesc().toString());
				throw ae;
			}
			_Logger.info("checkDuplicateInvoice method ends  ");
			return isDuplicateFound;
		}

	}
	
	private ExcelError appendDuplicateInvoiceNoMsg(TransactionType transaction,  String invNo, String msg,ExcelError ee) {
		String message=StringUtils.isEmpty(msg)?"Duplicate entry exist on other ctin for invoice number "+invNo:msg;
		StringBuilder error = ee.getErrorDesc();
		error.append(transaction.toString() + ", - ,- ," + message+"|");
		ee.setError(true);
		return ee;
	}
	

}
