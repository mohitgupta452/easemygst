package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;

import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.Utility;

public class B2BServiceNewImpl extends ItemService {

	public int insert(List<B2BTransactionEntity> b2bTransactionEntities, String isDelete)
			throws SQLException, AppException {
		
		if (Objects.isNull(isDelete))
			isDelete = "no";
		
		GstinTransactionDTO gstinTransactionDto = new GstinTransactionDTO();

		gstinTransactionDto.setTaxpayerGstin((TaxpayerGstinDTO) EntityHelper
				.convert(b2bTransactionEntities.get(0).getTaxpayerGstin(), TaxpayerGstinDTO.class));

		gstinTransactionDto.setReturnType(b2bTransactionEntities.get(0).getReturnType());
		gstinTransactionDto.setMonthYear(b2bTransactionEntities.get(0).getMonthYear());
		gstinTransactionDto.setTransactionType(TransactionType.B2B.toString());
		
		if("gstn_yes".equalsIgnoreCase(isDelete)) {
			
			this.deleteGstnInvs(gstinTransactionDto);
			
		}else {

		ExcelError excelError = new ExcelError();

		this.deleteInvoices(isDelete, b2bTransactionEntities, excelError);

		if ("yes".equalsIgnoreCase(isDelete))
			this.updateInvoices(b2bTransactionEntities);
		}
		int count = this.insertInvoices(b2bTransactionEntities);

		this.compareTransaction(gstinTransactionDto);

		
		return count;

	}

	private void deleteGstnInvs(GstinTransactionDTO gstinTransactionDTO) throws SQLException {

		String delGstnInvs = "delete from b2b_details where b2bTransactionId in "
				+ "(select id from b2b_transaction where taxpayerGstinId =? and monthYear =?) and dataSource='GSTN'";

		try (Connection connection = DBUtils.getConnection();
				PreparedStatement delGstnInvsStmt = connection.prepareStatement(delGstnInvs);) {

			connection.setAutoCommit(false);

			delGstnInvsStmt.setInt(1, gstinTransactionDTO.getTaxpayerGstin().getId());
			delGstnInvsStmt.setString(2, gstinTransactionDTO.getMonthYear());

			int delCount = delGstnInvsStmt.executeUpdate();
			connection.commit();

			_Logger.info("delete inv count for gstn {} ", delCount);

		}

	}

	private void deleteInvoices(String isDelete, List<B2BTransactionEntity> b2bTransactionEntities,
			ExcelError excelError) throws SQLException, AppException {

		String dropTemp = "DROP TABLE IF EXISTS tempb2bIds";

		String createTemp = "create TEMPORARY TABLE IF NOT EXISTS tempb2bIds (aiID bigint(100) NOT NULL, PRIMARY KEY(aiID)) ENGINE=MyISAM";

		String insertTemp = "insert into tempb2bIds (select aiID from b2b_details where (b2bTransactionId in (select id from b2b_transaction where "
				+ "taxpayerGstinId=? and returnType=? and monthYear=?) and isSynced=b'0' and dataSource=?) or aiID in (%s) order by aiID) ";

		String getSyncB2bDetails = "select b.aiID,b.invoiceNumber,b.invoiceDate,b2b.ctin from b2b_details b join b2b_transaction b2b "
				+ "on b.b2bTransactionId=b2b.id where b.isSynced=b'1' and b2b.taxpayerGstinId=%d and b2b.returnType='%s' and b2b.monthYear='%s' "
				+ "and dataSource='EMGST' ";

		Set<String> invIds = new HashSet<>();

		for (B2BTransactionEntity b2bTransactionEntity : b2bTransactionEntities) {

			for (B2BDetailEntity b2bDetailEntity : b2bTransactionEntity.getB2bDetails()) {

				if (!invIds.add(b2bDetailEntity.getInvoiceNumber())
						&& "GSTR1".equalsIgnoreCase(b2bTransactionEntity.getReturnType())) {
					this.generateErrorMessage(excelError,
							"Duplicate invoice number " + b2bDetailEntity.getInvoiceNumber() + " with diffrent ctin");
					// throw new AppException("100010", "Duplicate invoice number
					// "+b2bDetailEntity.getInvoiceNumber()+" with diffrent ctin");
				}

			}
		}

		if (!"yes".equalsIgnoreCase(isDelete)) {

			getSyncB2bDetails = "select b.aiID,b.invoiceNumber,b.invoiceDate,b.isSynced,b2b.ctin from b2b_details b join b2b_transaction b2b on "
					+ "b2b.id=b.b2bTransactionId where b.invoiceNumber in (%s) and b2b.taxpayerGstinId=%d and b2b.returnType='%s' "
					+ "and b2b.monthYear='%s' and dataSource='EMGST'";

			getSyncB2bDetails = String.format(getSyncB2bDetails, this.prepareStrPlaceholderQuery(invIds, 0),
					b2bTransactionEntities.get(0).getTaxpayerGstin().getId(),
					b2bTransactionEntities.get(0).getReturnType(), b2bTransactionEntities.get(0).getMonthYear());
		}

		getSyncB2bDetails = String.format(getSyncB2bDetails, b2bTransactionEntities.get(0).getTaxpayerGstin().getId(),
				b2bTransactionEntities.get(0).getReturnType(), b2bTransactionEntities.get(0).getMonthYear());

		QueryRunner run = new QueryRunner(DBUtils.getDataSource());

		ResultSetHandler<List<B2BDetailEntity>> existinB2bDetailsRsHandler = new BeanListHandler<B2BDetailEntity>(
				B2BDetailEntity.class);

		List<B2BDetailEntity> existinB2bDetailsEntities = run.query(getSyncB2bDetails, existinB2bDetailsRsHandler);

		try (Connection delconnection = DBUtils.getConnection();) {
			delconnection.setAutoCommit(false);
			delconnection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			List<Long> delInvIds = new ArrayList<>();

			String returnType = b2bTransactionEntities.get(0).getReturnType();

			for (B2BTransactionEntity b2bTransactionEntity : b2bTransactionEntities) {

				for (B2BDetailEntity b2bDetailEntity : b2bTransactionEntity.getB2bDetails()) {

					for (B2BDetailEntity exitingB2b : existinB2bDetailsEntities) {

						if (b2bDetailEntity.getInvoiceNumber().equals(exitingB2b.getInvoiceNumber())) {

							if (b2bDetailEntity.getInvoiceDate().compareTo(exitingB2b.getInvoiceDate()) == 0) {

								if (b2bTransactionEntity.getCtin().equalsIgnoreCase(exitingB2b.getCtin())) {
									b2bDetailEntity.setSynced(exitingB2b.getIsSynced());
									delInvIds.add(exitingB2b.getAiID());
								} else {
									if (b2bDetailEntity.getIsSynced()) {
										if (!"gstr2".equalsIgnoreCase(returnType))
											this.generateErrorMessage(excelError, "Invoice number "
													+ b2bDetailEntity.getInvoiceNumber() + "ctin "
													+ b2bTransactionEntity.getCtin()
													+ " already sync to gstn with diffrent ctin, you can not modify ctin");
									}
									else
										if (!"gstr2".equalsIgnoreCase(returnType)) {
										delInvIds.add(exitingB2b.getAiID());
										}
								}
							} else {
								if (b2bDetailEntity.getIsSynced()) {

									if ("gstr2".equalsIgnoreCase(returnType)) {
										if (b2bTransactionEntity.getCtin().equalsIgnoreCase(exitingB2b.getCtin()))
											this.generateErrorMessage(excelError,
													"Invoice number " + b2bDetailEntity.getInvoiceNumber() + "ctin "
															+ b2bTransactionEntity.getCtin()
															+ " already sync to gstn you can not modify invoice date");
									} else {
										this.generateErrorMessage(excelError,
												"Invoice number " + b2bDetailEntity.getInvoiceNumber() + "ctin "
														+ b2bTransactionEntity.getCtin()
														+ " already sync to gstn you can not modify invoice date");

									}

								} else
									if (!"gstr2".equalsIgnoreCase(returnType)) {
									delInvIds.add(exitingB2b.getAiID());
									}
							}

						}

					}
				}
			}

			if (excelError.isError()) {
				AppException appException = new AppException();
				appException.setCode(ExceptionCode._ERROR);
				appException.setMessage(excelError.getErrorDesc().toString());
				throw appException;
			}

			if (!"yes".equalsIgnoreCase(isDelete))
				insertTemp = "insert into tempb2bIds (select aiId from b2b_details where aiId in (%s) order by aiId)";

			insertTemp = String.format(insertTemp,
					StringUtils.isNotEmpty(this.preparePlaceHolders(delInvIds.size()))
							? this.preparePlaceHolders(delInvIds.size())
							: "?");
			try (PreparedStatement delB2BDetailsPreStmt = delconnection.prepareStatement(insertTemp);

					Statement dropTempStmt = delconnection.createStatement()) {

				dropTempStmt.executeUpdate(dropTemp);

				dropTempStmt.executeUpdate(createTemp);

				int offsetIndex = 0;

				if ("yes".equalsIgnoreCase(isDelete)) {
					offsetIndex = 4;

					delB2BDetailsPreStmt.setInt(1, b2bTransactionEntities.get(0).getTaxpayerGstin().getId());
					delB2BDetailsPreStmt.setString(2, b2bTransactionEntities.get(0).getReturnType());
					delB2BDetailsPreStmt.setString(3, b2bTransactionEntities.get(0).getMonthYear());
					delB2BDetailsPreStmt.setString(4, "EMGST");
				}

				this.setValues(delB2BDetailsPreStmt, delInvIds, offsetIndex);

				int delCount = delB2BDetailsPreStmt.executeUpdate();

				String delb2b = "delete from b2b_details using b2b_details inner join tempb2bIds on b2b_details.aiID=tempb2bIds.aiID";

				try (Statement createTempTable = delconnection.createStatement();) {

					delCount = createTempTable.executeUpdate(delb2b);
					delconnection.commit();

					DBUtils.closeQuietly(delconnection);
					_Logger.debug("delete count {}", delCount);
				}
			}
		}

	}

	/*
	 * update invoices with delete flag for gstn
	 */
	private void updateInvoices(List<B2BTransactionEntity> b2bTransactionEntities) throws SQLException {

		String createTemp = "create TEMPORARY TABLE IF NOT EXISTS tempbB2bUpdateIds (aiID bigint(100) NOT NULL, PRIMARY KEY(aiID)) ENGINE=MyISAM";

		String updateB2bInvs = "insert into tempbB2bUpdateIds (select aiID from b2b_details where b2bTransactionId in "
				+ "(select id from b2b_transaction where "
				+ "taxpayerGstinId=? and returnType=? and monthYear=?) and isSynced=b'1' and dataSource=?)";

		String update = "update b2b_details  inner join tempbB2bUpdateIds on "
				+ "b2b_details.aiID=tempbB2bUpdateIds.aiID set isDelete=b'1',flags='DELETE'";

		try (Connection updateConnection = DBUtils.getConnection();
				PreparedStatement updateB2BInvs = updateConnection.prepareStatement(updateB2bInvs);
				Statement updateStmt = updateConnection.createStatement();

		) {
			updateConnection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			updateConnection.setAutoCommit(false);
			updateB2BInvs.setInt(1, b2bTransactionEntities.get(0).getTaxpayerGstin().getId());
			updateB2BInvs.setString(2, b2bTransactionEntities.get(0).getReturnType());
			updateB2BInvs.setString(3, b2bTransactionEntities.get(0).getMonthYear());
			updateB2BInvs.setString(4, "EMGST");

			updateStmt.executeUpdate(createTemp);
			updateB2BInvs.executeUpdate();

			int b2bupdateCount = updateStmt.executeUpdate(update);

			updateConnection.commit();

			_Logger.debug("update b2b invoice for deletion {}", b2bupdateCount);

			DBUtils.closeQuietly(updateConnection);
		}

	}

	private int insertInvoices(List<B2BTransactionEntity> b2bTransactionEntities) throws AppException {

		String inserQueryB2bTransaction = " INSERT INTO `b2b_transaction` (`id`, `taxpayerGstinId`, `returnType`, `ctin`, `taxAmount`, `taxableValue`, `fillingStatus`,"
				+ " `monthYear`, `creationTime`, `creationIpAddress`, `createdBy`, `updationTime`, `updatedBy`, `updationIpAddress`, `ctinName`,`financialYear`) "
				+ "VALUES (?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP,?,?,?,?,?,?,?)";

		String b2bTransactionExist = "select id,isSubmit,fillingStatus,ctinName from b2b_transaction where taxpayerGstinId=? and ctin=? and monthYear=? and returnType=? ";

		String sql = "{call insertUpdate(?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		String b2bTransactionUpdate = "update b2b_transaction set fillingStatus=?,ctinName=? where id=?";

		int updateCount = 0;
		
		String financialYear=CalanderCalculatorUtility.getFiscalYearByMonthYear
				(Utility.convertStrToYearMonth(b2bTransactionEntities.get(0).getMonthYear()));

		try (Connection connection = DBUtils.getConnection();

				Connection delConnection = DBUtils.getConnection();

				PreparedStatement b2bTransctionPreparedStatement = connection
						.prepareStatement(inserQueryB2bTransaction);

				CallableStatement detailPs = connection.prepareCall(sql); // connection.prepareStatement(insertQueryB2bDetails);
				PreparedStatement b2bExist = connection.prepareStatement(b2bTransactionExist);
				PreparedStatement updateB2bTrasnaction = connection.prepareStatement(b2bTransactionUpdate);

				CallableStatement itemPS = connection.prepareCall(itemInsertSql);) {

			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			for (B2BTransactionEntity b2bTransactionEntity : b2bTransactionEntities) {

				b2bExist.setString(1, b2bTransactionEntity.getTaxpayerGstin().getId().toString());
				b2bExist.setString(2, b2bTransactionEntity.getCtin());
				b2bExist.setString(3, b2bTransactionEntity.getMonthYear());
				b2bExist.setString(4, b2bTransactionEntity.getReturnType());

				ResultSet resultSet = b2bExist.executeQuery();

				String id = null;
				if (resultSet.next()) {
					id = resultSet.getString(1);

					if ((StringUtils.isNotEmpty(b2bTransactionEntity.getFillingStatus())
							&& !b2bTransactionEntity.getFillingStatus().equalsIgnoreCase("N")
							&& !b2bTransactionEntity.getFillingStatus().equalsIgnoreCase(resultSet.getString(3)))
							|| StringUtils.isNotEmpty(b2bTransactionEntity.getCtinName())) {
						updateB2bTrasnaction.setString(1, b2bTransactionEntity.getFillingStatus());
						updateB2bTrasnaction.setString(2, b2bTransactionEntity.getCtinName());
						updateB2bTrasnaction.setString(3, id);
						updateB2bTrasnaction.executeUpdate();
					}
				}

				if (StringUtils.isEmpty(id)) {
					id = UUID.randomUUID().toString();

					b2bTransctionPreparedStatement.setString(1,
							b2bTransactionEntity.getId() != null ? b2bTransactionEntity.getId() : id);
					b2bTransctionPreparedStatement.setInt(2, b2bTransactionEntity.getTaxpayerGstin().getId());
					b2bTransctionPreparedStatement.setString(3, b2bTransactionEntity.getReturnType());
					b2bTransctionPreparedStatement.setString(4, b2bTransactionEntity.getCtin());
					b2bTransctionPreparedStatement.setDouble(5,
							b2bTransactionEntity.getTaxAmount() != null ? b2bTransactionEntity.getTaxAmount() : 0.0);
					b2bTransctionPreparedStatement.setDouble(6,
							b2bTransactionEntity.getTaxableValue() != null ? b2bTransactionEntity.getTaxableValue()
									: 0.0);
					b2bTransctionPreparedStatement.setString(7, b2bTransactionEntity.getFillingStatus());
					b2bTransctionPreparedStatement.setString(8, b2bTransactionEntity.getMonthYear());
					b2bTransctionPreparedStatement.setString(9, b2bTransactionEntity.getCreationIPAddress());
					b2bTransctionPreparedStatement.setString(10, b2bTransactionEntity.getCreatedBy());
					b2bTransctionPreparedStatement.setTimestamp(11, b2bTransactionEntity.getUpdationTime());
					b2bTransctionPreparedStatement.setString(12, b2bTransactionEntity.getUpdatedBy());
					b2bTransctionPreparedStatement.setString(13, b2bTransactionEntity.getUpdationIPAddress());
					b2bTransctionPreparedStatement.setString(14, b2bTransactionEntity.getCtinName());
					b2bTransctionPreparedStatement.setString(15,financialYear);
					b2bTransctionPreparedStatement.execute();
				}

				int count = 0;
				for (B2BDetailEntity b2bDetailEntity : b2bTransactionEntity.getB2bDetails()) {
					String invoiceId = UUID.randomUUID().toString();

					detailPs.setString(31, b2bDetailEntity.getChecksum());
					detailPs.setString(30, b2bDetailEntity.getDataSource().toString());
					detailPs.setString(29, "new");
					detailPs.setString(28, b2bDetailEntity.getPos());
					detailPs.setString(27, b2bDetailEntity.getInvoiceType());

					detailPs.setDate(26, new Date(b2bDetailEntity.getInvoiceDate().getTime()));
					detailPs.setBoolean(25, b2bDetailEntity.getReverseCharge());
					detailPs.setString(24,
							b2bDetailEntity.getFlags() != null ? b2bDetailEntity.getFlags().toString() : null);
					detailPs.setString(23, b2bDetailEntity.getSource());
					detailPs.setString(22, b2bDetailEntity.getSourceId());
					detailPs.setString(21, b2bDetailEntity.getSource());
					detailPs.setString(20, b2bDetailEntity.getTransitId());
					detailPs.setString(19, b2bDetailEntity.getSynchId());
					detailPs.setBoolean(18, b2bDetailEntity.getIsLocked());
					detailPs.setBoolean(17, b2bDetailEntity.getIsSynced());
					detailPs.setBoolean(16, b2bDetailEntity.getIsMarked());
					detailPs.setBoolean(15, b2bDetailEntity.getIsTransit());
					detailPs.setBoolean(14, b2bDetailEntity.getIsValid());
					detailPs.setBoolean(13, b2bDetailEntity.getIsAmmendment());
					;
					detailPs.setString(12,
							b2bDetailEntity.getB2bTransaction().getId() != null
									? b2bDetailEntity.getB2bTransaction().getId()
									: id);
					detailPs.setTimestamp(11, Timestamp.from(Instant.now()));
					detailPs.setString(10, b2bDetailEntity.getUpdationIPAddress());
					detailPs.setString(9, b2bDetailEntity.getUpdatedBy());
					detailPs.setString(8, b2bDetailEntity.getCreationIPAddress());
					detailPs.setString(7, b2bDetailEntity.getCreatedBy());
					detailPs.setString(1, invoiceId);
					detailPs.setString(2, b2bDetailEntity.getInvoiceNumber());
					detailPs.setString(3,financialYear);
					detailPs.setString(4, b2bDetailEntity.getEtin());
					detailPs.setDouble(5, b2bDetailEntity.getTaxAmount());
					detailPs.setDouble(6, b2bDetailEntity.getTaxableValue());
					// ammendment
					detailPs.setBoolean(32, b2bDetailEntity.getIsAmmendment());
					detailPs.setString(33, b2bDetailEntity.getOriginalInvoiceNumber());
					if (!Objects.isNull(b2bDetailEntity.getOriginalInvoiceDate()))
						detailPs.setDate(34, new Date(b2bDetailEntity.getOriginalInvoiceDate().getTime()));
					else
						detailPs.setDate(34, null);
					
					detailPs.addBatch();
					this.insertItems(b2bDetailEntity.getItems(), itemPS, OPERATION.INSERT, invoiceId);

					if (++count % 50000 == 0) {

						updateCount += detailPs.executeBatch().length;

						itemPS.executeBatch();
					}

				}

				updateCount += detailPs.executeBatch().length;
				itemPS.executeBatch();

			}
			connection.commit();

		} catch (SQLException sqlException) {

			_Logger.error("sql exception ", sqlException);
		}

		_Logger.debug("{} items inserted successfully tin {}", updateCount,
				b2bTransactionEntities.get(0).getTaxpayerGstin().getGstin());

		return updateCount;

	}

	public String preparePlaceHolders(int length) {
		return String.join(",", Collections.nCopies(length, "?"));
	}

	public void setValues(PreparedStatement preparedStatement, List<Long> values, int offsetIndex) throws SQLException {

		if (values == null || values.isEmpty())
			preparedStatement.setObject(1 + offsetIndex, 0);
		int index = 0;
		for (Long value : values) {
			preparedStatement.setObject((index++) + 1 + offsetIndex, value);
		}
	}

	public String prepareStrPlaceholderQuery(Set<String> values, int offsetIndex) throws SQLException {

		StringBuffer queryStr = new StringBuffer();

		for (String value : values) {
			queryStr.append("'" + value + "',");
		}

		return queryStr.replace(queryStr.length() - 1, queryStr.length(), "").toString();

	}

	private void generateErrorMessage(ExcelError ee, String message) {

		ee.setError(true);
		ee.getErrorDesc().append("-,-,-,").append(message).append("|");

	}

}
