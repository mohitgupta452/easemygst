package com.ginni.easemygst.portal.transaction.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.crud.TxpdService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class TXPDTransaction extends TransactionUtil implements Transaction {

	int _POS_INDEX = 0;
//	int _AMMENDMENT_MONTH_YEAR=12;
	
	int _ORIGINAL_MONTH = 12;
	/*int _ORIGINAL_POS = 14;
	int _ORIGINAL_SUPPLY_TYPE=15;*/
	
	public TXPDTransaction(){
		 _SNO_INDEX = 2;
		 _HSN_CODE_INDEX = 3;
		 _DESCRIPTION_INDEX = 4;
		 _UNIT_INDEX = 5;
		 _QUANTITY_INDEX = 6;
		 _TAXABLE_VALUE_INDEX = 1;
		 _TAX_RATE_INDEX = 7;
		 _IGST_AMT_INDEX = 8;
		 _CGST_AMT_INDEX = 9;
		 _SGST_AMT_INDEX = 10;
		 _CESS_AMT_INDEX = 11;
		 _TEMP_TALLY_POS=0;
		 
		  _ORIGINAL_MONTH = 12;
			/* _ORIGINAL_POS = 14;
			 _ORIGINAL_SUPPLY_TYPE=15;*/
		  
		  System.out.println("Inside TXPD Constructor...");
	}
	List<String> temp=new ArrayList<String>();

	private boolean isDate_index;

	private int _INVOICE_DATE_INDEX;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private int _SUPPLY_VALUE_INDEX;

	private boolean isInvoice_number;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;
	

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	public static List<TxpdTransaction> addTXPDObject(TxpdTransaction txpd, List<TxpdTransaction> txpds) {
		/*List<InvoiceItem> invoiceItems = new ArrayList<>();
		List<InvoiceItem> tInvoiceItems = impg.getImpgInvItms();
		if (impgs.contains(impg)) {
			impg = impgs.get(impgs.indexOf(impg));
			invoiceItems = impg.getImpgInvItms();
			for (InvoiceItem tInvoiceItem : tInvoiceItems) {
				if (!invoiceItems.contains(tInvoiceItem)) {
					invoiceItems.add(tInvoiceItem);
				}
			}

		} else {
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			impgs.add(impg);
		}*/
		Item item=txpd.getItems().get(0);
		List<Item> items;
		
		if(txpds.contains(txpd)){
			txpd=txpds.get(txpds.indexOf(txpd));
			items=txpd.getItems();
			if(!items.contains(item))
				items.add(item);
			
		}else{
			txpds.add(txpd);
		}

		return txpds;
	}

	public static List<IMPG> updateIMPGObject(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = impg.getImpgInvItms();

		String id = impg.getId();
		List<IMPG> searchedImpgs = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(a.getId())).collect(Collectors.toList());

		if (searchedImpgs.isEmpty()) {// to check if the gstin exist
			if (impgs.contains(impg)) {
				searchedImpgs.add(impgs.get(impgs.indexOf(impg)));
				impg.setId(searchedImpgs.get(0).getId());

			}
		}

		if (!searchedImpgs.isEmpty()) {// update existing impg

			IMPG existingImpg = searchedImpgs.get(0);
			items = existingImpg.getImpgInvItms();
			if (!items.isEmpty())
				items.clear();
			for (InvoiceItem tItem : tItems) {
				if (items.contains(tItem)) {// update item
					items.remove(tItem);
					items.add(tItem);
				} else
					items.add(tItem);// add
				if (impg.isGstnSynced())
					impg.setTaxPayerAction(TaxpayerAction.MODIFY);
				impg.setGstnSynced(false);

			}
			impg.setImpgInvItms(items);
			impgs.remove(existingImpg);
			impgs.add(impg);
		} else {// add new impg
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			impg.setGstnSynced(false);
			impgs.add(impg);
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGObjectByNo(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = impg.getImpgInvItms();
		String boeNum = impg.getBillOfEntryNum();
		List<IMPG> searchedImpgs = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(boeNum) && boeNum.equalsIgnoreCase(a.getBillOfEntryNum()))
				.collect(Collectors.toList());
		if (!searchedImpgs.isEmpty()) { // update existing impg

			IMPG existingImpg = searchedImpgs.get(0);
			items = existingImpg.getImpgInvItms();

			for (InvoiceItem tItem : tItems) {
				if (items.contains(tItem)) {// update item items.remove(tItem);
					items.add(tItem);
				} else {
					items.add(tItem);// add item
				}
				if (impg.isGstnSynced()) {
					impg.setTaxPayerAction(TaxpayerAction.MODIFY);
					impg.setGstnSynced(false);
				}
			}
			impg.setImpgInvItms(items);
			impgs.remove(existingImpg);
			impg.setId(existingImpg.getId());
			impgs.add(impg);
		} else {
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			if (impg.isGstnSynced()) {
				impg.setTaxPayerAction(TaxpayerAction.MODIFY);
				impg.setGstnSynced(false);
			}
			impgs.add(impg);
		}
		return impgs;
	}

	public static List<IMPG> deleteIMPGObject(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items;
		String impgId = impg.getId();
		List<IMPG> searchedImpg = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(impgId) && impgId.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedImpg.isEmpty()) {

			items = searchedImpg.get(0).getImpgInvItms();
			InvoiceItem item = impg.getImpgInvItms().get(0);
			if (item.getSNo() != null && items.contains(item)) {
				action = items.remove(item);
				searchedImpg.get(0).setTaxPayerAction(TaxpayerAction.DELETE);//
			}

			if (action == false) {
				if (searchedImpg.get(0).isGstnSynced()) {
					searchedImpg.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedImpg.get(0).setGstnSynced(false);
				} else {
					action = impgs.remove(searchedImpg.get(0));

				}
				action = true;

			}
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGItcData(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> invoiceItems;
		String bId = impg.getId();
		List<IMPG> searchedImpg = impgs.stream()
				.filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId())).collect(Collectors.toList());

		if (!searchedImpg.isEmpty()) {

			invoiceItems = searchedImpg.get(0).getImpgInvItms();
			List<InvoiceItem> invItems = impg.getImpgInvItms();

			for (InvoiceItem item : invItems) {
				if (item.getSNo() != null) {
					if (invoiceItems.indexOf(item) > -1) {
						InvoiceItem tempItem = invoiceItems.get(invoiceItems.indexOf(item));

						String elg = item.getEligOfTotalTax();
						if (!StringUtils.isEmpty(elg)) {
							if (elg.equals("no")) {
								tempItem.setEligOfTotalTax("no");
							} else if (elg.equalsIgnoreCase("ip") || elg.equalsIgnoreCase("cp")) {
								tempItem.setEligOfTotalTax(elg);
								Double igstTemp = tempItem.getIgstAmt();
								Double cgstTemp = tempItem.getCgstAmt();
								Double sgstTemp = tempItem.getSgstAmt();
								Double cessTemp = tempItem.getCessAmt();

								ItcDetail itcDetail = new ItcDetail();

								if (igstTemp != null && igstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalIgst(igstTemp);
								}

								if (cgstTemp != null && cgstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalCgst(cgstTemp);
								}

								if (sgstTemp != null && sgstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalSgst(sgstTemp);
								}

								if (cessTemp != null && cessTemp >= 0.0) {
									itcDetail.setTotalTaxAvalCess(cessTemp);
								}

								tempItem.setItcDetails(itcDetail);
							}
						}
					}
				}

			}
		}

		return impgs;

	}

	public static List<TxpdTransaction> addIMPGObject(List<TxpdTransaction> existingB2bs, List<TxpdTransaction> transactionB2bs) {

		for (TxpdTransaction impg : transactionB2bs) {
			existingB2bs = addTXPDObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGObject(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = updateIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> deleteIMPGObject(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = deleteIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGObjectByNo(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = updateIMPGObjectByNo(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGItcData(List<IMPG> existingImpgs, List<IMPG> transactionImpgs)
			throws AppException {

		for (IMPG impg : transactionImpgs) {
			existingImpgs = updateIMPGItcData(impg, existingImpgs);
		}

		return existingImpgs;
	}
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	@Override
	public int  processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException{

		List<TxpdTransaction> txpds = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if(Objects.nonNull(values))
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
					if(temp == null)
						break;
					// convert list to impg object
					index++;
					if(source==SourceType.TALLY)
						temp=this.processTallyExcel(temp, gstinPos, sheetName, ee);
					convertListToTransactionData(ee, txpds, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin,monthYear,taxpayerGstin);

				}

			}

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			int uploadingCount=0;
			uploadingCount=new TxpdService().insertUpdate(txpds,delete);
			//logging user action
			ApiLoggingService apiLoggingService;
			try {
				apiLoggingService = (ApiLoggingService) InitialContext.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
				MessageDto messageDto=new MessageDto();
				messageDto.setGstin(taxpayerGstin.getGstin());
				messageDto.setReturnType(gstReturn);
				messageDto.setMonthYear(monthYear);
				messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
				messageDto.setTransactionType(TransactionType.TXPD.toString());
				if(!StringUtils.isEmpty(delete)&& delete.equalsIgnoreCase("Yes"))
					apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,taxpayerGstin.getGstin());
					else
				apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto,taxpayerGstin.getGstin());
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//end logging user action
			return uploadingCount;
			
			//return output;

		} catch (SQLException e) {
			_Logger.error("sql exception ",e);
		}
		return 0;

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		List<TxpdTransaction> txpds = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					// convert list to impg object
					if(temp == null)
						break;
					// convert list to impg object
					index++;
					convertListToTransactionData(ee, txpds, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin,monthYear,taxpayerGstin);
				}

			}
			scanner.close();

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new TxpdService().insertUpdate(txpds,delete);

		} catch (SQLException e) {
			_Logger.error("sql exception ",e);
		}
		return 0;

	}
	
	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,List<UploadedCsv>lineItems)
			throws AppException {

		List<TxpdTransaction> txpds = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();


		  String customHeaderIndex="";
		  		if(gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
		  			customHeaderIndex="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18";
		  		else
		  			customHeaderIndex="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22";

		  
		// set column mapping
		setColumnMapping(customHeaderIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int index = 0;

			
			for(UploadedCsv lineItem:lineItems){
				
					index++;
					convertListToTransactionData(ee, txpds, this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin,monthYear,taxpayerGstin);
				

			}

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new TxpdService().insertUpdate(txpds,delete);

		} catch (SQLException e) {
			_Logger.error("sql exception ",e);
		}
		return 0;

	}
	
	
	private List<String> convertLineItemIntoList(UploadedCsv lineItem,ReturnType returnType){
		List<String>fieldList=new ArrayList<>();
		
		fieldList.add(lineItem.getCpGstinStateCode());
		fieldList.add(lineItem.getTaxableAmount());

		fieldList.add("1");//	 for serial no
		fieldList.add(lineItem.getHsnSacCode());
		fieldList.add(lineItem.getHsnSacDesc());
		fieldList.add(lineItem.getUom());
		fieldList.add(lineItem.getQuantity());
		fieldList.add(lineItem.getTotalTaxrate());
		fieldList.add(lineItem.getIgstAmount());
		fieldList.add(lineItem.getCgstAmount());
		fieldList.add(lineItem.getSgstAmount());
		fieldList.add(lineItem.getCessAmount());
		
		if(returnType==ReturnType.GSTR2){
		
		}

		return fieldList;
	}

	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			
			_POS_INDEX = Integer.valueOf(headers[0]);			
			_SNO_INDEX = Integer.valueOf(headers[2]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[3]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[4]);
			_UNIT_INDEX = Integer.valueOf(headers[5]);
			_QUANTITY_INDEX = Integer.valueOf(headers[6]);
			_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[1]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[7]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[8]);
			 _CGST_AMT_INDEX = Integer.valueOf(headers[9]);
			 _SGST_AMT_INDEX = Integer.valueOf(headers[10]);
			_CESS_AMT_INDEX = Integer.valueOf(headers[11]);
			/*if(gstReturn.equalsIgnoreCase("gstr1"))
			_AMMENDMENT_MONTH_YEAR=Integer.valueOf(headers[12]);*/
			if(gstReturn.equalsIgnoreCase("gstr1"))
			_ORIGINAL_MONTH=Integer.valueOf(headers[12]);
			/*_ELIGIBILITY_TAX_INDEX = Integer.valueOf(headers[13]);
			_IGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[14]);
			_CESS_TAX_AVAIL_INDEX = Integer.valueOf(headers[15]);
*/			

		
			

		}

	}

	private void convertListToTransactionData(ExcelError ee, List<TxpdTransaction> txpds, List<String> temp, int index,
			String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos, String gstin,String monthYear,
			TaxpayerGstin taxpayerGstin) {

		handleAmmendmentCase(temp, TransactionType.TXPD);

		ee.resetFieldsExceptErrorAndErrorDesc();
		
		ee.setUniqueRowValue("POS - ["+temp.get(_POS_INDEX)+"]");
		
		TxpdTransaction txpd=new TxpdTransaction();
		//IMPG impg = new IMPG();
		txpd.setSource(sourceName);
		txpd.setSourceId(sourceId);
		Item item = new Item();
		List<Item> items = new ArrayList<>();
		items.add(item);
		txpd.setItems(items);
		txpd.setMonthYear(monthYear);
		txpd.setGstin(taxpayerGstin);
		txpd.setReturnType(gstReturn);

		
				
		ee = this.validatePos(sheetName, _POS_INDEX, index, temp.get(_POS_INDEX), gstinPos, ee);
		if (!ee.isError()) {
			//txpd.setPos(String.valueOf(ee.getPosValue()));
			txpd.setPos(ee.getPos());
		}
		
		this.setIsIgst(sheetName, gstinPos, null, temp.get(_POS_INDEX), ee);
		if(!ee.isError())
		{
		txpd.setSupplyType(ee.isIgst()?SupplyType.INTER:SupplyType.INTRA);
		txpd.setStateName(AppConfig.getStateNameByCode(temp.get(_POS_INDEX)).getName());
		}
		
		if ("gstr1".equalsIgnoreCase(gstReturn) && SourceType.valueOf(sourceName) != SourceType.TALLY
				&& SourceType.valueOf(sourceName) != SourceType.SAP) {
			if (!StringUtils.isEmpty(temp.get(_ORIGINAL_MONTH))) {
				ee=this.validateMonthYear(sheetName, _ORIGINAL_MONTH, index, temp.get(_ORIGINAL_MONTH), ee,null,true);
				String oMonthYear=temp.get(_ORIGINAL_MONTH);
				oMonthYear=oMonthYear.length()==5?oMonthYear="0"+oMonthYear:oMonthYear;
				txpd.setOriginalMonth(oMonthYear);
				txpd.setIsAmmendment(true);
			}
			
		}
		
		/*if(gstReturn.equalsIgnoreCase("gstr1")&& SourceType.valueOf(sourceName)!=SourceType.TALLY)
		txpd.setAmmendmentMonthYear(temp.get(_AMMENDMENT_MONTH_YEAR));*/
		
		double taxAmount=0;
		if(SourceType.valueOf(sourceName)==SourceType.TALLY)
        taxAmount=   this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, gstin);
		else
		 taxAmount=this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item,TransactionType.TXPD);
		 
		
		txpd.setTaxAmount(taxAmount);
		txpd.setAdvanceAdjusted(item.getTaxableValue());
		
		if (!ee.isError())
			addTXPDObject(txpd, txpds);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<IMPG>>() {
				});
				List<IMPG> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<IMPG>>() {
				});

				if (action == null) {
					//existingB2bs = addIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateIMPGObjectByNo(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.UPDATE_ITC) {
					existingB2bs = updateIMPGItcData(existingB2bs, transactionB2bs);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<IMPG> impgs = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<IMPG>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			Date startDate = new Date();

			double totalTaxAmount = 0.0;
			double amTaxAmount = 0.0;
			double amTaxValue = 0.0;
			double totalTaxableValue = 0.0;

			double igstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (IMPG impg : impgs) {

				double taxableValueInv = 0.0;
				double taxAmountInv = 0.0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;

				// shipping bill date
				Date billDate = impg.getBillOfEntryDate();
				String sbDateError = "";
				if (inputMonth.getMonth() < 9) {
					startDate.setDate(1);
					startDate.setMonth(3);
					startDate.setYear(inputMonth.getYear() - 1);
					sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
							+ sdf.format(inputMonth);
				} else {
					startDate.setDate(1);
					startDate.setMonth(3);
					startDate.setYear(inputMonth.getYear());
					sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
							+ sdf.format(inputMonth);
				}
				if (!Objects.isNull(billDate)
						&& !(billDate.compareTo(startDate) >= 0 && billDate.compareTo(inputMonth) <= 0)) {
					errors.put("BILL_DATE", sbDateError);
					errorFlag = false;
				}
				/*
				 * if (!StringUtils.isEmpty(impg.getOrgiBillOfEntryNum()) &&
				 * impg.getBillOfEntryDate() != null) { impg.setAmendment(true);
				 * } else { impg.setAmendment(false); }
				 */

				impg.setError(errors);
				impg.setValid(errorFlag);

				invoiceCount++;

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}

				/*
				 * List<ImpgInvItem> items = impg.getImpgInvItms(); if
				 * (!Objects.isNull(items)) { int itemCount = 0; for
				 * (InvoiceItem it : items) { itemCount++; it.setSNo(itemCount);
				 * 
				 * if (!Objects.isNull(it.getTaxableValue())) taxableValueInv +=
				 * it.getTaxableValue(); if (!Objects.isNull(it.getIgstAmt())) {
				 * taxAmountInv += it.getIgstAmt(); igstAmount +=
				 * it.getIgstAmt(); }
				 * 
				 * if (!Objects.isNull(it.getCessAmt())) { taxAmountInv +=
				 * it.getCessAmt(); cessAmount += it.getCessAmt(); }
				 * 
				 * } }
				 */
				impg.setTaxAmount(taxAmountInv);
				impg.setTaxableValue(taxableValueInv);
				if (impg.isAmendment()) {
					//
				}

				if (!impg.isValid()) {
					impg.setType(ReconsileType.ERROR);
					impg.setFlags("");

				}

				// impg.setFlags(flags.toString());
				setImpgFlag(impg);

				if (impg.isAmendment()) {
					amTaxAmount += taxAmountInv;
					amTaxValue += taxableValueInv;
				} else {
					totalTaxAmount += taxAmountInv;
					totalTaxableValue += taxableValueInv;
				}

			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", impgs);
			TransactionDataDTO impgSumm = new TransactionDataDTO();
			impgSumm.setTaxAmount(totalTaxAmount);
			impgSumm.setAmTaxAmount(amTaxAmount);
			impgSumm.setTaxableValue(totalTaxableValue);
			impgSumm.setAmTaxableValue(amTaxValue);
			impgSumm.setReconcileDTO(reconcileDTO);
			impgSumm.setError(summaryError);
			impgSumm.setCessAmount(cessAmount);
			impgSumm.setIgstAmount(igstAmount);
			gstrSummaryDTO.setImpg(impgSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setImpgFlag(IMPG impg) {
		if (!Objects.isNull(impg.getTaxPayerAction())) {
			if (impg.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				impg.setFlags(TableFlags.ACCEPTED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.REJECT)
				impg.setFlags(TableFlags.REJECTED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.PENDING)
				impg.setFlags(TableFlags.PENDING.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.MODIFY)
				impg.setFlags(TableFlags.MODIFIED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.DELETE)
				impg.setFlags(TableFlags.DELETED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				impg.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<TxpdTransaction>b2csDatas=datas;
				for (TxpdTransaction invoice : b2csDatas) {
					
					for(Item item:invoice.getItems()){
						List<String> columns = this.getFlatData(invoice,item, returnType);
						tabularData.add(columns);
					}
				}

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}
	


private List<String> getFlatData(TxpdTransaction invoice, Item item, String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		columns.add(this.getStringValue(invoice.getPos()));
		columns.add(this.getStringValue(invoice.getAdvanceAdjusted()));
		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCgst()));
		columns.add(this.getStringValue(item.getSgst()));
		columns.add(this.getStringValue(item.getCess()));
		if(ReturnType.GSTR1.toString().equalsIgnoreCase(returnType))
		columns.add(this.getStringValue(invoice.getMonthYear()));
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));


		return columns;

		
		}
	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<IMPG> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<IMPG>>() {
				});

				data.removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;

	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getImpg();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<IMPG> datas = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				datas = objectMapper.readValue(input, new TypeReference<List<IMPG>>() {
				});

				if (subType.equals(ReconsileSubType.PENDING_ITC.toString())) {
					/*
					 * datas.removeIf(i -> i.getReconcileFlags() == null ||
					 * !i.getReconcileFlags().contains(ReconsileSubType.OWN.
					 * toString()));
					 */
					for (IMPG impgInvoice : datas) {
						impgInvoice.getImpgInvItms()
								.removeIf(it -> it.getEligOfTotalTax() != null && (it.getEligOfTotalTax().equals("no")
										|| it.getEligOfTotalTax().equals("ip") || it.getEligOfTotalTax().equals("cp")));
					}
					datas.removeIf(in -> in.getImpgInvItms().isEmpty());

				} else {

					/*
					 * datas.removeIf(i -> i.getReconcileFlags() == null ||
					 * !i.getReconcileFlags().contains(subType));
					 */
				}

				return JsonMapper.objectMapper.writeValueAsString(datas);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceImpgs = getData(source, returnType);

		Map<String, List<String>> changedImpgs = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionImpgs = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceImpgs) && !Objects.isNull(changedImpgs)) {

			Iterator it = sourceImpgs.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedImpgs.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedImpgs.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedImpgs.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceImpgs.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionImpgs.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionImpgs.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionImpgs.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionImpgs;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> impgs = objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
				});

				for (IMPG impg : impgs) {
					List<InvoiceItem> items = impg.getImpgInvItms();
					for (InvoiceItem item : items) {

						/*List<String> columns = getFlatData(impg, item, returnType);

						String temp = StringUtils.isEmpty(impg.getBillOfEntryNum()) ? ""
								: impg.getBillOfEntryNum() + ":" + item.getSNo() == null ? ""
										: String.valueOf(item.getSNo());
						tabularData.put(temp, columns);*/

					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		try {
			List<IMPG> impgs = JsonMapper.objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
			});

			List<IMPG> currentImpgs = new ArrayList<>();

			for (IMPG impg : impgs) {

				if (!impg.isAmendment() && !impg.isTransit()) {
					currentImpgs.add(impg);
					impg.setTransit(true);
					impg.setTransitId(transitId);
				}

			}

		//	gstr2.setImpgs(currentImpgs);

			if (currentImpgs.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(impgs);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<IMPG> impgs = JsonMapper.objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
			});
			for (IMPG impg : impgs) {
				if (impg.isTransit() && impg.getTransitId().equals(transitId)) {
					impg.setTransit(false);
					impg.setTransitId("");
					impg.setGstnSynced(true);
				}
			}
			return JsonMapper.objectMapper.writeValueAsString(impgs);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	private ReturnsService returnService;
	private CrudService crudService;
	private FinderService finderService;
	
	
	
	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");
			//finderService = (FinderService) InitialContext.doLookup("java:global/easemygst/FinderServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		gstinTransactionDTO
				.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "txpds"));
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		try {

			Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
					Gstr1Dto.class);
			List<TxpdTransaction> txpds = gstr.getTxpds();
			for (TxpdTransaction txpd : txpds) {
				txpd.setGstin(gstin);
				txpd.setReturnType(gstinTransactionDTO.getReturnType());
				txpd.setMonthYear(gstinTransactionDTO.getMonthYear());
				double taxableValue=0.0;
				double taxAmount=0.0;
				for(Item item:txpd.getItems()){
					taxableValue+=item.getTaxableValue();
					taxAmount+=item.getTaxAmount();
					item.setInvoiceId(txpd.getId());
				}
				txpd.setTaxAmount(taxAmount);
				txpd.setAdvanceAdjusted(taxableValue);
				txpd.setSource(SourceType.PORTAL.toString());
				String stateName=Objects.nonNull(AppConfig.getStateNameByCode(txpd.getPos()))?AppConfig.getStateNameByCode(txpd.getPos()).getName():"";
				txpd.setStateName(stateName);
				
				if (StringUtils.isEmpty(txpd.getId())) {
					InfoService.setInfo(txpd, gstinTransactionDTO.getUserBean());
					try {
						new TxpdService().insertUpdate(txpds,null);
					} catch (SQLException e) {
						e.printStackTrace();
					}

				} else {
					TxpdTransaction existingTxpd = crudService.find(TxpdTransaction.class, txpd.getId());
					if (existingTxpd.getIsTransit()) {
						AppException ae = new AppException();
						ae.setMessage("Txpd invoice is in transit state so can't be updated for the moment");
						_Logger.error("exception--Txpd invoice is in transit state so can't be updated for the moment ");
						throw ae;
					}
					existingTxpd.setPos(txpd.getPos());
					existingTxpd.setStateName(txpd.getStateName());
					existingTxpd.setSupplyType(txpd.getSupplyType());
					existingTxpd.setAdvanceAdjusted(txpd.getAdvanceAdjusted());
					existingTxpd.setMonthYear(txpd.getMonthYear());
					existingTxpd.setSupplyType(txpd.getSupplyType());
					existingTxpd.setIsError(false);
					existingTxpd.setErrMsg("");
					existingTxpd.setFlags("");
					
					existingTxpd.setOriginalMonth(txpd.getOriginalMonth());
					/*existingTxpd.setOriginalPos(txpd.getOriginalPos());
					existingTxpd.setOriginalSupplyType(txpd.getOriginalSupplyType());*/
					existingTxpd.setIsAmmendment(txpd.getIsAmmendment());
				

					InfoService.setInfo(existingTxpd, gstinTransactionDTO.getUserBean());
					crudService.update(existingTxpd);
					returnService.deleteItemsByInvoiceId(existingTxpd.getId());
					for(Item itm:txpd.getItems()){
						itm.setInvoiceId(existingTxpd.getId());
						crudService.create(itm);
					}

				}

			}

		}catch (AppException ae) {

			throw ae;
		} 
		catch (Exception e) {

			_Logger.error("exception in txpd save method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		} 

		return true;

	}



	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
//		gstExcel.loadSheet(14);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				
				if(temp!=null) {
					System.out.println(temp);
					if(ReturnType.GSTR1.toString().equals(gstReturn)) {
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
				}else if(ReturnType.GSTR2.toString().equals(gstReturn)) {
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(8), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(8).equals("10. Advances")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(date+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(8), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(8).equals("10. Advances")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(8), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(8).equals("10. Advances")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(8), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(8).equals("10. Advances")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(8),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(8).equals("10. Advances")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(8).equals("10. Advances")) {
    							System.out.println(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
				}
			}
		}
		return 0;
	}
}
