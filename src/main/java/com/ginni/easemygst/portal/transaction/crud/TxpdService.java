package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jfree.util.Log;

import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.TxpdTransaction;

public class TxpdService extends ItemService {
	
	/**
	 * @param txpdTransactions
	 * @return
	 * @throws SQLException
	 */
	public int insertUpdate(List<TxpdTransaction> txpdTransactions,String delete) throws SQLException
	{
		if(Objects.isNull(txpdTransactions)||txpdTransactions.isEmpty())
			return 0;
		 String sql ="{call txpdInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		 
		 String txpdExistForDelete="select bd.isSynced,bd.id from txpd_transaction bd where bd.gstin=? and bd.returnType=? and bd.monthYear=?  and bd.isDelete=0";
			
		 String updateDelFlag="update txpd_transaction set isDelete=1,flags='DELETE' where id=?";
			
		 String deleteIfNotSynced="delete from txpd_transaction where id=?";
			
		 String txpdDeleteDuplicate="DELETE a from txpd_transaction a JOIN(SELECT pos from txpd_transaction where isDelete=1 GROUP by pos HAVING COUNT(*)>1) b WHERE a.pos=? and a.isSynced=1 and a.gstin=? and a.returnType=? and a.monthYear=?";
		    
		  int updateCount=0;
		    
		  try(  Connection connection=	DBUtils.getConnection();
					CallableStatement itemCS = connection.prepareCall(itemInsertSql);
		       	CallableStatement txpdCS =connection.prepareCall(sql);
				  
				  PreparedStatement atDelExist=connection.prepareStatement(txpdExistForDelete);
		  			
				  PreparedStatement atDelDuplicate=connection.prepareStatement(txpdDeleteDuplicate);
				  			
				  PreparedStatement atDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
				  			
				  PreparedStatement atUpdateFlag=connection.prepareStatement(updateDelFlag);
				  
			){
			 connection.setAutoCommit(false); 
			 
			 if ("YES".equalsIgnoreCase(delete)) {
					// for marking delete flag
					atDelExist.setString(1, txpdTransactions.get(0).getGstin().getId().toString());
					atDelExist.setString(2, txpdTransactions.get(0).getReturnType());
					atDelExist.setString(3, txpdTransactions.get(0).getMonthYear());

					ResultSet deleteFlagResult = atDelExist.executeQuery();

					int updateFlagcount = 0;
					while (deleteFlagResult.next()) {
						if (deleteFlagResult.getBoolean(1)) {
							atUpdateFlag.setString(1, deleteFlagResult.getString(2));
							updateFlagcount += atUpdateFlag.executeUpdate();
						} else {
							atDelNotSynced.setString(1, deleteFlagResult.getString(2));
							updateFlagcount += atDelNotSynced.executeUpdate();
						}
					}
					
					DBUtils.closeQuietly(deleteFlagResult);
				}
          Set<String> posList=new HashSet();
		for(TxpdTransaction  txpdTransaction : txpdTransactions)
		{
			String invoiceId=UUID.randomUUID().toString();
			posList.add(txpdTransaction.getPos());
			
			
			//this.gstin = txpdTransaction.getGstin().getGstin();
			
			txpdCS.setString(1,invoiceId);
			txpdCS.setInt(2,txpdTransaction.getGstin().getId());
			txpdCS.setString(3,txpdTransaction.getReturnType());
			txpdCS.setString(4,txpdTransaction.getMonthYear());
			txpdCS.setString(5,txpdTransaction.getPos());
			txpdCS.setDouble(6,txpdTransaction.getAdvanceAdjusted());
			txpdCS.setDouble(7,txpdTransaction.getTaxAmount());
			txpdCS.setString(8, txpdTransaction.getSource());
			txpdCS.setString(9,txpdTransaction.getSourceId());
			txpdCS.setString(10,txpdTransaction.getCreatedBy());
			txpdCS.setTimestamp(11,Timestamp.from(Instant.now()));
			txpdCS.setString(12,txpdTransaction.getCreationIPAddress());
			txpdCS.setString(13,txpdTransaction.getAmmendmentMonthYear());
			txpdCS.setString(14,txpdTransaction.getStateName());
			txpdCS.setString(15,txpdTransaction.getSupplyType().toString());
			
			//ammendment
			txpdCS.setBoolean(16, txpdTransaction.getIsAmmendment());
			txpdCS.setString(17, txpdTransaction.getOriginalMonth());
			/*txpdCS.setString(18, txpdTransaction.getOriginalPos());
			txpdCS.setString(19,txpdTransaction.getOriginalSupplyType());*/
			
			this.insertItems(txpdTransaction.getItems(),itemCS,OPERATION.INSERT, invoiceId);
			txpdCS.addBatch();
		}
		
		updateCount=txpdCS.executeBatch().length;
		itemCS.executeBatch();

		if("YES".equalsIgnoreCase(delete)){
			for(String pos:posList){	
			atDelDuplicate.setString(1, pos);
			atDelDuplicate.setString(2,txpdTransactions.get(0).getGstin().getId().toString());
			atDelDuplicate.setString(3,txpdTransactions.get(0).getReturnType());
			atDelDuplicate.setString(4, txpdTransactions.get(0).getMonthYear());
			atDelDuplicate.executeUpdate();
		  }
	    }
		connection.commit();
		
		//if(updateCount>0) this.generateForm3B(this.gstin);
	}
		return updateCount;
}
}
