package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.crud.ItemService.OPERATION;
import com.jayway.jsonpath.internal.token.ArrayPathToken.Operation;

import net.bytebuddy.asm.Advice.This;

public abstract class ItemService {

	static final int _DEADLOCK_ERR_CODE = 1213;
	static final int _MAX_RETRY = 3;
	static final int _MAX_RETRY_WAIT_TIME = 5000;
	static final int _LOCK_WAIT_TIMEOUT_ERR_CODE = 1205;

	enum OPERATION {
		INSERT("INSERT"), UPDATE("UPDATE");

		private String operation;

		private OPERATION(String operation) {
			this.operation = operation;
		}

		public String toString() {
			return this.operation;
		}
	}

	final Logger _Logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

	String itemInsertSql = "{call itemInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

	String deleteItemSql = "delete from items where invoiceId=?";

	String form3bSql = "{call generate_form3b(?)}";

	String gstin = "";

	protected void insertItems(List<Item> items, CallableStatement insertItemPS, OPERATION operation, String invoiceId)
			throws SQLException {

		int sno = 1;
		for (Item item : items) {

			insertItemPS.setString(1, UUID.randomUUID().toString());
			insertItemPS.setInt(2, sno++);
			insertItemPS.setString(3, item.getHsnCode());
			insertItemPS.setString(4, item.getHsnDescription());
			insertItemPS.setString(5, item.getUnit());
			insertItemPS.setDouble(6, item.getQuantity());
			insertItemPS.setDouble(7, item.getTaxableValue());
			insertItemPS.setDouble(8, item.getTaxRate());
			insertItemPS.setDouble(9, item.getIgst());
			insertItemPS.setDouble(10, item.getSgst());
			insertItemPS.setDouble(11, item.getCess());
			insertItemPS.setDouble(12, item.getCgst());
			insertItemPS.setString(13, item.getTotalEligibleTax());
			insertItemPS.setDouble(14, item.getItcIgst());
			insertItemPS.setDouble(15, item.getItcCgst());
			insertItemPS.setDouble(16, item.getItcSgst());
			insertItemPS.setDouble(17, item.getItcCess());
			insertItemPS.setString(18, invoiceId);
			insertItemPS.setString(19, operation.toString());

			insertItemPS.addBatch();
			operation = OPERATION.INSERT;

		}

	}

	public void generateForm3B(String gstin) throws SQLException {

		try (Connection connection = DBUtils.getConnection();
				CallableStatement item3b = connection.prepareCall(form3bSql);

		) {
			connection.setAutoCommit(false);

			item3b.setString(1, gstin);

			item3b.executeQuery();

			_Logger.debug("update count {}", item3b.getUpdateCount());
			connection.commit();
		} catch (Exception e) {
			throw e;
		}

	}

	private final static String checkInvoiceNumber = "select id from invoiceMetadata where invoiceNumber=? and financialYear=? and taxpayerGstinId=?";

	private void checkInvoiceNumber(String invoiceNumber, TransactionType transactionType, int taxpayerGstinId,
			String ctin, Connection connection) throws SQLException {

		try (PreparedStatement checkInvPs = connection.prepareStatement(checkInvoiceNumber);) {
		}
	}

	protected void compareTransaction(GstinTransactionDTO gstinTransactionDto) {
		_Logger.info("compare transaction start {}", gstinTransactionDto.getTaxpayerGstin().getGstin());

		 if("gstr2".equalsIgnoreCase(gstinTransactionDto.getReturnType())){
		 ReturnsService returnService;
		 try {
		 returnService = (ReturnsService)
		 InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
		 returnService.compareTransactions(gstinTransactionDto);
		 //returnService.copyAcceptedInvItems(gstinTransactionDto.getTaxpayerGstin().getGstin(),
		 //gstinTransactionDto.getMonthYear(),gstinTransactionDto.getReturnType(),gstinTransactionDto.getTransactionType());
		 } catch (NamingException e) {
		 e.printStackTrace();
		 } catch (Exception e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }
		 }
		_Logger.info("compare transaction end {}", gstinTransactionDto.getTaxpayerGstin().getGstin());

	}

	protected void delItem(String invoiceId, PreparedStatement deleteItemPs) throws SQLException {

		deleteItemPs.setString(1, invoiceId);
		deleteItemPs.executeUpdate();
	}

}
