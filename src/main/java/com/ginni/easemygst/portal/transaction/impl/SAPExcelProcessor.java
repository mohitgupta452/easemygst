package com.ginni.easemygst.portal.transaction.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class SAPExcelProcessor extends TransactionUtil {

	int _GSTIN_INDEX = 3;
	int _GSTIN_NAME_INDEX = 2;
	int _INVOICE_NO_INDEX = 1;
	int _INVOICE_DATE_INDEX = 0;
	int _PLACE_OF_SUPPLY_INDEX = 4;
	int _IS_REVERSE_INDEX = 21;
	int _ETIN_INDEX = 25;
	// int _SNO_INDEX = 0;
	int _HSN_CODE_INDEX = 7;
	int _DESCRIPTION_INDEX = 6;
	int _UNIT_INDEX = 9;
	int _QUANTITY_INDEX = 8;
	int _TAXABLE_VALUE_INDEX = 12;
	int _TAX_RATE_INDEX =37;
	int _IGST_AMT_INDEX = 14;
	int _CGST_AMT_INDEX = 16;
	int _SGST_AMT_INDEX = 18;
	int _CESS_AMT_INDEX = 20;
	int _MONTH_YEAR_INDEX = 100;
	int _TYP_INDEX = 32;
	int _INVOICE_TYPE_INDEX = 29;
	int _SHIPPING_PORT_CODE = 30;
	int _SHIPPING_BILL_NO_INDEX = 30;
	int _SHIPPING_BILL_DATE_INDEX = 31;
	int _EXPORT_FLAG_INDEX = 28;
	int _IGST_RATE_INDEX = 13;
	int _CGST_RATE_INDEX = 15;
	int _CESS_RATE_INDEX = 19;
	int _SGST_RATE_INDEX = 17;

	String b2bColumns = "";
	String b2clColumns = "";
	String b2csColumns = "";
	String expColumns = "";

	public JsonNode processExcelList(GstExcel gstExcel, String gstReturn, String columns, TaxpayerGstin gstin,
			String monthYear, String sheetName, UserBean userBean,String delete) throws Exception {

		List<B2BTransactionEntity> b2bTransactionEntities = new ArrayList<>();
		List<B2CLTransactionEntity> b2cls = new ArrayList<>();
		List<B2CSTransactionEntity> b2csTransactionEntity = new ArrayList<>();
		List<EXPTransactionEntity> expTransactionEntities = new ArrayList<>();

		B2BTransaction b2bTransaction = new B2BTransaction();
		B2CLTransaction b2clTransaction = new B2CLTransaction();
		B2CSTransaction b2csTransaction = new B2CSTransaction();
		EXPTransaction expTransaction = new EXPTransaction();

		// set column mapping
		setColumnMapping(columns, gstReturn);

		// setting columns
		b2bTransaction.setColumnMapping(b2bColumns, gstReturn);
		b2clTransaction.setColumnMapping(b2clColumns, gstReturn);
		b2csTransaction.setColumnMapping(b2csColumns, gstReturn);
		expTransaction.setColumnMapping(expColumns, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin.getGstin()) ? "00" : gstin.getGstin().substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = SourceType.SAP;

		ExcelError ee = new ExcelError();
		
		int columnCount = 0;
		Integer index = 0;
		Iterator<Row> iterator = gstExcel.getSheet().iterator();
		while (iterator.hasNext()) {

			Row row = iterator.next();
			int rowNum = row.getRowNum();
			if (rowNum == 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
				List<String> temp = gstExcel.excelReadRow(cellRange);
				if (temp == null)
					break;
				_Logger.trace("EXCEL {}",temp);
				this.calculateTaxRate(temp); // add tax rate at last index

				ee.setUniqueRowValue("InvoiceNo - ["+ temp.get(_INVOICE_NO_INDEX)+"]");
				
				boolean isGstin = false;
				ee.setDate(null);
				ee.setIgst(false);
				ee.setPos("");
				ee.setPosValue(-1);
				ee.setReverseCharge(false);

				try {
					if (!StringUtils.isEmpty(temp.get(_GSTIN_INDEX))) {
						isGstin = true;
					} else {
						isGstin = false;
					}
				} catch (NumberFormatException e) {
					isGstin = false;
				}
				
				try{
				// setting is igst transaction
				this.setIsIgst(sheetName, gstinPos, gstin.getGstin(),AppConfig.getStateNameByStateCode(temp.get(_PLACE_OF_SUPPLY_INDEX)).getScode()
						, ee);
				}catch(NullPointerException exce){
					
				}
				boolean isB2Cs = true;
				index++;
				if (isGstin) {
					isB2Cs = false;
					B2BTransactionEntity b2bTransactionEntity = new B2BTransactionEntity();
					b2bTransactionEntity.setTaxpayerGstin(gstin);

					b2bTransactionEntity.setMonthYear(monthYear);
					// convert list to b2b object
					b2bTransaction.convertListToData(ee, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin.getGstin(), b2bTransactionEntity, b2bTransactionEntities, userBean);
				} else {
					if (temp.size() >= _EXPORT_FLAG_INDEX && temp.size() >= _INVOICE_TYPE_INDEX
							&& "y".equalsIgnoreCase(temp.get(_EXPORT_FLAG_INDEX))
							&& "export".equalsIgnoreCase(temp.get(_INVOICE_TYPE_INDEX))) {
						isB2Cs = false;
						expTransaction.convertListToTransactionData(ee, expTransactionEntities, temp, index, sheetName,
								sourceName, sourceId, gstReturn, gstinPos, gstin, monthYear, userBean);
					} else if (StringUtils.isNotEmpty(temp.get(_INVOICE_NO_INDEX)) && ee.isIgst()) {
						isB2Cs = false;
						b2clTransaction.convertListToData(ee, b2cls, temp, index, sheetName, sourceName, sourceId,
								gstReturn, gstinPos, gstin, userBean, monthYear);
					}

					if (isB2Cs)
						b2csTransaction.convertListToData(ee, b2csTransactionEntity, temp, index, sheetName, sourceName,
								sourceId, gstReturn, gstinPos, gstin, monthYear, userBean);
				}
				isB2Cs = true;

			}

		}
		return this.saveCompositTransaction(b2bTransactionEntities, expTransactionEntities, b2csTransactionEntity, b2cls, ee,
				b2bTransaction, b2clTransaction, expTransaction, b2csTransaction, userBean, gstin.getGstin(), monthYear, ReturnType.GSTR1, delete);
		
	}

	private void calculateTaxRate(List<String> temp) {

		temp.add(String.valueOf(Double.parseDouble(
						StringUtils.isNotEmpty(temp.get(_CGST_RATE_INDEX)) ? temp.get(_CGST_RATE_INDEX) : "0")
				+ Double.parseDouble(
						StringUtils.isNotEmpty(temp.get(_IGST_RATE_INDEX)) ? temp.get(_IGST_RATE_INDEX) : "0")
				+ Double.parseDouble(
						StringUtils.isNotEmpty(temp.get(_SGST_RATE_INDEX)) ? temp.get(_SGST_RATE_INDEX) : "0")));
	}

	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			

			if (gstReturn.equalsIgnoreCase("gstr1")) {
				
				 _GSTIN_INDEX = Integer.valueOf(headers[3]);
				 _GSTIN_NAME_INDEX = Integer.valueOf(headers[2]);
				 _INVOICE_NO_INDEX = Integer.valueOf(headers[1]);
				 _INVOICE_DATE_INDEX = Integer.valueOf(headers[0]);
				 _PLACE_OF_SUPPLY_INDEX = Integer.valueOf(headers[4]);
				 _IS_REVERSE_INDEX = Integer.valueOf(headers[21]);
				 _ETIN_INDEX = Integer.valueOf(headers[25]);
				//  _SNO_INDEX = 0;
				 _HSN_CODE_INDEX = Integer.valueOf(headers[7]);
				 _DESCRIPTION_INDEX = Integer.valueOf(headers[6]);
				 _UNIT_INDEX = Integer.valueOf(headers[9]);
				 _QUANTITY_INDEX = Integer.valueOf(headers[8]);
				 _TAXABLE_VALUE_INDEX = Integer.valueOf(headers[12]);
				 _TAX_RATE_INDEX =37;//Integer.valueOf(headers[3]);
				 _IGST_AMT_INDEX = Integer.valueOf(headers[14]);
				 _CGST_AMT_INDEX = Integer.valueOf(headers[16]);
				 _SGST_AMT_INDEX = Integer.valueOf(headers[18]);
				 _CESS_AMT_INDEX = Integer.valueOf(headers[20]);
				 _MONTH_YEAR_INDEX = 100;
				 _TYP_INDEX = Integer.valueOf(headers[32]);
				 _INVOICE_TYPE_INDEX = Integer.valueOf(headers[29]);
				 _SHIPPING_PORT_CODE = Integer.valueOf(headers[30]);
				 _SHIPPING_BILL_NO_INDEX = Integer.valueOf(headers[30]);
				 _SHIPPING_BILL_DATE_INDEX = Integer.valueOf(headers[31]);
				 _EXPORT_FLAG_INDEX = Integer.valueOf(headers[28]);
				 _IGST_RATE_INDEX = Integer.valueOf(headers[13]);
				 _CGST_RATE_INDEX = Integer.valueOf(headers[15]);
				 _CESS_RATE_INDEX = Integer.valueOf(headers[19]);
				 _SGST_RATE_INDEX = Integer.valueOf(headers[17]);
				}}

				b2bColumns = _GSTIN_INDEX + "," + _GSTIN_NAME_INDEX + "," + _INVOICE_NO_INDEX + ","
						+ _INVOICE_DATE_INDEX + "," + _PLACE_OF_SUPPLY_INDEX + "," + _IS_REVERSE_INDEX + ","
						+ _ETIN_INDEX + "," + _SNO_INDEX + "," + _HSN_CODE_INDEX + "," + _DESCRIPTION_INDEX + ","
						+ _UNIT_INDEX + "," + _QUANTITY_INDEX + "," + _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + ","
						+ _IGST_AMT_INDEX + "," + _CGST_AMT_INDEX + "," + _SGST_AMT_INDEX + "," + _CESS_AMT_INDEX + ","
						+ _INVOICE_TYPE_INDEX;

				b2clColumns = _PLACE_OF_SUPPLY_INDEX + "," + _INVOICE_NO_INDEX + "," + _INVOICE_DATE_INDEX + ","
						+ _ETIN_INDEX + "," + _SNO_INDEX + "," + _HSN_CODE_INDEX + "," + _DESCRIPTION_INDEX + ","
						+ _UNIT_INDEX + "," + _QUANTITY_INDEX + "," + _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + ","
						+ _IGST_AMT_INDEX + "," + _CESS_AMT_INDEX;

				b2csColumns = _PLACE_OF_SUPPLY_INDEX + "," + _ETIN_INDEX + "," + _SNO_INDEX + "," + _HSN_CODE_INDEX
						+ "," + _DESCRIPTION_INDEX + "," + _UNIT_INDEX + "," + _QUANTITY_INDEX + ","
						+ _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + "," + _IGST_AMT_INDEX + "," + _CGST_AMT_INDEX
						+ "," + _SGST_AMT_INDEX + "," + _CESS_AMT_INDEX + "," + _MONTH_YEAR_INDEX + "," + _TYP_INDEX;
				expColumns = _TYP_INDEX + "," + _INVOICE_NO_INDEX + "," + _INVOICE_DATE_INDEX + ","
						+ _SHIPPING_PORT_CODE + "," + _SHIPPING_BILL_NO_INDEX + "," + _SHIPPING_BILL_DATE_INDEX + ","
						+ _SNO_INDEX + "," + _HSN_CODE_INDEX + "," + _DESCRIPTION_INDEX + "," + _UNIT_INDEX + ","
						+ _QUANTITY_INDEX + "," + _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + "," + _IGST_AMT_INDEX;
	}
}
