package com.ginni.easemygst.portal.transaction.crud;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.persistence.entity.transaction.CommonAttributesEntity;

public class InfoService {

	public static void setInfo(CommonAttributesEntity commonAtr, UserBean userBean) {
		if (!Objects.isNull(userBean)) {
			commonAtr.setCreatedBy(userBean.getUserDto().getUsername());
			commonAtr.setCreationIPAddress(userBean.getIpAddress());

			commonAtr.setUpdatedBy(userBean.getUserDto().getUsername());
			commonAtr.setUpdationIPAddress(userBean.getIpAddress());
		}
		commonAtr.setCreationTime(Timestamp.from(Instant.now()));
		commonAtr.setUpdationTime(Timestamp.from(Instant.now()));
	}

}
