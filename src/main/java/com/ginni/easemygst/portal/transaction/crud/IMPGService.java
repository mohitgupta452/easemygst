package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPGTransactionEntity;

public class IMPGService  extends ItemService {

	private static final String checkExistSQL="select id,isSubmit from impg_transaction where gstin=? and monthYear=? and billOfEntryNumber=?"
			+ " and returnType=?";

	public int insert(List<IMPGTransactionEntity> impgTransactionEntities,String delete) throws SQLException{
		
        if(impgTransactionEntities.isEmpty())
        	return 0;
		String impgExistForDelete="select bd.isSynced,bd.id from impg_transaction bd where bd.gstin=? and bd.returnType=? and bd.monthYear=?  and bd.isDelete=0";
		
		String updateDelFlag="update impg_transaction set isDelete=1,flags='DELETE' where id=?";
		
		String deleteIfNotSynced="delete from impg_transaction where id=?";
		
		String deleteItems="DELETE a from items a,impg_transaction t JOIN(SELECT billOfEntryNumber,id from impg_transaction GROUP by billOfEntryNumber,id HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and t.isDelete=1 and t.isSynced=1 and t.gstin=? and t.returnType=? and t.monthYear=?";
		
		String impgDeleteDuplicate="DELETE a from impg_transaction a JOIN(SELECT billOfEntryNumber from impg_transaction GROUP by billOfEntryNumber HAVING COUNT(*)>1) b WHERE a.billOfEntryNumber=b.billOfEntryNumber and a.isDelete=1 and a.isSynced=1 and a.gstin=? and a.returnType=? and a.monthYear=?";
		
			
		int updateCount=0;
	try(	Connection connection=DBUtils.getConnection();
			
		CallableStatement impgCS=connection.prepareCall("{call impgInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		
		CallableStatement itemCS= connection.prepareCall(itemInsertSql);
		
		PreparedStatement b2burDelExist=connection.prepareStatement(impgExistForDelete);
			
		PreparedStatement b2burDelDuplicate=connection.prepareStatement(impgDeleteDuplicate);
				
	    PreparedStatement b2burDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
				
		PreparedStatement b2burUpdateFlag=connection.prepareStatement(updateDelFlag);
			
			 PreparedStatement itemdelete=connection.prepareStatement(deleteItems);
		
		PreparedStatement existPS=connection.prepareStatement(checkExistSQL);
			Connection delItemconn = DBUtils.getConnection();
)
	
	    
	{
		delItemconn.setAutoCommit(false);
		
		OPERATION operation=OPERATION.INSERT;
		
		if ("YES".equalsIgnoreCase(delete)) {
			// for marking delete flag
			b2burDelExist.setString(1, impgTransactionEntities.get(0).getGstin().getId().toString());
			b2burDelExist.setString(2, impgTransactionEntities.get(0).getReturnType());
			b2burDelExist.setString(3, impgTransactionEntities.get(0).getMonthYear());

			ResultSet deleteFlagResult = b2burDelExist.executeQuery();

			int updateFlagcount = 0;
			while (deleteFlagResult.next()) {
				if (deleteFlagResult.getBoolean(1)) {
					b2burUpdateFlag.setString(1, deleteFlagResult.getString(2));
					updateFlagcount += b2burUpdateFlag.executeUpdate();
				} else {
					b2burDelNotSynced.setString(1, deleteFlagResult.getString(2));
					updateFlagcount += b2burDelNotSynced.executeUpdate();
				}
			}
			
			DBUtils.closeQuietly(deleteFlagResult);
		}
		for(IMPGTransactionEntity impg:impgTransactionEntities){
			
			
			String invoiceId=UUID.randomUUID().toString();
			//this.gstin = impg.getGstin().getGstin();
			if(!"YES".equalsIgnoreCase(delete)){			
			existPS.setInt(1,impg.getGstin().getId());
			existPS.setString(2,impg.getMonthYear());
			existPS.setString(3,impg.getBillOfEntryNumber());
			existPS.setString(4,impg.getReturnType());
			
			ResultSet impgRS=existPS.executeQuery();
		   if(impgRS.next() && !impgRS.getBoolean(2)){
			   invoiceId=impgRS.getString(1);
			   operation=OPERATION.UPDATE;
		   }
		   
		   DBUtils.closeQuietly(impgRS);
		}
			
			impgCS.setString(1,invoiceId);
			impgCS.setString(2,impg.getBillOfEntryNumber());
			impgCS.setDate(3,new Date(impg.getBillOfEntryDate().getTime()));
			impgCS.setDouble(4,impg.getBillOfEntryValue());
		    impgCS.setString(5,impg.getCtin());
		    impgCS.setString(6,impg.getMonthYear());
		    impgCS.setString(7,impg.getPortCode());
		    impgCS.setString(8,impg.getReturnType());
		    impgCS.setString(9,impg.getSource());
		    impgCS.setString(10,impg.getSourceId());
		    impgCS.setString(11,impg.getSez().toString());
		    impgCS.setDouble(12,impg.getTaxableValue());
		    impgCS.setDouble(13,impg.getTaxAmount());
		    impgCS.setString(14,impg.getCreatedBy());
		    impgCS.setTimestamp(15,impg.getCreationTime());
		    impgCS.setString(16,impg.getCreationIPAddress());
		    impgCS.setTimestamp(17,impg.getUpdationTime());
		    impgCS.setString(18,impg.getUpdationIPAddress());
		    impgCS.setString(19,impg.getUpdatedBy());
		    impgCS.setInt(20,impg.getGstin().getId());
		    impgCS.setString(21,operation.toString());
		    
		    this.insertItems(impg.getItems(), itemCS, operation, invoiceId);
		    
		    operation=OPERATION.INSERT;
		    impgCS.addBatch();
		}
		updateCount=impgCS.executeBatch().length;
	    itemCS.executeBatch();

		if("YES".equalsIgnoreCase(delete)){
			itemdelete.setString(1, impgTransactionEntities.get(0).getGstin().getId().toString());
			itemdelete.setString(2, impgTransactionEntities.get(0).getReturnType());
			itemdelete.setString(3, impgTransactionEntities.get(0).getMonthYear());
			 itemdelete.executeUpdate();
			b2burDelDuplicate.setString(1,impgTransactionEntities.get(0).getGstin().getId().toString());
			b2burDelDuplicate.setString(2, impgTransactionEntities.get(0).getReturnType());
			b2burDelDuplicate.setString(3, impgTransactionEntities.get(0).getMonthYear());
			b2burDelDuplicate.executeUpdate();
	    }
		
		//if(updateCount>0) this.generateForm3B(this.gstin);
		
	}
	catch(Exception e)
	{
		updateCount=0;
		throw e;
	}
	return	updateCount;
		
	}
	
}
