package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;

public class B2BServcie extends ItemService {

	public int insertion(List<B2BTransactionEntity> b2bTransactionEntitiss,String temp)
			throws SQLException, JsonProcessingException, InterruptedException,AppException {
		if(b2bTransactionEntitiss != null){
		//String b2bExistForDelete="select bt.id,bd.isSynced,bd.id from b2b_transaction bt,b2b_details bd where taxpayerGstinId=? and returnType=? and monthYear=? and bt.id=bd.b2bTransactionId and bd.isDelete=0 and bd.dataSource='EMGST'";
		
		String updateDelFlag="update b2b_details set flags='DELETE',toBeSync=b'1' where b2bTransactionId in (select id from b2b_transaction where " 
								+ "taxpayerGstinId=? and returnType=? and monthYear=?) and isSynced=b'1' and dataSource=?";
		
		//String deleteIfNotSynced="delete from b2b_details where id=? and dataSource='EMGST'";
		
		String deleteB2B="delete from b2b_details where b2bTransactionId in (select id from b2b_transaction where "
				+ "taxpayerGstinId=? and returnType=? and monthYear=?) and isSynced=b'0' and dataSource=?";

		
		//String deleteItems="DELETE a from items a,b2b_details bd,b2b_transaction t JOIN(SELECT invoiceNumber,id from b2b_details GROUP by invoiceNumber,id HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and bd.b2bTransactionId=t.id and bd.isDelete=1 and bd.isSynced=1 and t.taxpayerGstinId=? and t.returnType=? and t.monthYear=? and bd.dataSource='EMGST'";
		
		//String b2bDeleteDuplicate="DELETE a from b2b_transaction bt,b2b_details a JOIN(SELECT invoiceNumber from b2b_details GROUP by invoiceNumber HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.isDelete=1 and a.isSynced=1 and bt.taxpayerGstinId=? and bt.returnType=? and bt.monthYear=? and a.dataSource='EMGST'";
		
		if(b2bTransactionEntitiss != null && !b2bTransactionEntitiss.isEmpty()){
			
		_Logger.info("b2b insertion start for gstin {}",b2bTransactionEntitiss.get(0).getTaxpayerGstin().getGstin());
		
		GstinTransactionDTO gstinTransactionDto=new GstinTransactionDTO();
		gstinTransactionDto.setTaxpayerGstin((TaxpayerGstinDTO)EntityHelper.convert(b2bTransactionEntitiss.get(0).getTaxpayerGstin(), TaxpayerGstinDTO.class));
		
		gstinTransactionDto.setReturnType(b2bTransactionEntitiss.get(0).getReturnType());
		gstinTransactionDto.setMonthYear(b2bTransactionEntitiss.get(0).getMonthYear());
		gstinTransactionDto.setTransactionType(TransactionType.B2B.toString());
		
		String inserQueryB2bTransaction = " INSERT INTO `b2b_transaction` (`id`, `taxpayerGstinId`, `returnType`, `ctin`, `taxAmount`, `taxableValue`, `fillingStatus`,"
				+ " `monthYear`, `creationTime`, `creationIpAddress`, `createdBy`, `updationTime`, `updatedBy`, `updationIpAddress`, `ctinName`) "
				+ "VALUES (?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP,?,?,?,?,?,?)";
		
		
		String delFlagRecord ="delete from b2b_transaction where id=?";
		
		String b2bTransactionExist = "select id,isSubmit,fillingStatus from b2b_transaction where taxpayerGstinId=? and ctin=? and monthYear=? and returnType=? ";

		String sql = "{call insertUpdate(?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
		String b2bTransactionUpdate="update b2b_transaction set fillingStatus=? where id=?";
		
		Set<String>invoiceNos=new HashSet<>();
		Set<String>ctinInvNos=new HashSet<>();

		int updateCount=0;
		try (Connection connection = DBUtils.getConnection();
				PreparedStatement b2bTransctionPreparedStatement = connection
						.prepareStatement(inserQueryB2bTransaction);
				
				CallableStatement detailPs = connection.prepareCall(sql); 
				
				
				PreparedStatement b2bExist = connection.prepareStatement(b2bTransactionExist);
				
				
				
				PreparedStatement delB2B=connection.prepareStatement(deleteB2B);
				PreparedStatement updateB2b=connection.prepareStatement(updateDelFlag);
				
						
				CallableStatement itemPS = connection.prepareCall(itemInsertSql);
				
				PreparedStatement updateB2bTrasnaction=connection.prepareStatement(b2bTransactionUpdate);
				) {
			connection.setAutoCommit(false);

			boolean update = false;
			
			
			if("YES".equalsIgnoreCase(temp) || "gstn_yes".equalsIgnoreCase(temp)){
			
				
				String dataSource="EMGST";
				if("gstn_yes".equalsIgnoreCase(temp))
					dataSource="GSTN";
				
				delB2B.setInt(1, b2bTransactionEntitiss.get(0).getTaxpayerGstin().getId());
				delB2B.setString(2, b2bTransactionEntitiss.get(0).getReturnType());
				delB2B.setString(3, b2bTransactionEntitiss.get(0).getMonthYear());
				delB2B.setString(4,dataSource);
				

				updateB2b.setInt(1, b2bTransactionEntitiss.get(0).getTaxpayerGstin().getId());
				updateB2b.setString(2, b2bTransactionEntitiss.get(0).getReturnType());
				updateB2b.setString(3, b2bTransactionEntitiss.get(0).getMonthYear());
				updateB2b.setString(4,dataSource);
								
			_Logger.info("delete {} records for {} month year {} while insert b2b",delB2B.executeUpdate(),b2bTransactionEntitiss.get(0).getTaxpayerGstin().getGstin()
					,b2bTransactionEntitiss.get(0).getMonthYear());
			
			
			_Logger.info("update {} records for {} month year {} while insert b2b",updateB2b.executeUpdate(),b2bTransactionEntitiss.get(0).getTaxpayerGstin().getGstin()
					,b2bTransactionEntitiss.get(0).getMonthYear());
				
				
				}
				 connection.commit();
			for (B2BTransactionEntity b2bTransactionEntity : b2bTransactionEntitiss) {
				b2bExist.setString(1, b2bTransactionEntity.getTaxpayerGstin().getId().toString());
				b2bExist.setString(2, b2bTransactionEntity.getCtin());
				b2bExist.setString(3, b2bTransactionEntity.getMonthYear());
				b2bExist.setString(4,b2bTransactionEntity.getReturnType());

				ResultSet resultSet = b2bExist.executeQuery();

				String id = null;
				if (resultSet.next()) {
					id = resultSet.getString(1);
					update = true;
					if (resultSet.getBoolean(2)) {
						System.out.println("transaction already filed for this month-year");
						continue;
					}
					if(StringUtils.isNotEmpty(b2bTransactionEntity.getFillingStatus()) &&
							!b2bTransactionEntity.getFillingStatus().equalsIgnoreCase("N") &&
							!b2bTransactionEntity.getFillingStatus().equalsIgnoreCase(resultSet.getString(3))){
						updateB2bTrasnaction.setString(1,b2bTransactionEntity.getFillingStatus());
					updateB2bTrasnaction.setString(2,id);
					updateB2bTrasnaction.executeUpdate();
						}
				}
				DBUtils.close(resultSet);

				if (StringUtils.isEmpty(id)) {
					id = UUID.randomUUID().toString();

					b2bTransctionPreparedStatement.setString(1,
							b2bTransactionEntity.getId() != null ? b2bTransactionEntity.getId() : id);
					b2bTransctionPreparedStatement.setInt(2, b2bTransactionEntity.getTaxpayerGstin().getId());
					b2bTransctionPreparedStatement.setString(3, b2bTransactionEntity.getReturnType());
					b2bTransctionPreparedStatement.setString(4, b2bTransactionEntity.getCtin());
					b2bTransctionPreparedStatement.setDouble(5,
							b2bTransactionEntity.getTaxAmount() != null ? b2bTransactionEntity.getTaxAmount() : 0.0);
					b2bTransctionPreparedStatement.setDouble(6, b2bTransactionEntity.getTaxableValue() != null
							? b2bTransactionEntity.getTaxableValue() : 0.0);
					b2bTransctionPreparedStatement.setString(7, b2bTransactionEntity.getFillingStatus());
					b2bTransctionPreparedStatement.setString(8, b2bTransactionEntity.getMonthYear());
					b2bTransctionPreparedStatement.setString(9, b2bTransactionEntity.getCreationIPAddress());
					b2bTransctionPreparedStatement.setString(10, b2bTransactionEntity.getCreatedBy());
					b2bTransctionPreparedStatement.setTimestamp(11, b2bTransactionEntity.getUpdationTime());
					b2bTransctionPreparedStatement.setString(12, b2bTransactionEntity.getUpdatedBy());
					b2bTransctionPreparedStatement.setString(13, b2bTransactionEntity.getUpdationIPAddress());
					b2bTransctionPreparedStatement.setString(14, b2bTransactionEntity.getCtinName());
					b2bTransctionPreparedStatement.execute();
				}

				int count = 0;
                Set<String> invoiceNumbers=new HashSet<>();
				for (B2BDetailEntity b2bDetailEntity : b2bTransactionEntity.getB2bDetails()) {
					ctinInvNos.add(b2bTransactionEntity.getCtin()+b2bDetailEntity.getInvoiceNumber());
					invoiceNos.add(b2bDetailEntity.getInvoiceNumber());
					OPERATION operation =OPERATION.INSERT;
					String invoiceId = UUID.randomUUID().toString();
					
					_Logger.info("b2b insertion end for gstin {} , invoice {}...",b2bTransactionEntitiss.get(0).getTaxpayerGstin().getGstin(),
							b2bDetailEntity.getId());

					if (update) {

						String checkDetailSql = "select id,isLocked from b2b_details where b2bTransactionId=? and invoiceNumber=? and dataSource=?";
						PreparedStatement checkDetail = connection.prepareStatement(checkDetailSql);;
						checkDetail.setString(1, id);
						checkDetail.setString(2, b2bDetailEntity.getInvoiceNumber());
						checkDetail.setString(3,b2bDetailEntity.getDataSource().toString());
						

						ResultSet set = checkDetail.executeQuery();

						if (set.next()) {
							if (Objects.nonNull(set.getBoolean(2)) && set.getBoolean(2) && set.getString(1) != null)
								continue;
							invoiceId = set.getString(1);
						    operation = OPERATION.UPDATE;
						} 
						
						DBUtils.closeQuietly(set);
							//invoiceId = 
					}
					invoiceNumbers.add(b2bDetailEntity.getInvoiceNumber());
					detailPs.setString(31,b2bDetailEntity.getChecksum());
					detailPs.setString(30,b2bDetailEntity.getDataSource().toString());
					detailPs.setString(29,operation.toString());
					detailPs.setString(28,b2bDetailEntity.getPos());
					detailPs.setString(27,b2bDetailEntity.getInvoiceType());
					
					detailPs.setDate(26,new Date(b2bDetailEntity.getInvoiceDate().getTime()));
					detailPs.setBoolean(25, b2bDetailEntity.getReverseCharge());
					detailPs.setString(24, b2bDetailEntity.getFlags()!= null?b2bDetailEntity.getFlags().toString():null);
					detailPs.setString(23, b2bDetailEntity.getSource());
					detailPs.setString(22, b2bDetailEntity.getSourceId());
					detailPs.setString(21, b2bDetailEntity.getSource());
					detailPs.setString(20, b2bDetailEntity.getTransitId());
					detailPs.setString(19, b2bDetailEntity.getSynchId());
					detailPs.setBoolean(18, b2bDetailEntity.getIsLocked());
					detailPs.setBoolean(17, b2bDetailEntity.getIsSynced());
					detailPs.setBoolean(16, b2bDetailEntity.getIsMarked());
					detailPs.setBoolean(15, b2bDetailEntity.getIsTransit());
					detailPs.setBoolean(14, b2bDetailEntity.getIsValid());
					detailPs.setBoolean(13, b2bDetailEntity.getIsAmmendment());
					;
					detailPs.setString(12, b2bDetailEntity.getB2bTransaction().getId() != null
							? b2bDetailEntity.getB2bTransaction().getId() : id);
					detailPs.setTimestamp(11, Timestamp.from(Instant.now()));
					detailPs.setString(10, b2bDetailEntity.getUpdationIPAddress());
					detailPs.setString(9, b2bDetailEntity.getUpdatedBy());
					detailPs.setString(8, b2bDetailEntity.getCreationIPAddress());
					detailPs.setString(7, b2bDetailEntity.getCreatedBy());
					detailPs.setString(1, invoiceId);
					detailPs.setString(2, b2bDetailEntity.getInvoiceNumber());
					detailPs.setString(3, "2017-2018");
					detailPs.setString(4, b2bDetailEntity.getEtin());
					detailPs.setDouble(5, b2bDetailEntity.getTaxAmount());
					detailPs.setDouble(6, b2bDetailEntity.getTaxableValue());

					detailPs.addBatch();

					if (++count % 50 == 0)
						updateCount+=detailPs.executeBatch().length;

					this.insertItems(b2bDetailEntity.getItems(), itemPS, operation, invoiceId);
				}
                
				 updateCount+=detailPs.executeBatch().length;
				
				update = false;
			}
		/*	if("YES".equalsIgnoreCase(temp)){
//			    itemDelete.setString(1, b2bTransactionEntitiss.get(0).getTaxpayerGstin().getId().toString());
//			    itemDelete.setString(2, b2bTransactionEntitiss.get(0).getReturnType());
//			    itemDelete.setString(3, b2bTransactionEntitiss.get(0).getMonthYear());
//				  itemDelete.executeUpdate();
				b2bDelDuplicate.setString(1,b2bTransactionEntitiss.get(0).getTaxpayerGstin().getId().toString());
				b2bDelDuplicate.setString(2, b2bTransactionEntitiss.get(0).getReturnType());
				b2bDelDuplicate.setString(3, b2bTransactionEntitiss.get(0).getMonthYear());
				  b2bDelDuplicate.executeUpdate();
             }
             */
			this.checkDuplicateInvoice(gstinTransactionDto, invoiceNos, ctinInvNos,temp);
			connection.commit();
			
			//if(updateCount>0) this.generateForm3B(this.gstin);
		}
		catch(SQLException exception)
		{			updateCount=0;

			_Logger.error("exception while inserting b2b records for gstin {}",b2bTransactionEntitiss.get(0).getTaxpayerGstin().getGstin(),exception);
			throw exception;
		}
		_Logger.info("b2b insertion end for gstin {} , records {} updated",b2bTransactionEntitiss.get(0).getTaxpayerGstin().getGstin(),updateCount);

		this.compareTransaction(gstinTransactionDto);
		return updateCount;
	}
		}
		return 0;
	}
	
	public int delInsert(String gstin,String monthYear,String returnType) throws SQLException{
		
		try(Connection delConn= DBUtils.getConnection();){
			//subscription id header gstn call
			
			String selectB2bTransaction="select id from b2b_transaction where  where gstin=? and monthYear=?and returnType=? order by id;";
			

			String delB2BTrans="delete from b2b_transaction where gstin=? and monthYear=?and returnType=?";
			String delB2B="delete form b2b_details where b2b_transactionId=?";
			
		}
		return 0;
		
	}
	
	private boolean checkDuplicateInvoice(GstinTransactionDTO gstinTransactionDto, Set<String> invoiceNumbers,
			Set<String> ctinInvNos,String deleteAll) throws SQLException, AppException {

		if (Objects.isNull(gstinTransactionDto) || Objects.isNull(invoiceNumbers) || Objects.isNull(ctinInvNos)
				|| !ReturnType.GSTR1.toString().equalsIgnoreCase(gstinTransactionDto.getReturnType())||"YES".equalsIgnoreCase(deleteAll))
			return false;
		_Logger.info("check checkDuplicateInvoice starts  ");
		try (Connection conn = DBUtils.getConnection();) {
			StringBuilder invNumBuilder = new StringBuilder();
			StringBuilder ctinInvNumBuilder = new StringBuilder();
			for (String inum : invoiceNumbers) {
				invNumBuilder.append("?,");
			}
			for (String inum : ctinInvNos) {
				ctinInvNumBuilder.append("?,");
			}
			if (invNumBuilder.length() > 1) {
				invNumBuilder.deleteCharAt(invNumBuilder.length() - 1);
				ctinInvNumBuilder.deleteCharAt(ctinInvNumBuilder.length() - 1);
			}
			String checkDuplicateInvoiceQuery = "SELECT b.invoiceNumber from b2b_details b\n"
					+ "INNER JOIN b2b_transaction  t on\n" + "b.b2bTransactionId=t.id\n" + "WHERE\n"
					+ "t.taxpayerGstinId=?\n" + "AND t.monthYear=?\n" + "AND t.returnType=?\n"
					+ "AND b.flags<>'DELETE' \n" + "AND b.invoiceNumber in(" + invNumBuilder.toString() + ")\n"
					+ "AND concat(t.ctin,b.invoiceNumber)  NOT in (" + ctinInvNumBuilder.toString() + ")"
					+ " AND b.dataSource='EMGST'";
			PreparedStatement checkDuplicateInvoicePreStmt = conn.prepareStatement(checkDuplicateInvoiceQuery);
			checkDuplicateInvoicePreStmt.setInt(1, gstinTransactionDto.getTaxpayerGstin().getId());
			checkDuplicateInvoicePreStmt.setString(2, gstinTransactionDto.getMonthYear());
			checkDuplicateInvoicePreStmt.setString(3, gstinTransactionDto.getReturnType());
			int count = 4;
			for (String inum : invoiceNumbers) {
				checkDuplicateInvoicePreStmt.setString(count, inum);
				count++;
			}

			for (String ctinInum : ctinInvNos) {
				checkDuplicateInvoicePreStmt.setString(count, ctinInum);
				count++;
			}
			ResultSet rs = checkDuplicateInvoicePreStmt.executeQuery();
			ExcelError ee = new ExcelError();
			boolean isDuplicateFound = false;
			while (rs.next()) {
				isDuplicateFound = true;
				this.appendDuplicateInvoiceNoMsg(TransactionType.B2B, rs.getString(1), null, ee);
				_Logger.debug("duplicate invoice numbers are {} ", rs.getString(1));
			}
			
			DBUtils.closeQuietly(rs);
			DBUtils.closeQuietly(checkDuplicateInvoicePreStmt);
			if (isDuplicateFound) {
				AppException ae = new AppException();
				ae.setMessage(ee.getErrorDesc().toString());
				throw ae;
			}
			_Logger.info("checkDuplicateInvoice method ends  ");
			return isDuplicateFound;
		}
		

	}
	
	private ExcelError appendDuplicateInvoiceNoMsg(TransactionType transaction,  String invNo, String msg,ExcelError ee) {
		String message=StringUtils.isEmpty(msg)?"Duplicate entry exist on other ctin for invoice number "+invNo:msg;
		StringBuilder error = ee.getErrorDesc();
		error.append(transaction.toString() + ", - ,- ," + message+"|");
		ee.setError(true);
		return ee;
	}

}