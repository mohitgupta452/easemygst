package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.transaction.crud.ItemService.OPERATION;

public class EXPService extends ItemService{
	
	public int insertOrUpdate(List<EXPTransactionEntity> expTransactions,String delete) throws SQLException, JsonProcessingException
	{
		int updateCount=0;

		if(expTransactions != null && !expTransactions.isEmpty()){
		String inserQueryEXPTransaction=" INSERT INTO `exp_transaction` (`id`, `taxpayerGstinId`, `returnType`,"
				+ " `monthYear`,"
				+ " `creationTime`, `creationIpAddress`, `createdBy`) "
				+ "VALUES (?,?,?,?,?,?,?)\n;"; 
		
		String expExistForDelete="select bd.isSynced,bd.id from exp_transaction bt,exp_details bd where bt.taxpayerGstinId=? and bt.returnType=? and bt.monthYear=? and bt.id=bd.expTransactionId and bd.isDelete=0";
		
		String updateDelFlag="update exp_details set isDelete=1,flags='DELETE' where id=?";
		
		String deleteIfNotSynced="delete from exp_details where id=?";
		
		String expDeleteDuplicate="DELETE a from exp_transaction bt,exp_details a JOIN(SELECT invoiceNumber from exp_details GROUP by invoiceNumber HAVING COUNT(*)>1) b WHERE a.invoiceNumber=b.invoiceNumber and a.isDelete=1 and a.isSynced=1 and bt.taxpayerGstinId=? and bt.returnType=? and bt.monthYear=?";

		String expTransactionExist="select id from exp_transaction where taxpayerGstinId=? and monthYear=? and returnType=?";
		
		String deleteItems="DELETE a from items a,exp_details ed,exp_transaction t JOIN(SELECT invoiceNumber,id from exp_details GROUP by invoiceNumber,id HAVING COUNT(*)>1) b WHERE a.invoiceId=b.id and ed.expTransactionId=t.id and ed.isDelete=1 and ed.isSynced=1 and t.taxpayerGstinId=? and t.returnType=? and t.monthYear=?";

      String sql ="{call expInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
      int count=0;
      
  	try(
	Connection connection=	DBUtils.getConnection();
	
	PreparedStatement expTransctionPS =connection.prepareStatement(inserQueryEXPTransaction);
	
	CallableStatement  expDetailCS =connection.prepareCall(sql);//connection.prepareStatement(insertQueryB2bDetails);
	
	PreparedStatement expExistsPS=connection.prepareStatement(expTransactionExist);
  			
	PreparedStatement expDelExist=connection.prepareStatement(expExistForDelete);
  			
	PreparedStatement expDelDuplicate=connection.prepareStatement(expDeleteDuplicate);
  			
	PreparedStatement expDelNotSynced=connection.prepareStatement(deleteIfNotSynced);
  			
	PreparedStatement expUpdateFlag=connection.prepareStatement(updateDelFlag);
			Connection delItemconn = DBUtils.getConnection();

  			
  			PreparedStatement itemdelete=connection.prepareStatement(deleteItems);	
  			
			CallableStatement itemPS = connection.prepareCall(itemInsertSql);
  			){
  		
  		connection.setAutoCommit(false);
  		delItemconn.setAutoCommit(false);
	boolean update=false;
	
	if ("YES".equalsIgnoreCase(delete)) {
		// for marking delete flag
		expDelExist.setString(1, expTransactions.get(0).getGstin().getId().toString());
		expDelExist.setString(2, expTransactions.get(0).getReturnType());
		expDelExist.setString(3, expTransactions.get(0).getMonthYear());

		ResultSet deleteFlagResult = expDelExist.executeQuery();

		int updateFlagcount = 0;
		while (deleteFlagResult.next()) {
			if (deleteFlagResult.getBoolean(1)) {
				expUpdateFlag.setString(1, deleteFlagResult.getString(2));
				updateFlagcount += expUpdateFlag.executeUpdate();
			} else {
				expDelNotSynced.setString(1, deleteFlagResult.getString(2));
				updateFlagcount += expDelNotSynced.executeUpdate();
			}
		}
		
		DBUtils.closeQuietly(deleteFlagResult);
	}
	for(EXPTransactionEntity expTransaction : expTransactions)
	{
		
		
		
		expExistsPS.setString(1,expTransaction.getGstin().getId().toString());
		expExistsPS.setString(2,expTransaction.getMonthYear());
		expExistsPS.setString(3,expTransaction.getReturnType());
		
		ResultSet resultSet=expExistsPS.executeQuery();
		
		String id=null;
		if(resultSet.next())
		{
		 id= resultSet.getString(1);
		if(id != null){
			if(!"YES".equalsIgnoreCase(delete))
			update=true;
		 }
		}
		else
		 {
		    id=UUID.randomUUID().toString();
		    
		    expTransctionPS.setString(1,id);
		    expTransctionPS.setInt(2,expTransaction.getGstin().getId());
		    expTransctionPS.setString(3,expTransaction.getReturnType());
		    expTransctionPS.setString(4,expTransaction.getMonthYear());
		    expTransctionPS.setTimestamp(5,Timestamp.from(Instant.now()));
		    expTransctionPS.setString(6,expTransaction.getCreationIPAddress());
		    expTransctionPS.setString(7,expTransaction.getCreatedBy());
		    
		    expTransctionPS.execute();
	}
		DBUtils.closeQuietly(resultSet);
		 
		 for(ExpDetail expDetail:expTransaction.getExpDetails())
		 {
			 
			 OPERATION operation =OPERATION.INSERT;
				String invoiceId = UUID.randomUUID().toString();
				if (update) {

					String checkDetailSql = "select id,isLocked from exp_details where expTransactionId=? and invoiceNumber=?";

					PreparedStatement checkDetail = connection.prepareStatement(checkDetailSql);

					checkDetail.setString(1, id);
					checkDetail.setString(2, expDetail.getInvoiceNumber());

					ResultSet set = checkDetail.executeQuery();

					if (set.next()) {
						if (Objects.nonNull(set.getBoolean(2)) && set.getBoolean(2) && set.getString(1) != null)
							continue;
						invoiceId = set.getString(1);
						operation = OPERATION.UPDATE;
					} 
						//invoiceId = 
				}

			 expDetailCS.setString(1,invoiceId);
			 expDetailCS.setString(2,expDetail.getInvoiceNumber());
			 expDetailCS.setString(3,expDetail.getShippingBillNo());
			 expDetailCS.setTimestamp(4,Timestamp.from(Instant.now()));
			 expDetailCS.setString(5,expDetail.getCreatedBy());
			 expDetailCS.setString(6,expDetail.getCreationIPAddress());
			 expDetailCS.setString(7,expDetail.getUpdatedBy());
			 expDetailCS.setString(8,expDetail.getUpdationIPAddress());
			 expDetailCS.setTimestamp(9,Timestamp.from(Instant.now()));
			 expDetailCS.setString(10,id);
			 expDetailCS.setString(11,expDetail.getSource());
			 expDetailCS.setString(12,expDetail.getSourceId());
			 expDetailCS.setString(13,expDetail.getType());
			 expDetailCS.setString(14,expDetail.getExportType());
			 expDetailCS.setDate(15,new Date(expDetail.getInvoiceDate().getTime()));
			 expDetailCS.setDate(16,Objects.isNull(expDetail.getShippingBillDate()) ? null : new Date(expDetail.getShippingBillDate().getTime()));
			 expDetailCS.setString(17,expDetail.getShippingBillPortCode());
			 expDetailCS.setDouble(18,expDetail.getTaxableValue());
			 expDetailCS.setDouble(19,expDetail.getTaxAmount());
			 expDetailCS.setString(20,operation.toString());
			 //ammendment
			 expDetailCS.setBoolean(21, expDetail.getIsAmmendment());
			 expDetailCS.setString(22, expDetail.getOriginalInvoiceNumber());
				if(!Objects.isNull(expDetail.getOriginalInvoiceDate()))
					expDetailCS.setDate(23, new Date(expDetail.getOriginalInvoiceDate().getTime()));
				else
					expDetailCS.setDate(23, null);
			 
			 expDetailCS.addBatch();
			 
			 this.insertItems(expDetail.getItems(), itemPS, operation, invoiceId);
			 
			 if(++count % 20 ==0) {
				 updateCount+=expDetailCS.executeBatch().length;
					itemPS.executeBatch();

			 }
		 }
		 update=false;
	}
	
	updateCount+=expDetailCS.executeBatch().length;
	itemPS.executeBatch();

	if("YES".equalsIgnoreCase(delete)){
		itemdelete.setString(1, expTransactions.get(0).getGstin().getId().toString());
		itemdelete.setString(2, expTransactions.get(0).getReturnType());
		itemdelete.setString(3, expTransactions.get(0).getMonthYear());
		itemdelete.executeUpdate();
		expDelDuplicate.setString(1,expTransactions.get(0).getGstin().getId().toString());
		expDelDuplicate.setString(2,expTransactions.get(0).getReturnType());
		expDelDuplicate.setString(3,expTransactions.get(0).getMonthYear());
		expDelDuplicate.executeUpdate();
    }
	connection.commit();
	
	//if(updateCount>0) this.generateForm3B(this.gstin);
	
  	   }
  	catch(Exception exception)
	{
		updateCount=0;
		throw exception;
	}
}
	return updateCount;
	}


}
