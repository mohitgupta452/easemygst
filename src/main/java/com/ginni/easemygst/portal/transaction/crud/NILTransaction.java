package com.ginni.easemygst.portal.transaction.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.NilSupplyType;

public class NILTransaction extends ItemService {
	
	public int insertUpdate(List<NILTransactionEntity> nilTransactions,String delete) throws SQLException
	{
		int updateCount=0;

		if(nilTransactions != null && !nilTransactions.isEmpty()){
			
		_Logger.info("nill transaction insertion start for {} {} {}",nilTransactions.get(0).getGstin(),
				nilTransactions.get(0).getReturnType(),nilTransactions.get(0).getMonthYear());
		
	      String sql ="{call nilInsertUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	      
	      String delNil="delete from nil_transaction where monthYear=? and gstin=? and returnType=?";
		try(
	      	Connection connection= DBUtils.getConnection();
			  
				CallableStatement nillCS=connection.prepareCall(sql);
				)
		{
			connection.setAutoCommit(false);
			
			if ("YES".equalsIgnoreCase(delete)) {
				
				try(Connection delConn= DBUtils.getConnection();
						PreparedStatement delNillTransaction=delConn.prepareStatement(delNil);){
					
					
					delConn.setAutoCommit(false);
					delNillTransaction.setString(1,nilTransactions.get(0).getMonthYear());
					delNillTransaction.setInt(2,nilTransactions.get(0).getGstin().getId());
					delNillTransaction.setString(3,nilTransactions.get(0).getReturnType());
					delNillTransaction.executeUpdate();
					delConn.commit();
					DBUtils.closeQuietly(delConn);
				}
			}
			
			for(NILTransactionEntity nilTransaction : nilTransactions)
		{
				
			nillCS.setString(1,UUID.randomUUID().toString());
			nillCS.setInt(2,nilTransaction.getGstin().getId());
			nillCS.setString(3,nilTransaction.getReturnType());
			nillCS.setString(4,nilTransaction.getMonthYear());
			nillCS.setString(5,String.valueOf(nilTransaction.getSupplyType()));
			nillCS.setDouble(6,nilTransaction.getTotExptAmt());
			nillCS.setDouble(7,nilTransaction.getTotNilAmt());
			nillCS.setDouble(8,nilTransaction.getTotNgsupAmt());
			nillCS.setDouble(9,nilTransaction.getTotCompAmt());
			nillCS.setString(10,nilTransaction.getSource());
			nillCS.setString(11,nilTransaction.getSourceId());
			nillCS.setString(12,nilTransaction.getFlags());
			nillCS.setString(13, nilTransaction.getCreatedBy());
			nillCS.setTimestamp(14, Timestamp.from(Instant.now()));
			nillCS.setString(15, nilTransaction.getCreationIPAddress());
			nillCS.setString(16, nilTransaction.getUpdatedBy());
			nillCS.setTimestamp(17, Timestamp.from(Instant.now()));
			nillCS.setString(18, nilTransaction.getUpdationIPAddress());
			
			nillCS.addBatch();
			
		}
			updateCount=nillCS.executeBatch().length;
			
			connection.commit();
			
		}
		catch(Exception e)
		{
			_Logger.info("nill transaction insertion exception for {} {} {}",nilTransactions.get(0).getGstin(),
					nilTransactions.get(0).getReturnType(),nilTransactions.get(0).getMonthYear());
			_Logger.error("sql exception ",e);
			updateCount=0;
			throw e;
		}
		_Logger.info("nill transaction insertion end for {} {} {}, records {} updated",nilTransactions.get(0).getGstin(),
				nilTransactions.get(0).getReturnType(),nilTransactions.get(0).getMonthYear(),updateCount);
		}
		return updateCount;
	}
}
