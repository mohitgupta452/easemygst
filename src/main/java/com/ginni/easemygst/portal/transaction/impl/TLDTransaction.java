package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.IMPG;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.TldTransaction;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class TLDTransaction extends TransactionUtil implements Transaction {

	int _POS_INDEX = 0;
	
	/*int _ELIGIBILITY_TAX_INDEX = 12;
	int _IGST_TAX_AVAIL_INDEX = 13;
	int _CESS_TAX_AVAIL_INDEX = 14;*/
	
	public TLDTransaction(){
		 _SNO_INDEX = 1;
		 _HSN_CODE_INDEX = 2;
		 _DESCRIPTION_INDEX = 3;
		 _UNIT_INDEX = 4;
		 _QUANTITY_INDEX = 5;
		 _TAXABLE_VALUE_INDEX = 6;//advance paid in tld
		 _TAX_RATE_INDEX = 7;
		 _IGST_AMT_INDEX = 8;
		 _CGST_AMT_INDEX = 9;
		 _SGST_AMT_INDEX = 10;
		 _CESS_AMT_INDEX = 11;
	}

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	public static List<TldTransaction> addTLDObject(TldTransaction tld, List<TldTransaction> tlds) {
		/*List<InvoiceItem> invoiceItems = new ArrayList<>();
		List<InvoiceItem> tInvoiceItems = impg.getImpgInvItms();
		if (impgs.contains(impg)) {
			impg = impgs.get(impgs.indexOf(impg));
			invoiceItems = impg.getImpgInvItms();
			for (InvoiceItem tInvoiceItem : tInvoiceItems) {
				if (!invoiceItems.contains(tInvoiceItem)) {
					invoiceItems.add(tInvoiceItem);
				}
			}

		} else {
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			impgs.add(impg);
		}*/
		Item item=tld.getItems().get(0);
		List<Item> items;
		
		if(tlds.contains(tld)){
			tld=tlds.get(tlds.indexOf(tld));
			items=tld.getItems();
			if(!items.contains(item))
				items.add(item);
			
		}else{
			tlds.add(tld);
		}

		return tlds;
	}

	public static List<IMPG> updateIMPGObject(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = impg.getImpgInvItms();

		String id = impg.getId();
		List<IMPG> searchedImpgs = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(a.getId())).collect(Collectors.toList());

		if (searchedImpgs.isEmpty()) {// to check if the gstin exist
			if (impgs.contains(impg)) {
				searchedImpgs.add(impgs.get(impgs.indexOf(impg)));
				impg.setId(searchedImpgs.get(0).getId());

			}
		}

		if (!searchedImpgs.isEmpty()) {// update existing impg

			IMPG existingImpg = searchedImpgs.get(0);
			items = existingImpg.getImpgInvItms();
			if (!items.isEmpty())
				items.clear();
			for (InvoiceItem tItem : tItems) {
				if (items.contains(tItem)) {// update item
					items.remove(tItem);
					items.add(tItem);
				} else
					items.add(tItem);// add
				if (impg.isGstnSynced())
					impg.setTaxPayerAction(TaxpayerAction.MODIFY);
				impg.setGstnSynced(false);

			}
			impg.setImpgInvItms(items);
			impgs.remove(existingImpg);
			impgs.add(impg);
		} else {// add new impg
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			impg.setGstnSynced(false);
			impgs.add(impg);
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGObjectByNo(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = impg.getImpgInvItms();
		String boeNum = impg.getBillOfEntryNum();
		List<IMPG> searchedImpgs = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(boeNum) && boeNum.equalsIgnoreCase(a.getBillOfEntryNum()))
				.collect(Collectors.toList());
		if (!searchedImpgs.isEmpty()) { // update existing impg

			IMPG existingImpg = searchedImpgs.get(0);
			items = existingImpg.getImpgInvItms();

			for (InvoiceItem tItem : tItems) {
				if (items.contains(tItem)) {// update item items.remove(tItem);
					items.add(tItem);
				} else {
					items.add(tItem);// add item
				}
				if (impg.isGstnSynced()) {
					impg.setTaxPayerAction(TaxpayerAction.MODIFY);
					impg.setGstnSynced(false);
				}
			}
			impg.setImpgInvItms(items);
			impgs.remove(existingImpg);
			impg.setId(existingImpg.getId());
			impgs.add(impg);
		} else {
			if (StringUtils.isEmpty(impg.getId()))
				impg.setId(getRandomUniqueId());
			if (impg.isGstnSynced()) {
				impg.setTaxPayerAction(TaxpayerAction.MODIFY);
				impg.setGstnSynced(false);
			}
			impgs.add(impg);
		}
		return impgs;
	}

	public static List<IMPG> deleteIMPGObject(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> items;
		String impgId = impg.getId();
		List<IMPG> searchedImpg = impgs.stream()
				.filter(a -> !StringUtils.isEmpty(impgId) && impgId.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedImpg.isEmpty()) {

			items = searchedImpg.get(0).getImpgInvItms();
			InvoiceItem item = impg.getImpgInvItms().get(0);
			if (item.getSNo() != null && items.contains(item)) {
				action = items.remove(item);
				searchedImpg.get(0).setTaxPayerAction(TaxpayerAction.DELETE);//
			}

			if (action == false) {
				if (searchedImpg.get(0).isGstnSynced()) {
					searchedImpg.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedImpg.get(0).setGstnSynced(false);
				} else {
					action = impgs.remove(searchedImpg.get(0));

				}
				action = true;

			}
		}

		return impgs;
	}

	public static List<IMPG> updateIMPGItcData(IMPG impg, List<IMPG> impgs) {

		List<InvoiceItem> invoiceItems;
		String bId = impg.getId();
		List<IMPG> searchedImpg = impgs.stream()
				.filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId())).collect(Collectors.toList());

		if (!searchedImpg.isEmpty()) {

			invoiceItems = searchedImpg.get(0).getImpgInvItms();
			List<InvoiceItem> invItems = impg.getImpgInvItms();

			for (InvoiceItem item : invItems) {
				if (item.getSNo() != null) {
					if (invoiceItems.indexOf(item) > -1) {
						InvoiceItem tempItem = invoiceItems.get(invoiceItems.indexOf(item));

						String elg = item.getEligOfTotalTax();
						if (!StringUtils.isEmpty(elg)) {
							if (elg.equals("no")) {
								tempItem.setEligOfTotalTax("no");
							} else if (elg.equalsIgnoreCase("ip") || elg.equalsIgnoreCase("cp")) {
								tempItem.setEligOfTotalTax(elg);
								Double igstTemp = tempItem.getIgstAmt();
								Double cgstTemp = tempItem.getCgstAmt();
								Double sgstTemp = tempItem.getSgstAmt();
								Double cessTemp = tempItem.getCessAmt();

								ItcDetail itcDetail = new ItcDetail();

								if (igstTemp != null && igstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalIgst(igstTemp);
								}

								if (cgstTemp != null && cgstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalCgst(cgstTemp);
								}

								if (sgstTemp != null && sgstTemp >= 0.0) {
									itcDetail.setTotalTaxAvalSgst(sgstTemp);
								}

								if (cessTemp != null && cessTemp >= 0.0) {
									itcDetail.setTotalTaxAvalCess(cessTemp);
								}

								tempItem.setItcDetails(itcDetail);
							}
						}
					}
				}

			}
		}

		return impgs;

	}

	public static List<TldTransaction> addIMPGObject(List<TldTransaction> existingB2bs, List<TldTransaction> transactionB2bs) {

		for (TldTransaction impg : transactionB2bs) {
			existingB2bs = addTLDObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGObject(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = updateIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> deleteIMPGObject(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = deleteIMPGObject(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGObjectByNo(List<IMPG> existingB2bs, List<IMPG> transactionB2bs) {

		for (IMPG impg : transactionB2bs) {
			existingB2bs = updateIMPGObjectByNo(impg, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<IMPG> updateIMPGItcData(List<IMPG> existingImpgs, List<IMPG> transactionImpgs)
			throws AppException {

		for (IMPG impg : transactionImpgs) {
			existingImpgs = updateIMPGItcData(impg, existingImpgs);
		}

		return existingImpgs;
	}

	@Override
	public int  processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException{

		List<TldTransaction> tlds = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.EXCEL.toString();

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);

					// convert list to impg object
					index++;
					convertListToTransactionData(ee, tlds, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin);

				}

			}

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			JSONObject output = new JSONObject();
			output.put("data", JsonMapper.objectMapper.writeValueAsString(tlds));
			output.put("size", index);

			//return output;
			return 0;

		} catch (JsonProcessingException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		List<TldTransaction> tlds = new ArrayList<>();
		  String gstin=taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					// convert list to impg object
					index++;
					convertListToTransactionData(ee, tlds, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin);
				}

			}
			scanner.close();

			if (ee.isError() || ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			JSONObject output = new JSONObject();
			output.put("data", JsonMapper.objectMapper.writeValueAsString(tlds));
			output.put("size", index);

			//return output;
			return 0;

		} catch (JsonProcessingException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	
	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		return 0;
	}
	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			
			_POS_INDEX = Integer.valueOf(headers[0]);			
			_SNO_INDEX = Integer.valueOf(headers[1]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[2]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[3]);
			_UNIT_INDEX = Integer.valueOf(headers[4]);
			_QUANTITY_INDEX = Integer.valueOf(headers[5]);
			_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[6]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[7]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[8]);
			 _CGST_AMT_INDEX = Integer.valueOf(headers[9]);
			 _SGST_AMT_INDEX = Integer.valueOf(headers[10]);
			_CESS_AMT_INDEX = Integer.valueOf(headers[11]);
			/*_ELIGIBILITY_TAX_INDEX = Integer.valueOf(headers[13]);
			_IGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[14]);
			_CESS_TAX_AVAIL_INDEX = Integer.valueOf(headers[15]);
*/			
		
			

		}

	}

	private void convertListToTransactionData(ExcelError ee, List<TldTransaction> tlds, List<String> temp, int index,
			String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos, String gstin) {

		ee.resetFieldsExceptErrorAndErrorDesc();
		
		ee.setUniqueRowValue("POS - ["+ temp.get(_POS_INDEX)+"]");
		
		TldTransaction tld=new TldTransaction();
		//IMPG impg = new IMPG();
		tld.setSource(sourceName);
		tld.setSourceId(sourceId);
		Item item = new Item();
		List<Item> items = new ArrayList<>();
		items.add(item);
		tld.setItems(items);
		//impg.setImpgInvItms(items);

		
	//	this.setIsIgst(sheetName, gstinPos, gstin, null, ee);
		
		
		// pos
				
		ee = this.validatePos(sheetName, _POS_INDEX, index, temp.get(_POS_INDEX), gstinPos, ee);
		if (!ee.isError()) {
			tld.setPos(String.valueOf(ee.getPosValue()));
		}
		 this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item,TransactionType.TLD);
		/*// setting serial no
		ee = this.validateSno(sheetName, _SNO_INDEX, index, temp.get(_SNO_INDEX), ee);
		if (!ee.isError()&&!StringUtils.isEmpty(temp.get(_SNO_INDEX)))
		invoiceItem.setSNo(Integer.parseInt(temp.get(_SNO_INDEX)));
		// setting goods and services code
		invoiceItem.setGoodsOrServiceCode(temp.get(_HSN_CODE_INDEX));

		// setting description
		invoiceItem.setHsnDesc(temp.get(_DESCRIPTION_INDEX));

		ee = this.validateHsnOrDesc(sheetName, _HSN_CODE_INDEX, index, temp.get(_HSN_CODE_INDEX),
				temp.get(_DESCRIPTION_INDEX), ee);

		// setting unit
		invoiceItem.setUnit(temp.get(_UNIT_INDEX));

		// setting quantity
		ee = this.validateQuantity(sheetName, _QUANTITY_INDEX, index, temp.get(_QUANTITY_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_QUANTITY_INDEX)))
			invoiceItem.setQuantity(Double.parseDouble(temp.get(_QUANTITY_INDEX)));

		// setting taxable value
		ee = this.validateTaxableValue(sheetName, _TAXABLE_VALUE_INDEX, index, temp.get(_TAXABLE_VALUE_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAXABLE_VALUE_INDEX)))
			invoiceItem.setTaxableValue(Double.parseDouble(temp.get(_TAXABLE_VALUE_INDEX)));

		// SETTING tax rate
		ee = this.validateTaxRate(sheetName, _TAX_RATE_INDEX, index, temp.get(_TAX_RATE_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAX_RATE_INDEX)))
			invoiceItem.setTaxRate(Double.parseDouble(temp.get(_TAX_RATE_INDEX)));

		// setting igst amt
		ee = this.validateIgstAmt(sheetName, _IGST_AMT_INDEX, index, temp.get(_IGST_AMT_INDEX),
				invoiceItem.getTaxRate(), invoiceItem.getAdvanceAmount(), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_IGST_AMT_INDEX)))
			invoiceItem.setIgstAmt(Double.parseDouble(temp.get(_IGST_AMT_INDEX)));

		// setting Cess amt
		ee = this.validateCessAmount(sheetName, _CESS_AMT_INDEX, index, temp.get(_CESS_AMT_INDEX),
				invoiceItem.getTaxableValue(), invoiceItem.getTaxRate(), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CESS_AMT_INDEX)))
			invoiceItem.setCessAmt(Double.parseDouble(temp.get(_CESS_AMT_INDEX)));
*/
		/*// following serial no can be changed
		ee = this.validateItcEligibility(sheetName, _ELIGIBILITY_TAX_INDEX, index, temp.get(_ELIGIBILITY_TAX_INDEX),
				ee);
		if (!ee.isError())
			invoiceItem.setEligOfTotalTax(temp.get(_ELIGIBILITY_TAX_INDEX));

		// following serial no can be changed
		ee = this.validateTaxAmt(sheetName, _IGST_TAX_AVAIL_INDEX, index, temp.get(_IGST_TAX_AVAIL_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_IGST_TAX_AVAIL_INDEX)))
			itcDetail.setTotalTaxAvalIgst(Double.parseDouble(temp.get(_IGST_TAX_AVAIL_INDEX)));

		// following serial no can be changed
		ee = this.validateTaxAmt(sheetName, _CESS_TAX_AVAIL_INDEX, index, temp.get(_CESS_TAX_AVAIL_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CESS_TAX_AVAIL_INDEX)))
			itcDetail.setTotalTaxAvalCess(Double.parseDouble(temp.get(_CESS_TAX_AVAIL_INDEX)));
*/
		if (!ee.isError())
			addTLDObject(tld, tlds);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<IMPG>>() {
				});
				List<IMPG> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<IMPG>>() {
				});

				if (action == null) {
					//existingB2bs = addIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteIMPGObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateIMPGObjectByNo(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.UPDATE_ITC) {
					existingB2bs = updateIMPGItcData(existingB2bs, transactionB2bs);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<IMPG> impgs = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<IMPG>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			Date startDate = new Date();

			double totalTaxAmount = 0.0;
			double amTaxAmount = 0.0;
			double amTaxValue = 0.0;
			double totalTaxableValue = 0.0;

			double igstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (IMPG impg : impgs) {

				double taxableValueInv = 0.0;
				double taxAmountInv = 0.0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;

				// shipping bill date
				Date billDate = impg.getBillOfEntryDate();
				String sbDateError = "";
				if (inputMonth.getMonth() < 9) {
					startDate.setDate(1);
					startDate.setMonth(3);
					startDate.setYear(inputMonth.getYear() - 1);
					sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
							+ sdf.format(inputMonth);
				} else {
					startDate.setDate(1);
					startDate.setMonth(3);
					startDate.setYear(inputMonth.getYear());
					sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
							+ sdf.format(inputMonth);
				}
				if (!Objects.isNull(billDate)
						&& !(billDate.compareTo(startDate) >= 0 && billDate.compareTo(inputMonth) <= 0)) {
					errors.put("BILL_DATE", sbDateError);
					errorFlag = false;
				}
				/*
				 * if (!StringUtils.isEmpty(impg.getOrgiBillOfEntryNum()) &&
				 * impg.getBillOfEntryDate() != null) { impg.setAmendment(true);
				 * } else { impg.setAmendment(false); }
				 */

				impg.setError(errors);
				impg.setValid(errorFlag);

				invoiceCount++;

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}

				/*
				 * List<ImpgInvItem> items = impg.getImpgInvItms(); if
				 * (!Objects.isNull(items)) { int itemCount = 0; for
				 * (InvoiceItem it : items) { itemCount++; it.setSNo(itemCount);
				 * 
				 * if (!Objects.isNull(it.getTaxableValue())) taxableValueInv +=
				 * it.getTaxableValue(); if (!Objects.isNull(it.getIgstAmt())) {
				 * taxAmountInv += it.getIgstAmt(); igstAmount +=
				 * it.getIgstAmt(); }
				 * 
				 * if (!Objects.isNull(it.getCessAmt())) { taxAmountInv +=
				 * it.getCessAmt(); cessAmount += it.getCessAmt(); }
				 * 
				 * } }
				 */
				impg.setTaxAmount(taxAmountInv);
				impg.setTaxableValue(taxableValueInv);
				if (impg.isAmendment()) {
					//
				}

				if (!impg.isValid()) {
					impg.setType(ReconsileType.ERROR);
					impg.setFlags("");

				}

				// impg.setFlags(flags.toString());
				setImpgFlag(impg);

				if (impg.isAmendment()) {
					amTaxAmount += taxAmountInv;
					amTaxValue += taxableValueInv;
				} else {
					totalTaxAmount += taxAmountInv;
					totalTaxableValue += taxableValueInv;
				}

			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", impgs);
			TransactionDataDTO impgSumm = new TransactionDataDTO();
			impgSumm.setTaxAmount(totalTaxAmount);
			impgSumm.setAmTaxAmount(amTaxAmount);
			impgSumm.setTaxableValue(totalTaxableValue);
			impgSumm.setAmTaxableValue(amTaxValue);
			impgSumm.setReconcileDTO(reconcileDTO);
			impgSumm.setError(summaryError);
			impgSumm.setCessAmount(cessAmount);
			impgSumm.setIgstAmount(igstAmount);
			gstrSummaryDTO.setImpg(impgSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setImpgFlag(IMPG impg) {
		if (!Objects.isNull(impg.getTaxPayerAction())) {
			if (impg.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				impg.setFlags(TableFlags.ACCEPTED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.REJECT)
				impg.setFlags(TableFlags.REJECTED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.PENDING)
				impg.setFlags(TableFlags.PENDING.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.MODIFY)
				impg.setFlags(TableFlags.MODIFIED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.DELETE)
				impg.setFlags(TableFlags.DELETED.toString());
			else if (impg.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				impg.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	public List<List<String>> getDataByType(String object, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> impgs = objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
				});

				List<IMPG> dataInvoices;
				if (fetchType.equals(DataFetchType.INVALID)) {
					dataInvoices = impgs.stream().filter(b -> b.isValid() == false).collect(Collectors.toList());
				} else if (fetchType.equals(DataFetchType.VALID)) {
					dataInvoices = impgs.stream().filter(b -> b.isValid()).collect(Collectors.toList());
				} else
					dataInvoices = impgs;
				for (IMPG impg : dataInvoices) {
					// Note:-please add the column in columns list in the same
					// sequence as they are in excel file
					for (InvoiceItem item : impg.getImpgInvItms()) {
						List<String> columns = this.getFlatData(impg, item, returnType);

						tabularData.add(columns);
					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	public List<String> getFlatData(IMPG impg, InvoiceItem item, String returnType) {
		List<String> columns = new ArrayList<>();
		columns.add(StringUtils.isEmpty(impg.getGstin()) ? "" : impg.getGstin());
		columns.add(StringUtils.isEmpty(impg.getBillOfEntryNum()) ? "" : impg.getBillOfEntryNum());
		columns.add(impg.getBillOfEntryDate() == null ? "" : sdf.format(impg.getBillOfEntryDate()));
		columns.add(item.getGoodsOrServiceCode() == null ? "" : item.getGoodsOrServiceCode());
		columns.add(item.getHsnDesc() == null ? "" : item.getHsnDesc());
		columns.add(StringUtils.isEmpty(item.getUnit()) ? "" : item.getUnit());
		columns.add(item.getQuantity() == null ? "" : String.valueOf(item.getQuantity()));
		columns.add(item.getTaxableValue() == null ? "" : String.valueOf(item.getTaxableValue()));
		columns.add(item.getTaxRate() == null ? "" : String.valueOf(item.getTaxRate()));
		columns.add(item.getIgstAmt() == null ? "" : String.valueOf(item.getIgstAmt()));
		columns.add(item.getCessAmt() == null ? "" : String.valueOf(item.getCessAmt()));
		ItcDetail itcDetails = new ItcDetail();
		itcDetails = item.getItcDetails();
		columns.add(StringUtils.isEmpty(item.getEligOfTotalTax()) ? "" : String.valueOf(item.getEligOfTotalTax()));
		columns.add(itcDetails.getTotalTaxAvalIgst() == null ? "" : String.valueOf(itcDetails.getTotalTaxAvalIgst()));
		columns.add(itcDetails.getTotalTaxAvalCess() == null ? "" : String.valueOf(itcDetails.getTotalTaxAvalCess()));

		return columns;
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<IMPG> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<IMPG>>() {
				});

				data.removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;

	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getImpg();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<IMPG> datas = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				datas = objectMapper.readValue(input, new TypeReference<List<IMPG>>() {
				});

				if (subType.equals(ReconsileSubType.PENDING_ITC.toString())) {
					/*
					 * datas.removeIf(i -> i.getReconcileFlags() == null ||
					 * !i.getReconcileFlags().contains(ReconsileSubType.OWN.
					 * toString()));
					 */
					for (IMPG impgInvoice : datas) {
						impgInvoice.getImpgInvItms()
								.removeIf(it -> it.getEligOfTotalTax() != null && (it.getEligOfTotalTax().equals("no")
										|| it.getEligOfTotalTax().equals("ip") || it.getEligOfTotalTax().equals("cp")));
					}
					datas.removeIf(in -> in.getImpgInvItms().isEmpty());

				} else {

					/*
					 * datas.removeIf(i -> i.getReconcileFlags() == null ||
					 * !i.getReconcileFlags().contains(subType));
					 */
				}

				return JsonMapper.objectMapper.writeValueAsString(datas);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceImpgs = getData(source, returnType);

		Map<String, List<String>> changedImpgs = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionImpgs = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceImpgs) && !Objects.isNull(changedImpgs)) {

			Iterator it = sourceImpgs.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedImpgs.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedImpgs.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedImpgs.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceImpgs.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionImpgs.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionImpgs.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionImpgs.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionImpgs;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<IMPG> impgs = objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
				});

				for (IMPG impg : impgs) {
					List<InvoiceItem> items = impg.getImpgInvItms();
					for (InvoiceItem item : items) {

						List<String> columns = getFlatData(impg, item, returnType);

						String temp = StringUtils.isEmpty(impg.getBillOfEntryNum()) ? ""
								: impg.getBillOfEntryNum() + ":" + item.getSNo() == null ? ""
										: String.valueOf(item.getSNo());
						tabularData.put(temp, columns);

					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		try {
			List<IMPG> impgs = JsonMapper.objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
			});

			List<IMPG> currentImpgs = new ArrayList<>();

			for (IMPG impg : impgs) {

				if (!impg.isAmendment() && !impg.isTransit()) {
					currentImpgs.add(impg);
					impg.setTransit(true);
					impg.setTransitId(transitId);
				}

			}

			//gstr2.setImpgs(currentImpgs);

			if (currentImpgs.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(impgs);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<IMPG> impgs = JsonMapper.objectMapper.readValue(object, new TypeReference<List<IMPG>>() {
			});
			for (IMPG impg : impgs) {
				if (impg.isTransit() && impg.getTransitId().equals(transitId)) {
					impg.setTransit(false);
					impg.setTransitId("");
					impg.setGstnSynced(true);
				}
			}
			return JsonMapper.objectMapper.writeValueAsString(impgs);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	public B2CLTransactionEntity saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action){
		
		return null;
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		return 0;
	}

}
