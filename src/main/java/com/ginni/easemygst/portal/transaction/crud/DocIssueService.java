package com.ginni.easemygst.portal.transaction.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;

public class DocIssueService {
	
	final  Logger _Logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	private static String docIssueInsert="INSERT INTO `doc_issue` (`gstin`, `monthYear`, `returnType`,`category`, `snoFrom`,"
			+ " `snoTo`, `totalNumber`, `canceled`,netIssued, `source`,`createdBy`,`creationTime`,"
			+ "`creationIpAddress`,`sourceId`,id)"
			+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\n";
	
	private static String docIssueDelete="DELETE from doc_issue  WHERE monthYear=? and gstin=? and returnType=?";
	
	public int insert(List<DocIssue> docIssues,String delete) throws SQLException
	{
		_Logger.debug("docIssue insertion start");
		int updateCount=0;
		
		boolean isFirst=false;

		try(
		Connection connection= DBUtils.getConnection();
		PreparedStatement docIssueDel=connection.prepareStatement(docIssueDelete);
		PreparedStatement docIssuePS=connection.prepareStatement(docIssueInsert);
				)
		{
           connection.setAutoCommit(false);	

       for(DocIssue dIssue: docIssues)
		{
			//hsnPS.setString(1,UUID.randomUUID().toString());
			docIssuePS.setInt(1,dIssue.getGstin().getId());
			docIssuePS.setString(2,dIssue.getMonthYear());
			docIssuePS.setString(3,dIssue.getReturnType());
			docIssuePS.setString(4,dIssue.getCategory());
			docIssuePS.setString(5,dIssue.getSnoFrom());
			docIssuePS.setString(6,dIssue.getSnoTo());
			docIssuePS.setInt(7,dIssue.getTotalNumber());
			docIssuePS.setInt(8,dIssue.getCanceled());
			docIssuePS.setInt(9,(dIssue.getTotalNumber()-dIssue.getCanceled()));
			docIssuePS.setString(10,dIssue.getSource());
			docIssuePS.setString(11,dIssue.getCreatedBy());
			docIssuePS.setTimestamp(12,Timestamp.from(Instant.now()));
			docIssuePS.setString(13,dIssue.getCreationIPAddress());
			docIssuePS.setString(14,dIssue.getSourceId());
			docIssuePS.setString(15,UUID.randomUUID().toString());

			
			docIssuePS.addBatch();
			
			
		}
       DocIssue dIssue=null;
       if(!docIssues.isEmpty())
    		   dIssue=docIssues.get(0);
       if("YES".equalsIgnoreCase(delete)&& !Objects.isNull(dIssue)) {
			docIssueDel.setString(1,dIssue.getMonthYear());
			docIssueDel.setInt(2, dIssue.getGstin().getId());
			docIssueDel.setString(3, dIssue.getReturnType());
			docIssueDel.executeUpdate();

   	}
		
		updateCount=docIssuePS.executeBatch().length;
		
		connection.commit();
		}
		catch(Exception e)
		{	
			_Logger.error("doc issue insertion  error {}",e);

			updateCount=0;
			throw e;
		}
		_Logger.debug("doc issue insertion end records {}",updateCount);

		return updateCount;
		
	}

}
