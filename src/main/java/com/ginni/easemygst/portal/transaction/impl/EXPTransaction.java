package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.ExpDetail;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;
import com.ginni.easemygst.portal.transaction.crud.B2BServcie;
import com.ginni.easemygst.portal.transaction.crud.EXPService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class EXPTransaction extends TransactionUtil implements Transaction {

	int _EXPORT_TYPE_INDEX = 0;
	int _INVOICE_NO_INDEX = 1;
	int _INVOICE_DATE_INDEX = 2;
	int PORT_CODE = 3;
	int _BILL_NO_INDEX = 4;
	int _BILL_DATE_INDEX = 5;
	
	int _ORIGINAL_INVOICE_NUMBER=14;
	int _ORIGINAL_INVOICE_DATE=15;

	public EXPTransaction() {

		_SNO_INDEX = 6;
		_HSN_CODE_INDEX = 7;
		_DESCRIPTION_INDEX = 8;
		_UNIT_INDEX = 9;
		_QUANTITY_INDEX = 10;
		_TAXABLE_VALUE_INDEX = 11;
		_TAX_RATE_INDEX = 12;
		_IGST_AMT_INDEX = 13;
		_ORIGINAL_INVOICE_NUMBER=14;
		_ORIGINAL_INVOICE_DATE=15;
		
		System.out.println("Inside EXP Constructor...");
	}
	List<String> temp=new ArrayList<String>();

	private boolean isDate_index;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private int _SUPPLY_VALUE_INDEX;

	private boolean isInvoice_number;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;


	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	/* No unique field in case of adding/updating item */
	public static List<EXP> addEXPObject(EXP exp, List<EXP> exps) {

		List<Invoice> allinvoice = exp.getInvoices();
		for (Invoice invoice2 : allinvoice) {
			List<Invoice> invoices = new ArrayList<>();
			List<InvoiceItem> invoiceItems = new ArrayList<>();

			if (exps.contains(exp)) {
				exp = exps.get(exps.indexOf(exp));
				invoices = exp.getInvoices();
				if (invoices.contains(invoice2)) {
					List<InvoiceItem> allinvoiceItem = invoice2.getItems();
					invoice2 = invoices.get(invoices.indexOf(invoice2));
					invoiceItems = invoice2.getItems();
					for (InvoiceItem invoiceItem2 : allinvoiceItem) {
						if (!invoiceItems.contains(invoiceItem2)) {
							invoiceItems.add(invoiceItem2);
							if (invoice2.isGstnSynced())
								invoice2.setTaxPayerAction(TaxpayerAction.MODIFY);
							invoice2.setGstnSynced(false);
						}
					}
				} else {
					String uuid = (StringUtils.isEmpty(invoice2.getId())) ? getRandomUniqueId() : invoice2.getId();
					invoice2.setId(uuid);
					invoice2.setGstnSynced(false);
					invoices.add(invoice2);
				}
			} else {
				for (Invoice inv : allinvoice) {
					String uuid = (StringUtils.isEmpty(inv.getId())) ? getRandomUniqueId() : inv.getId();
					inv.setId(uuid);
					inv.setGstnSynced(false);
				}
				String uuid = (StringUtils.isEmpty(exp.getId())) ? getRandomUniqueId() : exp.getId();
				exp.setId(uuid);
				exps.add(exp);
			}
		}
		return exps;

	}

	public static List<EXP> updateEXPObject(EXP exp, List<EXP> exps) throws AppException {

		List<Invoice> invoices = new ArrayList<>();
		List<Invoice> tInvoices = exp.getInvoices();
		EXP tempExp = exp;
		String id = exp.getId();
		List<EXP> searchedExps = exps.stream().filter(b -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());
		if (searchedExps.isEmpty()) {// to check if the gstin exist according to
										// handlle client side case
			if (exps.contains(exp)) {
				searchedExps.add(exps.get(exps.indexOf(exp)));
				exp.setId(searchedExps.get(0).getId());

			}
		}
		if (!searchedExps.isEmpty()) {// update existing exp
			EXP existingB2b = searchedExps.get(0);
			invoices = existingB2b.getInvoices();
			boolean isInvoiceUpdated = true;

			for (Invoice tInvoice : tInvoices) {
				String invId = tInvoice.getId();
				List<Invoice> searchedInvs = invoices.stream()
						.filter(b -> !StringUtils.isEmpty(invId) && invId.equalsIgnoreCase(b.getId()))
						.collect(Collectors.toList());
				if (!searchedInvs.isEmpty()) {// update existing
					Invoice existingInvoice = searchedInvs.get(0);
					invoices.remove(existingInvoice);
					if (tInvoice.isGstnSynced()) {
						tInvoice.setGstnSynced(false);
						tInvoice.setTaxPayerAction(TaxpayerAction.MODIFY);
					}

					// start-code to handle change in export type change
					// case:block-1
					if (!existingB2b.getExportType().toString().equalsIgnoreCase(exp.getExportType().toString())) {// if
																													// change
																													// in
																													// export
																													// type
																													// than
																													// it
																													// will
																													// create
																													// new
																													// exp
						if (!exps.contains(exp)) {// if export type does not
													// exist then set id empty
													// so that new id will be
													// genrated
							exp.setId("");

						}
						if (invoices.isEmpty())
							exps.remove(exp);
						exp.getInvoices().get(0).setId("");
						addEXPObject(exp, exps);// for creating new exp on
												// export type change
						isInvoiceUpdated = true;

					} else
						// End-code to handle change in export type case
						invoices.add(tInvoice);
				} else {// add new invoicerevisedInvDate
					if (StringUtils.isEmpty(tInvoice.getId()))
						if (invoices.contains(tInvoice)) {
							AppException ex = new AppException();
							ex.setCode(ExceptionCode._ERROR);
							ex.setMessage(
									"Invoice with same number already exist, Please choose a different Invoice no");
							throw ex;
						}
					tInvoice.setId(getRandomUniqueId());
					tInvoice.setGstnSynced(false);
					invoices.add(tInvoice);
				}

			}
			if (!isInvoiceUpdated) {
				exp.setInvoices(invoices);
				exps.remove(existingB2b);
				exps.add(exp);
			}
		} else {// add new exp
			if (StringUtils.isEmpty(exp.getId()))
				exp.setId(getRandomUniqueId());
			for (Invoice inv : exp.getInvoices()) {

				inv.setGstnSynced(false);
				if (StringUtils.isEmpty(inv.getId()))
					inv.setId(getRandomUniqueId());
				inv.setGstnSynced(false);
			}
			exps.add(exp);
		}

		return exps;
	}

	/* no unique field in case of updating item */
	public static List<EXP> updateEXPObjectByNo(EXP exp, List<EXP> exps) throws AppException {

		List<Invoice> invoices = new ArrayList<>();
		List<Invoice> tInvoices = exp.getInvoices();
		ExportType exportType = exp.getExportType();
		List<EXP> searchedExps = exps.stream()
				.filter(b -> !StringUtils.isEmpty(exportType.toString())
						&& exportType.toString().equalsIgnoreCase(b.getExportType().toString()))
				.collect(Collectors.toList());

		if (!searchedExps.isEmpty()) {// update existing exp
			EXP existingExp = searchedExps.get(0);
			invoices = existingExp.getInvoices();

			for (Invoice tInvoice : tInvoices) {
				String invNo = tInvoice.getSupplierInvNum();
				List<Invoice> searchedInvs = invoices.stream()
						.filter(b -> !StringUtils.isEmpty(invNo) && invNo.equalsIgnoreCase(b.getSupplierInvNum()))
						.collect(Collectors.toList());
				if (!searchedInvs.isEmpty()) {// update existing
					Invoice existingInvoice = searchedInvs.get(0);
					invoices.remove(existingInvoice);
					tInvoice.setId(existingInvoice.getId());

					invoices.add(tInvoice);
				} else {// add new invoice
					if (StringUtils.isEmpty(tInvoice.getId()))

						tInvoice.setId(getRandomUniqueId());
					tInvoice.setGstnSynced(false);
					invoices.add(tInvoice);
				}
				if (tInvoice.isGstnSynced()) {
					tInvoice.setTaxPayerAction(TaxpayerAction.MODIFY);
				}
				tInvoice.setGstnSynced(false);

			}
			exp.setInvoices(invoices);
			exps.remove(existingExp);
			exp.setId(existingExp.getId());
			exps.add(exp);
		} else {// add new exp
			if (StringUtils.isEmpty(exp.getId()))
				exp.setId(getRandomUniqueId());
			for (Invoice inv : exp.getInvoices()) {

				inv.setGstnSynced(false);
				if (StringUtils.isEmpty(inv.getId()))
					inv.setId(getRandomUniqueId());
				inv.setGstnSynced(false);
			}
			exps.add(exp);
		}

		return exps;
	}

	public static List<EXP> deleteEXPObject(EXP exp, List<EXP> exps) {
		InvoiceItem invoiceItem = exp.getInvoices().get(0).getItems().get(0);
		List<Invoice> invoices;
		List<InvoiceItem> invoiceItems = new ArrayList<>();
		String bId = exp.getId();
		List<EXP> searchedExp = exps.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedExp.isEmpty()) {
			// exp = searchedB2b.get(0);
			invoices = searchedExp.get(0).getInvoices();
			List<Invoice> searchedInv = invoices.stream()
					.filter(b -> exp.getInvoices() != null
							&& b.getId().equalsIgnoreCase(exp.getInvoices().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedInv.isEmpty()) {
				invoiceItems = searchedInv.get(0).getItems();
				if (exp.getInvoices().get(0).getItems() != null)
					invoiceItem = exp.getInvoices().get(0).getItems().get(0);
				if (invoiceItem.getSNo() != null && invoiceItems.contains(invoiceItem)) {
					action = invoiceItems.remove(invoiceItem);
					searchedInv.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedInv.get(0).setGstnSynced(false);
				}
				/*
				 * action = action == false ? invoices.remove(invoice) : true;
				 */
				if (action == false) {//
					if (searchedInv.get(0).isGstnSynced()) {
						searchedInv.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
						searchedInv.get(0).setGstnSynced(false);
					} else {
						action = invoices.remove(searchedInv.get(0));
						if (invoices.isEmpty())// will delete exp in case of no
												// invoices remaining
							exps.remove(searchedExp.get(0));
					}
					action = true;
				}

				/*
				 * action = action == false ?
				 * invoices.remove(searchedInv.get(0)) : true;
				 */
			}
			action = action == false ? exps.remove(searchedExp.get(0)) : true;
		}
		return exps;
	}

	public static List<EXP> addEXPObject(List<EXP> existingB2bs, List<EXP> transactionB2bs) {

		for (EXP exp : transactionB2bs) {
			existingB2bs = addEXPObject(exp, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<EXP> updateEXPObject(List<EXP> existingB2bs, List<EXP> transactionB2bs) throws AppException {

		for (EXP exp : transactionB2bs) {
			existingB2bs = updateEXPObject(exp, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<EXP> deleteEXPObject(List<EXP> existingB2bs, List<EXP> transactionB2bs) {

		for (EXP exp : transactionB2bs) {
			existingB2bs = deleteEXPObject(exp, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<EXP> updateEXPObjectByNo(List<EXP> existingB2bs, List<EXP> transactionB2bs) throws AppException {

		for (EXP exp : transactionB2bs) {
			existingB2bs = updateEXPObjectByNo(exp, existingB2bs);
		}

		return existingB2bs;
	}
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, SourceType source,String delete) throws AppException {

		// List<EXP> exps = new ArrayList<>();
		List<EXPTransactionEntity> exps = new ArrayList<>();

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		ExcelError ee = new ExcelError();

		int columnCount = 0;
		int index = 0;
		Iterator<Row> iterator = gstExcel.getSheet().iterator();
		while (iterator.hasNext()) {

			Row row = iterator.next();
			int rowNum = row.getRowNum();
			if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if(Objects.nonNull(values))
				columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
				List<String> temp = gstExcel.excelReadRow(cellRange);
				if (temp == null)
					break;
				// convert list to exp object
				index++;
				convertListToTransactionData(ee, exps, temp, index, sheetName, sourceName, sourceId, gstReturn,
						gstinPos, taxpayerGstin, monthYear, userBean);
			}

		}
		if (ee.isError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		}

		try {
			return new EXPService().insertOrUpdate(exps,delete);
		} catch (SQLException e) {
			_Logger.error("sql exception ", e);
			throw new AppException();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public int saveExpTransaction(List<EXPTransactionEntity> exps) throws AppException {
		try {
			return new EXPService().insertOrUpdate(exps,null);
		} catch (SQLException e) {
			_Logger.error("sql exception ", e);
			throw new AppException();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean,String delete,SourceType source) throws AppException {

		// List<EXP> exps = new ArrayList<>();
		List<EXPTransactionEntity> exps = new ArrayList<>();

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		ExcelError ee = new ExcelError();

		int index = 0;

		CSVParser csvParser = new CSVParser();

		boolean skipHeader = false;
		while (scanner.hasNext()) {
			List<String> temp = csvParser.parseLine(scanner.nextLine());
			if (skipHeader)
				skipHeader = false;
			else {
				// convert list to exp object
				if (temp == null)
					break;
				// convert list to exp object
				index++;
				convertListToTransactionData(ee, exps, temp, index, sheetName, sourceName, sourceId, gstReturn,
						gstinPos, taxpayerGstin, monthYear, userBean);
			}
		}
		scanner.close();

		if (ee.isError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		}

		try {
			return new EXPService().insertOrUpdate(exps,delete);
		} catch (SQLException e) {
			_Logger.error("sql exception ", e);
			throw new AppException();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public int saveTransaction(List<EXPTransactionEntity> exps) throws AppException {
		try {
			return new EXPService().insertOrUpdate(exps,null);
		} catch (SQLException e) {
			_Logger.error("sql exception ", e);
			throw new AppException();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	
	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		// List<EXP> exps = new ArrayList<>();
				List<EXPTransactionEntity> exps = new ArrayList<>();

				String gstin = taxpayerGstin.getGstin();
				String customHeaderIndex="";
				if(gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
					customHeaderIndex="0,1,2,3,4,5,6,7,8,9,10,11,12,13";
				else
					customHeaderIndex="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22";

				// set column mapping
				setColumnMapping(customHeaderIndex, gstReturn);

				String sourceId = Utility.randomString(8);
				SourceType sourceName = source;

				String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

				ExcelError ee = new ExcelError();

				int index = 0;

				
				for(UploadedCsv lineItem:lineItems)  {
					
						// convert list to exp object
						index=lineItem.getLineNumber();
						convertListToTransactionData(ee, exps, this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName, sourceName, sourceId, gstReturn,
								gstinPos, taxpayerGstin, monthYear, userBean);
				}

				if (ee.isError()) {
					AppException exp = new AppException();
					exp.setCode(ExceptionCode._ERROR);
					exp.setMessage(ee.getErrorDesc().toString());
					throw exp;
				}

				try {
					return new EXPService().insertOrUpdate(exps,delete);
				} catch (SQLException e) {
					_Logger.error("sql exception ", e);
					throw new AppException();
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
	}

	private List<String> convertLineItemIntoList(UploadedCsv lineItem,ReturnType returnType){
		List<String>fieldList=new ArrayList<>();
		
		String exportType=ExportType.WPAY.toString();
		
		if("OTHER OUTWARD SUPPLY".equalsIgnoreCase(lineItem.getGstApplicability()) &&
				StringUtils.isEmpty(lineItem.getTotalTaxrate())){
			exportType=ExportType.WOPAY.toString();
		}
		
		fieldList.add(exportType);
		
		fieldList.add(lineItem.getEntryNo());
		fieldList.add(lineItem.getEntryDate());
		fieldList.add("");//shipping bill code
		fieldList.add("");//shipping bill no
		fieldList.add(null);//shipping bill date
		fieldList.add("1");//	 for serial no
		fieldList.add(lineItem.getHsnSacCode());
		fieldList.add(lineItem.getHsnSacDesc());
		fieldList.add(lineItem.getUom());
		fieldList.add(lineItem.getQuantity());
		fieldList.add(ExportType.WOPAY.toString().equalsIgnoreCase(exportType)?lineItem.getNetAmount():lineItem.getTaxableAmount());
		fieldList.add(lineItem.getTotalTaxrate());
		fieldList.add(lineItem.getIgstAmount());
		//fieldList.add(lineItem.getCessAmount());
		
		_Logger.info("{}",fieldList);

		return fieldList;
	}
	
	

	
	
	public void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {
			String[] headers = headerIndex.split(",");

			_EXPORT_TYPE_INDEX = Integer.valueOf(headers[0]);
			_INVOICE_NO_INDEX = Integer.valueOf(headers[1]);
			_INVOICE_DATE_INDEX = Integer.valueOf(headers[2]);
			PORT_CODE = Integer.valueOf(headers[3]);
			_BILL_NO_INDEX = Integer.valueOf(headers[4]);
			_BILL_DATE_INDEX = Integer.valueOf(headers[5]);
			_SNO_INDEX = Integer.valueOf(headers[6]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[7]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[8]);
			_UNIT_INDEX = Integer.valueOf(headers[9]);
			_QUANTITY_INDEX = Integer.valueOf(headers[10]);
			_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[11]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[12]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[13]);

		}

	}

	public void convertListToTransactionData(ExcelError ee, List<EXPTransactionEntity> exps, List<String> temp,
			int index, String sheetName, SourceType sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {

		// EXP exp = new EXP();
		handleAmmendmentCase(temp, TransactionType.EXP);


		ee.setUniqueRowValue("InvoiceNo - [" + temp.get(_INVOICE_NO_INDEX) + "]");

		EXPTransactionEntity exp = new EXPTransactionEntity();
		ExpDetail expDetail = new ExpDetail();
		exp.setGstin(taxPayerGstin);
		exp.setReturnType(gstReturn);
		exp.setMonthYear(monthYear);
		if (exps.contains(exp)) {
			exp = exps.get(exps.indexOf(exp));
		} else {
			exps.add(exp);
		}
		expDetail.setExpTransaction(exp);
		expDetail.setSource(sourceName.toString());
		expDetail.setSourceId(sourceId);

		InfoService.setInfo(expDetail, userBean);
		InfoService.setInfo(exp, userBean);

		if (sourceName == SourceType.SAP)
			expDetail.setExportType(ExportType.WPAY.toString());
		else {
			ee = this.validateExportType(sheetName, _EXPORT_TYPE_INDEX, index, temp.get(_EXPORT_TYPE_INDEX), ee);
			if (!ee.isError()){
				expDetail.setExportType(temp.get(_EXPORT_TYPE_INDEX));
				}
			if(ExportType.WOPAY.toString().equalsIgnoreCase(temp.get(_EXPORT_TYPE_INDEX))&& sourceName!=SourceType.TALLY){
				ee.setSewopOrWopay(true);
			this.validateNoTaxAmt(sheetName, temp.get(_EXPORT_TYPE_INDEX), index, _IGST_AMT_INDEX, temp.get(_IGST_AMT_INDEX), _TAX_RATE_INDEX, temp.get(_TAX_RATE_INDEX), ee);
			}
		}

		// setting invoice number
		ee = this.validateInvoiceNo(sheetName, _INVOICE_NO_INDEX, index, temp.get(_INVOICE_NO_INDEX), ee, true);
		if (!ee.isError())
			expDetail.setInvoiceNumber(temp.get(_INVOICE_NO_INDEX));

		// setting invoice date
		ee = this.validateInvoiceDate(sheetName, _INVOICE_DATE_INDEX, index, temp.get(_INVOICE_DATE_INDEX), ee,
				monthYear,TransactionType.EXP);
		if (!ee.isError())
			expDetail.setInvoiceDate(ee.getDate());

		// setting shipping bill number
		ee = this.validateShippingBillNo(sheetName, PORT_CODE, index, temp.get(PORT_CODE), ee);
		if (!ee.isError())
			expDetail.setShippingBillPortCode(temp.get(PORT_CODE));
		// setting shipping bill number
		ee = this.validateShippingBillNo(sheetName, _BILL_NO_INDEX, index, temp.get(_BILL_NO_INDEX), ee);
		if (!ee.isError())
			expDetail.setShippingBillNo(temp.get(_BILL_NO_INDEX));

		// setting shipping bill date
		ee.setDate(null);
		if (!StringUtils.isEmpty(temp.get(_BILL_DATE_INDEX))) {
			ee = this.validateDate(sheetName, _BILL_DATE_INDEX, index, temp.get(_BILL_DATE_INDEX), ee);
			if (!ee.isError()) {
				this.validateExpDate(sheetName, _BILL_DATE_INDEX, index, expDetail.getInvoiceDate(), ee.getDate(), ee);
				expDetail.setShippingBillDate(ee.getDate());
			}
		}


		if ("gstr1".equalsIgnoreCase(gstReturn) && sourceName != SourceType.TALLY
				&& sourceName != SourceType.SAP ) {
			
			boolean isAmmendment=false;
			isAmmendment = this.isAmmendment(temp.get(_ORIGINAL_INVOICE_NUMBER), temp.get(_ORIGINAL_INVOICE_DATE), null,
					null, null, null, null);
			if(isAmmendment){
				this.validateInvoiceNo(sheetName, _ORIGINAL_INVOICE_NUMBER, index, temp.get(_ORIGINAL_INVOICE_NUMBER), ee, true);
			
				expDetail.setOriginalInvoiceNumber(temp.get(_ORIGINAL_INVOICE_NUMBER));
				expDetail.setAmmendment(true);

			
				this.validateOriginalInvoiceDate(sheetName, _ORIGINAL_INVOICE_DATE, index, temp.get(_ORIGINAL_INVOICE_DATE), ee, exp.getMonthYear(),TransactionType.EXP,true);
				expDetail.setAmmendment(true);

				if(!ee.isError()){
					expDetail.setOriginalInvoiceDate(ee.getOriginalInvoiceDate());
				}
			}
		}

		
		Item item = new Item();
		double taxAmount =0;
		ee.setIgst(true);
		if(sourceName==SourceType.TALLY)
			taxAmount=this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, taxPayerGstin.getGstin());
		else
		 taxAmount = this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item, TransactionType.EXP);

		if (exp.getExpDetails() == null) {
			exp.setExpDetails(new ArrayList<ExpDetail>());
			exp.getExpDetails().add(expDetail);
			List<Item> items = new ArrayList<>();
			items.add(item);
			expDetail.setItems(items);
			expDetail.setTaxableValue(item.getTaxableValue());
			expDetail.setTaxAmount(taxAmount);

		} else if (exp.getExpDetails().contains(expDetail)) {
			expDetail = exp.getExpDetails().get(exp.getExpDetails().indexOf(expDetail));
			expDetail.setTaxableValue(expDetail.getTaxableValue() + item.getTaxableValue());
			expDetail.setTaxAmount(expDetail.getTaxAmount() + taxAmount);
			expDetail.getItems().add(item);

		}

		else {
			exp.getExpDetails().add(expDetail);

			// expDetail.setInvoiceItems(invoiceItems);
			expDetail.setTaxableValue(item.getTaxableValue());
			expDetail.setTaxAmount(taxAmount);

			List<Item> items = new ArrayList<>();
			items.add(item);
			expDetail.setItems(items);
		}

	}

	private void validateExpDate(String sheetName, int _SHIPPING_DATE_INDEX, int index, Date invDate, Date shippingDate,
			ExcelError ee) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		if (shippingDate.compareTo(invDate) < 0) {
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index
					+ ",Shipping date should be greater than invoice date |");
			isError = true;
		}
		ee.setFinalError(isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<EXP> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<EXP>>() {
				});
				List<EXP> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<EXP>>() {
				});

				if (action == null) {
					existingB2bs = addEXPObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateEXPObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteEXPObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateEXPObjectByNo(existingB2bs, transactionB2bs);
				} else if (action != null) {
					// existingB2bs = changeStatusB2BObject(existingB2bs,
					// transactionB2bs, action);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<EXP> exps = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<EXP>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			/*
			 * Calendar cal = Calendar.getInstance(); Date currentDate =
			 * cal.getTime(); int currYear = currentDate.getYear();
			 * if(currentDate.before(inputMonth)) { inputMonth = currentDate; }
			 */

			Date startDate = new Date();

			double totalTaxableValue = 0.0;
			double totalTaxAmount = 0;
			double amTaxableValue = 0.0;
			double amTaxAmount = 0;

			double igstAmount = 0.0;
			double cgstAmount = 0.0;
			double sgstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (EXP exp : exps) {
				double taxableValue = 0.0;
				double taxAmount = 0;
				List<Invoice> invoices = exp.getInvoices();
				for (Invoice invoice : invoices) {
					double taxableValueInv = 0.0;
					double taxAmountInv = 0;
					Map<String, String> errors = new HashMap<>();
					boolean errorFlag = true;

					Double invoiceValue = invoice.getSupplierInvVal();

					Date invoiceDate = invoice.getSupplierInvDt();
					String dateError = "";
					if (inputMonth.getMonth() < 9) {
						startDate.setDate(1);
						startDate.setMonth(3);
						startDate.setYear(inputMonth.getYear() - 1);
						dateError = "Invalid invoice date, it should be between " + sdf.format(startDate) + " and "
								+ sdf.format(inputMonth);
					} else {
						startDate.setDate(1);
						startDate.setMonth(3);
						startDate.setYear(inputMonth.getYear());
						dateError = "Invalid invoice date, it should be between " + sdf.format(startDate) + " and "
								+ sdf.format(inputMonth);
					}
					if (!(invoiceDate.compareTo(startDate) >= 0 && invoiceDate.compareTo(inputMonth) <= 0)) {
						errors.put("INVOICE_DATE", dateError);
						errorFlag = false;
					}

					// shipping bill date
					Date shipBillDate = invoice.getShippingBillDate();
					String sbDateError = "";
					if (inputMonth.getMonth() < 9) {
						startDate.setDate(1);
						startDate.setMonth(3);
						startDate.setYear(inputMonth.getYear() - 1);
						sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
								+ sdf.format(inputMonth);
					} else {
						startDate.setDate(1);
						startDate.setMonth(3);
						startDate.setYear(inputMonth.getYear());
						sbDateError = "Invalid bill date, it should be between " + sdf.format(startDate) + " and "
								+ sdf.format(inputMonth);
					}
					if (!(shipBillDate.compareTo(startDate) >= 0 && shipBillDate.compareTo(inputMonth) <= 0)) {
						errors.put("SHIPPING_BILL_DATE", sbDateError);
						errorFlag = false;
					}

					// ammendment

					List<InvoiceItem> items = invoice.getItems();
					if (!Objects.isNull(items)) {
						for (InvoiceItem it : items) {
							taxableValueInv += it.getTaxableValue();
							if (!Objects.isNull(it.getIgstAmt())) {
								taxAmountInv += it.getIgstAmt();
								igstAmount += it.getIgstAmt();
							}
							if (!Objects.isNull(it.getCessAmt())) {
								taxAmountInv += it.getCessAmt();
								cessAmount += it.getCessAmt();

							}
						}
					}

					invoice.setTaxableValue(taxableValueInv);
					invoice.setTaxAmount(taxAmountInv);
					taxableValue += taxableValueInv;
					taxAmount += taxAmountInv;
					/*
					 * if (invoiceValue != (taxableValueInv + taxAmountInv)) {
					 * errors.put("INVOICE_VALUE",
					 * "Invalid invoice value, it must be equal to sum of taxabale and tax amount values"
					 * ); errorFlag = false; }
					 */

					invoice.setError(errors);
					invoice.setValid(errorFlag);
					invoiceCount++;

					if (!errorFlag) {
						summaryError = true;
						errorCount += 1;
					}
					if (invoice.isAmendment()) {
						amTaxableValue += taxableValueInv;
						amTaxAmount += taxAmountInv;
					} else {
						totalTaxableValue += taxableValueInv;
						totalTaxAmount += taxAmountInv;
					}

					if (invoice.isAmendment()) {

					}
					setInvoiceFlag(invoice);

					if (!invoice.isValid()) {
						invoice.setType(ReconsileType.ERROR);
						invoice.setFlags("");

					}
				}
				exp.setTaxableValue(taxableValue);
				exp.setTaxAmount(taxAmount);
				/*
				 * totalTaxableValue+=taxableValue; totalTaxAmount+=taxAmount;
				 */
			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", exps);
			TransactionDataDTO expSumm = new TransactionDataDTO();
			expSumm.setTaxableValue(totalTaxableValue);
			expSumm.setTaxAmount(totalTaxAmount);
			expSumm.setAmTaxableValue(amTaxableValue);
			expSumm.setAmTaxAmount(amTaxAmount);
			expSumm.setError(summaryError);
			expSumm.setCessAmount(cessAmount);
			expSumm.setIgstAmount(igstAmount);
			expSumm.setCgstAmount(cgstAmount);
			expSumm.setSgstAmount(sgstAmount);
			expSumm.setReconcileDTO(reconcileDTO);
			gstrSummaryDTO.setExp(expSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setInvoiceFlag(Invoice invoice) {
		if (!Objects.isNull(invoice.getTaxPayerAction())) {
			if (invoice.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				invoice.setFlags(TableFlags.ACCEPTED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.REJECT)
				invoice.setFlags(TableFlags.REJECTED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.PENDING)
				invoice.setFlags(TableFlags.PENDING.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.MODIFY)
				invoice.setFlags(TableFlags.MODIFIED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.DELETE)
				invoice.setFlags(TableFlags.DELETED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				invoice.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<ExpDetail>b2bDatas=datas;
				for (ExpDetail invoice : b2bDatas) {
					
					for(Item item:invoice.getItems()){
						List<String> columns = this.getFlatData(invoice,item, returnType);
						tabularData.add(columns);
					}
				}

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}

	
private List<String> getFlatData(ExpDetail invoice, Item item, String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		columns.add(this.getStringValue(invoice.getExportType()));
		columns.add(this.getStringValue(invoice.getInvoiceNumber()));
		columns.add(this.getStringValue(invoice.getInvoiceDate()));
		columns.add(this.getStringValue(invoice.getShippingBillPortCode()));
		columns.add(this.getStringValue(invoice.getShippingBillNo()));
		columns.add(this.getStringValue(invoice.getShippingBillDate()));
		
		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxableValue()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));


		return columns;

		
		}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);

	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<EXP> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<EXP>>() {
				});

				for (EXP exp : data) {
					exp.getInvoices().removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);
				}

				data.removeIf(i -> i.getInvoices().isEmpty());

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;

	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getExp();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceExps = getData(source, returnType);

		Map<String, List<String>> changedExps = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionExps = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceExps) && !Objects.isNull(changedExps)) {

			Iterator it = sourceExps.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedExps.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedExps.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedExps.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceExps.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionExps.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionExps.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionExps.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionExps;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<EXP> exps = objectMapper.readValue(object, new TypeReference<List<EXP>>() {
				});

				for (EXP exp : exps) {
					List<Invoice> dataInvoices = exp.getInvoices();
					for (Invoice invoice : dataInvoices) {

						for (InvoiceItem item : invoice.getItems()) {
							/*List<String> columns = getFlatData(exp, invoice, item, returnType);

							String temp = exp.getExportType() == null ? ""
									: exp.getExportType() + ":" + invoice.getSupplierInvNum() + ":"
											+ item.getGoodsOrServiceCode() == null
													? ""
													: String.valueOf(item.getGoodsOrServiceCode()) + ":"
															+ item.getTaxableValue() == null ? ""
																	: String.valueOf(item.getTaxableValue());

							tabularData.put(temp, columns);*/
						}

					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<EXP> exps = JsonMapper.objectMapper.readValue(object, new TypeReference<List<EXP>>() {
			});

			List<EXP> currentExps = new ArrayList<>();

			for (EXP exp : exps) {
				EXP expNew = new EXP();
				List<Invoice> invoicesNew = new ArrayList<>();
				expNew.setExportType(exp.getExportType());

				expNew.setInvoices(invoicesNew);
				List<Invoice> invoices = exp.getInvoices();
				for (Invoice invoice : invoices) {
					if (!invoice.isAmendment() && !invoice.isTransit()) {
						invoicesNew.add(invoice);
						invoice.setTransit(true);
						invoice.setTransitId(transitId);
					}
				}
				if (!expNew.getInvoices().isEmpty()) {
					currentExps.add(expNew);
				}
			}

			// gstr1.setExp(currentExps);

			if (currentExps.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(exps);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<EXP> exps = JsonMapper.objectMapper.readValue(object, new TypeReference<List<EXP>>() {
			});

			for (EXP exp : exps) {
				List<Invoice> invoices = exp.getInvoices();
				for (Invoice invoice : invoices) {
					if (invoice.isTransit() && invoice.getTransitId().equals(transitId)) {
						invoice.setTransit(false);
						invoice.setTransitId("");

						invoice.setGstnSynced(true);
					}
				}
			}

			return JsonMapper.objectMapper.writeValueAsString(exps);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private ReturnsService returnService;
	private CrudService crudService;
	private FinderService finderService;
	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return false;
			} else {
				gstinTransactionDTO.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "exps"));

				Gstr1Dto gstr=JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(), Gstr1Dto.class);;
				
				List<EXPTransactionEntity> transactionExps = gstr.getExps();
				for (EXPTransactionEntity expTran : transactionExps) {
					expTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					expTran.setReturnType(gstinTransactionDTO.getReturnType());
					
					expTran.setGstin((TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
					ExpDetail inv=expTran.getExpDetails().get(0);
					//inv.setExpTransaction(expTran);
					double taxableValue=0.0;
					double taxAmount=0.0;
					for(Item item:inv.getItems()){
						taxableValue+=item.getTaxableValue();
						taxAmount+=item.getTaxAmount();
						item.setInvoiceId(inv.getId());
					}
					inv.setTaxableValue(taxableValue);
					inv.setTaxAmount(taxAmount);
					if(StringUtils.isEmpty(inv.getId())){//create invoice
						/*List<EXPTransactionEntity> expSearch=crudService.findWithNamedQuery("ExpTransaction.getByGstinMonthyearReturntype",QueryParameter.with("taxPayerGstin", expTran.getGstin()).and("monthYear", gstinTransactionDTO.getMonthYear())
								.and("returnType",expTran.getReturnType()).parameters());
						if(expSearch.isEmpty()){*/
							/*inv.setB2bTransaction(b2bTran);
							crudService.create(b2bTran);*///use kaustub method here
							inv.setExpTransaction(expTran);
							InfoService.setInfo(expTran, gstinTransactionDTO.getUserBean());
							InfoService.setInfo(inv, gstinTransactionDTO.getUserBean());
							try {
								
								new EXPService().insertOrUpdate(transactionExps,null);
							} catch (SQLException e) {
								Log.error("exception in saving exp transaction from portal");
								e.printStackTrace();
							}
						/*}else{
							EXPTransactionEntity existingExp=expSearch.get(0);
							inv.setExpTransaction(existingExp);
							InfoService.setInfo(existingExp, gstinTransactionDTO.getUserBean());
							InfoService.setInfo(existingExp, gstinTransactionDTO.getUserBean());
							try {
								new EXPService().insertOrUpdate(transactionExps);
							} catch (SQLException e) {
								e.printStackTrace();
							}

							crudService.create(inv);
							crudService.update(existingExp);
						}
						//add new invoice
*/					}else{
						
						List<EXPTransactionEntity> expSearch=crudService.findWithNamedQuery("ExpTransaction.getByGstinMonthyearReturntype",QueryParameter.with("taxPayerGstin", expTran.getGstin()).and("monthYear", gstinTransactionDTO.getMonthYear())
								.and("returnType",expTran.getReturnType()).parameters());
						InfoService.setInfo(expTran, gstinTransactionDTO.getUserBean());
						ExpDetail existingInv=crudService.find(ExpDetail.class, inv.getId());
						if (existingInv.getIsTransit()) {
							AppException ae = new AppException();
							ae.setMessage("exp item is in transit state so can't be updated for the moment");
							_Logger.error("exception--exp item is in transit state so can't be updated for the moment ");
							throw ae;
						}
						//InfoService.setInfo(existingExp, gstinTransactionDTO.getUserBean());

						existingInv.setInvoiceNumber(inv.getInvoiceNumber());
						existingInv.setInvoiceDate(inv.getInvoiceDate());
						existingInv.setTaxableValue(inv.getTaxableValue());
						existingInv.setExportType(inv.getExportType());
						existingInv.setShippingBillPortCode(inv.getShippingBillPortCode());
						existingInv.setShippingBillNo(inv.getShippingBillNo());
						existingInv.setShippingBillDate(inv.getShippingBillDate());
						existingInv.setTaxAmount(inv.getTaxAmount());
						existingInv.setIsError(false);
						existingInv.setErrMsg("");
						existingInv.setFlags("");
						existingInv.setIsAmmendment(inv.getIsAmmendment());
						
						existingInv.setOriginalInvoiceDate(inv.getOriginalInvoiceDate());;
						existingInv.setOriginalInvoiceNumber(inv.getOriginalInvoiceNumber());;

						InfoService.setInfo(existingInv, gstinTransactionDTO.getUserBean());

						
						
						if(expSearch.isEmpty()){
							List<ExpDetail> expDetails=new ArrayList<>();
							expDetails.add(existingInv);
							
							expTran.setExpDetails(expDetails);
							existingInv.setExpTransaction(expTran);
							crudService.update(expTran);
							returnService.deleteItemsByInvoiceId(existingInv.getId());
							for(Item itm:inv.getItems()){
								itm.setInvoiceId(existingInv.getId());
								crudService.create(itm);
							}
						}else{
							EXPTransactionEntity existingExp=expSearch.get(0);
//							InfoService.setInfo(existingExp, gstinTransactionDTO.getUserBean());
							inv.setExpTransaction(existingExp);
							
							crudService.update(existingInv);
							//crudService.update(existingExp);
							returnService.deleteItemsByInvoiceId(existingInv.getId());
							
							for(Item itm:inv.getItems()){
								itm.setInvoiceId(existingInv.getId());
								crudService.create(itm);
							}
							
							
						}
						
						
						
					}
					
				}
				
				
				return true;
			}
		} catch (IOException e) {
			_Logger.error("exception in exp save traction",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		catch(AppException ae){
			throw ae;
		}
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
		gstExcel.loadSheet(10);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				
				if(temp!=null) {
//					System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
			}
		}
		return 0;
	}
}
