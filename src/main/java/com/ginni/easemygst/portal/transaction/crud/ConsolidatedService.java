package com.ginni.easemygst.portal.transaction.crud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;
import com.ginni.easemygst.portal.transaction.impl.ConsoldateCsvProcessor.ConsolidatedQuery;
import com.ginni.easemygst.portal.utils.CalanderCalculatorUtility;
import com.ginni.easemygst.portal.utils.Utility;

public class ConsolidatedService {
	
	public enum DATA_SEPARATOR{
		PIPE,COMMA
	}

	public static Logger log = LoggerFactory.getLogger(ConsolidatedService.class);

	/**
	 * @param gstinTransactionDTO
	 * @param dataUploadId
	 * @return get data from uploaded_csv as per trasaction
	 */

	private List getDataByReturnTypeTransaction(GstinTransactionDTO gstinTransactionDTO, String dataUploadId) {
		ReturnType rType = ReturnType.valueOf(gstinTransactionDTO.getReturnType());
		TransactionType type = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		ConsolidatedQuery query = ConsolidatedQuery.valueOf(rType.toString() + "_" + type.toString());
		QueryRunner run = new QueryRunner(DBUtils.getDataSource());
		ResultSetHandler<List<UploadedCsv>> handler = new BeanListHandler<UploadedCsv>(UploadedCsv.class);
		List<UploadedCsv> result = null;
		String gstin = gstinTransactionDTO.getTaxpayerGstin().getGstin();
		try {
			String queryWithParam = query.getQuery();

			result = run.query(queryWithParam.replace("[uploadDataInfoId]", dataUploadId), handler);
			log.debug("query genrated for trnsaction {} {} {}", rType.toString(), type.toString(), queryWithParam);
			if (!Objects.isNull(result))
				log.debug("process result gstin {} transaction {} size {}  return type {}", gstin, type, result.size(),
						rType);
			else
				log.debug("transactio size {}", 0);

		} catch (SQLException e) {
			log.error("sql exception", e);
		}

		return result;
	}

	/**
	 * @param gstinTransactionDto
	 * @param lineItems
	 * @param source
	 * @return
	 * @throws AppException
	 *             save data to transaction table
	 */
	private int saveData(GstinTransactionDTO gstinTransactionDto, List<UploadedCsv> lineItems, SourceType source)
			throws AppException {
		int count = 0;

		TaxpayerGstin taxpayerGstin = (TaxpayerGstin) EntityHelper.convert(gstinTransactionDto.getTaxpayerGstin(),
				TaxpayerGstin.class);
		TransactionFactory tf = new TransactionFactory(gstinTransactionDto.getTransactionType());
		if (!Objects.isNull(lineItems) && !lineItems.isEmpty()) {
			count = tf.processCsvDataConsolidated(gstinTransactionDto.getReturnType(), "", taxpayerGstin,
					gstinTransactionDto.getReturnType() + gstinTransactionDto.getTransactionType(),
					
					gstinTransactionDto.getMonthYear(),
					
//					Utility.getValidReturnPeriod(info.getMonthYear(), taxpayerGstin
//							.getFilePreferenceByFinancialYear(CalanderCalculatorUtility
//									.getFiscalYearByMonthYear(Utility
//											.convertStrToYearMonth(info.getMonthYear())));
					
					null, taxpayerGstin.isFlushData() ? "yes" : "no", source, lineItems);
		}
		return count;
	}

	/**
	 * @param gstinTransactionDTO
	 * @param dataUploadInfoId
	 * @param source
	 * @return
	 * @throws AppException
	 *             iterate through every transaction of gstr1 and gstr2
	 * @throws SQLException
	 */
	public int proecessConsolidateCsv(GstinTransactionDTO gstinTransactionDTO, String dataUploadInfoId,
			SourceType source) throws AppException, SQLException {

		String monthYear = gstinTransactionDTO.getMonthYear();

		boolean isError = false;
		StringBuilder errorMsgBuilder = new StringBuilder();

		isError = this.processConsolidateErrors(dataUploadInfoId, monthYear, errorMsgBuilder);

		int totalCount = 0;

		for (String returnTransaction : this.getDistinctTransactionAndReturns(Integer.parseInt(dataUploadInfoId))) {

			String[] returnsTransactions = StringUtils.split(returnTransaction, "-");
			TransactionType transactionType = TransactionType.valueOf(returnsTransactions[0]);
			ReturnType returnType = ReturnType.valueOf(returnsTransactions[1]);

			try {
				log.info("process start for  gstin : {}  transaction : {} , return {}",
						gstinTransactionDTO.getTaxpayerGstin(), transactionType, returnType);
				
				if(!Utility.isSubmitReturn(gstinTransactionDTO.getTaxpayerGstin().getId(), monthYear, returnType.toString()))
				{

				totalCount += this.processCosolidatedByTransaction(transactionType, returnType, monthYear,
						dataUploadInfoId, gstinTransactionDTO.getTaxpayerGstin(), source);
				}else {
					log.info("return already submitted for gstin : {}  transaction : {} , return {}",
							gstinTransactionDTO.getTaxpayerGstin(), transactionType, returnType);
				}
				
			} catch (AppException e) {
				createEcelError(errorMsgBuilder, e, returnType.toString(), transactionType.toString());
				isError = true;
			}
			log.info("process completed for  gstin : {}  transaction : {} , return {} ",
					gstinTransactionDTO.getTaxpayerGstin(), transactionType, returnType);
		}
		if (isError) {
			AppException ae = new AppException();
			ae.setMessage(errorMsgBuilder.toString());
			ae.setCode(ExceptionCode._FAILURE);
			throw ae;
		}

		return totalCount;
	}

	private List<String> getDistinctTransactionAndReturns(int datauploadINfoId) throws SQLException {

		List<String> transactionReturns = new ArrayList<>();

		String query = "select DISTINCT(CONCAT(type,'-',returnType)) from uploaded_csv u where u.uploadDataInfoId=? and u.isError=false";

		try (Connection connection = DBUtils.getConnection();
				PreparedStatement statement = connection.prepareStatement(query);) {

			statement.setInt(1, datauploadINfoId);

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {

				String returnTransaction = resultSet.getString(1);

				transactionReturns.add(returnTransaction);
			}
		}

		return transactionReturns;
	}

	/**
	 * @param gstinTransactionDTO
	 * @param dataUploadInfoId
	 *            set return type and transaction type as per rule defined in
	 *            procedure
	 * @throws SQLException
	 */
	public void callSetReturnTypeProcedure(String gstin, String monthYear, String dataUploadInfoId)
			throws SQLException {

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		log.debug("processing returns and transaction for uploaded csv tin {}", gstin);

		InputStream inputStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("sql/consolidated-returns-tagging.sql");

		BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
		String updateSql = buffer.lines().collect(Collectors.joining("\n"));

		updateSql = updateSql.replace("<%uploadDataInfoId%>", dataUploadInfoId).replace("<%monthYear%>", monthYear)
				.replace("<%gstnPos%>", gstinPos);

		try (Connection connection = DBUtils.getConnection(); Statement statement = connection.createStatement();) {

			String[] updates = updateSql.split(";");

			for (String update : updates) {

				log.debug("update query for tin {} , query {}", gstin, update);

				if (StringUtils.isNoneEmpty(update))
					statement.addBatch(update);

			}
			int updateCount = statement.executeBatch().length;

			log.debug("processed returns and transaction for uploaded csv tin {}  update count {}", gstin, updateCount);

		}

	}

	private void createEcelError(StringBuilder errorMsg, AppException ae, String returnType, String transactionType) {

		String errors = ae.getMessage();
		errors = processErrorMessage(errors, returnType, transactionType);
		errorMsg.append(errors);
	}

	public int processCosolidatedByTransaction(TransactionType tran, ReturnType returnType, String monthYear,
			String dataUploadInfoId, TaxpayerGstinDTO taxpayerGstinDto, SourceType source) throws AppException {
		try {

			log.info("consolidate process start for gstin : {}, transaction: {} , return :  {} , monthYear :  {}",
					taxpayerGstinDto.getGstin(), tran, returnType, monthYear);
			int count = 0;
			GstinTransactionDTO gstinTransactionDto = new GstinTransactionDTO();
			gstinTransactionDto.setReturnType(returnType.toString());
			gstinTransactionDto.setTransactionType(tran.toString());
			gstinTransactionDto.setMonthYear(monthYear);
			gstinTransactionDto.setTaxpayerGstin(taxpayerGstinDto);
			List result = this.getDataByReturnTypeTransaction(gstinTransactionDto, dataUploadInfoId);
			log.info("list values {}", ToStringBuilder.reflectionToString(gstinTransactionDto));
			count = this.saveData(gstinTransactionDto, result, source);
			

			log.info(
					"consolidate process end for gstin : {}, transaction: {} , return :  {} , monthYear :  {}, , process count : {} ....",
					taxpayerGstinDto.getGstin(), tran, returnType, monthYear, count);

			return count;
		}

		catch (AppException ae) {
			log.error(
					" error while process consolidated for gstin : {}, transaction: {} , return :  {} , monthYear :  {}",
					taxpayerGstinDto.getGstin(), tran, returnType, monthYear, ae);
			throw ae;
		}

	}

	/**
	 * @param fileName
	 * @param id
	 * @return
	 * @throws AppException
	 *             import consolidated excel to db table
	 */
	public boolean importExcelToDatabase(String fileName, int id,DATA_SEPARATOR separator) throws AppException {

		log.debug("uploading csv to db....");
		Statement stmt = null;
		try (Connection connection = DBUtils.getConnection();) {
			String deleteQuery = "DELETE FROM `uploaded_csv` WHERE uploadDataInfoId=" + id;
			Statement deleteStmt = connection.createStatement();
			deleteStmt.executeUpdate(deleteQuery);
			String varQuery = "set @row:=0;";
			Statement declareVariable = connection.createStatement();
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			String delimeters = " FIELDS  TERMINATED BY '|' LINES TERMINATED BY '\\n'";

			if(DATA_SEPARATOR.COMMA==separator) {
				delimeters="FIELDS  TERMINATED BY ',' LINES TERMINATED BY '\\n' IGNORE 1 LINES ";
			}
			String query = "LOAD DATA LOCAL INFILE '" + fileName + "' INTO TABLE uploaded_csv CHARACTER SET latin1 "
					+ delimeters
					+ " (ENTRY_SOURCE,	ENTRY_SITE,	ENTRY_DATE,	ENTRY_NO,	TRADETYPE,ENTRY_NETVALUE,	REF_DOC_NO,	REF_DOC_DATE,	"
					+ " GST_APPLICABILITY,	ETIN,	CP_GSTIN_NO,	CP_GSTIN_STATE_CODE,	CP_GSTIN_REGISTRATION_DATE,	"
					+ "CP_REGISTRATION_STATUS,	CP_NAME,	CP_PANCARD_NO,	CP_CONTACT_PERSON,	"
					+ " CP_MOBILE_NO,	CP_EMAIL,	CP_CLASS_NAME,	CP_AGENT_NAME,	CP_AGENT_CLASS_NAME,	ORIGINAL_INV_ENT_NO,	"
					+ "ORIGINAL_INV_ENT_DT,	ORIGINAL_INV_ENT_NETVALUE,	ORIGINAL_INV_ENT_REFNO,	ORIGINAL_INV_ENT_REFDT,	DNCN_REASON,	"
					+ "HSN_SAC_CODE,	HSN_SAC_DESC,	UOM,	CGST_RATE,	SGST_RATE,	IGST_RATE,	CESS_RATE,	TOTAL_TAXRATE,	"
					+ "quantity,	TAXABLE_AMOUNT,	CGST_AMOUNT,	SGST_AMOUNT,	IGST_AMOUNT,	CESS_AMOUNT,	NET_AMOUNT,"
					+ "	INPUT_ELIGIBILITY,	INPUT_CGST_AMOUNT,	INPUT_SGST_AMOUNT,	INPUT_IGST_AMOUNT,	INPUT_CESS_AMOUNT\n"
					+ ")  set id=null, uploadDataInfoId=" + id + ",lineNumber=@row:=@row+1;";

			declareVariable.executeQuery(varQuery);
			stmt.executeUpdate(query);

			log.debug("csv uploaded to db....");

		} catch (Exception e) {
			log.error("error occ {}", e);
			AppException ae = new AppException();
			ae.setMessage("Unable to import csv");
			throw ae;
		}
		return true;
	}

	private void lineNumberCalculator() throws SQLException {

		try (Connection connection = DBUtils.getConnection();
				Statement statement = connection.createStatement();
				PreparedStatement updateRowPs = connection.prepareStatement("update  uploaded_csv set tmpLineNumber=")

		) {

		}

	}

	private Map<String, Integer> getMapping(String returnType, String transactionType) {
		String mapString = getMappingJson(returnType, transactionType);
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		try {
			mapping = JsonMapper.objectMapper.readValue(mapString, Map.class);
		} catch (JsonParseException e) {
			log.error("error ocuured {} in getting consolidated mapping", e);
		} catch (JsonMappingException e) {
			log.error("error ocuured {} in getting consolidated mapping", e);

		} catch (IOException e) {
			log.error("error ocuured {} in getting consolidated mapping", e);

		}
		return mapping;

	}

	private String getMappingJson(String returnType, String transactionType) {
		Properties props = new Properties();
		InputStream inputStream = null;
		String fileName = "/portalconsolidated.properties";
		try {
			inputStream = DBUtils.class.getResourceAsStream(fileName);
			if (inputStream == null)
				throw new IOException();
			props.load(inputStream);
			String fields = props.getProperty(StringUtils.lowerCase(returnType + "." + transactionType));
			return fields;
		}

		catch (IOException e) {
			log.error("file not found with this name--{}", fileName);
		}

		return null;

	}

	private int getCosolidatedColumn(int portalColumnNo, String returnType, String transactionType) {
		Map<String, Integer> mapping = getMapping(returnType, transactionType);
		if (!Objects.isNull(mapping.get(String.valueOf(portalColumnNo))))
			return mapping.get(String.valueOf(portalColumnNo));
		else
			return 0;
	}

	private String processErrorMessage(String errorMsg, String returnType, String transactionType) {
		
		log.debug("error message before process {}", errorMsg);
		StringBuilder editedMessage = new StringBuilder();
		if (StringUtils.isNoneEmpty(errorMsg)) {
			String[] errorMessages = errorMsg.split("\\|");

			for (int i = 0; i < errorMessages.length; i++) {
				String[] msgs = errorMessages[i].split(",");

				int colNum = 0;
				try {
					colNum = Integer.parseInt(msgs[1]);
				} catch (Exception e) {
					log.error("error  on return type {} transaction type {} value {} error {}", returnType,
							transactionType, msgs[1], e);
				}

				int consolColumn = getCosolidatedColumn(colNum, returnType, transactionType);
				msgs[1] = String.valueOf(consolColumn + 1);
				String row = StringUtils.join(msgs, ",");
				editedMessage.append(row);
				editedMessage.append("|");

			}
		} else {
			editedMessage.append(returnType + "-" + transactionType
					+ ",-,-,Unable to process your data, please contact support team");
		}
		log.debug("error message after process {}", editedMessage.toString());

		return editedMessage.toString();

	}

	private boolean processConsolidateErrors(String dataUPloadInfoId, String monthYear, StringBuilder errorMsgBuider)
			throws SQLException {

		String findRecordsWithError = "select errMsg as 'errorMsg',lineNumber from uploaded_csv where isError=true and uploadDataInfoId="
				+ dataUPloadInfoId + " and monthYear='" + monthYear + "'";

		QueryRunner run = new QueryRunner(DBUtils.getDataSource());

		ResultSetHandler<List<UploadedCsv>> csvErrorRecordsRS = new BeanListHandler<UploadedCsv>(UploadedCsv.class);

		List<UploadedCsv> csvErrorRecords = run.query(findRecordsWithError, csvErrorRecordsRS);

		return generateErrorMsg(csvErrorRecords, errorMsgBuider);

	}

	private boolean generateErrorMsg(List<UploadedCsv> uploadedCsvs, StringBuilder errorMsgBuider) {

		boolean isError = false;
		for (UploadedCsv uploadedCsv : uploadedCsvs) {

			errorMsgBuider.append("-,-,").append(uploadedCsv.getLineNumber()).append(",")
					.append(uploadedCsv.getErrorMsg()).append("|");
			isError = true;
		}

		return isError;

	}

}
