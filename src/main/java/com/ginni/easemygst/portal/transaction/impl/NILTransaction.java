package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService.DeleteType;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.NIL;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.DocIssue;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.entity.transaction.NILTransactionEntity;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class NILTransaction extends TransactionUtil implements Transaction {

	int _INTRSTR_NIL_AMOUNT_INDEX = 0;
	int _INTRSTR_EXMPTD_AMOUNT_INDEX = 1;
	int _INTRSTR_NONGST_AMOUNT_INDEX = 2;
	int _INTRASTR_NIL_AMOUNT_INDEX = 3;
	int _INTRASTR_EXMPTD_AMOUNT_INDEX = 4;
	int _INTRASTR_NONGST_AMOUNT_INDEX = 5;

	int _INTRSTU_NIL_AMOUNT_INDEX = 6;
	int _INTRSTU_EXMPTD_AMOUNT_INDEX = 7;
	int _INTRSTU_NONGST_AMOUNT_INDEX = 8;
	int _INTRASTU_NIL_AMOUNT_INDEX = 9;
	int _INTRASTU_EXMPTD_AMOUNT_INDEX = 10;
	int _INTRASTU_NONGST_AMOUNT_INDEX = 11;

	int _INTRSTR_COMPOSITE_AMOUNT_INDEX = 1;
	int _INTRASTR_COMPOSITE_AMOUNT_INDEX = 5;

	int _TALLY_DESCRIPTION_INDEX = 0;
	int _TALLY_NILL_SUPPLIES_INDEX = 1;
	int _TALLY_EXEMPTED_INDEX = 2;
	int _TALLY_NON_GST_SUPPLY_INDEX = 3;
	int _TALLY_COMPOSITE_SUPPLY_INDEX = 1;

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	public static List<NIL> addNILObject(NIL nil, List<NIL> nils) {
		String uId = getRandomUniqueId();
		if (nils.contains(nil)) {
			nils.remove(nils.indexOf(nil));
			nil.setGstnSynced(false);
			nil.setId(uId);
			nils.add(nil);
		} else {
			nil.setGstnSynced(false);
			nil.setId(uId);
			nils.add(nil);
		}

		return nils;
	}

	public static List<NIL> updateNILObject(NIL nil, List<NIL> nils) {
		/*
		 * List<NilData> nilDatas = new ArrayList<>(); List<NilData> tNilDatas =
		 * nil.getNilDatas(); String id = nil.getId(); List<NIL> searchedNils =
		 * nils.stream().filter(a -> !StringUtils.isEmpty(id) &&
		 * id.equalsIgnoreCase(a.getId())) .collect(Collectors.toList());
		 * 
		 * if (searchedNils.isEmpty()) {// to check if the gstin exist according to //
		 * handlle client side case if (nils.contains(nil)) {
		 * searchedNils.add(nils.get(nils.indexOf(nil)));
		 * nil.setId(searchedNils.get(0).getId());
		 * 
		 * } }
		 * 
		 * if (!searchedNils.isEmpty()) {// update existing at
		 * 
		 * NIL existingNil = searchedNils.get(0); nilDatas = existingNil.getNilDatas();
		 * for (NilData tNilData : tNilDatas) { List<NilData> searchedNilData =
		 * nilDatas.stream() .filter(b ->
		 * b.getId().equalsIgnoreCase(tNilData.getId())).collect(Collectors. toList());
		 * 
		 * if (!searchedNilData.isEmpty()) {// update
		 * nilDatas.remove(searchedNilData.get(0)); if (nil.isGstnSynced())
		 * nil.setTaxPayerAction(TaxpayerAction.MODIFY); nil.setGstnSynced(false);
		 * nilDatas.add(tNilData);
		 * 
		 * } else {// add new if (StringUtils.isEmpty(tNilData.getId()))
		 * tNilData.setId(getRandomUniqueId()); nilDatas.add(tNilData);// add if
		 * (nil.isGstnSynced()) nil.setGstnSynced(false); }
		 * 
		 * } nil.setNilDatas(nilDatas); ; nils.remove(existingNil); nils.add(nil); }
		 * else {// add new at if (StringUtils.isEmpty(nil.getId()))
		 * nil.setId(getRandomUniqueId()); if (nil.isGstnSynced())
		 * nil.setGstnSynced(false); for (NilData nilData : nil.getNilDatas()) {//
		 * genrating ids for // items if (StringUtils.isEmpty(nilData.getId()))
		 * nilData.setId(getRandomUniqueId());
		 * 
		 * } nils.add(nil); }
		 */
		return nils;
	}

	public static List<NIL> updateNILObjectByNo(NIL nil, List<NIL> nils) {
		/*
		 * List<NilData> nilDatas = new ArrayList<>(); List<NilData> tNilDatas =
		 * nil.getNilDatas(); NilSupplyType supplyType = nil.getSupplyType(); List<NIL>
		 * searchedNils = nils.stream().filter(a -> supplyType == a.getSupplyType())
		 * .collect(Collectors.toList());
		 * 
		 * if (!searchedNils.isEmpty()) {// update existing at
		 * 
		 * NIL existingNil = searchedNils.get(0); nilDatas = existingNil.getNilDatas();
		 * for (NilData tNilData : tNilDatas) { List<NilData> searchedNilData =
		 * nilDatas.stream() .filter(b -> b.getGoodsOrService() ==
		 * tNilData.getGoodsOrService()) .collect(Collectors.toList());
		 * 
		 * if (!searchedNilData.isEmpty()) {// update
		 * nilDatas.remove(searchedNilData.get(0)); if (nil.isGstnSynced())
		 * nil.setTaxPayerAction(TaxpayerAction.MODIFY); nil.setGstnSynced(false); // id
		 * tNilData.setId(searchedNilData.get(0).getId());//copying id
		 * nilDatas.add(tNilData);
		 * 
		 * } else {// add new if (StringUtils.isEmpty(tNilData.getId()))
		 * tNilData.setId(getRandomUniqueId()); nilDatas.add(tNilData);// add if
		 * (nil.isGstnSynced()) nil.setGstnSynced(false); }
		 * 
		 * }
		 * 
		 * nil.setId(existingNil.getId()); nil.setNilDatas(nilDatas); ;
		 * nils.remove(existingNil); nils.add(nil); } else {// add new nil if
		 * (StringUtils.isEmpty(nil.getId())) nil.setId(getRandomUniqueId()); if
		 * (nil.isGstnSynced()) nil.setGstnSynced(false); for (NilData nilData :
		 * nil.getNilDatas()) {// genrating ids for // items if
		 * (StringUtils.isEmpty(nilData.getId())) nilData.setId(getRandomUniqueId());
		 * 
		 * } nils.add(nil); }
		 */

		return nils;
	}

	public static List<NIL> deleteNILObject(NIL nil, List<NIL> nils) {
		/*
		 * List<NilData>nilDatas; String nilId = nil.getId(); List<NIL> searchedNil =
		 * nils.stream() .filter(a -> !StringUtils.isEmpty(nilId) &&
		 * nilId.equalsIgnoreCase(a.getId())) .collect(Collectors.toList());
		 * 
		 * boolean action = false; if (!searchedNil.isEmpty()) { nilDatas =
		 * searchedNil.get(0).getNilDatas(); List<NilData> searchedNilData =
		 * nilDatas.stream() .filter(b -> nil.getNilDatas() != null &&
		 * b.getId().equalsIgnoreCase(nil.getNilDatas().get(0).getId()))
		 * .collect(Collectors.toList()); if (!searchedNilData.isEmpty()) {
		 * 
		 * action = action == false ? cdnDatas.remove(searchedCdnData.get(0)) : true;
		 * 
		 * if (action == false) { if (searchedNil.get(0).isGstnSynced()) {
		 * searchedNil.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
		 * searchedNil.get(0).setGstnSynced(false); } else { action =
		 * nilDatas.remove(searchedNilData.get(0)); if (nilDatas.isEmpty())// will
		 * delete b2b in case of no // invoices remaining
		 * nils.remove(searchedNil.get(0)); } action = true;
		 * 
		 * } }
		 * 
		 * action = action == false ? nils.remove(searchedNil.get(0)) : true;
		 * 
		 * }
		 */
		return nils;
	}

	public static List<NIL> addNILObject(List<NIL> existingB2bs, List<NIL> transactionB2bs) {

		for (NIL nil : transactionB2bs) {
			existingB2bs = addNILObject(nil, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<NIL> updateNILObject(List<NIL> existingB2bs, List<NIL> transactionB2bs) {

		for (NIL nil : transactionB2bs) {
			existingB2bs = updateNILObject(nil, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<NIL> deleteNILObject(List<NIL> existingB2bs, List<NIL> transactionB2bs) {

		for (NIL nil : transactionB2bs) {
			existingB2bs = deleteNILObject(nil, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<NIL> updateNILObjectByNo(List<NIL> existingB2bs, List<NIL> transactionB2bs) throws AppException {

		for (NIL b2b : transactionB2bs) {
			existingB2bs = updateNILObjectByNo(b2b, existingB2bs);
		}

		return existingB2bs;
	}
	List<String> temp=new ArrayList<String>();
	private boolean isDate_index;
	private int _INVOICE_DATE_INDEX;
	private boolean isTaxable_index;
	private boolean isInvoice_type;
	private int _INVOICE_TYPE_INDEX;
	private boolean isSupply_index;
	private int _SUPPLY_VALUE_INDEX;
	private boolean isInvoice_number;
	private int _INVOICE_NUMBER_INDEX;
	private boolean isInvoice_value;
	private String cellRange;
	
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}

	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxPayerGstin,
			String sheetName, String monthYear, UserBean userBean, SourceType source, String delete)
			throws AppException {
		String gstin = taxPayerGstin.getGstin();
		List<NILTransactionEntity> nils = new ArrayList<>();
		// set column mapping

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		if (source != SourceType.TALLY)
			setColumnMapping(headerIndex, gstReturn);

		// List<NIL> nils = new ArrayList<>();

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0 || this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if (Objects.nonNull(values))
						columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
					if (temp == null)
						break;
					// convert list to nil object
					index++;
					if (source != SourceType.TALLY)
						convertListToTransactionData(ee, nils, temp, index, sheetName, sourceName, sourceId, gstReturn,
								gstinPos, taxPayerGstin, monthYear, userBean);
					else
						convertTallyListToTransactionObj(ee, nils, temp, index, sheetName, sourceName, sourceId,
								gstReturn, gstinPos, taxPayerGstin, monthYear, userBean);

				}
			}

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new com.ginni.easemygst.portal.transaction.crud.NILTransaction().insertUpdate(nils, delete);

		} catch (Exception e) {
			_Logger.error("sql excwption", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source)
			throws AppException {

		String gstin = taxpayerGstin.getGstin();
		List<NILTransactionEntity> nils = new ArrayList<>();
		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		int index = 0;

		CSVParser csvParser = new CSVParser();

		boolean skipHeader = false;
		while (scanner.hasNext()) {
			List<String> temp = csvParser.parseLine(scanner.nextLine());
			if (skipHeader)
				skipHeader = false;
			else {
				// convert list to nil object
				if (!Objects.isNull(temp)) {
					index++;
					convertListToTransactionData(ee, nils, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin, monthYear, userBean);
				} else
					break;
			}
		}
		scanner.close();

		if (ee.isError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			_Logger.error("appexception ", exp);
			throw exp;
		}

		try {
			return new com.ginni.easemygst.portal.transaction.crud.NILTransaction().insertUpdate(nils, delete);

		} catch (SQLException e) {

			throw new AppException(e);
		}

	}

	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		String gstin = taxpayerGstin.getGstin();
		List<NILTransactionEntity> nils = new ArrayList<>();
		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		try {
			int index = 0;

			// for(UploadedCsv lineItem:lineItems) {
			index++;
			convertLineItemIntoList(lineItems, ee, nils, index, sheetName, sourceName, sourceId, gstReturn, gstinPos,
					taxpayerGstin, monthYear, userBean);
			// }

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return new com.ginni.easemygst.portal.transaction.crud.NILTransaction().insertUpdate(nils, delete);

		}
		catch (AppException e) {
			_Logger.error("exception while process nil ", e);
			throw e;
		}
		catch (Exception e) {
			_Logger.error("exception while process nil ", e);
			throw new AppException(e);
		}
	}

	private void convertLineItemIntoList(List<UploadedCsv> lineItems, ExcelError ee, List<NILTransactionEntity> nils,
			int index, String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {

		ee.setUniqueRowValue("");

		NILTransactionEntity intrStR = new NILTransactionEntity();
		NILTransactionEntity intraStR = new NILTransactionEntity();
		NILTransactionEntity intrStU = new NILTransactionEntity();
		NILTransactionEntity intraStU = new NILTransactionEntity();
		NILTransactionEntity inter = new NILTransactionEntity();
		NILTransactionEntity intra = new NILTransactionEntity();

		intrStR.setSupplyType(NilSupplyType.INTR_REGISTERED);
		intraStR.setSupplyType(NilSupplyType.INTRA_REGISTERED);
		intrStU.setSupplyType(NilSupplyType.INTR_UNREGISTERED);
		intraStU.setSupplyType(NilSupplyType.INTRA_UNREGISTERED);

		intrStR.setSource(sourceName);
		intrStR.setSourceId(sourceId);
		intraStR.setSource(sourceName);
		intraStR.setSourceId(sourceId);
		intrStU.setSource(sourceName);
		intrStU.setSourceId(sourceId);
		intraStU.setSource(sourceName);
		intraStU.setSourceId(sourceId);

		inter.setSupplyType(NilSupplyType.INTER);
		intra.setSupplyType(NilSupplyType.INTRA);
		inter.setSource(sourceName);
		inter.setSourceId(sourceId);
		intra.setSource(sourceName);
		intra.setSourceId(sourceId);

		if (gstReturn.equalsIgnoreCase("gstr1")) {

			// INTRB2B/ INTRB2C/ INTRAB2B/ INTRAB2C

			for (UploadedCsv uploadedCsv : lineItems) {
				
					ee = this.validateNumericValue(sheetName, 0,uploadedCsv.getLineNumber(), uploadedCsv.getNetAmount(),
							"Invalid Nil Amount Value", ee);
					
					if(!ee.isError()) {

					if ("INTRA-B2B_NONGST".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intraStR.setTotNgsupAmt(
									intraStR.getTotNgsupAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTRA-B2B_NIL".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intraStR.setTotNilAmt(
									intraStR.getTotNilAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTRA-B2CS_NONGST".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intraStU.setTotNgsupAmt(
									intraStU.getTotNgsupAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTRA-B2CS_NIL".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intraStU.setTotNilAmt(
									intraStU.getTotNilAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					}

					else if ("INTER-B2B_NONGST".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intrStR.setTotNgsupAmt(
									intrStR.getTotNgsupAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTER-B2B_NIL".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intrStR.setTotNilAmt(
									intrStR.getTotNilAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTER-B2CS_NONGST".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intrStU.setTotNgsupAmt(
									intrStU.getTotNgsupAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTER-B2CS_NIL".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intrStU.setTotNilAmt(
									intrStU.getTotNilAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					}
					}
			}
		}

		if ("gstr2".equalsIgnoreCase(gstReturn)) {

			for (UploadedCsv uploadedCsv : lineItems) {

					ee = this.validateNumericValue(sheetName, 0,uploadedCsv.getLineNumber(), uploadedCsv.getNetAmount(),
							"Invalid Nil Amount Value", ee);
					if(!ee.isError()) {

					if ("INTRA-NONGST".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intra.setTotNgsupAmt(
									intra.getTotNgsupAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));

					} else if ("INTRA-NIL".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intra.setTotNilAmt(intra.getTotNilAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTRA-EXEMPT".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intra.setTotExptAmt(intra.getTotExptAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));

					} else if ("INTRA-COMPOUNDING".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							intra.setTotCompAmt(intra.getTotCompAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					}

					// inter
					if ("INTER-NONGST".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							inter.setTotNgsupAmt(
									inter.getTotNgsupAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));

					} else if ("INTER-NIL".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							inter.setTotNilAmt(inter.getTotNilAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					} else if ("INTER-EXEMPT".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							inter.setTotExptAmt(inter.getTotExptAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));

					} else if ("INTER-COMPOUNDING".equalsIgnoreCase(uploadedCsv.getSubType())) {
						if (!ee.isError())
							inter.setTotCompAmt(inter.getTotCompAmt() + Double.parseDouble(uploadedCsv.getNetAmount()));
					}
					}

				}

		}

		if (!ee.isError() && "gstr1".equalsIgnoreCase(gstReturn)) {
			// NILTransactionEntity intrStRNil=new NILTransactionEntity();
			intrStR.setGstin(taxPayerGstin);
			intrStR.setMonthYear(monthYear);
			intrStR.setReturnType(gstReturn);

			// NILTransactionEntity intraStRNil=new NILTransactionEntity();
			intraStR.setGstin(taxPayerGstin);
			intraStR.setMonthYear(monthYear);
			intraStR.setReturnType(gstReturn);

			// NILTransactionEntity intrStUNil=new NILTransactionEntity();
			intrStU.setGstin(taxPayerGstin);
			intrStU.setMonthYear(monthYear);
			intrStU.setReturnType(gstReturn);

			// NILTransactionEntity intraStUNil=new NILTransactionEntity();
			intraStU.setGstin(taxPayerGstin);
			intraStU.setMonthYear(monthYear);
			intraStU.setReturnType(gstReturn);

			nils.add(intrStR);
			nils.add(intraStR);
			nils.add(intrStU);
			nils.add(intraStU);

		} else if (!ee.isError() && "gstr2".equalsIgnoreCase(gstReturn)) {

			// NILTransactionEntity inter=new NILTransactionEntity();
			inter.setGstin(taxPayerGstin);
			inter.setMonthYear(monthYear);
			inter.setReturnType(gstReturn);

			// NILTransactionEntity intraNil=new NILTransactionEntity();
			intra.setGstin(taxPayerGstin);
			intra.setMonthYear(monthYear);
			intra.setReturnType(gstReturn);
			nils.add(inter);
			nils.add(intra);
		}

	}

	private void convertTallyListToTransactionObj(ExcelError ee, List<NILTransactionEntity> nils, List<String> temp,
			int index, String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {
		if (ReturnType.GSTR1.toString().equalsIgnoreCase(gstReturn)) {

			ee.setUniqueRowValue("");

			NILTransactionEntity nill = new NILTransactionEntity();

			nill.setSource(sourceName);
			nill.setSourceId(sourceId);

			double nillRated = 0;
			double exempted = 0;
			double nGSt = 0;

			if (!StringUtils.isEmpty(temp.get(_TALLY_NILL_SUPPLIES_INDEX))) {
				ee = this.validateNumericValue(sheetName, _TALLY_NILL_SUPPLIES_INDEX, index,
						temp.get(_TALLY_NILL_SUPPLIES_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_NILL_SUPPLIES_INDEX)))
					nillRated = Double.parseDouble(temp.get(_TALLY_NILL_SUPPLIES_INDEX));
			}
			if (!StringUtils.isEmpty(temp.get(_TALLY_EXEMPTED_INDEX))) {
				ee = this.validateNumericValue(sheetName, _TALLY_EXEMPTED_INDEX, index, temp.get(_TALLY_EXEMPTED_INDEX),
						"Invalid exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_EXEMPTED_INDEX)))
					exempted = Double.parseDouble(temp.get(_TALLY_EXEMPTED_INDEX));
			}
			if (!StringUtils.isEmpty(temp.get(_TALLY_NON_GST_SUPPLY_INDEX))) {
				ee = this.validateNumericValue(sheetName, _TALLY_NON_GST_SUPPLY_INDEX, index,
						temp.get(_TALLY_NON_GST_SUPPLY_INDEX), "Invalid NON GST Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_NON_GST_SUPPLY_INDEX)))
					nGSt = Double.parseDouble(temp.get(_TALLY_NON_GST_SUPPLY_INDEX));
			}

			nill.setTotNilAmt(nillRated);
			nill.setTotExptAmt(exempted);
			nill.setTotNgsupAmt(nGSt);

			if ("Inter-State supplies to registered persons".equalsIgnoreCase(temp.get(_TALLY_DESCRIPTION_INDEX))) {
				nill.setSupplyType(NilSupplyType.INTR_REGISTERED);

			} else if ("Intra-State supplies to registered persons"
					.equalsIgnoreCase(temp.get(_TALLY_DESCRIPTION_INDEX))) {
				nill.setSupplyType(NilSupplyType.INTRA_REGISTERED);

			} else if ("Inter-State supplies to unregistered persons"
					.equalsIgnoreCase(temp.get(_TALLY_DESCRIPTION_INDEX))) {
				nill.setSupplyType(NilSupplyType.INTR_UNREGISTERED);

			} else if ("Intra-State supplies to unregistered persons"
					.equalsIgnoreCase(temp.get(_TALLY_DESCRIPTION_INDEX))) {
				nill.setSupplyType(NilSupplyType.INTRA_UNREGISTERED);
			}

			if (!ee.isError() && "gstr1".equalsIgnoreCase(gstReturn)) {
				nill.setGstin(taxPayerGstin);
				nill.setMonthYear(monthYear);
				nill.setReturnType(gstReturn);
				nils.add(nill);
			}

		} else if (ReturnType.GSTR2.toString().equalsIgnoreCase(gstReturn)) {
			convertTallyListToTransactionObjGstr2(ee, nils, temp, index, sheetName, sourceName, sourceId, gstReturn,
					gstinPos, taxPayerGstin, monthYear, userBean);

		}

	}

	private void convertTallyListToTransactionObjGstr2(ExcelError ee, List<NILTransactionEntity> nils,
			List<String> temp, int index, String sheetName, String sourceName, String sourceId, String gstReturn,
			String gstinPos, TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {

		_TALLY_DESCRIPTION_INDEX = 0;
		_TALLY_COMPOSITE_SUPPLY_INDEX = 1;
		_TALLY_NILL_SUPPLIES_INDEX = 2;
		_TALLY_EXEMPTED_INDEX = 3;
		_TALLY_NON_GST_SUPPLY_INDEX = 4;

		ee.setUniqueRowValue("");

		NILTransactionEntity nill = new NILTransactionEntity();

		nill.setSource(sourceName);
		nill.setSourceId(sourceId);

		double nillRated = 0;
		double exempted = 0;
		double nGSt = 0;
		double totalComposite = 0;

		if (!StringUtils.isEmpty(temp.get(_TALLY_COMPOSITE_SUPPLY_INDEX))) {
			ee = this.validateNumericValue(sheetName, _TALLY_COMPOSITE_SUPPLY_INDEX, index,
					temp.get(_TALLY_COMPOSITE_SUPPLY_INDEX), "Invalid Composite Amount Value", ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_COMPOSITE_SUPPLY_INDEX)))
				totalComposite = Double.parseDouble(temp.get(_TALLY_COMPOSITE_SUPPLY_INDEX));
		}

		if (!StringUtils.isEmpty(temp.get(_TALLY_NILL_SUPPLIES_INDEX))) {
			ee = this.validateNumericValue(sheetName, _TALLY_NILL_SUPPLIES_INDEX, index,
					temp.get(_TALLY_NILL_SUPPLIES_INDEX), "Invalid Nil Amount Value", ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_NILL_SUPPLIES_INDEX)))
				nillRated = Double.parseDouble(temp.get(_TALLY_NILL_SUPPLIES_INDEX));
		}
		if (!StringUtils.isEmpty(temp.get(_TALLY_EXEMPTED_INDEX))) {
			ee = this.validateNumericValue(sheetName, _TALLY_EXEMPTED_INDEX, index, temp.get(_TALLY_EXEMPTED_INDEX),
					"Invalid exempted Amount Value", ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_EXEMPTED_INDEX)))
				exempted = Double.parseDouble(temp.get(_TALLY_EXEMPTED_INDEX));
		}
		if (!StringUtils.isEmpty(temp.get(_TALLY_NON_GST_SUPPLY_INDEX))) {
			ee = this.validateNumericValue(sheetName, _TALLY_NON_GST_SUPPLY_INDEX, index,
					temp.get(_TALLY_NON_GST_SUPPLY_INDEX), "Invalid NON GST Amount Value", ee);
			if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TALLY_NON_GST_SUPPLY_INDEX)))
				nGSt = Double.parseDouble(temp.get(_TALLY_NON_GST_SUPPLY_INDEX));
		}

		nill.setTotNilAmt(nillRated);
		nill.setTotExptAmt(exempted);
		nill.setTotNgsupAmt(nGSt);
		nill.setTotCompAmt(totalComposite);

		if ("Inter-State supplies".equalsIgnoreCase(temp.get(_TALLY_DESCRIPTION_INDEX))) {
			nill.setSupplyType(NilSupplyType.INTER);

		} else if ("Intra-State supplies".equalsIgnoreCase(temp.get(_TALLY_DESCRIPTION_INDEX))) {
			nill.setSupplyType(NilSupplyType.INTRA);

		}

		if (!ee.isError() && "gstr2".equalsIgnoreCase(gstReturn)) {
			nill.setGstin(taxPayerGstin);
			nill.setMonthYear(monthYear);
			nill.setReturnType(gstReturn);
			nils.add(nill);
		}

	}

	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			if (gstReturn.equalsIgnoreCase("gstr1")) {

				_INTRSTR_NIL_AMOUNT_INDEX = Integer.valueOf(headers[0]);
				_INTRSTR_EXMPTD_AMOUNT_INDEX = Integer.valueOf(headers[1]);
				_INTRSTR_NONGST_AMOUNT_INDEX = Integer.valueOf(headers[2]);
				_INTRASTR_NIL_AMOUNT_INDEX = Integer.valueOf(headers[3]);
				_INTRASTR_EXMPTD_AMOUNT_INDEX = Integer.valueOf(headers[4]);
				_INTRASTR_NONGST_AMOUNT_INDEX = Integer.valueOf(headers[5]);

				_INTRSTU_NIL_AMOUNT_INDEX = Integer.valueOf(headers[6]);
				_INTRSTU_EXMPTD_AMOUNT_INDEX = Integer.valueOf(headers[7]);
				_INTRSTU_NONGST_AMOUNT_INDEX = Integer.valueOf(headers[8]);
				_INTRASTU_NIL_AMOUNT_INDEX = Integer.valueOf(headers[9]);
				_INTRASTU_EXMPTD_AMOUNT_INDEX = Integer.valueOf(headers[10]);
				_INTRASTU_NONGST_AMOUNT_INDEX = Integer.valueOf(headers[11]);

			} else if (gstReturn.equalsIgnoreCase("gstr2")) {

				_INTRSTR_COMPOSITE_AMOUNT_INDEX = Integer.valueOf(headers[0]);
				_INTRSTR_EXMPTD_AMOUNT_INDEX = Integer.valueOf(headers[1]);
				_INTRSTR_NONGST_AMOUNT_INDEX = Integer.valueOf(headers[2]);
				_INTRSTR_NIL_AMOUNT_INDEX = Integer.valueOf(headers[3]);

				_INTRASTR_COMPOSITE_AMOUNT_INDEX = Integer.valueOf(headers[4]);
				_INTRASTR_EXMPTD_AMOUNT_INDEX = Integer.valueOf(headers[5]);
				_INTRASTR_NONGST_AMOUNT_INDEX = Integer.valueOf(headers[6]);
				_INTRASTR_NIL_AMOUNT_INDEX = Integer.valueOf(headers[7]);

			}

		} else if (gstReturn.equalsIgnoreCase("gstr2")) {
			_INTRSTR_COMPOSITE_AMOUNT_INDEX = 0;
			_INTRSTR_EXMPTD_AMOUNT_INDEX = 1;
			_INTRSTR_NONGST_AMOUNT_INDEX = 2;
			_INTRSTR_NIL_AMOUNT_INDEX = 3;

			_INTRASTR_COMPOSITE_AMOUNT_INDEX = 4;
			_INTRASTR_EXMPTD_AMOUNT_INDEX = 5;
			_INTRASTR_NONGST_AMOUNT_INDEX = 6;
			_INTRASTR_NIL_AMOUNT_INDEX = 7;
		}

	}

	private void convertListToTransactionData(ExcelError ee, List<NILTransactionEntity> nils, List<String> temp,
			int index, String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {

		ee.setUniqueRowValue("");

		NILTransactionEntity intrStR = new NILTransactionEntity();
		NILTransactionEntity intraStR = new NILTransactionEntity();
		NILTransactionEntity intrStU = new NILTransactionEntity();
		NILTransactionEntity intraStU = new NILTransactionEntity();
		NILTransactionEntity inter = new NILTransactionEntity();
		NILTransactionEntity intra = new NILTransactionEntity();

		intrStR.setSupplyType(NilSupplyType.INTR_REGISTERED);
		intraStR.setSupplyType(NilSupplyType.INTRA_REGISTERED);
		intrStU.setSupplyType(NilSupplyType.INTR_UNREGISTERED);
		intraStU.setSupplyType(NilSupplyType.INTRA_UNREGISTERED);

		intrStR.setSource(sourceName);
		intrStR.setSourceId(sourceId);
		intraStR.setSource(sourceName);
		intraStR.setSourceId(sourceId);
		intrStU.setSource(sourceName);
		intrStU.setSourceId(sourceId);
		intraStU.setSource(sourceName);
		intraStU.setSourceId(sourceId);

		inter.setSupplyType(NilSupplyType.INTER);
		intra.setSupplyType(NilSupplyType.INTRA);
		inter.setSource(sourceName);
		inter.setSourceId(sourceId);
		intra.setSource(sourceName);
		intra.setSourceId(sourceId);

		if (gstReturn.equalsIgnoreCase("gstr1")) {

			// setting nil amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_NIL_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTR_NIL_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_NIL_AMOUNT_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTR_NIL_AMOUNT_INDEX)))
					intrStR.setTotNilAmt(Double.parseDouble(temp.get(_INTRSTR_NIL_AMOUNT_INDEX)));
			}

			// setting exempted amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTR_EXMPTD_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX), "Invalid Exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX)))
					intrStR.setTotExptAmt(Double.parseDouble(temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX)));
			}
			// setting non gst amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_NONGST_AMOUNT_INDEX))) {

				ee = this.validateNumericValue(sheetName, _INTRSTR_NONGST_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_NONGST_AMOUNT_INDEX), "Invalid non gst outward supplies value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_NIL_AMOUNT_INDEX)))
					intrStR.setTotNgsupAmt(Double.parseDouble(temp.get(_INTRSTR_NONGST_AMOUNT_INDEX)));
			}

			// setting nil amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_NIL_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTR_NIL_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_NIL_AMOUNT_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_NIL_AMOUNT_INDEX)))
					intraStR.setTotNilAmt(Double.parseDouble(temp.get(_INTRASTR_NIL_AMOUNT_INDEX)));
			}

			// setting exempted amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRASTR_EXMPTD_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX), "Invalid Exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX)))
					intraStR.setTotExptAmt(Double.parseDouble(temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX)));
			}
			// setting non gst amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_NONGST_AMOUNT_INDEX))) {

				ee = this.validateNumericValue(sheetName, _INTRASTR_NONGST_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_NONGST_AMOUNT_INDEX), "Invalid non gst outward supplies value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_NONGST_AMOUNT_INDEX)))
					intraStR.setTotNgsupAmt(Double.parseDouble(temp.get(_INTRASTR_NONGST_AMOUNT_INDEX)));
			}

			if (!StringUtils.isEmpty(temp.get(_INTRSTU_NIL_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTU_NIL_AMOUNT_INDEX, index,
						temp.get(_INTRSTU_NIL_AMOUNT_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTU_NIL_AMOUNT_INDEX)))
					intrStU.setTotNilAmt(Double.parseDouble(temp.get(_INTRSTU_NIL_AMOUNT_INDEX)));
			}

			// setting exempted amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTU_EXMPTD_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTU_EXMPTD_AMOUNT_INDEX, index,
						temp.get(_INTRSTU_EXMPTD_AMOUNT_INDEX), "Invalid Exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTU_EXMPTD_AMOUNT_INDEX)))
					intrStU.setTotExptAmt(Double.parseDouble(temp.get(_INTRSTU_EXMPTD_AMOUNT_INDEX)));
			}
			// setting non gst amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTU_NONGST_AMOUNT_INDEX))) {

				ee = this.validateNumericValue(sheetName, _INTRSTU_NONGST_AMOUNT_INDEX, index,
						temp.get(_INTRSTU_NONGST_AMOUNT_INDEX), "Invalid non gst outward supplies value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTU_NONGST_AMOUNT_INDEX)))
					intrStU.setTotNgsupAmt(Double.parseDouble(temp.get(_INTRSTU_NONGST_AMOUNT_INDEX)));
			}

			// setting nil amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTU_NIL_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRASTU_NIL_AMOUNT_INDEX, index,
						temp.get(_INTRASTU_NIL_AMOUNT_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTU_NIL_AMOUNT_INDEX)))
					intraStU.setTotNilAmt(Double.parseDouble(temp.get(_INTRASTU_NIL_AMOUNT_INDEX)));
			}

			// setting exempted amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTU_EXMPTD_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRASTU_EXMPTD_AMOUNT_INDEX, index,
						temp.get(_INTRASTU_EXMPTD_AMOUNT_INDEX), "Invalid Exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTU_EXMPTD_AMOUNT_INDEX)))
					intraStU.setTotExptAmt(Double.parseDouble(temp.get(_INTRASTU_EXMPTD_AMOUNT_INDEX)));
			}
			// setting non gst amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTU_NONGST_AMOUNT_INDEX))) {

				ee = this.validateNumericValue(sheetName, _INTRASTU_NONGST_AMOUNT_INDEX, index,
						temp.get(_INTRASTU_NONGST_AMOUNT_INDEX), "Invalid non gst outward supplies value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTU_NONGST_AMOUNT_INDEX)))
					intraStU.setTotNgsupAmt(Double.parseDouble(temp.get(_INTRASTU_NONGST_AMOUNT_INDEX)));
			}

		} else if (gstReturn.equalsIgnoreCase("gstr2")) {

			// setting composite amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_COMPOSITE_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTR_COMPOSITE_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_COMPOSITE_AMOUNT_INDEX), "Invalid Composite Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTR_COMPOSITE_AMOUNT_INDEX)))
					inter.setTotCompAmt(Double.parseDouble(temp.get(_INTRSTR_COMPOSITE_AMOUNT_INDEX)));
			}

			// setting nil amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_NIL_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTR_NIL_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_NIL_AMOUNT_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTR_NIL_AMOUNT_INDEX)))
					inter.setTotNilAmt(Double.parseDouble(temp.get(_INTRSTR_NIL_AMOUNT_INDEX)));
			}

			// setting exempted amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRSTR_EXMPTD_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX), "Invalid Exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX)))
					inter.setTotExptAmt(Double.parseDouble(temp.get(_INTRSTR_EXMPTD_AMOUNT_INDEX)));
			}
			// setting non gst amount
			if (!StringUtils.isEmpty(temp.get(_INTRSTR_NONGST_AMOUNT_INDEX))) {

				ee = this.validateNumericValue(sheetName, _INTRSTR_NONGST_AMOUNT_INDEX, index,
						temp.get(_INTRSTR_NONGST_AMOUNT_INDEX), "Invalid non gst outward supplies value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRSTR_NONGST_AMOUNT_INDEX)))
					inter.setTotNgsupAmt(Double.parseDouble(temp.get(_INTRSTR_NONGST_AMOUNT_INDEX)));
			}

			// setting composite amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_COMPOSITE_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRASTR_COMPOSITE_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_COMPOSITE_AMOUNT_INDEX), "Invalid Composite Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_COMPOSITE_AMOUNT_INDEX)))
					intra.setTotCompAmt(Double.parseDouble(temp.get(_INTRASTR_COMPOSITE_AMOUNT_INDEX)));
			}

			// setting nil amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_NIL_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRASTR_NIL_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_NIL_AMOUNT_INDEX), "Invalid Nil Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_NIL_AMOUNT_INDEX)))
					intra.setTotNilAmt(Double.parseDouble(temp.get(_INTRASTR_NIL_AMOUNT_INDEX)));
			}

			// setting exempted amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX))) {
				ee = this.validateNumericValue(sheetName, _INTRASTR_EXMPTD_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX), "Invalid Exempted Amount Value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX)))
					intra.setTotExptAmt(Double.parseDouble(temp.get(_INTRASTR_EXMPTD_AMOUNT_INDEX)));
			}
			// setting non gst amount
			if (!StringUtils.isEmpty(temp.get(_INTRASTR_NONGST_AMOUNT_INDEX))) {

				ee = this.validateNumericValue(sheetName, _INTRASTR_NONGST_AMOUNT_INDEX, index,
						temp.get(_INTRASTR_NONGST_AMOUNT_INDEX), "Invalid non gst outward supplies value", ee);
				if (!ee.isError() && !StringUtils.isEmpty(temp.get(_INTRASTR_NONGST_AMOUNT_INDEX)))
					intra.setTotNgsupAmt(Double.parseDouble(temp.get(_INTRASTR_NONGST_AMOUNT_INDEX)));
			}

		}

		if (!ee.isError() && "gstr1".equalsIgnoreCase(gstReturn)) {
			// NILTransactionEntity intrStRNil=new NILTransactionEntity();
			intrStR.setGstin(taxPayerGstin);
			intrStR.setMonthYear(monthYear);
			intrStR.setReturnType(gstReturn);

			// NILTransactionEntity intraStRNil=new NILTransactionEntity();
			intraStR.setGstin(taxPayerGstin);
			intraStR.setMonthYear(monthYear);
			intraStR.setReturnType(gstReturn);

			// NILTransactionEntity intrStUNil=new NILTransactionEntity();
			intrStU.setGstin(taxPayerGstin);
			intrStU.setMonthYear(monthYear);
			intrStU.setReturnType(gstReturn);

			// NILTransactionEntity intraStUNil=new NILTransactionEntity();
			intraStU.setGstin(taxPayerGstin);
			intraStU.setMonthYear(monthYear);
			intraStU.setReturnType(gstReturn);

			nils.add(intrStR);
			nils.add(intraStR);
			nils.add(intrStU);
			nils.add(intraStU);

		} else if (!ee.isError() && "gstr2".equalsIgnoreCase(gstReturn)) {

			// NILTransactionEntity inter=new NILTransactionEntity();
			inter.setGstin(taxPayerGstin);
			inter.setMonthYear(monthYear);
			inter.setReturnType(gstReturn);

			// NILTransactionEntity intraNil=new NILTransactionEntity();
			intra.setGstin(taxPayerGstin);
			intra.setMonthYear(monthYear);
			intra.setReturnType(gstReturn);
			nils.add(inter);
			nils.add(intra);
		}

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<NIL> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<NIL>>() {
				});
				List<NIL> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<NIL>>() {
				});

				if (action == null) {
					existingB2bs = addNILObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateNILObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteNILObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateNILObjectByNo(existingB2bs, transactionB2bs);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<NIL> nils = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<NIL>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			/*
			 * Calendar cal = Calendar.getInstance(); Date currentDate = cal.getTime(); int
			 * currYear = currentDate.getYear(); int currMonth = currentDate.getMonth();
			 * 
			 * if (currMonth - 1 == inputMonth.getMonth()) { cal.set(Calendar.DATE, 1);
			 * cal.add(Calendar.DATE, -1); currentDate = cal.getTime(); }
			 */

			double totalNilAmount = 0.0;
			double totalExptAmount = 0.0;
			double totalNonGstAmount = 0.0;

			double amTaxAmount = 0.0;
			double amAdvanceRcvd = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;
			boolean summaryError = false;

			for (NIL nil : nils) {

				StringBuilder flags = new StringBuilder();
				boolean isFirstFlag = true;

				double advncRcvdInv = 0.0;
				double taxAmountInv = 0.0;
				double exptAmt = 0.0;
				double nilAmt = 0.0;
				double nonGstAmt = 0.0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;
				/*
				 * for(NilData nd:nil.getNilDatas()){
				 * 
				 * if (!Objects.isNull(nd.getExptAmt())) exptAmt+=nd.getExptAmt(); if
				 * (!Objects.isNull(nd.getNilAmt())) nilAmt+=nd.getNilAmt(); if
				 * (!Objects.isNull(nd.getNgsupAmt())) nonGstAmt+=nd.getNgsupAmt(); }
				 */
				invoiceCount++;

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}

				totalExptAmount += exptAmt;
				totalNilAmount += nilAmt;
				totalNonGstAmount += nonGstAmt;
				/*
				 * nil.setTotExptAmt(exptAmt); nil.setTotNgsupAmt(nonGstAmt);
				 * nil.setTotNilAmt(nilAmt);
				 */

				setNilFlag(nil);

				if (!nil.isValid()) {
					nil.setType(ReconsileType.ERROR);
					nil.setFlags("");
				}

			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", nils);
			TransactionDataDTO nilSumm = new TransactionDataDTO();
			nilSumm.setExemptedAmount(totalExptAmount);
			nilSumm.setNilAmount(totalNilAmount);
			nilSumm.setNonGstAmount(totalNonGstAmount);
			nilSumm.setReconcileDTO(reconcileDTO);

			gstrSummaryDTO.setOther(nilSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setNilFlag(NIL nil) {
		if (!Objects.isNull(nil.getTaxPayerAction())) {
			if (nil.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				nil.setFlags(TableFlags.ACCEPTED.toString());
			else if (nil.getTaxPayerAction() == TaxpayerAction.REJECT)
				nil.setFlags(TableFlags.REJECTED.toString());
			else if (nil.getTaxPayerAction() == TaxpayerAction.PENDING)
				nil.setFlags(TableFlags.PENDING.toString());
			else if (nil.getTaxPayerAction() == TaxpayerAction.MODIFY)
				nil.setFlags(TableFlags.MODIFIED.toString());
			else if (nil.getTaxPayerAction() == TaxpayerAction.DELETE)
				nil.setFlags(TableFlags.DELETED.toString());
			else if (nil.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				nil.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {

			List<NILTransactionEntity> b2csDatas = datas;
			List<String> columns = this.getFlatData(b2csDatas, returnType);
			tabularData.add(columns);

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;

	}

	private List<String> getFlatData(List<NILTransactionEntity> nils, String returnType) {

		List<String> columns = new ArrayList<>();
		List<NILTransactionEntity> sortedNils = new ArrayList<>();
		if (nils.isEmpty())
			return columns;
		NILTransactionEntity temp = nils.get(0);
		NILTransactionEntity intrStR = new NILTransactionEntity();
		NILTransactionEntity intraStR = new NILTransactionEntity();
		NILTransactionEntity intrStU = new NILTransactionEntity();
		NILTransactionEntity intraStU = new NILTransactionEntity();
		NILTransactionEntity inter = new NILTransactionEntity();
		NILTransactionEntity intra = new NILTransactionEntity();
		intrStR.setSupplyType(NilSupplyType.INTR_REGISTERED);
		intrStR.setGstin(temp.getGstin());
		intrStR.setReturnType(temp.getReturnType());
		intrStR.setMonthYear(temp.getMonthYear());

		intraStR.setSupplyType(NilSupplyType.INTRA_REGISTERED);
		intraStR.setGstin(temp.getGstin());
		intraStR.setReturnType(temp.getReturnType());
		intraStR.setMonthYear(temp.getMonthYear());

		intrStU.setSupplyType(NilSupplyType.INTR_UNREGISTERED);
		intrStU.setGstin(temp.getGstin());
		intrStU.setReturnType(temp.getReturnType());
		intrStU.setMonthYear(temp.getMonthYear());

		intraStU.setSupplyType(NilSupplyType.INTRA_UNREGISTERED);
		intraStU.setGstin(temp.getGstin());
		intraStU.setReturnType(temp.getReturnType());
		intraStU.setMonthYear(temp.getMonthYear());

		inter.setSupplyType(NilSupplyType.INTER);
		inter.setGstin(temp.getGstin());
		inter.setReturnType(temp.getReturnType());
		inter.setMonthYear(temp.getMonthYear());

		intra.setSupplyType(NilSupplyType.INTRA);
		intra.setGstin(temp.getGstin());
		intra.setReturnType(temp.getReturnType());
		intra.setMonthYear(temp.getMonthYear());

		if ("gstr1".equalsIgnoreCase(returnType)) {
			// columns.add(nil.getSupplyType() == null ? "" :
			// String.valueOf(nil.getSupplyType()));
			sortedNils.add(nils.get(nils.indexOf(intrStR)));
			sortedNils.add(nils.get(nils.indexOf(intraStR)));
			sortedNils.add(nils.get(nils.indexOf(intrStU)));
			sortedNils.add(nils.get(nils.indexOf(intraStU)));
			for (NILTransactionEntity nil : sortedNils) {
				columns.add(this.getStringValue(nil.getTotNilAmt()));
				columns.add(this.getStringValue(nil.getTotExptAmt()));
				columns.add(this.getStringValue(nil.getTotNgsupAmt()));
			}
			columns.add(this.getStringValue(this.processErrorMsg(temp.getErrMsg())));

		} else {
			sortedNils.add(nils.get(nils.indexOf(inter)));
			sortedNils.add(nils.get(nils.indexOf(intra)));
			for (NILTransactionEntity nil : sortedNils) {
				columns.add(this.getStringValue(nil.getTotCompAmt()));
				columns.add(this.getStringValue(nil.getTotExptAmt()));
				columns.add(this.getStringValue(nil.getTotNgsupAmt()));
				columns.add(this.getStringValue(nil.getTotNilAmt()));

			}
			columns.add(this.getStringValue(this.processErrorMsg(temp.getErrMsg())));

		}
		return columns;

	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<NIL> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<NIL>>() {
				});

				data.removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;

	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getOther();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<NIL> nils = JsonMapper.objectMapper.readValue(object, new TypeReference<List<NIL>>() {
			});

			List<NIL> currentNils = new ArrayList<>();

			for (NIL nil : nils) {

				if (!nil.isTransit()) {
					currentNils.add(nil);
					nil.setTransit(true);
					nil.setTransitId(transitId);
				}

			}

			// gstr1.setNil(currentNils);

			if (currentNils.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(nils);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		try {
			List<NIL> nils = JsonMapper.objectMapper.readValue(object, new TypeReference<List<NIL>>() {
			});

			List<NIL> currentNils = new ArrayList<>();

			for (NIL nil : nils) {

				if (!nil.isTransit()) {
					currentNils.add(nil);
					nil.setTransit(true);
					nil.setTransitId(transitId);
				}

			}

			// gstr2.setNil(currentNils);

			if (currentNils.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(nils);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<NIL> nils = JsonMapper.objectMapper.readValue(object, new TypeReference<List<NIL>>() {
			});
			for (NIL nil : nils) {
				if (nil.isTransit() && nil.getTransitId().equals(transitId)) {
					nil.setTransit(false);
					nil.setTransitId("");
					nil.setGstnSynced(true);
				}
			}
			return JsonMapper.objectMapper.writeValueAsString(nils);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private ReturnsService returnService;
	private CrudService crudService;
	private FinderService finderService;

	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {

		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");
			finderService = (FinderService) InitialContext.doLookup("java:global/easemygst/FinderServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		gstinTransactionDTO
				.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "nils"));
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		try {

			Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
					Gstr1Dto.class);
			List<NILTransactionEntity> nils = gstr.getNils();

			Map<String, Object> result = finderService
					.getGstinTransactionByGstinMonthYearReturnType(gstinTransactionDTO, null);
			List<NILTransactionEntity> existingNils = (List) result.get("transactionData");
			for (NILTransactionEntity n : existingNils) {
				if (n.getIsTransit()) {
					AppException ae = new AppException();
					ae.setMessage("Nil items  are in transit state so can't be updated for the moment");
					throw ae;
				}
			}

			int count = returnService.deleteTransactionData(gstinTransactionDTO.getTaxpayerGstin().getGstin(),
					gstinTransactionDTO.getMonthYear(), TransactionType.NIL,
					ReturnType.valueOf(StringUtils.upperCase(gstinTransactionDTO.getReturnType())), null,
					DeleteType.ALL, DataSource.EMGST);
			_Logger.debug(" previous nil items deleted ", count);

			for (NILTransactionEntity nil : nils) {
				nil.setGstin(gstin);
				nil.setReturnType(gstinTransactionDTO.getReturnType());
				nil.setMonthYear(gstinTransactionDTO.getMonthYear());
				nil.setSource(SourceType.PORTAL.toString());
				nil.setToBeSync(true);
				InfoService.setInfo(nil, gstinTransactionDTO.getUserBean());
				crudService.create(nil);
			}

		} catch (AppException ae) {

			throw ae;
		} catch (Exception e) {

			_Logger.error("exception in docissue method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return true;

	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
//		gstExcel.loadSheet(3);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}

				if(temp!=null) {
					System.out.println("Inside 7.Comp-exempt-nil-noGST data...");
					System.out.println(temp);
					if(ReturnType.GSTR2.toString().equals(gstReturn)) {
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(5).equals("7.Comp-exempt-nil-noGST")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(5).equals("7.Comp-exempt-nil-noGST")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(5).equals("7.Comp-exempt-nil-noGST")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(5).equals("7.Comp-exempt-nil-noGST")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(5).equals("7.Comp-exempt-nil-noGST")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(5).equals("7.Comp-exempt-nil-noGST")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    						}
    					
						}
					}
				}
			}
		}
		return 0;
	}
}
