package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.AT;
import com.ginni.easemygst.portal.data.transaction.EXP;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2bUrTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.ATService;
import com.ginni.easemygst.portal.transaction.crud.B2CSService;
import com.ginni.easemygst.portal.transaction.crud.B2bUrService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class ATTransaction extends TransactionUtil implements Transaction {

	private FinderService finderService;

	int _STATE_CODE_INDEX = 0;
	//int _MONTH_YEAR_INDEX = 12;
	int _ORIGINAL_MONTH = 12;
	/*int _ORIGINAL_POS = 14;
	int _ORIGINAL_SUPPLY_TYPE=15;*/
	
	public ATTransaction() {

		 _SNO_INDEX = 2;
		 _HSN_CODE_INDEX = 3;
		 _DESCRIPTION_INDEX = 4;
		 _UNIT_INDEX = 5;
		 _QUANTITY_INDEX = 6;
		 _TAX_RATE_INDEX = 7;
		 _IGST_AMT_INDEX = 8;
		 _CGST_AMT_INDEX = 9;
		 _SGST_AMT_INDEX = 10;
		 _CESS_AMT_INDEX = 11;
         _ADVANCE_AMOUNT_INDEX = 1;
         _TEMP_TALLY_POS=0;
         
          _ORIGINAL_MONTH = 12;
     	 /*_ORIGINAL_POS = 14;
     	 _ORIGINAL_SUPPLY_TYPE=15;*/

	}
	List<String> temp=new ArrayList<String>();

	private boolean isDate_index;

	private int _INVOICE_DATE_INDEX;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private int _SUPPLY_VALUE_INDEX;

	private boolean isInvoice_number;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;


	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	/* No unique field in case of adding/updating item */
	public static List<AT> addATObject(AT at, List<AT> ats) {

		List<InvoiceItem> invoiceItems = new ArrayList<>();
		List<InvoiceItem> tInvoiceItems = at.getInvoiceItems();
		if (ats.contains(at)) {
			at = ats.get(ats.indexOf(at));
			invoiceItems = at.getInvoiceItems();
			for (InvoiceItem tInvoiceItem : tInvoiceItems) {
				if (!invoiceItems.contains(tInvoiceItem)) {
					if (StringUtils.isEmpty(tInvoiceItem.getId()))
						tInvoiceItem.setId(getRandomUniqueId());
					invoiceItems.add(tInvoiceItem);
				}
			}

		} else {
			if (StringUtils.isEmpty(at.getId()))
				at.setId(getRandomUniqueId());
			for (InvoiceItem tInvoiceItem : tInvoiceItems) {
				if (StringUtils.isEmpty(tInvoiceItem.getId()))
					tInvoiceItem.setId(getRandomUniqueId());
			}
			ats.add(at);
		}

		return ats;
	}

	
	public static List<AT> updateATObject(AT at, List<AT> ats) {
		List<InvoiceItem> items = new ArrayList<>();
		List<InvoiceItem> tItems = at.getInvoiceItems();
		AT tempAt = at;
		String id = at.getId();
		List<AT> searchedAts = ats.stream().filter(a -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());
		if (searchedAts.isEmpty()) {// to check if the gstin exist according to
									// handlle client side case
			if (ats.contains(at)) {
				searchedAts.add(ats.get(ats.indexOf(at)));
				at.setId(searchedAts.get(0).getId());

			}
		}
		if (!searchedAts.isEmpty()) {// update existing at

			AT existingAt = searchedAts.get(0);
			items = existingAt.getInvoiceItems();
			for (InvoiceItem tItem : tItems) {
				List<InvoiceItem> searchedItem = items.stream().filter(b -> b.getId().equalsIgnoreCase(tItem.getId()))
						.collect(Collectors.toList());

				if (!searchedItem.isEmpty()) {// update
					items.remove(searchedItem.get(0));
				}
				if (StringUtils.isEmpty(tItem.getId()))
					tItem.setId(getRandomUniqueId());
				items.add(tItem);// add
				if (at.isGstnSynced())
					at.setTaxPayerAction(TaxpayerAction.MODIFY);
				at.setGstnSynced(false);

			}
			at.setInvoiceItems(items);
			ats.remove(existingAt);
			ats.add(at);
		} else {// add new at
			if (StringUtils.isEmpty(at.getId()))
				at.setId(getRandomUniqueId());
			at.setGstnSynced(false);
			for (InvoiceItem itm : at.getInvoiceItems()) {// genrating ids for
															// items
				if (StringUtils.isEmpty(itm.getId()))
					itm.setId(getRandomUniqueId());

			}
			ats.add(at);
		}

		return ats;
	}
	/* No unique field in case of adding/updating item */

	public static List<AT> updateATObjectByNo(AT at, List<AT> ats) {
		/*
		 * List<InvoiceItem> items = new ArrayList<>(); List<InvoiceItem> tItems
		 * = at.getInvoiceItems(); //String docNum = at.getDocumentNumber();
		 * List<AT> searchedAts = ats.stream() .filter(a ->
		 * !StringUtils.isEmpty(docNum) &&
		 * docNum.equalsIgnoreCase(a.getDocumentNumber()))
		 * .collect(Collectors.toList());
		 * 
		 * if (!searchedAts.isEmpty()) {// update existing at
		 * 
		 * AT existingAt = searchedAts.get(0); //items =
		 * existingAt.getInvoiceItems(); for (InvoiceItem tItem : tItems) {
		 * List<InvoiceItem> searchedItem = items.stream().filter(b ->
		 * b.getId().equalsIgnoreCase(tItem.getId()))
		 * .collect(Collectors.toList());// not decided on which // number item
		 * to be // searched
		 * 
		 * if (!searchedItem.isEmpty()) {// update
		 * items.remove(searchedItem.get(0));
		 * tItem.setId(searchedItem.get(0).getId()); items.add(tItem);
		 * 
		 * } else { if (StringUtils.isEmpty(tItem.getId()))
		 * tItem.setId(getRandomUniqueId()); items.add(tItem); } if
		 * (at.isGstnSynced()) at.setTaxPayerAction(TaxpayerAction.MODIFY);
		 * at.setGstnSynced(false);
		 * 
		 * } at.setInvoiceItems(items); ats.remove(existingAt);
		 * at.setId(existingAt.getId()); ats.add(at); } else {// add new at if
		 * (StringUtils.isEmpty(at.getId())) at.setId(getRandomUniqueId()); if
		 * (at.isGstnSynced()) at.setTaxPayerAction(TaxpayerAction.MODIFY);
		 * at.setGstnSynced(false); for (InvoiceItem itm : at.getInvoiceItems())
		 * {// genrating ids for // items if (StringUtils.isEmpty(itm.getId()))
		 * itm.setId(getRandomUniqueId());
		 * 
		 * } ats.add(at); }
		 */

		return ats;
	}

	public static List<AT> deleteATObject(AT at, List<AT> ats) {

		List<InvoiceItem> items;
		String aId = at.getId();
		List<AT> searchedAt = ats.stream().filter(a -> !StringUtils.isEmpty(aId) && aId.equalsIgnoreCase(a.getId()))
				.collect(Collectors.toList());

		boolean action = false;
		if (!searchedAt.isEmpty()) {

			if (action == false) {
				if (searchedAt.get(0).isGstnSynced()) {
					searchedAt.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedAt.get(0).setGstnSynced(false);
				} else {
					action = ats.remove(searchedAt.get(0));

				}
				action = true;

			}
		}

		return ats;
	}

	public static List<AT> changeStatusB2CSObject(AT at, List<AT> ats, TaxpayerAction taxpayerAction) {
		String bId = at.getId();
		List<AT> temp = ats.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equals(b.getId()))
				.collect(Collectors.toList());
		if (!temp.isEmpty()) {
			temp.get(0).setTaxPayerAction(taxpayerAction);
			temp.get(0).setGstnSynced(false);
		}

		return ats;
	}

	public static List<AT> addATObject(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = addATObject(at, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<AT> updateATObject(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = updateATObject(at, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<AT> deleteATObject(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = deleteATObject(at, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<AT> changeStatusATObject(List<AT> existingAts, List<AT> transactionAts,
			TaxpayerAction taxpayerAction) {

		for (AT at : transactionAts) {
			existingAts = changeStatusB2CSObject(at, existingAts, taxpayerAction);
		}

		return existingAts;
	}

	public static List<AT> updateATObjectByNo(List<AT> existingB2bs, List<AT> transactionB2bs) {

		for (AT at : transactionB2bs) {
			existingB2bs = updateATObjectByNo(at, existingB2bs);
		}

		return existingB2bs;
	}
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex,
			TaxpayerGstin taxPayerGstin, String sheetName, String monthYear,UserBean userBean,SourceType source,String delete) throws AppException {
		String gstin = taxPayerGstin.getGstin();
		List<ATTransactionEntity> ats = new ArrayList<>();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if(Objects.nonNull(values))
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);

					if(temp==null)
						break;
					// convert list to at object
					index++;
                     if(source==SourceType.TALLY){
                    	 temp= this.processTallyExcel(temp, gstinPos, sheetName, ee);
         				_TAXABLE_VALUE_INDEX = _ADVANCE_AMOUNT_INDEX;
                     }
					convertListToTransactionData(ee, ats, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxPayerGstin, monthYear,userBean);

				}

			}

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.printStackTrace();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}
			int uploadingCount=0;
			
		uploadingCount=	new ATService().insertUpdate(ats,delete);
		//logging user action
				ApiLoggingService apiLoggingService;
				try {
					apiLoggingService = (ApiLoggingService) InitialContext.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
					MessageDto messageDto=new MessageDto();
					messageDto.setGstin(taxPayerGstin.getGstin());
					messageDto.setReturnType(gstReturn);
					messageDto.setMonthYear(monthYear);
					messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
					messageDto.setTransactionType(TransactionType.AT.toString());
					if(!StringUtils.isEmpty(delete)&& delete.equalsIgnoreCase("Yes"))
						apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,taxPayerGstin.getGstin());
						else
					apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto,taxPayerGstin.getGstin());
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//end logging user action
		return uploadingCount;
		} catch (AppException e) {
			_Logger.error("exception", e);
			throw e;
		} catch (SQLException e) {
			_Logger.error("sql exception", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		String gstin = taxpayerGstin.getGstin();
		List<ATTransactionEntity> ats = new ArrayList<>();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					if(temp==null)
						break;
					// convert list to at object
					index++;
					convertListToTransactionData(ee, ats, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin, monthYear,userBean);
				}
			}
			scanner.close();

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return	new ATService().insertUpdate(ats,delete);
		} catch (AppException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,List<UploadedCsv>lineItems)
			throws AppException {

		String gstin = taxpayerGstin.getGstin();
		List<ATTransactionEntity> ats = new ArrayList<>();

		String customHeaderIndex="";
				if(gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
					customHeaderIndex="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18";
				else
					customHeaderIndex="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22";

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		ExcelError ee = new ExcelError();

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();
		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		try {
			int index = 0;

			
			for(UploadedCsv lineItem:lineItems) {
				
					// convert list to at object
					index++;
					convertListToTransactionData(ee, ats, this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin, monthYear,userBean);
			}

			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return	new ATService().insertUpdate(ats,delete);
		} catch (AppException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}
	
	
	private List<String> convertLineItemIntoList(UploadedCsv lineItem,ReturnType returnType){
		List<String>fieldList=new ArrayList<>();
		
		fieldList.add(lineItem.getCpGstinStateCode());
		fieldList.add(lineItem.getTaxableAmount());

		fieldList.add("1");//	 for serial no
		fieldList.add(lineItem.getHsnSacCode());
		fieldList.add(lineItem.getHsnSacDesc());
		fieldList.add(lineItem.getUom());
		fieldList.add(lineItem.getQuantity());
		fieldList.add(lineItem.getTotalTaxrate());
		fieldList.add(lineItem.getIgstAmount());
		fieldList.add(lineItem.getCgstAmount());
		fieldList.add(lineItem.getSgstAmount());
		fieldList.add(lineItem.getCessAmount());
		
		if(returnType==ReturnType.GSTR2){
		
		}

		return fieldList;
	}
	
	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			_STATE_CODE_INDEX = Integer.valueOf(headers[0]);
			_ADVANCE_AMOUNT_INDEX = Integer.valueOf(headers[1]);
			_SNO_INDEX = Integer.valueOf(headers[2]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[3]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[4]);
			_UNIT_INDEX = Integer.valueOf(headers[5]);
			_QUANTITY_INDEX = Integer.valueOf(headers[6]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[7]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[8]);
			_CGST_AMT_INDEX = Integer.valueOf(headers[9]);
			_SGST_AMT_INDEX = Integer.valueOf(headers[10]);
			_CESS_AMT_INDEX = Integer.valueOf(headers[11]);
			
			if(gstReturn.equalsIgnoreCase("gstr1"))
			_ORIGINAL_MONTH=Integer.valueOf(headers[12]);
			/*if(gstReturn.equalsIgnoreCase("gstr1"))
			_MONTH_YEAR_INDEX = Integer.valueOf(headers[12]);*/

		}

	}

	private void convertListToTransactionData(ExcelError ee, List<ATTransactionEntity> atEntities, List<String> temp,
			int index, String sheetName, String sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin, String monthYear,UserBean userBean) {

		handleAmmendmentCase(temp, TransactionType.AT);

		String gstin = taxPayerGstin.getGstin();
		ee.resetFieldsExceptErrorAndErrorDesc();

		ee.setUniqueRowValue("POS - ["+String.valueOf(ee.getPosValue())+"]");
		
		ATTransactionEntity atEntity = new ATTransactionEntity();
		atEntity.setGstin(taxPayerGstin);
		atEntity.setReturnType(gstReturn);
		atEntity.setMonthYear(monthYear);
		atEntity.setSource(sourceName);
		atEntity.setSourceId(sourceId);
		
		InfoService.setInfo(atEntity, userBean);
		
		Item item = new Item();
		
		List<Item> items = new ArrayList<>();
 		
		items.add(item);
		atEntity.setItems(items);
		
		// setting state code
		ee = this.validatePos(sheetName, _STATE_CODE_INDEX, index, temp.get(_STATE_CODE_INDEX), gstinPos, ee);
		if (!ee.isError()) {
			//atEntity.setPos(String.valueOf(ee.getPosValue()));
			atEntity.setPos(ee.getPos());
			atEntity.setStateName(AppConfig.getStateNameByCode(temp.get(_STATE_CODE_INDEX)).getName());
		}
				
		if (atEntities.contains(atEntity)) {

			atEntity = atEntities.get(atEntities.indexOf(atEntity));
			
		} else {
			atEntities.add(atEntity);
		}
		// setting advance amount
		
		if ("gstr1".equalsIgnoreCase(gstReturn) && SourceType.valueOf(sourceName) != SourceType.TALLY
				&& SourceType.valueOf(sourceName) != SourceType.SAP) {
			if (!StringUtils.isEmpty(temp.get(_ORIGINAL_MONTH))) {
				ee=this.validateMonthYear(sheetName, _ORIGINAL_MONTH, index, temp.get(_ORIGINAL_MONTH), ee,null,true);
				String oMonthYear=temp.get(_ORIGINAL_MONTH);
				oMonthYear=oMonthYear.length()==5?oMonthYear="0"+oMonthYear:oMonthYear;
				atEntity.setOriginalMonth(oMonthYear);
				atEntity.setAmmendment(true);
			}
			
		}

		// setting is igst transaction
		this.setIsIgst(sheetName, gstinPos, gstin, temp.get(_STATE_CODE_INDEX), ee);
		
		atEntity.setSupplyType(ee.isIgst()?SupplyType.INTER:SupplyType.INTRA);
		
		this.validateAdvanceAdjustedValue(sheetName, _ADVANCE_AMOUNT_INDEX, index, temp.get(_ADVANCE_AMOUNT_INDEX), ee);
		if(!ee.isError())
		atEntity.setAdvanceReceived(Double.parseDouble(temp.get(_ADVANCE_AMOUNT_INDEX)));
		
		 double taxAmount=0;
		if(SourceType.valueOf(sourceName)==SourceType.TALLY)
			taxAmount=this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, gstin);
		else
	      taxAmount=this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item,TransactionType.AT);
	
	     atEntity.setTaxAmount(taxAmount);
	
	/*if(gstReturn.equalsIgnoreCase("gstr1")&& SourceType.valueOf(sourceName)!=SourceType.TALLY)
	atEntity.setAmmendmentMonthYear(temp.get(_MONTH_YEAR_INDEX));*/
	
	/*	// setting serial no
		ee = this.validateSno(sheetName, _SNO_INDEX, index, temp.get(_SNO_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SNO_INDEX)))s
			invoiceItem.setSNo(Integer.parseInt(temp.get(_SNO_INDEX)));

		// setting goods and services code
		invoiceItem.setGoodsOrServiceCode(temp.get(_HSN_CODE_INDEX));

		// setting description
		invoiceItem.setHsnDesc(temp.get(_DESCRIPTION_INDEX));

		ee = this.validateHsnOrDesc(sheetName, _HSN_CODE_INDEX, index, temp.get(_HSN_CODE_INDEX),
				temp.get(_DESCRIPTION_INDEX), ee);

		// setting unit
		invoiceItem.setUnit(temp.get(_UNIT_INDEX));

		// setting quantity
		ee = this.validateQuantity(sheetName, _QUANTITY_INDEX, index, temp.get(_QUANTITY_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_QUANTITY_INDEX)))
			invoiceItem.setQuantity(Double.parseDouble(temp.get(_QUANTITY_INDEX)));

		// SETTING tax rate
		ee = this.validateTaxRate(sheetName, _TAX_RATE_INDEX, index, temp.get(_TAX_RATE_INDEX), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_TAX_RATE_INDEX)))
			invoiceItem.setTaxRate(Double.parseDouble(temp.get(_TAX_RATE_INDEX)));

		// setting igst amt
		ee = this.validateIgstAmt(sheetName, _IGST_AMT_INDEX, index, temp.get(_IGST_AMT_INDEX),
				invoiceItem.getTaxRate(), invoiceItem.getAdvanceAmount(), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_IGST_AMT_INDEX)))
			invoiceItem.setIgstAmt(Double.parseDouble(temp.get(_IGST_AMT_INDEX)));

		// setting cgst amt
		ee = this.validateCgstAmt(sheetName, _CGST_AMT_INDEX, index, temp.get(_CGST_AMT_INDEX),
				invoiceItem.getTaxRate(), invoiceItem.getAdvanceAmount(), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CGST_AMT_INDEX)))
			invoiceItem.setIgstAmt(Double.parseDouble(temp.get(_CGST_AMT_INDEX)));

		// setting sgst amt
		ee = this.validateSgstAmt(sheetName, _SGST_AMT_INDEX, index, temp.get(_SGST_AMT_INDEX),
				invoiceItem.getTaxRate(), invoiceItem.getAdvanceAmount(), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_SGST_AMT_INDEX)))
			invoiceItem.setSgstAmt(Double.parseDouble(temp.get(_SGST_AMT_INDEX)));

		// setting Cess amt
		ee = this.validateCessAmount(sheetName, _CESS_AMT_INDEX, index, temp.get(_CESS_AMT_INDEX),
				invoiceItem.getTaxableValue(), invoiceItem.getTaxRate(), ee);
		if (!ee.isError() && !StringUtils.isEmpty(temp.get(_CESS_AMT_INDEX)))
			invoiceItem.setCessAmt(Double.parseDouble(temp.get(_CESS_AMT_INDEX)));

*/
		// setting Cess amt
		//

		/*
		 * if (!ee.isError() && !ee.isFinalError()) addATObject(at, ats);
		 */
		//atEntity.getItems().add(item);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<AT> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<AT>>() {
				});
				List<AT> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<AT>>() {
				});

				if (action == null) {
					existingB2bs = addATObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateATObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteATObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateATObjectByNo(existingB2bs, transactionB2bs);
				} else if (action != null) {
					existingB2bs = changeStatusATObject(existingB2bs, transactionB2bs, action);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<AT> ats = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<AT>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			List<StateDTO> states = new ArrayList<>();

			try {
				finderService = (FinderService) InitialContext.doLookup("java:comp/env/finder");
				states = finderService.getAllState();
			} catch (NamingException e) {
				e.printStackTrace();
			}

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}
			/*
			 * Calendar cal = Calendar.getInstance(); Date currentDate =
			 * cal.getTime(); int currYear = currentDate.getYear(); int
			 * currMonth = currentDate.getMonth();
			 * 
			 * if (currMonth - 1 == inputMonth.getMonth()) {
			 * cal.set(Calendar.DATE, 1); cal.add(Calendar.DATE, -1);
			 * currentDate = cal.getTime(); }
			 */

			Date startDate = new Date();

			double totalTaxAmount = 0.0;
			double totalAdvanceRcvd = 0.0;
			double amTaxAmount = 0.0;
			double amAdvanceRcvd = 0.0;

			double igstAmount = 0.0;
			double cgstAmount = 0.0;
			double sgstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (AT at : ats) {

				StateDTO temp = new StateDTO();
				temp.setId(Double.valueOf(at.getRecipientStateCode()).intValue());
				int i = states.indexOf(temp);
				if (i > -1) {
					StateDTO stateDTO = states.get(states.indexOf(temp));
					at.setStateName(stateDTO.getName());
				} else {
					at.setStateName("No Name");
				}

				double advncRcvdInv = 0.0;
				double taxAmountInv = 0.0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;

				/*
				 * Date doucmentDate = at.getDocumentDate(); String dateError =
				 * ""; if (inputMonth.getMonth() < 9) { startDate.setDate(1);
				 * startDate.setMonth(3); startDate.setYear(inputMonth.getYear()
				 * - 1); dateError =
				 * "Invalid document date, it should be between " +
				 * sdf.format(startDate) + " and " + sdf.format(inputMonth); }
				 * else { startDate.setDate(1); startDate.setMonth(3);
				 * startDate.setYear(inputMonth.getYear()); dateError =
				 * "Invalid document date, it should be between " +
				 * sdf.format(startDate) + " and " + sdf.format(inputMonth); }
				 * if (!(doucmentDate.compareTo(startDate) >= 0 &&
				 * doucmentDate.compareTo(inputMonth) <= 0)) {
				 * errors.put("DOCUMENT_DATE", dateError); errorFlag = false; }
				 */

				/*
				 * if (!StringUtils.isEmpty(at.getOriginalDocumentNumber()) &&
				 * !StringUtils.isEmpty(at.getOriginalGstin())) {
				 * at.setAmendment(true); } else { at.setAmendment(false); }
				 */

				at.setError(errors);
				at.setValid(errorFlag);
				List<InvoiceItem> items = at.getInvoiceItems();
				if (!Objects.isNull(items)) {
					for (InvoiceItem it : items) {

						if (!Objects.isNull(it.getCgstAmt())) {
							taxAmountInv += it.getCgstAmt();
							cgstAmount = +it.getCgstAmt();
						}
						if (!Objects.isNull(it.getSgstAmt())) {
							taxAmountInv += it.getSgstAmt();
							sgstAmount = +it.getSgstAmt();

						}
						if (!Objects.isNull(it.getIgstAmt())) {
							taxAmountInv += it.getIgstAmt();
							igstAmount = +it.getIgstAmt();

						}
						if (!Objects.isNull(it.getCessAmt())) {
							taxAmountInv += it.getCessAmt();
							cessAmount = +it.getCessAmt();

						}
						if (!Objects.isNull(it.getAdvanceAmount())) {
							advncRcvdInv += it.getAdvanceAmount();
						}
					}
				}
				invoiceCount++;

				setAtFlag(at);

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}
				at.setTaxAmount(taxAmountInv);
				at.setAdvanceRcvd(advncRcvdInv);
				if (at.isAmendment()) {

				}

				if (!at.isValid()) {
					at.setType(ReconsileType.ERROR);
					at.setFlags("");

				}

				// at.setFlags(flags.toString());

				if (at.isAmendment()) {
					amTaxAmount += taxAmountInv;
					amAdvanceRcvd += advncRcvdInv;
				} else {
					totalTaxAmount += taxAmountInv;
					totalAdvanceRcvd += advncRcvdInv;
				}

			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", ats);
			TransactionDataDTO atSumm = new TransactionDataDTO();
			atSumm.setTaxAmount(totalTaxAmount);
			atSumm.setAmTaxAmount(amTaxAmount);
			atSumm.setTaxableValue(totalAdvanceRcvd);// storing advance amount
			atSumm.setAmTaxableValue(amAdvanceRcvd);// storing advance amount
			atSumm.setError(summaryError);
			atSumm.setCessAmount(cessAmount);
			atSumm.setIgstAmount(igstAmount);
			atSumm.setCgstAmount(cgstAmount);
			atSumm.setSgstAmount(sgstAmount);
			atSumm.setReconcileDTO(reconcileDTO);
			gstrSummaryDTO.setAta(atSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private void setAtFlag(AT at) {
		if (!Objects.isNull(at.getTaxPayerAction())) {
			if (at.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				at.setFlags(TableFlags.ACCEPTED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.REJECT)
				at.setFlags(TableFlags.REJECTED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.PENDING)
				at.setFlags(TableFlags.PENDING.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.MODIFY)
				at.setFlags(TableFlags.MODIFIED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.DELETE)
				at.setFlags(TableFlags.DELETED.toString());
			else if (at.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				at.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<ATTransactionEntity>b2csDatas=datas;
				for (ATTransactionEntity invoice : b2csDatas) {
					
					for(Item item:invoice.getItems()){
						List<String> columns = this.getFlatData(invoice,item, returnType);
						tabularData.add(columns);
					}
				}

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}
	
	

private List<String> getFlatData(ATTransactionEntity invoice, Item item, String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		columns.add(this.getStringValue(invoice.getPos()));
		columns.add(this.getStringValue(invoice.getAdvanceReceived()));
		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCgst()));
		columns.add(this.getStringValue(item.getSgst()));
		columns.add(this.getStringValue(item.getCess()));
		if(ReturnType.GSTR1.toString().equalsIgnoreCase(returnType))
		columns.add(this.getStringValue(invoice.getMonthYear()));
		columns.add(this.getStringValue(invoice.getErrMsg()));


		return columns;

		
		}

	

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<AT> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<AT>>() {
				});

				data.removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getAta();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceAts = getData(source, returnType);

		Map<String, List<String>> changedAts = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionAts = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceAts) && !Objects.isNull(changedAts)) {

			Iterator it = sourceAts.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedAts.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedAts.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedAts.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceAts.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionAts.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionAts.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionAts.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionAts;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<AT> ats = objectMapper.readValue(object, new TypeReference<List<EXP>>() {
				});

				for (AT at : ats) {
					List<InvoiceItem> items = at.getInvoiceItems();

					for (InvoiceItem item : items) {
						//List<String> columns = getFlatData(at, item, returnType);

						/*
						 * String temp =
						 * StringUtils.isEmpty(at.getDocumentNumber())? "" :
						 * at.getDocumentNumber() + ":" +
						 * item.getGoodsOrServiceCode() == null ? "" :
						 * String.valueOf(item.getGoodsOrServiceCode()) + ":" +
						 * item.getTaxableValue() == null ? "" :
						 * String.valueOf(item.getAdvanceAmount());
						 */
						// tabularData.put(temp, columns);
					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<AT> ats = JsonMapper.objectMapper.readValue(object, new TypeReference<List<AT>>() {
			});

			List<AT> currentAts = new ArrayList<>();

			for (AT at : ats) {

				if (!at.isAmendment() && !at.isTransit()) {
					currentAts.add(at);
					at.setTransit(true);
					at.setTransitId(transitId);
				}

			}

			//gstr1.setAt(currentAts);

			if (currentAts.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(ats);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<AT> ats = JsonMapper.objectMapper.readValue(object, new TypeReference<List<AT>>() {
			});
			for (AT at : ats) {
				if (at.isTransit() && at.getTransitId().equals(transitId)) {
					at.setTransit(false);
					at.setTransitId("");
					at.setGstnSynced(true);
				}
			}
			return JsonMapper.objectMapper.writeValueAsString(ats);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private ReturnsService returnService;
	private CrudService crudService;
	
	public ATTransactionEntity saveTransactionData1(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		boolean isToBeUpdated=false;
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return null;
			} else {
				gstinTransactionDTO.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "b2css"));

				Gstr1Dto gstr=JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(), Gstr1Dto.class);;
				List<B2CSTransactionEntity> transactionb2bs = gstr.getB2css();
				for (B2CSTransactionEntity b2bTran : transactionb2bs) {
					b2bTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					b2bTran.setReturnType(gstinTransactionDTO.getReturnType());
					b2bTran.setGstin((TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
					if(!StringUtils.isEmpty(b2bTran.getId())){
						isToBeUpdated=true;
						this.updateAtTransactionData(b2bTran);
					}
				}
				
				try {
					if(!isToBeUpdated){
					new B2CSService().insertUpdate(transactionb2bs,null);
					}
				} catch (SQLException e) {
					Log.error("exception in saving AT transaction from portal");
					e.printStackTrace();
				} 
				return null;
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}
	
	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");
			finderService = (FinderService) InitialContext.doLookup("java:global/easemygst/FinderServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		gstinTransactionDTO
				.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "ats"));
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		try {

			Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
					Gstr1Dto.class);
			List<ATTransactionEntity> ats = gstr.getAts();
			for (ATTransactionEntity at : ats) {
				at.setGstin(gstin);
				at.setReturnType(gstinTransactionDTO.getReturnType());
				at.setMonthYear(gstinTransactionDTO.getMonthYear());
				double taxableValue=0.0;
				double taxAmount=0.0;
				for(Item item:at.getItems()){
					taxAmount+=item.getTaxAmount();
					taxableValue+=item.getTaxableValue();
					item.setInvoiceId(at.getId());
				}
				at.setTaxAmount(taxAmount);
				at.setAdvanceReceived(taxableValue);
				at.setSource(SourceType.PORTAL.toString());
				String stateName=Objects.nonNull(AppConfig.getStateNameByCode(at.getPos()))?AppConfig.getStateNameByCode(at.getPos()).getName():"";
				at.setStateName(stateName);
				
				if (StringUtils.isEmpty(at.getId())) {
					InfoService.setInfo(at, gstinTransactionDTO.getUserBean());
					try {
						new ATService().insertUpdate(ats,null);
					} catch (SQLException e) {
						e.printStackTrace();
					}

				} else {
					ATTransactionEntity existingAt = crudService.find(ATTransactionEntity.class, at.getId());
					if (existingAt.getIsTransit()) {
						AppException ae = new AppException();
						ae.setMessage("AT invoice is in transit state so can't be updated for the moment");
						_Logger.error("exception--AT invoice is in transit state so can't be updated for the moment ");
						throw ae;
					}
					existingAt.setPos(at.getPos());
					existingAt.setStateName(at.getStateName());
					existingAt.setSupplyType(at.getSupplyType());
					existingAt.setTaxAmount(at.getTaxAmount());
					existingAt.setAdvanceReceived(at.getAdvanceReceived());
					/*existingAt.setAmmendment(at.getIsAmmendment());
					existingAt.setAmmendmentMonthYear(at.getAmmendmentMonthYear());
					existingAt.setFlags(at.getFlags());
					existingAt.setIsTransit(at.getIsTransit());
					existingAt.setMonthYear(at.getMonthYear());
					existingAt.setReturnType(at.getReturnType());*/
					existingAt.setSource(at.getSource());
					existingAt.setIsError(false);
					existingAt.setErrMsg("");
					existingAt.setFlags("");
					existingAt.setOriginalMonth(at.getOriginalMonth());
					/*existingAt.setOriginalPos(at.getOriginalPos());
					existingAt.setOriginalSupplyType(at.getOriginalSupplyType());*/
					existingAt.setIsAmmendment(at.getIsAmmendment());
				

					InfoService.setInfo(existingAt, gstinTransactionDTO.getUserBean());
					crudService.update(existingAt);
					returnService.deleteItemsByInvoiceId(existingAt.getId());
					for(Item itm:at.getItems()){
						itm.setInvoiceId(existingAt.getId());
						crudService.create(itm);
					}

				}

			}

		}  catch (AppException ae) {

			throw ae;
		}
		catch (Exception e) {

			_Logger.error("exception in at save method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return true;

	}
	
	
	private  boolean updateAtTransactionData(B2CSTransactionEntity b2cs)
			throws AppException {

		try {
			if (Objects.isNull(b2cs)) {
				return false;
			} else {
				/*B2CSTransactionEntity b2csTran = crudService.find(B2CSTransactionEntity.class, b2cs.getId());*/
				returnService.deleteItemsByInvoiceId(b2cs.getId());
				crudService.update(b2cs);
			}
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return true;
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
		gstExcel.loadSheet(12);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				
				if(temp!=null) {
//					System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
			}
		}
		return 0;
	}

}
