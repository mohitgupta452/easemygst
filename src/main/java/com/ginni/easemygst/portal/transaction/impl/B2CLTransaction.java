package com.ginni.easemygst.portal.transaction.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.B2CL;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.IMPSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;
import com.ginni.easemygst.portal.transaction.crud.B2BServcie;
import com.ginni.easemygst.portal.transaction.crud.B2CLService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class B2CLTransaction extends TransactionUtil implements Transaction {

	private FinderService finderService;

	int _STATE_CODE_INDEX = 0;
	int _INVOICE_NO_INDEX = 1;
	int _INVOICE_DATE_INDEX = 2;
	int _ETIN_INDEX = 3;
	int _ORIGINAL_INVOICE_NUMBER=13;
	int _ORIGINAL_INVOICE_DATE=14;

	public B2CLTransaction() {

		 _SNO_INDEX = 4;
		 _HSN_CODE_INDEX = 5;
		 _DESCRIPTION_INDEX = 6;
		 _UNIT_INDEX = 7;
		 _QUANTITY_INDEX = 8;
		 _TAXABLE_VALUE_INDEX = 9;
		 _TAX_RATE_INDEX = 10;
		 _IGST_AMT_INDEX = 11;
		 _CESS_AMT_INDEX = 12;
		 _INVOICE_VALUE_INDEX=2;
		_TEMP_TALLY_POS=3;
		 _ORIGINAL_INVOICE_NUMBER=13;
		 _ORIGINAL_INVOICE_DATE=14;
		 
		 System.out.println("Inside B2CL Constructor...");
	}
	List<String> temp=new ArrayList<String>();

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}

	public static List<B2CL> addB2CLObject(B2CL b2cl, List<B2CL> b2cls) {
		List<Invoice> allinvoice = b2cl.getInvoices();
		for (Invoice invoice2 : allinvoice) {
			List<Invoice> invoices = new ArrayList<>();
			List<InvoiceItem> invoiceItems = new ArrayList<>();
			if (invoice2.isGstnSynced())
				invoice2.setTaxPayerAction(TaxpayerAction.MODIFY);
			invoice2.setGstnSynced(false);
			if (b2cls.contains(b2cl)) {
				b2cl = b2cls.get(b2cls.indexOf(b2cl));
				invoices = b2cl.getInvoices();
				if (invoices.contains(invoice2)) {
					List<InvoiceItem> allinvoiceItem = invoice2.getItems();
					invoice2 = invoices.get(invoices.indexOf(invoice2));
					invoiceItems = invoice2.getItems();
					for (InvoiceItem invoiceItem2 : allinvoiceItem) {
						if (!invoiceItems.contains(invoiceItem2)) {
							invoiceItems.add(invoiceItem2);
						}
					}
				} else {
					invoice2.setId(getRandomUniqueId());
					invoices.add(invoice2);
				}
			} else {
				for (Invoice inv : allinvoice) {
					inv.setId(getRandomUniqueId());
				}
				b2cl.setId(getRandomUniqueId());
				b2cls.add(b2cl);
			}
		}

		return b2cls;
	}

	public static List<B2CL> updateB2CLObject(B2CL b2cl, List<B2CL> b2cls) throws AppException {

		List<Invoice> invoices = new ArrayList<>();
		List<Invoice> tInvoices = b2cl.getInvoices();
		B2CL tempB2cl = b2cl;
		String id = b2cl.getId();
		List<B2CL> searchedB2cls = b2cls.stream()
				.filter(b -> !StringUtils.isEmpty(id) && id.equalsIgnoreCase(b.getId())).collect(Collectors.toList());
		if (searchedB2cls.isEmpty()) {// to check if the gstin exist according
										// to handlle client side case
			if (b2cls.contains(b2cl)) {
				searchedB2cls.add(b2cls.get(b2cls.indexOf(b2cl)));
				b2cl.setId(searchedB2cls.get(0).getId());
			}
		}
		if (!searchedB2cls.isEmpty()) {// update existing b2cl
			B2CL existingB2b = searchedB2cls.get(0);
			boolean isInvoiceUpdated = false;
			invoices = existingB2b.getInvoices();
			for (Invoice tInvoice : tInvoices) {
				String invId = tInvoice.getId();
				List<Invoice> searchedInvs = invoices.stream()
						.filter(b -> b.getId() != null && b.getId().equalsIgnoreCase(invId))
						.collect(Collectors.toList());
				if (!searchedInvs.isEmpty()) {// update existing
					Invoice existingInvoice = searchedInvs.get(0);
					invoices.remove(existingInvoice);
					if (tInvoice.isGstnSynced()) {
						tInvoice.setGstnSynced(false);
						tInvoice.setTaxPayerAction(TaxpayerAction.MODIFY);
					}

					// start-code to handle state code change case:block-1
					if (existingB2b.getStateCode() != b2cl.getStateCode()) {// if
																			// change
																			// in
																			// state
																			// code
																			// than
																			// it
																			// will
																			// create
																			// new
																			// b2cl
						if (!b2cls.contains(b2cl)) {// if state code not exist
													// then set id empty so that
													// new id will be genrated
							b2cl.setId("");

						}
						if (invoices.isEmpty())
							b2cls.remove(existingB2b);
						b2cl.getInvoices().get(0).setId("");
						addB2CLObject(b2cl, b2cls);// for creating new b2b on
													// gstin change
						isInvoiceUpdated = true;

					} else
						// End-code to handle gstin or gstin name change case
						invoices.add(tInvoice);
				} else {// add new invoice
					if (StringUtils.isEmpty(tInvoice.getId()))
						if (invoices.contains(tInvoice)) {
							AppException exp = new AppException();
							exp.setCode(ExceptionCode._ERROR);
							exp.setMessage(
									"Invoice with same number already exist, Please choose a different Invoice no");
							throw exp;
						}
					tInvoice.setId(getRandomUniqueId());// genrating ids for new
														// invoices
					tInvoice.setGstnSynced(false);
					invoices.add(tInvoice);
				}

			}
			if (!isInvoiceUpdated) {
				b2cl.setInvoices(invoices);
				b2cls.remove(existingB2b);
				b2cls.add(b2cl);
			}
		} else {// add new b2cl
			if (StringUtils.isEmpty(b2cl.getId()))
				b2cl.setId(getRandomUniqueId());
			for (Invoice inv : b2cl.getInvoices()) {
				if (StringUtils.isEmpty(inv.getId()))
					inv.setId(getRandomUniqueId());// genrating ids for new
													// invoices
				inv.setGstnSynced(false);
			}
			b2cls.add(b2cl);
		}

		return b2cls;
	}

	public static List<B2CL> updateB2CLObjectByNo(B2CL b2cl, List<B2CL> b2cls) throws AppException {
		List<Invoice> invoices = new ArrayList<>();
		List<Invoice> tInvoices = b2cl.getInvoices();
		int stateCode = b2cl.getStateCode();
		List<B2CL> searchedB2cls = b2cls.stream().filter(b -> b.getStateCode() == stateCode)
				.collect(Collectors.toList());

		if (!searchedB2cls.isEmpty()) {// update existing b2cl
			B2CL existingB2cl = searchedB2cls.get(0);
			invoices = existingB2cl.getInvoices();

			for (Invoice tInvoice : tInvoices) {
				String invNumber = tInvoice.getSupplierInvNum();
				List<Invoice> searchedInvs = invoices.stream().filter(
						b -> !StringUtils.isEmpty(invNumber) && invNumber.equalsIgnoreCase(b.getSupplierInvNum()))
						.collect(Collectors.toList());
				if (!searchedInvs.isEmpty()) {// update existing
					Invoice existingInvoice = searchedInvs.get(0);
					invoices.remove(existingInvoice);
					if (tInvoice.isGstnSynced()) {
						tInvoice.setGstnSynced(false);
						tInvoice.setTaxPayerAction(TaxpayerAction.MODIFY);
					}
					if (StringUtils.isEmpty(tInvoice.getId()))// since we are
						tInvoice.setId(existingInvoice.getId());// removing
																// existing
																// invoice and
																// adding new
																// from excel so
																// we need to
																// copy id from
																// existing
																// invoice into
																// new one
					invoices.add(tInvoice);
				} else {// add new invoice
					if (StringUtils.isEmpty(tInvoice.getId()))
						tInvoice.setId(getRandomUniqueId());
					tInvoice.setGstnSynced(false);
					invoices.add(tInvoice);
				}

			}
			if (StringUtils.isEmpty(b2cl.getId()))// since we are removing
				b2cl.setId(existingB2cl.getId()); // existing b2cl and
													// adding new from excel so
													// we
													// need to copy id from
													// existing b2cl into new
													// one
			b2cl.setInvoices(invoices);
			b2cls.remove(existingB2cl);
			b2cls.add(b2cl);
		} else {// add new b2cl
			if (StringUtils.isEmpty(b2cl.getId()))
				b2cl.setId(getRandomUniqueId());
			for (Invoice inv : b2cl.getInvoices()) {
				if (inv.isGstnSynced())
					inv.setTaxPayerAction(TaxpayerAction.MODIFY);
				inv.setGstnSynced(false);
				if (StringUtils.isEmpty(inv.getId()))
					inv.setId(getRandomUniqueId());
				inv.setGstnSynced(false);
			}
			b2cls.add(b2cl);
		}

		return b2cls;
	}

	public static List<B2CL> deleteB2CLObject(B2CL b2cl, List<B2CL> b2cls) {

		InvoiceItem invoiceItem = b2cl.getInvoices().get(0).getItems().get(0);
		List<Invoice> invoices;
		;
		List<InvoiceItem> invoiceItems = new ArrayList<>();
		String bId = b2cl.getId();
		List<B2CL> searchedB2cl = b2cls.stream()
				.filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId())).collect(Collectors.toList());

		boolean action = false;
		if (!searchedB2cl.isEmpty()) {
			invoices = searchedB2cl.get(0).getInvoices();
			List<Invoice> searchedInv = invoices.stream()
					.filter(b -> b2cl.getInvoices() != null
							&& b.getId().equalsIgnoreCase(b2cl.getInvoices().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedInv.isEmpty()) {
				invoiceItems = searchedInv.get(0).getItems();
				if (b2cl.getInvoices().get(0).getItems() != null)
					invoiceItem = b2cl.getInvoices().get(0).getItems().get(0);
				if (invoiceItem.getSNo() != null && invoiceItems.contains(invoiceItem)) {
					action = invoiceItems.remove(invoiceItem);
					searchedInv.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
					searchedInv.get(0).setGstnSynced(false);
				}
				/*
				 * action = action == false ? invoices.remove(invoice) : true;
				 */
				if (action == false) {//
					if (searchedInv.get(0).isGstnSynced()) {
						searchedInv.get(0).setTaxPayerAction(TaxpayerAction.DELETE);
						searchedInv.get(0).setGstnSynced(false);
					} else {
						action = invoices.remove(searchedInv.get(0));
						if (invoices.isEmpty())// will delete b2cl in case of no
												// invoices remaining
							b2cls.remove(searchedB2cl.get(0));
					}
					action = true;
				}

			}
			action = action == false ? b2cls.remove(searchedB2cl.get(0)) : true;
		}

		return b2cls;
	}

	public static List<B2CL> changeStatusB2CLObject(B2CL b2cl, List<B2CL> b2cls, TaxpayerAction taxpayerAction) {

		InvoiceItem invoiceItem = b2cl.getInvoices().get(0).getItems().get(0);
		List<Invoice> invoices;
		;
		List<InvoiceItem> invoiceItems = new ArrayList<>();
		String bId = b2cl.getId();
		List<B2CL> searchedB2cl = b2cls.stream()
				.filter(b -> !StringUtils.isEmpty(bId) && bId.equalsIgnoreCase(b.getId())).collect(Collectors.toList());

		if (!searchedB2cl.isEmpty()) {
			invoices = searchedB2cl.get(0).getInvoices();
			List<Invoice> searchedInv = invoices.stream()
					.filter(b -> b2cl.getInvoices() != null
							&& b.getId().equalsIgnoreCase(b2cl.getInvoices().get(0).getId()))
					.collect(Collectors.toList());
			if (!searchedInv.isEmpty()) {
				invoiceItems = searchedInv.get(0).getItems();
				if (b2cl.getInvoices().get(0).getItems() != null)
					invoiceItem = b2cl.getInvoices().get(0).getItems().get(0);
				if (invoiceItem.getSNo() != null && invoiceItems.contains(invoiceItem)) {
					searchedInv.get(0).setTaxPayerAction(taxpayerAction);
					searchedInv.get(0).setGstnSynced(false);
				}

				searchedInv.get(0).setTaxPayerAction(taxpayerAction);
				searchedInv.get(0).setGstnSynced(false);
			}
		}

		return b2cls;
	}

	public static List<B2CL> addB2CLObject(List<B2CL> existingB2bs, List<B2CL> transactionB2bs) {

		for (B2CL b2cl : transactionB2bs) {
			existingB2bs = addB2CLObject(b2cl, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CL> updateB2CLObject(List<B2CL> existingB2bs, List<B2CL> transactionB2bs) throws AppException {

		for (B2CL b2cl : transactionB2bs) {
			existingB2bs = updateB2CLObject(b2cl, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CL> updateB2CLObjectByNo(List<B2CL> existingB2bs, List<B2CL> transactionB2bs)
			throws AppException {

		for (B2CL b2cl : transactionB2bs) {
			existingB2bs = updateB2CLObjectByNo(b2cl, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CL> deleteB2CLObject(List<B2CL> existingB2bs, List<B2CL> transactionB2bs) {

		for (B2CL b2cl : transactionB2bs) {
			existingB2bs = deleteB2CLObject(b2cl, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CL> changeStatusB2CLObject(List<B2CL> existingB2bs, List<B2CL> transactionB2bs,
			TaxpayerAction taxpayerAction) {

		for (B2CL b2cl : transactionB2bs) {
			existingB2bs = changeStatusB2CLObject(b2cl, existingB2bs, taxpayerAction);
		}

		return existingB2bs;
	}
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}

	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex,TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException {

		List<B2CLTransactionEntity> b2clTrans = new ArrayList<>();
		//List<B2CL> b2cls = new ArrayList<>();

		
		
		//b2clTransaction.setGstin(4);//static value assigned it must be changed
		
		//b2clTrans.add(b2clTransaction);
		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName =source;

		ExcelError ee = new ExcelError();

		try {
			int columnCount = 0;
			int index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if(Objects.nonNull(values))
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
                     
					if(temp == null)
						break;
					if(sourceName==SourceType.TALLY)
						temp=this.processTallyExcel(temp, gstinPos, sheetName, ee);
					_Logger.trace("excel row {}",temp);
					// convert list to b2cl object
					
					index++;
					convertListToTransactionData(ee, b2clTrans, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin,monthYear,userBean);
				}

			}
			this.validateInvoiceValue(sheetName, ee, b2clTrans);
			if (ee.isError()|| ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			int uploadingCount=0;
			
		uploadingCount=	new B2CLService().inserUpdate(b2clTrans,delete);
		//logging user action
		ApiLoggingService apiLoggingService = (ApiLoggingService) InitialContext.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
		MessageDto messageDto=new MessageDto();
		messageDto.setGstin(taxpayerGstin.getGstin());
		messageDto.setReturnType(gstReturn);
		messageDto.setMonthYear(monthYear);
		messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
		messageDto.setTransactionType(TransactionType.B2CL.toString());
		if(!StringUtils.isEmpty(delete)&& delete.equalsIgnoreCase("Yes"))
			apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,taxpayerGstin.getGstin());
			else
		apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto,taxpayerGstin.getGstin());
		//end logging user action
		return uploadingCount;
		
		}
		catch(AppException e)
		{
			_Logger.error("exception",e);
			throw e;
		} catch (Exception e) {
			_Logger.error("sql exception ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}
	
		

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		List<B2CLTransactionEntity> b2clTrans = new ArrayList<>();
		//List<B2CL> b2cls = new ArrayList<>();

		
		
		//b2clTransaction.setGstin(4);//static value assigned it must be changed
		
		//b2clTrans.add(b2clTransaction);
		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					// convert list to b2cl object
					if(temp == null)
						break;
					_Logger.trace("excel row {}",temp);
					// convert list to b2cl object
					index++;
					convertListToTransactionData(ee, b2clTrans, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin,monthYear,userBean);
				}

			}
			scanner.close();

			this.validateInvoiceValue(sheetName, ee, b2clTrans);
			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return	new B2CLService().inserUpdate(b2clTrans,delete);
			
		}
		catch(AppException e)
		{
			_Logger.error("exception",e);
			throw e;
		} catch (Exception e) {
			_Logger.error("sql exception ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	
	public int processCsvDataConsolidated( String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source,List<UploadedCsv>lineItems)
			throws AppException {

		List<B2CLTransactionEntity> b2clTrans = new ArrayList<>();
		//List<B2CL> b2cls = new ArrayList<>();

		String customHeaderIndex="";
				if(gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
					customHeaderIndex="0,1,2,3,4,5,6,7,8,9,10,11,12";
				else
					customHeaderIndex="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22";

		
		
		//b2clTransaction.setGstin(4);//static value assigned it must be changed
		
		//b2clTrans.add(b2clTransaction);
		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(customHeaderIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		try {
			int index = 0;
			
			for(UploadedCsv lineItem:lineItems) {
				
					
					// convert list to b2cl object
				index=lineItem.getLineNumber();
					convertListToTransactionData(ee, b2clTrans, this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, taxpayerGstin,monthYear,userBean);

			}

			this.validateInvoiceValue(sheetName, ee, b2clTrans);
			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return	new B2CLService().inserUpdate(b2clTrans,delete);
			
		}
		catch(AppException e)
		{
			_Logger.error("exception",e);
			throw e;
		} catch (Exception e) {
			_Logger.error("sql exception ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	private List<String> convertLineItemIntoList(UploadedCsv lineItem,ReturnType returnType){
		List<String>fieldList=new ArrayList<>();
		
		fieldList.add(lineItem.getCpGstinStateCode());
		fieldList.add(lineItem.getEntryNo());
		fieldList.add(lineItem.getEntryDate());
		fieldList.add(lineItem.getEtin());
		fieldList.add("1");//	 for serial no
		fieldList.add(lineItem.getHsnSacCode());
		fieldList.add(lineItem.getHsnSacDesc());
		fieldList.add(lineItem.getUom());
		fieldList.add(lineItem.getQuantity());
		fieldList.add(lineItem.getTaxableAmount());
		fieldList.add(lineItem.getTotalTaxrate());
		fieldList.add(lineItem.getIgstAmount());
		fieldList.add(lineItem.getCessAmount());

		return fieldList;
	}

	
	/*public int processCsvDataNew( String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {

		List<UploadedCsv>lineItems=new ArrayList<>();
		List<B2CLTransactionEntity> b2clTrans = new ArrayList<>();
		//List<B2CL> b2cls = new ArrayList<>();

		
		
		//b2clTransaction.setGstin(4);//static value assigned it must be changed
		
		//b2clTrans.add(b2clTransaction);
		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		try {
			int index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			for(UploadedCsv lineItem:lineItems){
				List<String> lineItem = csvParser.parseLine(scanner.nextLine());
					_LOGGER.debug("excel row  {} ", lineItem.getCpClassName() );
					
					B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();

					b2bTransaction.setTaxpayerGstin(taxpayerGstin);

					b2bTransaction.setMonthYear(monthYear);
					// convert list to b2b object
					index++;
					convertListToB2bTransactionCsv(ee, b2bTransaction, b2bTransactions, lineItem, index, sheetName, sourceName,
							sourceId, gstReturn, gstinPos, gstin, userBean);
			}

			this.validateInvoiceValue(sheetName, ee, b2clTrans);
			if (ee.isError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}

			return	new B2CLService().inserUpdate(b2clTrans,delete);
			
		}
		catch(AppException e)
		{
			_Logger.error("exception",e);
			throw e;
		} catch (Exception e) {
			_Logger.error("sql exception ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	*/
	
	
	
	
	public int saveTransaction(List<B2CLTransactionEntity> b2clTrans) throws AppException
	{
		try{
		return	new B2CLService().inserUpdate(b2clTrans,null);
		
	}
	 catch (Exception e) {
		_Logger.error("sql exception ",e);
		throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
	}
	}
	
	public static void setColumns(String headerIndex, String gstReturn, B2CLTransaction b2clTransaction) {
		b2clTransaction.setColumnMapping(headerIndex, gstReturn);
	}

	public void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			_STATE_CODE_INDEX = Integer.valueOf(headers[0]);
			_INVOICE_NO_INDEX = Integer.valueOf(headers[1]);
			_INVOICE_DATE_INDEX = Integer.valueOf(headers[2]);
			_ETIN_INDEX = Integer.valueOf(headers[3]);
			_SNO_INDEX = Integer.valueOf(headers[4]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[5]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[6]);
			_UNIT_INDEX = Integer.valueOf(headers[7]);
			_QUANTITY_INDEX = Integer.valueOf(headers[8]);
			_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[9]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[10]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[11]);
			_CESS_AMT_INDEX = Integer.valueOf(headers[12]);

		}

	}

	public  void convertListToData(ExcelError ee, List<B2CLTransactionEntity> b2cls, List<String> temp, int index,
			String sheetName, SourceType sourceName, String sourceId, String gstReturn, String gstinPos, TaxpayerGstin gstin,UserBean userBean,
			 String monthYear) {
	  convertListToTransactionData(ee, b2cls, temp, index, sheetName, sourceName, sourceId, gstReturn, gstinPos, gstin,
				monthYear,userBean);
	}

	private void convertListToTransactionData(ExcelError ee, List<B2CLTransactionEntity> b2cls, List<String> temp, int index,
			String sheetName, SourceType sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin taxPayerGstin,String monthYear,UserBean userBean) {
		handleAmmendmentCase(temp, TransactionType.B2CL);

		
		ee.setUniqueRowValue("State - ["+temp.get(_STATE_CODE_INDEX)+"] InvoiceNo - ["+temp.get(_INVOICE_NO_INDEX)+"]");
		
		String gstin=taxPayerGstin.getGstin();
		//B2CL b2cl = new B2CL();
		B2CLTransactionEntity b2cl=new B2CLTransactionEntity();
		B2CLDetailEntity b2clDetail=new B2CLDetailEntity();
		b2cl.setGstin(taxPayerGstin);
		b2cl.setMonthYear(monthYear);
		b2cl.setReturnType(gstReturn);
		
		// set sourceb2cl
		b2clDetail.setSource(sourceName.toString());
		b2clDetail.setSourceId(sourceId);
		
		if(b2cls.contains(b2cl))
			b2cl=b2cls.get(b2cls.indexOf(b2cl));
		else
			b2cls.add(b2cl);
		b2clDetail.setB2clTransaction(b2cl);
		//b2clDetail.setInvoiceData(invoice);
		
		InfoService.setInfo(b2cl, userBean);
		InfoService.setInfo(b2clDetail, userBean);

		
		Item item = new Item();
		
		if(sourceName==SourceType.SAP)
			ee=this.validateStateCode(sheetName, _STATE_CODE_INDEX, index, temp.get(_STATE_CODE_INDEX), ee);
			else
		// setting state code
		ee = this.validatePos(sheetName, _STATE_CODE_INDEX, index, temp.get(_STATE_CODE_INDEX), gstinPos, ee);
		if (!ee.isError()) {
			
			String stCode=String.valueOf(ee.getPosValue());
			b2clDetail.setPos(stCode);
			b2clDetail.setStateName(AppConfig.getStateNameByCode(stCode).getName());
			this.setIsIgst(sheetName, gstinPos, gstin, ee.getPos(), ee);
			this.validateB2CLPos(sheetName, _STATE_CODE_INDEX, index, ee.getPos(), gstinPos, ee);
		}
		// setting invoice number
		ee = this.validateInvoiceNo(sheetName, _INVOICE_NO_INDEX, index, temp.get(_INVOICE_NO_INDEX), ee,true);
		if (!ee.isError())
			b2clDetail.setInvoiceNumber(temp.get(_INVOICE_NO_INDEX));

		// setting invoice date
		this.validateInvoiceDate(sheetName, index, index, temp.get(_INVOICE_DATE_INDEX), ee, monthYear,TransactionType.B2CL);
		if (!ee.isError())
			b2clDetail.setInvoiceDate(ee.getDate());

		// setting invoice date
		ee = this.validateEtin(sheetName, _INVOICE_DATE_INDEX, index, temp.get(_ETIN_INDEX), ee);
		if (!ee.isError())
			b2clDetail.setEtin(temp.get(_ETIN_INDEX));
		

		if ("gstr1".equalsIgnoreCase(gstReturn) && sourceName != SourceType.TALLY
				&& sourceName != SourceType.SAP ) {
				boolean isAmmendment=false;
				isAmmendment = this.isAmmendment(temp.get(_ORIGINAL_INVOICE_NUMBER), temp.get(_ORIGINAL_INVOICE_DATE), null,
						null, null,null,null);
				if(isAmmendment){
					this.validateInvoiceNo(sheetName, _ORIGINAL_INVOICE_NUMBER, index, temp.get(_ORIGINAL_INVOICE_NUMBER), ee, true);

							b2clDetail.setOriginalInvoiceNumber(temp.get(_ORIGINAL_INVOICE_NUMBER));
				b2clDetail.setAmmendment(true);

			
			
				this.validateOriginalInvoiceDate(sheetName, _ORIGINAL_INVOICE_DATE, index, temp.get(_ORIGINAL_INVOICE_DATE), ee, b2cl.getMonthYear(),TransactionType.B2CL,true);
				b2clDetail.setAmmendment(true);

				if(!ee.isError()){
					b2clDetail.setOriginalInvoiceDate(ee.getOriginalInvoiceDate());
				}
			}
		}
		
		double taxAmount=0;
		if(sourceName==SourceType.TALLY)
			taxAmount=	this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, gstin);
		else
			taxAmount=this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item,TransactionType.B2CL);

		  if(b2cl.getB2clDetails() ==null)
		  {
			  b2cl.setB2clDetails(new ArrayList<B2CLDetailEntity>());
			  b2cl.getB2clDetails().add(b2clDetail);
			  List<Item> items=new ArrayList<>();
			  b2clDetail.setTaxableValue(item.getTaxableValue());
			  b2clDetail.setTaxAmount(taxAmount);
			    items.add(item);
			    b2clDetail.setItems(items);
		  }
		  else
			 if(b2cl.getB2clDetails().contains(b2clDetail))
			 {
			     b2clDetail=b2cl.getB2clDetails().get(b2cl.getB2clDetails().indexOf(b2clDetail));
			     b2clDetail.setTaxableValue(BigDecimal.valueOf(b2clDetail.getTaxableValue()).add(BigDecimal.valueOf(item.getTaxableValue())).doubleValue());
			     b2clDetail.setTaxAmount(BigDecimal.valueOf(b2clDetail.getTaxAmount()).add(BigDecimal.valueOf(item.getTaxAmount())).doubleValue());
			     b2clDetail.getItems().add(item);
			     
			 }
		  
			     else{
					  b2cl.getB2clDetails().add(b2clDetail);

						//b2clDetail.setInvoiceItems(invoiceItems);
					    b2clDetail.setTaxableValue(item.getTaxableValue());
					    b2clDetail.setTaxAmount(item.getTaxAmount());
					    List<Item> items=new ArrayList<>();
					    items.add(item);
					    b2clDetail.setItems(items);
			     }

	}
	
	private ExcelError validateB2CLPos(String sheetName, int _PLACE_OF_SUPPLY_INDEX, int index, String placeOfSupply,
			String gstinPos, ExcelError ee)
	{
		
		boolean isError=false;
		StringBuilder error = ee.getErrorDesc();

		if(!ee.isIgst())
		{
			error.append(sheetName + "," + (_PLACE_OF_SUPPLY_INDEX + 1) + "," + index
					+ ",Invalid pos it should not same as State code of Supplier|");
			isError=true;
		}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}

	private ExcelError validateInvoiceValue(String sheetName,ExcelError ee,List<B2CLTransactionEntity> b2clTransactions)
	{
		boolean isError=false;
		StringBuilder error = ee.getErrorDesc();
		
		for(B2CLTransactionEntity b2clTransaction : b2clTransactions)
		{
			for(B2CLDetailEntity b2clDetailEntity :b2clTransaction.getB2clDetails())
			{

		if((b2clDetailEntity.getTaxableValue()+b2clDetailEntity.getTaxAmount()) < 250000.00)
		{
			error.append(sheetName + ",-,-,"
					+ " Invoice value must be greater than 250000 in invoice number "+b2clDetailEntity.getInvoiceNumber()+"|");
			isError=true;
		}
			}
	}
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return ee;
	}
	
	
	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<B2CL> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<B2CL>>() {
				});
				List<B2CL> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<B2CL>>() {
				});

				if (action == null) {
					existingB2bs = addB2CLObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateB2CLObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteB2CLObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateB2CLObjectByNo(existingB2bs, transactionB2bs);
				} else if (action != null) {
					existingB2bs = changeStatusB2CLObject(existingB2bs, transactionB2bs, action);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Object validateData(Object object, String monthYear) throws AppException {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<B2CL> b2cls = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<B2CL>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			List<StateDTO> states = new ArrayList<>();

			try {
				finderService = (FinderService) InitialContext.doLookup("java:comp/env/finder");
				states = finderService.getAllState();
			} catch (NamingException e) {
				e.printStackTrace();
			}

			Date inputMonth = new Date();
			if (!StringUtils.isEmpty(monthYear) && monthYear.length() == 6) {
				int inpMonth = Integer.parseInt(monthYear.substring(0, 2));
				int inpYear = Integer.parseInt(monthYear.substring(2, 6));
				Calendar cal1 = Calendar.getInstance();
				cal1.set(Calendar.MONTH, inpMonth);
				cal1.set(Calendar.YEAR, inpYear);
				cal1.set(Calendar.DATE, 1);
				cal1.add(Calendar.DATE, -1);
				inputMonth = cal1.getTime();
			} else {
				throw new IOException();
			}

			/*
			 * Calendar cal = Calendar.getInstance(); Date currentDate =
			 * cal.getTime(); int currYear = currentDate.getYear(); if
			 * (currentDate.before(inputMonth)) { inputMonth = currentDate; }
			 */

			Date startDate = new Date();

			double totalTaxableValue = 0.0;
			double totalTaxAmount = 0;
			double amTaxableValue = 0.0;
			double amTaxAmount = 0;

			double igstAmount = 0.0;
			double cgstAmount = 0.0;
			double sgstAmount = 0.0;
			double cessAmount = 0.0;

			int errorCount = 0;
			int invoiceCount = 0;

			boolean summaryError = false;

			for (B2CL b2cl : b2cls) {

				double taxableValue = 0.0;
				double taxAmount = 0;

				StateDTO temp = new StateDTO();
				temp.setId(b2cl.getStateCode());
				int i = states.indexOf(temp);
				if (i > -1) {
					StateDTO stateDTO = states.get(states.indexOf(temp));
					b2cl.setStateName(stateDTO.getName());
				} else {
					b2cl.setStateName("No Name");
				}

				List<Invoice> invoices = b2cl.getInvoices();
				for (Invoice invoice : invoices) {

					double taxableValueInv = 0.0;
					double taxAmountInv = 0;
					Map<String, String> errors = new HashMap<>();
					boolean errorFlag = true;
					/*
					 * Double invoiceValue = invoice.getSupplierInvVal(); if
					 * (invoiceValue <= 250000.0) { errors.put("INVOICE_VALUE",
					 * "Invalid invoice value, it must be greater than 2.5L");
					 * errorFlag = false; }
					 */
					Date invoiceDate = invoice.getSupplierInvDt();
					String dateError = "";
					if (inputMonth.getMonth() < 9) {
						startDate.setDate(1);
						startDate.setMonth(3);
						startDate.setYear(inputMonth.getYear() - 1);
						dateError = "Invalid invoice date, it should be between " + sdf.format(startDate) + " and "
								+ sdf.format(inputMonth);
					} else {
						startDate.setDate(1);
						startDate.setMonth(3);
						startDate.setYear(inputMonth.getYear());
						dateError = "Invalid invoice date, it should be between " + sdf.format(startDate) + " and "
								+ sdf.format(inputMonth);
					}
					if (!(invoiceDate.compareTo(startDate) >= 0 && invoiceDate.compareTo(inputMonth) <= 0)) {
						errors.put("INVOICE_DATE", dateError);
						errorFlag = false;
					}

					//ammendment

					List<InvoiceItem> items = invoice.getItems();
					if (!Objects.isNull(items)) {
						for (InvoiceItem it : items) {
							taxableValueInv += it.getTaxableValue();
							if (!Objects.isNull(it.getCgstAmt())) {
								// cgstAmount += it.getCgstAmt();
							}
							if (!Objects.isNull(it.getSgstAmt())) {
								// sgstAmount += it.getSgstAmt();
							}
							if (!Objects.isNull(it.getIgstAmt())) {
								taxAmountInv += it.getIgstAmt();
								igstAmount += it.getIgstAmt();
							}
							if (!Objects.isNull(it.getCessAmt())) {
								taxAmountInv += it.getCessAmt();
								cessAmount += it.getCessAmt();
							}
						}
					}
					invoice.setTaxableValue(taxableValueInv);
					invoice.setTaxAmount(taxAmountInv);
					taxableValue += taxableValueInv;
					taxAmount += taxAmountInv;

					invoice.setError(errors);
					invoice.setValid(errorFlag);

					invoiceCount++;

					if (!errorFlag) {
						summaryError = true;
						errorCount += 1;
					}

					if (invoice.isAmendment()) {
						amTaxableValue += taxableValueInv;
						amTaxAmount += taxAmountInv;
					} else {
						totalTaxableValue += taxableValueInv;
						totalTaxAmount += taxAmountInv;
					}

					if (invoice.isAmendment()) {
						// amend flag
					}

					setInvoiceFlag(invoice);

					if (!invoice.isValid()) {
						invoice.setType(ReconsileType.ERROR);
						invoice.setFlags("");
					}
				}
				b2cl.setTaxableValue(taxableValue);
				b2cl.setTaxAmount(taxAmount);
			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", b2cls);
			TransactionDataDTO b2clSumm = new TransactionDataDTO();
			b2clSumm.setTaxableValue(totalTaxableValue);
			b2clSumm.setTaxAmount(totalTaxAmount);
			b2clSumm.setAmTaxableValue(amTaxableValue);
			b2clSumm.setAmTaxAmount(amTaxAmount);
			b2clSumm.setError(summaryError);
			b2clSumm.setCessAmount(cessAmount);
			b2clSumm.setIgstAmount(igstAmount);
			b2clSumm.setCgstAmount(cgstAmount);
			b2clSumm.setSgstAmount(sgstAmount);
			b2clSumm.setReconcileDTO(reconcileDTO);
			gstrSummaryDTO.setB2cl(b2clSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void setInvoiceFlag(Invoice invoice) {
		if (!Objects.isNull(invoice.getTaxPayerAction())) {
			if (invoice.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				invoice.setFlags(TableFlags.ACCEPTED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.REJECT)
				invoice.setFlags(TableFlags.REJECTED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.PENDING)
				invoice.setFlags(TableFlags.PENDING.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.MODIFY)
				invoice.setFlags(TableFlags.MODIFIED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.DELETE)
				invoice.setFlags(TableFlags.DELETED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				invoice.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<B2CLDetailEntity>b2bDatas=datas;
				for (B2CLDetailEntity invoice : b2bDatas) {
					
					for(Item item:invoice.getItems()){
						List<String> columns = this.getFlatData(invoice,item, returnType);
						tabularData.add(columns);
					}
				}

		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}
	
private List<String> getFlatData(B2CLDetailEntity invoice, Item item, String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		columns.add(this.getStringValue(invoice.getPos()));
		columns.add(this.getStringValue(invoice.getInvoiceNumber()));
		columns.add(this.getStringValue(invoice.getInvoiceDate()));
		columns.add(this.getStringValue(invoice.getEtin()));
		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxableValue()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCess()));
		
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));


		return columns;

		
		}
	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<B2CL> b2cls = objectMapper.readValue(object, new TypeReference<List<B2CL>>() {
				});

				for (B2CL b2cl : b2cls) {
					List<Invoice> dataInvoices = b2cl.getInvoices();

					for (Invoice invoice : dataInvoices) {

						for (InvoiceItem item : invoice.getItems()) {
							/*List<String> columns = this.getFlatData(b2cl, invoice, item, returnType);
							String temp = b2cl.getStateCode() == null ? ""
									: b2cl.getStateCode() + ":" + invoice.getSupplierInvNum() + ":"
											+ item.getSNo() == null ? "" : String.valueOf(item.getSNo());
							tabularData.put(temp, columns);
*/
						}

					}

				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<B2CL> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<B2CL>>() {
				});

				for (B2CL b2cl : data) {
					b2cl.getInvoices().removeIf(i -> i.getTaxPayerAction() == TaxpayerAction.DELETE);
				}

				data.removeIf(i -> i.getInvoices().isEmpty());

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {

		List<B2CL> b2cls = new ArrayList<>();

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.GINESYS.toString();

		try {
			JsonNode objects = JsonMapper.objectMapper.readTree(data);

			for (JsonNode jsonNode : objects) {

				B2CL b2cl = new B2CL();

				Invoice invoice = new Invoice();
				InvoiceItem invoiceItem = new InvoiceItem();
				List<Invoice> invoices = new ArrayList<>();
				List<InvoiceItem> items = new ArrayList<>();

				// set source
				invoice.setSource(sourceName);
				invoice.setSourceId(sourceId);

				b2cl.setInvoices(invoices);

				items.add(invoiceItem);
				invoices.add(invoice);
				invoice.setItems(items);

				// get state code
				if (jsonNode.has("stateCode")) {
					int sc = Double.valueOf(jsonNode.get("stateCode").asText()).intValue();
					b2cl.setStateCode(sc);
				}

				if (jsonNode.has("ctin_name"))
					// b2cl.setGstinName(jsonNode.get("ctin_name").asText());

					// setting invoice number
					if (jsonNode.has("invoiceNo"))
						invoice.setSupplierInvNum(jsonNode.get("invoiceNo").asText());

				// setting invoice date
				try {
					if (jsonNode.has("invoiceDate"))
						invoice.setSupplierInvDt(sdfGcc.parse(jsonNode.get("invoiceDate").asText()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				// setting supplier invoice value
				if (jsonNode.has("invoiceValue"))
					invoice.setSupplierInvVal(Double.parseDouble(jsonNode.get("invoiceValue").asText()));

				// setting place of supply
				if (jsonNode.has("pos")) {
					int pos = Double.valueOf(jsonNode.get("pos").asText()).intValue();
					invoice.setPos(pos + "");
				}

				/*
				 * if (jsonNode.has("provisional")) { if
				 * (jsonNode.get("provisional").asText().equalsIgnoreCase("Y")
				 * ||
				 * jsonNode.get("provisional").asText().equalsIgnoreCase("YES"))
				 * //invoice.setProvisionalAssessment(true); else if
				 * (jsonNode.get("provisional").asText().equalsIgnoreCase("N")
				 * ||
				 * jsonNode.get("provisional").asText().equalsIgnoreCase("NO"))
				 * System.err.println(); //
				 * invoice.setProvisionalAssessment(false); }
				 */

				// setting invoice item no
				if (jsonNode.has("sNo")) {
					int sno = Double.valueOf(jsonNode.get("sNo").asText()).intValue();
					invoiceItem.setSNo(sno);
				}

				// setting whether this is goods or services
				if (jsonNode.has("gs")) {
					if (jsonNode.get("gs").asText().equalsIgnoreCase("G")
							|| jsonNode.get("gs").asText().equalsIgnoreCase("GOODS")
							|| jsonNode.get("gs").asText().equalsIgnoreCase("GOOD"))
						System.out.println();// invoiceItem.setGoodsOrService(GoodsService.GOODS);
					else if (jsonNode.get("gs").asText().equalsIgnoreCase("S")
							|| jsonNode.get("gs").asText().equalsIgnoreCase("SERVICES")
							|| jsonNode.get("gs").asText().equalsIgnoreCase("SERVICE"))
						System.out.println();// invoiceItem.setGoodsOrService(GoodsService.SERVICE);
				}

				if (jsonNode.has("hsnsac"))
					invoiceItem.setGoodsOrServiceCode(jsonNode.get("hsnsac").asText());

				if (jsonNode.has("taxableValue"))
					invoiceItem.setTaxableValue(Double.parseDouble(jsonNode.get("taxableValue").asText()));
				if (jsonNode.has("igstr"))
					// invoiceItem.setIgstRate(Double.parseDouble(jsonNode.get("igstr").asText()));
					if (jsonNode.has("igsta"))
						invoiceItem.setIgstAmt(Double.parseDouble(jsonNode.get("igsta").asText()));

				addB2CLObject(b2cl, b2cls);
			}

			return JsonMapper.objectMapper.writeValueAsString(b2cls);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {

		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getB2cl();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {
		Map<String, List<String>> sourceB2cls = getData(source, returnType);

		Map<String, List<String>> changedB2cls = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionB2cls = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceB2cls) && !Objects.isNull(changedB2cls)) {

			Iterator it = sourceB2cls.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedB2cls.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();// source data
					Object temp2 = changedB2cls.get(pair.getKey());// changed
																	// data
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedB2cls.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceB2cls.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionB2cls.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionB2cls.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionB2cls.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionB2cls;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<B2CL> b2cls = JsonMapper.objectMapper.readValue(object, new TypeReference<List<B2CL>>() {
			});

			List<B2CL> currentB2cls = new ArrayList<>();

			for (B2CL b2cl : b2cls) {
				B2CL b2clNew = new B2CL();
				List<Invoice> invoicesNew = new ArrayList<>();
				b2clNew.setStateCode(b2cl.getStateCode());
				b2clNew.setStateName(b2cl.getStateName());
				// b2clNew.setGstinName(b2cl.getGstinName());
				b2clNew.setInvoices(invoicesNew);
				List<Invoice> invoices = b2cl.getInvoices();
				for (Invoice invoice : invoices) {
					if (!invoice.isAmendment() && !invoice.isTransit()) {
						invoicesNew.add(invoice);
						invoice.setTransit(true);
						invoice.setTransitId(transitId);
					}
				}
				if (!b2clNew.getInvoices().isEmpty()) {
					currentB2cls.add(b2clNew);
				}
			}

		//	gstr1.setB2cl(currentB2cls);

			if (currentB2cls.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(b2cls);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<B2CL> b2cls = JsonMapper.objectMapper.readValue(object, new TypeReference<List<B2CL>>() {
			});

			for (B2CL b2cl : b2cls) {
				List<Invoice> invoices = b2cl.getInvoices();
				for (Invoice invoice : invoices) {
					if (invoice.isTransit() && invoice.getTransitId().equals(transitId)) {
						invoice.setTransit(false);
						invoice.setTransitId("");

						invoice.setGstnSynced(true);
					}
				}
			}

			return JsonMapper.objectMapper.writeValueAsString(b2cls);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	private ReturnsService returnService;
	private CrudService crudService;

	private boolean isDate_index;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private boolean isInvoice_number;

	private int _SUPPLY_VALUE_INDEX;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;
	

	public B2CLTransactionEntity saveTransactionData1(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		boolean isToBeUpdated=false;
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return null;
			} else {
				gstinTransactionDTO.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "b2bs"));

				Gstr1Dto gstr=JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(), Gstr1Dto.class);;
				List<B2BTransactionEntity> transactionb2bs = gstr.getB2bs();
				for (B2BTransactionEntity b2bTran : transactionb2bs) {
					b2bTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					b2bTran.setReturnType(gstinTransactionDTO.getReturnType());
					b2bTran.setTaxpayerGstin((TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
					B2BDetailEntity inv=b2bTran.getB2bDetails().get(0);
					if(!StringUtils.isEmpty(inv.getId())){
						isToBeUpdated=true;
						this.updateB2bTransactionData(inv);
					}
					
					
				}
				
				try {
					if(!isToBeUpdated)
					new B2BServcie().insertion(transactionb2bs,null);
				} catch (Exception e) {
					Log.error("exception in saving b2b transaction from portal");
					e.printStackTrace();
				}
				return null;
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {

		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return false;
			} else {
				gstinTransactionDTO
						.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "b2cls"));

				Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
						Gstr1Dto.class);
				;

				List<B2CLTransactionEntity> transactionb2cls = gstr.getB2cls();
				for (B2CLTransactionEntity b2clTran : transactionb2cls) {
					double taxableValue = 0.0;
					double taxAmount = 0.0;
					b2clTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					b2clTran.setReturnType(gstinTransactionDTO.getReturnType());
					b2clTran.setGstin((TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));

					B2CLDetailEntity inv = b2clTran.getB2clDetails().get(0);
					inv.setB2clTransaction(b2clTran);
					for (Item item : inv.getItems()) {
						taxableValue += item.getTaxableValue();
						taxAmount += item.getTaxAmount();
						item.setInvoiceId(inv.getId());
					}
					inv.setTaxableValue(taxableValue);
					inv.setTaxAmount(taxAmount);
					if (StringUtils.isEmpty(inv.getId())) {// create invoice

						InfoService.setInfo(b2clTran, gstinTransactionDTO.getUserBean());
						InfoService.setInfo(inv, gstinTransactionDTO.getUserBean());
						try {

							new B2CLService().inserUpdate(transactionb2cls,null);
						} catch (SQLException e) {
							Log.error("exception in saving b2cl transaction from portal");
							e.printStackTrace();
						}

						// add new invoice
					} else {

						B2CLDetailEntity existingInv = crudService.find(B2CLDetailEntity.class, inv.getId());
						this.checkIsEditible(existingInv, inv);

						existingInv.setInvoiceNumber(inv.getInvoiceNumber());
						existingInv.setInvoiceDate(inv.getInvoiceDate());
						existingInv.setTaxableValue(inv.getTaxableValue());
						existingInv.setEtin(inv.getEtin());
						existingInv.setType(inv.getType());
						existingInv.setPos(inv.getPos());
						existingInv.setTaxAmount(inv.getTaxAmount());
						existingInv.setIsError(false);
						existingInv.setErrMsg("");
						existingInv.setFlags("");
						existingInv.setOriginalInvoiceDate(inv.getOriginalInvoiceDate());;
						existingInv.setOriginalInvoiceNumber(inv.getOriginalInvoiceNumber());;
						existingInv.setIsAmmendment(inv.getIsAmmendment());

						InfoService.setInfo(existingInv, gstinTransactionDTO.getUserBean());
						crudService.update(existingInv);
						// crudService.update(existingB2cl);
						returnService.deleteItemsByInvoiceId(existingInv.getId());
						for (Item itm : inv.getItems()) {
							itm.setInvoiceId(existingInv.getId());
							crudService.create(itm);
						}
					}
				}
				return true;
			}
		} catch (AppException ae) {

			throw ae;
		} catch (Exception e) {

			_Logger.error("exception in b2cl method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}
	
	private boolean checkIsEditible(B2CLDetailEntity existingInvoice,B2CLDetailEntity editedInvoice) throws AppException{
		if (existingInvoice.getIsTransit()) {
			AppException ae = new AppException();
			ae.setMessage("invoice is in transit state so can't be updated for the moment");
			_Logger.error("exception--invoice is in transit state so can't be updated for the moment ");
			throw ae;
		}
		if (existingInvoice.getIsSynced()
				&& (!existingInvoice.getInvoiceNumber().equalsIgnoreCase(editedInvoice.getInvoiceNumber())
				||existingInvoice.getInvoiceDate().compareTo(editedInvoice.getInvoiceDate())!=0)) {
			
			AppException ae = new AppException();
			ae.setMessage("invoice has been synced with GSTN so invoice number and invoice date can't be updated for the moment");
			_Logger.error("invoice has been synced with GSTN so invoice number and invoice date can't be updated for the moment ");
			throw ae;
		}
		
		return true;
	}
	
	private  boolean updateB2bTransactionData(B2BDetailEntity b2cs)
			throws AppException {

		try {
			if (Objects.isNull(b2cs)) {
				return false;
			} else {
				/*B2CSTransactionEntity b2csTran = crudService.find(B2CSTransactionEntity.class, b2cs.getId());*/
				returnService.deleteItemsByInvoiceId(b2cs.getId());
				crudService.update(b2cs);
			}
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
		return true;
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
		gstExcel.loadSheet(3);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}

				if(temp!=null) {
//					System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2cl")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2cl")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2cl")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2cl")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2cl")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2cl")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
			}
		}
		return 0;
	}
}
