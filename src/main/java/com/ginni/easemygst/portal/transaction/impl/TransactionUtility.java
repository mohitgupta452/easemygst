package com.ginni.easemygst.portal.transaction.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CLTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.EXPTransactionEntity;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class TransactionUtility extends TransactionUtil {

	int _GSTIN_INDEX = 0;
	int _GSTIN_NAME_INDEX = 1;
	int _INVOICE_NO_INDEX = 2;
	int _INVOICE_DATE_INDEX = 3;
	int _PLACE_OF_SUPPLY_INDEX = 4;
	int _IS_REVERSE_INDEX = 5;
	int _ETIN_INDEX = 6;
	int _SNO_INDEX = 7;
	int _HSN_CODE_INDEX = 8;
	int _DESCRIPTION_INDEX = 9;
	int _UNIT_INDEX = 10;
	int _QUANTITY_INDEX = 11;
	int _TAXABLE_VALUE_INDEX = 12;
	int _TAX_RATE_INDEX = 13;
	int _IGST_AMT_INDEX = 14;
	int _CGST_AMT_INDEX = 15;
	int _SGST_AMT_INDEX = 16;
	int _CESS_AMT_INDEX = 17;
	int _MONTH_YEAR_INDEX = 18;
	int _TYP_INDEX = 19;
	int _INVOICE_TYPE_INDEX = 20;
	int _SHIPPING_PORT_CODE = 21;
	int _SHIPPING_BILL_NO_INDEX = 22;
	int _SHIPPING_BILL_DATE_INDEX = 23;

	String b2bColumns = "";
	String b2clColumns = "";
	String b2csColumns = "";
	String expColumns = "";

	public com.fasterxml.jackson.databind.JsonNode processExcelList(GstExcel gstExcel, String gstReturn, String columns,
			TaxpayerGstin gstin, String monthYear, String sheetName, UserBean userBean,String delete) throws Exception {

		List<B2BTransactionEntity> b2bTransactionEntities = new ArrayList<>();
		List<B2CLTransactionEntity> b2cls = new ArrayList<>();
		List<B2CSTransactionEntity> b2csTransactionEntity = new ArrayList<>();
		List<EXPTransactionEntity> expTransactionEntities = new ArrayList<>();

		B2BTransaction b2bTransaction = new B2BTransaction();
		B2CLTransaction b2clTransaction = new B2CLTransaction();
		B2CSTransaction b2csTransaction = new B2CSTransaction();
		EXPTransaction expTransaction = new EXPTransaction();

		// set column mapping
		setColumnMapping(columns, gstReturn);

		// setting columns
		b2bTransaction.setColumnMapping(b2bColumns, gstReturn);
		b2clTransaction.setColumnMapping(b2clColumns, gstReturn);
		b2csTransaction.setColumnMapping(b2csColumns, gstReturn);
		expTransaction.setColumnMapping(expColumns, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin.getGstin()) ? "00" : gstin.getGstin().substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = SourceType.EXCEL;

		ExcelError ee = new ExcelError();

		int columnCount = 0;
		Integer index = 0;
		Iterator<Row> iterator = gstExcel.getSheet().iterator();
		while (iterator.hasNext()) {

			Row row = iterator.next();
			int rowNum = row.getRowNum();
			if (rowNum == 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
				List<String> temp = gstExcel.excelReadRow(cellRange);
				if(temp == null)
					break;

				boolean isGstin = false;
				ee.setDate(null);
				ee.setIgst(false);
				ee.setPos("");
				ee.setPosValue(-1);
				ee.setReverseCharge(false);

				try {
					if (!StringUtils.isEmpty(temp.get(_GSTIN_INDEX))) {
						isGstin = true;
					} else {
						isGstin = false;
					}
				} catch (NumberFormatException e) {
					isGstin = false;
				}

				// setting is igst transaction
				this.setIsIgst(sheetName, gstinPos, gstin.getGstin(), temp.get(_PLACE_OF_SUPPLY_INDEX), ee);
				boolean isB2Cs = true;
				index++;
				if (isGstin) {
					isB2Cs = false;
					B2BTransactionEntity b2bTransactionEntity = new B2BTransactionEntity();
					b2bTransactionEntity.setTaxpayerGstin(gstin);

					b2bTransactionEntity.setMonthYear(monthYear);
					// convert list to b2b object
					b2bTransaction.convertListToData(ee, temp, index, sheetName, sourceName, sourceId, gstReturn,
							gstinPos, gstin.getGstin(), b2bTransactionEntity, b2bTransactionEntities, userBean);
				} else {
                              if(temp.size() >= _TYP_INDEX)
                              {
						ExcelError expErr = new ExcelError();
						expErr = this.validateExportType(sheetName, _TYP_INDEX, index, temp.get(_TYP_INDEX), expErr);
						if (!expErr.isError()) {
							isB2Cs = false;
							expTransaction.convertListToTransactionData(ee, expTransactionEntities, temp, index,
									sheetName, sourceName, sourceId, gstReturn, gstinPos, gstin, monthYear, userBean);
						} else if (StringUtils.isNotEmpty(temp.get(_INVOICE_NO_INDEX)) && ee.isIgst()) {
							isB2Cs = false;
							b2clTransaction.convertListToData(ee, b2cls, temp, index, sheetName, sourceName, sourceId,
									gstReturn, gstinPos, gstin, userBean, monthYear);
						}
					}

					if (isB2Cs)
						b2csTransaction.convertListToData(ee, b2csTransactionEntity, temp, index, sheetName, sourceName,
								sourceId, gstReturn, gstinPos, gstin, monthYear, userBean);
				}
				isB2Cs = true;

			}

		}
		return this.saveCompositTransaction(b2bTransactionEntities, expTransactionEntities, b2csTransactionEntity, b2cls, ee,
				b2bTransaction, b2clTransaction, expTransaction, b2csTransaction, userBean, gstin.getGstin(), monthYear, ReturnType.GSTR1, delete);

	}


	/*
	 * public JSONObject processJsonData(String gstReturn,String gstin,String
	 * monthYear,String request) throws AppException {
	 * 
	 * Integer index = 0; String sheetName = "invoices"; List<B2B> b2bs = new
	 * ArrayList<>(); List<B2CL> b2cls = new ArrayList<>(); List<B2CS> b2css =
	 * new ArrayList<>();
	 * 
	 * B2BTransaction b2bTransaction = new B2BTransaction(); B2CLTransaction
	 * b2clTransaction = new B2CLTransaction(); B2CSTransaction b2csTransaction
	 * = new B2CSTransaction();
	 * 
	 * String columns = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
	 * // set column mapping setColumnMapping(columns, gstReturn); // setting
	 * columns B2BTransaction.setColumns(b2bColumns, gstReturn, b2bTransaction);
	 * B2CLTransaction.setColumns(b2clColumns, gstReturn, b2clTransaction);
	 * B2CSTransaction.setColumns(b2csColumns, gstReturn, b2csTransaction);
	 * 
	 * String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0,
	 * 2);
	 * 
	 * String sourceId = Utility.randomString(8); String sourceName =
	 * SourceType.EXCEL.toString();
	 * 
	 * ExcelError ee = new ExcelError();
	 * 
	 * 
	 * List<String> temp = new ArrayList<>(); JsonNode jsonNode; try { jsonNode
	 * = new ObjectMapper().readTree(request);
	 * 
	 * if (jsonNode.isArray()) {
	 * 
	 * ArrayNode arrayNode = (ArrayNode)jsonNode; for (JsonNode objNode :
	 * arrayNode) {	
	 * 
	 * Iterator<String> iterator=objNode.fieldNames();
	 * 
	 * while(iterator.hasNext()){ JsonNode val = objNode.get(iterator.next());
	 * temp.add(val.toString().replaceAll("\"", "")); }
	 * 
	 * boolean isGstin = false; ee.setDate(null); ee.setIgst(false);
	 * ee.setPos(""); ee.setPosValue(-1); ee.setReverseCharge(false);
	 * 
	 * try { if (!StringUtils.isEmpty(temp.get(_GSTIN_INDEX))) { isGstin = true;
	 * } else { isGstin = false; } } catch (NumberFormatException e) { isGstin =
	 * false; }
	 * 
	 * // setting is igst transaction this.setIsIgst(sheetName, gstinPos, gstin,
	 * temp.get(_PLACE_OF_SUPPLY_INDEX), ee);
	 * 
	 * index++; if (isGstin) { // convert list to b2b object
	 * B2BTransaction.convertListToData(ee, b2bs, temp, index, sheetName,
	 * sourceName, sourceId, gstReturn, gstinPos, gstin, b2bTransaction); } else
	 * { if (ee.isIgst()) { B2CLTransaction.convertListToData(ee, b2cls, temp,
	 * index, sheetName, sourceName, sourceId, gstReturn, gstinPos, gstinPos,
	 * b2clTransaction); } else { B2CSTransaction.convertListToData(ee, b2css,
	 * temp, index, sheetName, sourceName, sourceId, gstReturn, gstinPos,
	 * gstinPos, b2csTransaction); } }
	 * 
	 * } }
	 * 
	 * if (ee.isError()) { AppException exp = new AppException();
	 * exp.setCode(ExceptionCode._ERROR);
	 * exp.setMessage(ee.getErrorDesc().toString()); throw exp; }
	 * 
	 * // transfer b2cl to b2cs if invoice value is less than or equal to
	 * 250000.0 for (B2CL b2cl : b2cls) { List<Invoice> invoices =
	 * b2cl.getInvoices(); if (!Objects.isNull(invoices)) { for (Invoice invoice
	 * : invoices) { double taxableValueInv = 0.0; double taxAmountInv = 0.0;
	 * for (InvoiceItem it : invoice.getItems()) { taxableValueInv +=
	 * it.getTaxableValue(); if (!Objects.isNull(it.getTaxAmount())) {
	 * taxAmountInv += it.getTaxAmount(); } if
	 * (!Objects.isNull(it.getCessAmt())) { taxAmountInv += it.getCessAmt(); } }
	 * invoice.setTaxableValue(taxableValueInv);
	 * invoice.setTaxAmount(taxAmountInv);
	 * invoice.setSupplierInvVal(taxableValueInv + taxAmountInv); if
	 * (invoice.getSupplierInvVal() <= 250000.0) { for (InvoiceItem it :
	 * invoice.getItems()) { B2CS b2cs = new B2CS();
	 * b2cs.setSource(invoice.getSource());
	 * b2cs.setSourceId(invoice.getSourceId());
	 * b2cs.setStateCode(b2cl.getStateCode());
	 * b2cs.setEcomTin(invoice.getEcomTin());
	 * b2cs.setGoodsOrServiceCode(it.getGoodsOrServiceCode());
	 * b2cs.setHsnDesc(it.getHsnDesc()); b2cs.setUnit(it.getUnit());
	 * b2cs.setQuantity(it.getQuantity());
	 * b2cs.setTaxableValue(it.getTaxableValue());
	 * b2cs.setTaxRate(it.getTaxRate()); b2cs.setIgstAmt(it.getIgstAmt());
	 * b2cs.setCessAmt(it.getCessAmt()); if
	 * (!StringUtils.isEmpty(invoice.getEcomTin())) b2cs.setTyp("E"); else
	 * b2cs.setTyp("OE"); b2css = B2CSTransaction.addB2CSObject(b2cs, b2css); }
	 * } } } } b2cls.stream().forEach(b2cl -> b2cl.getInvoices().removeIf(i ->
	 * i.getSupplierInvVal() <= 250000.0));
	 * 
	 * JSONObject output = new JSONObject(); output.put("b2b",
	 * JsonMapper.objectMapper.writeValueAsString(b2bs)); output.put("b2cl",
	 * JsonMapper.objectMapper.writeValueAsString(b2cls)); output.put("b2cs",
	 * JsonMapper.objectMapper.writeValueAsString(b2css)); output.put("size",
	 * index);
	 * 
	 * return output;
	 * 
	 * } catch (JsonProcessingException e) { e.printStackTrace(); } catch
	 * (IOException e) { e.printStackTrace(); }
	 * 
	 * return null; }
	 */
	private void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			if (gstReturn.equalsIgnoreCase("gstr1")) {
				_GSTIN_INDEX = Integer.valueOf(headers[0]);
				_GSTIN_NAME_INDEX = Integer.valueOf(headers[1]);
				_INVOICE_NO_INDEX = Integer.valueOf(headers[2]);
				_INVOICE_DATE_INDEX = Integer.valueOf(headers[3]);
				_PLACE_OF_SUPPLY_INDEX = Integer.valueOf(headers[4]);
				_IS_REVERSE_INDEX = Integer.valueOf(headers[5]);
				_ETIN_INDEX = Integer.valueOf(headers[6]);
				_SNO_INDEX = Integer.valueOf(headers[7]);
				_HSN_CODE_INDEX = Integer.valueOf(headers[8]);
				_DESCRIPTION_INDEX = Integer.valueOf(headers[9]);
				_UNIT_INDEX = Integer.valueOf(headers[10]);
				_QUANTITY_INDEX = Integer.valueOf(headers[11]);
				_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[12]);
				_TAX_RATE_INDEX = Integer.valueOf(headers[13]);
				_IGST_AMT_INDEX = Integer.valueOf(headers[14]);
				_CGST_AMT_INDEX = Integer.valueOf(headers[15]);
				_SGST_AMT_INDEX = Integer.valueOf(headers[16]);
				_CESS_AMT_INDEX = Integer.valueOf(headers[17]);
				_MONTH_YEAR_INDEX = Integer.valueOf(headers[18]);
				_TYP_INDEX = Integer.valueOf(headers[19]);
				_INVOICE_TYPE_INDEX = Integer.valueOf(headers[20]);
				_SHIPPING_PORT_CODE = Integer.valueOf(headers[21]);
				_SHIPPING_BILL_NO_INDEX = Integer.valueOf(headers[22]);
				_SHIPPING_BILL_DATE_INDEX = Integer.valueOf(headers[23]);

				b2bColumns = _GSTIN_INDEX + "," + _GSTIN_NAME_INDEX + "," + _INVOICE_NO_INDEX + ","
						+ _INVOICE_DATE_INDEX + "," + _PLACE_OF_SUPPLY_INDEX + "," + _IS_REVERSE_INDEX + ","
						+ _ETIN_INDEX + "," + _SNO_INDEX + "," + _HSN_CODE_INDEX + "," + _DESCRIPTION_INDEX + ","
						+ _UNIT_INDEX + "," + _QUANTITY_INDEX + "," + _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + ","
						+ _IGST_AMT_INDEX + "," + _CGST_AMT_INDEX + "," + _SGST_AMT_INDEX + "," + _CESS_AMT_INDEX + ","
						+ _MONTH_YEAR_INDEX + "," + _INVOICE_TYPE_INDEX;

				b2clColumns = _PLACE_OF_SUPPLY_INDEX + "," + _INVOICE_NO_INDEX + "," + _INVOICE_DATE_INDEX + ","
						+ _ETIN_INDEX + "," + _SNO_INDEX + "," + _HSN_CODE_INDEX + "," + _DESCRIPTION_INDEX + ","
						+ _UNIT_INDEX + "," + _QUANTITY_INDEX + "," + _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + ","
						+ _IGST_AMT_INDEX + "," + _CESS_AMT_INDEX;

				b2csColumns = _PLACE_OF_SUPPLY_INDEX + "," + _ETIN_INDEX + "," + _SNO_INDEX + "," + _HSN_CODE_INDEX
						+ "," + _DESCRIPTION_INDEX + "," + _UNIT_INDEX + "," + _QUANTITY_INDEX + ","
						+ _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + "," + _IGST_AMT_INDEX + "," + _CGST_AMT_INDEX
						+ "," + _SGST_AMT_INDEX + "," + _CESS_AMT_INDEX + "," + _MONTH_YEAR_INDEX + "," + _TYP_INDEX;
				expColumns = _TYP_INDEX + "," + _INVOICE_NO_INDEX + "," + _INVOICE_DATE_INDEX + ","
						+ _SHIPPING_PORT_CODE + "," + _SHIPPING_BILL_NO_INDEX + "," + _SHIPPING_BILL_DATE_INDEX + ","
						+ _SNO_INDEX + "," + _HSN_CODE_INDEX + "," + _DESCRIPTION_INDEX + "," + _UNIT_INDEX + ","
						+ _QUANTITY_INDEX + "," + _TAXABLE_VALUE_INDEX + "," + _TAX_RATE_INDEX + "," + _IGST_AMT_INDEX;
			}
		}
	}

}
