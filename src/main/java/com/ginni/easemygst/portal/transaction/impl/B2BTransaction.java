package com.ginni.easemygst.portal.transaction.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.Invoice;
import com.ginni.easemygst.portal.data.InvoiceItem;
import com.ginni.easemygst.portal.data.ItcDetail;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.B2B;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.persistence.service.QueryParameter;
import com.ginni.easemygst.portal.transaction.crud.B2BServiceNewImpl;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class B2BTransaction extends TransactionUtil implements Transaction {
	
	private ReturnsService returnService;
	private int _SUPPLY_VALUE_INDEX;
	private int _INVOICE_NUMBER_INDEX;

	private CrudService crudService;
	private boolean isInvoice_type;
	private boolean isInvoice_number;
	private boolean isDate_index;
	private boolean isTaxable_index;
	private boolean isSupply_index;
	private boolean isInvoice_value;

	public enum B2BInvType {
		R, SEWP, SEWOP, DE,
	}

	int _GSTIN_INDEX = 0;
	int _GSTIN_NAME_INDEX = 1;
	int _INVOICE_NO_INDEX = 2;
	int _INVOICE_DATE_INDEX = 3;
	int _PLACE_OF_SUPPLY_INDEX = 4;
	int _IS_REVERSE_INDEX = 5;
	int _ETIN_INDEX = 6;
	int _INVOICE_TYPE_INDEX = 18;
	int _ORIGINAL_INVOICE_NUMBER = 19;
	int _ORIGINAL_INVOICE_DATE = 20;

	private final static Logger _LOGGER = LoggerFactory.getLogger(B2BTransaction.class);

	public B2BTransaction() {

		_SNO_INDEX = 7;
		_HSN_CODE_INDEX = 8;
		_DESCRIPTION_INDEX = 9;
		_UNIT_INDEX = 10;
		_QUANTITY_INDEX = 11;
		_TAXABLE_VALUE_INDEX = 12;
		_TAX_RATE_INDEX = 13;
		_IGST_AMT_INDEX = 14;
		_CGST_AMT_INDEX = 15;
		_SGST_AMT_INDEX = 16;
		_CESS_AMT_INDEX = 17;
		_INVOICE_TYPE_INDEX = 18;
		_ELIGIBILITY_TAX_INDEX = 18;
		_IGST_TAX_AVAIL_INDEX = 19;
		_CGST_TAX_AVAIL_INDEX = 20;
		_SGST_TAX_AVAIL_INDEX = 21;
		_CESS_TAX_AVAIL_INDEX = 22;
		_TEMP_TALLY_POS = 4;
		_ORIGINAL_INVOICE_NUMBER = 19;
		_ORIGINAL_INVOICE_DATE = 20;
		System.out.println("Inside B2B Constructor...");

	}
	List<String> temp=new ArrayList<String>();
	Item item = new Item();
	List<Item> items = new ArrayList<>();
	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();
		return uId;
	}
	
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, SourceType source, String delete)
			throws AppException {

		// List<B2B> b2bs = new ArrayList<>();

		List<B2BTransactionEntity> b2bTransactions = new ArrayList<>();

		// b2bTransaction.setCreationIpAddress("0000");

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		int columnCount = 0;
		Integer index = 0;
		Iterator<Row> iterator = gstExcel.getSheet().iterator();
		while (iterator.hasNext()) {

			Row row = iterator.next();
			int rowNum = row.getRowNum();
			if (rowNum == 0 || this.getTallyColumCountCondition(source, rowNum)) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
				List<String> temp = gstExcel.excelReadRow(cellRange);
				
				_LOGGER.debug("data list {}",temp);

			
				B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();

				b2bTransaction.setTaxpayerGstin(taxpayerGstin);

				b2bTransaction.setMonthYear(monthYear);
				// convert list to b2b object
				index++;
                   
				if (temp == null)
					break;
				if (source == SourceType.TALLY)
					temp = this.processTallyExcel(temp, gstinPos, sheetName, ee);
				
				convertListToB2bTransaction(ee, b2bTransaction, b2bTransactions, temp, index, sheetName, sourceName,
						sourceId, gstReturn, gstinPos, gstin, userBean);

			}

		}

		if (ee.isFinalError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		}

		// JSONObject output = new JSONObject();
		// output.put("data", JsonMapper.objectMapper.writeValueAsString(b2bs));
		// output.put("size", index);

		// return b2bTransactions;

		// variable which we going to get from process excel from frontEnd to find
		// delete

		try {
			int uploadingCount = 0;
			if(!CollectionUtils.isEmpty(b2bTransactions)){
			synchronized (b2bTransactions.get(0).getTaxpayerGstin().getGstin()) {
				uploadingCount = new B2BServiceNewImpl().insert(b2bTransactions, delete);
				// logging user action
				ApiLoggingService apiLoggingService = (ApiLoggingService) InitialContext
						.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
				MessageDto messageDto = new MessageDto();
				messageDto.setGstin(taxpayerGstin.getGstin());
				messageDto.setReturnType(gstReturn);
				messageDto.setMonthYear(monthYear);
				messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
				messageDto.setTransactionType(TransactionType.B2B.toString());
				if (!StringUtils.isEmpty(delete) && delete.equalsIgnoreCase("Yes"))
					apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,
							taxpayerGstin.getGstin());
				else
					apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto, taxpayerGstin.getGstin());
				// end logging user action
			}
			}
			return uploadingCount;
		} catch (AppException ae) {
			_LOGGER.trace("app exception ", ae);
			ae.setCode(ExceptionCode._ERROR);
			throw ae;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_LOGGER.error("sql exception ", e);
			throw new AppException(ExceptionCode._ERROR);
		}

		// } catch (JsonProcessingException e) {
		// throw new
		// AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		// }
	}

	private String validateInvoiceType(ExcelError ee, String sheetName, int _INVOICE_TYPE_INDEX, int index,
			String invoiceType) {
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}

	int retry = 0;
	private String cellRange;

	public int saveTransaction(List<B2BTransactionEntity> b2bTransactions, String delete) throws AppException {
		try {

			return new B2BServiceNewImpl().insert(b2bTransactions, delete);
		} catch (Exception e) {
				_LOGGER.info("exception from save transaction");
			if (e instanceof SQLException) {
				SQLException exception = (SQLException) e;

				if (exception.getErrorCode() == _DEADLOCK_ERR_CODE
						|| exception.getErrorCode() == _LOCK_WAIT_TIMEOUT_ERR_CODE) {
					while (retry++ < _MAX_RETRY) {
						try {
							Thread.sleep(_MAX_RETRY_WAIT_TIME);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch blockg
							e1.printStackTrace();
						}
						_Logger.info("Deadlock in {} while insertion for gstin {} return {} retry count {}",
								this.getClass().getName(), b2bTransactions.get(0).getTaxpayerGstin().getGstin(),
								b2bTransactions.get(0).getReturnType(), retry);

						return this.saveTransaction(b2bTransactions, delete);

					}
				}

			} else if (e instanceof AppException) {
				throw (AppException) e;
			}
			_LOGGER.error("sql exception ", e);
			throw new AppException(ExceptionCode._ERROR);
		}

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source)
			throws AppException {

		List<B2BTransactionEntity> b2bTransactions = new ArrayList<>();

		// b2bTransaction.setCreationIpAddress("0000");

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : gstin.substring(0, 2);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		Integer index = 0;

		CSVParser csvParser = new CSVParser();

		boolean skipHeader = false;
		while (scanner.hasNext()) {
			List<String> temp = csvParser.parseLine(scanner.nextLine());
			if (skipHeader)
				skipHeader = false;
			else {
				if (temp == null)
					break;
				B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();

				b2bTransaction.setTaxpayerGstin(taxpayerGstin);

				b2bTransaction.setMonthYear(monthYear);
				// convert list to b2b object
				index++;
				convertListToB2bTransaction(ee, b2bTransaction, b2bTransactions, temp, index, sheetName, sourceName,
						sourceId, gstReturn, gstinPos, gstin, userBean);
			}
		}
		scanner.close();

		if (ee.isError() || ee.isFinalError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		}

		return this.saveTransaction(b2bTransactions, delete);

		/*
		 * } catch (JsonProcessingException e) { throw new
		 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
		 */
	}

	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException {

		// List<UploadedCsv>lineItems=new ArrayList<>();
		List<B2BTransactionEntity> b2bTransactions = new ArrayList<>();

		// b2bTransaction.setCreationIpAddress("0000");

		String customHeaderIndex = "";
		if (gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
			customHeaderIndex = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18";
		else
			customHeaderIndex = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22";

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(customHeaderIndex, gstReturn);

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		Integer index = 0;

		for (UploadedCsv lineItem : lineItems) {
			/* List<String> lineItem = csvParser.parseLine(scanner.nextLine()); */

			B2BTransactionEntity b2bTransaction = new B2BTransactionEntity();

			b2bTransaction.setTaxpayerGstin(taxpayerGstin);

			b2bTransaction.setMonthYear(monthYear);
			// convert list to b2b object
			index = lineItem.getLineNumber();
			String gstinPos = gstin.substring(0, 2);

			convertListToB2bTransaction(ee, b2bTransaction, b2bTransactions,
					this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn), gstinPos), index, sheetName,
					sourceName, sourceId, gstReturn, gstinPos, gstin, userBean);
		}

		if (ee.isError() || ee.isFinalError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		}

		return this.saveTransaction(b2bTransactions, delete);

		/*
		 * } catch (JsonProcessingException e) { throw new
		 * AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST); }
		 */
	}

	private List<String> convertLineItemIntoList(UploadedCsv lineItem, ReturnType returnType, String pos) {

		List<String> fieldList = new ArrayList<>();
		if (ReturnType.GSTR1 == returnType) {
			fieldList.add(lineItem.getCpGstinNo());
			fieldList.add(lineItem.getCpName());

			fieldList.add(lineItem.getEntryNo());
			fieldList.add(lineItem.getEntryDate());
			fieldList.add(lineItem.getCpGstinStateCode());
			String isReverse = !StringUtils.isEmpty(lineItem.getGstApplicability())
					&& "REVERSE INVOICE".equalsIgnoreCase(lineItem.getGstApplicability()) ? "Y" : "N";
			fieldList.add(isReverse); // temporary done
			fieldList.add(lineItem.getEtin());
			fieldList.add("1");// for serial no
			fieldList.add(lineItem.getHsnSacCode());
			fieldList.add(lineItem.getHsnSacDesc());
			fieldList.add(lineItem.getUom());
			fieldList.add(lineItem.getQuantity());
			fieldList.add(lineItem.getTaxableAmount());
			fieldList.add(lineItem.getTotalTaxrate());
			fieldList.add(lineItem.getIgstAmount());
			fieldList.add(lineItem.getCgstAmount());
			fieldList.add(lineItem.getSgstAmount());
			fieldList.add(lineItem.getCessAmount());
			fieldList.add(lineItem.getEntryType());
		} else if (returnType == ReturnType.GSTR2) {
			fieldList.add(lineItem.getCpGstinNo());
			fieldList.add(lineItem.getCpName());

			fieldList.add(lineItem.getRefDocNo());
			fieldList.add(lineItem.getRefDocDate());
			fieldList.add(pos);
			String isReverse = !StringUtils.isEmpty(lineItem.getGstApplicability())
					&& "REVERSE INVOICE".equalsIgnoreCase(lineItem.getGstApplicability()) ? "Y" : "N";
			fieldList.add(isReverse); // temporary done
			// fieldList.add(lineItem.getEtin());
			fieldList.add("1");// for serial no
			fieldList.add(lineItem.getHsnSacCode());
			fieldList.add(lineItem.getHsnSacDesc());
			fieldList.add(lineItem.getUom());
			fieldList.add(lineItem.getQuantity());
			fieldList.add(lineItem.getTaxableAmount());
			fieldList.add(lineItem.getTotalTaxrate());
			fieldList.add(lineItem.getIgstAmount());
			fieldList.add(lineItem.getCgstAmount());
			fieldList.add(lineItem.getSgstAmount());
			fieldList.add(lineItem.getCessAmount());
			fieldList.add(lineItem.getEntryType());
			fieldList.add(lineItem.getInputEligibility());
			fieldList.add(lineItem.getInputIgstAmount());
			fieldList.add(lineItem.getInputCgstAmount());
			fieldList.add(lineItem.getInputSgstAmount());
			fieldList.add(lineItem.getInputCessAmount());
		}

		return fieldList;
	}

	public static void setColumns(String headerIndex, String gstReturn, B2BTransaction b2bTransaction) {
		b2bTransaction.setColumnMapping(headerIndex, gstReturn);
	}

	public void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			if (gstReturn.equalsIgnoreCase("gstr1")) {
				_GSTIN_INDEX = Integer.valueOf(headers[0]);
				_GSTIN_NAME_INDEX = Integer.valueOf(headers[1]);
				_INVOICE_NO_INDEX = Integer.valueOf(headers[2]);
				_INVOICE_DATE_INDEX = Integer.valueOf(headers[3]);
				_PLACE_OF_SUPPLY_INDEX = Integer.valueOf(headers[4]);
				_IS_REVERSE_INDEX = Integer.valueOf(headers[5]);
				_ETIN_INDEX = Integer.valueOf(headers[6]);
				_SNO_INDEX = Integer.valueOf(headers[7]);
				_HSN_CODE_INDEX = Integer.valueOf(headers[8]);
				_DESCRIPTION_INDEX = Integer.valueOf(headers[9]);
				_UNIT_INDEX = Integer.valueOf(headers[10]);
				_QUANTITY_INDEX = Integer.valueOf(headers[11]);
				_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[12]);
				_TAX_RATE_INDEX = Integer.valueOf(headers[13]);
				_IGST_AMT_INDEX = Integer.valueOf(headers[14]);
				_CGST_AMT_INDEX = Integer.valueOf(headers[15]);
				_SGST_AMT_INDEX = Integer.valueOf(headers[16]);
				_CESS_AMT_INDEX = Integer.valueOf(headers[17]);
				_INVOICE_TYPE_INDEX = Integer.valueOf(headers[18]);

			} else if (gstReturn.equalsIgnoreCase("gstr2")) {// difference
																// -gstr1 has
																// etin and
																// gstr2 dont
																// have etin
				_GSTIN_INDEX = Integer.valueOf(headers[0]);
				_GSTIN_NAME_INDEX = Integer.valueOf(headers[1]);
				_INVOICE_NO_INDEX = Integer.valueOf(headers[2]);
				_INVOICE_DATE_INDEX = Integer.valueOf(headers[3]);
				_PLACE_OF_SUPPLY_INDEX = Integer.valueOf(headers[4]);
				_IS_REVERSE_INDEX = Integer.valueOf(headers[5]);
				_SNO_INDEX = Integer.valueOf(headers[6]);
				_HSN_CODE_INDEX = Integer.valueOf(headers[7]);
				_DESCRIPTION_INDEX = Integer.valueOf(headers[8]);
				_UNIT_INDEX = Integer.valueOf(headers[9]);
				_QUANTITY_INDEX = Integer.valueOf(headers[10]);
				_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[11]);
				_TAX_RATE_INDEX = Integer.valueOf(headers[12]);
				_IGST_AMT_INDEX = Integer.valueOf(headers[13]);
				_CGST_AMT_INDEX = Integer.valueOf(headers[14]);
				_SGST_AMT_INDEX = Integer.valueOf(headers[15]);
				_CESS_AMT_INDEX = Integer.valueOf(headers[16]);
				_INVOICE_TYPE_INDEX = Integer.valueOf(headers[17]);
				_ELIGIBILITY_TAX_INDEX = Integer.valueOf(headers[18]);
				_IGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[19]);
				_CGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[20]);
				_SGST_TAX_AVAIL_INDEX = Integer.valueOf(headers[21]);
				_CESS_TAX_AVAIL_INDEX = Integer.valueOf(headers[22]);

			}

		} else if (gstReturn.equalsIgnoreCase("gstr2")) {
			_SNO_INDEX = 6;
			_HSN_CODE_INDEX = 7;
			_DESCRIPTION_INDEX = 8;
			_UNIT_INDEX = 9;
			_QUANTITY_INDEX = 10;
			_TAXABLE_VALUE_INDEX = 11;
			_TAX_RATE_INDEX = 12;
			_IGST_AMT_INDEX = 13;
			_CGST_AMT_INDEX = 14;
			_SGST_AMT_INDEX = 15;
			_CESS_AMT_INDEX = 16;
			_INVOICE_TYPE_INDEX = 17;
			_ELIGIBILITY_TAX_INDEX = 18;
			_IGST_TAX_AVAIL_INDEX = 19;
			_CGST_TAX_AVAIL_INDEX = 20;
			_SGST_TAX_AVAIL_INDEX = 21;
			_CESS_TAX_AVAIL_INDEX = 22;
		}

	}
	
	
	public void convertListToData(ExcelError ee, List<String> temp, Integer index, String sheetName,
			SourceType sourceName, String sourceId, String gstReturn, String gstinPos, String gstin,
			B2BTransactionEntity b2bTransaction, List<B2BTransactionEntity> b2bTransactionEntities, UserBean userBean) {
		convertListToB2bTransaction(ee, b2bTransaction, b2bTransactionEntities, temp, index, sheetName, sourceName,
				sourceId, gstReturn, gstinPos, gstin, userBean);
	}

	private void convertListToB2bTransaction(ExcelError ee, B2BTransactionEntity b2bTransaction,
			List<B2BTransactionEntity> b2bTransactionEntities, List<String> temp, Integer index, String sheetName,
			SourceType sourceName, String sourceId, String gstReturn, String gstinPos, String gstin,
			UserBean userBean) {
		
		handleAmmendmentCase(temp, TransactionType.B2B);
		ee.setNoValidationCheckForGinesys(false);
		b2bTransaction.setReturnType(gstReturn);

		ee.setUniqueRowValue(
				"CTIN - [" + temp.get(_GSTIN_INDEX) + "] InvoiceNo - [" + temp.get(_INVOICE_NO_INDEX) + "]");

		// setting gstins
		ee = this.validateGstin(sheetName, _GSTIN_INDEX, index, temp.get(_GSTIN_INDEX), ee);
		b2bTransaction.setCtin(StringUtils.upperCase(temp.get(_GSTIN_INDEX)));
		this.validateCtinAgainstGSTIN(sheetName, _GSTIN_INDEX, index, gstin, temp.get(_GSTIN_INDEX), ee);

		if (b2bTransactionEntities.contains(b2bTransaction))
			b2bTransaction = b2bTransactionEntities.get(b2bTransactionEntities.indexOf(b2bTransaction));
		else
			b2bTransactionEntities.add(b2bTransaction);

		if (userBean != null)
			InfoService.setInfo(b2bTransaction, userBean);

		B2BDetailEntity b2bDetail = new B2BDetailEntity();

		Item item = new Item();

		List<Item> items = new ArrayList<>();

		b2bDetail.setSource(sourceName.toString());
		b2bDetail.setSourceId(sourceId);

		// b2bDetail.setId(b2bDetailPK);

		// ObjectNode invoiceJson = _MAPPER.createObjectNode();

		if ("gstr1".equalsIgnoreCase(gstReturn)) {
			boolean isAmmendment = false;
			isAmmendment = this.isAmmendment(temp.get(_ORIGINAL_INVOICE_NUMBER), temp.get(_ORIGINAL_INVOICE_DATE), null,
					null, null, null, null);
			if (isAmmendment) {
				this.validateInvoiceNo(sheetName, _ORIGINAL_INVOICE_NUMBER, index, temp.get(_ORIGINAL_INVOICE_NUMBER),
						ee, true);

				b2bDetail.setOriginalInvoiceNumber(temp.get(_ORIGINAL_INVOICE_NUMBER));
				b2bDetail.setAmmendment(true);

				this.validateOriginalInvoiceDate(sheetName, _ORIGINAL_INVOICE_DATE, index,
						temp.get(_ORIGINAL_INVOICE_DATE), ee, b2bTransaction.getMonthYear(), TransactionType.B2B, true);
				b2bDetail.setAmmendment(true);

				if (!ee.isError()) {
					b2bDetail.setOriginalInvoiceDate(ee.getOriginalInvoiceDate());
				}

			}
		}

		if ("gstr1".equalsIgnoreCase(gstReturn)) {
			// setting etin
			ee = this.validateEtin(sheetName, _ETIN_INDEX, index, temp.get(_ETIN_INDEX), ee);
			if (!ee.isError())
				b2bDetail.setEtin(temp.get(_ETIN_INDEX));

			// setting reverse charge
			ee = this.validateReverseCharge(sheetName, _IS_REVERSE_INDEX, index, temp.get(_IS_REVERSE_INDEX), ee);
			if (!ee.isError())
				b2bDetail.setReverseCharge(ee.isReverseCharge());
			ee = this.validateRevAndEtin(sheetName, _ETIN_INDEX, index, b2bDetail.getReverseCharge(),
					temp.get(_ETIN_INDEX), ee);
		} else if ("gstr2".equalsIgnoreCase(gstReturn)) {
			// setting reverse charge
			ee = this.validateReverseCharge(sheetName, _IS_REVERSE_INDEX, index, temp.get(_IS_REVERSE_INDEX), ee);
			if (!ee.isError())
				b2bDetail.setReverseCharge(ee.isReverseCharge());
		}

		if (sourceName != SourceType.TALLY && !StringUtils.isEmpty(temp.get(_GSTIN_NAME_INDEX)))
			b2bTransaction.setCtinName(temp.get(_GSTIN_NAME_INDEX));
		// else
		// b2bDetail.setCtinName("");

		b2bDetail.setB2bTransaction(b2bTransaction);

		// setting invoice number
		ee = this.validateInvoiceNo(sheetName, _INVOICE_NO_INDEX, index, temp.get(_INVOICE_NO_INDEX), ee, true);
		b2bDetail.setInvoiceNumber(temp.get(_INVOICE_NO_INDEX));

		if (sourceName == SourceType.SAP)
			ee = this.validateStateCode(sheetName, _PLACE_OF_SUPPLY_INDEX, index, temp.get(_PLACE_OF_SUPPLY_INDEX), ee);
		else
			ee = this.validatePos(sheetName, _PLACE_OF_SUPPLY_INDEX, index, temp.get(_PLACE_OF_SUPPLY_INDEX), gstinPos,
					ee);
		if (!ee.isError())
			b2bDetail.setPos(ee.getPos());
		// following code is used for itc purpose
		ee.setGstinPosImps(gstinPos);
		ee.setPosImps(temp.get(_PLACE_OF_SUPPLY_INDEX));
		// end
		// setting is igst transaction
		if ("gstr1".equalsIgnoreCase(gstReturn)) {
			this.setIsIgst(sheetName, gstinPos, temp.get(_GSTIN_INDEX), ee.getPos(), ee);
		} else if ("gstr2".equalsIgnoreCase(gstReturn)) {
			if (!StringUtils.isEmpty(temp.get(_GSTIN_INDEX)) && temp.get(_GSTIN_INDEX).length() > 2) {
				gstinPos = temp.get(_GSTIN_INDEX).substring(0, 2);
				this.setIsIgst(sheetName, gstinPos, temp.get(_GSTIN_INDEX), ee.getPos(), ee);
			}
		}

		// b2b.setInvoices(invoices);

		// b2b.setInvoices(invoices);

		// setting invoice date
		this.validateInvoiceDate(sheetName, _INVOICE_DATE_INDEX, index, temp.get(_INVOICE_DATE_INDEX), ee,
				b2bTransaction.getMonthYear(), TransactionType.B2B);
		b2bDetail.setInvoiceDate(ee.getDate());

		// setting state code
		ee.setSewopOrWopay(false);

		if (!StringUtils.isEmpty(temp.get(_INVOICE_TYPE_INDEX))) {
			String invType = temp.get(_INVOICE_TYPE_INDEX);
			if (sourceName == SourceType.SAP && invType.equalsIgnoreCase("deemed"))
				invType = B2BInvType.DE.toString();
			else if (sourceName == SourceType.TALLY)
				invType = this.validateInvoiceType(ee, sheetName, _INVOICE_TYPE_INDEX, index, invType);
			else
				this.validateInvoiceType(sheetName, _INVOICE_TYPE_INDEX, index, invType, ee);
			b2bDetail.setInvoiceType(invType.toUpperCase());
			if (B2BInvType.SEWP.toString().equalsIgnoreCase(invType)
					|| B2BInvType.DE.toString().equalsIgnoreCase(invType)) {
				ee.setIgst(true);
			}
			if (B2BInvType.SEWOP.toString().equalsIgnoreCase(invType)) {
				ee.setSewopOrWopay(true);
				if (SourceType.TALLY == sourceName)
					this.noTaxTally(sheetName, invType, index, _INVOICE_DATE_INDEX, temp.get(_TAX_RATE_INDEX), ee,
							temp.get(_CESS_AMT_INDEX), _CESS_AMT_INDEX);
				else
					this.validateNoTaxAmtB2b(sheetName, invType, index, _IGST_AMT_INDEX, temp.get(_IGST_AMT_INDEX),
							_CGST_AMT_INDEX, temp.get(_CGST_AMT_INDEX), _SGST_AMT_INDEX, temp.get(_SGST_AMT_INDEX),
							_CESS_AMT_INDEX, temp.get(_CESS_AMT_INDEX), _TAX_RATE_INDEX, temp.get(_TAX_RATE_INDEX), ee);
			}
		} else {
			b2bDetail.setInvoiceType(InvoiceType.R.toString());
		}
		if (sourceName != SourceType.TALLY)
			this.setInvoiceItems(ee, sheetName, gstReturn, index, temp, item, TransactionType.B2B);
		else
			this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, gstin);
		
		if (b2bTransaction.getB2bDetails() == null) {
			b2bTransaction.setB2bDetails(new ArrayList<B2BDetailEntity>());
			b2bTransaction.getB2bDetails().add(b2bDetail);
			items.add(item);
			b2bDetail.setTaxableValue(item.getTaxableValue());
			b2bDetail.setTaxAmount(item.getTaxAmount());
			b2bDetail.setItems(items);

		} else if (b2bTransaction.getB2bDetails().contains(b2bDetail)) {
			b2bDetail = b2bTransaction.getB2bDetails().get(b2bTransaction.getB2bDetails().indexOf(b2bDetail));
			b2bDetail.setTaxableValue(b2bDetail.getTaxableValue() + item.getTaxableValue());
			b2bDetail.setTaxAmount(BigDecimal.valueOf(b2bDetail.getTaxAmount())
					.add(BigDecimal.valueOf(item.getTaxAmount())).doubleValue());
			b2bDetail.getItems().add(item);

		}

		else {
			b2bTransaction.getB2bDetails().add(b2bDetail);

			items.add(item);
			b2bDetail.setTaxableValue(item.getTaxableValue());
			b2bDetail.setTaxAmount(item.getTaxAmount());
			b2bDetail.setItems(items);
		}
		
		InfoService.setInfo(b2bDetail, userBean);

	}

	private void convertListToB2bTransactionCsv(ExcelError ee, B2BTransactionEntity b2bTransaction,
			List<B2BTransactionEntity> b2bTransactionEntities, UploadedCsv lineItem, Integer index, String sheetName,
			SourceType sourceName, String sourceId, String gstReturn, String gstinPos, String gstin,
			UserBean userBean) {

		ee.setNoValidationCheckForGinesys(false);
		b2bTransaction.setReturnType(gstReturn);

		ee.setUniqueRowValue("CTIN - [" + lineItem.getCpGstinNo() + "] InvoiceNo - [" + lineItem.getEntryNo() + "]");

		// setting gstins
		ee = this.validateGstin(sheetName, _GSTIN_INDEX, index, lineItem.getCpGstinNo(), ee);
		b2bTransaction.setCtin(StringUtils.upperCase(lineItem.getCpGstinNo()));

		if (b2bTransactionEntities.contains(b2bTransaction))
			b2bTransaction = b2bTransactionEntities.get(b2bTransactionEntities.indexOf(b2bTransaction));
		else
			b2bTransactionEntities.add(b2bTransaction);

		if (userBean != null)
			InfoService.setInfo(b2bTransaction, userBean);

		B2BDetailEntity b2bDetail = new B2BDetailEntity();

		Item item = new Item();

		List<Item> items = new ArrayList<>();

		b2bDetail.setSource(sourceName.toString());
		b2bDetail.setSourceId(sourceId);

		// b2bDetail.setId(b2bDetailPK);

		// ObjectNode invoiceJson = _MAPPER.createObjectNode();

		if ("gstr1".equalsIgnoreCase(gstReturn)) {
			// setting etin
			ee = this.validateEtin(sheetName, _ETIN_INDEX, index, lineItem.getEtin(), ee);
			if (!ee.isError())
				b2bDetail.setEtin(lineItem.getEtin());

			// setting reverse charge
			/*
			 * ee = this.validateReverseCharge(sheetName, _IS_REVERSE_INDEX, index,
			 * lineItem., ee); if (!ee.isError())
			 * b2bDetail.setReverseCharge(ee.isReverseCharge()); ee =
			 * this.validateRevAndEtin(sheetName, _ETIN_INDEX, index,
			 * b2bDetail.getReverseCharge(), lineItem.getEtin(), ee);
			 */
		} else if ("gstr2".equalsIgnoreCase(gstReturn)) {
			// setting reverse charge
			/*
			 * ee = this.validateReverseCharge(sheetName, _IS_REVERSE_INDEX, index,
			 * temp.get(_IS_REVERSE_INDEX), ee); if (!ee.isError())
			 * b2bDetail.setReverseCharge(ee.isReverseCharge());
			 */
		}

		if (sourceName != SourceType.TALLY && !StringUtils.isEmpty(lineItem.getCpName()))
			b2bTransaction.setCtinName(lineItem.getCpName());
		// else
		// b2bDetail.setCtinName("");

		b2bDetail.setB2bTransaction(b2bTransaction);

		// setting invoice number
		ee = this.validateInvoiceNo(sheetName, _INVOICE_NO_INDEX, index, lineItem.getEntryNo(), ee, true);
		b2bDetail.setInvoiceNumber(lineItem.getEntryNo());

		if (sourceName == SourceType.SAP)
			ee = this.validateStateCode(sheetName, _PLACE_OF_SUPPLY_INDEX, index, lineItem.getCpGstinStateCode(), ee);
		else
			ee = this.validatePos(sheetName, _PLACE_OF_SUPPLY_INDEX, index, lineItem.getCpGstinStateCode(), gstinPos,
					ee);
		if (!ee.isError())
			b2bDetail.setPos(ee.getPos());
		// following code is used for itc purpose
		ee.setGstinPosImps(gstinPos);
		ee.setPosImps(lineItem.getCpGstinStateCode());
		// end
		// setting is igst transaction
		if ("gstr1".equalsIgnoreCase(gstReturn)) {
			this.setIsIgst(sheetName, gstinPos, lineItem.getCpGstinNo(), ee.getPos(), ee);
		} else if ("gstr2".equalsIgnoreCase(gstReturn)) {
			if (!StringUtils.isEmpty(lineItem.getCpGstinNo()) && lineItem.getCpGstinNo().length() > 2) {
				gstinPos = lineItem.getCpGstinNo().substring(0, 2);
				this.setIsIgst(sheetName, gstinPos, lineItem.getCpGstinNo(), ee.getPos(), ee);
			}
		}

		// b2b.setInvoices(invoices);

		// b2b.setInvoices(invoices);

		// setting invoice date
		this.validateInvoiceDate(sheetName, _INVOICE_DATE_INDEX, index, lineItem.getEntryDate(), ee,
				b2bTransaction.getMonthYear(), TransactionType.B2B);
		b2bDetail.setInvoiceDate(ee.getDate());

		// setting state code
		if (!StringUtils.isEmpty(lineItem.getEntryType())) {
			String invType = lineItem.getEntryType();
			if (sourceName == SourceType.SAP && invType.equalsIgnoreCase("deemed"))
				invType = B2BInvType.DE.toString();
			else if (sourceName == SourceType.TALLY)
				invType = this.validateInvoiceType(ee, sheetName, _INVOICE_TYPE_INDEX, index, invType);
			else
				this.validateInvoiceType(sheetName, _INVOICE_TYPE_INDEX, index, invType, ee);
			b2bDetail.setInvoiceType(invType.toUpperCase());
			if (B2BInvType.SEWP.toString().equalsIgnoreCase(invType)
					|| B2BInvType.DE.toString().equalsIgnoreCase(invType)) {
				ee.setIgst(true);
			}
			if (B2BInvType.SEWOP.toString().equalsIgnoreCase(invType)) {
				ee.setSewopOrWopay(true);
				if (SourceType.TALLY == sourceName)
					this.noTaxTally(sheetName, invType, index, _INVOICE_DATE_INDEX, lineItem.getEntryDate(), ee,
							lineItem.getCessAmount(), _CESS_AMT_INDEX);
				else
					this.validateNoTaxAmtB2b(sheetName, invType, index, _IGST_AMT_INDEX, lineItem.getIgstAmount(),
							_CGST_AMT_INDEX, lineItem.getCgstAmount(), _SGST_AMT_INDEX, lineItem.getSgstAmount(),
							_CESS_AMT_INDEX, lineItem.getCessAmount(), _TAX_RATE_INDEX, lineItem.getTotalTaxrate(), ee);
			}
		} else {
			b2bDetail.setInvoiceType(InvoiceType.R.toString());
		}

		if (sourceName != SourceType.TALLY)
			this.setInvoiceItemsCsv(ee, sheetName, gstReturn, index, lineItem, item, TransactionType.B2B);
		/*
		 * else this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item,
		 * gstin);
		 */

		if (b2bTransaction.getB2bDetails() == null) {
			b2bTransaction.setB2bDetails(new ArrayList<B2BDetailEntity>());
			b2bTransaction.getB2bDetails().add(b2bDetail);
			items.add(item);
			b2bDetail.setTaxableValue(item.getTaxableValue());
			b2bDetail.setTaxAmount(item.getTaxAmount());
			b2bDetail.setItems(items);

		} else if (b2bTransaction.getB2bDetails().contains(b2bDetail)) {
			b2bDetail = b2bTransaction.getB2bDetails().get(b2bTransaction.getB2bDetails().indexOf(b2bDetail));
			b2bDetail.setTaxableValue(b2bDetail.getTaxableValue() + item.getTaxableValue());
			b2bDetail.setTaxAmount(BigDecimal.valueOf(b2bDetail.getTaxAmount())
					.add(BigDecimal.valueOf(item.getTaxAmount())).doubleValue());
			b2bDetail.getItems().add(item);

		}

		else {
			b2bTransaction.getB2bDetails().add(b2bDetail);

			items.add(item);
			b2bDetail.setTaxableValue(item.getTaxableValue());
			b2bDetail.setTaxAmount(item.getTaxAmount());
			b2bDetail.setItems(items);
		}
		InfoService.setInfo(b2bDetail, userBean);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {
		return null;
	}

	private void noTaxTally(String sheetName, String invType, int index, int _TAX_RATE_INDEX, String taxRate,
			ExcelError ee, String cessAmt, int _CESS_AMT_INDEX) {

		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();

		try {
			if (!StringUtils.isEmpty(taxRate) && Double.parseDouble(taxRate) != 0.0) {
				isError = true;
				error.append(sheetName + "," + (_SGST_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + taxRate
						+ "] - Remove tax rate as this is not required if invoice type is SEWOP|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_TAX_RATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + taxRate
					+ "] - Input 0 or leave the field blank as this is not required if invoice type is SEWOP|");
		}

		try {
			if (!StringUtils.isEmpty(cessAmt) && Double.parseDouble(cessAmt) != 0.0) {
				isError = true;
				error.append(sheetName + "," + (_CESS_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
						+ " Value - [" + cessAmt
						+ "] - Remove Cess amount as this is not required if invoice type is SEWOP|");
			}
		} catch (NumberFormatException e) {
			isError = true;
			error.append(sheetName + "," + (_CESS_AMT_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
					+ " Value - [" + cessAmt
					+ "] - Input 0 or leave the field blank as this is not required if invoice type is SEWOP|");
		}

	}

	private void setInvoiceFlag(Invoice invoice) {
		if (!Objects.isNull(invoice.getTaxPayerAction())) {
			if (invoice.getTaxPayerAction() == TaxpayerAction.ACCEPT)
				invoice.setFlags(TableFlags.ACCEPTED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.REJECT)
				invoice.setFlags(TableFlags.REJECTED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.PENDING)
				invoice.setFlags(TableFlags.PENDING.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.MODIFY)
				invoice.setFlags(TableFlags.MODIFIED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.DELETE)
				invoice.setFlags(TableFlags.DELETED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.UPLOADED)
				invoice.setFlags(TableFlags.UPLOADED.toString());
			else if (invoice.getTaxPayerAction() == TaxpayerAction.DRAFT)
				invoice.setFlags(TableFlags.DRAFT.toString());
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		return null;
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.ALL, returnType);

	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {

			List<B2BDetailEntity> b2bDatas = datas;
			for (B2BDetailEntity invoice : b2bDatas) {

				for (Item item : invoice.getItems()) {
					List<String> columns = this.getFlatData(invoice, item, returnType);
					tabularData.add(columns);
				}
			}

		} catch (Exception e) {
			_LOGGER.debug("exception in get data by type ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;

	}

	private List<String> getFlatData(B2BDetailEntity invoice, Item item, String returnType) {

		List<String> columns = new ArrayList<>();

		columns.add(this.getStringValue(invoice.getB2bTransaction().getCtin()));
		columns.add(this.getStringValue(invoice.getB2bTransaction().getCtinName()));
		columns.add(this.getStringValue(invoice.getInvoiceNumber()));
		columns.add(this.getStringValue(invoice.getInvoiceDate()));
		columns.add(this.getStringValue(invoice.getPos()));
		columns.add(this.getStringValue(invoice.getRevCharge()));

		if (ReturnType.GSTR1.toString().equalsIgnoreCase(returnType))
			columns.add(this.getStringValue(invoice.getEtin()));// gstr1

		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxableValue()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCgst()));
		columns.add(this.getStringValue(item.getSgst()));
		columns.add(this.getStringValue(item.getCess()));
		columns.add(this.getStringValue(invoice.getInvoiceType()));
		if (ReturnType.GSTR2.toString().equalsIgnoreCase(returnType)) {
			columns.add(this.getStringValue(item.getTotalEligibleTax()));
			columns.add(this.getStringValue(item.getItcIgst()));
			columns.add(this.getStringValue(item.getItcCgst()));
			columns.add(this.getStringValue(item.getItcSgst()));
			columns.add(this.getStringValue(item.getItcCess()));
		}
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));

		return columns;

	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		return null;
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		return null;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {

		List<B2B> b2bs = new ArrayList<>();

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.GINESYS.toString();

		try {
			JsonNode objects = JsonMapper.objectMapper.readTree(data);

			for (JsonNode jsonNode : objects) {

				B2B b2b = new B2B();

				Invoice invoice = new Invoice();
				InvoiceItem invoiceItem = new InvoiceItem();
				List<Invoice> invoices = new ArrayList<>();
				List<InvoiceItem> items = new ArrayList<>();

				// set source
				invoice.setSource(sourceName);
				invoice.setSourceId(sourceId);

				if (jsonNode.has("ctin"))
					b2b.setGstin(jsonNode.get("ctin").asText());

				if (jsonNode.has("ctin_name"))
					b2b.setGstinName(jsonNode.get("ctin_name").asText());

				b2b.setInvoices(invoices);
				items.add(invoiceItem);
				invoices.add(invoice);
				b2b.setInvoices(invoices);
				invoice.setItems(items);

				// setting invoice number
				if (jsonNode.has("invoiceNo"))
					invoice.setSupplierInvNum(jsonNode.get("invoiceNo").asText());

				// setting invoice date
				try {
					if (jsonNode.has("invoiceDate"))
						invoice.setSupplierInvDt(sdfGcc.parse(jsonNode.get("invoiceDate").asText()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				// setting supplier invoice value
				if (jsonNode.has("invoiceValue"))
					invoice.setSupplierInvVal(Double.parseDouble(jsonNode.get("invoiceValue").asText()));

				// setting place of supply
				if (jsonNode.has("pos")) {
					int pos = Double.valueOf(jsonNode.get("pos").asText()).intValue();
					invoice.setPos(pos + "");
				}

				// setting reverse charge
				/*
				 * if (jsonNode.has("reverse") & jsonNode.has("reversePer")) { if
				 * (jsonNode.get("reverse").asText().equalsIgnoreCase("Y") ||
				 * jsonNode.get("reverse").asText().equalsIgnoreCase("YES")) {
				 * invoice.setReverseCharge(true); int i =
				 * Double.valueOf(jsonNode.get("reversePer").asText()).intValue( );
				 * invoice.setReverseChargePercentage(Double.valueOf(i).intValue ()); } else if
				 * (jsonNode.get("reverse").asText().equalsIgnoreCase("N") ||
				 * jsonNode.get("reverse").asText().equalsIgnoreCase("NO")) {
				 * invoice.setReverseCharge(false); invoice.setReverseChargePercentage(0); } }
				 */

				// setting invoice item no
				if (jsonNode.has("sNo")) {
					int sno = Double.valueOf(jsonNode.get("sNo").asText()).intValue();
					invoiceItem.setSNo(sno);
				}

				// setting goods and services code
				if (jsonNode.has("hsnsac"))
					invoiceItem.setGoodsOrServiceCode(jsonNode.get("hsnsac").asText());

				// setting taxable value
				if (jsonNode.has("taxableValue"))
					invoiceItem.setTaxableValue(Double.parseDouble(jsonNode.get("taxableValue").asText()));
				if (jsonNode.has("igstr"))
					invoiceItem.setTaxRate(Double.parseDouble(jsonNode.get("igstr").asText()));
				if (jsonNode.has("igsta"))
					invoiceItem.setIgstAmt(Double.parseDouble(jsonNode.get("igsta").asText()));
				if (jsonNode.has("cgsta"))
					invoiceItem.setCgstAmt(Double.parseDouble(jsonNode.get("cgsta").asText()));
				if (jsonNode.has("sgsta"))
					invoiceItem.setSgstAmt(Double.parseDouble(jsonNode.get("sgsta").asText()));

				// setting itc details
				ItcDetail itcDetails = new ItcDetail();
				invoiceItem.setItcDetails(itcDetails);

				// following serial no can be changed
				if (jsonNode.has("eitc"))
					invoiceItem.setEligOfTotalTax(jsonNode.get("eitc").asText());
				if (jsonNode.has("ttaigst"))
					itcDetails.setTotalTaxAvalIgst(Double.parseDouble(jsonNode.get("ttaigst").asText()));
				if (jsonNode.has("ttacgst"))
					itcDetails.setTotalTaxAvalCgst(Double.parseDouble(jsonNode.get("ttacgst").asText()));
				if (jsonNode.has("ttasgst"))
					itcDetails.setTotalTaxAvalSgst(Double.parseDouble(jsonNode.get("ttasgst").asText()));

				// addB2BObject(b2b, b2bs);
			}

			return JsonMapper.objectMapper.writeValueAsString(b2bs);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public EmailDTO getEmailContent(EmailDTO emailDTO, Object primary, Object secondary) throws AppException {

		B2BDetailEntity b2bPrimary = (B2BDetailEntity) primary;
		B2BDetailEntity b2bSecondary = (B2BDetailEntity) secondary;

		try {

			String body = emailDTO.getMailbody();

			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");

			body = body.replace("[PRIMARY_COMPANY_NAME]",
					b2bPrimary.getB2bTransaction().getTaxpayerGstin().getTaxpayer().getLegalName());
			body = body.replace("[PRIMARY_GSTIN]", b2bPrimary.getB2bTransaction().getTaxpayerGstin().getGstin());
			body = body.replace("[PRIMARY_INVOICE_NO]", b2bPrimary.getInvoiceNumber());
			body = body.replace("[PRIMARY_INVOICE_DATE]", sdf.format(b2bPrimary.getInvoiceDate()));
			body = body.replace("[PRIMARY_INVOICE_VALUE]",
					b2bPrimary.getTaxableValue() + b2bPrimary.getTaxAmount() + "");
			body = body.replace("[PRIMARY_TAXABLE_VALUE]", b2bPrimary.getTaxableValue() + "");
			body = body.replace("[PRIMARY_TAX_AMOUNT]", b2bPrimary.getTaxAmount() + "");
			body = body.replace("[PRIMARY_TAXABLE_VALUE]", b2bPrimary.getTaxableValue() + "");
			String primaryInvoiceData = sdf.format(b2bPrimary.getInvoiceDate());
			String primaryInvoiceValue = String.valueOf(b2bPrimary.getTaxableValue() + b2bPrimary.getTaxAmount());
			String primaryTaxAmount = String.valueOf(b2bPrimary.getTaxAmount());
			ReconsileType reconsileType = ReconsileType.valueOf(StringUtils.upperCase(b2bPrimary.getType()));

			if (ReconsileType.MISMATCH.equals(reconsileType) || ReconsileType.MISMATCH_INVOICENO == reconsileType
					|| ReconsileType.MISMATCH_DATE == reconsileType
					|| ReconsileType.MISMATCH_ROUNDOFF_TAXABLEVALUE == reconsileType
					|| ReconsileType.MISMATCH_ROUNDOFF_TAXAMOUNT == reconsileType
					|| ReconsileType.MISMATCH_MAJOR == reconsileType) {

				body = body.replace("[SECONDARY_COMPANY_NAME]",
						Objects.nonNull(b2bSecondary.getB2bTransaction().getCtinName())
								? b2bSecondary.getB2bTransaction().getCtinName()
								: "");
				body = body.replace("[SECONDARY_GSTIN]", b2bSecondary.getB2bTransaction().getCtin());
				body = body.replace("[SECONDARY_INVOICE_NO]", b2bSecondary.getInvoiceNumber());
				body = body.replace("[SECONDARY_INVOICE_DATE]", sdf.format(b2bSecondary.getInvoiceDate()));
				body = body.replace("[SECONDARY_INVOICE_VALUE]",
						b2bSecondary.getTaxableValue() + b2bSecondary.getTaxAmount() + "");
				body = body.replace("[SECONDARY_TAXABLE_VALUE]", b2bSecondary.getTaxableValue() + "");
				body = body.replace("[SECONDARY_TAX_AMOUNT]", b2bSecondary.getTaxAmount() + "");
				body = body.replace("[SECONDARY_TAXABLE_VALUE]", b2bSecondary.getTaxableValue() + "");

				String secondaryInvoiceData = sdf.format(b2bSecondary.getInvoiceDate());
				String secondaryInvoiceValue = String
						.valueOf(b2bSecondary.getTaxableValue() + b2bSecondary.getTaxAmount());
				String secondaryTaxAmount = String.valueOf(b2bSecondary.getTaxAmount());

				if (primaryInvoiceData.equalsIgnoreCase(secondaryInvoiceData)) {
					body = body.replace("[colorDate]", "black");
				} else {
					body = body.replace("[colorDate]", "red");
				}

				if (primaryInvoiceValue.equalsIgnoreCase(secondaryInvoiceValue)) {
					body = body.replace("[colorValue]", "black");
				} else {
					body = body.replace("[colorValue]", "red");
				}

				if (primaryTaxAmount.equalsIgnoreCase(secondaryTaxAmount)) {
					body = body.replace("[colorAmount]", "black");
				} else {
					body = body.replace("[colorAmount]", "red");
				}

			}
			emailDTO.setRemarks("");
			emailDTO.setMailbody(body);

			return emailDTO;

		} catch (Exception e) {
			_LOGGER.error("exception email content ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {

		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getB2b();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {

		Map<String, List<String>> sourceB2bs = getData(source, returnType);

		Map<String, List<String>> changedB2bs = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionB2bs = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceB2bs) && !Objects.isNull(changedB2bs)) {

			Iterator it = sourceB2bs.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedB2bs.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();
					Object temp2 = changedB2bs.get(pair.getKey());
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedB2bs.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceB2bs.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionB2bs.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionB2bs.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionB2bs.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionB2bs;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		return null;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		return null;
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		return null;
	}

	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {

		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return false;
			} else {
				gstinTransactionDTO
						.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "b2bs"));

				Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
						Gstr1Dto.class);
				;

				List<B2BTransactionEntity> transactionb2bs = gstr.getB2bs();
				for (B2BTransactionEntity b2bTran : transactionb2bs) {
					double taxableValue = 0.0;
					double taxAmount = 0.0;
					b2bTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					b2bTran.setReturnType(gstinTransactionDTO.getReturnType());

					b2bTran.setTaxpayerGstin((TaxpayerGstin) EntityHelper
							.convert(gstinTransactionDTO.getTaxpayerGstin(), TaxpayerGstin.class));
					B2BDetailEntity inv = b2bTran.getB2bDetails().get(0);
					inv.setB2bTransaction(b2bTran);
					ExcelError ee = new ExcelError();
					for (Item item : inv.getItems()) {
						taxableValue += item.getTaxableValue();
						taxAmount += item.getTaxAmount();
						item.setInvoiceId(inv.getId());
						/*
						 * if(ReturnType.GSTR2.toString().equalsIgnoreCase(gstinTransactionDTO.
						 * getReturnType())){ ee= new B2BTransaction().validateItcTaxAmt("transaction",
						 * 1, 1, String.valueOf(item.getIgst()), item.getTotalEligibleTax(),
						 * String.valueOf(item.getItcIgst()), ee); if(ee.isError()||ee.isFinalError()){
						 * AppException ae=new AppException();
						 * ae.setMessage(ee.getErrorDesc().toString()); throw ae; } }
						 */
					}
					inv.setTaxableValue(taxableValue);
					inv.setTaxAmount(taxAmount);
					if (StringUtils.isEmpty(inv.getId())) {// create invoice
						List<B2BTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
								"B2bTransaction.getByGstinMonthyearReturntypeCtin",
								QueryParameter.with("taxPayerGstin", b2bTran.getTaxpayerGstin())
										.and("monthYear", gstinTransactionDTO.getMonthYear())
										.and("returnType", b2bTran.getReturnType()).and("ctin", b2bTran.getCtin())
										.parameters());
						if (b2bSearch.isEmpty()) {
							/*
							 * inv.setB2bTransaction(b2bTran); crudService.create(b2bTran);
							 */// use kaustub method here
							InfoService.setInfo(b2bTran, gstinTransactionDTO.getUserBean());
							InfoService.setInfo(inv, gstinTransactionDTO.getUserBean());
							try {

								new B2BServiceNewImpl().insert(transactionb2bs, "no");
							} catch (SQLException e) {
								Log.error("exception in saving b2b transaction from portal");
								e.printStackTrace();
							}
						} else {
							B2BTransactionEntity existingB2b = b2bSearch
									.get(0);/* incase ctin name is different in new invoice */
							existingB2b.setCtinName(b2bTran.getCtinName());
							inv.setB2bTransaction(existingB2b);
							// crudService.create(inv);
							crudService.update(existingB2b);
							InfoService.setInfo(existingB2b, gstinTransactionDTO.getUserBean());
							InfoService.setInfo(inv, gstinTransactionDTO.getUserBean());
							try {

								new B2BServiceNewImpl().insert(transactionb2bs, "no");
							} catch (SQLException e) {
								Log.error("exception in saving b2b transaction from portal");
								e.printStackTrace();
							}

							// crudService.create(inv);
						}
						// add new invoice
					} else {

						List<B2BTransactionEntity> b2bSearch = crudService.findWithNamedQuery(
								"B2bTransaction.getByGstinMonthyearReturntypeCtin",
								QueryParameter.with("taxPayerGstin", b2bTran.getTaxpayerGstin())
										.and("monthYear", gstinTransactionDTO.getMonthYear())
										.and("returnType", b2bTran.getReturnType()).and("ctin", b2bTran.getCtin())
										.parameters());
						InfoService.setInfo(b2bTran, gstinTransactionDTO.getUserBean());

						InfoService.setInfo(b2bTran, gstinTransactionDTO.getUserBean());

						B2BDetailEntity existingInv = crudService.find(B2BDetailEntity.class, inv.getId());
						this.checkIsEditible(existingInv, inv);
						existingInv.setInvoiceNumber(inv.getInvoiceNumber());
						existingInv.setInvoiceDate(inv.getInvoiceDate());
						existingInv.setTaxableValue(inv.getTaxableValue());
						existingInv.setEtin(inv.getEtin());
						existingInv.setInvoiceType(inv.getInvoiceType());
						existingInv.setPos(inv.getPos());
						existingInv.setReverseCharge(inv.getReverseCharge());
						existingInv.setTaxAmount(inv.getTaxAmount());
						existingInv.setIsError(false);
						existingInv.setErrMsg("");
						existingInv.setFlags("");
						existingInv.setOriginalInvoiceDate(inv.getOriginalInvoiceDate());
						existingInv.setOriginalInvoiceNumber(inv.getOriginalInvoiceNumber());
						existingInv.setIsAmmendment(inv.getIsAmmendment());

						if (b2bSearch.isEmpty()) {
							inv.setB2bTransaction(b2bTran);
							existingInv.setB2bTransaction(b2bTran);
							b2bTran.setB2bDetails(new ArrayList<>());
							crudService.update(b2bTran);
							returnService.deleteItemsByInvoiceId(existingInv.getId());
							for (Item itm : inv.getItems()) {
								itm.setInvoiceId(existingInv.getId());
								crudService.create(itm);
							}

						} else {
							B2BTransactionEntity existingB2b = b2bSearch.get(0);
							new InfoService().setInfo(existingB2b, gstinTransactionDTO.getUserBean());
							String ctinName = b2bTran.getCtinName();
							existingB2b.setCtinName(ctinName);
							inv.setB2bTransaction(existingB2b);
							existingInv.setB2bTransaction(existingB2b);

							crudService.update(existingInv);
							crudService.update(existingB2b);
							returnService.deleteItemsByInvoiceId(existingInv.getId());

							for (Item itm : inv.getItems()) {
								itm.setInvoiceId(existingInv.getId());
								crudService.create(itm);
							}

						}

					}

				}

				return true;
			}
		} catch (AppException ae) {

			throw ae;
		} catch (Exception e) {

			_Logger.error("exception in hsn method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private boolean checkIsEditible(B2BDetailEntity existingInvoice, B2BDetailEntity editedInvoice)
			throws AppException {
		if (existingInvoice.getIsTransit()) {
			AppException ae = new AppException();
			ae.setMessage("invoice is in transit state so can't be updated for the moment");
			_Logger.error("exception--invoice is in transit state so can't be updated for the moment ");
			throw ae;
		}
		if (existingInvoice.getIsSynced()
				&& (!existingInvoice.getInvoiceNumber().equalsIgnoreCase(editedInvoice.getInvoiceNumber())
						|| !existingInvoice.getB2bTransaction().getCtin()
								.equalsIgnoreCase(editedInvoice.getB2bTransaction().getCtin())
						|| existingInvoice.getInvoiceDate().compareTo(editedInvoice.getInvoiceDate()) != 0)) {

			AppException ae = new AppException();
			ae.setMessage(
					"invoice has been synced with GSTN so ctin,invoice number and invoice date can't be updated for the moment");
			_Logger.error(
					"invoice has been synced with GSTN so ctin,invoice number and invoice date can't be updated for the moment ");
			throw ae;
		}

		return true;
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String supplier, String receiver, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int processList(GstExcel gstExcel,String transaction,String gstReturn, TaxpayerGstin taxpayerGstin, String sheetName,
			String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		List<B2BTransactionEntity> b2bTransactions=new ArrayList<>();
		B2BTransactionEntity b2bTransaction=new B2BTransactionEntity();
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		
		ExcelError ee = new ExcelError();
		
		String delete="no";

//		gstExcel.loadSheet(0);
//        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				
				if(temp!=null) {
//					System.out.println(temp);
//					_LOGGER.info(temp.toString());
					if(ReturnType.GSTR1.toString().equalsIgnoreCase(gstReturn)) {
						System.out.println("Inside b2b gstr1");
						System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(date+" ");
//		    					_LOGGER.info(date.toString());
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_number+" ");
//	    						_LOGGER.info(invoice_number);
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_value+" ");
//	    						_LOGGER.info(Double.toString(invoice_value));
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(invoiceType+" ");
//		    					_LOGGER.info(invoiceType);
		    					
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(taxable_value+" ");
//		    					_LOGGER.info(Double.toString(taxable_value));
		    					
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
    							System.out.println(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
//    							_LOGGER.info(b2bDetail.getPos());
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
				}else if(ReturnType.GSTR2.toString().equalsIgnoreCase(gstReturn)) {
					System.out.println("Inside b2b gstr2");
					System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(1), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(1).equals("3. Suppl-regd per - B2B")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(date+" ");
//		    					_LOGGER.info(date.toString());
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(1).equals("3. Suppl-regd per - B2B")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_number+" ");
//	    						_LOGGER.info(invoice_number);
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(1).equals("3. Suppl-regd per - B2B")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_value+" ");
//	    						_LOGGER.info(Double.toString(invoice_value));
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(1).equals("3. Suppl-regd per - B2B")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(invoiceType+" ");
//		    					_LOGGER.info(invoiceType);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(1).equals("3. Suppl-regd per - B2B")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(taxable_value+" ");
//		    					_LOGGER.info(Double.toString(taxable_value));
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(1).equals("3. Suppl-regd per - B2B")) {
//    							_LOGGER.info(place_of_supply);
    							System.out.println(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
//    							_LOGGER.info(b2bDetail.getPos());
//    							_LOGGER.info(place_of_supply);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
				}
//					_LOGGER.info(b2bTransaction.getB2bDetails().toString());
			}
			}
		}
		this.saveTransaction(b2bTransactions, delete);
		return 0;
	}
}
