package com.ginni.easemygst.portal.transaction.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ConsolidatedCsvProcessorManager;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.TaxpayerService;
import com.ginni.easemygst.portal.database.DBUtils;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.persistence.entity.DataUploadInfo;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.factory.TransactionFactory;

@Stateless
/*
 * @Named("consolidated")
 * 
 * @RequestScoped
 */
public class ConsoldateCsvProcessor implements ConsolidatedCsvProcessorManager {

	private static final long serialVersionUID = 1L;

	Logger log = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext(unitName = "easemygst")
	private EntityManager em;

	@Inject
	private UserBean userBean;

	@Inject
	private TaxpayerService taxPayerService;

	@Inject
	private CrudService crudService;

	public enum ConsolidatedQuery {
		GSTR1_B2B("GSTR1_B2B",
				"SELECT GST_APPLICABILITY as gstApplicability,tradeType , CP_GSTIN_NO as cpGstinNo,CP_NAME as cpName,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,CP_GSTIN_STATE_CODE as cpGstinStateCode ,ETIN as etin,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,TRADETYPE as tradeType,lineNumber FROM uploaded_csv "
						+ " WHERE returnType='GSTR1' and type='B2B' and uploadDataInfoId=[uploadDataInfoId]"),

		GSTR1_B2CL("GSTR1_B2CL",
				"SELECT lineNumber,GST_APPLICABILITY as gstApplicability,tradeType,CP_GSTIN_STATE_CODE as cpGstinStateCode ,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,ETIN as etin,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CESS_AMOUNT as cessAmount FROM uploaded_csv"
						+ "  WHERE returnType='GSTR1' and type='B2CL' and uploadDataInfoId=[uploadDataInfoId]  "),

		GSTR1_EXP("GSTR1_EXP",
				"SELECT lineNumber,tradeType as tradtype,GST_APPLICABILITY as gstApplicability,ENTRY_NO as entryNo,NET_AMOUNT as netAmount,ENTRY_DATE as entryDate,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CESS_AMOUNT as cessAmount FROM uploaded_csv"
						+ "  WHERE returnType='GSTR1' and type='EXP'  and uploadDataInfoId=[uploadDataInfoId]  "),

		GSTR1_B2CS("GSTR1_B2CS",
				"SELECT lineNumber,GST_APPLICABILITY as gstApplicability,tradeType,CP_GSTIN_STATE_CODE as cpGstinStateCode,ETIN as etin,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount FROM uploaded_csv"
						+ "  WHERE returnType='GSTR1' and type='B2CS' and uploadDataInfoId=[uploadDataInfoId]"),

		GSTR1_CDN("GSTR1_CDN",
				"SELECT lineNumber,GST_APPLICABILITY as gstApplicability,tradeType,CP_GSTIN_NO as cpGstinNo,CP_NAME as cpName,ORIGINAL_INV_ENT_NO as originalInvEntNo,ORIGINAL_INV_ENT_DT as originalInvEntDt,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,ENTRY_TYPE as entryType,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,DNCN_REASON as dncnReason FROM uploaded_csv "
						+ "  WHERE returnType='GSTR1' and type='CDN' and uploadDataInfoId=[uploadDataInfoId]"),

		GSTR1_CDNUR("GSTR1_CDNUR",
				"SELECT lineNumber,invoiceType,GST_APPLICABILITY as gstApplicability,tradeType,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,CP_GSTIN_NO as cpGstinNo,CP_NAME as cpName,ORIGINAL_INV_ENT_NO as originalInvEntNo,ORIGINAL_INV_ENT_DT as originalInvEntDt,REF_DOC_NO as refDocNo,REF_DOC_DATE as refDocDate,ENTRY_TYPE as entryType,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,DNCN_REASON as dncnReason FROM uploaded_csv "
						+ "  WHERE returnType='GSTR1' and type='CDNUR' and uploadDataInfoId=[uploadDataInfoId]"),

		GSTR1_NIL("GSTR1_NIL",
				"select lineNumber,subType,NET_AMOUNT as netAmount from uploaded_csv where type='NIL' and returnType='GSTR1' and  uploadDataInfoId=[uploadDataInfoId]"		),

		GSTR2_B2B("GSTR2_B2B",
				"SELECT REF_DOC_NO as refDocNo,REF_DOC_DATE as refDocDate,lineNumber,GST_APPLICABILITY as gstApplicability,tradeType,CP_GSTIN_NO as cpGstinNo,CP_NAME as cpName,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,CP_GSTIN_STATE_CODE as cpGstinStateCode ,ETIN as etin,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,TRADETYPE as tradeType,INPUT_ELIGIBILITY as inputEligibility,INPUT_IGST_AMOUNT as inputIgstAmount,INPUT_CGST_AMOUNT as inputCgstAmount,INPUT_SGST_AMOUNT as inputSgstAmount,INPUT_CESS_AMOUNT as inputCessAmount  FROM uploaded_csv "
						+ "  WHERE returnType='GSTR2' and type='B2B' and uploadDataInfoId=[uploadDataInfoId]"),

		GSTR2_B2BUR("GSTR2_B2BUR",
				"SELECT lineNumber,GST_APPLICABILITY as gstApplicability,tradeType, CP_GSTIN_STATE_CODE as cpGstinStateCode ,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,ETIN as etin,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,INPUT_ELIGIBILITY as inputEligibility,INPUT_IGST_AMOUNT as inputIgstAmount,INPUT_CGST_AMOUNT as inputCgstAmount,INPUT_SGST_AMOUNT as inputSgstAmount,INPUT_CESS_AMOUNT as inputCessAmount  FROM uploaded_csv"
						+ " WHERE  returnType='GSTR2' and type='B2BUR' and uploadDataInfoId=[uploadDataInfoId] "),

		GSTR2_CDN("GSTR2_CDN",
				"SELECT lineNumber,GST_APPLICABILITY as gstApplicability,ENTRY_DATE as entryDate,ENTRY_NO as entryNo,ORIGINAL_INV_ENT_NO as originalInvEntNo,ORIGINAL_INV_ENT_DT as originalInvEntDt, tradeType,CP_GSTIN_NO as cpGstinNo,CP_NAME as cpName,REF_DOC_NO as refDocNo,REF_DOC_DATE as refDocDate,ORIGINAL_INV_ENT_REFNO as originalInvEntRefno ,ORIGINAL_INV_ENT_REFDT as originalInvEntRefdt,ENTRY_TYPE as entryType,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,DNCN_REASON as dncnReason, INPUT_ELIGIBILITY as inputEligibility,INPUT_IGST_AMOUNT as inputIgstAmount,INPUT_CGST_AMOUNT as inputCgstAmount,INPUT_SGST_AMOUNT as inputSgstAmount,INPUT_CESS_AMOUNT as inputCessAmount  FROM uploaded_csv "
						+ " WHERE returnType='GSTR2' and type='CDN' and  uploadDataInfoId=[uploadDataInfoId]"),

		GSTR2_CDNUR("GSTR2_CDNUR",
				"SELECT lineNumber,GST_APPLICABILITY as gstApplicability,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,tradeType,CP_GSTIN_NO as cpGstinNo,CP_NAME as cpName,ORIGINAL_INV_ENT_NO as originalInvEntNo,ORIGINAL_INV_ENT_DT as originalInvEntDt,REF_DOC_NO as refDocNo,REF_DOC_DATE as refDocDate,ENTRY_TYPE as entryType,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CGST_AMOUNT as cgstAmount,SGST_AMOUNT as sgstAmount,CESS_AMOUNT as cessAmount,DNCN_REASON as dncnReason, INPUT_ELIGIBILITY as inputEligibility,INPUT_IGST_AMOUNT as inputIgstAmount,INPUT_CGST_AMOUNT as inputCgstAmount,INPUT_SGST_AMOUNT as inputSgstAmount,INPUT_CESS_AMOUNT as inputCessAmount  FROM uploaded_csv "
						+ " WHERE returnType='GSTR2' and type='CDNUR' and  uploadDataInfoId=[uploadDataInfoId]"),

		GSTR2_NIL("GSTR2_NIL",
				"select lineNumber,subType,NET_AMOUNT as netAmount from uploaded_csv where type='NIL' and returnType='GSTR2' and  uploadDataInfoId=[uploadDataInfoId]"
		),

		GSTR2_IMPG("GSTR2_IMPG",
				"SELECT REF_DOC_NO as refDocNo,REF_DOC_DATE as refDocDate,ENTRY_DATE as entryDate,,NET_AMOUNT as netAmount,ENTRY_NO as entryNo,lineNumber,GST_APPLICABILITY as gstApplicability, CP_GSTIN_NO as cpGstinNo,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,CP_GSTIN_STATE_CODE as cpGstinStateCode ,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CESS_AMOUNT as cessAmount,TRADETYPE as tradeType,INPUT_ELIGIBILITY as inputEligibility,INPUT_IGST_AMOUNT as inputIgstAmount,INPUT_CGST_AMOUNT as inputCgstAmount,INPUT_SGST_AMOUNT as inputSgstAmount,INPUT_CESS_AMOUNT as inputCessAmount  FROM uploaded_csv"
						+ " WHERE returnType='GSTR2' and type='IMPG' and uploadDataInfoId=[uploadDataInfoId]"),

		GSTR2_IMPS("GSTR2_IMPS",
				"SELECT REF_DOC_NO as refDocNo,REF_DOC_DATE as refDocDate,,NET_AMOUNT as netAmount,ENTRY_DATE as entryDate,ENTRY_NO as entryNo,lineNumber,GST_APPLICABILITY as gstApplicability, CP_GSTIN_NO as cpGstinNo,ENTRY_NO as entryNo,ENTRY_DATE as entryDate,CP_GSTIN_STATE_CODE as cpGstinStateCode ,HSN_SAC_CODE as hsnSacCode,HSN_SAC_DESC as hsnSacDesc,UOM as uom,quantity as quantity,TAXABLE_AMOUNT as taxableAmount,TOTAL_TAXRATE as totalTaxRate,IGST_AMOUNT as igstAmount,CESS_AMOUNT as cessAmount,TRADETYPE as tradeType,INPUT_ELIGIBILITY as inputEligibility,INPUT_IGST_AMOUNT as inputIgstAmount,INPUT_CGST_AMOUNT as inputCgstAmount,INPUT_SGST_AMOUNT as inputSgstAmount,INPUT_CESS_AMOUNT as inputCessAmount  FROM uploaded_csv"
						+ " WHERE returnType='GSTR2' and type='IMPS' and uploadDataInfoId=[uploadDataInfoId]"),

		;

		private ConsolidatedQuery(String name, String query) {
			this.queryName = query;
			this.query = query;

		}

		String queryName;
		String query;

		public String getQuery() {
			return this.query;
		}
	}

	@Override
	public boolean importExcelToDatabase(String fileName, int id) {

		Statement stmt = null;
		try (Connection connection = DBUtils.getConnection();) {
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			String delimeters = " FIELDS  TERMINATED BY ','  OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES ";

			String query = "LOAD DATA LOCAL INFILE '" + fileName + "' INTO TABLE uploaded_csv  " + delimeters
					+ " ( `TRADETYPE`, `ENTRY_DATE`, `ENTRY_NO`, `GST_APPLICABILITY`, "
					+ "`ETIN`, `REF_DOC_NO`, `REF_DOC_DATE`, `CP_GSTIN_NO`, `CP_NAME`, `CP_REGISTRATION_STATUS`, `CP_GSTIN_REGISTRATION_DATE`,"
					+ " `CP_MOBILE_NO`, `CP_EMAIL`, `CP_CLASS_NAME`, `CP_AGENT_CLASS_NAME`, `CP_PANCARD_NO`,  `CP_GSTIN_STATE_CODE`,"
					+ " `ORIGINAL_INV_ENT_NO`, `ORIGINAL_INV_ENT_DT`, `ORIGINAL_INV_ENT_REFNO`, "
					+ "`ORIGINAL_INV_ENT_REFDT`, `DNCN_REASON`, `HSN_SAC_CODE`, `HSN_SAC_DESC`, `UOM`, `CGST_RATE`, `SGST_RATE`, `IGST_RATE`, "
					+ "`CESS_RATE`, `TOTAL_TAXRATE`, `TAXABLE_AMOUNT`, `CGST_AMOUNT`, `SGST_AMOUNT`, `IGST_AMOUNT`, `CESS_AMOUNT`, `NET_AMOUNT`,"
					+ " `INPUT_ELIGIBILITY`, `INPUT_CGST_AMOUNT`, `INPUT_SGST_AMOUNT`, `INPUT_IGST_AMOUNT`, `INPUT_CESS_AMOUNT`, `quantity`) set id=null, uploadDataInfoId="
					+ id + ";";

			stmt.executeUpdate(query);

		} catch (Exception e) {
			log.error("error occ {}", e);
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public List getDataByReturnTypeTransaction(GstinTransactionDTO gstinTransactionDTO, String dataUploadId) {
		/*
		 * DataUploadInfo info=crudService.find(DataUploadInfo.class,dataUploadId);
		 * gstinTransactionDTO.setReturnType(info.getReturnType());
		 * gstinTransactionDTO.setTransactionType(TransactionType.B2B.toString());
		 * gstinTransactionDTO.setMonthYear(info.getMonthYear());
		 * gstinTransactionDTO.setTaxpayerGstin((TaxpayerGstinDTO)EntityHelper.convert(
		 * info.getTaxpayerGstin(),TaxpayerGstinDTO.class));
		 */
		ReturnType rType = ReturnType.valueOf(gstinTransactionDTO.getReturnType());
		TransactionType type = TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
		// ConsolidatedQuery
		// query=ConsolidatedQuery.valueOf(rType.toString()+"_"+type.toString());
		Query jpaQuery = em.createNamedQuery(rType.toString() + "_" + type.toString() + "." + "findByDataUploadInfoId");
		// jpaQuery.setParameter("dataUploadId",
		// dataUploadId);GSTR1_B2B.findByDataUploadInfoId
		List result = jpaQuery.getResultList();

		return result;
	}

	/*
	 * public List getDataByReturnTypeTransaction(GstinTransactionDTO
	 * gstinTransactionDTO,String dataUploadId){ ReturnType
	 * rType=ReturnType.valueOf(gstinTransactionDTO.getReturnType());
	 * TransactionType
	 * type=TransactionType.valueOf(gstinTransactionDTO.getTransactionType());
	 * ConsolidatedQuery
	 * query=ConsolidatedQuery.valueOf(rType.toString()+"_"+type.toString()); Query
	 * jpaQuery=em.createQuery(query.getQuery());
	 * jpaQuery.setParameter("dataUploadId", dataUploadId); List
	 * result=jpaQuery.getResultList(); return result; }
	 * 
	 * 
	 */

	@Override
	public int saveData(GstinTransactionDTO gstinTransactionDto, List<UploadedCsv> lineItems) {
		int count = 0;
		try {
			TransactionFactory tf = new TransactionFactory(gstinTransactionDto.getTransactionType());

			count = tf.processCsvDataConsolidated(gstinTransactionDto.getReturnType(), "",
					em.find(TaxpayerGstin.class, gstinTransactionDto.getTaxpayerGstin().getId()),
					gstinTransactionDto.getTransactionType(), gstinTransactionDto.getMonthYear(), null, "Yes",
					SourceType.GINESYS, lineItems);
		} catch (AppException e) {
			log.error("error {}", e);
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public int saveUploadedInfo(GstinTransactionDTO gstinTransactionDto, String file) {
		DataUploadInfo info = new DataUploadInfo();
		info.setUrl(file);
		info.setFileFormat("csv");
		info.setFileName(file);
		info.setGstin(gstinTransactionDto.getTaxpayerGstin().getGstin());
		info.setTaxpayerGstin(em.find(TaxpayerGstin.class, gstinTransactionDto.getTaxpayerGstin().getId()));
		info.setIsPrimary((byte) 1);
		info.setMonthYear(gstinTransactionDto.getMonthYear());
		crudService.create(info);
		return info.getId();
	}

}
