package com.ginni.easemygst.portal.transaction.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;
import org.json.JSONObject;
import org.omg.CORBA._PolicyStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.dto.ReconcileDTO;
import com.ginni.easemygst.portal.business.dto.TransactionDataDTO;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.StateDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.SupplyType;
import com.ginni.easemygst.portal.data.TableFlags;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.B2CS;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.helper.config.AppConfig;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2CSTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.B2CSService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class B2CSTransaction extends TransactionUtil implements Transaction {

	private FinderService finderService;

	private ReturnsService returnService;

	private CrudService crudService;

	private final static Logger _LOGGER = LoggerFactory.getLogger(B2CSTransaction.class);

	int _STATE_CODE_INDEX = 0;
	int _INVOICE_NUMBER = 1;
	int _ETIN_INDEX = 2;
	int _MONTH_YEAR_INDEX = 14;
	int _TYP_INDEX = 15;
	int _ORIGINAL_MONTH = 14;
	int _ORIGINAL_POS = 15;

	public B2CSTransaction() {

		_SNO_INDEX = 3;
		_HSN_CODE_INDEX = 4;
		_DESCRIPTION_INDEX = 5;
		_UNIT_INDEX = 6;
		_QUANTITY_INDEX = 7;
		_TAXABLE_VALUE_INDEX = 8;
		_TAX_RATE_INDEX = 9;
		_IGST_AMT_INDEX = 10;
		_CGST_AMT_INDEX = 11;
		_SGST_AMT_INDEX = 12;
		_CESS_AMT_INDEX = 13;
		_TEMP_TALLY_POS = 01;
		_ORIGINAL_MONTH = 15;
		_ORIGINAL_POS = 16;
		
		System.out.println("Inside B2CS Constructor");

	}
	List<String> temp=new ArrayList<String>();

	public static List<B2CS> addB2CSObject(B2CS b2cs, List<B2CS> b2css) {
		String uId = getRandomUniqueId();
		if (uId != null) {
			b2cs.setGstnSynced(false);
			b2cs.setId(uId);
			b2css.add(b2cs);
		}
		return b2css;
	}

	public static String getRandomUniqueId() {
		String uId = UUID.randomUUID().toString();

		return uId;

	}

	public static List<B2CS> updateB2CSObject(B2CS b2cs, List<B2CS> b2css) {

		List<B2CS> tB2css = b2css.stream().filter(b -> b.getId().equalsIgnoreCase(b2cs.getId()))
				.collect(Collectors.toList());
		if (!StringUtils.isEmpty(b2cs.getId()) && !tB2css.isEmpty()) {
			B2CS existingB2cs = tB2css.get(0);
			b2css.remove(existingB2cs);
			b2cs.setGstnSynced(false); // setting tax payer action modified
			b2cs.setTaxpayerAction(TaxpayerAction.MODIFY);
			b2css.add(b2cs);

		} else {

			if (StringUtils.isEmpty(b2cs.getId()))
				b2cs.setId(getRandomUniqueId());
			b2cs.setGstnSynced(false); // setting tax payer action modified in
										// case of adding new b2cs
			b2css.add(b2cs);
		}

		return b2css;
	}

	public static List<B2CS> updateB2CSObjectByNo(B2CS b2cs, List<B2CS> b2css) {

		List<B2CS> searchedB2css = b2css.stream().filter(b -> b.getStateCode() == b2cs.getStateCode())
				.collect(Collectors.toList());
		if (!searchedB2css.isEmpty()) {
			B2CS existingB2cs = searchedB2css.get(0);
			b2css.remove(existingB2cs);
			b2cs.setGstnSynced(false); // setting tax payer action modified
			b2cs.setTaxpayerAction(TaxpayerAction.MODIFY);
			b2cs.setId(existingB2cs.getId());// since we are replacing the
												// existing b2cs with the
												// updated one so we need to
												// copy id from existing one
			b2css.add(b2cs);

		} else {

			if (StringUtils.isEmpty(b2cs.getId()))
				b2cs.setId(getRandomUniqueId());
			b2cs.setGstnSynced(false); // setting tax payer action modified in
										// case of adding new b2cs
			b2css.add(b2cs);
		}

		return b2css;
	}

	public static List<B2CS> deleteB2CSObject(B2CS b2cs, List<B2CS> b2css) {
		String bId = b2cs.getId();
		List<B2CS> temp = b2css.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equals(b.getId()))
				.collect(Collectors.toList());
		if (!temp.isEmpty()) {
			if (temp.get(0).isGstnSynced()) {
				temp.get(0).setGstnSynced(false);
				temp.get(0).setTaxpayerAction(TaxpayerAction.DELETE);
			} else
				b2css.remove(temp.get(0));
		}

		return b2css;
	}

	public static List<B2CS> changeStatusB2CSObject(B2CS b2cs, List<B2CS> b2css, TaxpayerAction taxpayerAction) {
		String bId = b2cs.getId();
		List<B2CS> temp = b2css.stream().filter(b -> !StringUtils.isEmpty(bId) && bId.equals(b.getId()))
				.collect(Collectors.toList());
		if (!temp.isEmpty()) {
			temp.get(0).setTaxpayerAction(taxpayerAction);
			temp.get(0).setGstnSynced(false);
		}

		return b2css;
	}

	public static List<B2CS> addB2CSObject(List<B2CS> existingB2bs, List<B2CS> transactionB2bs) {

		for (B2CS b2cs : transactionB2bs) {
			existingB2bs = addB2CSObject(b2cs, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CS> updateB2CSObject(List<B2CS> existingB2bs, List<B2CS> transactionB2bs) {

		for (B2CS b2cs : transactionB2bs) {
			existingB2bs = updateB2CSObject(b2cs, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CS> updateB2CSObjectByNo(List<B2CS> existingB2bs, List<B2CS> transactionB2bs) {

		for (B2CS b2cs : transactionB2bs) {
			existingB2bs = updateB2CSObjectByNo(b2cs, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CS> deleteB2CSObject(List<B2CS> existingB2bs, List<B2CS> transactionB2bs) {

		for (B2CS b2cs : transactionB2bs) {
			existingB2bs = deleteB2CSObject(b2cs, existingB2bs);
		}

		return existingB2bs;
	}

	public static List<B2CS> changeStatusB2CSObject(List<B2CS> existingB2bs, List<B2CS> transactionB2bs,
			TaxpayerAction taxpayerAction) {

		for (B2CS b2cs : transactionB2bs) {
			existingB2bs = changeStatusB2CSObject(b2cs, existingB2bs, taxpayerAction);
		}

		return existingB2bs;
	}
	
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}

	@Override
	public int processExcelList(GstExcel gstExcel, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, SourceType source, String delete)
			throws AppException {

		List<B2CSTransactionEntity> b2csTransactionEntities = new ArrayList<>();

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : new String(gstin.substring(0, 2)).intern();

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		int columnCount = 0;
		int index = 0;
		Iterator<Row> iterator = gstExcel.getSheet().iterator();
		while (iterator.hasNext()) {

			Row row = iterator.next();
			int rowNum = row.getRowNum();
			if (rowNum == 0 || this.getTallyColumCountCondition(source, rowNum)) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
				List<String> temp = gstExcel.excelReadRow(cellRange);
				_LOGGER.trace("excel row  {}", temp);

				if (temp == null)
					break;
				// convert list to b2cs object
				if (source == SourceType.TALLY)
					temp = this.processTallyExcel(temp, gstinPos, sheetName, ee);
				index++;
				convertListToTransactionData(ee, b2csTransactionEntities, temp, index, sheetName, sourceName, sourceId,
						gstReturn, gstinPos, taxpayerGstin, monthYear, userBean);

			}
		}

		if (ee.isError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			throw exp;
		}

		// JSONObject output = new JSONObject();
		// output.put("data", JsonMapper.objectMapper.writeValueAsString(b2css));
		// output.put("size", index);

		int uploadingCount = 0;
		b2csTransactionEntities = b2csTransactionEntities.stream().distinct().collect(Collectors.toList());
		uploadingCount = this.saveTransaction(b2csTransactionEntities, delete);
		// logging user action
		ApiLoggingService apiLoggingService;
		try {
			apiLoggingService = (ApiLoggingService) InitialContext
					.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
			MessageDto messageDto = new MessageDto();
			messageDto.setGstin(taxpayerGstin.getGstin());
			messageDto.setReturnType(gstReturn);
			messageDto.setMonthYear(monthYear);
			messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
			messageDto.setTransactionType(TransactionType.B2CS.toString());
			if (!StringUtils.isEmpty(delete) && delete.equalsIgnoreCase("Yes"))
				apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,
						taxpayerGstin.getGstin());
			else
				apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto, taxpayerGstin.getGstin());
			// end logging user action
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return uploadingCount;

	}

	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source)
			throws AppException {

		List<B2CSTransactionEntity> b2csTransactionEntities = new ArrayList<>();

		String gstin = taxpayerGstin.getGstin();

		// set column mapping
		setColumnMapping(headerIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : new String(gstin.substring(0, 2)).intern();

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		int index = 0;

		CSVParser csvParser = new CSVParser();
		boolean isMappingSet = false;
		boolean skipHeader = false;
		while (scanner.hasNext()) {
			List<String> temp = csvParser.parseLine(scanner.nextLine());
			if (skipHeader)
				skipHeader = false;
			else {
				// convert list to b2cs object
				_LOGGER.trace("excel row  {}", temp);

				if (temp == null)
					break;
				// convert list to b2cs object

				if (!isMappingSet) {
					if (temp.size() == 16) {
						headerIndex = "0,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17";
						_INVOICE_NUMBER = 1;
					} else {
						headerIndex = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17";

					}
					isMappingSet = true;
					// set column mapping
					setColumnMapping(headerIndex, gstReturn);
				}
				index++;
				convertListToTransactionData(ee, b2csTransactionEntities, temp, index, sheetName, sourceName, sourceId,
						gstReturn, gstinPos, taxpayerGstin, monthYear, userBean);
			}

		}
		scanner.close();

		if (ee.isFinalError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			System.out.println(ee.getErrorDesc().toString());
			throw exp;
		}

		return this.saveTransaction(b2csTransactionEntities, delete);

	}

	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException {

		List<B2CSTransactionEntity> b2csTransactionEntities = new ArrayList<>();

		String gstin = taxpayerGstin.getGstin();

		String customHeaderIndex = "";
		if (gstReturn.equalsIgnoreCase(ReturnType.GSTR1.toString()))
			customHeaderIndex = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,100,100";
		else
			customHeaderIndex = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22";

		// set column mapping
		setColumnMapping(customHeaderIndex, gstReturn);

		String gstinPos = StringUtils.isEmpty(gstin) ? "00" : new String(gstin.substring(0, 2)).intern();

		String sourceId = Utility.randomString(8);
		SourceType sourceName = source;

		ExcelError ee = new ExcelError();

		int index = 0;

		for (UploadedCsv lineItem : lineItems) {
			// convert list to b2cs object
			index = lineItem.getLineNumber();
			convertListToTransactionData(ee, b2csTransactionEntities,
					this.convertLineItemIntoList(lineItem, ReturnType.valueOf(gstReturn)), index, sheetName, sourceName,
					sourceId, gstReturn, gstinPos, taxpayerGstin, monthYear, userBean);

		}

		if (ee.isFinalError()) {
			AppException exp = new AppException();
			exp.setCode(ExceptionCode._ERROR);
			exp.setMessage(ee.getErrorDesc().toString());
			System.out.println(ee.getErrorDesc().toString());
			throw exp;
		}

		return this.saveTransaction(b2csTransactionEntities, delete);

	}

	private List<String> convertLineItemIntoList(UploadedCsv lineItem, ReturnType returnType) {
		List<String> fieldList = new ArrayList<>();

		fieldList.add(lineItem.getCpGstinStateCode());
		fieldList.add(lineItem.getEtin());
		fieldList.add("1");// for serial no
		fieldList.add(lineItem.getHsnSacCode());
		fieldList.add(lineItem.getHsnSacDesc());
		fieldList.add(lineItem.getUom());
		fieldList.add(lineItem.getQuantity());
		fieldList.add(lineItem.getTaxableAmount());
		fieldList.add(lineItem.getTotalTaxrate());
		fieldList.add(lineItem.getIgstAmount());
		fieldList.add(lineItem.getCgstAmount());
		fieldList.add(lineItem.getSgstAmount());
		fieldList.add(lineItem.getCessAmount());
		fieldList.add("");// monthYear
		String type = StringUtils.isEmpty(lineItem.getEtin()) ? "OE" : "E";
		fieldList.add(type);// tppe information type ecom /orhter than ecoomS

		return fieldList;
	}

	int retry = 0;

	private boolean isDate_index;

	private int _INVOICE_DATE_INDEX;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private int _SUPPLY_VALUE_INDEX;

	private boolean isInvoice_number;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;

	public int saveTransaction(List<B2CSTransactionEntity> b2csTransactionEntities, String delete) throws AppException {

		try {
			return new B2CSService().insertUpdate(b2csTransactionEntities, delete);
		} catch (SQLException e) {

			if (e instanceof SQLException) {
				SQLException exception = (SQLException) e;

				if (exception.getErrorCode() == _DEADLOCK_ERR_CODE
						|| exception.getErrorCode() == _LOCK_WAIT_TIMEOUT_ERR_CODE) {
					while (retry++ < _MAX_RETRY) {
						try {
							Thread.sleep(_MAX_RETRY_WAIT_TIME);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						_Logger.info("Deadlock in {} while insertion for gstin {} return {} retry count {}",
								this.getClass().getName(), b2csTransactionEntities.get(0).getGstin().getGstin(),
								b2csTransactionEntities.get(0).getReturnType(), retry);

						return this.saveTransaction(b2csTransactionEntities, delete);

					}
				}

			}
			_LOGGER.error("exception ", e);
			throw new AppException(ExceptionCode._ERROR);
		}
	}

	public static void setColumns(String headerIndex, String gstReturn, B2CSTransaction b2csTransaction) {
		b2csTransaction.setColumnMapping(headerIndex, gstReturn);
	}

	public void setColumnMapping(String headerIndex, String gstReturn) {

		if (!StringUtils.isEmpty(headerIndex)) {
			String[] headers = headerIndex.split(",");
			_STATE_CODE_INDEX = Integer.valueOf(headers[0]);
			_ETIN_INDEX = Integer.valueOf(headers[1]);
			_SNO_INDEX = Integer.valueOf(headers[2]);
			_HSN_CODE_INDEX = Integer.valueOf(headers[3]);
			_DESCRIPTION_INDEX = Integer.valueOf(headers[4]);
			_UNIT_INDEX = Integer.valueOf(headers[5]);
			_QUANTITY_INDEX = Integer.valueOf(headers[6]);
			_TAXABLE_VALUE_INDEX = Integer.valueOf(headers[7]);
			_TAX_RATE_INDEX = Integer.valueOf(headers[8]);
			_IGST_AMT_INDEX = Integer.valueOf(headers[9]);
			_CGST_AMT_INDEX = Integer.valueOf(headers[10]);
			_SGST_AMT_INDEX = Integer.valueOf(headers[11]);
			_CESS_AMT_INDEX = Integer.valueOf(headers[12]);
			_MONTH_YEAR_INDEX = Integer.valueOf(headers[13]);
			_TYP_INDEX = Integer.valueOf(headers[14]);
			/* _ORIGINAL_MONTH =Integer.valueOf(headers[15]); */
			// _ORIGINAL_POS =Integer.valueOf(headers[16]);

			_INVOICE_NUMBER = -1;
		}

	}

	public void convertListToData(ExcelError ee, List<B2CSTransactionEntity> b2csTransactionEntity, List<String> temp,
			int index, String sheetName, SourceType sourceName, String sourceId, String gstReturn, String gstinPos,
			TaxpayerGstin gstin, String monthYear, UserBean userBean) {
		convertListToTransactionData(ee, b2csTransactionEntity, temp, index, sheetName, sourceName, sourceId, gstReturn,
				gstinPos, gstin, monthYear, userBean);
	}

	private void convertListToTransactionData(ExcelError ee, List<B2CSTransactionEntity> b2csEntities,
			List<String> temp, int index, String sheetName, SourceType sourceName, String sourceId, String gstReturn,
			String gstinPos, TaxpayerGstin taxPayerGstin, String monthYear, UserBean userBean) {

		handleAmmendmentCase(temp, TransactionType.B2CS);

		ee.resetFieldsExceptErrorAndErrorDesc();

		ee.setUniqueRowValue("State - [" + temp.get(_STATE_CODE_INDEX) + "]");

		// B2CS b2cs = new B2CS();

		/*
		 * B2CSTransactionEntity.contains(temp.get(_STATE_CODE_INDEX),temp.get(
		 * _TYP_INDEX, b2csType, taxrate, b2csEntities)
		 */
		B2CSTransactionEntity b2csEntity = null;
		Item item = null;

		// setting state code
		if (sourceName == SourceType.SAP)
			ee = this.validateStateCode(sheetName, _STATE_CODE_INDEX, index, temp.get(_STATE_CODE_INDEX), ee);
		else
			ee = this.validatePos(sheetName, _STATE_CODE_INDEX, index, temp.get(_STATE_CODE_INDEX), gstinPos, ee);

		if (!(sourceName == SourceType.TALLY) && _HSN_CODE_INDEX != 100 && !ee.isError()) {
			b2csEntity = B2CSTransactionEntity.contains(temp.get(_STATE_CODE_INDEX), temp.get(_TYP_INDEX),
					Double.parseDouble(temp.get(_TAX_RATE_INDEX)), b2csEntities, temp.get(_HSN_CODE_INDEX));
		}

		if (Objects.isNull(b2csEntity)) {
			b2csEntity = new B2CSTransactionEntity();
			item = new Item();
			b2csEntities.add(b2csEntity);
		} else {
			item = b2csEntity.getItem();
		}

		if (_INVOICE_NUMBER != -1) {
			b2csEntity.setInvoiceNumber(temp.get(_INVOICE_NUMBER));
			ee.setUniqueRowValue(
					"State - [" + temp.get(_STATE_CODE_INDEX) + "] InvoiceNo - [" + temp.get(_INVOICE_NUMBER) + "]");
		}

		b2csEntity.setMonthYear(monthYear);

		String gstin = taxPayerGstin.getGstin();
		b2csEntity.setSource(sourceName.toString());
		b2csEntity.setSourceId(sourceId);

		b2csEntity.setGstin(taxPayerGstin);

		b2csEntity.setReturnType(gstReturn);

		InfoService.setInfo(b2csEntity, userBean);

		if (!ee.isError()) {
			String sCode = String.valueOf(ee.getPosValue());
			b2csEntity.setPos(sCode);
			b2csEntity.setStateName(
					AppConfig.getStateNameByCode(sCode) != null ? AppConfig.getStateNameByCode(sCode).getName() : null);
			this.setIsIgst(sheetName, gstinPos, gstin, ee.getPos(), ee);

		}

		// setting is igst transaction

		b2csEntity.setSupplyType(ee.isIgst() ? SupplyType.INTER : SupplyType.INTRA);

		// setting etin
		ee = this.validateEtin(sheetName, _ETIN_INDEX, index, temp.get(_ETIN_INDEX), ee);
		if (!ee.isError())
			b2csEntity.setEtin(temp.get(_ETIN_INDEX));

		double taxAmount = 0;
		if (sourceName == SourceType.TALLY)
			taxAmount = this.setTallyInvoiceItem(ee, sheetName, gstReturn, index, temp, item, gstin);
		else
			taxAmount = this.setInvoiceItemsB2CS(ee, sheetName, gstReturn, index, temp, item, TransactionType.B2CS);

		List<Item> items = new ArrayList<>();

		items.add(item);

		b2csEntity.setItems(items);

		this.validateTaxableValueB2cs(sheetName, _TAXABLE_VALUE_INDEX, index, temp.get(_TAXABLE_VALUE_INDEX), ee);
		if (!ee.isError())
			b2csEntity
					.setTaxableValue(b2csEntity.getTaxableValue() + Double.parseDouble(temp.get(_TAXABLE_VALUE_INDEX)));
		b2csEntity.setTaxAmount(b2csEntity.getTaxAmount() + taxAmount);

		if (sourceName != SourceType.SAP) {
			ee = this.validateType(sheetName, _TYP_INDEX, index, temp.get(_TYP_INDEX), ee, b2csEntity.getEtin());
			/* if(!ee.isError()) */
			b2csEntity.setB2csType(StringUtils.upperCase(temp.get(_TYP_INDEX)));
		} else {
			if (StringUtils.isNotEmpty(b2csEntity.getEtin()))
				b2csEntity.setB2csType("E");
			else
				b2csEntity.setB2csType("OE");
		}

		// if (!ee.isFinalError())
		// addB2CSObject(b2cs, b2css);

	}

	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction action)
			throws AppException {

		try {
			if (StringUtils.isEmpty(existingData)) {
				return transactions;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<B2CS> existingB2bs = objectMapper.readValue(existingData, new TypeReference<List<B2CS>>() {
				});
				List<B2CS> transactionB2bs = objectMapper.readValue(transactions, new TypeReference<List<B2CS>>() {
				});

				if (action == null) {
					existingB2bs = addB2CSObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFY) {
					existingB2bs = updateB2CSObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.DELETE) {
					existingB2bs = deleteB2CSObject(existingB2bs, transactionB2bs);
				} else if (action == TaxpayerAction.MODIFYBYNO) {
					existingB2bs = updateB2CSObjectByNo(existingB2bs, transactionB2bs);
				} else if (action != null) {
					existingB2bs = changeStatusB2CSObject(existingB2bs, transactionB2bs, action);
				}

				return objectMapper.writeValueAsString(existingB2bs);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public static B2CSTransactionEntity contains(String pos, String b2csType, Double taxrate,
			List<B2CSTransactionEntity> B2CSTransactionEntitylist) {

		for (B2CSTransactionEntity b2csTransactionEntity : B2CSTransactionEntitylist) {
			if (pos.equalsIgnoreCase(b2csTransactionEntity.getPos()) && b2csTransactionEntity.getTaxRate() == taxrate
					&& b2csType.equals(b2csTransactionEntity.getB2csType())) {
				return b2csTransactionEntity;
			}
		}
		return null;
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String object1, String object2, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object validateData(Object object, String monthYear) throws AppException {

		try {

			String input = (String) object;
			JSONObject jsonObject = new JSONObject(input);
			String transactions = jsonObject.get("data") != null ? jsonObject.get("data").toString() : "[]";
			String summary = jsonObject.get("summary") != null ? jsonObject.get("summary").toString() : "{}";

			List<B2CS> b2cs = JsonMapper.objectMapper.readValue(transactions, new TypeReference<List<B2CS>>() {
			});
			GstrSummaryDTO gstrSummaryDTO = JsonMapper.objectMapper.readValue(summary, GstrSummaryDTO.class);

			List<StateDTO> states = new ArrayList<>();

			try {
				finderService = (FinderService) InitialContext.doLookup("java:comp/env/finder");
				states = finderService.getAllState();
			} catch (NamingException e) {
				e.printStackTrace();
			}

			double totalTaxableValue = 0.0;
			double totalTaxAmount = 0;
			double amTaxableValue = 0.0;
			double amTaxAmount = 0;

			double igstAmount = 0.0;
			double cgstAmount = 0.0;
			double sgstAmount = 0.0;
			double cessAmount = 0.0;

			boolean summaryError = false;

			int errorCount = 0;
			int invoiceCount = 0;

			for (B2CS b2cs2 : b2cs) {

				StateDTO temp = new StateDTO();
				temp.setId(b2cs2.getStateCode());
				int i = states.indexOf(temp);
				if (i > -1) {
					StateDTO stateDTO = states.get(states.indexOf(temp));
					b2cs2.setStateName(stateDTO.getName());
				} else {
					b2cs2.setStateName("No Name");
				}

				double taxAmount = 0;
				Map<String, String> errors = new HashMap<>();
				boolean errorFlag = true;
				Double invoiceValue = b2cs2.getTaxableValue();
				if (invoiceValue < 0.0) {
					errors.put("TAXABLE_VALUE", "Invalid taxable value, it must be greater than zero");
					errorFlag = false;
				}
				b2cs2.setError(errors);
				b2cs2.setValid(errorFlag);

				if (!errorFlag) {
					summaryError = true;
					errorCount += 1;
				}

				if (!StringUtils.isEmpty(b2cs2.getOriginalStateCode()) && !Objects.isNull(b2cs2.getOriginalMonth())) {
					b2cs2.setAmendment(true);
				} else {
					b2cs2.setAmendment(false);
				}

				if (!Objects.isNull(b2cs2.getCgstAmt())) {
					taxAmount += b2cs2.getCgstAmt();
					cgstAmount += b2cs2.getCgstAmt();
				}
				if (!Objects.isNull(b2cs2.getSgstAmt())) {
					taxAmount += b2cs2.getSgstAmt();
					sgstAmount += b2cs2.getSgstAmt();
				}
				if (!Objects.isNull(b2cs2.getIgstAmt())) {
					taxAmount += b2cs2.getIgstAmt();
					igstAmount += b2cs2.getIgstAmt();
				}
				if (!Objects.isNull(b2cs2.getCessAmt())) {
					taxAmount += b2cs2.getCessAmt();
					cessAmount += b2cs2.getCessAmt();
				}
				b2cs2.setTaxAmount(taxAmount);

				if (b2cs2.isAmendment()) {
					amTaxableValue += b2cs2.getTaxableValue();
					amTaxAmount += taxAmount;
				} else {
					totalTaxableValue += b2cs2.getTaxableValue();
					totalTaxAmount += taxAmount;
				}

				if (b2cs2.isAmendment()) {
					// amend flag
				}

				invoiceCount++;

				setB2csFlag(b2cs2);

				if (!b2cs2.isValid()) {
					b2cs2.setType(ReconsileType.ERROR);
					b2cs2.setFlags("");
				}
			}

			ReconcileDTO reconcileDTO = new ReconcileDTO();
			reconcileDTO.setErrorCount(errorCount);
			reconcileDTO.setInvoiceCount(invoiceCount);

			Map<String, Object> tempMap = new HashMap<>();
			tempMap.put("data", b2cs);
			TransactionDataDTO b2csSumm = new TransactionDataDTO();
			b2csSumm.setTaxableValue(totalTaxableValue);
			b2csSumm.setTaxAmount(totalTaxAmount);
			b2csSumm.setAmTaxableValue(amTaxableValue);
			b2csSumm.setAmTaxAmount(amTaxAmount);
			b2csSumm.setError(summaryError);
			b2csSumm.setCessAmount(cessAmount);
			b2csSumm.setCgstAmount(cgstAmount);
			b2csSumm.setSgstAmount(sgstAmount);
			b2csSumm.setIgstAmount(igstAmount);
			b2csSumm.setReconcileDTO(reconcileDTO);
			gstrSummaryDTO.setB2cs(b2csSumm);
			tempMap.put("summary", gstrSummaryDTO);

			return JsonMapper.objectMapper.writeValueAsString(tempMap);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void setB2csFlag(B2CS b2cs) {
		if (!Objects.isNull(b2cs.getTaxpayerAction())) {
			if (b2cs.getTaxpayerAction() == TaxpayerAction.ACCEPT)
				b2cs.setFlags(TableFlags.ACCEPTED.toString());
			else if (b2cs.getTaxpayerAction() == TaxpayerAction.REJECT)
				b2cs.setFlags(TableFlags.REJECTED.toString());
			else if (b2cs.getTaxpayerAction() == TaxpayerAction.PENDING)
				b2cs.setFlags(TableFlags.PENDING.toString());
			else if (b2cs.getTaxpayerAction() == TaxpayerAction.MODIFY)
				b2cs.setFlags(TableFlags.MODIFIED.toString());
			else if (b2cs.getTaxpayerAction() == TaxpayerAction.DELETE)
				b2cs.setFlags(TableFlags.DELETED.toString());
			else if (b2cs.getTaxpayerAction() == TaxpayerAction.UPLOADED)
				b2cs.setFlags(TableFlags.UPLOADED.toString());
		}
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {

			List<B2CSTransactionEntity> b2csDatas = datas;
			for (B2CSTransactionEntity invoice : b2csDatas) {

				for (Item item : invoice.getItems()) {
					List<String> columns = this.getFlatData(invoice, item, returnType);
					tabularData.add(columns);
				}
			}

		} catch (Exception e) {
			_LOGGER.debug("exception in get data by type ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;

	}

	private List<String> getFlatData(B2CSTransactionEntity invoice, Item item, String returnType) {

		List<String> columns = new ArrayList<>();

		columns.add(this.getStringValue(invoice.getPos()));
		columns.add(this.getStringValue(invoice.getEtin()));
		columns.add(this.getStringValue(item.getSerialNumber()));
		columns.add(this.getStringValue(item.getHsnCode()));
		columns.add(this.getStringValue(item.getHsnDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getTaxableValue()));
		columns.add(this.getStringValue(item.getTaxRate()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCgst()));
		columns.add(this.getStringValue(item.getSgst()));
		columns.add(this.getStringValue(item.getCess()));
		columns.add(this.getStringValue(invoice.getMonthYear()));
		columns.add(this.getStringValue(invoice.getB2csType()));
		columns.add(this.getStringValue(this.processErrorMsg(invoice.getErrMsg())));

		return columns;

	}

	@Override
	public String getTransactionsData(String input) throws AppException {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<B2CS> data = new ArrayList<>();

			if (!StringUtils.isEmpty(input)) {
				data = objectMapper.readValue(input, new TypeReference<List<B2CS>>() {
				});

				data.removeIf(i -> i.getTaxpayerAction() == TaxpayerAction.DELETE);

				return JsonMapper.objectMapper.writeValueAsString(data);
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return input;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {

		List<B2CS> b2css = new ArrayList<>();

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.GINESYS.toString();

		try {
			JsonNode objects = JsonMapper.objectMapper.readTree(data);

			for (JsonNode jsonNode : objects) {

				B2CS b2cs = new B2CS();

				// set source
				b2cs.setSource(sourceName);
				b2cs.setSourceId(sourceId);

				if (jsonNode.has("stateCode")) {
					int sc = Double.valueOf(jsonNode.get("stateCode").asText()).intValue();
					b2cs.setStateCode(sc);
				}

				// setting whether this is goods or services
				if (jsonNode.has("hsnsac"))
					b2cs.setGoodsOrServiceCode(jsonNode.get("hsnsac").asText());

				if (jsonNode.has("taxableValue"))
					b2cs.setTaxableValue(Double.parseDouble(jsonNode.get("taxableValue").asText()));
				if (jsonNode.has("igstr"))
					b2cs.setTaxRate(Double.parseDouble(jsonNode.get("igstr").asText()));
				if (jsonNode.has("igsta"))
					b2cs.setIgstAmt(Double.parseDouble(jsonNode.get("igsta").asText()));
				if (jsonNode.has("cgsta"))
					b2cs.setCgstAmt(Double.parseDouble(jsonNode.get("cgsta").asText()));
				if (jsonNode.has("sgsta"))
					b2cs.setSgstAmt(Double.parseDouble(jsonNode.get("sgsta").asText()));

				addB2CSObject(b2cs, b2css);
			}

			return JsonMapper.objectMapper.writeValueAsString(b2css);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {

		try {
			TransactionDataDTO dataDTO = null;
			if (!Objects.isNull(gstrSummaryDTO)) {
				dataDTO = gstrSummaryDTO.getB2cs();
			} else {
				dataDTO = new TransactionDataDTO();
			}

			return JsonMapper.objectMapper.writeValueAsString(dataDTO);
		} catch (Exception e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {
		Map<String, List<String>> sourceB2css = getData(source, returnType);

		Map<String, List<String>> changedB2css = getData(changed, returnType);

		Map<String, List<List<String>>> exceptionB2css = new HashMap<>();

		List<List<String>> changedList = new ArrayList<>();
		List<List<String>> deletedList = new ArrayList<>();
		List<List<String>> addedList = new ArrayList<>();

		if (!Objects.isNull(sourceB2css) && !Objects.isNull(changedB2css)) {

			Iterator it = sourceB2css.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (changedB2css.containsKey(pair.getKey())) {
					Object temp1 = pair.getValue();// source data
					Object temp2 = changedB2css.get(pair.getKey());// changed
																	// data
					if (!temp1.equals(temp2)) {
						changedList.add((List<String>) temp1);
						changedList.add((List<String>) temp2);
					}
				} else {
					deletedList.add((List<String>) pair.getValue());
				}
			}

			Iterator its = changedB2css.entrySet().iterator();
			while (its.hasNext()) {
				Map.Entry pair = (Map.Entry) its.next();
				if (!sourceB2css.containsKey(pair.getKey())) {
					addedList.add((List<String>) pair.getValue());
				}
			}

			exceptionB2css.put(ExceptionSheet.CHANGES.toString(), changedList);
			exceptionB2css.put(ExceptionSheet.ADDED.toString(), addedList);
			exceptionB2css.put(ExceptionSheet.DELETED.toString(), deletedList);

			return exceptionB2css;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

	}

	private Map<String, List<String>> getData(String object, String returnType) throws AppException {
		// Note:-please add the column in columns list in the same sequence as
		// they are in excel file

		Map<String, List<String>> tabularData = new HashMap<>();
		try {
			if (StringUtils.isEmpty(object)) {
				return null;
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				List<B2CS> b2css = objectMapper.readValue(object, new TypeReference<List<B2CS>>() {
				});

				for (B2CS b2cs : b2css) {

					/*
					 * List<String> columns = this.getFlatData(b2cs, returnType); String temp =
					 * b2cs.getStateCode() == null ? "" : String.valueOf(b2cs.getStateCode());
					 * tabularData.put(temp, columns);
					 */
				}

			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		try {
			List<B2CS> b2css = JsonMapper.objectMapper.readValue(object, new TypeReference<List<B2CS>>() {
			});

			List<B2CS> currentB2css = new ArrayList<>();

			for (B2CS b2cs : b2css) {
				if (!b2cs.isAmendment() && !b2cs.isTransit()) {
					b2cs.setTransit(true);
					b2cs.setTransitId(transitId);
					currentB2css.add(b2cs);
				}
			}

			// gstr1.setB2cs(currentB2css);

			if (currentB2css.isEmpty())
				return null;
			else
				return JsonMapper.objectMapper.writeValueAsString(b2css);

		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		try {
			List<B2CS> b2css = JsonMapper.objectMapper.readValue(object, new TypeReference<List<B2CS>>() {
			});

			for (B2CS b2cs : b2css) {
				if (b2cs.isTransit() && b2cs.getTransitId().equals(transitId)) {
					b2cs.setTransit(false);
					b2cs.setTransitId("");

					b2cs.setGstnSynced(true);
				}
			}

			return JsonMapper.objectMapper.writeValueAsString(b2css);
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		try {
			returnService = (ReturnsService) InitialContext.doLookup("java:global/easemygst/ReturnsServiceImpl");
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}

		boolean isToBeUpdated = false;
		try {
			if (Objects.isNull(gstinTransactionDTO)) {
				return false;
			} else {
				gstinTransactionDTO
						.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "b2css"));

				Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
						Gstr1Dto.class);
				;
				List<B2CSTransactionEntity> transactionb2bs = gstr.getB2css();
				for (B2CSTransactionEntity b2bTran : transactionb2bs) {
					b2bTran.setMonthYear(gstinTransactionDTO.getMonthYear());
					b2bTran.setReturnType(gstinTransactionDTO.getReturnType());
					b2bTran.setGstin((TaxpayerGstin) EntityHelper.convert(gstinTransactionDTO.getTaxpayerGstin(),
							TaxpayerGstin.class));
					b2bTran.setSource(SourceType.PORTAL.toString());

					if (!StringUtils.isEmpty(b2bTran.getId())) {
						isToBeUpdated = true;
						this.updateB2csTransactionData(b2bTran);
					}
				}

				try {
					if (!isToBeUpdated) {
						new B2CSService().insertUpdate(transactionb2bs, null);
					}
				} catch (SQLException e) {
					Log.error("exception in saving b2cs transaction from portal");
					e.printStackTrace();
				}
				return true;
			}
		} catch (IOException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private boolean updateB2csTransactionData(B2CSTransactionEntity b2cs) throws AppException {

		try {
			if (Objects.isNull(b2cs)) {
				return false;
			} else {
				B2CSTransactionEntity b2csTran = crudService.find(B2CSTransactionEntity.class, b2cs.getId());
				/*
				 * b2cs.setIsAmmendment(b2csTran.getIsAmmendment());
				 * b2cs.setIsValid(b2csTran.getIsValid());
				 * b2cs.setIsTransit(b2csTran.getIsTransit());
				 * b2cs.setIsMarked(b2csTran.getIsMarked());
				 * b2cs.setIsSynced(b2csTran.getIsSynced());
				 * b2cs.setIsLocked(b2csTran.getIsLocked());
				 * b2cs.setTransitId(b2csTran.getTransitId());
				 * b2cs.setSource(b2csTran.getSource());
				 * b2cs.setSourceId(b2csTran.getSourceId()); b2cs.setFlags(b2csTran.getFlags());
				 * b2cs.setType(b2csTran.getType());
				 */
				if (b2csTran.getIsTransit()) {
					AppException ae = new AppException();
					ae.setMessage("b2cs invoice is in transit state so can't be updated for the moment");
					_Logger.error("exception--b2cs invoice is in transit state so can't be updated for the moment ");
					throw ae;
				}

				b2csTran.setPos(b2cs.getPos());
				b2csTran.setStateName(b2cs.getStateName());
				b2csTran.setSupplyType(b2cs.getSupplyType());
				b2csTran.setB2csType(b2cs.getB2csType());
				b2csTran.setTaxableValue(b2cs.getTaxableValue());
				b2csTran.setEtin(b2cs.getEtin());
				b2csTran.setTaxAmount(b2cs.getTaxAmount());
				b2csTran.setIsError(false);
				b2csTran.setErrMsg("");
				b2csTran.setFlags("");
				b2csTran.setOriginalMonth(b2cs.getOriginalMonth());
				b2csTran.setOriginalPos(b2cs.getOriginalPos());
				b2csTran.setIsAmmendment(b2cs.getIsAmmendment());
				crudService.update(b2csTran);
				returnService.deleteItemsByInvoiceId(b2csTran.getId());
				for (Item item : b2cs.getItems()) {
					crudService.create(item);
				}

			}
		} catch (AppException ae) {

			throw ae;
		} catch (Exception e) {
			_LOGGER.error("exception ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return true;
	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
		gstExcel.loadSheet(4);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				
				if(temp!=null) {
//					System.out.println(temp);
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
			}
		}
		return 0;
	}

}
