package com.ginni.easemygst.portal.transaction.impl;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jfree.util.Log;

import com.ginni.easemygst.portal.bean.UserBean;
import com.ginni.easemygst.portal.business.dto.Gstr1Dto;
import com.ginni.easemygst.portal.business.dto.GstrSummaryDTO;
import com.ginni.easemygst.portal.business.dto.MessageDto;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.service.ApiLoggingService;
import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.service.impl.ApiLoggingServiceImpl.UserAction;
import com.ginni.easemygst.portal.constant.Constant.ViewType1;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.data.TaxpayerAction;
import com.ginni.easemygst.portal.data.returns.GSTR1;
import com.ginni.easemygst.portal.data.returns.GSTR1A;
import com.ginni.easemygst.portal.data.returns.GSTR2;
import com.ginni.easemygst.portal.data.transaction.helper.ExcelError;
import com.ginni.easemygst.portal.gst.response.GetGstr1ReturnStatusResp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.UploadedCsv;
import com.ginni.easemygst.portal.persistence.entity.transaction.ATTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BDetailEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Hsn;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.persistence.service.CrudService;
import com.ginni.easemygst.portal.transaction.crud.HSNService;
import com.ginni.easemygst.portal.transaction.crud.InfoService;
import com.ginni.easemygst.portal.transaction.factory.Transaction;
import com.ginni.easemygst.portal.transaction.factory.TransactionUtil;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataFetchType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.InvoiceType;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.utils.CSVParser;
import com.ginni.easemygst.portal.utils.GstExcel;
import com.ginni.easemygst.portal.utils.Utility;

public class HSNTransaction extends TransactionUtil implements Transaction{
	
	int _CODE_INDEX=0;
	int _DESCRIPTION_INDEX=1;
	int _UNIT_INDEX=2;
	int _QUANTITY_INDEX=3;
	int _VALUE_INDEX=4;
	int _TAXABLEVALUE_INDEX=5;
	int _IGST_INDEX=6;
	int _CGST_INDEX=7;
	int _SGST_INDEX=8;
	int _CESS_INDEX=9;
	
	List<String> temp=new ArrayList<String>();

	private boolean isDate_index;

	private int _INVOICE_DATE_INDEX;

	private boolean isTaxable_index;

	private boolean isInvoice_type;

	private int _INVOICE_TYPE_INDEX;

	private boolean isSupply_index;

	private int _SUPPLY_VALUE_INDEX;

	private boolean isInvoice_number;

	private int _INVOICE_NUMBER_INDEX;

	private boolean isInvoice_value;

	private String cellRange;

	
	private void setColumnMapping(String headerIndex) {

		if (!StringUtils.isEmpty(headerIndex)) {

			String[] headers = headerIndex.split(",");
			_CODE_INDEX = Integer.valueOf(headers[0]);
	        _DESCRIPTION_INDEX=	Integer.valueOf(headers[1]);
	         _UNIT_INDEX=Integer.valueOf(headers[2]);
	    	 _QUANTITY_INDEX=Integer.valueOf(headers[3]);
	    	 _VALUE_INDEX=Integer.valueOf(headers[4]);
	    	 _TAXABLEVALUE_INDEX=Integer.valueOf(headers[5]);
	    	 _IGST_INDEX=Integer.valueOf(headers[6]);
	    	 _CGST_INDEX=Integer.valueOf(headers[7]);
	    	 _SGST_INDEX=Integer.valueOf(headers[8]);
	    	 _CESS_INDEX=Integer.valueOf(headers[9]);
		}
	}
	public static String ValidateInvoiceType(ExcelError ee,String sheetName, int _INVOICE_TYPE_INDEX, int index,String invoiceType)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		for (InvoiceType type : InvoiceType.values()) {

			if (type.getValue().equalsIgnoreCase(invoiceType))
				return type.toString();
		}
		isError = true;
		error.append(
				sheetName + "," + (_INVOICE_TYPE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue() + " Value - ["
						+ invoiceType + "] - Invalid invoice type value it should be Regular/SEZ supplies with payment/"
						+ "SEZ supplies without payment/Deemed Exp|");
		ee.setFinalError(ee.isFinalError() || isError);
		System.out.println(error);
		ee.setError(isError);
		ee.setErrorDesc(error);
		return null;
	}
	public static String ValidateInvoiceNumber(ExcelError ee,String sheetName, int _INVOICE_NUMBER_INDEX, int index,String invoiceNumber)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		int length=invoiceNumber.length();
		if(length>0 && length<16) {
			Pattern p1=Pattern.compile("^[a-zA-Z0-9-/]+$");
			Matcher m1=p1.matcher(invoiceNumber);
			if(m1.find()) {
				return invoiceNumber;
			}else {
				isError=true;
				error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
				+ " Value - [" + invoiceNumber
				+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
				ee.setError(isError);
				ee.setErrorDesc(error);
				return null;
			}
		}else {
			isError=true;
			error.append(sheetName + "," + (_INVOICE_NUMBER_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + invoiceNumber
			+ "] - Invalid invoice number  please input relevant data. it should not exceed 16 characters and should not contain special character other than hyphen(-) and slash(/) |");
			ee.setError(isError);
			ee.setErrorDesc(error);
			return null;
		}
	}
	public static Date ValidateDate(ExcelError ee,String sheetName,int _INVOICE_DATE_INDEX,int index,String sdf)
	{
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Date date = null;
		if(null == sdf) {
			return null;
		}
		try {
			df3.setLenient(false);
			date = df3.parse(sdf);
		} catch (ParseException e) {
			
			error.append(sheetName + "," + (_INVOICE_DATE_INDEX + 1) + "," + index + "," + ee.getUniqueRowValue()
			+ " Value - [" + sdf + "] - Unable to parse invoice date use format dd/mm/yyyy|");
		isError = true;
		ee.setDate(date);
		ee.setFinalError(ee.isFinalError() || isError);
		ee.setError(isError);
		ee.setErrorDesc(error);
		}
		if (date == null) {
			return null;
		}else {
			return date;
		}
	}
	public static double ValidateTaxableValue(ExcelError ee,String sheetName,int _TAXABLE_VALUE_INDEX,int index,String taxable)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(taxable);
        if(m.find()) {
        	return Double.parseDouble(taxable);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_TAXABLE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+taxable+"] -Invalid  Tax Amount  please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public static String ValidatePlaceOfSupply(ExcelError ee,String sheetName,int _SUPPLY_VALUE_INDEX,int index,String supply)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9]"+"[-]"+"[a-zA-z]");
        Matcher m = p.matcher(supply);
        if(m.find()) {
        	return supply;
        }else {
        	isError=true;
        	error.append(sheetName+","+(_SUPPLY_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+supply+"] -Invalid  Format of Place of Supply. please input relevant data ");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
		return null;
        }
	}
	public static double ValidateInvoiceValue(ExcelError ee,String sheetName,int _INVOICE_VALUE_INDEX,int index,String invoice_value)
	{
		boolean isError = ee.isError();
		StringBuilder error = ee.getErrorDesc();
		Pattern p = Pattern.compile("[0-9.]");
        Matcher m = p.matcher(invoice_value);
        if(m.find()) {
        	return Double.parseDouble(invoice_value);
        }else {
        	isError=true;
        	error.append(sheetName+","+(_INVOICE_VALUE_INDEX +1 )+","+index+","+ee.getUniqueRowValue()+"value - ["+invoice_value+"] -Invoice Value must be greater than 25000 in Invoice Number");
        	ee.setError(isError);
        	ee.setErrorDesc(error);
        	return 0L;
        }
		
	}
	public int processExcelList(GstExcel gstExcel,String returnType,String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName,String monthYear,UserBean userBean,SourceType source,String delete) throws AppException {

        String gstin=taxpayerGstin.getGstin();
        
        List<Hsn> hsns= new ArrayList<>();
		
		setColumnMapping(headerIndex);

		String sourceId = Utility.randomString(8);
		String sourceName = SourceType.EXCEL.toString();

		ExcelError ee = new ExcelError();

			int columnCount = 0;
			Integer index = 0;
			Iterator<Row> iterator = gstExcel.getSheet().iterator();
			while (iterator.hasNext()) {

				Row row = iterator.next();
				int rowNum = row.getRowNum();
				if (rowNum == 0||this.getTallyColumCountCondition(source, rowNum)) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
							+ rowNum;
					List<String> values = gstExcel.excelReadRow(cellRange);
					if (!Objects.isNull(values))
						values.removeIf(item -> item == null || "".equals(item));
					if(Objects.nonNull(values))
					columnCount = values.size();
				} else if (rowNum > 0) {
					rowNum++;
					String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(columnCount) + rowNum;
					List<String> temp = gstExcel.excelReadRow(cellRange);
					if(temp == null)
						break;
					index++;
					convertListToHSNSUM(ee,hsns,temp, index, sheetName, sourceName, sourceId,
							 taxpayerGstin,userBean,returnType,monthYear);

				}
			
			}
			
			if (ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}
			
			try {
				int uploadingCount=0;
				uploadingCount= new HSNService().insert(hsns);
				//logging user action
				ApiLoggingService apiLoggingService;
				try {
					apiLoggingService = (ApiLoggingService) InitialContext.doLookup("java:global/easemygst/ApiLoggingServiceImpl");
					MessageDto messageDto=new MessageDto();
					messageDto.setGstin(taxpayerGstin.getGstin());
					messageDto.setReturnType(returnType);
					messageDto.setMonthYear(monthYear);
					messageDto.setNoOfInvoices(String.valueOf(uploadingCount));
					messageDto.setTransactionType(TransactionType.HSNSUM.toString());
					if(!StringUtils.isEmpty(delete)&& delete.equalsIgnoreCase("Yes"))
						apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES_WITH_DELETE_ALL, messageDto,taxpayerGstin.getGstin());
						else
					apiLoggingService.saveAction(UserAction.UPLOAD_INVOICES, messageDto,taxpayerGstin.getGstin());
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//end logging user action
				return uploadingCount;
				
				
			} catch (SQLException e) {
                  _Logger.error("sql exception ",e);
				throw new AppException(ExceptionCode._ERROR);
			}
	}
	
	private  void convertListToHSNSUM(ExcelError ee,List<Hsn> hsns, List<String> temp, Integer index,
			String sheetName, String sourceName, String sourceId, TaxpayerGstin gstin,UserBean userBean,String returnType,String monthYear)
	{
		
		ee.setUniqueRowValue("HSNCode - ["+temp.get(_CODE_INDEX)+"]");
		
		     Hsn hsn = new Hsn();
		     
		     hsn.setSource(sourceName);
		     hsn.setSourceId(sourceId);
		     
		     InfoService.setInfo(hsn, userBean);
		  hsn.setTaxpayerGstin(gstin);
		  hsn.setMonthYear(monthYear);
		  hsn.setReturnType(returnType);
		
		   String hsnCode=temp.get(_CODE_INDEX);
		   this.validateHsnCode(sheetName, _CODE_INDEX, index, hsnCode, ee);
		   if(!ee.isError())
		   hsn.setCode(hsnCode);
		   
		   String hsnDesc=	temp.get( _DESCRIPTION_INDEX);
		   hsn.setDescription(hsnDesc);
		   
		   this.validateHsnOrDesc(sheetName, _CODE_INDEX, index, hsnCode, hsnDesc, ee);
		   
		   String units=temp.get(_UNIT_INDEX);
		   this.validateUnit(sheetName, _UNIT_INDEX, index, units, ee);
		   if(!ee.isError())
		   hsn.setUnit(units);
		   
       		String quantity=temp.get(_QUANTITY_INDEX);
       		this.validateQuantityB2CS(sheetName, _QUANTITY_INDEX, index, quantity, ee);
 		   if(!ee.isError() && StringUtils.isNotEmpty(quantity))
       		hsn.setQuantity(Math.abs(Double.parseDouble(quantity)));
       		
       		  
       		this.validateNumericValue(sheetName, _VALUE_INDEX, index, temp.get(_VALUE_INDEX), "Invalid value ", ee);
       		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_VALUE_INDEX)))
              hsn.setValue(Double.parseDouble(temp.get(_VALUE_INDEX)));       		

       		
              this.validateTaxableValueB2cs(sheetName, _TAXABLEVALUE_INDEX, index, temp.get(_TAXABLEVALUE_INDEX), ee);
              if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_TAXABLEVALUE_INDEX)))
               hsn.setTaxableValue(Double.parseDouble(temp.get(_TAXABLEVALUE_INDEX)));
              

          		this.validateNumericValue(sheetName, _IGST_INDEX, index, temp.get(_IGST_INDEX), "Invalid igst amount ", ee);
          		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_IGST_INDEX)))
		       hsn.setIgst(Double.parseDouble(temp.get(_IGST_INDEX)));
		       

           		this.validateNumericValue(sheetName, _CGST_INDEX, index, temp.get(_CGST_INDEX), "Invalid cgst amount ", ee);
           		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_CGST_INDEX)))
		       hsn.setCgst(Double.parseDouble(temp.get(_CGST_INDEX)));
		       

           		this.validateNumericValue(sheetName, _SGST_INDEX, index, temp.get(_SGST_INDEX), "Invalid sgst amount ", ee);
           		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_SGST_INDEX)))
		       hsn.setSgst(Double.parseDouble(temp.get(_SGST_INDEX)));
		       

           		this.validateNumericValue(sheetName, _CESS_INDEX, index, temp.get(_CESS_INDEX), "Invalid cess amount ", ee);
           		if(!ee.isError()&&!StringUtils.isEmpty(temp.get(_CESS_INDEX)))
		       hsn.setCess(Double.parseDouble(temp.get(_CESS_INDEX)));
           		
           		hsn.setTaxAmount(hsn.getCess()+hsn.getCgst()+hsn.getIgst()+hsn.getSgst());
		       
		       hsns.add(hsn);
	}



	@Override
	public int processCsvData(Scanner scanner, String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin, String sheetName,String monthYear,UserBean userBean,String delete,SourceType source)
			throws AppException {
		
String gstin=taxpayerGstin.getGstin();
        
        List<Hsn> hsns= new ArrayList<>();
		
		setColumnMapping(headerIndex);

		String sourceId = Utility.randomString(8);
		String sourceName = source.toString();

		ExcelError ee = new ExcelError();

		/*try {*/
			Integer index = 0;

			CSVParser csvParser = new CSVParser();

			boolean skipHeader = false;
			while (scanner.hasNext()) {
				List<String> temp = csvParser.parseLine(scanner.nextLine());
				if (skipHeader)
					skipHeader = false;
				else {
					if(temp == null)
						break;
					// convert list to b2b object
					index++;
					convertListToHSNSUM(ee,hsns,temp, index, sheetName, sourceName, sourceId,
							 taxpayerGstin,userBean,gstReturn,monthYear);

				}
			}
			scanner.close();

			if (ee.isFinalError()) {
				AppException exp = new AppException();
				exp.setCode(ExceptionCode._ERROR);
				exp.setMessage(ee.getErrorDesc().toString());
				throw exp;
			}
			
			try {
				return new HSNService().insert(hsns);
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(ExceptionCode._ERROR);
			}

		/*} catch (JsonProcessingException e) {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}*/
	}

	public int processCsvDataConsolidated(String gstReturn, String headerIndex, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, UserBean userBean, String delete, SourceType source,
			List<UploadedCsv> lineItems) throws AppException

	{
		return 0;
	}
	
	@Override
	public String processTransactionData(String transactions, String existingData, TaxpayerAction taxpayerAction)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String fetchTransactionData(String object, String key) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processFetchedData(String finalObj, String object, String key, boolean isFiled) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compareTransactions(String supplier, String receiver, ViewType1 viewType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTransactionsData(String input) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReconciledTransactionsData(String input, String subType) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object readDataFromGcc(String data) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object validateData(Object object, String monthYear) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, List<List<String>>> getExceptionData(String source, String changed, String returnType)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<List<String>> getErrorData(List datas, String returnType) throws AppException {
		return this.getDataByType(datas, DataFetchType.INVALID, returnType);
	}

	@Override
	public List<List<String>> getCompleteData(List datas, String returnType) throws AppException {
		return null;
	}

	
	private List<List<String>> getDataByType(List datas, DataFetchType fetchType, String returnType)
			throws AppException {
		List<List<String>> tabularData = new ArrayList<>();
		try {
			
				List<Hsn>b2csDatas=datas;
				for (Hsn invoice : b2csDatas) {
					
						List<String> columns = this.getFlatData(invoice, returnType);
						tabularData.add(columns);
					
				}

		} catch (Exception e) {
			_Logger.debug("exception in get data by type ",e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}

		return tabularData;
		
		}

private List<String> getFlatData(Hsn item,  String returnType) {
		
		List<String> columns = new ArrayList<>();
		
		
		columns.add(this.getStringValue(item.getCode()));
		columns.add(this.getStringValue(item.getDescription()));
		columns.add(this.getStringValue(item.getUnit()));
		columns.add(this.getStringValue(item.getQuantity()));
		columns.add(this.getStringValue(item.getValue()));
		columns.add(this.getStringValue(item.getTaxableValue()));
		columns.add(this.getStringValue(item.getIgst()));
		columns.add(this.getStringValue(item.getCgst()));
		columns.add(this.getStringValue(item.getSgst()));
		columns.add(this.getStringValue(item.getCess()));
		columns.add(this.getStringValue(this.processErrorMsg(item.getErrMsg())));


		return columns;

		
		}
	
	@Override
	public Object getTransactionSummary(GstrSummaryDTO gstrSummaryDTO) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processTransitData(String object, String transitId, GetGstr1ReturnStatusResp gstr1ReturnStatusResp)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1TransactionData(String object, GSTR1 gstr1, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr2TransactionData(String object, GSTR2 gstr2, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveGstr1aTransactionData(String object, GSTR1A gstr1a, String transitId) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	private CrudService crudService;
	@Override
	public Boolean saveTransactionData(GstinTransactionDTO gstinTransactionDTO, TaxpayerAction action)
			throws AppException {
		
		try {
			crudService = (CrudService) InitialContext.doLookup("java:global/easemygst/CrudServiceImpl");

		} catch (NamingException e) {
			e.printStackTrace();
		}
		TaxpayerGstin gstin = new TaxpayerGstin();
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		gstinTransactionDTO
				.setTransactionObject(gstinTransactionDTO.getTransactionObject().replace("invoices", "hsnsums"));
		gstin.setId(gstinTransactionDTO.getTaxpayerGstin().getId());
		try {

			Gstr1Dto gstr = JsonMapper.objectMapper.readValue(gstinTransactionDTO.getTransactionObject(),
					Gstr1Dto.class);
			List<Hsn> hsns = gstr.getHsnsums();
			for (Hsn hsn : hsns) {
				hsn.setTaxpayerGstin(gstin);
				hsn.setReturnType(gstinTransactionDTO.getReturnType());
				hsn.setMonthYear(gstinTransactionDTO.getMonthYear());
				hsn.setTaxpayerGstin(gstin);
				hsn.setSource(SourceType.PORTAL.toString());
				if (StringUtils.isEmpty(hsn.getId())) {
					InfoService.setInfo(hsn, gstinTransactionDTO.getUserBean());
					/*try {
						new HSNService().insert(hsns);
					} catch (SQLException e) {
						e.printStackTrace();
					}
*/					
					hsn.setIsSynced(false);
					hsn.setToBeSync(true);
					hsn.setIsTransit(false);
					hsn.setIsError(false);
					hsn.setFlags("");
					hsn.setErrMsg("");
					hsn.setSourceId("");

					crudService.update(hsn);
				} else {
					Hsn existingHsn = crudService.find(Hsn.class, hsn.getId());
					if (existingHsn.getIsTransit()) {
						AppException ae = new AppException();
						ae.setMessage("hsn item is in transit state so can't be updated for the moment");
						_Logger.error("exception--hsn item is in transit state so can't be updated for the moment ");
						throw ae;
					}
					existingHsn.setCode(hsn.getCode());
					existingHsn.setDescription(hsn.getDescription());
					existingHsn.setUnit(hsn.getUnit());
					existingHsn.setQuantity(hsn.getQuantity());
					existingHsn.setIgst(hsn.getIgst());
					existingHsn.setCgst(hsn.getCgst());
					existingHsn.setSgst(hsn.getSgst());
					existingHsn.setCess(hsn.getCess());
					existingHsn.setTaxableValue(hsn.getTaxableValue());
					existingHsn.setTaxAmount(hsn.getTaxAmount());
					existingHsn.setValue(hsn.getValue());
					existingHsn.setIsError(false);
					existingHsn.setErrMsg("");
					
					InfoService.setInfo(existingHsn, gstinTransactionDTO.getUserBean());

					crudService.update(existingHsn);

				}

			}

		}catch (AppException ae) {

			throw ae;
		} 
		catch (Exception e) {

			_Logger.error("exception in hsn method ", e);
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		} 

		return true;

	}

	@Override
	public int processList(GstExcel gstExcel, String transaction, String gstReturn, TaxpayerGstin taxpayerGstin,
			String sheetName, String monthYear, SourceType source) throws AppException {
		// TODO Auto-generated method stub
		B2BDetailEntity b2bDetail = new B2BDetailEntity();
		ExcelError ee = new ExcelError();
		gstExcel.loadSheet(17);
        Sheet sheet=gstExcel.getSheet();
        Iterator<Row> iterator = gstExcel.getSheet().iterator();
        int columnCount=0;
       
		while (iterator.hasNext()) {
			Row row = iterator.next();
			int rowNum = row.getRowNum();
			int colNum = row.getLastCellNum();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) 
            {
            	Cell cell = cellIterator.next();
            	
            	if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                		if(cell.getStringCellValue().equals("Invoice date"))
                		{
                			isDate_index=true;
                			_INVOICE_DATE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Taxable Value"))
                		{
                			isTaxable_index=true;
                			_TAXABLE_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Type"))
                		{
                			isInvoice_type=true;
                			_INVOICE_TYPE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Place Of Supply"))
                		{
                			isSupply_index=true;
                			_SUPPLY_VALUE_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Number"))
                		{
                			isInvoice_number=true;
                			_INVOICE_NUMBER_INDEX=cell.getColumnIndex();
                		}
                		if(cell.getStringCellValue().equals("Invoice Value"))
                		{
                			isInvoice_value=true;
                			_INVOICE_VALUE_INDEX=cell.getColumnIndex();
                		}
            	}
            }
			int maxCell=  row.getLastCellNum();
			int max=1;
			if (rowNum == 0 ) {
				rowNum++;

				String cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(row.getPhysicalNumberOfCells())
						+ rowNum;
				List<String> values = gstExcel.excelReadRow(cellRange);
				if (!Objects.isNull(values))
					values.removeIf(item -> item == null || "".equals(item));
				if (Objects.nonNull(values))
					columnCount = values.size();
			} else if (rowNum > 0) {
				rowNum++;
				while(max>0 && max<=maxCell)
				{
    				cellRange = "A" + rowNum + ":" + gstExcel.getCharForNumber(max) + rowNum;
    				temp=gstExcel.excelReadRow(cellRange);
    				max++;
				}
				System.out.println("Inside HSN Transaction Data...");
				
				if(temp!=null) {
//					System.out.println(temp);
					if(ReturnType.GSTR1.toString().equals(gstReturn)) {
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(0), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(date);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
//	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
//		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(0).equals("b2b")) {
//    							System.out.print(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
    					
					}
				}
				}else if(ReturnType.GSTR2.toString().equals(gstReturn)) {
					System.out.println("Inside hsn gstr2");
					if(isDate_index) {
    					Date date=this.ValidateDate(ee,gstExcel.getWb().getSheetName(1), _INVOICE_DATE_INDEX, rowNum,temp.get(_INVOICE_DATE_INDEX));
    					if(date!=null) {
    						if(gstExcel.getWb().getSheetName(11).equals("13. HSN summary")) {
		    					b2bDetail.setOriginalInvoiceDate(date);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(date+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					if(isInvoice_number) {
						String invoice_number=this.ValidateInvoiceNumber(ee,gstExcel.getWb().getSheetName(0), _INVOICE_NUMBER_INDEX, rowNum,temp.get(_INVOICE_NUMBER_INDEX));
						if(invoice_number!=null) {
							if(gstExcel.getWb().getSheetName(11).equals("13. HSN summary")) {
	    						b2bDetail.setOriginalInvoiceNumber(invoice_number);
	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_number+" ");
							}
						}else {
							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_value) {
						double invoice_value=this.ValidateInvoiceValue(ee,gstExcel.getWb().getSheetName(0), _INVOICE_VALUE_INDEX, rowNum,temp.get(_INVOICE_VALUE_INDEX));
						if(invoice_value!=0L) {
							if(gstExcel.getWb().getSheetName(11).equals("13. HSN summary")) {
//	    						b2bDetail.setInvoiceValue(invoice_value);
//	        					b2bDetail.setAmmendment(true);
	    						System.out.print(invoice_value+" ");
							}
						}else {
//							System.out.println(ee.getErrorDesc());
						}
						
					}
					if(isInvoice_type) {
    					String invoiceType=this.ValidateInvoiceType(ee,gstExcel.getWb().getSheetName(0), _INVOICE_TYPE_INDEX, rowNum,temp.get(_INVOICE_TYPE_INDEX));
    					if(invoiceType!=null) {
    						if(gstExcel.getWb().getSheetName(11).equals("13. HSN summary")) {
		    					b2bDetail.setInvoiceType(invoiceType);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(invoiceType+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isTaxable_index) {
    					double taxable_value=this.ValidateTaxableValue(ee,gstExcel.getWb().getSheetName(0),_TAXABLE_VALUE_INDEX, rowNum,temp.get(_TAXABLE_VALUE_INDEX));
    					if(taxable_value!=0L) {
    						if(gstExcel.getWb().getSheetName(11).equals("13. HSN summary")) {
		    					b2bDetail.setTaxableValue(taxable_value);
		    					b2bDetail.setAmmendment(true);
		    					System.out.print(taxable_value+" ");
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
					
					if(isSupply_index) {
    					String place_of_supply=this.ValidatePlaceOfSupply(ee,"firstSheet", _SUPPLY_VALUE_INDEX, rowNum, temp.get(_SUPPLY_VALUE_INDEX));
    					if(place_of_supply!=null) {
    						if(gstExcel.getWb().getSheetName(11).equals("13. HSN summary")) {
    							System.out.println(place_of_supply);
    							b2bDetail.setPos(place_of_supply);
    							b2bDetail.setAmmendment(true);
    						}
    					}else {
    						System.out.println(ee.getErrorDesc());
    					}
					}
				}
			}
		}
		return 0;
	}
}
