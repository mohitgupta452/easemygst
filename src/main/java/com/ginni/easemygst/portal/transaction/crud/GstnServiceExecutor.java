package com.ginni.easemygst.portal.transaction.crud;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.spi.LoggerFactory;
import org.jfree.data.xy.TableXYDataset;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.service.MasterService.ReturnType;
import com.ginni.easemygst.portal.business.service.MasterService.TransactionType;
import com.ginni.easemygst.portal.business.entity.GstinTransactionDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerDTO;
import com.ginni.easemygst.portal.business.entity.TaxpayerGstinDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.ReturnsService;
import com.ginni.easemygst.portal.gst.response.GetGstr2Resp;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.EntityHelper;
import com.ginni.easemygst.portal.helper.JsonMapper;
import com.ginni.easemygst.portal.persistence.entity.TaxpayerGstin;
import com.ginni.easemygst.portal.persistence.entity.transaction.B2BTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.CDNTransactionEntity;
import com.ginni.easemygst.portal.persistence.entity.transaction.Item;
import com.ginni.easemygst.portal.transaction.factory.Transaction.DataSource;
import com.ginni.easemygst.portal.transaction.factory.Transaction.SourceType;
import com.ginni.easemygst.portal.transaction.impl.B2BTransaction;
import com.ginni.easemygst.portal.transaction.impl.CDNTransaction;
import com.ginni.easemygst.portal.utils.Utility;

import antlr.StringUtils;
import lombok.Synchronized;

public class GstnServiceExecutor {
	
	private final static Logger _LOGGER = org.slf4j.LoggerFactory.getLogger(GstnServiceExecutor.class);
	
	
	
	 public Integer processJsonData(InputStream is,String key,GstinTransactionDTO gstnDTO,TaxpayerGstin taxpayerGstin){
		 
		 
		 
		 
		    List<Integer> responseAfterUpload=new ArrayList<>();
			Integer result=null;
				try{
					String json=Utility.unzipData(is, key);
					GetGstr2Resp getResp=JsonMapper.objectMapper.readValue(json, GetGstr2Resp.class);
					result =new GstnServiceExecutor().insertUpdateGstnData(getResp, TransactionType.valueOf(gstnDTO.getTransactionType()),taxpayerGstin, gstnDTO.getMonthYear(), ReturnType.valueOf(gstnDTO.getReturnType()));
				}catch(Exception e){
					e.printStackTrace();
				}
				return result;
//			}
		}
	
	public synchronized int insertUpdateGstnData(GetGstr2Resp getGstr2Resp,TransactionType transactionType,TaxpayerGstin taxpayerGstin,String monthYear,ReturnType returnType) throws Exception{
		//ExecutorService executorService = Executors.newSingleThreadExecutor();

		_LOGGER.debug("gstn data insertion method start");
		if(getGstr2Resp != null){
//		executorService.execute(new Runnable() {
//
//			@Override
//			public void run() {
//				try {

					int gstr2ACount= saveData(getGstr2Resp, transactionType,taxpayerGstin,monthYear,returnType);
					
					
					GstinTransactionDTO gstinTransactionDto=new GstinTransactionDTO();
					gstinTransactionDto.setTaxpayerGstin((TaxpayerGstinDTO)EntityHelper.convert(taxpayerGstin, TaxpayerGstinDTO.class));
					
					gstinTransactionDto.setReturnType("GSTR2");
					gstinTransactionDto.setMonthYear(monthYear);
					gstinTransactionDto.setTransactionType(transactionType.toString());
					
					ReturnsService returnService;
					
					return gstr2ACount;
					
//				} catch (AppException e) {
//					e.printStackTrace();
//				}
//				
//			}
//		});
		}
		_LOGGER.debug("gstn data insertion method end");

		return 0;
			
	}

	private int saveData(GetGstr2Resp getGstr2Resp,TransactionType transactionType,TaxpayerGstin taxpayerGstin,String monthYear,ReturnType returnType) throws Exception{
		int count=0;
		
		GstinTransactionDTO gstinTransactionDto=new GstinTransactionDTO();
		gstinTransactionDto.setTaxpayerGstin((TaxpayerGstinDTO)EntityHelper.convert(taxpayerGstin, TaxpayerGstinDTO.class));
		gstinTransactionDto.setReturnType(returnType.toString());
		gstinTransactionDto.setMonthYear(monthYear);
		gstinTransactionDto.setTransactionType(transactionType.toString());
		
		switch(transactionType){
		case B2B:
			List<B2BTransactionEntity> b2bTransactionEntity=getGstr2Resp.getB2b();
			this.setDataSource(b2bTransactionEntity,taxpayerGstin,monthYear,returnType);
			   B2BTransaction b2bTransaction=new B2BTransaction();
			   String delete="no";
			   if(getGstr2Resp.getToken()!=null ){
				   if(!org.apache.commons.lang3.StringUtils.isEmpty(getGstr2Resp.getToken())){
			        if(getGstr2Resp.getIsDelete())
					  ///b2bTransaction.deleteTransaction(taxpayerGstin.getGstin(), monthYear, TransactionType.B2B, returnType, "YES",DataSource.GSTN);
			        	delete="gstn_yes";
				   }
			   }else{
		        	// b2bTransaction.deleteTransaction(taxpayerGstin.getGstin(), monthYear, TransactionType.B2B, returnType, "YES",DataSource.GSTN);
		        	delete="gstn_yes";
			   }
			 count= b2bTransaction.saveTransaction(b2bTransactionEntity,delete);
			_LOGGER.debug("b2b transaction updated {}", count);
			//returnService.compareTransactions(gstinTransactionDto);
			break;
		case CDN:
			List<CDNTransactionEntity> cdnTransactionEntities =getGstr2Resp.getCdn();
			if(cdnTransactionEntities != null){
			this.setDatSource(cdnTransactionEntities,taxpayerGstin,monthYear,returnType);
			CDNTransaction cdnTransaction=new CDNTransaction();
			if(getGstr2Resp.getToken()!=null ){
		        if(getGstr2Resp.getIsDelete())
		        	cdnTransaction.deleteTransaction(taxpayerGstin.getGstin(), monthYear, TransactionType.CDN,returnType, "YES",DataSource.GSTN);
		   }else{
					cdnTransaction.deleteTransaction(taxpayerGstin.getGstin(), monthYear, TransactionType.CDN,
							returnType, "YES", DataSource.GSTN);
		   }
			
			count =cdnTransaction.saveTransaction(cdnTransactionEntities);
			_LOGGER.debug("cdn transaction updated {}",count);
				//returnService.compareTransactions(gstinTransactionDto);
			}
			break;
		case CDNUR:
			break;
		default:
			break;
		}
		return count;
	}
	
	
	private void setDataSource(List<B2BTransactionEntity> b2bTransactionEntities,TaxpayerGstin taxpayerGstin,String monthYear,ReturnType returnType){
		if(b2bTransactionEntities !=null){
		b2bTransactionEntities.forEach(
				b2bTransaction->{
					b2bTransaction.setMonthYear(monthYear);
					b2bTransaction.setTaxpayerGstin(taxpayerGstin);
					b2bTransaction.setReturnType(ReturnType.GSTR2.toString());
			b2bTransaction.getB2bDetails().forEach(b2b->{
				b2b.setDataSource(DataSource.GSTN);
				b2b.setSource(SourceType.GSTN.toString());
				b2b.setB2bTransaction(b2bTransaction);
				double taxableValue=0.0;
				double taxAmount=0.0;

				for(Item item : b2b.getItems()){
					taxableValue+=item.getTaxableValue();
					taxAmount+=item.getIgst()+item.getCgst()+item.getCess()+item.getSgst();
			}
				b2b.setTaxAmount(taxAmount);
				b2b.setTaxableValue(taxableValue);
			});
		});}
	}
	
	private void setDatSource(List<CDNTransactionEntity> cdnTransactionEntities,TaxpayerGstin taxpayerGstin,String monthYear,ReturnType returnType){
		cdnTransactionEntities.forEach(cdnTransaction->{
			cdnTransaction.setMonthYear(monthYear);
			cdnTransaction.setGstin(taxpayerGstin);
			cdnTransaction.setReturnType(returnType.toString());
			cdnTransaction.getCdnDetails().forEach(cdn->{
				cdn.setDataSource(DataSource.GSTN);
				cdn.setSource(SourceType.GSTN.toString());
				cdn.setCdnTransaction(cdnTransaction);
				double taxableValue=0.0;
				double taxAmount=0.0;
				for(Item item : cdn.getItems()){
					taxableValue+=item.getTaxableValue();
					taxAmount+=item.getIgst()+item.getCgst()+item.getCess()+item.getSgst();

			}
				cdn.setTaxAmount(taxAmount);
				cdn.setTaxableValue(taxableValue);
				
			});
		});
	}

}
