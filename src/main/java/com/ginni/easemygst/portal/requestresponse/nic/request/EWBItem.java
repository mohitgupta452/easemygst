package com.ginni.easemygst.portal.requestresponse.nic.request;

import java.math.BigDecimal;

import lombok.Data;

public @Data class EWBItem {
	
	private String productName;
	private String productDesc;
	private int hsnCode;
	private BigDecimal quantity;
	private String qtyUnit;
	private BigDecimal cgstRate;
	private BigDecimal sgstRate;
	private BigDecimal igstRate;
	private BigDecimal cessRate;
	private BigDecimal cessAdvol;
	private BigDecimal taxableAmount;
}
