package com.ginni.easemygst.portal.requestresponse.nic.response;

import lombok.Data;

public @Data class EWBAuthtokenResponse extends EWBBaseResponse{
	
	private String authtoken;
	
	private String sek;

}
