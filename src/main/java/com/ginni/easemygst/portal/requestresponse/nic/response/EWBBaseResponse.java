package com.ginni.easemygst.portal.requestresponse.nic.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(value=Include.NON_NULL)
public @Data class EWBBaseResponse {
	
	private String status;
	
	private EWBErrorResponse errorResponse;
	
	private String data;
	
	private String error;
	
	
}
