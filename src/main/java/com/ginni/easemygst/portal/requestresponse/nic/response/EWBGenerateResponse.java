package com.ginni.easemygst.portal.requestresponse.nic.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(value=Include.NON_NULL)
public @Data class EWBGenerateResponse extends EWBBaseResponse {
	
	private String ewayBillNo;
	
	private String ewayBillDate;
	
	private String validUpto;
	
	private String referenceNumber;
	
	private String ewaybillUrl;

}
