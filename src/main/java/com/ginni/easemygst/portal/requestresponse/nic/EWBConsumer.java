package com.ginni.easemygst.portal.requestresponse.nic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ginni.easemygst.portal.business.service.EWayBillService.EWB_EVENT;
import com.ginni.easemygst.portal.client.RestClient;
import com.ginni.easemygst.portal.constant.EwayBillConstant;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.constant.GstMetadata;
import com.ginni.easemygst.portal.exception.EwayBillException;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBBaseRequest;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBRequestModel;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBBaseResponse;
import com.ginni.easemygst.portal.requestresponse.nic.response.EWBResponseProcessor;
import com.ginni.easemygst.portal.security.ExactEncrypt;
import com.ginni.easemygst.portal.security.nic.EWBEncyrptionDecryptionService;
import com.mashape.unirest.http.HttpMethod;

@RequestScoped
public class EWBConsumer {

	@Inject
	private EWBEncyrptionDecryptionService encyrptionDecryptionService;

	@Inject
	private RedisService redisService;
	
	@Inject
	private EWBResponseProcessor ewbResponseProcessor;
	
	private static final Logger _LOGGER=LoggerFactory.getLogger(EWBConsumer.class);   

	Properties ewbConfigProperties = null;

	public EWBConsumer() throws IOException {

		ewbConfigProperties = new Properties();
		ewbConfigProperties
				.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/ewbconfig.properties"));

	}

	public JsonNode callEWBAPI(EWBBaseRequest ewbBaseRequest) throws EwayBillException, JsonProcessingException, IOException, AppException{

		 String encResponse= EWBRestClient.call(this.generateEWBRequest(ewbBaseRequest));
		 
		 _LOGGER.info("enc response from nic {}",encResponse);
		 
		return ewbResponseProcessor.processResponseFromNIC(encResponse, ewbBaseRequest.getEvent(),ewbBaseRequest.getGstin());
	}

	private EWBRequestModel generateEWBRequest(EWBBaseRequest ewbBaseRequest) throws EwayBillException  {

		EWBRequestModel ewbRequestModel = new EWBRequestModel();

		this.generateEWBURL(ewbBaseRequest.getEvent(), ewbRequestModel);
		this.generateRequestHeaders(ewbRequestModel, ewbBaseRequest);
		this.generateRequestBody(ewbBaseRequest, ewbRequestModel);

		return ewbRequestModel;

	}

	private void encryptRequest(EWBBaseRequest ewbBaseRequest) throws EwayBillException  {

		try {
		encyrptionDecryptionService.encryptRequest(ewbBaseRequest);
		}
		catch (Exception exception){
			
			_LOGGER.error("exception while encrypt request ",exception);
			throw new EwayBillException(exception);
		}

	}

	private void generateRequestBody(EWBBaseRequest baseRequest, EWBRequestModel ewbRequestModel) throws EwayBillException  {

		this.encryptRequest(baseRequest);

		ObjectNode encRequestBody = (ObjectNode) baseRequest.getData();

		ewbRequestModel.setBody(encRequestBody.put("action", ewbRequestModel.getAction()));
	}

	private void generateRequestHeaders(EWBRequestModel ewbRequestModel, EWBBaseRequest baseRequest) {

		Map<String, String> headers = ewbRequestModel.getHeaders();

		headers.put("client-id", ewbConfigProperties.getProperty("client-id"));
		headers.put("client-secret", ewbConfigProperties.getProperty("client-secret"));
		headers.put("gstin", baseRequest.getGstin());// ewbConfigProperties.getProperty("cilent-id"));
		headers.putAll(this.exacttAuth());

		if (EWB_EVENT.AUTHENTICATE != baseRequest.getEvent())
			headers.put("authtoken",redisService.getValue(String.format(EwayBillConstant._AUTHTOKEN,baseRequest.getGstin())));

	}

	private String generateEWBURL(EWB_EVENT event, EWBRequestModel ewbRequestModel) {

		StringBuilder urlBuilder = new StringBuilder();
		
		urlBuilder.append(ewbConfigProperties.getProperty("url"));
		urlBuilder.append(ewbConfigProperties.getProperty(event.toString().toLowerCase() + "-api-version"));
		urlBuilder.append(ewbConfigProperties.getProperty(event.toString().toLowerCase() + "-api-path"));

		ewbRequestModel.setUrl(urlBuilder.toString());
		ewbRequestModel.setHttpMethod(HttpMethod.valueOf(StringUtils
				.upperCase(ewbConfigProperties.getProperty(event.toString().toLowerCase() + "-http-method"))));

		ewbRequestModel.setAction(ewbConfigProperties.getProperty(event.toString().toLowerCase() + "-action"));

		return null;
	}

	private Map<String, String> exacttAuth() {

		Map<String, String> outputHeaders = new HashMap<>();
		outputHeaders.put(GstMetadata.HEADER_KEY_GSP_REQUEST_ID, UUID.randomUUID().toString());
		// outputHeaders.put(GstMetadata.HEADER_KEY_GSP_ID,
		// config.getProperty("main.gst.excellon.subscription.id"));

		boolean isApiCallRequired = true;
		
		if (!Objects.isNull(redisService.getValue("exact-ewb-auth-token"))) {
			outputHeaders.put(GstMetadata.HEADER_KEY_GSP_AUTH_TOKEN, redisService.getValue("exact-ewb-auth-token"));
			isApiCallRequired = false;
		}

		if (isApiCallRequired) {
			String clientId = ewbConfigProperties.getProperty("client-id");
			String secureKey = ewbConfigProperties.getProperty("client-secret");
			String authUrl = "http://demoapi.exactgst.com/api/authentication/Authenticate"; // config.getProperty("main.gst.excellon.auth.url");

			String clientSecret = "";
			try {
				clientSecret = ExactEncrypt.encrypt(secureKey);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
				} catch (AppException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("ClientId", clientId);
			jsonObject.put("ClientSecret", clientSecret);

			Map<String, String> headers = new HashMap<>();
			headers.put("RequestId", UUID.randomUUID().toString());
			headers.put("Content-Type", "application/json");

			String output = RestClient.callApi(authUrl, "POST",
					new com.mashape.unirest.http.JsonNode(jsonObject.toString()), headers);
			JSONObject object = new JSONObject(output);

			if (!object.has("status_cd")) {
				if (object.has("IsAuthenticated") && object.getBoolean("IsAuthenticated")) {
					if (object.has("AuthenticationValidTillDateTime")) {
						String timing = object.getString("AuthenticationValidTillDateTime");
						/*
						 * Date dt = new Date(timing); Date dtNow = Calendar.getInstance().getTime();
						 * int timeDiff = (int) ((dt.getTime() - dtNow.getTime() - 60000) / 1000);
						 */

						redisService.setKeyValue("exact-ewb-auth-token", object.getString("AuthenticationToken"));

						outputHeaders.put(GstMetadata.HEADER_KEY_GSP_AUTH_TOKEN,
								object.getString("AuthenticationToken"));
					}
				}
			}
		}

		return outputHeaders;

	}

}
