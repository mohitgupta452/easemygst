package com.ginni.easemygst.portal.requestresponse;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBBaseRequest;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBItem;

import lombok.Data;

public @Data class EWBGenerateRequest extends EWBBaseRequest{
	
	private String referenceNumber;
	
    private String supplyType;
	private String subSupplyType;
	private String docType;
	private String docNo;
	private String docDate;
	private String fromGstin;
	private String fromTrdName;
	private String fromAddr1;
	private String fromAddr2;
	private String fromPlace;
	private String fromPincode;
	private String fromStateCode;
	private String toGstin;
	private String toTrdName;
	private String toAddr1;
	private String toAddr2;
	private String toPlace;
	private String toPincode;
	private String toStateCode;
	private java.math.BigDecimal totalValue;
	private java.math.BigDecimal cgstValue;
	private java.math.BigDecimal sgstValue;
	private java.math.BigDecimal igstValue;
	private java.math.BigDecimal cessValue;
    private String transporterId;
	private String transporterName;
	private String transDocNo;
	private String transMode;
	private String transDistance;
	private String transDocDate;
	private String vehicleNo;
	private String vehicleType;
	
	
	
	@JsonProperty("itemList")
	private List<EWBItem> ewbItems;

}
