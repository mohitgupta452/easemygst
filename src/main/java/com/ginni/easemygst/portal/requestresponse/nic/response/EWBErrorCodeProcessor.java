package com.ginni.easemygst.portal.requestresponse.nic.response;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EWBErrorCodeProcessor {
	
	private static final Logger _LOGGER=LoggerFactory.getLogger(EWBErrorCodeProcessor.class);

	private static final Properties _EWB_ERROR_CONFIG=new Properties();
	
	static {
		try {
		_EWB_ERROR_CONFIG.load(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("/ewberrorcode.properties"));
		}catch(IOException exception){
			_LOGGER.error("io exception while load ewb error config file ",exception);
			
		}
	}
	
	public static String getErrorMessage(String errorCode) {
		
	 String errorMessage=	_EWB_ERROR_CONFIG.getProperty(errorCode);
	 
	 if(StringUtils.isEmpty(errorMessage))
		 errorMessage="Unable to process your request.";
	 return errorMessage;
		
	}

}
