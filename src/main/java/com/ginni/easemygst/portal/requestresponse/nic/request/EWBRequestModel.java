package com.ginni.easemygst.portal.requestresponse.nic.request;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.mashape.unirest.http.HttpMethod;

import lombok.Data;

public @Data class EWBRequestModel {
	
	
	Map<String,String> headers= new HashMap<>();
	
	String url;
	
	HttpMethod httpMethod;
	
	String action;
	
	JsonNode body;

}
