package com.ginni.easemygst.portal.requestresponse.nic.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.ginni.easemygst.portal.business.service.EWayBillService.EWB_EVENT;

import lombok.Data;

public abstract @Data class EWBBaseRequest {
	
	@JsonIgnore
	private EWB_EVENT event;
	
	@JsonIgnore
	private JsonNode data;
	
	@JsonIgnore
	private  String gstin;
	
	@JsonIgnore
	private String authToken;
	
	@JsonIgnore 
	private byte[] sek;
	
}
