package com.ginni.easemygst.portal.requestresponse.nic.response;

import lombok.Data;

public @Data class EWBErrorResponse {
	
	private String errorCode;
	
	private String errorMessage;
}
