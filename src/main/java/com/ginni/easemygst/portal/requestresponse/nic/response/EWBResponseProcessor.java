package com.ginni.easemygst.portal.requestresponse.nic.response;

import java.io.IOException;
import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginni.easemygst.portal.business.service.EWayBillService.EWB_EVENT;
import com.ginni.easemygst.portal.constant.EwayBillConstant;
import com.ginni.easemygst.portal.exception.EwayBillException;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.security.nic.EWBEncyrptionDecryptionService;

@RequestScoped
public class EWBResponseProcessor {

	private static final Logger _LOGGER = LoggerFactory.getLogger(EWBResponseProcessor.class);

	private static final ObjectMapper _MAPPER = new ObjectMapper();

	private static final String SUCCESS_STATUS = "1";

	private static final String FAILURE_STATUS = "0";

	private static final int EWB_TOKEN_VALIDITY = 6 * 60 * 60;

	@Inject
	private EWBEncyrptionDecryptionService encyrptionDecryptionService;

	@Inject
	private RedisService redisService;

	public JsonNode processResponseFromNIC(String baseResponseStr, EWB_EVENT event, String gstin)
			throws EwayBillException, JsonProcessingException, IOException, AppException {

		if (!StringUtils.isEmpty(baseResponseStr)) {

			EWBBaseResponse baseResponse=null;
			JsonNode responseJson=null;
			
			try {
				 responseJson=_MAPPER.readTree(baseResponseStr);
			} catch (IOException e) {
				
				throw new EwayBillException(e);
			}
			
			if(EWB_EVENT.AUTHENTICATE !=event)
			 baseResponse = _MAPPER.convertValue(responseJson, EWBBaseResponse.class);
			else
				 baseResponse = _MAPPER.convertValue(responseJson, EWBAuthtokenResponse.class);

			if (!Objects.isNull(baseResponse) && !StringUtils.isEmpty(baseResponse.getStatus())) {

				String status = baseResponse.getStatus();

				String response=null;
				if (SUCCESS_STATUS.equalsIgnoreCase(status)) {

					if (EWB_EVENT.AUTHENTICATE == event) {

						EWBAuthtokenResponse authtokenResponse = (EWBAuthtokenResponse) baseResponse;

						this.updateEWBSekAndAuthtoken(gstin, authtokenResponse.getAuthtoken(),
								authtokenResponse.getSek());
						return null;
					} else {
						 try {
							response=encyrptionDecryptionService.decryptResponse(baseResponse.getData(), gstin);
						} catch (AppException e) {
							
							throw new EwayBillException(e);
							
						}
						
						_LOGGER.debug("nic response gstin :{}  response : {}",gstin,response);
						
					return	_MAPPER.readTree(response);
					
					}
				} else {
					if (!Objects.isNull(baseResponse.getError())) {

						String encErrorCode = baseResponse.getError();

						String error = encyrptionDecryptionService.decryptErrorCode(encErrorCode);
						
						String errorCode=_MAPPER.readTree(error).get("errorCodes").asText();
						
						String errorMessage = EWBErrorCodeProcessor.getErrorMessage(errorCode);

						throw new EwayBillException(errorCode, errorMessage);
					}

				}

			}

		}
		throw new EwayBillException();

	}

	private void updateEWBSekAndAuthtoken(String gstin, String authtoken, String sek) {

		redisService.setValue(String.format(EwayBillConstant._SEK,gstin), sek, true, EWB_TOKEN_VALIDITY);
		redisService.setValue(String.format(EwayBillConstant._AUTHTOKEN,gstin), authtoken, true, EWB_TOKEN_VALIDITY);
	}

}
