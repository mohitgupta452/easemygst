package com.ginni.easemygst.portal.requestresponse.nic;


import java.util.Objects;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.exception.EwayBillException;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.requestresponse.nic.request.EWBRequestModel;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;

public class EWBRestClient {
	
	private static final  Logger _LOGGER=org.slf4j.LoggerFactory.getLogger(EWBRestClient.class); 
	
	public static String call(EWBRequestModel ewbRequestModel) throws EwayBillException {
		
		HttpMethod httpMethod = ewbRequestModel.getHttpMethod();
		
		HttpRequestWithBody request=null;
		
		     if(HttpMethod.POST==httpMethod) {
		
	          request=Unirest.post(ewbRequestModel.getUrl());
		
	           request.body(ewbRequestModel.getBody().toString());
	           
	           request.headers(ewbRequestModel.getHeaders());
	          }
		   _LOGGER.info("ewb request to nic {}",ToStringBuilder.reflectionToString(request));
		   
		   if(!Objects.isNull(request)) {
			   
			   try {
				HttpResponse<String> response=request.asString();
				_LOGGER.info("ewb response from nic {}",ToStringBuilder.reflectionToString(response));
				
				return response.getBody();
				
			} catch (UnirestException e) {
				_LOGGER.error("Unirest excetion while reading ewb response ",e);
			  throw new EwayBillException();
			}
		   }
		 return null;    
		     
	}

}
