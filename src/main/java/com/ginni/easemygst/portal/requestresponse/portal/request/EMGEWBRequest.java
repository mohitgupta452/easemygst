package com.ginni.easemygst.portal.requestresponse.portal.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ginni.easemygst.portal.requestresponse.EWBGenerateRequest;

import lombok.Data;

public @Data class EMGEWBRequest {
	
	@JsonProperty("data")
	private List<EWBGenerateRequest> ewbGenerateRequests;

}
