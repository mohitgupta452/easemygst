package com.ginni.easemygst.portal.requestresponse.nic.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class EWBAuthRequest extends EWBBaseRequest{
	
	String username;
	
	String password;
	
	@JsonProperty("app_key")
	String appKey;
	
	@JsonIgnore
	byte [] appKeyBytes;
}
