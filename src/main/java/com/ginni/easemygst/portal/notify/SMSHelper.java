package com.ginni.easemygst.portal.notify;

import java.util.Map;

public interface SMSHelper {

	void smsSecureCode(Map<String, String> params);
	
	void smsPassword(Map<String, String> params);
	
}
