package com.ginni.easemygst.portal.notify;

import java.util.Map;

public class SMSFactory implements SMSHelper {
	
	SMSHelper smsHelper;
	
	String smsFunction = "kapsystem";
	
	public enum SMS {

		KAP_SYSTEM("kapsystem");

		SMS(String channel) {
			this.channel = channel;
		}

		private String channel;

		@Override
		public String toString() {
			return this.channel;
		};
	};

	public SMSFactory() {
		if(smsFunction.equals(SMS.KAP_SYSTEM.toString())) {
			smsHelper = new SMSRequest();
		} 
	}

	@Override
	public void smsSecureCode(Map<String, String> params) {
		smsHelper.smsSecureCode(params);
	}

	@Override
	public void smsPassword(Map<String, String> params) {
		smsHelper.smsPassword(params);
	}
	
}
