package com.ginni.easemygst.portal.notify;

import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SMSRequest implements SMSHelper {

	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();
	
	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);
		
	static Properties configFile = new Properties();
	
	String url = "http://123.63.33.43/blank/sms/user/urlsmstemp.php?username=ginnisys&pass=Ginesys@90&senderid=EMYGST&response=Y";
	
	public void smsSecureCode(Map<String, String> params) {
		
		log.info(CLASS_NAME + "smsSecureCode method starts...");
		
		//String SMS_MESSAGE = "Your EASEMYGST otp authentication code is %s";
		String TEMPLATE_ID = "51275";
		
    	String code = params.get("code");
    	String toNumber = params.get("toNumber");
    	
    	//SMS_MESSAGE = String.format(SMS_MESSAGE, code);
    	
		String finalUrl = url+"&tempid="+TEMPLATE_ID+"&F1="+code+"&dest_mobileno="+toNumber;
		
		log.debug(finalUrl);
		
		try {
			CustomHTTPRequest.sendGet(finalUrl);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		
		log.info(CLASS_NAME + "smsSecureCode method ends...");
		
	}

	@Override
	public void smsPassword(Map<String, String> params) {
		
		log.info(CLASS_NAME + "smsPassword method starts...");
		
		String SMS_MESSAGE = "Your EASEMYGST password has been reset succussfully to %s";
		String TEMPLATE_ID = "51276";
		
    	String password = params.get("password");
    	String toNumber = params.get("toNumber");
    	
    	SMS_MESSAGE = String.format(SMS_MESSAGE, password);
    	
		String finalUrl = url+"&tempid="+TEMPLATE_ID+"&F1="+password+"&dest_mobileno="+toNumber;
		
		try {
			CustomHTTPRequest.sendGet(finalUrl);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		
		log.info(CLASS_NAME + "smsSecureCode method ends...");
		
	}
 
	
	
}
