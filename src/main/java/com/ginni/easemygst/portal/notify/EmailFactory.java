package com.ginni.easemygst.portal.notify;

import java.util.Map;

import com.ginni.easemygst.portal.business.entity.EmailDTO;

public class EmailFactory implements EmailHelper {

	EmailHelper emailHelper;

	String emailFunction = "legacy";

	public enum EMAIL {

		GOOGLE_APPS("google_apps"), LEGACY("legacy");

		EMAIL(String channel) {
			this.channel = channel;
		}

		private String channel;

		@Override
		public String toString() {
			return this.channel;
		};
	};

	public EmailFactory() {
		if (emailFunction.equals(EMAIL.LEGACY.toString())) {
			emailHelper = new LegacyEmail();
		} else if (emailFunction.equals(EMAIL.GOOGLE_APPS.toString())) {
			emailHelper = new GoogleAppsEmail();
		}
	}

	@Override
	public boolean sendWelcomeUserEmail(Map<String, String> params) {
		return emailHelper.sendWelcomeUserEmail(params);
	}

	@Override
	public boolean sendWelcomePartnerEmail(Map<String, String> params) {
		return emailHelper.sendWelcomePartnerEmail(params);
	}

	@Override
	public boolean sendMailActivationEmail(Map<String, String> params) {
		return emailHelper.sendMailActivationEmail(params);
	}

	@Override
	public boolean sendPasswordChangedEmail(Map<String, String> params) {
		return emailHelper.sendPasswordChangedEmail(params);
	}

	@Override
	public boolean sendAddPanUserEmail(Map<String, String> params) {
		return emailHelper.sendAddPanUserEmail(params);
	}

	@Override
	public boolean sendDeletePanUserEmail(Map<String, String> params) {
		return emailHelper.sendDeletePanUserEmail(params);
	}

	@Override
	public boolean sendAddGstinEmail(Map<String, String> params) {
		return emailHelper.sendAddGstinEmail(params);
	}

	@Override
	public boolean sendAddGstinUserEmail(Map<String, String> params) {
		return emailHelper.sendAddGstinUserEmail(params);
	}

	@Override
	public boolean sendDeleteGstinUserEmail(Map<String, String> params) {
		return emailHelper.sendDeleteGstinUserEmail(params);
	}

	@Override
	public boolean sendPartnerSubscribedEmail(Map<String, String> params) {
		return emailHelper.sendPartnerSubscribedEmail(params);
	}

	@Override
	public boolean sendPartnerVerificationEmail(Map<String, String> params) {
		return emailHelper.sendPartnerVerificationEmail(params);
	}

	@Override
	public boolean sendContactUsFormEmail(Map<String, String> params) {
		return emailHelper.sendContactUsFormEmail(params);
	}

	@Override
	public boolean sendPartnerFormEmail(Map<String, String> params) {
		return emailHelper.sendPartnerFormEmail(params);
	}

	@Override
	public boolean sendGstReadinessFormEmail(Map<String, String> params) {
		return emailHelper.sendGstReadinessFormEmail(params);
	}

	@Override
	public boolean sendEmailUsingEmailDTO(EmailDTO emailDTO) {
		return emailHelper.sendEmailUsingEmailDTO(emailDTO);
	}

	@Override
	public boolean sendNewUserTempEmail(Map<String, String> params) {
		return emailHelper.sendNewUserTempEmail(params);
	}
	
	@Override
	public boolean sendNewUserAccountForActivationEmail(Map<String, String> params) {
		return emailHelper.sendNewUserAccountForActivationEmail(params);
	}

	@Override
	public boolean sendGSTRApproachingEmail(Map<String, String> params) {
		return emailHelper.sendGSTRApproachingEmail(params);
	}

	@Override
	public boolean sendGSTRDiscrepancyEmail(Map<String, String> params) {
		return emailHelper.sendGSTRDiscrepancyEmail(params);
	}

	@Override
	public boolean sendGSTRPreviousPeriodEmail(Map<String, String> params) {
		return emailHelper.sendGSTRPreviousPeriodEmail(params);
	}

	@Override
	public boolean sendGSTRFiledEmail(Map<String, String> params) {
		return emailHelper.sendGSTRFiledEmail(params);
	}

	@Override
	public boolean sendGSTRGeneratedEmail(Map<String, String> params) {
		return emailHelper.sendGSTRGeneratedEmail(params);
	}

	@Override
	public boolean sendTaxpayerVendorEmail(Map<String, String> params) {
		return emailHelper.sendTaxpayerVendorEmail(params);
	}
	
	@Override
	public boolean sendTaxpayerVendorFollowEmail(Map<String, String> params) {
		return emailHelper.sendTaxpayerVendorFollowEmail(params);
	}
	
	@Override
	public boolean sendInvoiceEmail(Map<String, String> params, EmailDTO emailDTO) {
		return emailHelper.sendInvoiceEmail(params, emailDTO);
	}

}
