package com.ginni.easemygst.portal.notify;

import java.util.Map;

import com.ginni.easemygst.portal.business.entity.EmailDTO;

public interface EmailHelper {

	boolean sendWelcomePartnerEmail(Map<String,String> params);
	
	boolean sendPartnerSubscribedEmail(Map<String,String> params);
	
	boolean sendPartnerVerificationEmail(Map<String,String> params);
	
	boolean sendWelcomeUserEmail(Map<String,String> params);
	
	boolean sendMailActivationEmail(Map<String,String> params);
	
	boolean sendPasswordChangedEmail(Map<String,String> params);
	
	boolean sendAddPanUserEmail(Map<String,String> params);
	
	boolean sendDeletePanUserEmail(Map<String,String> params);

	boolean sendAddGstinEmail(Map<String,String> params);
	
	boolean sendAddGstinUserEmail(Map<String,String> params);
	
	boolean sendDeleteGstinUserEmail(Map<String,String> params);
	
	boolean sendContactUsFormEmail(Map<String,String> params);
	
	boolean sendPartnerFormEmail(Map<String,String> params);
	
	boolean sendGstReadinessFormEmail(Map<String,String> params);
	
	boolean sendEmailUsingEmailDTO(EmailDTO emailDTO);
	
	boolean sendNewUserTempEmail(Map<String,String> params);
	
	boolean sendGSTRApproachingEmail(Map<String,String> params);
	
	boolean sendGSTRDiscrepancyEmail(Map<String,String> params);
	
	boolean sendGSTRPreviousPeriodEmail(Map<String,String> params);
	
	boolean sendGSTRFiledEmail(Map<String,String> params);
	
	boolean sendGSTRGeneratedEmail(Map<String,String> params);
	
	boolean sendTaxpayerVendorEmail(Map<String, String> params);
	
	boolean sendNewUserAccountForActivationEmail(Map<String, String> params);
	
	boolean sendTaxpayerVendorFollowEmail(Map<String, String> params);
	
	boolean sendInvoiceEmail(Map<String, String> params, EmailDTO emailDTO);
	
}
