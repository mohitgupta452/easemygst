package com.ginni.easemygst.portal.notify;

import java.util.Map;

import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.client.UrlFetch;

public class GoogleAppsEmail implements EmailHelper {
	
	private String EMAIL_URL = "https://script.google.com/macros/s/AKfycbwyH3Y3iQT92lW_q2ixlUC_NhvH-Wp0XKVBe6KaZ1OgDg1sU_ST/exec?";
	
	public enum EMAIL_FUNC {

		WELCOME_USER("WELCOME_USER"),
		WELCOME_PARTNER("WELCOME_PARTNER"),
		PARTNER_SUBSCRIBED("PARTNER_SUBSCRIBED"),
		PARTNER_EMAIL_CODE("PARTNER_EMAIL_CODE"),
		EMAIL_ACTIVATION("EMAIL_ACTIVATION"),
		ADD_PAN("ADD_PAN"),
		ADD_PAN_USER("ADD_PAN_USER"),
		DELETE_PAN_USER("DELETE_PAN_USER"),
		ADD_GSTIN("ADD_GSTIN"),
		ADD_GSTIN_USER("ADD_GSTIN_USER"),
		PARTNER_FORM("PARTNER_FORM"),
		READINESS_FORM("READINESS_FORM"),
		CONTACT_US_FORM("CONTACT_US_FORM"),
		DELETE_GSTIN_USER("DELETE_GSTIN_USER");
		
		EMAIL_FUNC(String function) {
			this.function = function;
		}

		private String function;

		@Override
		public String toString() {
			return this.function;
		};
	};

	private String getMapEntries(Map<String, String> params) {
		String str = "";
		for (Map.Entry<String, String> entry : params.entrySet()) {
		    str += entry.getKey()+"="+entry.getValue()+"&";
		}
		return str;
	}
	
	@Override
	public boolean sendWelcomeUserEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.WELCOME_USER.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendWelcomePartnerEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.WELCOME_PARTNER.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendMailActivationEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.EMAIL_ACTIVATION.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendPasswordChangedEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.ADD_PAN.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendAddPanUserEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.ADD_PAN_USER.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendDeletePanUserEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.DELETE_PAN_USER.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendAddGstinEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.ADD_GSTIN.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendAddGstinUserEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.ADD_GSTIN_USER.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendDeleteGstinUserEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.DELETE_GSTIN_USER.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendPartnerSubscribedEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.PARTNER_SUBSCRIBED.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendPartnerVerificationEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.PARTNER_EMAIL_CODE.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendContactUsFormEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.CONTACT_US_FORM.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendPartnerFormEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.PARTNER_FORM.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendGstReadinessFormEmail(Map<String, String> params) {
		String urlParam = getMapEntries(params);
		urlParam += "funcKey="+EMAIL_FUNC.READINESS_FORM.toString();
		UrlFetch.callURL(EMAIL_URL+urlParam);
		return true;
	}

	@Override
	public boolean sendEmailUsingEmailDTO(EmailDTO emailDTO) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean sendNewUserTempEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendGSTRApproachingEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendGSTRDiscrepancyEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendGSTRPreviousPeriodEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendGSTRFiledEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendGSTRGeneratedEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendTaxpayerVendorEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean sendTaxpayerVendorFollowEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendInvoiceEmail(Map<String, String> params, EmailDTO emailDTO) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendNewUserAccountForActivationEmail(Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

}
