package com.ginni.easemygst.portal.notify;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ginni.easemygst.portal.business.entity.EmailDTO;
import com.ginni.easemygst.portal.utils.Utility;
import com.sendgrid.Attachments;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

public class LegacyEmail implements EmailHelper {

	private final String API_KEY = "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";
	
	private Logger log = Logger.getLogger(getClass().getName());

	@SuppressWarnings("unused")
	private final static String TEXT_PLAIN_EMAIL = "text/plain";

	private final static String HTML_EMAIL = "text/html";

	Properties emailConfig;

	Email from;

	public LegacyEmail() {
		emailConfig = new Properties();
		from = new Email("no-reply@easemygst.com", "EaseMyGST-NoReply");
		try {
			emailConfig.load(
					MethodHandles.lookup().lookupClass().getClassLoader().getResourceAsStream("email.properties"));
		} catch (IOException e) {

		}
	}

	public static Map<String, String> regularExpressionconversion(String body, Map<String, String> params) {
		Pattern p = Pattern.compile("\\[(.*?)\\]");
		Matcher m = p.matcher(body);
		Map<String, String> keys = new HashMap<>();
		while (m.find()) {
			StringBuffer sb = new StringBuffer();
			boolean isFirst = true;
			for (String s : m.group(1).split(" ")) {
				if (isFirst) {
					isFirst = false;
					sb.append(s.toLowerCase());
				} else {
					String temp = s.toLowerCase();
					if (temp.length() > 0) {
						String first = temp.substring(0, 1);
						String last = temp.substring(1);
						sb.append(first.toUpperCase());
						sb.append(last.toLowerCase());
					}
				}

			}
			keys.put("[" + m.group(1) + "]", sb.toString());
		}
		return keys;
	}

	public boolean sendMail(Mail mail) {
		SendGrid sg = new SendGrid(API_KEY);
		Request request = new Request();
		Response response = null;
		try {
			request.setMethod(Method.POST);
			request.setBaseUri("api.sendgrid.com");
			request.setEndpoint("/v3/mail/send");
			request.addHeader("content-type", "application/json; text/html; charset=utf-8");
			request.addHeader("Authorization", "Bearer " + API_KEY);
			request.setBody(mail.build());
			log.debug("body {} "+mail.build());
			log.debug("request {} "+request.toString());
         	response = sg.makeCall(request);
			if (Objects.isNull(response))
				return false;
			if (response.statusCode == 202) {
				log.debug("status code {} "+response.statusCode);
				return true;
			} else {
				log.debug("status code {} "+response.statusCode);
				return false;
			}
		} catch (IOException ex) {
			log.debug("exception sending email {} "+ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean sendWelcomeUserEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("WELCOME_USER_EMAIL");
		String subject = emailConfig.getProperty("WELCOME_USER_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));
		Content content = new Content(HTML_EMAIL, body);

		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendWelcomePartnerEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("WELCOME_PARTNER_EMAIL");
		String subject = emailConfig.getProperty("WELCOME_PARTNER_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendMailActivationEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("EMAIL_ACTIVATION");
		String subject = emailConfig.getProperty("EMAIL_ACTIVATION_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);

		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendPasswordChangedEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("PASSWORD_CHANGED_EMAIL");
		String subject = emailConfig.getProperty("PASSWORD_CHANGED_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));
		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendAddPanUserEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("ADD_PAN_USER_EMAIL");
		String subject = emailConfig.getProperty("ADD_PAN_USER_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendDeletePanUserEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("DELETE_PAN_USER_EMAIL");
		String subject = emailConfig.getProperty("DELETE_PAN_USER_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendAddGstinEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("ADD_GSTIN_EMAIL");
		String subject = emailConfig.getProperty("ADD_GSTIN_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);

		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendAddGstinUserEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("ADD_GSTIN_USER_EMAIL");
		String subject = emailConfig.getProperty("ADD_GSTIN_USER_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendDeleteGstinUserEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("DELETE_GSTIN_USER_EMAIL");
		String subject = emailConfig.getProperty("DELETE_GSTIN_USER_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendPartnerSubscribedEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("PARTNER_SUBSCRIBED_EMAIL");
		String subject = emailConfig.getProperty("PARTNER_SUBSCRIBED_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendPartnerVerificationEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("PARTNER_EMAIL_VERIFICATION_CODE");
		String subject = emailConfig.getProperty("PARTNER_EMAIL_VERIFICATION_CODE_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendContactUsFormEmail(Map<String, String> params) {
		String subject="";
		String body="";
		
		if(params.get("title").equalsIgnoreCase("REQUEST_A_DEMO")){
			 subject ="Receive a demo | "+params.get("firstName")+" | Source - "+params.get("source");
			try {
			 body = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("mailer/receive-demo-request.html"),"UTF-8"); 
				Map<String, String> keys = regularExpressionconversion(body, params);
				for (Map.Entry<String, String> entry : keys.entrySet()) {
					if (params.containsKey(entry.getValue()))
						if (!StringUtils.isEmpty(params.get(entry.getValue())))
							body = body.replace(entry.getKey(), params.get(entry.getValue()));
				}
				log.debug("html body {}"+body);
				this.sendRequestAdemo(params);				
			} catch (IOException e) {
				log.debug("reading file from mailer{}"+e.getMessage());
				e.printStackTrace();
			}
		    
		}
		else
		{
	 body = emailConfig.getProperty("CONTACT_US_FORM_EMAIL");
    subject = emailConfig.getProperty("CONTACT_US_FORM_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				if (!StringUtils.isEmpty(params.get(entry.getValue())))
					body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}
		}
		Email to = new Email(params.get("toEmail"));
		Email cc = new Email(params.get("ccEmail"));
		

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);
		mail.personalization.get(0).addCc(cc);
	
//		if (params.containsKey("ccEmail")) {
//			Email cc = new Email(params.get("ccEmail"));
//			mail.personalization.get(0).addCc(cc);
//		}

		return sendMail(mail);
	}
	
	public boolean sendRequestAdemo(Map<String, String> params){
		
		String subject="";
		String body="";
		
		if(params.get("title").equalsIgnoreCase("REQUEST_A_DEMO")){
			 subject ="Request a demo";
		    try {
				 body = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("mailer/request-a-demo.html"),"UTF-8"); 
				Email to = new Email(params.get("emailAddress"));
				Content content = new Content(HTML_EMAIL, body);
				Mail mail = new Mail(from, subject, to, content);
				return sendMail(mail);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	

	@Override
	public boolean sendPartnerFormEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("PARTNER_FORM_EMAIL");
		String subject = emailConfig.getProperty("PARTNER_FORM_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendGstReadinessFormEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("READINESS_FORM_EMAIL");
		String subject = emailConfig.getProperty("READINESS_FORM_SUBJECT");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendInvoiceEmail(Map<String, String> params, EmailDTO emailDTO) {

		Mail mail = null;
		Email cc = null;
		Attachments attachments = null;

		String body = emailConfig.getProperty("INVOICE_EMAIL");
		String subject = emailDTO.getMailsubject();

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}
		Content content = new Content(HTML_EMAIL, body);

		if (!StringUtils.isEmpty(body) && !StringUtils.isEmpty(subject) && !StringUtils.isEmpty(emailDTO.getMailto())) {
			if (Utility.validateEmail(emailDTO.getMailto())) {

				Email to = new Email(emailDTO.getMailto());

				mail = new Mail(from, subject, to, content);

				if (!StringUtils.isEmpty(emailDTO.getAttachments())) {

					String attachmentFile = emailDTO.getAttachments();

					File file = new File(attachmentFile);
					byte[] fileData = null;
					try {
						fileData = org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(file));
					} catch (IOException ex) {

					}
					attachments = new Attachments();
					Base64 x = new Base64();
					String encodedString = x.encodeToString(fileData);
					attachments.setContent(encodedString);
					attachments.setType("application/vnd.ms-excel");// "application/pdf"
					attachments.setContentId("ExcelData");
					attachments.setDisposition("inline");
					attachmentFile = file.getName();
					String name = attachmentFile.contains(":") ? attachmentFile.split(":")[1] : attachmentFile;
					attachments.setFilename(name);
					mail.addAttachments(attachments);

				}

				if (!StringUtils.isEmpty(emailDTO.getMailcc())
						&& !emailDTO.getMailto().equalsIgnoreCase(emailDTO.getMailcc())) {
					if (Utility.validateEmail(emailDTO.getMailcc())) {
						cc = new Email(emailDTO.getMailcc());
						mail.personalization.get(0).addCc(cc);
					}
				}
			}
		}

		return Objects.isNull(mail) ? false : sendMail(mail);
	}

	@Override
	public boolean sendEmailUsingEmailDTO(EmailDTO emailDTO) {
		Mail mail = null;
		Email cc = null;
		Email bcc = null;
		Attachments attachments;
		String body = emailDTO.getMailbody();
		String subject = emailDTO.getMailsubject();
		Email to = new Email(emailDTO.getMailto());
		if (!StringUtils.isEmpty(emailDTO.getRemarks())) {
			body = body.replace("[REMARK]", emailDTO.getRemarks());
		} else {
			body = body.replace("[REMARK]", "");
		}
		Content content = new Content(HTML_EMAIL, body);
		if (!StringUtils.isEmpty(body) && !StringUtils.isEmpty(subject) && !StringUtils.isEmpty(emailDTO.getMailto())) {
			if (Utility.validateEmail(emailDTO.getMailto())) {
				mail = new Mail(from, subject, to, content);
				if (!StringUtils.isEmpty(emailDTO.getAttachments())) {

					String attachmentFile = emailDTO.getAttachments();

					File file = new File(attachmentFile);
					byte[] fileData = null;
					try {
						fileData = org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(file));
					} catch (IOException ex) {

					}
					attachments = new Attachments();
					Base64 x = new Base64();
					String encodedString = x.encodeToString(fileData);
					attachments.setContent(encodedString);
					attachments.setType("application/vnd.ms-excel");// "application/pdf"
					attachments.setContentId("ExcelData");
					attachments.setDisposition("inline");
					attachmentFile = file.getName();
					String name = attachmentFile.contains(":") ? attachmentFile.split(":")[1] : attachmentFile;
					attachments.setFilename(name);
					mail.addAttachments(attachments);

				}
				if (!StringUtils.isEmpty(emailDTO.getMailcc())
						&& !emailDTO.getMailto().equalsIgnoreCase(emailDTO.getMailcc())) {
					if (Utility.validateEmail(emailDTO.getMailcc())) {
						cc = new Email(emailDTO.getMailcc());
						mail.personalization.get(0).addCc(cc);
					}
				}
				if (!StringUtils.isEmpty(emailDTO.getMailbcc())
						&& !emailDTO.getMailto().equalsIgnoreCase(emailDTO.getMailbcc())) {
					if (Utility.validateEmail(emailDTO.getMailbcc())) {
						bcc = new Email(emailDTO.getMailbcc());
						mail.personalization.get(0).addBcc(bcc);
					}
				}
			}
		}

		return Objects.isNull(mail) ? false : sendMail(mail);
	}

	@Override
	public boolean sendNewUserAccountForActivationEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("NEW_USER_ACCOUNT_ACTIVATION");
		String subject = emailConfig.getProperty("NEW_USER_ACCOUNT_ACTIVATION_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendNewUserTempEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("NEW_USER_TEMP");
		String subject = emailConfig.getProperty("NEW_USER_TEMP_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendGSTRApproachingEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("GSTR_APPROACHNG");
		String subject = emailConfig.getProperty("GSTR_APPROACHNG_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendGSTRDiscrepancyEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("GSTR_DISCREPANCY");
		String subject = emailConfig.getProperty("GSTR_DISCREPANCY_SUB");

		subject = subject.replace("[Value]", params.get("value"));

		Email to = new Email(params.get("toEmail"));

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendGSTRPreviousPeriodEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("GSTR_PREVIOUS_PERIOD");
		String subject = emailConfig.getProperty("GSTR_PREVIOUS_PERIOD_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendGSTRFiledEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("GSTR_FILED");
		String subject = emailConfig.getProperty("GSTR_FILED_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendGSTRGeneratedEmail(Map<String, String> params) {
		String body = emailConfig.getProperty("GSTR_GENERATED");
		String subject = emailConfig.getProperty("GSTR_GENERATED_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);
		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendTaxpayerVendorEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("TAXPAYER_VENDOR_EMAIL");
		String subject = emailConfig.getProperty("TAXPAYER_VENDOR_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);

		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}

	@Override
	public boolean sendTaxpayerVendorFollowEmail(Map<String, String> params) {

		String body = emailConfig.getProperty("TAXPAYER_VENDOR_FOLLOW_EMAIL");
		String subject = emailConfig.getProperty("TAXPAYER_VENDOR_FOLLOW_SUB");

		Map<String, String> keys = regularExpressionconversion(body, params);
		for (Map.Entry<String, String> entry : keys.entrySet()) {
			if (params.containsKey(entry.getValue()))
				body = body.replace(entry.getKey(), params.get(entry.getValue()));
		}

		Email to = new Email(params.get("toEmail"));

		Content content = new Content(HTML_EMAIL, body);

		Mail mail = new Mail(from, subject, to, content);

		return sendMail(mail);
	}
	
	public boolean sendToEMGSupport(String body,String subject) {
		
		Email to= new Email("emgst@turningcloud.com");

		//Email to= new Email("esupport@turningcloud.com"); previous
		Content content= new Content(TEXT_PLAIN_EMAIL, body);
		
		Mail mail = new Mail(from, "GSTN SYNC ERROR DESCRIPTION - "+subject, to, content);
		
		return sendMail(mail);
	}

}
