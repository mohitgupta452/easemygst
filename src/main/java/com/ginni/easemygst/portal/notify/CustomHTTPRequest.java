package com.ginni.easemygst.portal.notify;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;

public class CustomHTTPRequest {

	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	private static final String USER_AGENT = "Mozilla/5.0";

	protected static Logger log = LoggerFactory.getLogger(CLASS_NAME);

	// HTTP GET request
	public static String sendGet(String url) throws Exception {

		log.info(CLASS_NAME + "sendGet method starts...");
		if (!StringUtils.isEmpty(url)) {
			url = url.replaceAll("\\ ", "%20");
			log.debug(CLASS_NAME + "sendGet final url is {}", url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			log.debug("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			log.info(CLASS_NAME + "sendGet method ends...");

			return response.toString();
		} else {
			throw new Exception(AppException.getMessage(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST));
		}

	}

	// HTTP POST request
	public static String sendPost(String url, String urlParam) throws Exception {

		log.info(CLASS_NAME + "sendPost method starts...");
		if (!StringUtils.isEmpty(url)) {
			url = url.replaceAll("\\ ", "%20");
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParam);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			log.debug("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			log.info(CLASS_NAME + "sendPost method ends...");

			return response.toString();
		} else {
			throw new Exception(AppException.getMessage(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST));
		}

	}

	public static void main(String args[]) {
		String str = "http://123.63.33.43/blank/sms/user/urlsmstemp.php?username=ginnisys&pass=Ginesys@90&senderid=EMYGST&response=Y&message=Your EASEMYGST otp authentication code is 325635&dest_mobileno=9643101022";
		System.out.println(str.replaceAll("\\ ", "%20"));
	}

}
