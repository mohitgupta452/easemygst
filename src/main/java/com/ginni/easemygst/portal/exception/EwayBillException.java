package com.ginni.easemygst.portal.exception;

public class EwayBillException extends Exception{

	private static final long serialVersionUID = 5822677512785435884L;

	private String message="Unable to process your request, please contact suppport team";
	
	private String errorCode="1001";
	
	public EwayBillException() {
	}
	
	public EwayBillException(String errorCode,String message) {

		this.message=message;
		this.errorCode=errorCode;
	}
	
	public EwayBillException(Throwable throwable){
		super(throwable);
	}

	public String getMessage() {
		return message;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	
}
