package com.ginni.easemygst.portal.bean;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.service.AccountService;
import com.ginni.easemygst.portal.helper.AppException;

@Named("paymentBean")
@RequestScoped
public class PaymentBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private AccountService accountService;

	@SuppressWarnings("finally")
	public boolean checkPaymentStatus(String paymentId) {
		try {
			accountService = (AccountService) InitialContext.doLookup("java:comp/env/account");
			return accountService.checkPaymentStatus(paymentId);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (AppException e) {
			e.printStackTrace();
		} finally {
			return Boolean.FALSE;
		}
	}

}
