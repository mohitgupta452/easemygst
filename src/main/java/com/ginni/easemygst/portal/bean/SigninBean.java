package com.ginni.easemygst.portal.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.business.service.FinderService;
import com.ginni.easemygst.portal.business.service.UserService;
import com.ginni.easemygst.portal.constant.ExceptionCode;
import com.ginni.easemygst.portal.helper.AppException;
import com.ginni.easemygst.portal.helper.RedisService;
import com.ginni.easemygst.portal.notify.SMSFactory;
import com.ginni.easemygst.portal.security.TokenService;
import com.ginni.easemygst.portal.utils.Utility;

@Named
@RequestScoped
public class SigninBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;

	@Inject
	private UserService userService;

	@Inject
	private FinderService finderService;

	@Inject
	private RedisService redisService;

	@Inject
	private TokenService tokenService;

	private UserDTO userDTO;

	private String otpCode;

	@PostConstruct
	public void init() {

	}
	
	private final String password_pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";

	public String storeUserDetails(UserDTO userDTO) throws AppException {
		this.otpCode = null;
		if (!(Objects.isNull(userDTO) && StringUtils.isEmpty(userDTO.getFirstName())
				&& StringUtils.isEmpty(userDTO.getLastName()) && StringUtils.isEmpty(userDTO.getPassword())
				&& StringUtils.isEmpty(userDTO.getUsername()) && Objects.isNull(userDTO.getUserContacts())
				&& Objects.isNull(userDTO.getUserContacts().get(0))
				&& StringUtils.isEmpty(userDTO.getUserContacts().get(0).getEmailAddress())
				&& StringUtils.isEmpty(userDTO.getUserContacts().get(0).getMobileNumber())
				&& Objects.isNull(userDTO.getOrganisation())
				&& StringUtils.isEmpty(userDTO.getOrganisation().getName()))) {

			if (!Objects.isNull(finderService.getExistingUser(userDTO.getUsername()))) {
				throw new AppException(ExceptionCode._DUPLICATE_USER);
			}
			
			final Pattern pattern1 = Pattern.compile(password_pattern);
			if (!pattern1.matcher(userDTO.getPassword()).matches()) {
				throw new AppException(ExceptionCode._INVALID_PASSWORD);
			}

			log.debug("storing user details attributes validation successful");
			this.userDTO = userDTO;
			this.otpCode = Utility.randomOtp(6);

			log.debug("storing data to redis server for 10 mins");
			redisService.setUser(userDTO.getUsername() + "_SIGNUPUSER", this.userDTO, true, 600);
			redisService.setValue(userDTO.getUsername() + "_SIGNUPOTP", this.otpCode, true, 600);

			log.debug("sending otp {} to user mobile number {}", this.otpCode,
					userDTO.getUserContacts().get(0).getMobileNumber());
			SMSFactory factory = new SMSFactory();
			Map<String, String> params = new HashMap<>();
			params.put("code", this.otpCode);
			params.put("toNumber", userDTO.getUserContacts().get(0).getMobileNumber());
			factory.smsSecureCode(params);
		} else {
			throw new AppException(ExceptionCode._INSUFFICIENT_PARAMETERS);
		}
		return this.otpCode;
	}

	public String resendOtp(String username) throws AppException {
		log.debug("getting data from redis using key {}", username);
		this.userDTO = redisService.getUser(username + "_SIGNUPUSER");
		this.otpCode = Utility.randomOtp(6);
		if (this.userDTO != null) {
			log.debug("storing resend otp to redis server for 10 mins");
			redisService.setValue(userDTO.getUsername() + "_SIGNUPOTP", this.otpCode, true, 600);

			log.debug("sending otp {} to user mobile number {}", this.otpCode,
					userDTO.getUserContacts().get(0).getMobileNumber());
			SMSFactory factory = new SMSFactory();
			Map<String, String> params = new HashMap<>();
			params.put("code", this.otpCode);
			params.put("toNumber", userDTO.getUserContacts().get(0).getMobileNumber());
			factory.smsSecureCode(params);
			return userDTO.getUserContacts().get(0).getMobileNumber();
		} else {
			throw new AppException(ExceptionCode._INSUFFICIENT_PARAMETERS);
		}
	}

	public String getMobileOtp(String username, String mobileNumber) throws AppException {
		log.debug("getting data from redis using key {}", username);
		String otp = Utility.randomOtp(6);
		if (!StringUtils.isEmpty(username)) {
			log.debug("storing resend otp to redis server for 10 mins");
			redisService.setValue(username + "_SIGNINOTP", otp, true, 600);
			redisService.setValue(username + "_SIGNINMOBILE", mobileNumber, true, 600);
			log.debug("sending otp {} to user mobile number {}", otp, mobileNumber);
			SMSFactory factory = new SMSFactory();
			Map<String, String> params = new HashMap<>();
			params.put("code", otp);
			params.put("toNumber", mobileNumber);
			factory.smsSecureCode(params);
			return mobileNumber;
		} else {
			throw new AppException(ExceptionCode._INSUFFICIENT_PARAMETERS);
		}
	}

	public UserDTO verifyAccountActivate(String username, String firstName, String lastName, String otp, String password)
			throws AppException {
		log.debug("getting data from redis using key {}", username);
		if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(firstName) && !StringUtils.isEmpty(lastName)
				&& !StringUtils.isEmpty(otp) && !StringUtils.isEmpty(password)) {
			
			final Pattern pattern1 = Pattern.compile(password_pattern);
			if (!pattern1.matcher(password).matches()) {
				throw new AppException(ExceptionCode._INVALID_PASSWORD);
			}
			
			log.debug("gettin otp from redis server");
			String redisotp = redisService.getValue(username + "_SIGNINOTP");
			String mobilenumber = redisService.getValue(username + "_SIGNINMOBILE");
			if (redisotp.equals(otp)) {
				UserDTO userDTO = userService.verifyActivateAccount(username, firstName, lastName, mobilenumber, password);
				return userDTO;
			} else {
				throw new AppException(ExceptionCode._INVALID_OTP);
			}
		} else {
			throw new AppException(ExceptionCode._INSUFFICIENT_PARAMETERS);
		}
	}

	public boolean validateOtp(String otp, String username) throws AppException {
		log.debug("getting data from redis using key {}", username);
		this.userDTO = redisService.getUser(username + "_SIGNUPUSER");
		this.otpCode = redisService.getValue(username + "_SIGNUPOTP");
		if (this.otpCode != null && this.userDTO != null) {
			if (this.otpCode.equals(otp)) {
				log.debug("provided otp validated successfully");
				this.userDTO.getUserContacts().get(0).setMobileAuth((byte) 1);
				this.userDTO.setActive((byte) 1);
				this.userDTO.setStatus(UserService.UserStatus.ACTIVE.toString());

				log.debug("processing user account");
				processUserAccount();
				return true;
			} else {
				log.debug("provided otp is not correct");
				throw new AppException(ExceptionCode._INVALID_OTP);
			}
		}
		return false;
	}

	public UserDTO validateForgotOtp(String otp, String username) throws AppException {
		log.debug("getting data from redis using key {}", username);
		String redisotp = redisService.getValue(username + "_FORGOTOTP");
		if (redisotp != null && !StringUtils.isEmpty(username)) {
			if (redisotp.equals(otp)) {
				UserDTO userDTO = userService.getUserInfo(username);
				return userDTO;
			} else {
				log.debug("provided otp is not correct");
				throw new AppException(ExceptionCode._INVALID_OTP);
			}
		}
		return null;
	}

	public UserDTO getNewUserAuthToken(UserDTO userDTO) throws AppException {
		if (!Objects.isNull(userDTO)) {
			String token = tokenService.generateToken(userDTO);
			userDTO.setStatus(token);
			return userDTO;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	private void processUserAccount() throws AppException {
		this.userDTO = userService.createUser(userDTO, UserService.RegisterUsing.EASEMYGST);
	}

	public UserDTO getUserAuthToken() throws AppException {
		if (this.userDTO != null) {
			String token = tokenService.generateToken(this.userDTO);
			this.userDTO.setStatus(token);
			return this.userDTO;
		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public String forgotPassword(String identity, String ipAddress) throws AppException {

		if (!StringUtils.isEmpty(identity) && !StringUtils.isEmpty(ipAddress)) {
			String mobileNumber = userService.forgotPassword(identity, ipAddress);
			if(identity.equalsIgnoreCase("ankush@goyalsons.com")){
				String defaultotp="123456";
				redisService.setValue(identity + "_FORGOTOTP",defaultotp, true, 600);
				log.debug("sending otp {} to user mobile number {}",defaultotp, mobileNumber);
				return mobileNumber;
			}
			
			String otp = Utility.randomOtp(6);
			redisService.setValue(identity + "_FORGOTOTP", otp, true, 600);
			log.debug("sending otp {} to user mobile number {}", otp, mobileNumber);
			SMSFactory factory = new SMSFactory();
			Map<String, String> params = new HashMap<>();
			params.put("code", otp);
			params.put("toNumber", mobileNumber);
			factory.smsSecureCode(params);

			return mobileNumber;

		} else {
			throw new AppException(ExceptionCode._NULL_OBJECT_FOUND_INVALID_REQUEST);
		}
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

}
