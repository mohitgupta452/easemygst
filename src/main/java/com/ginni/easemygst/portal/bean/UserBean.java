package com.ginni.easemygst.portal.bean;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

import com.ginni.easemygst.portal.business.entity.UserDTO;
import com.ginni.easemygst.portal.helper.RedisService;

@Named("userBean")
@RequestScoped
public class UserBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger log;
	
	@Inject
	private RedisService redisService;
	
	private UserDTO userDto;
	
	private String ipAddress;

	public UserDTO getUserDto() {
		return userDto;
	}

	public void setUserDto(UserDTO userDto) {
		this.userDto = userDto;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setUserFromRedis(String key) {
		this.userDto = redisService.getUser(key);
	}
	
}
