update uploaded_csv set monthYear='<%monthYear%>', errMsg=case WHEN GST_APPLICABILITY NOT IN ('OUTWARD SUPPLY', 'OTHER OUTWARD SUPPLY', 'SALES DEBIT NOTE', 'SALES CREDIT NOTE', 'OTHER SALES DEBIT NOTE', 'OTHER SALES CREDIT NOTE', 'INWARD SUPPLY', 'REVERSE INVOICE', 'REVERSE DEBIT NOTE', 'REVERSE CREDIT NOTE', 'PURCHASE DEBIT NOTE', 'PURCHASE CREDIT NOTE') THEN 'GST_APPLICABILITY is not as per the allowed values.' WHEN TRADETYPE NOT IN ('LOCAL', 'INTER STATE', 'EXPORT/IMPORT') THEN 'Allowed values of ENTRY_TRADETYPE are Local/Inter state/(Export/Import).'

WHEN GST_APPLICABILITY IN ('Outward SUPPLY','Sales Debit Note','Sales Credit Note') AND (TOTAL_TAXRATE ='' or TOTAL_TAXRATE IS NULL)  THEN 'GST has to be charged when GST Debit Note is being raised. The rate can be 0 though.'WHEN

 HSN_SAC_CODE IN ('NON GST','Non GST Services') AND ((TRADETYPE='EXPORT/IMPORT' AND TOTAL_TAXRATE<>0) OR (TOTAL_TAXRATE <> '' and TOTAL_TAXRATE IS NOT NULL and TOTAL_TAXRATE <> 0)) THEN 'When HSN SAC Code is NON GST, GST cannot be applied.'

 WHEN GST_APPLICABILITY IN ('Other Outward Supply','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE <> '' AND TOTAL_TAXRATE IS NOT NULL) THEN 'GST cannot be charged when GST_APPLICABILITY is Other type document.' WHEN 

GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase CreditNote') AND (CP_GSTIN_NO IS NULL OR CP_GSTIN_NO = '') AND (TOTAL_TAXRATE <> '' AND TOTAL_TAXRATE IS NOT NULL) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE IN ('LOCAL','Inter state') THEN 'Counter party GSTIN is mandatory when GST is charged.'

 WHEN GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase CreditNote') AND TRADETYPE='EXPORT/IMPORT' AND (TOTAL_TAXRATE <> '' AND TOTAL_TAXRATE IS NOT NULL)  THEN 'GST cannot be charged on imports if reverse charge is not done.'

 WHEN GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note') AND (TOTAL_TAXRATE ='' and TOTAL_TAXRATE IS NULL) THEN 'GST has to be charged when Reverse Credit / Debit Note is being raised. The rate can be 0 though.' END where uploadDataInfoId=<%uploadDataInfoId%>;


update uploaded_csv set isError=true where errMsg is not null and errMsg <>'' and uploadDataInfoId=<%uploadDataInfoId%>;


UPDATE uploaded_csv set returnType='GSTR1',type='B2B',monthYear='<%monthYear%>' WHERE GST_APPLICABILITY='OUTWARD SUPPLY' and (CP_GSTIN_NO IS NOT NULL and CP_GSTIN_NO <> '' ) AND HSN_SAC_CODE<>'' and uploadDataInfoId=<%uploadDataInfoId%> and (TRADETYPE='LOCAL' or TRADETYPE='INTER STATE') and TOTAL_TAXRATE <> 0 AND isError=false AND (type='' or type is null);


UPDATE uploaded_csv set returnType='GSTR1' ,type='B2CL',monthYear='<%monthYear%>' 
WHERE GST_APPLICABILITY IN ('OUTWARD SUPPLY') and (CP_GSTIN_NO IS  NULL or CP_GSTIN_NO='') and ENTRY_NO<>'CONSOLIDATED' and (TRADETYPE='INTER STATE' ) AND ENTRY_NETVALUE >=250000 and TOTAL_TAXRATE >=0 AND HSN_SAC_CODE<>'NON GST' and uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);

UPDATE uploaded_csv set returnType='GSTR1',type='B2CS',monthYear='<%monthYear%>'
WHERE GST_APPLICABILITY IN ('OUTWARD SUPPLY','Other Sales Debit Note','Sales Debit Note','SALES CREDIT NOTE') and (CP_GSTIN_NO IS null or CP_GSTIN_NO='') and ( TRADETYPE='LOCAL' OR (TRADETYPE='INTER STATE' and (ENTRY_NETVALUE <250000 or ENTRY_NO='CONSOLIDATED')) ) and TOTAL_TAXRATE > 0  AND HSN_SAC_CODE<>'NON GST' and uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);

UPDATE uploaded_csv set returnType='GSTR1' ,type='EXP',monthYear='<%monthYear%>'
WHERE ((GST_APPLICABILITY='OUTWARD SUPPLY' and TOTAL_TAXRATE>=0) or (GST_APPLICABILITY='Other Outward Supply' and (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE=''))) and (TRADETYPE='EXPORT/IMPORT') and  uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);

UPDATE uploaded_csv set returnType='GSTR1' ,type='CDN',monthYear='<%monthYear%>'
WHERE GST_APPLICABILITY in ('Other Sales Debit Note','SALES DEBIT NOTE','SALES CREDIT NOTE')  and (CP_GSTIN_NO IS NOT NULL and CP_GSTIN_NO <> '' ) and TOTAL_TAXRATE>0 AND uploadDataInfoId=<%uploadDataInfoId%> and (TRADETYPE='INTER STATE' OR TRADETYPE='LOCAL') and HSN_SAC_CODE not in ('Non-GST Services','NON GST') AND isError=false AND (type='' or type is null);


UPDATE uploaded_csv set returnType='GSTR1' ,type='CDNUR',monthYear='<%monthYear%>',invoiceType='EXPWOP'
WHERE ((GST_APPLICABILITY='OTHER SALES DEBIT NOTE' or GST_APPLICABILITY='SALES DEBIT NOTE') AND TOTAL_TAXRATE>=0 )OR ((GST_APPLICABILITY='OTHER SALES CREDIT NOTE' or GST_APPLICABILITY='SALES CREDIT NOTE' ) AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE=''))AND (TRADETYPE='INTER STATE' AND ENTRY_NETVALUE >= 250000 AND (CP_GSTIN_STATE_CODE IS NULL OR CP_GSTIN_STATE_CODE ='') OR TRADETYPE='EXPORT/IMPORT') AND uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);


UPDATE uploaded_csv set returnType='GSTR2' ,type='B2B',monthYear='<%monthYear%>'
WHERE (GST_APPLICABILITY='INWARD SUPPLY' OR GST_APPLICABILITY='REVERSE INVOICE' ) and (CP_GSTIN_NO IS NOT NULL and CP_GSTIN_NO <> '' ) and  TOTAL_TAXRATE>0 and HSN_SAC_CODE<>'NON GST' AND (TRADETYPE='LOCAL' or TRADETYPE='INTER STATE') and uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);


UPDATE uploaded_csv set returnType='GSTR2' ,type='B2BUR',monthYear='<%monthYear%>'
WHERE GST_APPLICABILITY='REVERSE INVOICE'  AND (CP_GSTIN_NO IS  NULL or CP_GSTIN_NO = '' ) AND HSN_SAC_CODE<>'NON GST' and TOTAL_TAXRATE>0 and uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null); 


UPDATE uploaded_csv set returnType='GSTR2' ,type='CDN',monthYear='<%monthYear%>'
WHERE (GST_APPLICABILITY='PURCHASE CREDIT NOTE' or GST_APPLICABILITY='PURCHASE DEBIT NOTE' ) AND HSN_SAC_CODE<>'NON GST' and (CP_GSTIN_NO IS NOT NULL and CP_GSTIN_NO <> '' ) and  TOTAL_TAXRATE > 0 AND uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);


UPDATE uploaded_csv set returnType='GSTR2' ,type='CDNUR',monthYear='<%monthYear%>'
WHERE ((GST_APPLICABILITY='REVERSE CREDIT NOTE' or GST_APPLICABILITY='REVERSE DEBIT NOTE') AND (TRADETYPE='LOCAL' and TOTAL_TAXRATE >0) AND (CP_GSTIN_NO IS  NULL or CP_GSTIN_NO = '' )) OR  ((GST_APPLICABILITY='Purchase Debit Note' OR GST_APPLICABILITY='Purchase Credit Note') AND TRADETYPE='EXPORT/IMPORT' and (TOTAL_TAXRATE ='' OR TOTAL_TAXRATE IS NULL) AND HSN_SAC_CODE<>'NON GST') AND uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);

UPDATE uploaded_csv set returnType='GSTR2' ,type='IMPG',monthYear='<%monthYear%>'
WHERE ((((TOTAL_TAXRATE = 0 OR (TOTAL_TAXRATE > 0 AND HSN_SAC_CODE <>'NON GST')) AND GST_APPLICABILITY='REVERSE INVOICE')  and (CP_GSTIN_NO IS  NULL or CP_GSTIN_NO ='' )) or (GST_APPLICABILITY='INWARD SUPPLY' AND (TOTAL_TAXRATE ='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='EXPORT/IMPORT' AND HSN_SAC_CODE not LIKE '99%' and HSN_SAC_CODE<>'NON GST' AND uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);

UPDATE uploaded_csv set returnType='GSTR2' ,type='IMPS',monthYear='<%monthYear%>'
WHERE ((((TOTAL_TAXRATE = 0 OR (TOTAL_TAXRATE > 0 AND HSN_SAC_CODE <>'NON GST')) AND GST_APPLICABILITY='REVERSE INVOICE')  and (CP_GSTIN_NO IS  NULL or CP_GSTIN_NO ='' )) or (GST_APPLICABILITY='INWARD SUPPLY' AND (TOTAL_TAXRATE ='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='EXPORT/IMPORT' AND HSN_SAC_CODE  LIKE '99%' and HSN_SAC_CODE<>'NON GST' AND uploadDataInfoId=<%uploadDataInfoId%> AND isError=false AND (type='' or type is null);

update uploaded_csv set returnType='GSTR1',type='NIL',monthYear='<%monthYear%>',subType=case WHEN (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='LOCAL'  AND (CP_GSTIN_NO is null OR CP_GSTIN_NO='') AND HSN_SAC_CODE='NON GST'  THEN 'INTRA-B2CS_NONGST' 
WHEN  (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='LOCAL'  AND (CP_GSTIN_NO is NOT null OR CP_GSTIN_NO<>'') AND HSN_SAC_CODE='NON GST'  THEN 'INTRA-B2B_NONGST' 
 
WHEN (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='LOCAL'  AND (CP_GSTIN_NO is null OR CP_GSTIN_NO='') AND HSN_SAC_CODE<>'NON GST'  THEN 'INTRA-B2CS_NIL' 

WHEN  (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='LOCAL'  AND (CP_GSTIN_NO is NOT null OR CP_GSTIN_NO<>'') AND HSN_SAC_CODE<>'NON GST'  THEN 'INTRA-B2B_NIL'

WHEN (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='INTER STATE'  AND (CP_GSTIN_NO is null OR CP_GSTIN_NO='') AND HSN_SAC_CODE='NON GST'  THEN 'INTER-B2CS_NONGST' 
WHEN  (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='INTER STATE'  AND (CP_GSTIN_NO is NOT null OR CP_GSTIN_NO<>'') AND HSN_SAC_CODE='NON GST'  THEN 'INTER-B2B_NONGST' 
 WHEN (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='INTER STATE'  AND (CP_GSTIN_NO is null OR CP_GSTIN_NO='') AND HSN_SAC_CODE<>'NON GST' AND ENTRY_NETVALUE < 250000 THEN 'INTER-B2CS_NIL' 
WHEN  (((GST_APPLICABILITY='OUTWARD SUPPLY' OR GST_APPLICABILITY='Sales Credit Note') AND TOTAL_TAXRATE=0) OR (GST_APPLICABILITY IN('OTHER OUTWARD SUPPLY','Other Sales Debit Note','Other Sales Credit Note') AND (TOTAL_TAXRATE='' OR TOTAL_TAXRATE IS NULL))) AND TRADETYPE='INTER STATE'  AND (CP_GSTIN_NO is NOT null OR CP_GSTIN_NO<>'') AND HSN_SAC_CODE<>'NON GST'  THEN 'INTER-B2B_NIL' END 
 WHERE uploadDataInfoId=<%uploadDataInfoId%> AND (type='' or type is null) AND GST_APPLICABILITY IN ('OUTWARD SUPPLY', 'OTHER OUTWARD SUPPLY', 'SALES DEBIT NOTE', 'SALES CREDIT NOTE', 'OTHER SALES DEBIT NOTE', 'OTHER SALES CREDIT NOTE') and isError=false;

UPDATE uploaded_csv set returnType='GSTR2',type='NIL',monthYear='<%monthYear%>',subType=case 
WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE='NON GST' AND TRADETYPE='LOCAL' THEN 'INTRA-NONGST'

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE='LOCAL' AND CP_REGISTRATION_STATUS IN ('Normal Registered','Unregistered') THEN 'INTRA-NIL'

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE='LOCAL' AND CP_REGISTRATION_STATUS IN ('Unregistered','Exempt') THEN 'INTRA-EXEMPT'

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE='LOCAL' AND CP_REGISTRATION_STATUS IN ('Composite') THEN 'INTRA-COMPOUNDING'

-- INTER NIL GSTR 2

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND ((TRADETYPE ='INTER STATE' AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) OR (TRADETYPE ='EXPORT/IMPORT' AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE=''))) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND TRADETYPE ='INTER STATE') AND HSN_SAC_CODE='NON GST'  THEN 'INTER-NONGST'

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE='INTER STATE' AND CP_REGISTRATION_STATUS IN ('Normal Registered','Unregistered') THEN 'INTER-NIL'

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE='INTER STATE' AND CP_REGISTRATION_STATUS IN ('Unregistered','Exempt') THEN 'INTER-EXEMPT'

WHEN ((GST_APPLICABILITY IN ('Inward Supply','Purchase Debit Note','Purchase Credit Note')  AND (TOTAL_TAXRATE IS NULL OR TOTAL_TAXRATE='' OR TOTAL_TAXRATE=0)) or (GST_APPLICABILITY IN ('Reverse Invoice','Reverse Credit Note','Reverse Debit Note')  AND TOTAL_TAXRATE=0)) AND HSN_SAC_CODE<>'NON GST' AND TRADETYPE='LOCAL' AND CP_REGISTRATION_STATUS IN ('Composite') THEN 'INTER-COMPOUNDING' END 

 WHERE uploadDataInfoId=<%uploadDataInfoId%> AND GST_APPLICABILITY IN ('INWARD SUPPLY', 'REVERSE INVOICE', 'REVERSE DEBIT NOTE', 'REVERSE CREDIT NOTE', 'PURCHASE DEBIT NOTE', 'PURCHASE CREDIT NOTE') AND (type='' or type is null) and isError=false;


UPDATE uploaded_csv SET ctinAvailability=case WHEN CP_GSTIN_NO='' or CP_GSTIN_NO is null THEN 'NOT-EXISTS' ELSE 'EXISTS' END , traderTypeNil= CASE WHEN TRADETYPE='LOCAL' THEN 'INTRA' WHEN TRADETYPE='INTER STATE' OR TRADETYPE='EXPORT/IMPORT' THEN 'INTER' WHEN CP_GSTIN_STATE_CODE='<%gstnPos%>' THEN 'INTRA' ELSE 'INTER' END , hsnSacNil= CASE WHEN HSN_SAC_CODE='Non-GST Services' THEN 'NON-GST' ELSE 'OTHER THAN NON-GST' END WHERE uploadDataInfoId=<%uploadDataInfoId%> AND (type='' or type is null);

update uploaded_csv set TAXABLE_AMOUNT=	TAXABLE_AMOUNT *(-1),CGST_AMOUNT=(-1)*CGST_AMOUNT,SGST_AMOUNT=(-1)*SGST_AMOUNT,IGST_AMOUNT=(-1)*IGST_AMOUNT,CESS_AMOUNT=(-1)*CESS_AMOUNT,NET_AMOUNT=(-1)*NET_AMOUNT where type IN ('B2CS','NIL') and GST_APPLICABILITY IN ('SALES CREDIT NOTE','OTHER SALES CREDIT NOTE') AND uploadDataInfoId=<%uploadDataInfoId%>
