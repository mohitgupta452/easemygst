<%@page import="com.ginni.easemygst.portal.bean.PaymentBean"%>
<%@page import="com.ginni.easemygst.portal.payment.PaytmConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,com.paytm.pg.merchant.CheckSumServiceHelper"%>

<%
Enumeration<String> paramNames = request.getParameterNames();

Map<String, String[]> mapData = request.getParameterMap();
TreeMap<String,String> parameters = new TreeMap<String,String>();
String paytmChecksum =  "";
while(paramNames.hasMoreElements()) {
	String paramName = (String)paramNames.nextElement();
	%>
	<%= paramName %>
	<%
	if(paramName.equals("CHECKSUMHASH")){
		paytmChecksum = mapData.get(paramName)[0];
	}else{
		parameters.put(paramName,mapData.get(paramName)[0]);
	}
}%>
<%=parameters%>
<%boolean isValideChecksum = false;

PaymentBean paymentBean = new PaymentBean();
paymentBean.checkPaymentStatus(parameters.get("ORDERID"));

//String outputHTML="";
try{
	isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(PaytmConstants.MERCHANT_KEY,parameters,paytmChecksum);
	if(isValideChecksum && parameters.containsKey("RESPCODE")){
		if(parameters.get("RESPCODE").equals("01")){
			//outputHTML = parameters.toString();
			response.sendRedirect("https://dev.easemygst.com/dev/restricted/payment-success.html");
		}else{
			response.sendRedirect("https://dev.easemygst.com/dev/restricted/payment-failure.html");
		}
	}else{
		response.sendRedirect("https://dev.easemygst.com/dev/restricted/payment-failure.html");
	}
}catch(Exception e){
	//outputHTML=e.toString();
	response.sendRedirect("https://dev.easemygst.com/dev/restricted/payment-failure.html");
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%-- <%= outputHTML %> --%>
</body>
</html>